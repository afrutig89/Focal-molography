.. Moloreader Setup User Interface documentation master file, created by
   sphinx-quickstart on Thu Jul 28 17:45:19 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Data Processing Library
===========================================================

This is a guide on how to set up and use the user interface for the moloreader setup. 

.. toctree::
   :maxdepth: 2
   
.. automodule:: MologramProcessing
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

