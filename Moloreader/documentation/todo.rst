 cvfg6t7Todo
====

- experiment tab
    - generate protocol (experiment)
    - update time
    - status label
    - debug: position if not mologram
    - adjust MS
    - stop experiment

- time stack
    - time interval -> total time changeable
    - Curve combo index bug

- z-stack
    - increase in um bug

- settings tab
    - save defaults (database)
    - mask different type / name
    - image naming (1 hour)
    - settings:
        - focus
        - image quicksave (scalebar, intensity)
        - shortkeys
        - surface - mologram height

- Array measurements
    - log / protocol
    - center only if SNR > 10

- Camera Feed widget (1 hour)
    - set as background: background -> find position
    - low signal detection: crop image
    - tabs
    - 1D axes

- log

- MS correction (surface measurements)

- image processing
    - statements about number of particles grooves / ridges
    - analyse surface only at specific range
    - Mologram / aside
    - Otsu filter
    - generate video


- protocol tab
    - add objects


- calibration
    - as thread
    - calibration and offset are not consistent
    - calibration tab: go to reference position
    - implement go to start position
    - go to reference
    - database: different positions stored
    - find surface
    - adjust MS
    - define limits -> restrictions


- Manual motor control (1 hour)
    - set reference z-axis
    - implement goToPreviousPosition
    

- Database Plots
    - show curve (qtablewidget)
    - filter (e.g. surface, mologram)
    - rewrite

- Load image: use infos stored in image file

- bragg condition tab

- molowidget: Field not well determined (e.g. y = -0.1 ==> A)

- z-axis negative (?)

- distance surface - molo adjustable

- distance measurement in image

- start window

- define focal point / range manually

- subtract background

- find surface
