Theory
======

How to calculate intensity
--------------------------

- Digital output :math:`M`
- Pixel gain :math:`k` (calculated in `noise experiment`__)
- Number of electrons :math:`n_e`

	.. math:: n_e = \frac{M}{k}

- Quantum efficiency :math:`q` (datasheet_)
- Number of photons :math:`n_p`

	.. math:: n_p = \frac{n_e}{q}

- Camera gain :math:`g_c`
- Pixelsize :math:`a` :math:`\mathrm{[m]}` (datasheet_)
- Flux of photons :math:`\phi` :math:`\left[\mathrm{\frac{Photons}{s m^2}}\right]`

	.. math:: \phi = \frac{n_p}{a^2 t g_c}

- Wavelength :math:`\lambda` :math:`\mathrm{[m]}`
- Intensity :math:`I` :math:`\mathrm{\left[\frac{W}{m^2}\right]}`

	.. math:: I = \phi \frac{hc}{\lambda}

	with :math:`h` being the Planck constant and :math:`c` the speed of light.

- Elementary charge :math:`q_e` :math:`\mathrm{[C]}`
- Photocurrent density :math:`j` :math:`\mathrm{\left[\frac{A}{m^2}\right]}`

    .. math:: j = q \frac{q_ecI}{h\lambda}


.. _datasheet: /home/focal-molography/Focal Molography/Moloreader/documentation/Datasheets/UI-3480CP-M-GL_Rev_2.pdf
.. _experiment: /home/focal-molography/Experiments/_build/html/029_SB_Debugging/Report.html

__ experiment_