Reference Mologram
======

-----------------------------------------


Introduction
^^^^^^^^^^^^^
We would like to measure the intensity of two molograms at the same time. One mologram will be used as a referencing system.
Two molograms, if sufficiently close to each other, should experience similar if not same fluctuation and noise. Therefore, by
implementing a referencing system, we wish to decrease the level of fluctuation and other types of noise by differential measurements.

This functionality can be used with current moloreader set up. There is a input button on the bottom right corner named "reference mologram".
If this button is selected, the algorithm will assume there are two molograms present in the field of the view and try to compute their 
corresponding intensities. The program only assumes two molograms are horizontally next to each other at the moment. Therefore if one wishes
to perform referencing with vertically arranged molograms, a little modification needs to be done (change hsplit to vsplit in MologramProcessing.py)

Real time measurement is also available. When taking stack measurements with "reference mologram" button checked, the program will
plot both the signal intensiy and the reference intensity in real time.



Following files enable the reference mologram functionality
^^^^^^
- GUI/CameraWidget/CameraControl.py
    Set up the GUI input button for reference mologram
- GUI/CameraWidget/CameraFeed.py
    Set up the GUI display for reference mologram
- GUI/Main_widget.py
    In updateImg function, the program will perform differently according to the reference mologram selection.
- Data_Acquisition/Experiment.py
    Where all the mologram calculations are. Reference mologram will also be calculated if selected in the GUI
- Data_Processing/MologramProcessing.py
    The algorithms for intensity calculation can be found. The algorithm was expanded from measuring only one mologram to possibly two as needed.
- GUI/Image_Acquisition_Tab/Stack.py
    enable the time series measurements with reference mologram functionaly
