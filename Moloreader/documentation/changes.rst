Changes
=======

31 Mar 2017
-----------
- All stacks work properly now

30 Mar 2017
-----------
- Highlight maximum
- Different design (Manual Motor Control)


28 Mar 2017
-----------
- Count particles


22 Mar 2017
-----------
- General settings
- load database optimized
- background measurements

20 Mar 2017
-----------
- Adjust coupling when background measurements (array)
- some bugs fixed in database plots and experiment tab
- low signal detection works, but slow


17 Mar 2017
-----------
- Experiment widget works
- one defined experiment folder

08 Mar 2017
-----------
- Protocol widget works: load protocol, save RST, generate HTML


02 Mar 2017
-----------
- array measurments adaptation for Bragg (fixed exposure, reversed line scanning)


28 Feb 2017
-----------
- adjust finding mologram -> to be completed tomorrow (x,y in mask not correct)

23 Feb 2017
-----------
- Changed x,y and adjusted the plots and functions


17 Feb 2017
-----------
- Experiment tab
- Settings tab


16 Feb 2017
-----------
- Debugging
- Adjust maximum


15 Feb 2017
-----------
- Tidy up
- Camera kaputt?!
- Calibration

14 Feb 2017
-----------
- Time stack


10 Feb 2017
-----------
- Check if file exists
- limit exposure range (linear < 300)
- database
- time stack
- background analysis
- image processing

09 Feb 2017
-----------
- (SB) improved Adjust focus
- (SB) colorbar
- (SB) changeable color power norm
- (SB) settings tab (not working yet)
- (SB) optimization Array Measurements


08 Feb 2017
-----------
- (SB) Array measurements working


07 Feb 2017
-----------
- (SB) multiple changes
- (SB) preparation array measurements (due 08 Feb 2017)

06 Feb 2017
-----------
- (SB) Autocoupling


03 Feb 2017
-----------
- (SB) Auto exposure (binary search. However, if adjusted downwards its not saturated and adjusted upwards its over saturated. Maybe need to take multiple pictures. However that would take longer)
- (SB) Auto center



01 Feb 2017
-----------
- (SB) Got rid of expert mode
- (SB) Got rid of savety button (always set)
- (SB) Save permanent position (B11)


31 Jan 2017
-----------
- (SB) Adjusted size of current mologram (objective size)
- (SB) added set position and surface / mologram heights to manual motor control


26 Jan 2017
-----------
- Change in Reader_Control/Axis_Control/Axis.py:

	.. code-block:: python

	        396 # Start in negative direction at 1 revolution
	        397 steps = self.steps_per_revolution/369

	to

	.. code-block:: python

	        396 # Start in negative direction at 1 revolution
	        397 steps = self.steps_per_revolution/360

- Deleted redundant function in Reader_Control/Axis_Control/Axis.py:

	.. code-block:: python

		    370 def calib(self):
		    371    print('Calibrating...')
		    372    print('Done calibration')

25 Jan 2017
-----------
- (SB) Go to mologram height / go to surface should work now


23 Jan 2017
-----------
- (SB) Got rid of 'offline mode'
- (SB) Executable file