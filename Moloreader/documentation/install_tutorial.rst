************
Installation
************


The first step in installing the software to control the molography setup is installing git and cloning the software repository. To install git

.. code-block:: bash

	sudo apt-get install git

Next, clone the control software repository using

.. code-block:: bash
	
	git clone https://github.com/afrutig/Moloreader_high_end.git

Before installing the software, download the correct IDS STK file from here_ and save it in your downloads folder.

.. _here: https://en.ids-imaging.com/download-ueye-win32.html

You are now ready to run the install.sh file in the Moloreader_high_end directory using

.. code-block:: bash
	
	sudo ./install.sh

The software should now be installed but in order to connect to the setup properly you need to modify some settings from your wireless connection. Click on the connections tab  in the upper left corner of your screen and select 'Edit Connections...' at the bottom of the drop-down menu. Next, select the wired conection used to connect to the Raspberry Pi via ethernet and go to the 'IPv4 Settings' tab and set to 'Shared to ther computers'. 

You should now be ready to use the setup. You can start the program by typing the following command in the command line in the Moloreader_high_end directory

.. code-block:: bash
	
	./run.sh