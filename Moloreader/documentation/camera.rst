Camera
======

Application notes UI-148x/UI-348x/UI-548x
-----------------------------------------

Sensor
^^^^^^
- The use of the global start function slightly reduces the maximum possible frame rate because the sensor needs to be run in trigger mode.
- At very long exposure times (> 100 ms) and minimum gain, the white level may not be reached. The gain should be increased by one step in this case.
- For hardware reasons, the sensor can only perform 3x vertical binning. When 3x horizontal binning is activated in the uEye software, the driver uses 3x subsampling instead. Therefore, the image will not become brighter when 3x horizontal binning is activated.
- Subsampling and Binning cannot be used simultaneously.
- A very low pixel clock and a high gain in combination with high temperature may cause a strip pattern in the image.

Color version
^^^^^^^^^^^^^
- Live color display with color correction and de-Bayering with a big filter mask results in high CPU load (see Color filter (Bayer filter)).

Monochrome version
^^^^^^^^^^^^^^^^^^
- The sensor has no gain boost (factor) function. Compared to the color sensor this function is integrated in the increased master gain.
- The sensor performs color binning and color subsampling. This might lead to artifacts when using binning and subsampling. Therefore, the monochrome sensor does not support binning factors higher than 2x.
- Gain settings 0…49 use analog signal gain; from 50 up, the stronger digital gain is used. High gain settings may cause visible noise.
- At 12 bits per pixel mode, the master gain shall not be set to value over 50.

Lens
^^^^
- It is recommended to use a high-resolution megapixel lens.


Implementation
--------------

.. automodule:: Camera
   :members:
   :undoc-members:
   :show-inheritance: