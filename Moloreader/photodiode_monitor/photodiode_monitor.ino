
typedef unsigned long int uint_32;

void setup(){

	// begin serial communication and set baud rate to 9600
	Serial.begin(9600);
}

// return the intensity of the laser summed over the given time duration
double read_laser_intensity(double duration){

	// initialize variables

        int     photodiode_pin    = 0;
	uint_32 laser_intensity   = 0;
	double  time              = 0;
	double  start_time        = millis();

	// record while time is less than specified duration
	while( (millis()-start_time) < duration )
		laser_intensity += analogRead(photodiode_pin);

	//return (int)laser_intensity;
        return laser_intensity;
}

void loop(){

	// wait for start signal and duration
	if(Serial.available() < 2)
	{
		// decode received message
		int duration = 0;
		for(int i = Serial.available(); i>0; i--)
			duration += Serial.read() << (i-1)*8;

		// record value
		uint_32 val = read_laser_intensity(duration);

	        byte mask = 0b11111111;
                byte byte0 = val & mask;
                val = val >> 8;
                byte byte1 = val & mask;
                val = val >> 8;
                byte byte2 = val & mask;

		// transmit back
		Serial.write(byte0);
		Serial.write(byte1);
		Serial.write(byte2);
	}
}
