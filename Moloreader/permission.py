"""
This file can be used to give all files, folders and subfolders the 
same rights and change the ownership (SUDO_UID).

Permissions are a bit mask, for example, rwxr-xr-x is 111101101 in 
binary, and is grouped to bits by 3 (octal).

Use 0755 in Python2
Use 0o755 in Python3

This file must be run as root

:author:    Silvio Bischof
:created:    23-Jan-2017
"""


import os

def changePermissionsRecursive(path, rights):
    """
    Changes the rights of all files and folders.

    :param path: path of the root folder
    :param rights: rights given as octal number
    :type rights: int
    """

    uid = os.environ.get('SUDO_UID')
    gid = os.environ.get('SUDO_GID')
    for root, dirs, files in os.walk(path, topdown=False):
        dirList = ([os.path.join(root, d) for d in dirs])
        for directory in dirList:
            os.chmod(directory, rights)
            os.chown(directory, int(uid), int(gid))
        for file in [os.path.join(root, f) for f in files]:
            os.chmod(file, rights)
            os.chown(file, int(uid), int(gid))

if __name__ == '__main__':
    changePermissionsRecursive('.', 0o755)