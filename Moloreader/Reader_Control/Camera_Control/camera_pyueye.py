#!/usr/bin/env python

#------------------------------------------------------------------------------
#                 PyuEye example - camera modul
#
# Copyright (c) 2017 by IDS Imaging Development Systems GmbH.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#------------------------------------------------------------------------------

from pyueye import ueye
from Reader_Control.Camera_Control.utils_pyueye import (uEyeException, Rect, get_bits_per_pixel,
                                  ImageBuffer, check, ImageData)

import time

import ctypes
import numpy as np
import matplotlib.pylab as plt

class Cam():

    def __init__(self, device_id=0):

        self.h_cam = ueye.HIDS(device_id)
        self.img_buffers = []
        self.exposure_time = 0
        self.minExposure = 100
        self.maxExposure = 0

    def __enter__(self):
        self.init()
        return self

    def __exit__(self, _type, value, traceback):
        self.exit()

    def handle(self):
        return self.h_cam

    def alloc(self, buffer_count=1):

        rect = self.get_aoi()
        bpp = get_bits_per_pixel(self.get_colormode())

        # clean up all the memory that was allocated, I am not sure, whether this needs to be here.
        for buff in self.img_buffers:
            check(ueye.is_FreeImageMem(self.h_cam, buff.mem_ptr, buff.mem_id))

        # this function instantiates the circular image buffer.
        for i in range(buffer_count):

            buff = ImageBuffer()
            ueye.is_AllocImageMem(self.h_cam,
                                  rect.width, rect.height, bpp,
                                  buff.mem_ptr, buff.mem_id)
            
            check(ueye.is_AddToSequence(self.h_cam, buff.mem_ptr, buff.mem_id))

            self.img_buffers.append(buff)

        check(ueye.is_InitImageQueue(self.h_cam, 0))


    def clear_buffer(self, buffer_count=1):
        # clears the image buffer
        check(ueye.is_ExitImageQueue(self.h_cam))
        check(ueye.is_ClearSequence(self.h_cam))
        self.img_buffers = []
        self.alloc(buffer_count)
        #check(ueye.is_InitImageQueue(self.h_cam, 0))

        
        #self.alloc(buffer_count)

    def get_image_info(self):

        pass

    def init(self):

        ret = ueye.is_InitCamera(self.h_cam, None)
        if ret != ueye.IS_SUCCESS:
            self.h_cam = None
            raise uEyeException(ret)

        pix_min, pix_max, increment = self.get_pixel_clock_range() # if the increment is 0, then only discrete pixel clocks are allowed.

        self.pixel_clocks = []

        # if the increment is zero the camera has only discrete pixel clocks. 
        if increment == 0:

            for i in list(np.arange(pix_min,pix_max)):
                # try is necessary, because certain pixelclocks are not allowed. and the pixel_clock_list_function of the camera returns a few corrupted values.
                try:

                    self.set_pixel_clock(i)
                    self.pixel_clocks.append(i)
                    
                except:
                    pass

        else:
            self.pixel_clocks = list(np.arange(pix_min,pix_max))

        self.gain = self.get_gain() # gets the hardware gain of the camera. 
            
        return ret

    def exit(self):
        ret = None
        if self.h_cam is not None:
            ret = ueye.is_ExitCamera(self.h_cam)
        if ret == ueye.IS_SUCCESS:
            self.h_cam = None

    def get_aoi(self):
        rect_aoi = ueye.IS_RECT()
        ueye.is_AOI(self.h_cam, ueye.IS_AOI_IMAGE_GET_AOI, rect_aoi, ueye.sizeof(rect_aoi))

        return Rect(rect_aoi.s32X.value,
                    rect_aoi.s32Y.value,
                    rect_aoi.s32Width.value,
                    rect_aoi.s32Height.value)

    def set_aoi(self, x, y, width, height):
        rect_aoi = ueye.IS_RECT()
        rect_aoi.s32X = ueye.int(x)
        rect_aoi.s32Y = ueye.int(y)
        rect_aoi.s32Width = ueye.int(width)
        rect_aoi.s32Height = ueye.int(height)
        # I would need to update the image buffer here. 

        ret = ueye.is_AOI(self.h_cam, ueye.IS_AOI_IMAGE_SET_AOI, rect_aoi, ueye.sizeof(rect_aoi))

        self.max_exposure_for_pixel_clock = [] # array of exposure ranges for different pixel_clocks

        self.minExposure = 100
        self.maxExposure = 0
        for i in self.pixel_clocks:
            
            self.set_pixel_clock(i)
            min_exp, max_exp,_ = self.get_exposure_range_current_pixelclock()

            self.max_exposure_for_pixel_clock.append(max_exp)

            self.minExposure = np.min((self.minExposure,min_exp))
            self.maxExposure = np.max((self.maxExposure,max_exp))

        return ret

    def capture_video(self, wait=False):
        wait_param = ueye.IS_WAIT if wait else ueye.IS_DONT_WAIT
        return ueye.is_CaptureVideo(self.h_cam, wait_param)

    def stop_video(self):
        return ueye.is_StopLiveVideo(self.h_cam, ueye.IS_FORCE_VIDEO_STOP)
    
    def capture_single_frame(self, wait=False):
        """acquire a single image, wait should be False since otherwise the camera waits for a trigger. This function is slow, do not use it
        for timestacks. 

        :returns: numpy array of the image

        """

        wait_param = ueye.IS_WAIT if wait else ueye.IS_DONT_WAIT

        check(ueye.is_FreezeVideo(self.h_cam, wait_param))
        img_buffer = ImageBuffer()
        self.timeout = 3000
        check(ueye.is_WaitForNextImage(self.h_cam,
                                           self.timeout,
                                           img_buffer.mem_ptr,
                                           img_buffer.mem_id))


        image_data = ImageData(self.h_cam, img_buffer)

        image_data.unlock()
        
        return image_data.as_raw_image()

    def set_colormode(self, colormode):
        check(ueye.is_SetColorMode(self.h_cam, colormode))
        
    def get_colormode(self):
        ret = ueye.is_SetColorMode(self.h_cam, ueye.IS_GET_COLOR_MODE)
        return ret

    def get_format_list(self):
        count = ueye.UINT()
        check(ueye.is_ImageFormat(self.h_cam, ueye.IMGFRMT_CMD_GET_NUM_ENTRIES, count, ueye.sizeof(count)))
        format_list = ueye.IMAGE_FORMAT_LIST(ueye.IMAGE_FORMAT_INFO * count.value)
        format_list.nSizeOfListEntry = ueye.sizeof(ueye.IMAGE_FORMAT_INFO)
        format_list.nNumListElements = count.value
        check(ueye.is_ImageFormat(self.h_cam, ueye.IMGFRMT_CMD_GET_LIST,
                                  format_list, ueye.sizeof(format_list)))
        return format_list

    def has_video_started(self):

        return ueye.is_HasVideoStarted(self.h_cam,ueye.c_int())

    def get_exposure_time(self):
        """
        :returns: exposure time in ms
        """

        exposure_time = ueye.c_double()
        check(ueye.is_Exposure(self.h_cam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE, exposure_time, ueye.sizeof(exposure_time)))

        return exposure_time
    
    def set_exposure_time(self, exposure_time_new, check_pixel_clock=True):
        """
        :param exposure time: exposure time in ms
        """
        if check_pixel_clock:
            # select the highest pixelclock that can still achieve this exposure in order to be fast.

            pixel_clocks = np.asarray(self.pixel_clocks)
            pixelclock = pixel_clocks[np.asarray(self.max_exposure_for_pixel_clock) > exposure_time_new][-1]

            # set the new pixel clock
            self.set_pixel_clock(pixelclock)   

            # check if exposure time actually in exposure time range given by this pixelclock.
            min_exp, max_exp ,_ = self.get_exposure_range_current_pixelclock()

            
            if not (exposure_time_new > min_exp and exposure_time_new < max_exp):

                print(min_exp)
                print(max_exp)
                print(pixelclock)
                print('Wanted to set the following exposure time:' + str(exposure_time_new))
                print('Range of the camera is:' + str(self.minExposure) + ',' + str(self.maxExposure))

                raise RuntimeError('The selected exposure time is not available for this camera.')


        exposure_time = ueye.c_double()
        exposure_time.value = exposure_time_new

        check(ueye.is_Exposure(self.h_cam, ueye.IS_EXPOSURE_CMD_SET_EXPOSURE ,exposure_time, ueye.sizeof(exposure_time)))

        self.exposure_time = self.get_exposure_time()
        # acquire an image in order to clear the buffer
        self.capture_single_frame() # capture 1 frame to free the buffer.

        return self.exposure_time


    def get_theoretical_frame_rate(self):

        frame_rate = ueye.c_double()
        
        check(ueye.is_SetFrameRate(self.h_cam, ueye.IS_GET_FRAMERATE,frame_rate))

        return frame_rate.value


    def get_exposure_range_current_pixelclock(self):
        """returns the exposure range as a min, max, incr, for current pixelclock"""

        lower = ueye.c_double()
        check(ueye.is_Exposure(self.h_cam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MIN, lower, ueye.sizeof(lower)))
        upper = ueye.c_double()
        check(ueye.is_Exposure(self.h_cam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MAX, upper, ueye.sizeof(upper)))
        incr = ueye.c_double()
        check(ueye.is_Exposure(self.h_cam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_INC, incr, ueye.sizeof(incr)))

        return lower.value, upper.value, incr.value

    def get_exposure_range_cam(self):

        return self.minExposure, self.maxExposure


    def get_pixel_clock(self):

        pixel_clock = ueye.c_int()
        check(ueye.is_PixelClock(self.h_cam, ueye.IS_PIXELCLOCK_CMD_GET, pixel_clock, ueye.sizeof(pixel_clock)))
        
        return pixel_clock

    def set_pixel_clock(self, pixel_clock_new):
        """
        :param pixel_clock: integer of the pixelclock in MHz.
        """
        pixel_clock = ueye.c_int()
        pixel_clock.value = pixel_clock_new
        check(ueye.is_PixelClock(self.h_cam, ueye.IS_PIXELCLOCK_CMD_SET, pixel_clock, ueye.sizeof(pixel_clock)))
        self.current_pixel_clock = self.get_pixel_clock() # update the current pixel clock 

    def get_pixel_clock_range(self):

        pixel_clock_range = ueye.c_int()
        check(ueye.is_PixelClock(self.h_cam, ueye.IS_PIXELCLOCK_CMD_GET_RANGE, pixel_clock_range, ueye.c_uint(12)))
        pixel_clock_range_pointer = ctypes.pointer(pixel_clock_range)

        return [pixel_clock_range_pointer[0].value, pixel_clock_range_pointer[1].value, pixel_clock_range_pointer[2].value]

    def get_pixel_clock_list(self):
        """
        :returns: the list of allowed pixel clocks. """


        pixel_clock_numbers = ueye.c_int()
        check(ueye.is_PixelClock(self.h_cam, ueye.IS_PIXELCLOCK_CMD_GET_NUMBER, pixel_clock_numbers, ueye.c_int(4))) # no camera has more than 150 different pixels clocks. 
        numbers = pixel_clock_numbers.value
        pixel_clock_list = ueye.c_int()
        check(ueye.is_PixelClock(self.h_cam, ueye.IS_PIXELCLOCK_CMD_GET_LIST, pixel_clock_list, ueye.c_int(4*numbers))) # no camera has more than 150 different pixels clocks. 
        pixel_clock_list_pointer = ctypes.pointer(pixel_clock_list)
        # filter out any corrupted values (I do not know why they occur)
        pixel_clock_list = [pixel_clock_list_pointer[i].value for i in range(numbers) if pixel_clock_list_pointer[i].value > 1 and pixel_clock_list_pointer[i].value < 500]

        return pixel_clock_list

    def _convert_intensity(self, img):
        pass

    def correct_hot_pixel(self):
        """
        correct the hot pixels, close camera cap, when you perform use this function. """

        hot_pixels = ueye.c_int()
        ueye.is_HotPixel(self.h_cam,ueye.IS_HOTPIXEL_ADAPTIVE_CORRECTION_DETECT_ONCE, hot_pixels, ueye.sizeof(hot_pixels))

    def get_gain(self):
        

        gain = ueye.c_int()
        dummy = ueye.c_int()
        ueye.is_SetHardwareGain(self.h_cam, gain, dummy, dummy, dummy)

        return gain.value



if __name__ == '__main__':

    #########################################################################################
    # test status
    # linearity over entire exposure range is fullfilled                -   AF. 17.4.2018

    linearity_test = False

    cam = Cam()

    cam.init()
    cam.set_colormode(ueye.IS_CM_SENSOR_RAW12)
    cam.set_aoi(0,0, 2560, 1920)
    
    cam.set_aoi(0,0, 400, 400)
    
    # cam.set_pixel_clock(104)
    # cam.set_aoi(0,0, 10, 10)
    print(cam.get_gain())
    print(cam.get_theoretical_frame_rate())

    cam.stop_video()
    cam.exit()
    
    # print(cam.get_pixel_clock())
    # for i in range(10):
    #     cam.set_exposure_time(10.)
    #     print(cam.get_exposure_time())
    #     print(cam.get_pixel_clock())
    # print(cam.get_exposure_range_current_pixelclock())
    # print(cam.get_pixel_clock_range())
    

    ### linearity exposure range test ###

    if linearity_test:
        cam = Camera()

        cam.init()
        cam.set_colormode(ueye.IS_CM_SENSOR_RAW12)
        cam.set_aoi(0,0, 2560, 1920)
        cam.alloc() # allocate the memory for this area of interest

        min_exp, max_exp = cam.get_exposure_range_cam()
        
        start = time.time()
        mean = []
        exp_times = np.logspace(np.log10(min_exp*1.1),np.log10(max_exp*0.9),20)
        for i in exp_times:
            
            cam.set_exposure_time(i)
            mean.append(np.mean(cam.capture_single_frame()))
            
        cam.stop_video()
        cam.exit()
        
        plt.loglog(exp_times,mean)
        plt.show()

    

