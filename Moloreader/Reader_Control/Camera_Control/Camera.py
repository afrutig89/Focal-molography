import os
import time
from PIL import Image
import tifffile
from PIL.ImageQt import ImageQt
from PyQt4 import QtGui
import numpy as np
import scipy
from scipy import ndimage
from Data_Acquisition.permission import changePermissionsPath
import matplotlib.pyplot as plt
import time
from GUI.PhotodiodeTab.PhotodiodeTab import LabJack
from settings import CameraSettings
from Reader_Control.Camera_Control.camera_pyueye import Cam
from pyueye import ueye
# Should be a general class for cameras.


class Camera():
    """
    Class that allows to connect, setup and control an IDS camera
    """
    def __init__(self, starting_exposure = 60, color_mode = 1, model = 'UI-3480CP-M-GL Rev.2', magnification = 20):
        """
        Constructor
        """
        self.specifications = CameraSettings(model, magnification)
        print('  >> power cycling USB ports')
        self.hard_reset()
        time.sleep(1)

        print('  >> connecting to camera')
        self.cam = Cam()
        print('hello')
        self.cam.init()
        print('hello')
        self.cam.set_colormode(ueye.IS_CM_SENSOR_RAW12)
        print('hello')
        self.cam.set_aoi(0,0, 2560, 1920)
        print('hello')
        self.cam.alloc()

        print('  >> initializing camera settings')
        self.color_mode = color_mode # Get images in RGB format
        
        self.auto_exposure = False
        self.gain = 0

        # define exposure time range
        self.exposure = self.set_exposure(500.)

        min_exp, max_exp = self.getExposureRange()
        self.minExposure = min_exp
        self.maxExposure = max_exp
        self.exposure = 60.
        self.info = dict()
        self.info['pixel_size'] = 2.2
        self.info['max_width'] = 2560
        self.info['max_height'] = 1920

        #self.exposure = self.set_exposure(starting_exposure)
        print('  >> Done camera initialization')

    def set_exposure(self, exposure, adjustPixelclock = True):
        """
        Set camera exposure setting to desired value. Note: pixel clock 
        must be adjusted in order to use the whole range optimally.

        :param exposure: Exposure time in milliseconds

        Returns the actual exposure time of the camera
        """

        self.exposure = self.cam.set_exposure_time(exposure, adjustPixelclock)

        return self.exposure

    def set_pixel_clock(self, pixelclock):

        self.cam.set_pixel_clock(pixelclock)

    def set_auto_exposure(self,setting):
        """ 
        Turn camera auto exposure setting on/off 
        """
        self.continuous_capture = False
        self.auto_exposure = setting


    def set_color_mode(self,color_mode):
        """
        Set the colour mode the camera uses to capture images
        """
        self.color_mode = color_mode


    def get_raw_img(self,labjack = None):
        """
        Capture an image and return an array containing the data in raw 12-bit Bayer format
        :param labjack: optional , is required to do 

        """
        
        if labjack is None:
            data = self.cam.capture_single_frame()
        else:
            precap = np.mean(labjack.getNPhotoDiodeReads(5))
            data = self.cam.capture_single_frame()
            postcap = np.mean(labjack.getNPhotoDiodeReads(5))

            labjack.PowerOutcoupledMean = (precap + postcap)/2.
        data = np.flipud(data)
        retval = True

        return data, retval

    def save_img(self, name, img, metadata = None):
        """
        to be deleted
        Save a given image as a .tiff
        """
        # try:

        image_name = name + '.tiff'

        while os.path.isfile(image_name):
            try:
                imgNo = int(name.split('_')[-1])
                name = name.replace(str(imgNo),str(imgNo+1))
                imgNo += 1
                image_name = name + '.tiff'
                
            except:
                imgNo = 1
                name = name.replace(str(imgNo),str(imgNo+1))
                imgNo += 1
                image_name = name + '.tiff'

        img = np.array(img, dtype='uint16')


        img = Image.fromarray(img,'I;16')

        if metadata != None:
            img.save(image_name, 'TIFF', description = str(metadata))

        else:
            img.save(image_name,'TIFF')

        changePermissionsPath(image_name)

        return True

        # except:
        #     # could not save file
        #     return False


    def crop_and_save_img(self,name,img):
        # to be deleted

            img_dim = img.shape

            index = 120./self.specifications.actual_pixelsize

            cropped_image = img[int(img_dim[0]/2 - index/2):int(img_dim[0]/2 + index/2),int(img_dim[1]/2 - index/2):int(img_dim[1]/2 + index/2)]

            image_name = name
            
            return self.save_img(image_name,cropped_image)


    def hard_reset(self):
        num_ports = 2

        # power cycle USB ports
        for i in range(num_ports):
            # disable ASLR
            os.system('echo 0 | sudo tee /sys/bus/usb/devices/usb' + str(i+1) + '/power/autosuspend')


    def getExposureRange(self, pixelclock = None):
        """
        Get exposure bandwidth the given pixel clock
        """
        # save exposure time
        

        return self.cam.get_exposure_range_cam()