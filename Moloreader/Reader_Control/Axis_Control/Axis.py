import Reader_Control.Axis_Control.Serial_communication as tmcl
import pigpio
import serial
import time
import numpy as np

# AF: 14.12.2017 this class looks general enough I think I do not need to change it anymore

# This is a good design pattern, these abstract base class functions define the interface that is exposed to other classes
# everything else is internal.

class Axis:
    def __init__(self,model,port_number):
        raise NotImplementedError("Subclass must implement abstract method")
    def calibrate():
        raise NotImplementedError("Subclass must implement abstract method")
    def move_by(distance):
        raise NotImplementedError("Subclass must implement abstract method")
    def move_to(position):
        raise NotImplementedError("Subclass must implement abstract method")
    def curr_pos():
        raise NotImplementedError("Subclass must implement abstract method")

class Faulhaber(Axis):

    """ Initialize axis and maximum model parameters """
    def __init__(self,
            model,
            port_number,
            motor_number,
            start_switch_pin,
            limit_switch_pin,
            mm_per_revolution,
            starting_direction,
            pi,
            reference_position,
            range_mm):

        self.port_number = port_number              # e.g. ACM0, ACM1, ...
        self.motor_number = motor_number            # e.g. motor 0, motor 1, ...
        self.max_position = 0                       # position of stop switch
        self.start_switch_pin = start_switch_pin    # corresponding pin on the RPi
        self.limit_switch_pin = limit_switch_pin
        self.mm_per_revolution = mm_per_revolution
        self.starting_direction = starting_direction
        self.pi = pi
        self.reference_position = reference_position
        self.range_mm = range_mm
        self.axis_type = 'distance'    # define axis type

        self.ser = serial.Serial(
            port = '/dev/ttyACM'+str(port_number),
            baudrate = 9600,
            parity = serial.PARITY_NONE,
            stopbits= serial.STOPBITS_ONE,
            bytesize = 8
            )

        if  (model=='AM-1524-2R-A-0.25-12.5-55'):
            self.steps_per_revolution = 384
            self.microstep_resolution = 4               # set to max (256 microsteps per step = 8, 16 microsteps per step = 4) Please also change it in the TMCL code below.
        
            self.params = """
SAP, 140, 0, 4;
SAP, 154, 0,1;
SAP, 153, 0, 7;
SAP, 179, 0,0;
SAP,6,0,64;
SAP,7,0,24;
SAP,4,0,12;
SAP, 5, 0, 500
"""
# Parameters Louis with 256 steps
# self.params = """
# SAP, 140, 0, 8;
# SAP, 154, 0,1;
# SAP, 153, 0, 7;
# SAP, 179, 0,0;
# SAP,6,0,64;
# SAP,7,0,8;
# SAP,4,0,500;
# SAP, 5, 0, 500
# """
#SAP 140, 0, 8 // usrs = 8 (0=full step, 8=256 micro steps) 
#SAP 154, 0, 1 // pulse_div = 8 (0...15) 
#SAP 153, 0, 7 // ramp_div = 10 (0...15)
#SAP 179, 0,0 // Vsense = 0 --> I_RMS = <value>*1A/255
#SAP 6, 0, 110 // I_max = 430 mA (0...1500 mA) I_RMS = <value>*1A/255, AM-2224 maximum current is 0.5 AMPs --> maximum value is 127
#SAP 7, 0, 008 // I_standby = 117 mA (0...1500 mA) I_RMS = <value>*1A/255
#SAP 4, 0, 1677 // v_max = 2000 (0...2047) 
#SAP 5, 0, 500 // a_max = 2000 (0...2047) 


        elif(model=='AM-2224-R3-AV-4.8-85'):
            self.steps_per_revolution = 384 # 384 for 16 microsteps
            self.microstep_resolution = 4              # set to max (256 microsteps per step = 8, 16 microsteps per step = 4) Please also change it in the TMCL code below.
            self.params = """
SAP, 140, 0, 4;
SAP, 154, 0,1;
SAP, 153, 0, 7;
SAP, 179, 0,0;
SAP,6,0,127;
SAP,7,0,24;
SAP,4,0,12;
SAP, 5, 0, 500
""" 
    # set the parameters when the class is constructed.
        self.set_params()

    """ Set all parameters at once """
    def set_params(self):
        tmcl.SendCommandBlock(self.ser,self.params)

    def stop(self):
        tmcl_command = 'MST,0,' + str(self.motor_number) + ',0'
        reply = tmcl.SendCommand(self.ser,tmcl_command)
        return reply[0]


    def set_position(self,pos,unit):
        # store absolute position
        try:
            self.reference_position -= self.convert_pos_to_steps(self.get_parameter(1), 'mm')
        except:
            self.reference_position = self.convert_pos_to_steps(self.get_parameter(1), 'mm')

        pos = self.convert_pos_to_steps(pos,unit)

        self.set_parameter(1,pos)
        self.set_parameter(0,pos)


    """ Set axis parameter to given value """
    def set_parameter(self,parameter,value):

        # Send an SAP command
        tmcl_command = 'SAP,' + str(parameter) + ',' + str(self.motor_number) + ',' + str(value)
        reply = tmcl.SendCommand(self.ser,tmcl_command)

        # Return if was valid
        return reply[0]

    """ Return axis parameter by number """
    def get_parameter(self, parameter):

        # Send command
        tmcl_command = 'GAP,' + str(parameter) + ',' + str(self.motor_number) + ',0'
        reply = tmcl.SendCommand(self.ser,tmcl_command)

        # Check if valid, then read value
        return reply[1]


    def move(self,qty,unit):
        """
        Moves motor by a certain distance qty.
        """
        if   unit == 'mm':
            self.move_mm(qty,True)
        elif unit == 'um':
            self.move_um(qty,True)
        elif unit == 'steps':
            self.move_steps_safe(qty)


    def pos(self, unit='mm'):
        """
        Read current position using Get Axis Parameters
        """

        # Read current position using Get Axis Parameters
        pos = self.get_parameter(1)

        if   unit == 'mm':
            return pos*self.mm_per_revolution/self.steps_per_revolution
        elif unit == 'um':
            return pos*self.mm_per_revolution/self.steps_per_revolution*1000
        elif unit == 'steps':
            return int(pos)

    def convert_pos(self,pos,unit):
        """converts the position in steps to the unit specified"""

        if   unit == 'mm':
            return(pos*self.mm_per_revolution/self.steps_per_revolution)
        elif unit == 'um':
            return(pos*self.mm_per_revolution/self.steps_per_revolution*1000)
        elif unit == 'steps':
            return(pos)
        else:
            return

    def convert_pos_to_steps(self,pos,unit):

        if   unit == 'mm':
            return(int(pos/self.mm_per_revolution*self.steps_per_revolution))
        elif unit == 'um':
            return(int(pos/self.mm_per_revolution*self.steps_per_revolution/1000))
        elif unit == 'steps':
            return(int(pos))

    def move_steps_safe(self,steps):
        """
        Makes small steps and 
        """
        if steps < 0:
            sign = -1
        else:
            sign = 1

        safety = self.steps_per_revolution
        steps_done = 0

        while (abs(steps) - steps_done) > safety:

            self.move_steps_fast(sign*safety)
            steps_done += abs(safety)

        self.move_steps_fast(sign*(abs(steps) - steps_done))
        return 0

    """ Move 'steps' steps all at once """
    def move_steps_fast(self,steps):

        steps = int(steps)

        pos = self.pos(unit='steps')

        # first go 1 step in order to go the right direction (bug)
        tmcl_command = 'MVP,1,' + str(self.motor_number) + ',' + str(np.sign(steps))
        tmcl.SendCommand(self.ser,tmcl_command)
        time.sleep(0.01)

        if pos == self.pos(unit='steps')-np.sign(steps):
            # right direction
            tmcl_command = 'MVP,1,' + str(self.motor_number) + ',' + str(steps-np.sign(steps))

        else:
            # wrong direction
            tmcl_command = 'MVP,1,' + str(self.motor_number) + ',' + str(steps+np.sign(steps))

        tmcl.SendCommand(self.ser,tmcl_command)

        while self.is_done()==False:
            continue

        return 0

    """ Move a certain distance in mm """
    def move_mm(self, distance, check_for_switches = True):

        revolutions = distance / self.mm_per_revolution
        steps = revolutions * self.steps_per_revolution
        steps = int(steps)

        if check_for_switches == True:
            return self.move_steps_safe(steps)
        else:
            return self.move_steps_fast(steps)


    def increment(self):
        self.move_mm(0.5)
        return 0

    def decrement(self):
        self.move_mm(-0.5)
        return 0

    """ Move a certain distance in um """
    def move_um(self,distance,check_for_switches = False):

        distance = distance / 1000
        return self.move_mm(distance,check_for_switches)
        

    def valid_position(self):
        """
        Return whether current position is whithin bounds and both switches are off

        """
        position = self.get_parameter(1)

        if   (self.get_limit_switch() != 0) or (self.get_start_switch() != 0):
            return -1
        elif (position > 2**23) or (position < -2**23):
            print(position)
            print(position >  2**23)
            print(position < -2**23)
            return 1
        else:
            return 0 # no error

    """ Indicates if the module reached its destination (i.e. if the current movement is finished) """
    def is_done(self):
        # Get 'reached target position'
        response = self.get_parameter(8)
        return (response == 1)


    """ Return whether start switch is on/off """
    def get_start_switch(self):
        value = (self.pi.read(self.start_switch_pin) == 1)

        # Add a 0.1s buffer for debouncing
        if(value == True):
            time.sleep(0.1) 
            return (self.pi.read(self.start_switch_pin) == 1)
        return False


    """ Return whether limit switch is on/off """
    def get_limit_switch(self):
        value = (self.pi.read(self.limit_switch_pin) == 1)
        
        # Add a 0.1s buffer for debouncing
        if(value == True):
            time.sleep(0.1)
            return (self.pi.read(self.limit_switch_pin) == 1)
        return False


    def find_start_switch(self,direction,state,steps):
        while(self.get_start_switch() == state):
            self.move_steps_fast(direction*steps)
            time.sleep(0.001)


    def goToSwitch(self):
        print('Finding switch...')

        # Start in negative direction at 1 revolution
        steps = self.steps_per_revolution
        steps = int(steps)
        direction = self.starting_direction
        state = False

        # divide distance by 2 every time
        while steps > 3:
            self.find_start_switch(direction,state,steps)
            steps  *= 0.5
            direction *= -1
            state = not state
            print('move ' + str(steps) + ' steps')



    def calib(self, goToReference=True):

        self.goToSwitch()

        if goToReference:
            print('Found switch. Go to reference ' + str(self.reference_position))

            # Move away from switch
            self.move_mm(self.reference_position, False)

            # Set reference position to 0
            self.set_parameter(1,0)
            self.set_parameter(0,0)

            print('Done calibration')


    def convertUnits(self, oldUnit, newUnit):
        if oldUnit == 'steps':
            return self.convertSteps(1, newUnit)

        if newUnit == 'steps':
            return self.returnSteps(1, oldUnit)

        if oldUnit == 'um' and newUnit == 'mm':
            return 1/1000

        if oldUnit == 'mm' and newUnit == 'um':
            return 1000

        return 1

    def getSmallestValue(self, unit='um'):
        return self.convertSteps(1, unit) # convert 1 step


    def returnSteps(self, value, unit):
        """
        Returns the amount of steps required to move a certain distance 
        with the given unit.
        """
        if unit == 'um':
            value /= 1000.

            revolutions = value / self.mm_per_revolution
            steps = int(revolutions * self.steps_per_revolution)

        elif unit == 'mm':
            revolutions = value / self.mm_per_revolution
            steps = int(revolutions * self.steps_per_revolution)

        elif unit == 'steps':
            steps = int(value)

        else:
            print('unknown units')
            return

        return int(steps)


    def convertSteps(self, steps, unit):
        """
        Converts a certain amount of steps to a distance or an 
        angle, respectively.
        """

        if unit == 'mm':
            return steps*self.mm_per_revolution/self.steps_per_revolution

        elif unit == 'um':
            return steps*self.mm_per_revolution/self.steps_per_revolution*1000

        elif unit == 'steps':
            return int(steps)

        else:
            print('unknown unit: {}'.format(unit))
            return


class BergerLahr(Axis):

    """ Constructor """
    def __init__(self,
            RPplus,
            Rminus,
            Pminus,
            ref_switch_pin,
            starting_direction,
            starting_state,
            reference_position,
            range_deg,
            pi):

        self.RPplus = RPplus
        self.Rminus = Rminus
        self.Pminus = Pminus
        self.start_switch_pin = ref_switch_pin
        self.steps_per_revolution = 20000
        self.position = 0
        self.starting_direction = starting_direction
        self.starting_state = starting_state
        self.reference_position = reference_position
        self.range_deg = range_deg
        self.pi = pi
        self.axis_type = 'angle'    # define axis type

    def stop(self):
        return False

    def move_steps_safe(self, steps):
        self.move_steps_fast(steps)
        return 0

    """ Move N steps at a time """
    def move_steps_fast(self, steps):

        # Rightung -  and Pulse - always set to high
        self.pi.write(self.RPplus,1)

        # Set direction
        if steps > 0:
            self.pi.write(self.Rminus,1)
        else:
            self.pi.write(self.Rminus,0)

        # generate pulse
        for i in range(abs(steps)):
            self.pi.write(self.Pminus,1)
            time.sleep(0.001)
            self.pi.write(self.Pminus,0)
            time.sleep(0.001)

        self.position += steps
        return 0

    
    def get_start_switch(self):
        """ gets the state of the reference switch True is on, False is off."""
        return (self.pi.read(self.start_switch_pin) == 1)

    def move_degrees(self,degrees):
        # Determine number of steps
        steps = degrees / 360 * self.steps_per_revolution
        steps = int(steps)

        self.move_steps_fast(steps)
        return 0

    def increment(self):
        self.move_degrees(1)

    def decrement(self):
        self.move_degrees(-1)

    def convert_pos(self,pos,units):        
        if   units == 'steps':
           return pos
        elif units == 'deg': 
           return pos / self.steps_per_revolution * 360


    def pos(self, units='deg'):
        if   units == 'steps':
           return int(self.position)
        elif units == 'deg': 
           return self.position / self.steps_per_revolution * 360

    def find_start_switch(self,direction,state,steps):
        while(self.get_start_switch() == state):
            self.move_steps_fast(direction*steps)
            time.sleep(0.001)



    def goToSwitch(self):
        print('Finding switch...')

        # Start in negative direction at 1 revolution
        steps = self.steps_per_revolution/360
        steps = int(steps)
        direction = self.starting_direction
        state = self.starting_state

        # divide distance by 2 every time
        while steps > 2:
            self.find_start_switch(direction,state,steps)
            steps  *= 0.5
            steps   = int(steps)
            direction *= -1
            state = not state
            print(steps)


    def calib(self, goToReference=True):

        self.goToSwitch()

        if goToReference:
            print('Found switch. Go to reference ' + str(self.reference_position))
            self.move_degrees(self.reference_position)

            # Set reference position to 0
            self.position = 0


            print('Done calibration')

    def set_refposition(self):

        self.position = 0 


    def getSmallestValue(self, unit='deg'):
        return self.convertSteps(1, unit) # convert 1 step


    def returnSteps(self, value, unit):
        """
        Returns the amount of steps required to move a certain distance 
        with the given unit.
        """

        if unit == 'deg':
            steps = int(value / 360 * self.steps_per_revolution)

        elif unit == 'steps':
            steps = int(value)

        else:
            print('unknown units')
            return

        return int(steps)


    def convertSteps(self, steps, unit):
        """
        Converts a certain amount of steps to a distance or an 
        angle, respectively.
        """

        if unit == 'deg':
            return steps * 360 / self.steps_per_revolution

        elif unit == 'steps':
            return int(steps)

        else:
            return


    def convertUnits(self, oldUnit, newUnit):

        if oldUnit == 'steps':
            return self.convertSteps(1, newUnit)

        if newUnit == 'steps':
            return self.returnSteps(1, oldUnit)

        if oldUnit == 'um' and newUnit == 'mm':
            return 1000

        if oldUnit == 'mm' and newUnit == 'um':
            return 1/1000

        return 1


        