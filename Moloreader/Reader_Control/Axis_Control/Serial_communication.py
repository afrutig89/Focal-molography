import serial
import Reader_Control.Axis_Control.TMCL_command_wrapper as cmd
import struct

""" Sends a TMCL command over serial port"""

# Return two's complement of a number if it is negative
def twos_comp(input):

    # Only calculate 2's complement if number is negative, else return input
    if(input<0):

        # Set string format to 32 bits with no '0b'
        input = format(-input,'032b')
        input = list(input)

        i = len(input)-1

        # Start right and replicate all zeros
        while (i >= 0) and (input[i] == '0'):
            i -= 1
        
        # Also replicate first one
        i-=1

        # Now invert remaining characters
        while(i>=0):
            if(input[i]=='1'):
                input[i] ='0'
            else:
                input[i]='1'
            i -= 1

        # Convert back to integer
        input = ''.join(input)
        input = int(input,2)

    return input

def SendCommand(ser,Command_line):
    """ Sends a command over serial port.
    
        :params ser: Serial port object
        :params Command_line: A command line that should be sent over the serial port

    """

    [Command,Type,Motor,Value] = str.split(Command_line,',')
    
    Command = cmd.commands[Command]
    Type = int(Type)
    Motor = int(Motor)
    Value = int(Value)

    # get rid of negative numbers
    Value = twos_comp(Value)

    # flush anything in the Serial Buffer
    ser.flushInput()
    
    Txbuffer = []

    Txbuffer.append(0)
    Txbuffer.append(Command)
    Txbuffer.append(Type)
    Txbuffer.append(Motor)
    Txbuffer.append((Value >> 24) & 0xff)
    Txbuffer.append((Value >> 16) & 0xff)
    Txbuffer.append((Value >>  8) & 0xff)
    Txbuffer.append(Value & 0xff)
    Txbuffer.append(0)

    for i in range(0,len(Txbuffer)-1):
        Txbuffer[-1] += Txbuffer[i]
    Txbuffer[-1] &= 0xff

    ser.write(bytearray(Txbuffer))
    ser.flush()

    # wait for the motor to communicate
    length = 0

    while length == 0:
        length = ser.inWaiting()

    Rxbuffer = []
    for i in range(0,length):
        Rxbuffer += ser.read()

    # byte4 through byte7 is the response if the command was retrieving information (e.g. GAP)
    operand = struct.pack('4B',Rxbuffer[4],Rxbuffer[5],Rxbuffer[6],Rxbuffer[7])
    operand = struct.unpack('>i',operand)
    operand = operand[0]

    # the third byte is the status byte, if it has the value 100, then communication
    # was successful and the motor controller returned no error
    if Rxbuffer[2] == 100:
        return (True,operand)
    else:
        return (False,operand)

def SendCommandBlock(ser,Command_block):
    """A really useful function.

    :param etype: exception type
    :param value: exception value
    :param tb: traceback object

    :param limit: maximum number of stack frames to show
    :type limit: integer or None
    
    :rtype: list of strings"""

    Command_block = Command_block.replace('\n','')
    
    
    Commandlines = str.split(Command_block,';')

    Exec_Ok = []
    
    for i in range(0,len(Commandlines)):

        Exec_Ok.append(SendCommand(ser,Commandlines[i]))

    return all(Exec_Ok)

