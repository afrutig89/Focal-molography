#Opcodes of all TMCL commands that can be used in direct mode

commands = {
	
	# Motion Commands
	#-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	'ROR':1, # Rotate right
	'ROL':2, # Rotate left
	'MVP':4, # Move to position
	'MST':3, # Motor stop
	'RFS':13, # Reference Search
	'SCO':30, # Store coordinate
	'GCO':31, # Capture coordinate
	'CCO':32, # Get coordinate
 
	# Parameter commands 
	#-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	'SAP':5, # Set Axis Parameters
	'GAP':6, # Get Axis Parameters
	'STAP':7, # Store Axis Parameters into EEPROM
	'RSAP':8, # Restore Axis Parameters from EEPROM
	'SGP':9, # Set Global Parameters
	'GGP':10, # Get Global Parameters
	'STGP':11, # Store Global Axis Parameters into EEPROM 
	'RSGP':12, #  Restore Global Axis Parameters into EEPROM

	# I/O port commands
	#-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	'SIO':14,
	'GIO':15

}


