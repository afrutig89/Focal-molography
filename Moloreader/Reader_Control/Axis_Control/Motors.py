import Reader_Control.Axis_Control.Axis as axis
import pigpio
import paramiko
import time
import json
import subprocess

""" Class that holds all three axis 
    Should initialize once at the beginning of every script
"""

class Motors:

    def fake__init__(self,port_numbers):

        self.init_motors(port_numbers)

    def __init__(self,port_numbers):

        # launch pigpio daemon through SSH
        #ip_addresses = ['10.42.0.85','10.42.1.85','10.42.2.85','10.42.0.1']
        ip_addresses = ['10.42.0.100','10.42.1.100','10.42.2.100','10.42.0.1']

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # try both IP-adresses
        for ip_address in ip_addresses:
            try:
                print('  >> connecting to pi@'+ip_address)
                ssh.connect(hostname=ip_address, username='pi', password='raspberry', timeout=10)
                break
            except:
                continue

        print('  >> starting the pigpio daemon')
        cmd = 'sudo pigpiod'
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd)
        time.sleep(1)

        # set up pigpio connection and GPIO pins
        print('  >> setting up remote GPIO connection')
        self.pi = pigpio.pi(ip_address)
        time.sleep(1)

        # set up GPIO pins
        for pin in (4,17,23,24,25,27):
            self.pi.set_mode(pin, pigpio.OUTPUT)  # pin as output
        for pin in (6,12,13,16,19,20,21,26):
            self.pi.set_mode(pin, pigpio.INPUT)  # pin as input, pull down
            self.pi.set_pull_up_down(pin, pigpio.PUD_DOWN)

        # initialize serial connection to motors
        try:
            print('  >> connecting to /dev/ttyACM' + port_numbers[0] + ' to /dev/ttyACM' + port_numbers[2])
            self.init_motors(port_numbers)
        except Exception:
            raise ValueError('Unable to connect to Faulhaber module')

    def fake_init_motors(self,port_numbers):

        self.x  = None
        self.y  = None
        self.z  = None
        self.TE = None
        self.MS = None

    def init_motors(self,port_numbers):

        #in mm
        try:
            print('  >> Current directory: ' + subprocess.check_output('pwd').decode("utf-8"))
            reference_positions = self.read_starting_positions()
            print('  >> Reference position: ' + str(reference_positions))
            
            x_lower_edge_chip = reference_positions['X']
            y_left_side_chip  = reference_positions['Y']
            z_surface         = reference_positions['Z']
            TE_starting_angle = reference_positions['TE']
            MS_starting_angle = reference_positions['MS']

        except Exception:
            raise ValueError('Unable to read starting positions')

        # setting up axes
        print('  >> x-axis setup')
        self.x = axis.Faulhaber(
                model = 'AM-1524-2R-A-0.25-12.5-55', 
                port_number = port_numbers[0],
                motor_number = '0',
                start_switch_pin = 26,
                limit_switch_pin = 19,
                mm_per_revolution = 0.2,
                starting_direction = -1,
                pi = self.pi,
                reference_position = y_left_side_chip,
                range_mm = 27.5,
            )

        print('  >> y-axis setup')
        self.y = axis.Faulhaber(
                model ='AM-1524-2R-A-0.25-12.5-55',
                port_number = port_numbers[2],
                motor_number = '0',
                start_switch_pin = 20,
                limit_switch_pin = 21,
                mm_per_revolution = 0.5,
                starting_direction = -1,
                pi = self.pi,
                reference_position = x_lower_edge_chip,
                range_mm = 61.4,
            )

        print('  >> z-axis setup')
        self.z = axis.Faulhaber(
                model = 'AM-2224-R3-AV-4.8-85',
                port_number = port_numbers[1],
                motor_number = '0',
                start_switch_pin = 16,
                limit_switch_pin = 12,
                mm_per_revolution = 0.5,
                starting_direction = 1,
                pi = self.pi,
                reference_position = z_surface,
                range_mm = 36.5,
            )

        print('  >> TE axis setup')
        self.TE = axis.BergerLahr(
                RPplus = 27,
                Rminus = 4,
                Pminus = 17,
                ref_switch_pin = 6,
                starting_direction = -1,
                starting_state = True,
                reference_position = TE_starting_angle,
                range_deg = 94.5,
                pi = self.pi
            )

        print('  >> MS axis setup')
        self.MS = axis.BergerLahr(
                RPplus = 25,
                Rminus = 23,
                Pminus = 24,
                ref_switch_pin = 13,
                starting_direction = 1,
                starting_state = True,
                reference_position = MS_starting_angle,
                range_deg = 28.5,
                pi = self.pi
            )

        
    def read_starting_positions(self):
        """Positions of x,y,z are in mm, whereas TE and MS are in deg from the switch in counter clockwise direction. """
        file = open('starting_positions.txt','r')
        positions = json.load(file)
        file.close()

        return positions

    def pos(self,units):
        pos  = str(self.x.pos(units))
        pos += ','
        pos += str(self.y.pos(units))
        pos += ','
        pos += str(self.z.pos(units))
        return pos

    def calib(self, axis='XYZTEMS', goToReference=True):
        # reload reference (if changed)
        reference_positions = self.read_starting_positions()
        print('  >> Reference position: ' + str(reference_positions))


        if axis != 'Z':

            if 'X' in axis or 'Y' in axis or 'MS' in axis:
                # move z-axis in order to not smash something
                self.z.move_mm(15)

                if 'X' in axis:
                    print('>> Calibrating x-axis')
                    self.x.reference_position = reference_positions['X']
                    self.x.calib(goToReference)

                if 'Y' in axis:
                    print('>> Calibrating y-axis')
                    self.y.reference_position = reference_positions['Y']
                    self.y.calib(goToReference)

                if 'MS' in  axis:
                    print('>> Calibrating MS-axis')
                    self.MS.reference_position = reference_positions['MS']
                    self.MS.calib(goToReference)

                if 'Z' not in axis and goToReference:
                    self.z.move_mm(-15)
                

            if 'TE' in axis:
                print('>> Calibrating TE-axis')
                self.TE.reference_position = reference_positions['TE']
                self.TE.calib(goToReference)

        if 'Z' in axis:
            print('>> Calibrating z-axis')
            self.z.reference_position = reference_positions['Z']
            self.z.calib(goToReference)


    def move_to_position(self,position,units):
        x, y, z = position.split(',')
        unit_x, unit_y, unit_z = units.split(',')

        # print(position + units)

        x = float(x) - self.x.pos(unit_x)
        y = float(y) - self.y.pos(unit_y)
        z = float(z) - self.z.pos(unit_z)

        # print(str(self.x.pos(unit_x))+','+str(self.y.pos(unit_y))+','+str(self.z.pos(unit_z)))

        print('move_to_position: ' + str(x)+','+str(y)+','+str(z))
        # print(str(unit_x)+','+str(unit_y)+','+str(unit_z))

        self.x.move(x,unit_x)
        self.y.move(y,unit_y)
        self.z.move(z,unit_z)



