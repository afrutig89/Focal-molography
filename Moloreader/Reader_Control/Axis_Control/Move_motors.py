from PyQt4 import QtGui,QtCore, QtSql

class Motor_movement_thread(QtCore.QThread):
	"""
	This is a thread to move one axis.
	"""

	def __init__(self):

		# Call parent class constructor
		super(Motor_movement_thread,self).__init__()

		self.exiting = False


	def move(self, axis, distance):
		"""
		Worker thread to move motors using the right function for each unit
		"""

		# Must pass parameters to the "run" thread this way
		self.axis	 = axis
		self.distance = distance

		# Start "run" thread
		self.start()


	def run(self):
		retval = self.axis.move_steps_safe(self.distance)

		self.emit(QtCore.SIGNAL("done_moving(int)"),retval)


	def __del__(self):
		self.exiting = True 
		self.wait()



class Move_to_mologram_thread(QtCore.QThread):

	def __init__(self, chip, motors):

		# Call parent class constructor
		super(Move_to_mologram_thread,self).__init__()

		# assign connection to motors
		self.chip = chip
		self.motors = motors

		self.exiting = False


	def move_to(self, position):
		"""
		Moves motors to a certain position. The variable position can be a string (e.g. 'B,3,1')
		or a tuple of len 2 (only x,y) or 3 (x,y and z).
		"""

		if type(position) == str:
			# if position is given as mologram number (e.g. "B,3,1")
			# get x,y,z coordinates of new location
			x, y = self.chip.cartesian_position(position)
			# z-axis not adjusted
			z = self.motors.z.pos('mm')

		else:
			if len(position) == 3:
				x, y, z = position
			elif len(position) == 2:
				# z not defined: no movement in z-direction
				x, y = position
				z = self.motors.z.pos('mm')
			else:
				# wrong format of position
				return

		# go to new position
		self.delta_x = x - self.motors.x.pos('mm')
		self.delta_y = y - self.motors.y.pos('mm')
		self.delta_z = z - self.motors.z.pos('mm')

		print('move {},{},{}'.format(self.delta_x,self.delta_y,self.delta_z))

		self.start()



	def run(self):

		# execute movement
		self.motors.x.move_mm(self.delta_x)
		self.motors.y.move_mm(self.delta_y)
		self.motors.z.move_mm(self.delta_z)

		# let GUI know that movement is finished
		self.emit(QtCore.SIGNAL("done_moving"))


	def __del__(self):
		self.exiting = True 
		self.wait()


class Calibration(QtCore.QThread):
	"""
	todo
	"""

	def __init__(self, chip, motors):

		# Call parent class constructor
		super(Move_to_mologram_thread,self).__init__()

		# assign connection to motors
		self.chip = chip
		self.motors = motors

		self.exiting = False


	def run(self):

		# execute movement
		self.move_to(self.position)



	def __del__(self):
		self.exiting = True 
		self.wait()