from Reader_Control.Axis_Control.Motors import Motors
from Reader_Control.Peripheral_Communication.autosampler_arduino_communication import AutoSampler
from settings import Chip
import time
from serial import Serial
import os
import numpy as np
import subprocess
import re

# this should be a class Moloreader


def bash_output(cmd):
    """ 
    Get the output form a unix bash command 
    :params:
    """

    out = subprocess.check_output(cmd).decode("utf-8")
    out = np.array(out.split('\n'))
    return out

""" Get a specific attribute of a given tty port """
def usb_attribute(port_number,attribute):

    cmd  = ["udevadm", "info", "-a", "-n", "/dev/" + port_number]    
    
    info = str.join('',bash_output(cmd))
    info = re.findall(r'ATTRS\{' + attribute + '\}=="(.*?)"',info)
    
    return info[0]

""" Get list of ttyACM ports currently connected to computer """
def get_ttyACM_ports():

    # filter all the devices in '/dev' containing 'ttyACM' in their names
    os.chdir('/dev/')
    ports = np.array([s for s in bash_output('ls') if 'ttyACM' in s])

    # return to working directory
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)

    return ports

""" Return a serial object connected to the serial port of the Arduino """
def arduino_connection():

    ports = get_ttyACM_ports()

    for port in ports:
        if usb_attribute(port,'product') == 'Arduino Uno':
            arduino_port = port

    ser = Serial('/dev/'+arduino_port,9600)
    return ser

def faulhaber_port_numbers():
    ports = get_ttyACM_ports()

    faulhaber_ports = []

    for port in ports:
        if usb_attribute(port,'product') == 'MCST3601  (virtual COM) ':
            port_number = re.findall(r'ttyACM(.+$)',port)[0]
            faulhaber_ports.append(port_number)

    return np.sort(faulhaber_ports)

class Setup:

    def __init__(self, main_widget):

        # set working directory
        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(abspath)
        os.chdir(dname)
        print('Current directory: ' + subprocess.check_output('pwd').decode("utf-8"))

        if not main_widget.offline_mode:
            # import ids module
            from Reader_Control.Camera_Control.Camera import Camera
            import ids
            
            try:
                # setting up motors
                print('> Initializing motors')
                ports = faulhaber_port_numbers()
                self.motors = Motors(ports)

            except:
                print('  >> could not connect to motors. Set offline mode.')
                main_widget.offline_mode = True

            try:
                # seting up camera
                print('> Initializing camera')
                self.camera =  Camera(
                        starting_exposure = 60,
                        color_mode = ids.ids_core.COLOR_BAYER_16,
                        magnification = 20
                    )
            except:
                print('  >> could not connect to camera. Set offline mode.')
                main_widget.offline_mode = True

            try:
                print('> Connect autosampler arduino communication')
                self.autosampler = AutoSampler('Autosampler',0)

            except:
                print('  >> could not connect to autosampler.')
                pass



        # # setting up arduino communication
        # print('Initializing arduino connection')
        # self.arduino_conn = arduino_connection()

        self.cluster_size_threshold = 1000
        self.by_size = False
        self.nominal_laser_intensity = 41
        self.correct_for_laser = True

        print('> Initializing mologram position control')
        self.chip = Chip()

        print('System booted normally')        

    def photodiode_intensity(self):
        val = self.arduino_conn.read()
        val = ord(val)

        return val


    def max_pixel(self):
        num_pixels = 50
        buffer_length = 5
        values = np.empty(buffer_length)
        
        for i in range(buffer_length):  
            img,retval = self.camera.get_raw_img()
            values[i] = np.mean(heapq.nlargest(num_pixels, img.flatten()))
            #values[i] = np.max(img.flatten())
            
        value = np.mean(values)
        
        return value



def get_hostname_by_ssh(host_ip, username='user', password='password'):
    '''
    Make a connection to the remote system and get its hostname
    '''
    print('Checking out {}'.format(host_ip))
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host_ip, username=username, password=password)
    _, stdout, _ = ssh.exec_command('hostname')
    hostname = stdout.read()
    ssh.close()
    if isinstance(hostname, bytes):
        hostname = hostname.decode('utf-8')
    print('Found {}'.format(hostname.strip()))
    return hostname.strip()