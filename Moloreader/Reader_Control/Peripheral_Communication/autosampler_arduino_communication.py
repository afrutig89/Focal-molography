import serial

class PheripheralDevice(object):
    """This is a class that handles external instruments that communicate with the moloreader."""
    

    def __init__(self, instrument_name, port_num):
        self.port_num = port_num
        self.instrument_name = instrument_name

        self.initializeComPort()


    def initializeComPort(self):
        self.ser = serial.Serial(
                port = '/dev/ttyACM'+str(self.port_num),
                baudrate = 9600,
                parity = serial.PARITY_NONE,
                stopbits= serial.STOPBITS_ONE,
                bytesize = 8 
            )

    def info(self):
        infoDict = {
            'instrument_name' : self.instrument_name,
            'port' : '/dev/ttyACM'+str(self.port_num),
            'baudrate' : 9600,
            'parity' : serial.PARITY_NONE,
            'stopbits' : serial.STOPBITS_ONE,
            'bytesize' : 8 
        }
        return infoDict


class AutoSampler(PheripheralDevice):

    """The Autosampler class can be used to control an autosampler over serial communication"""
    
    def __init__(self,instrument_name,port_num):
        super().__init__(instrument_name,port_num)
        
        self.number_of_injections = 0
        self.injection_times = []


    def injectionTrigger(self):

        """returns True if the Autosampler has made an injection, needs to be called in a loop with sufficient sampling frequency otherwise 
        events will be missed."""

        self.ser.flush()
        read_byte = True

        while read_byte:
            if self.ser.in_waiting:

                # injection value is an integer between 0 and 1023, 0 is the injection signal and 1023 stands for no injection. 

                # [:-2] is used because the arduino sends /r/n at the end of each line. 
                injection_value = self.ser.readline()[:-2]
                
                # necessary, because sometimes python does not get the right value from the stream, which then results in an error upon conversion to int. 
                try:
                    injection_value = int(injection_value)
                except:
                    continue

                if injection_value == 0:
                    return True
                else:
                    return False
                

if __name__ == "__main__":

    autosampler = AutoSampler('Autosampler',0)

    print(autosampler.port_num)

    for i in range(0,1000):
        print(autosampler.injectionTrigger())



