"""
:created:       13-Mai-2017
:author:        Roland Dreyfus
:revised:       13-Mai-2017 by Roland Dreyfus
:references:    
"""

from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))


import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image, ImageDraw
from scipy.ndimage.filters import gaussian_filter, median_filter
import scipy.signal as signal
import scipy.fftpack
from matplotlib import colors

import auxiliariesMoloreader.constants as constants
from Data_Processing.imageAnalysisBragg import ImageAnalysisBragg
from SharedLibraries.Database.dataBaseOperations import saveDB, loadDB, createDB, createSQLDict





class PostProcessingBindingCurves():
    """docstring for DataProcessingBraggCondition"""
    def __init__(self,database_path, images_path, curve, crop_range):
        self.database_path = database_path
        self.images_path = images_path
        self.curve = curve
        self.crop_range = crop_range
        

    def studyGaussFilter(self):

        sigmas = [0,1,3,7,20,50]

        fig = plt.figure()
        for sigma in sigmas:
            index_image, sig_mean_physic_array, bkg_mean_physic_array = self.getValuesFromImage(gauss_sigma=sigma)
            plt.plot(index_image,sig_mean_physic_array/np.max(sig_mean_physic_array), label=('sigma = '+str(sigma)))
        plt.xlabel('image #')
        plt.ylabel('normalized signal [-]')
        plt.legend()


        fig = plt.figure()
        for sigma in sigmas:
            index_image, sig_mean_physic_array, bkg_mean_physic_array = self.getValuesFromImage(gauss_sigma=sigma)
            plt.plot(index_image,sig_mean_physic_array, label=('sigma = '+str(sigma)))
        plt.xlabel('image #')
        plt.ylabel('signal [W/m2]')
        plt.legend()


    def studyAiryDiskSize(self):
        radii = [0.1,0.5,1,3,7,20]

        fig = plt.figure()
        for radius in radii:
            self.imageAnalysisBragg = ImageAnalysisBragg(airy_disk_radius = radius, lowSignalSearch=False)
            index_image, sig_mean_physic_array, bkg_mean_physic_array = self.getValuesFromImage(gauss_sigma=0)
            plt.plot(index_image,sig_mean_physic_array/np.max(sig_mean_physic_array), label=('radius = '+str(radius)))
        plt.xlabel('image #')
        plt.ylabel('normalized signal [-]')
        plt.legend()


        fig = plt.figure()
        for radius in radii:
            self.imageAnalysisBragg = ImageAnalysisBragg(airy_disk_radius = radius, lowSignalSearch=False)
            index_image, sig_mean_physic_array, bkg_mean_physic_array = self.getValuesFromImage(gauss_sigma=0)
            plt.plot(index_image,sig_mean_physic_array, label=('radius = '+str(radius)))

            self.plotOneImageMasked('B-3-8_exp-22.99-ms_229',radius)

        plt.xlabel('image #')
        plt.ylabel('signal [W/m2]')
        plt.legend()



    def studybutterWorthFiltering(self):

        # design of Buterworth filter
        N  = 2    # Filter order
        cutOffs = [0.001,0.01,0.1,0.3,0.7,1] # Cutoff frequency
 

        fig = plt.figure()
        for Wn in cutOffs:

            self.imageAnalysisBragg = ImageAnalysisBragg(airy_disk_radius = 3, lowSignalSearch=False)
            index_image, sig_mean_physic_array, bkg_mean_physic_array = self.getValuesFromImage(gauss_sigma=0)

            # building filter
            B, A = signal.butter(N, Wn, output='ba')
            
            # applying filter
            sig_mean_physic_array = signal.filtfilt(B,A, sig_mean_physic_array)

            plt.plot(index_image,sig_mean_physic_array, label=('cutoff = '+str(Wn)))

        plt.xlabel('image #')
        plt.ylabel('signal [W/m2]')
        plt.legend()


    def binLargestPixels(self):

        # N_largest_pixels = [1,10,50,200,500,1000]
        airy_disk_radius = [0.1, 1,10, 50, 100]
        N = 500
        fig = plt.figure()
        # for N in N_largest_pixels:
        for radius in airy_disk_radius:

            self.imageAnalysisBragg = ImageAnalysisBragg(airy_disk_radius = radius, lowSignalSearch=False)
            index_image, sig_mean_physic_array, bkg_mean_physic_array = self.getValuesFromImage(gauss_sigma=0, N_largest_pixels=N)

            plt.plot(index_image,sig_mean_physic_array, 
                    # label=('N largest = '+str(N))
                    label=('N largest = '+str(N),'airydisk radius = '+str(radius))
                    )

        plt.xlabel('image #')
        plt.ylabel('signal [W/m2]')
        plt.legend()




    def optimisedData(self, airy_disk_radius, gauss_sigma, filter_cutoff, N_largest_pixels):

        print('>> airy_disk_radius = '+str(airy_disk_radius)+'um')
        print('>> filter_cutoff = '+str(filter_cutoff))
        print('>> gauss_sigma = '+str(gauss_sigma))
        print('>> N_largest_pixels = '+str(N_largest_pixels))

        self.imageAnalysisBragg = ImageAnalysisBragg(airy_disk_radius, lowSignalSearch=False)
        index_image, sig_mean_physic_array, bkg_mean_physic_array, startTime_array = self.getValuesFromImage(
                                                                                        airy_disk_radius=airy_disk_radius, 
                                                                                        gauss_sigma=gauss_sigma, 
                                                                                        filter_cutoff=filter_cutoff, 
                                                                                        N_largest_pixels=N_largest_pixels
                                                                                        )
        # plt.plot(startTime_array,bkg_mean_physic_array/np.max(bkg_mean_physic_array),
        #          # label=('airydisk = '+str(airy_disk_radius)+'um'+'; filter_cutoff = '+str(filter_cutoff)+'; N largest = '+str(N_largest_pixels))
        #          linewidth=1
        #          )
        plt.plot(startTime_array,sig_mean_physic_array,
                 # label=('airydisk = '+str(airy_disk_radius)+'um'+'; filter_cutoff = '+str(filter_cutoff)+'; N largest = '+str(N_largest_pixels))
                 linewidth=1
                 )
        plt.xlabel('time [s]')
        plt.ylabel('signal [W/m2]')
        plt.legend()

        # Hide the right and top spines
        plt.gca().spines['right'].set_visible(False)
        plt.gca().spines['top'].set_visible(False)

        # impose ticks
        # tick_spacing = np.max(sig_mean_physic_array)
        # plt.gca().yaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))

        plt.savefig(''.join([self.images_path,'figure_background_mean','.png']),dpi=400,format='png')



    def getValuesFromImage(self, airy_disk_radius, gauss_sigma, filter_cutoff, N_largest_pixels):
        """
        Function extracts signal information from images and saves them in arrays.
        """
        self.imageAnalysisBragg = ImageAnalysisBragg(airy_disk_radius = airy_disk_radius, lowSignalSearch=False)
        curve = self.curve

        data_one_curve = self.DBtoListsOneCurve(curve)

        index_image = data_one_curve['index']
        exposureTime = data_one_curve['exposureTime']
        imagePath = data_one_curve['imagePath']
        startTime = data_one_curve['startTime']


        # crop data
        crop_range = self.crop_range
        index_image = self.cropData(data_to_crop=index_image, 
                                    crop_by_list=startTime,
                                    crop_range=crop_range)
        exposureTime = self.cropData(data_to_crop=exposureTime, 
                                    crop_by_list=startTime,
                                    crop_range=crop_range)
        imagePath = self.cropData(data_to_crop=imagePath, 
                                    crop_by_list=startTime,
                                    crop_range=crop_range)
        startTime = self.cropData(data_to_crop=startTime, 
                                    crop_by_list=startTime,
                                    crop_range=crop_range)
        # initialize data arrays
        sig_mean_array = []
        bkg_mean_array = []
        bkg_std_array = []
        SNR_array = []
        sig_mean_physic_array = []
        bkg_mean_physic_array = []
        exposureTime_array = []
        startTime_array = startTime

        # Iterate images
        print('>> processing images ...')
        for i in range(len(index_image)):
        # itarate images: load, get signals, save values in array; next image..
            # get image name from path
            imageName = str.split(imagePath[i].encode('ascii','ignore'),'/')[-1]
            image = self.loadImagesFromFolder(self.images_path, imageName)
            
            if gauss_sigma!=False:
                image = gaussian_filter(image, sigma= gauss_sigma)

            # get valuable information from image
            # mask pixels outside of ROI of peak -> keep peak only
            image_sig = self.imageAnalysisBragg.maskPeak(
                                image, 
                                airy_disk_radius
                                )

            # mask pixels outside of ROI of background -> keep background
            image_bckgrnd = self.imageAnalysisBragg.maskBackgroundROI(
                                        image,
                                        )


            if N_largest_pixels!=False:
            # signal is the average of the N_largest_pixels largest pixels
                image_sorted = np.ravel(image)
                image_sorted.sort()
                sig_mean = np.ma.mean(image_sorted[-N_largest_pixels:])

            elif N_largest_pixels==False:
                sig_mean = np.ma.mean(image_sig)

            # mean of background
            bkg_mean = np.ma.mean(image_bckgrnd)


            sig_mean_array.append(sig_mean)
            bkg_mean_array.append(bkg_mean)

            # convert signal to physical units
            exposureTime_array.append(exposureTime[i])
            sig_mean_physic_array.append(self.convertPhysical(sig_mean_array[i],exposureTime[i]))
            bkg_mean_physic_array.append(self.convertPhysical(bkg_mean_array[i],exposureTime[i]))

            # print status
            if i==int(0.25*len(index_image)):
                print('')
                print('         25% ('+str(i)+'/'+str(len(index_image))+') processed')
            if i==int(0.5*len(index_image)):
                print('         50% ('+str(i)+'/'+str(len(index_image))+') processed')
            if i==int(0.75*len(index_image)):
                print('         75% ('+str(i)+'/'+str(len(index_image))+') processed')
                print('')

        sig_mean_physic_array = np.array(sig_mean_physic_array)
        bkg_mean_physic_array = np.array(bkg_mean_physic_array)


        if filter_cutoff!=False:
            # design of Buterworth filter
            N  = 2    # Filter order
            Wn = filter_cutoff # Cutoff frequency

            # building filter
            B, A = signal.butter(N, Wn, output='ba')
            
            # applying filter
            sig_mean_physic_array = signal.filtfilt(B,A, sig_mean_physic_array)


        return index_image, sig_mean_physic_array, bkg_mean_physic_array, startTime_array

        

    def cropData(self, data_to_crop, crop_by_list, crop_range):
        """
        Crops lists of data
        Arguments: data that has to be cropped (data_to_crop),
                   list with same length as data_to_crop having time or index etc. to which to cropping range corresponds (crop_by_list)
                   range of cropping: crop_range
        """
        if crop_range==[]:
            data_cropped = data_to_crop

        else:
            data_cropped = []
            for i in range(len(crop_by_list)):
                if crop_by_list[i]>crop_range[0] and crop_by_list[i]<crop_range[1]:
                    data_cropped.append(data_to_crop[i])

        return data_cropped



    def plotOneImage(self, imageName):
        image = self.loadImagesFromFolder(self.images_path, imageName)

        # crop image
        image = image[500:1300,900:1700]

        # gauss filter
        image = gaussian_filter(image, sigma=0)

        # plot image
        fig = plt.figure()
        colbNorm = 0.25

        CS = plt.imshow(
                image,
                cmap=plt.cm.inferno, 
                # origin = 'lower',
                norm = colors.PowerNorm(gamma=colbNorm)
                )
        
        # adjust colorbar   
        cbar = plt.colorbar(CS, format='%.e')
        plt.autoscale(False)




    def plotOneImageMasked(self, imageName, airy_disk_radius):

        # self.imageAnalysisBragg = ImageAnalysisBragg(airy_disk_radius = 3, lowSignalSearch=False)
        image = self.loadImagesFromFolder(self.images_path, imageName)
        
         # crop image
        image = image[500:1300,900:1700]

        # # gauss filter
        # image = gaussian_filter(image, sigma=3)

        image_bkg_mask = self.imageAnalysisBragg.maskBackgroundROI(image)
        image_sig_mask = self.imageAnalysisBragg.maskPeak(image, airy_disk_radius)


        fig = plt.figure()
        colbNorm = 0.25

        #
        CS = plt.imshow(
                image,
                cmap=plt.cm.inferno, 
                # origin = 'lower',
                norm = colors.PowerNorm(gamma=colbNorm)
                )

        CS = plt.imshow(
                image_bkg_mask,
                # cmap=plt.cm.inferno, 
                # origin = 'lower',
                norm = colors.PowerNorm(gamma=colbNorm)
                )

        CS = plt.imshow(
                image_sig_mask,
                # cmap=plt.cm.inferno, 
                # origin = 'lower',
                norm = colors.PowerNorm(gamma=colbNorm)
                )
                        
        cbar = plt.colorbar(CS, format='%.e')
        plt.autoscale(False)




    def convertPhysical(self, img, exposureTime):
        quantumEfficiency = 0.48 # quantum efficiency at 632.8 nm
        exposureTime = exposureTime*1e-3    # in SI
        gain = 0 # Gain settings 0...49 use analog signal gain; from 50 up, the stronger digital gain is used. High gain settings may cause visible noise
        pixelGain = 0.3826 # 5.272 # measurements
        pixelsize = 2.2*1e-6 # in SI
        wavelength = 632.8e-9  # nm

        # convert to physical units
        # averagedImages (?)
        # number of photons
        n_p = img / (pixelGain * quantumEfficiency)
        # photon flux
        phi = n_p / (pixelsize**2 * exposureTime * (gain+1))
        # intensity
        img_phys = phi * (constants.h*constants.c/wavelength)

        return img_phys



    def DBtoListsOneCurve(self, curve):

        #load DB
        data = loadDB(self.database_path + '/data.db', 'results')

        exposureTime = []
        imagePath = []
        moloLine = []
        signalIntensityPhys = []
        experimentID = []
        ID = []
        field = []
        averagedImages = []
        SNR = []
        gain = []
        startTime = []
        position = []
        bkg_std = []
        signalIntensity = []
        bkg_mean = []
        line = []
        index = []

        # rearange dict: maiking a list for each key and fill in the data points of the key
        for i in range(len(data)):
            if data[i]['experimentID'] == curve:
                exposureTime.append(data[i]['exposureTime'])
                imagePath.append(data[i]['imagePath'])
                moloLine.append(data[i]['moloLine'])
                signalIntensityPhys.append(data[i]['signalIntensityPhys'])
                experimentID.append(data[i]['experimentID'])
                ID.append(data[i]['ID'])
                field.append(data[i]['field'])
                averagedImages.append(data[i]['averagedImages'])
                SNR.append(data[i]['SNR'])
                gain.append(data[i]['gain'])
                startTime.append(data[i]['startTime'])
                position.append(data[i]['position'])
                bkg_std.append(data[i]['bkg_std'])
                signalIntensity.append(data[i]['signalIntensity'])
                bkg_mean.append(data[i]['bkg_mean'])
                line.append(data[i]['line'])
                index.append(i)

        # return dict with only the important data
        return  {'startTime': startTime, 
                 'exposureTime': exposureTime, 
                 'imagePath': imagePath, 
                 'index': index}


    def loadImagesFromFolder(self, folder, filename):
        """
        Loads an image from folder. Folder and file are arguments.
        """
        # load images and save in list


        if folder == '':
            return

        # folder = '/home/focal-molography/Experiments/043_RD_test/images/'
        filenames = os.listdir(folder)

        # # (dirty bugfix)
        if filenames[0] == ".DS_Store":
            filenames = filenames[1:]
        if filenames[-1] == "Icon\r":
            filenames = filenames[0:-1]


        # open images, put them in images, and labels in molobram_label
        image = np.array(Image.open(folder + '/' + filename))

        return image



if __name__ == '__main__':
    from imageProcessing.figureFormat import *
    import matplotlib.ticker as ticker
    formatOpticsLetters(figsizex = 2,figsizey = 4)
    style()

    database_path = '/home/focal-molography/Experiments/086_RD015_BG21_B_L3M8_binding_curve (post procressing)/database'
    images_path = '/home/focal-molography/Experiments/086_RD015_BG21_B_L3M8_binding_curve (post procressing)/images'

    crop_range = []     #[50,400]
    curve = 5
    postProcessing = PostProcessingBindingCurves(database_path, images_path, curve, crop_range)

    # postProcessing.getValuesFromImage(gauss_sigma=0)

    # # Plot single image
    # postProcessing.plotOneImage('B-3-8_exp-22.99-ms_229')

    # # Plot single image masked
    # postProcessing.plotOneImageMasked('B-3-8_exp-22.99-ms_229')
    
    # # Gauss filter
    # postProcessing.studyGaussFilter()

    # Airdiskradius
    # postProcessing.studyAiryDiskSize()

    # Butterworth filter
    # postProcessing.butterWorthFiltering()

    # # Binning of largest pixels in ROI
    # postProcessing.binLargestPixels()




    # # Optimised data
    postProcessing.optimisedData(airy_disk_radius=0.5, gauss_sigma=False, filter_cutoff=False, N_largest_pixels = False)

    plt.show()
