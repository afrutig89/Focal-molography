"""
:created:       01-Mai-2017
:author:        Roland Dreyfus
:revised:       01-Mai-2017 by Roland Dreyfus
:references:    
"""

import sys
sys.path.append('../')
sys.path.append('../../Simulation Framework')
import matplotlib.pyplot as plt
from matplotlib import colors
from PIL import Image, ImageDraw

from Data_Processing.imageAnalysisBragg import ImageAnalysisBragg
from imageProcessing.figureFormat import *


class PublicationFigImageProcessing():
    """docstring for PublicationFigImageProcessing"""
    def __init__(self):
        self.ImageAnalysisBragg = ImageAnalysisBragg()


    def plotForPaper(self, filename, path_folder_save):

        # load image from folder
        image_raw = np.array(Image.open(filename))

        image_raw, x_axis, y_axis = self.ImageAnalysisBragg.defineXYimageAndAxis(image_raw)

        image_bkg_mask = self.ImageAnalysisBragg.maskBackgroundROI(
                                                      image_raw,
                                                      Delta_r=2.5*1e-6, 
                                                      r_bkg_inner=40.0*1e-6,
                                                      r_bkg_outer=110.0*1e-6)
        image_sig_mask = self.ImageAnalysisBragg.maskPeak(
                                                    image_raw, 
                                                    airy_disk_radius=1)

        formatOpticsLetters()
        style()

        fig1 = plt.figure()
        fig2 = plt.figure()


        ax1 = fig1.add_subplot(111)
        ax2 = fig2.add_subplot(111)

        normFactor = 0.25
        minColor = np.min(image_raw)
        maxColor = np.max(image_raw)

        ax1.imshow(
                image_raw,
                cmap=plt.cm.Reds,
                norm = colors.PowerNorm(
                                    gamma=normFactor,
                                    vmin = minColor,
                                    vmax = maxColor)
                )

        ax2.imshow(image_raw,
                cmap=plt.cm.Greys,
                norm = colors.PowerNorm(
                                    gamma=normFactor,
                                    vmin = minColor,
                                    vmax = maxColor*5e-1),
                 extent=[np.min(x_axis)*1e6,np.max(x_axis)*1e6,
                        np.min(-y_axis)*1e6,np.max(-y_axis)*1e6]
                )  
        ax2.imshow(
                image_bkg_mask**normFactor,
                cmap=plt.cm.Reds,
                norm = colors.PowerNorm(
                                    gamma=1, 
                                    vmin = minColor,
                                    vmax = maxColor*3e-3),
                 extent=[np.min(x_axis)*1e6,np.max(x_axis)*1e6,
                        np.min(-y_axis)*1e6,np.max(-y_axis)*1e6]
                )
        ax2.imshow(
                image_sig_mask**normFactor,
                cmap=plt.cm.Reds,
                norm = colors.PowerNorm(
                                    gamma=1, 
                                    vmin = minColor,
                                    vmax = maxColor*3e-3),
                extent=[np.min(x_axis)*1e6,np.max(x_axis)*1e6,
                        np.min(-y_axis)*1e6,np.max(-y_axis)*1e6]
                )
        plt.xlabel(u'x [\u03bcm]')
        plt.ylabel(u'y [\u03bcm]')

        fig1.savefig(
                ''.join([
                        path_folder_save,
                        'pub_fig_imag_porcess_unmasked',
                        '.png'
                        ]),
                dpi=600,format='png'
                )
        fig2.savefig(
                ''.join([
                    path_folder_save,
                    'pub_fig_imag_porcess_masked',
                    '.png'
                    ]),
                dpi=600,format='png')



if __name__ == '__main__':

    filename = "/home/focal-molography/Experiments/Test RD/ID 2 (Exposure)/D-3-6_exp-9.98-ms_1"
    path_folder_save = ''
    # Bragg conditon data processing object
    publicationFig = PublicationFigImageProcessing()

    # funciton that performs the data processing and plots
    publicationFig.plotForPaper(filename, path_folder_save)