from PyQt4 import QtGui,QtCore, QtSql
import os
import numpy as np
from PIL import Image
import Data_Processing.MologramProcessing as mologram_processing
import Data_Acquisition.DataBase as db
import matplotlib.pylab as plt
import sqlite3

class ReProcessImages(QtCore.QThread):
    """This class reprocesses all the images and saves the data to a sqlite database."""

    def __init__(self):

        # Call parent class constructor
        super(ReProcessImages,self).__init__()

        self.exiting = False
        

        self.column_names = [
            'key INTEGER',
            'time REAL',
            'zPos REAL',
            'molointensity REAL',
            'bkgmean REAL',
            'bkgstd REAL',
            'exposure REAL',
            'MIExpWeighted REAL'
        ]


    def start_processing(self,experiment_folder_path,experiment_info,experiment_length,database_content,column_names):

        # Reset member variable
        self.abort_processing = False

        self.experiment_folder_path     = experiment_folder_path
        self.experiment_info             = experiment_info
        self.experiment_length             = experiment_length
        self.old_database_content         = database_content
        self.db_column_names             = column_names
        self.database_path                 = self.experiment_folder_path + '/database'
        self.start()

    def run(self):

        experiment_length = self.experiment_length
        exposures = self.old_database_content[:,self.db_column_names.index('exposure')]
        time = self.old_database_content[:,self.db_column_names.index('time')]
        
        try:
            z_position = self.old_database_content[:,self.db_column_names.index('zPoss')]
        except Exception:
            z_position = np.ones((experiment_length,1))


        columns = []
        for i in self.column_names:

            columns.append(i)
        
        self.num_columns = len(columns)

        

        con = sqlite3.connect(self.database_path + '/mologram_data.db')

        cur = con.cursor()

        
        cur.execute('DROP TABLE mologram_data')
        
        con.commit()
        con.close()


        con = sqlite3.connect(self.database_path + '/mologram_data.db')

        cur = con.cursor()

        cur.execute("CREATE TABLE mologram_data(" + ','.join(columns) + ")")
        con.commit()
        con.close()
        
        #self.database.create_sql_database(self.column_names,self.experiment_folder_path + '/database')


        # Go through all available images and re-process them

        for index in range(experiment_length):

            # Send signal to update progress bar in main thread
            self.emit(QtCore.SIGNAL("update_progress_bar(int,int)"),index,experiment_length)

            # keys in SQL database start at 1
            key = index + 1



            # If abort signal was received, stop processing
            if self.abort_processing:
                break

            # Get next image path
            image_path,image_name = self.image_path(key,exposures)

            # If image exists, read data and process it
            if os.path.isfile(image_path):
                im = Image.open(self.experiment_folder_path + '/images/' + image_name)
                im = np.array(im.getdata()).reshape(im.size[0], im.size[1])
                im = im.astype('uint16')
                entry = mologram_processing.calc_datapoint_entry(key,time[index],z_position[index,0],exposures[index],im,1)
            elif os.path.isfile(image_path + '.tiff'):
                im = Image.open(self.experiment_folder_path + '/images/' + image_name + '.tiff')
                im = np.array(im.getdata()).reshape(im.size[1], im.size[0])
                im = im.astype('uint16')
                entry = mologram_processing.calc_datapoint_entry(key,time[index],z_position[index,0],exposures[index],im,1)
            else:
                print('Unable to find image file: ' + self.experiment_folder_path + '/images/' + image_name)
                mologram_intensity = -1

            con = sqlite3.connect(self.database_path + '/mologram_data.db')

            cur = con.cursor()

            cur.execute('INSERT INTO mologram_data VALUES (' + entry + ')')
            con.commit()
            con.close()
            

            #print(image_name + ': ' + str(mologram_intensity))

        # Send signal to update progress bar in main thread
        self.emit(QtCore.SIGNAL("update_progress_bar(int,int)"),key,experiment_length)

        # fetch all rows from database
        # if not new_column_name in self.db_column_names():
        #     executed = False
        #     while executed == False:
        #         try:    
        #             cur.execute('ALTER TABLE mologram_data ADD COLUMN ' + new_column_name + ' real')
        #             con.commit()
        #             executed = True
        #         except Exception as inst:
        #             print(inst)

        # for i in range(len(new_column)):
        #     key = i+1
        #     val = new_column[key]
        #     cmd = "UPDATE mologram_data SET " + new_column_name + "=" + str(val) + " where key=" + str(key)
        #     print(cmd)
        #     cur.execute(cmd)
        #     con.commit()

        # Close connection with SQL databse
        # con.close()

        # let GUI know that movement is finished
        self.emit(QtCore.SIGNAL("done_processing"))

    def abort(self):
        self.abort_processing = True
        self.emit(QtCore.SIGNAL("abort_processing"))

    def image_path(self,key,exposures):
        image_name  = self.experiment_info['Mologram Name']
        image_name += '_' + 'exposure-' + str(int(exposures[key-1])) + '-ms' 
        image_name += '_' + str(key)

        # print('self.experiment_folder_path' + str(self.experiment_folder_path))
        # print('image_name' + str(image_name))

        image_path = self.experiment_folder_path + '/images/' + image_name
        return image_path,image_name

    def __del__(self):
        self.exiting = True 
        self.wait()