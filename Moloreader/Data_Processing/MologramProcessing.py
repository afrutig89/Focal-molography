import numpy as np
from scipy import ndimage
from scipy.ndimage.filters import gaussian_filter, median_filter
from Data_Processing.imageAnalysisBragg import ImageAnalysisBragg
from scipy import signal,special
from scipy.signal import fftconvolve

def filterRadiusAiry(img,radiusAiry,camera,magnification):
	"""filters the image with a kernel of the shape of the Airy function whereas the location of the first zero is at
	radius Airy

	:param img: img is a 2d-numpy array
	:param radiusAiry: float, in [m], radius of the Airy disk
	:param camera: camera object
	:param magnification: magnification of the imaging system.

	:returns: 2d numpy array of the convoluted image.
	"""
	radiusAiryPixels = radiusAiry*magnification/(camera.pixelsize)
	# Transform the Airy function such that the first zero is at radiusAiryPixels.
	AiryFunction = lambda x: (special.jn(1,(x*3.83/radiusAiryPixels))/(x*3.83/radiusAiryPixels))**2

	kernel = np.outer(AiryFunction(np.linspace(-50,50,100)),AiryFunction(np.linspace(-50,50,100)))

	kernel = kernel/np.sum(kernel) # normalization of the kernel. (Is this correct? If I normalize I calculate the average intensity in the airy disk with the convolution)

	# plt.figure()
	# plt.imshow(kernel)
	# plt.show()

	# plot the Airy function
	# formatPRX(8,8)
	# fig = plt.figure()
	# x = np.linspace(-7.5,7.5,100)
	# plt.plot(x,AiryFunction(x))
	# plt.gca().set_axis_off()
	# plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, 
	# 	hspace = 0, wspace = 0)
	# plt.margins(0,0)
	# plt.xlim([-7.5,7.5])
	# plt.ylim([-0.01,0.255])
	# plt.gca().xaxis.set_major_locator(plt.NullLocator())
	# plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# fig = removeWhiteSpace(fig)
	# plt.savefig('02plots/Filter_Kernel.png',format='png',dpi=600)
	# plt.show()

	img_filtered = fftconvolve(img,kernel,mode='same')

	return img_filtered

def calc_IntAiryDisk(image, pixelsize = 0.110,  Airy_disk_radius = 0.5, lowSignalSearch=False,referenceMologram=False):
	"""assumes that the Airy disk of the mologram is in a circle with radius 40 um in the center
	of the mologram. uses the algorithm bkg_mean + three times the bkg_std
	params:

	:param pixelsize: size of one pixel in the image in um (camera pixel size / magnification)
	:param Airy_disk_radius: radius of the signal spot (will be averaged)

	"""
	image = np.copy(image)
	
	# calculate the maximum of the image, which must be the molographic center image.
	image = image.astype('float')

	try:
		if lowSignalSearch == True:

			img = image.copy()
			img = median_filter(img, size=(int(2*Airy_disk_radius/pixelsize),int(2*Airy_disk_radius/pixelsize)))

			i, j = np.unravel_index(img.argmax(),img.shape)
			center = j, i # image processing: different coordinate system

			print('low signal detection: mologram at (x,y) = ' + str(center))


		else: 	
			if referenceMologram==False:
				i, j = np.unravel_index(image.argmax(),image.shape)
				center=j,i
			else: #split the image into two halves. One contains the signal the other contains the reference Mologram.
			      #vsplit instead of hsplit can be used in the case of reference molograms where they are on top/bottom
			      #of the signal Mologram.
				image_signal,image_ref=np.hsplit(image,2)
				i_signal, j_signal = np.unravel_index(image_signal.argmax(),image_signal.shape)
				i_ref, j_ref = np.unravel_index(image_ref.argmax(),image_ref.shape)
				center = j_signal, i_signal # image processing: different coordinate system
				center_molo = j_ref,i_ref

	except:
		# empty image
		return 0, 0, 0, 0, 0

	# cut an image area with a radius of 40 microns. 
	img = np.copy(image)
	if referenceMologram == False:
		# cut the background as a torus with inner radius 20 microns and outer radius 40 microns. 
		mask = c_mask(img, center, 20/pixelsize, 40/pixelsize)
		# calculate mean and standard deviation
		bkg_mean = np.mean(img[mask])
		bkg_std = np.std(img[mask])

		if Airy_disk_radius == 0:
			mologram_intensity = float(np.max(image)) - bkg_mean - 3*bkg_std

		else:
			# substract the background + 3 times the standard deviation of the molographic focal spot. 
			signal_image = image #- bkg_mean # - 3*bkg_std
			
			# average the signal in a disk with diameter 0.5 um.
			mask_signal = c_mask(signal_image,center,0,int(Airy_disk_radius/pixelsize))

			mologram_intensity = np.sum(signal_image[mask_signal])/np.sum(mask_signal)

		if mologram_intensity < 0:
			mologram_intensity = 0
		
		SNR = (mologram_intensity- 3*bkg_std) / bkg_std

		reference_intensity=0

		return mologram_intensity, bkg_mean, bkg_std, SNR, reference_intensity
	
	else:
		#If the reference mologram functionality is selected. The image will be split in to two equal sub images.
		img_signal,img_ref=np.hsplit(img,2)
		#The image on the left is the interested signal while the image on the right is the reference signal.
		#Both sub images are passed into the same algorithm to calculate the corresponding intensities.
		mask = c_mask(img_signal, center, 20/pixelsize, 40/pixelsize)
		mask_molo=c_mask(img_ref, center_molo, 20/pixelsize, 40/pixelsize)
		bkg_mean = np.mean(img_signal[mask])
		bkg_std = np.std(img_signal[mask])
		bkg_mean_molo=np.mean(img_ref[mask_molo])
		bkg_std_molo=np.std(img_ref[mask_molo])

		if Airy_disk_radius == 0:
			mologram_intensity = float(np.max(img_signal)) - bkg_mean - 3*bkg_std
			reference_intensity =  float(np.max(img_ref)) - bkg_mean_molo - 3*bkg_std_molo

		else:
			# substract the background + 3 times the standard deviation of the molographic focal spot. 
			signal_image = img_signal #- bkg_mean # - 3*bkg_std
			signal_image_molo = img_ref #- bkg_mean_molo
		    
			# sum up the signal in a disk with diameter 0.5 um.
			mask_signal = c_mask(signal_image,center,0,int(Airy_disk_radius/pixelsize))
			mask_signal_molo = c_mask(signal_image_molo,center_molo,0,int(Airy_disk_radius/pixelsize))

			mologram_intensity = np.sum(signal_image[mask_signal])/np.sum(mask_signal)
			reference_intensity = np.sum(signal_image_molo[mask_signal_molo])/np.sum(mask_signal_molo)

		if mologram_intensity < 0:
			mologram_intensity = 0
		if reference_intensity < 0:
			reference_intensity = 0
		
		SNR = (mologram_intensity - 3*bkg_std ) / bkg_std 

		return mologram_intensity, bkg_mean, bkg_std, SNR, reference_intensity

def c_mask(image, centre, radius1, radius2):
	"""
	Return a boolean mask for a donat with two radii specified by radius1 (inner radius) and radius2 (outer radius) 
	"""

	x, y = np.ogrid[:image.shape[0],:image.shape[1]]
	cy, cx = centre

	# circular mask
	circmask1 = (x-cx)**2 + (y-cy)**2 >= radius1**2
	circmask2 = (x-cx)**2 + (y-cy)**2 <= radius2**2

	return np.logical_and(circmask1,circmask2)


def calc_datapoint_entry(img, algorithm,  pixelsize = 0.110, Airy_disk_radius=0.5, lowSignalSearch=False,referenceMologram=False):
	"""This function does all the Polymorphism of the dataprocessing."""
	if algorithm == 'sum over background plus 3 sigma (filter)':
		img = gaussian_filter(img, sigma=5)
		mologram_intensity, bkg_mean, bkg_std, SNR, reference_intensity = calc_IntAiryDisk(img, pixelsize = pixelsize, lowSignalSearch=lowSignalSearch,referenceMologram=referenceMologram)

	elif algorithm == 'maximum over background plus 3 sigma':
		mologram_intensity, bkg_mean, bkg_std, SNR, reference_intensity = calc_IntAiryDisk(img, pixelsize = pixelsize, Airy_disk_radius=0, lowSignalSearch=lowSignalSearch,referenceMologram=referenceMologram)

	elif algorithm == 'Bragg':
		# pixelsizeCamera = self.camera.info['pixel_size']*1e-6
		# magnification = self.main_widget.setup.camera.specifications.magnification
		img = gaussian_filter(img, sigma=5)
		imagAnalBragg = ImageAnalysisBragg(
			image = img,
			# pixelsizeCamera = pixelsizeCamera,
			# magnification =magnification,
			# airy_disk_radius = Airy_disk_radius,
			lowSignalSearch=False
			)
		mologram_intensity, bkg_mean, bkg_std, SNR = imagAnalBragg.calcAiryDisk(img)
		pass

	else: # algorithm == 'sum over background plus 3 sigma':
		mologram_intensity, bkg_mean, bkg_std , SNR, reference_intensity = calc_IntAiryDisk(img, pixelsize = pixelsize, lowSignalSearch=lowSignalSearch,referenceMologram=referenceMologram)

	if SNR < 0:
		SNR = 0
	return mologram_intensity, bkg_mean, bkg_std, SNR, reference_intensity


################################################################################################
################################################################################################
