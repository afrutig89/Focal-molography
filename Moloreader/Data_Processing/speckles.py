import numpy as np
import scipy
import scipy.ndimage as ndimage
import scipy.ndimage.filters as filters
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import time
import sys
from scipy.signal import fftconvolve
from scipy.ndimage.morphology import generate_binary_structure

## Framework for spekle analysis


def averageSpeckleDimension(data, specklePositions):
    minimum = np.inf
    for i in range(len(specklePositions[0])):
        if minimum > data[specklePositions[1][i],specklePositions[0][i]]:
            minimum = data[specklePositions[1][i],specklePositions[0][i]]
    
    average = np.sum(data > minimum)/len(specklePositions[0])
    return average

def averageSpeckleSize(data, specklePositions):
    minimum = np.inf
    for i in range(len(specklePositions[0])):
        if minimum > data[specklePositions[1][i],specklePositions[0][i]]:
            minimum = data[specklePositions[1][i],specklePositions[0][i]]
    
    average = (np.sum(data[data > minimum])-minimum)/len(specklePositions[0])
    return average


def findSpeckles(data, neighborhoodSize, threshold=0):
    neighborhood = np.ones((neighborhoodSize, neighborhoodSize))
    data_max = filters.maximum_filter(data, footprint=neighborhood)
    maxima = (data == data_max)
    diff = (data > threshold)
    maxima[diff == False] = 0

    labeled, num_objects = ndimage.label(maxima)
    specklePositions = np.array(ndimage.center_of_mass(data, labeled, range(1, num_objects+1)))

    return np.array(
                np.vstack(
                    [specklePositions[:, 1],
                    specklePositions[:, 0]]
                ), dtype=int)


if __name__ == '__main__':
    pass