#!/bin/bash

"""
Installation file focal molography for linux
"""

#############################
## Python 3 for moloreader ##
#############################
"""
Not required for simulation framework and offline mode of moloreader.
"""

sudo apt-get install python3-dev

# Re-install pip3
#sudo apt-get remove  python3-pip
sudo apt-get install python3-pip

# Install Git
sudo apt-get install git

# Install required standard packages for program to run
sudo apt-get install python3-matplotlib
sudo apt-get install python3-numpy
sudo apt-get install python3-scipy
sudo apt-get install python3-paramiko
sudo apt-get install python3-Pillow
sudo apt-get install python3-serial
sudo apt-get install python3-pyqt4
sudo apt-get install python3-pyqt4.qtsql


#####################################
## Modules required for moloreader ##
#####################################

# Install IDS SDK from archive

cd uEyeSDK
sudo sh ./ueyesdk-setup-4.80-usb-amd64.gz.run


# Install IDS module
sudo apt-get install lib32z1-dev

cd ~/Downloads
git clone https://github.com/ncsuarc/ids
cd ids
sudo python3 setup.py install

# Install pigpio
cd ~/Downloads
wget abyz.co.uk/rpi/pigpio/pigpio.zip
unzip pigpio.zip
cd PIGPIO
sudo make
sudo make install


####################
## Other software ##
####################


# Install DB browser
sudo apt-get install build-essential
sudo apt-get install cmake
sudo apt-get install libqt4-dev
sudo apt-get install libsqlite3-dev

# Install DB browser
cd ~/Downloads
git clone https://github.com/sqlitebrowser/sqlitebrowser.git
cd sqlitebrowser
mkdir build
cd build
cmake ..
make
sudo make install

# install jupyter notebook
sudo pip install jupyter

# install sphinx documentation
sudo pip install sphinx
