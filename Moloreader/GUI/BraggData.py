from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit



from SharedLibraries.Database.dataBaseOperations import loadDB, formGroup
from SettingsTab.settingsTab import SettingsTab
from settings import getVariables, getLogExperimentDict, getResultsDict

# line styles
linestyles = ['solid', 'dashed', 'dashdot', 'dotted']
colormap = ['black', 'red', 'green', 'blue', 'cyan', 'magenta']


class BraggData(object):
    """docstring for BraggData"""
    def __init__(self,settingsTab):
        super(BraggData, self).__init__()
        
        self.figure  = plt.figure(figsize=(8,4),facecolor='None',edgecolor='None')
        

    def loadDatabase(self):
        # databaseFile = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home/focal-molography/Experiments', "Database files (*.db)")
        databaseFile = '/Users/rolanddreyfus/Google Drive/Diffractometric Biosensing Subgroup/Experiments/0000_Chip Log/BG02/045_BG02_entire_chip/F_exp_2ms/database/data.db'

        if databaseFile != '':

            # add experiment folder to settings
            folder = os.path.dirname(str(databaseFile))
            self.database_path = folder

          # load database
        if self.database_path == '':
            experimentFolder = self.main_widget.getExperimentFolder()
            if experimentFolder == '': return
            else: self.database_path = '{}/database'.format(experimentFolder)

        self.data = loadDB(self.database_path + '/data.db', 'results')
        self.log = loadDB(self.database_path + '/data.db', 'logExperiment')

        self.groupPlots = 'line'
        if self.data != []:
            # # update combo if database not yet known
            # if not self.comboUpdated:
            #     self.updateCombo()

            # data by keys
            if type(self.data) != list:
                # variable must be a list in order to iterate
                self.data = list([self.data])

            # group dictionaries
            grouped = formGroup(self.data, self.groupPlots)

            self.logNumbers = []

            # self.analyseCurve.clear()

            self.dicts = []
            for group in grouped:
                self.logNumbers.append(str(group[0]['experimentID']))
                dictByKeys = dict()
                for dataSet in group:
                    if type(dataSet) != dict:
                        # empty database (no entries, but table exists)
                        x = y = 1
                        continue

                    # if not empty, go through list entries
                    for key, value in dataSet.items():
                        if key not in list(dictByKeys.keys()):
                            dictByKeys[key] = []
                        dictByKeys[key].append(value)


                self.dicts.append(dictByKeys)


            # add titles
            self.variablesDict = getVariables()


    def initImage(self):

        # clear plot if exist
        if 'ax' in dir(self):
            try:
                plt.sca(self.ax)
            except: pass

        plt.cla()
        # clear and redeclare axes
        self.ax = self.figure.add_subplot(111)

        # plot layout
        plt.xlim([0,1])
        plt.ylim([0,1])
        self.figure.tight_layout()


    def fitFunction(self, x, a, b, c):
        return a * np.exp(-b * x) + c


    def fitData(self, xdata, ydata):
        popt, pcov = curve_fit(self.fitFunction, xdata, ydata)
        yFitted = popt

        return yFitted


    def plot(self):

        self.initImage()
        self.loadDatabase()
        self.plotX = 'moloLine'
        self.plotY = 'signalIntensity'
        # self.plotY = 'bkg_mean'


        # add titles
        variablesDict = getVariables()
        self.ax.set_xlabel(variablesDict[self.plotX])
        self.ax.set_ylabel(variablesDict[self.plotY])
        self.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


        # plot data and show canvas
        x_max = y_max = 0
        x_min = y_min = float('inf')

        for i in range(len(self.dicts)):

            # if set visible
            x_max = np.max((x_max,np.max(self.dicts[i][self.plotX])))
            x_min = np.min((x_min,np.min(self.dicts[i][self.plotX])))
            y_max = np.max((y_max,np.max(self.dicts[i][self.plotY])))
            y_min = np.min((y_min,np.min(self.dicts[i][self.plotY])))


            self.groupLines = 'line'
            if self.groupLines != '':
                # plot with time on x-axis
                lines = []
                for line in self.dicts[i][self.groupLines]:
                    if line not in lines:
                        lines.append(line)

                for line in lines:
                    indices = np.array(self.dicts[i][self.groupLines]) == line
                    x = np.array(self.dicts[i][self.plotX])[indices]
                    y = np.array(self.dicts[i][self.plotY])[indices]
                    if line == lines[0]:
                        self.points = plt.plot(x, y, 
                                            linestyle=linestyles[lines.index(line) % len(linestyles)],
                                            color=colormap[i % len(colormap)],
                                            label='{}, {}'.format(self.dicts[i][self.groupPlots][0],lines)
                                        )


                    else:
                        self.points = plt.plot(x, y, 
                                            linestyle=linestyles[lines.index(line) % len(linestyles)],
                                            color=colormap[i % len(colormap)],
                                        )

            else:
                # plot with time on x-axis
                x = self.dicts[i][self.plotX]
                y = self.dicts[i][self.plotY]
                self.points = plt.plot(x, y, 
                                            color=colormap[i % len(colormap)],
                                            label='{}'.format(self.dicts[i][self.groupPlots][0])
                                        )

            # fit data
            lines_to_fit = []
            for line_to_fit in lines_to_fit:
                x = np.array(self.dicts[line_to_fit-1][self.plotX])
                y = np.array(self.dicts[line_to_fit-1][self.plotY])

                yFitted = self.fitData(x,y)
                xFitted = np.linspace(np.min(x),np.max(x),150)
                plt.plot(xFitted, self.fitFunction(xFitted, *yFitted),'k--')

            if self.groupLines != '':
                plt.legend(title='{},{}'.format(getVariables()[self.groupPlots],getVariables()[self.groupLines]), ncol=int(len(self.dicts)/9)+1) # plot appearance

            else:
                plt.legend(title=getVariables()[self.groupPlots], ncol=int(len(self.dicts)/9)+1) # plot appearance


            plt.xlim([x_min, x_max])
            plt.ylim([y_min,y_max])
            self.figure.tight_layout()

        else:
            return




if __name__ == '__main__':

    settingsTab = SettingsTab
    bragg_data = BraggData(settingsTab)
    bragg_data.plot()
    plt.show()

