from PyQt4 import QtGui, QtCore
from GUI.Manual_Motor_Control_Tab.GUI_Axis import GUIAxis
import time

class AxisControl(QtGui.QWidget):
    """ Constructor """
    def __init__(self,main_widget):

        # Call parent class constructor
        super(AxisControl,self).__init__()

        # connect to rest of GUI
        self.main_widget = main_widget

        self.initUI()


    def initUI(self):
        # Create all axes

        self.x  = GUIAxis('X', self.main_widget.setup.motors.x, self.main_widget, 'right', 'left', self.main_widget.setup.chip.fields.hor_spacing, 'mm',('Left','Right'))
        self.y  = GUIAxis('Y', self.main_widget.setup.motors.y, self.main_widget, 'down', 'up', self.main_widget.setup.chip.fields.ver_spacing, 'mm',('Up','Down'))
        self.z  = GUIAxis('Z', self.main_widget.setup.motors.z, self.main_widget, 'down', 'up', 20, 'um',('PgUp','PgDown'))
        self.TE = GUIAxis('TE', self.main_widget.setup.motors.TE, self.main_widget, 'ccw_up', 'cw_up', 0.1, 'deg',('Ctrl+Left','Ctrl+Right'))
        self.MS = GUIAxis('MS', self.main_widget.setup.motors.MS, self.main_widget, 'cw_down', 'ccw_down', 0, 'deg',('Shift+Left','Shift+Right'))
        self.axes = [self.x, self.y, self.z, self.TE, self.MS]

        # set offset to B,1,1
        x_offset, y_offset = self.main_widget.setup.chip.cartesian_position('B,1,1')
        self.x.set_offset(x_offset, 'mm')
        self.y.set_offset(y_offset, 'mm')

        # Initialize widgets and layout
        self.keyboard   = self.initKeyboard()

        # Set overall layout
        positions = QtGui.QGroupBox('Position')
        keyboardParams = QtGui.QVBoxLayout()
        for axis in self.axes:
            keyboardParams.addWidget(axis.simple_widget)

        switchFeedLayout = QtGui.QHBoxLayout()
        self.start_btn = QtGui.QPushButton('Start RefSwitch Feed')
        self.stop_btn = QtGui.QPushButton('Stop RefSwitch Feed')
        self.start_btn.clicked.connect(self.startRefSwitchFeed)
        self.stop_btn.clicked.connect(self.stopRefSwitchFeed)

        switchFeedLayout.addWidget(self.start_btn)
        switchFeedLayout.addWidget(self.stop_btn)

        keyboardParams.addLayout(switchFeedLayout)


        positions.setLayout(keyboardParams)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(positions)
        layout.addWidget(self.keyboard)
        self.setLayout(layout)

        self.refSwitchFeed = RefSwitchFeed(self.axes)


    def initKeyboard(self):

        controller = QtGui.QGroupBox('Controller')

        fbox = QtGui.QFormLayout()
        fbox.addRow(QtGui.QLabel('X'),self.x.dist_edit)
        fbox.addRow(QtGui.QLabel('Y'),self.y.dist_edit)

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)

        # Place direction buttons in a grid layout
        grid.addWidget(self.x.neg, 3,1)
        grid.addWidget(self.x.pos, 3,4)
        grid.addWidget(self.y.neg, 2,3)
        grid.addWidget(self.y.pos, 5,3)
        grid.addWidget(self.z.neg, 2,6)
        grid.addWidget(self.z.dist_edit, 3,6)
        grid.addWidget(self.z.pos, 5,6)
        grid.addWidget(self.TE.pos,2,8)
        grid.addWidget(self.TE.dist_edit,2,9)
        grid.addWidget(self.TE.neg,2,10)
        grid.addWidget(self.MS.neg,5,8)
        grid.addWidget(self.MS.dist_edit,5,9)
        grid.addWidget(self.MS.pos,5,10)

        # Set stretch areas between axes controls
        grid.setColumnStretch(5,1)
        grid.setColumnStretch(7,1)


        layout = QtGui.QVBoxLayout()
        layout.addLayout(grid)
        layout.addWidget(QtGui.QLabel(''))
        layout.addLayout(fbox)

        controller.setLayout(layout)
        controller.setFixedWidth(500)



        return controller



    def updatePos(self):
        for axis in self.axes: 
            axis.updatePos()

    def updateRefSwitch(self):
        for axis in self.axes: 
            axis.updatePos()
        
    def display_axis(self,i):
        self.axis_stack.setCurrentIndex(i)


    def enableButtons(self, state):
        if state == False:
            self.x.neg.setEnabled(False)
            self.x.pos.setEnabled(False)
            self.y.neg.setEnabled(False)
            self.y.pos.setEnabled(False)
            self.z.neg.setEnabled(False)
            self.z.pos.setEnabled(False)
            self.TE.pos.setEnabled(False)
            self.TE.neg.setEnabled(False)
            self.MS.neg.setEnabled(False)
            self.MS.pos.setEnabled(False)
        else:
            self.x.neg.setEnabled(True)
            self.x.pos.setEnabled(True)
            self.y.neg.setEnabled(True)
            self.y.pos.setEnabled(True)
            self.z.neg.setEnabled(True)
            self.z.pos.setEnabled(True)
            self.TE.pos.setEnabled(True)
            self.TE.neg.setEnabled(True)
            self.MS.neg.setEnabled(True)
            self.MS.pos.setEnabled(True)


    def getPositionAbs(self):
        reference = self.main_widget.setup.motors.read_starting_positions()
        positions = dict()
        for ax in self.axes:
            positions[ax.name_label.text()] = ax.axis.pos() + reference[ax.name_label.text()] - ax.get_offset()

        return positions


    def getPositionRel(self):
        positions = dict()
        for ax in self.axes:
            positions[ax.name_label.text()] = ax.axis.pos() - ax.get_offset()

        return positions


    def uploadReference(self):
        reference = self.main_widget.setup.motors.read_starting_positions()
        for ax in self.axes:
            ax.reference_position = reference[ax.name_label.text()]


    def startRefSwitchFeed(self):

        self.refSwitchFeed.display = True
        self.refSwitchFeed.start()

    def stopRefSwitchFeed(self):

        self.refSwitchFeed.display = False

class RefSwitchFeed(QtCore.QThread):
    """A class that dynamically displays the state of the ref switches in the UI with 100 ms intervals."""
    def __init__(self, axis):
        super(RefSwitchFeed, self).__init__()
        self.axis = axis
        self.display = False

    def run(self):

        while self.display:

            for axis in self.axis:

                axis.updateRefSwitchState()

            
            time.sleep(0.1)

        