from PyQt4 import QtGui, QtCore
from Reader_Control.Axis_Control.Move_motors import Motor_movement_thread
from GUI.ImageButton import ImageButton
from PyQt4.QtGui import QMessageBox


# initialize possible units for distance and angle coordinates
units = {
	'distance' : ['um', 'mm', 'steps'],
	'angle' : ['deg','steps']
}

class GUIAxis(QtGui.QWidget):
    def __init__(
            self,
            name,
            axis,
            main_widget,
            pos,
            neg,
            default_value,
            default_unit,
            shortcuts):

        # Call parent constructor
        super(GUIAxis,self).__init__()

        # Connect to main widget
        self.main_widget = main_widget

        # Create seperate thread to handle moving motors
        self.worker_thread = Motor_movement_thread()

        # save name
        self.name = name

        # declare axis (i.e. Axisl_class.py axis)
        self.axis  = axis

        # Initialize widgets and layout
        self.simple_widget = self.initUI(pos, neg, default_value, default_unit, shortcuts)

        # Connect appropriate siganls to worker thread
        self.connect(self.worker_thread, QtCore.SIGNAL("done_moving(int)"), self.stop_moving)


    def initUI(self, pos_dir, neg_dir, default_value, default_unit, shortcuts):
        """
        Initializes the lines for the manual control widget.

        :param name: label of the axis (e.g. 'x')
        :param axis: motor control for the axis
        :param units: possible units for the given axis
        """

        # set default unit to first one in the list (mm,deg)
        self.currUnit_rel = units[self.axis.axis_type][0]

        # initialize offset
        self.offset = 0
        stepUnit = units[self.axis.axis_type][0]
        smallestValue = self.axis.convertSteps(1, stepUnit)

        # Declare Widgets for the position controls
        self.name_label = QtGui.QLabel(self.name)
        self.name_label.setToolTip('Maximal resolution: %.3f%s' % (smallestValue, stepUnit))
        self.dist_edit = QtGui.QDoubleSpinBox(value = default_value)
        self.dist_edit.setToolTip('Maximal resolution: %.3f%s' % (smallestValue, stepUnit))
        self.dist_units = QtGui.QComboBox()
        self.pos_label  = QtGui.QLabel('')
        self.ref_switch_state = QtGui.QLabel('')
        self.ref_switch_state_lbl = QtGui.QLabel('Ref Switch:')

        widgets = [self.name_label, self.pos_label, self.dist_units, self.dist_edit,self.ref_switch_state_lbl,self.ref_switch_state]

        # Generate buttons which will be placed in another layout
        neg_img_path = './images/' + neg_dir + '_arrow.png'
        pos_img_path = './images/' + pos_dir + '_arrow.png'

        self.neg = ImageButton(neg_img_path)
        self.neg.setStatusTip('Move {} in negative direction ({})'.format(self.name, shortcuts[0]))
        self.neg.setToolTip('Move {} in negative direction ({})'.format(self.name, shortcuts[0]))
        self.neg.setShortcut(shortcuts[0])
        self.neg.clicked.connect(lambda: self.onMoveClicked(sign = -1))

        self.pos = ImageButton(pos_img_path)
        self.pos.setStatusTip('Move {} in positive direction ({})'.format(self.name, shortcuts[1]))
        self.pos.setToolTip('Move {} in positive direction ({})'.format(self.name, shortcuts[1]))
        self.pos.setShortcut(shortcuts[1])
        self.pos.clicked.connect(lambda: self.onMoveClicked())

        # Add unit names to combo boxes
        for unit in units[self.axis.axis_type]:
            self.dist_units.addItem(unit)

        self.currentUnit = default_unit
        self.dist_units.setCurrentIndex(units[self.axis.axis_type].index(self.currentUnit))
        self.dist_units.currentIndexChanged.connect(self.unitsChanged)

        # set the range of the distance input to smallestStep - 10000
        smallestValueDefaultUnit = self.axis.convertSteps(1, self.dist_units.currentText()) + 0.01 # add 0.01 in order to move
        self.dist_edit.setRange(smallestValueDefaultUnit, 10000)

        # Setup layout
        simple_layout = QtGui.QHBoxLayout()

        for widget in widgets:
            simple_layout.addWidget(widget)

        # Create widget using layout
        simple_widget = QtGui.QWidget()
        simple_widget.setLayout(simple_layout)

        return simple_widget

    
    def onMoveClicked(self, sign = 1):
        """
        Fetch information from GUI and call function to 
        parse parameters and move motors
        """

        # Fetch information from GUI
        distance = float(self.dist_edit.value())
        unit = self.dist_units.currentText()

        # In case the 'neg' button of the normal mode was pressed
        distance *= sign

        # Move using these parameters
        self.start_moving(distance, unit)


    def start_moving(self, distance, unit, relative=True):
        """
        Parse movement parameters and launch thread to move motors 
        accordingly 
        """

        # convert input value to steps
        steps = self.axis.returnSteps(distance, unit)

        # Adjust distance if movement is absolute
        if relative == False:
            steps -= self.axis.pos('steps')
            steps += self.offset

        # Check for correct user input
        if self.check_valid_input(steps, 'steps') == -1:
            return

        # Launch separate thread for movement and disable any other movement while it is running
        try: self.main_widget.set_in_movement(True)
        except: pass

        self.worker_thread.move(self.axis, steps)


    def stop_moving(self,retval = 0):

        # If there was an error, diplay a text box accordingly
        if retval != 0:
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Warning")
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setStandardButtons(QtGui.QMessageBox.Ok)

            if   retval == 1:
                msg.setText("The current position is out of bounds")
                msg.setInformativeText("Recalibrate the current axis to fix")
                msg.exec_()
            elif retval == -1:
                msg.setText("You have hit one of the end switches")
                msg.setInformativeText("Select the 'no safety' mode and move away from the edge")
                msg.exec_()

        # Update position and image
        self.main_widget.stop_moving()


    def check_valid_input(self, distance, unit, popup=True):
        """ 
        Check if user input is correct, display appropriate message
        boxes and return status 
        """

        max_mm_distance = 3
        max_um_distance = 1e3

        # Check for invalid float
        try:
            distance = float(distance)
        except Exception:
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Error")
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setStandardButtons(QtGui.QMessageBox.Ok)
            msg.setText("Please enter a valid distance or position")
            msg.exec_()
            return -1

        # Check for unusually large distance
        if   unit == 'mm' and abs(distance) > max_mm_distance and popup == True:
            confirm_distance = True
        elif unit == 'um' and abs(distance) > max_um_distance and popup == True:
            confirm_distance = True
        else:
            confirm_distance = False

        # If distance is unusually large, confirm with user before proceeding
        if confirm_distance == True:
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Question")
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            msg.setText("Are you sure you want to move " + str(distance) + "mm")
            retval = msg.exec_()

            if retval == QtGui.QMessageBox.No:
                return -1


    def get_offset(self, unit=None):
        if unit == None:
            unit = units[self.axis.axis_type][0]

        return self.axis.convertSteps(self.offset, unit)

    def set_offset(self, offset, unit=None):
        if unit == None:
            unit = units[self.axis.axis_type][0]

        self.offset = self.axis.returnSteps(offset, unit)


    def onCalActivated(self, popup = True):
        """ Executed when calibration button is pressed. Calibrate 
        axis and tell user when finished. """

        # calibrate just this axis
        self.axis.calib()

        # update all other widgets
        self.main_widget.updateAll()

        # show message when done
        if popup == True:
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Calibration")
            msg.setIcon(QtGui.QMessageBox.Information)
            msg.setStandardButtons(QtGui.QMessageBox.Ok)
            msg.setText("Calibration is finshed")
            msg.exec_()


    """ Executed when restore button is pressed. """
    def onRestoreActivated(self):
        self.main_widget.updatePos()


    """ Executed when reset button is pressed. Resets axis offset to zero. """
    def onResetActivated(self):
        self.offset = 0
        self.main_widget.updatePos()


    def updatePos(self):
        """
        Executed whenever the axis position or settings are changed.
        Updates position label appropriately.

        """

        # convert self.offset to current units
        offset = self.axis.convertSteps(self.offset, self.currUnit_rel)

        position = str(round(self.axis.pos(self.dist_units.currentText())-offset,3))
        self.pos_label.setText(position)

        self.updateRefSwitchState()
        

    def updateRefSwitchState(self):
        """Updates the reference switch states."""

        self.ref_switch_state.setText(str(self.axis.get_start_switch()))


    def unitsChanged(self):
        value = float(self.dist_edit.value())
        conversionFactor = self.axis.convertUnits(self.currentUnit, self.dist_units.currentText())
        self.currentUnit = self.dist_units.currentText()

        # set the range of the distance input to smallestStep - 10000
        smallestValueDefaultUnit = self.axis.convertSteps(1, self.dist_units.currentText()) + 0.01 # add 0.01 in order to move
        self.dist_edit.setRange(smallestValueDefaultUnit, 10000)

        if self.currentUnit == 'steps':
            newValue = int(value*conversionFactor)
        else:
            newValue = value*conversionFactor

        self.dist_edit.setValue(newValue)
        self.updatePos()

