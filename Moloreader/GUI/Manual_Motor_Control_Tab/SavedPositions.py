from PyQt4 import QtGui, QtCore
import json
import numpy as np
import time
from GUI.MologramSelector import MologramSelector
from Reader_Control.Axis_Control.Move_motors import Move_to_mologram_thread


class SavedPositions(QtGui.QWidget):
    def __init__(self,main_widget):
        super(SavedPositions,self).__init__()

        self.main_widget = main_widget
        self.camera = self.main_widget.setup.camera

        self.heights = self.read_heights()

        # initialize thread to do movements
        self.worker_thread = self.main_widget.mologramTab.mologramLineRow.worker_thread
        self.connect(self.worker_thread, QtCore.SIGNAL("done_moving"), self.main_widget.stop_moving)

        self.initUI()


    def initUI(self):

        # Define buttons
        self.set_position      = QtGui.QPushButton('Set position')
        self.goto_position     = QtGui.QPushButton('Go to position')
        self.surfaceHeight     = QtGui.QLabel()
        if 'heights' in dir(self):
            self.surfaceHeight.setText('Surface height: %.1fum' % (self.heights["surface_height"]*1e3))
        self.moloHeight        = QtGui.QLabel()
        if 'heights' in dir(self):
            self.moloHeight.setText('Mologram height: %.1fum' % (self.heights["mologram_height"]*1e3))

        self.setSurfaceHeight  = QtGui.QPushButton('Set surface height')
        self.setMoloHeight     = QtGui.QPushButton('Set focal plane Height')
        self.goToSurfaceHeight = QtGui.QPushButton('Go to surface height')
        self.goToMoloHeight    = QtGui.QPushButton('Go to focal plane height')

        self.mologram_pos = MologramSelector(self.main_widget.setup.chip)

        # connect buttons
        self.set_position.clicked.connect(self.onSetPositionClicked)
        self.goto_position.clicked.connect(self.onGoToPositionClicked)
        self.setSurfaceHeight.clicked.connect(self.onSetSurfaceHeightClicked)
        self.setMoloHeight.clicked.connect(self.onSetMologramHeightClicked)
        self.goToSurfaceHeight.clicked.connect(self.onGoToSurfaceHeightClicked)
        self.goToMoloHeight.clicked.connect(self.onGoToMologramHeightClicked)

        self.adjustExposureTime = QtGui.QPushButton('Adjust exposure time')
        self.adjustExposureTime.clicked.connect(self.findExposureTime)

        self.centerMologram = QtGui.QPushButton('Center mologram')
        self.centerMologram.clicked.connect(self.findMologramCenter)

        self.adjustCoupling = QtGui.QPushButton('Adjust coupling')
        self.adjustCoupling.clicked.connect(self.findCouplingAngle)


        self.subtractImageButton = QtGui.QPushButton("Subtract image")
        self.subtractImageButton.clicked.connect(self.subtractImageBrowse)
        self.find_maximum_image_fine = QtGui.QPushButton("Focus (fine)")
        self.find_maximum_image_fine.clicked.connect(self.focusFine)
        self.find_maximum_image_coarse = QtGui.QPushButton("Focus (coarse)")
        self.find_maximum_image_coarse.clicked.connect(self.focusCoarse)

        # Layout
        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(self.mologram_pos)
        hbox.addWidget(self.set_position)
        hbox.addWidget(self.goto_position)
        hbox.addWidget(self.centerMologram)
        hbox.addWidget(self.subtractImageButton)

        hbox2 = QtGui.QHBoxLayout()
        hbox2.addWidget(self.surfaceHeight)
        hbox2.addWidget(self.setSurfaceHeight)
        hbox2.addWidget(self.goToSurfaceHeight)
        hbox2.addWidget(self.adjustCoupling)
        hbox2.addWidget(self.find_maximum_image_coarse)

        hbox3 = QtGui.QHBoxLayout()
        hbox3.addWidget(self.moloHeight)
        hbox3.addWidget(self.setMoloHeight)
        hbox3.addWidget(self.goToMoloHeight)
        hbox3.addWidget(self.adjustExposureTime)
        hbox3.addWidget(self.find_maximum_image_fine)

        vbox.addLayout(hbox)
        vbox.addLayout(hbox2)
        vbox.addLayout(hbox3)

        self.set_position.resize(self.set_position.sizeHint())
        self.goto_position.resize(self.goto_position.sizeHint())
        self.setSurfaceHeight.resize(self.setSurfaceHeight.sizeHint())
        self.setMoloHeight.resize(self.setMoloHeight.sizeHint())
        self.goToSurfaceHeight.resize(self.goToSurfaceHeight.sizeHint())
        self.goToMoloHeight.resize(self.goToMoloHeight.sizeHint())


    def onSetPositionClicked(self):
        pos = self.mologram_pos.position()
        position = self.main_widget.setup.chip.cartesian_position(pos)

        self.setPosition(position)

        # update position and images
        self.main_widget.updatePos()


    def setPosition(self, position):
        if type(position) == str:
            # position is given as string (E.g. 'B,1,1')
            moloPosition = position
            # make (x,y)-tuple
            position = self.main_widget.setup.chip.cartesian_position(position)

        else:
            # position is directly given (x,y)-tuple. Use position given in the GUI (Set Position)
            moloPosition = self.mologram_pos.position()

        x, y = position
        self.main_widget.setup.motors.x.set_position(x, 'mm')
        self.main_widget.setup.motors.y.set_position(y, 'mm')

        print('Saved position as ' + str(moloPosition) \
                + ' Coordinates: ' + str((x,y)))


    def onGoToPositionClicked(self):
        pos = self.mologram_pos.position()

        print('>> Go to position {}'.format(pos))
        # move to mologram
        self.main_widget.move_to_nothread(pos,updatePositionOnly= False)


    def write_heights(self,heights):
        file = open('heights.txt','w')
        json.dump(heights,file)
        file.close()
        

    def read_heights(self):
        file = open('heights.txt','r')
        heights = json.load(file)
        file.close()

        return heights


    def onSetSurfaceHeightClicked(self):
        surfaceHeight = self.main_widget.setup.motors.z.pos('mm')
        offset = self.main_widget.manualControlTab.axisControl.z.get_offset('mm')
        surfaceHeight -= offset

        self.heights["surface_height"] = surfaceHeight
        self.heights["mologram_height"] = self.heights["surface_height"] + 0.590 # mologram height +590um

        self.write_heights(self.heights)

        self.surfaceHeight.setText('Surface height: %.1fum' % (self.heights["surface_height"]*1e3))
        self.moloHeight.setText('Mologram height: %.1fum' % (self.heights["mologram_height"]*1e3))

    def onGoToSurfaceHeightClicked(self):

        self.main_widget.manualControlTab.axisControl.z.start_moving(
            distance = self.heights["surface_height"],
            unit     = 'mm',
            relative = False
        )

    def onGoToSurfaceHeightClickedNoThread(self):
        """When in the experiment tab, it is necessary not to move in a thread"""
        x = self.main_widget.setup.motors.x.pos('mm')
        y = self.main_widget.setup.motors.y.pos('mm')
        z = self.heights["surface_height"]
        self.main_widget.move_to_nothread([x,y,z],updatePositionOnly= True)


    def onSetMologramHeightClicked(self):
        mologramHeight = self.main_widget.setup.motors.z.pos('mm')
        offset = self.main_widget.manualControlTab.axisControl.z.get_offset('mm')
        mologramHeight -= offset

        self.heights["mologram_height"] = mologramHeight
        self.heights["surface_height"] = self.heights["mologram_height"] - 0.590 

        self.write_heights(self.heights)
        
        self.surfaceHeight.setText('Surface height: %.1fum' % (self.heights["surface_height"]*1e3))
        self.moloHeight.setText('Mologram height: %.1fum' % (self.heights["mologram_height"]*1e3))


    def onGoToMologramHeightClicked(self):
        currentPosition = self.main_widget.setup.motors.z.pos('mm')
        relativeDistance = self.heights["mologram_height"]-currentPosition

        self.main_widget.manualControlTab.axisControl.z.start_moving(
            distance = relativeDistance,
            unit     = 'mm',
            relative = True
        )

    def onGoToMologramHeightClickedNoThread(self):
        """When in the experiment tab, it is necessary not to move in a thread"""
        x = self.main_widget.setup.motors.x.pos('mm')
        y = self.main_widget.setup.motors.y.pos('mm')
        z = self.heights["mologram_height"]
        self.main_widget.move_to_nothread([x,y,z],updatePositionOnly= True)



    def findExposureTime(self):
        
        self.main_widget.cameraWidget.cameraFeed.adjustExposure()
        self.main_widget.cameraWidget.cameraControl.update_exposure_value()
        self.main_widget.cameraWidget.cameraFeed.updateCameraFeed()

        print(self.main_widget.setup.camera.exposure)
        

    def findMologramCenter(self):
        image, retval = self.camera.get_raw_img()
        self.main_widget.cameraWidget.cameraFeed.centerImage(image)


    def findCouplingAngle(self):
        """Very fast adjustment of the coupling angle."""
        self.main_widget.cameraWidget.cameraFeed.adjustCoupling()


    def focusFine(self):
        self.main_widget.cameraWidget.cameraFeed.fineFocusZ(numberOfSteps=10, stepSize=1)
        self.main_widget.cameraWidget.cameraFeed.updateCameraFeed()


    def focusCoarse(self):
        self.main_widget.cameraWidget.cameraFeed.coarseFocusZ(numberOfSteps=10, stepSize=3)
        self.main_widget.cameraWidget.cameraFeed.updateCameraFeed()


    def subtractImageBrowse(self):
        """
        todo
        """
        imagePath = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home/focal-molography/Experiments', "Image files (*)")
        if imagePath != '':
            image = np.array(Image.open(imagePath))
            self.cameraWidget.cameraFeed.plotImage(image)
