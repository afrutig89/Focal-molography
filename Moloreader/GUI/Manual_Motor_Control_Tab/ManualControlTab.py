from PyQt4 import QtGui

from GUI.Manual_Motor_Control_Tab.Axis_Control_widgets import AxisControl
from GUI.Manual_Motor_Control_Tab.SavedPositions import SavedPositions


class ManualControlTab(QtGui.QWidget):
    def __init__(self, main_widget):
        super(ManualControlTab, self).__init__()

        self.main_widget = main_widget

        self.initUI()


    def initUI(self):
        self.axisControl    = AxisControl(self.main_widget)
        self.savedPositions = SavedPositions(self.main_widget)

        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        vbox.addWidget(self.axisControl)
        vbox.addWidget(self.savedPositions)
        vbox.addStretch()


    def goToStartPosition(self):
        pass


    def goToPreviousPosition(self):
        pass