from PyQt4 import QtGui,QtCore
class ComboBox(QtGui.QComboBox):
	def __init__(self,items):
		super(ComboBox,self).__init__()

		for item in items:
			self.addItem(item)