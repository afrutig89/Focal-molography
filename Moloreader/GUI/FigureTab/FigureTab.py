from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui,QtCore

# matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches


from PIL import Image
from matplotlib import colors
from matplotlib.colors import LogNorm
from matplotlib import cm


from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT #as NavigationToolbar

from SharedLibraries.Database.dataBaseOperations import loadDB, formGroup
from Data_Acquisition.permission import changePermissionsPath

from GUI.CameraWidget.CameraWidget import CameraWidget

import os
import numpy as np

class FigureTab(QtGui.QWidget):

    """ Constructor """
    def __init__(self,main_widget):

        # Call parent class constructor of the QtGuiWidget
        super(FigureTab,self).__init__()

        # Assign main widget
        self.main_widget = main_widget

        # Call child constructor
        self.initUI()

        self.initImage()

        self.current_plot = None

    """ Initialization of GUI """
    def initUI(self):

        self.chip = self.main_widget.setup.chip

        # Set layout
        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)
        
        # set up canvas
        self.figure, self.ax = plt.subplots(
                                        self.chip.fields.lines, 
                                        self.chip.fields.molos,
                                        facecolor='None',
                                        edgecolor='None'
                                    )

        self.canvas  = FigureCanvas(self.figure)
        self.figure.canvas.mpl_connect('button_press_event', self.onClick)

        # declare widgets
        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Experiment Folder')
        
        # line edits and selectors
        self.editFolder = QtGui.QLineEdit()

        self.load_Imgs = QtGui.QPushButton('Load images')
        self.load_Imgs.clicked.connect(self.load_images)
        self.load_Imgs.resize(self.load_Imgs.sizeHint())
        

        self.toolbar = QtGui.QToolBar()
        self.toolbar.addWidget(NavigationToolbar(self.canvas,self))
        self.toolbar.addWidget(QtGui.QWidget()) # some dummy widget


        # layout
        fbox = QtGui.QFormLayout()
        hbox_folder   = QtGui.QHBoxLayout()
        self.toolbar.addWidget(self.editFolder)
        self.toolbar.addWidget(self.load_Imgs)
          

        # Assign widget to layout
        vbox.addWidget(self.toolbar)

        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.canvas)
        hbox.addLayout(fbox)

        vbox.addLayout(hbox)

        # fbox.addRow(self.editFolder_lbl, hbox_folder)


    def initImage(self):

        plt.subplots_adjust(wspace=0,hspace=0)

        # turn off axis label

        for i in range(np.shape(self.ax)[0]):
            for j in range(np.shape(self.ax)[1]):

                self.ax[i, j].xaxis.set_ticklabels([])
                self.ax[i, j].yaxis.set_ticklabels([])
                self.ax[i, j].set_xticks([])
                self.ax[i, j].set_yticks([])

        plt.subplots_adjust(left=0.0, right=1.0, top=1.0, bottom=0.0)

        # show image
        self.canvas.draw()
    

    def load_images(self):

        # get images
        images, mologram_label = self.loadImagesFromFolder()
        print('>> load images')


        # init image
        self.initImage()


        k = 0
        for image in images:
            line = int(mologram_label[k,1])
            molo = int(mologram_label[k,2])
            self.plotImage(image, line-1, molo-1)
            k += 1

        self.canvas.draw()

    

    def plotImage(self, image_to_plot, line, molo):
        # clear plot if exist
        if 'ax' in dir(self):
            plt.cla()

        colbNorm = 0.25

        CS = self.ax[line, molo].imshow(
                                    image_to_plot,
                                    cmap = plt.cm.inferno,
                                    # clim = (0,4095),
                                    norm = colors.PowerNorm(gamma=colbNorm, vmin = 0,
                                    vmax = 4095),
                                )

        self.ax[line, molo].text(
                                5,
                                220, 
                                ''.join(['L',str(line+1),', ','M',str(molo+1)]), 
                                fontsize=8, 
                                color='white'
                            )
        


    def loadImagesFromFolder(self):
        """
        Loads images from a specified folder and saves them in 
        a list of arrays.
        """
        # load images and save in list


        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '/home/focal-molography/Experiments')
        if folder == '':
            return

        self.editFolder.setText(folder)

        # folder = '/home/focal-molography/Experiments/043_RD_test/images/'
        filenames = os.listdir(folder)

        mologram_label = np.zeros((len(filenames),3))

        images = []
        i = 0

        for filename in filenames:
            imarray = np.array(Image.open(folder + '/' + filename))
            images.append(imarray)
            mologram_label[i,0] = float(ord(str.split(filename,'-')[0]))
            mologram_label[i,1] = float(str.split(filename,'-')[1])
            mologram_label[i,2] = float(str.split(str.split(filename, '_')[0],'-')[2])

            i += 1


        ## sort images
        # sums mologram_labels: ex.: B,1,4 -> 14
        sort_index = np.zeros(len(mologram_label))
        for k in range(0,len(mologram_label)):
            sort_index [k] = int(
                            float(
                                str(mologram_label[k,1])[0]
                                + str(mologram_label[k,2])[0]
                            )
                         )

        # sorts image array
        arr1inds = sort_index.argsort()
        sorted_index = sort_index[arr1inds]
        images = np.array(images)
        sorted_images = images[arr1inds]
        sorted_mologram_label = mologram_label[arr1inds]

        self.images = sorted_images

        return sorted_images, sorted_mologram_label


    def onClick(self, event):
        if event.inaxes is not None:
            axis = event.inaxes.get_axes()
            try:
                self.main_widget.cameraWidget.cameraFeed.plotImage(self.images[axis.get_geometry()[2]-1])
            except:
                pass
        else:
            pass
    

class NavigationToolbar(NavigationToolbar2QT):

    def mouse_move(self, event):
        pass