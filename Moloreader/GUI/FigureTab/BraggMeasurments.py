from PyQt4 import QtGui

from GUI.Image_Acquisition_Tab.NoiseRecorder import NoiseRecorder
from GUI.Image_Acquisition_Tab.Stack import Stack
from GUI.Image_Acquisition_Tab.SaveImage import SaveImage
from GUI.Image_Acquisition_Tab.ArrayMeasurements import ArrayMeasurements

class BraggMeasurments(QtGui.QWidget):
	def __init__(self,main_widget):
		super(BraggMeasurments,self).__init__()

		self.main_widget = main_widget

		self.initUI()

	def initUI(self):
		
		self.saveImage = SaveImage(self.main_widget)
		self.stack = Stack(self.main_widget)
		self.noiseRecorder = NoiseRecorder(self.main_widget)
		self.arrayMeasurements = ArrayMeasurements(self.main_widget)

		self.tabs = QtGui.QTabWidget()
		self.tabs.addTab(self.saveImage, 'Save Image')
		self.tabs.addTab(self.arrayMeasurements, 'Array Measurements')
		self.tabs.addTab(self.stack, 'Stack')
		self.tabs.addTab(self.noiseRecorder, 'Noise Recorder')

		main_hbox  = QtGui.QHBoxLayout()
		main_hbox.addWidget(self.tabs)
		self.setLayout(main_hbox)

