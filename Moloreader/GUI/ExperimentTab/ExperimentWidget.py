from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui,QtCore
from Data_Acquisition.Experiment import Experiment
from GUI.MologramSelector import MologramSelector
from SharedLibraries.Database.dataBaseOperations import saveDB, loadDB, createDB, createSQLDict
from Data_Acquisition.permission import changePermissionsPath
import sqlite3
from PyQt4.QtGui import QMessageBox
import json
import numpy as np
from datetime import timedelta
import time, datetime
from settings import getVariables, getLogExperimentDict, getResultsDict


class ExperimentTab(QtGui.QWidget):
    def __init__(self,main_widget):
        super(ExperimentTab, self).__init__()

        self.main_widget   = main_widget
        self.experimentList = []
        self.experimentNo = 0
        self.experimentRunning = False
        
        self.initUI()


    def initUI(self):

        # declare widgets
        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Experiment Folder')
        # line edits and selectors
        self.editFolder = QtGui.QLineEdit()

        self.chipNo_lbl = QtGui.QLabel('Chip No')
        self.chipNo = QtGui.QLineEdit('R304')
        # push button
        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.browse_f)
        self.browse.resize(self.browse.sizeHint())

        # setup table
        self.horHeaders = ['Start time', 'Time', 'Type', 'Mologram', 'Settings']
        self.table = QtGui.QTableWidget()
        self.table.setColumnCount(len(self.horHeaders))
        self.table.setHorizontalHeaderLabels(self.horHeaders)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        self.table.cellClicked.connect(self.setCurrentCell)

        self.add = QtGui.QPushButton('Add')
        self.add.clicked.connect(self.addRow)
        self.delete = QtGui.QPushButton('Delete')
        self.delete.clicked.connect(self.deleteEntry)
        self.deleteAll = QtGui.QPushButton('Delete all')
        self.deleteAll.clicked.connect(self.deleteAllEntries)
        self.moveUp = QtGui.QPushButton('Move up')
        self.moveUp.clicked.connect(lambda: self.moveEntry('up'))
        self.moveDown = QtGui.QPushButton('Move down')
        self.moveDown.clicked.connect(lambda: self.moveEntry('down'))

        self.connectAutoSampler = QtGui.QCheckBox('Connect with autosampler')
        self.connectAutoSampler.setEnabled('autosampler' in dir(self.main_widget.setup)) # set enabled if connected

        self.start = QtGui.QPushButton('Start Experiment')
        self.start.clicked.connect(self.start_f)
        self.start.resize(self.start.sizeHint())
        self.stopMeasurement = QtGui.QPushButton('Stop Measurement')
        self.stopExperiment = QtGui.QPushButton('Stop Experiment')
        self.stopExperiment.clicked.connect(self.stopExperimentClicked)
        self.pauseMeasurement = QtGui.QPushButton('Pause/Continue Measurement')
        self.pauseExperiment = QtGui.QPushButton('Pause/Continue Experiment')
        self.stopExperiment.clicked.connect(self.pauseExperimentClicked)

        self.stop  = False
        self.pause = False

        self.textBrowser = QtGui.QTextEdit() 

        # layout
        hbox_folder   = QtGui.QHBoxLayout()
        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        hbox_table = QtGui.QHBoxLayout()
        vbox_table_buttons = QtGui.QVBoxLayout()
        vbox_table_buttons.addWidget(self.add)
        vbox_table_buttons.addWidget(self.delete)
        vbox_table_buttons.addWidget(self.deleteAll)
        vbox_table_buttons.addWidget(self.moveUp)
        vbox_table_buttons.addWidget(self.moveDown)

        hbox_table.addWidget(self.table)
        hbox_table.addLayout(vbox_table_buttons)

        hbox_stop = QtGui.QHBoxLayout()
        hbox_stop.addWidget(self.stopExperiment)
        hbox_stop.addWidget(self.stopMeasurement)

        hbox_pause = QtGui.QHBoxLayout()
        hbox_pause.addWidget(self.pauseExperiment)
        hbox_pause.addWidget(self.pauseMeasurement)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.editFolder_lbl, hbox_folder)
        fbox.addRow(hbox_table)
        fbox.addRow(self.connectAutoSampler)
        fbox.addRow(self.start)
        fbox.addRow(hbox_stop)
        fbox.addRow(hbox_pause)
        fbox.addRow(self.textBrowser)

        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(fbox)
        self.setLayout(hbox)


    def start_f(self):
        """
        Fetch all parameters for experiment from GUI and 
        launch the experiment in a seperate thread
        """
        
        variables = getVariables()

        # Thread for recording the Experiment
        experimentFolder = self.editFolder.text()
        if self.initExperiment() == -1: return

        rowNumber = self.table.rowCount()
        rows = []
        for row in range(rowNumber):
            rows.append(dict())
            for horHeader in self.horHeaders:
                rows[-1][horHeader] = self.table.item(row, self.horHeaders.index(horHeader)).text()

        if len(rows) != 0:
            self.experimentList = []
            for row in rows:
                settings = row['Settings'].split('\n')
                info = dict(row)
                for item in settings:
                    key, value = item.split(': ')
                    info[key] = value

                info['Experiment type'] = row['Type']
                info['Mologram'] = row['Mologram']

                log = self.createLogEntry(self.database_path, info)
                self.experimentList.append(log)

            self.experimentNo = 0
            self.startExperiment = time.time()
            self.experimentRunning = True
            self.continueExperiment()


    def continueExperiment(self):
        self.main_widget.stop_moving()

        # update time required
        if self.experimentNo > 0:
            item = QtGui.QTableWidgetItem(time.strftime('%H:%M:%S', time.gmtime(time.time()-self.startMeasurement)))
            self.table.setItem(self.experimentNo-1, 1, item)

        if len(self.experimentList) > self.experimentNo:
            if self.stop:
                self.statusMessage('Experiment aborted')
                return
            while self.pause: continue

            if self.connectAutoSampler.isChecked() and self.experimentNo != 1:
                while not self.main_widget.setup.autosampler.injectionTrigger():
                    if self.stop:
                        self.statusMessage('Experiment aborted')
                        return
                    else:
                        continue

                if self.main_widget.setup.autosampler.injectionTrigger():
                    print('Trigger received')
                    # time.sleep(120)


            experiment = self.experimentList[self.experimentNo]

            # update start time
            if self.experimentNo > 0:
                item = QtGui.QTableWidgetItem(time.strftime('%H:%M:%S', time.gmtime(time.time()-self.startExperiment)))
                self.table.setItem(self.experimentNo, 0, item)

            # select row
            self.table.selectRow(self.experimentNo)
            self.startMeasurement = time.time()

            # create image folder
            if experiment['experimentType'] != 'Go to position' \
                    and experiment['experimentType'] != 'focus' \
                    and experiment['experimentType'] != 'center':
                # create image folder
                imageFolder = 'ID {} ({})'.format(experiment.get('experimentID',None), experiment['experimentType'])
                image_path = '{}/{}'.format(self.experimentFolder,imageFolder)
                if not os.path.exists(image_path):
                    os.makedirs(image_path)

                    # add permission (rwxr-xr-x -> 111 101 101)
                    changePermissionsPath(image_path)

            if experiment['experimentType'] == 'Array measurement':
                # check molos
                self.main_widget.imageAquisitionTab.arrayMeasurements.checkMolos(experiment['mologram'])

                # connect status
                self.experiment.status.connect(self.main_widget.imageAquisitionTab.arrayMeasurements.highlightMolo)


            self.experimentNo += 1
            self.updateStatus('Start experiment {}: {}'.format(self.experimentNo, experiment['experimentType']))
            self.experiment.record_data(
                    experimentType = experiment['experimentType'],
                    imageFolder = locals().get('imageFolder', None),
                    
                    # array measurement
                    mologram = experiment.get('mologram',None),
                    measureSurface = experiment.get('measureSurface',None),
                    measureMologram = experiment.get('measureMologram',None),

                    # time stack
                    numCaptures = experiment.get('numCaptures',None),
                    numAvg = experiment.get('numAvg',None),
                    timeInterval = experiment.get('timeInterval',None),

                    # position stack
                    stackIncrement = experiment.get('stackIncrement',None),
                    startPosition = experiment.get('startPosition', None),
                    startExposure = experiment.get('startExposure', None),
                    startGain = experiment.get('startGain', None),

                    # database
                    ID = experiment.get('experimentID', None),
                    algorithm = experiment.get('algorithm',None),

                    # image acquisition
                    focus = experiment.get('focus',None),
                    center = experiment.get('center',None),
                    autoExposure = experiment.get('autoExposure',None),
                    adjustExposure = experiment.get('adjustExposure',None),
                    fixedExposure = experiment.get('fixedExposure',None),
                    saveImages = experiment.get('saveImages',None),
                    subtractPreviousImage = experiment.get('subtractPreviousImage',None)
                )

        else:
            self.experimentRunning = False



    """ Once experiment is done or if it stopped unexpectedly, show appropriate message box and update GUI """
    def done_experiment(self):

        # Indicate experiment is complete
        msg = QtGui.QMessageBox()
        msg.setWindowTitle("Experiment")
        msg.setIcon(QtGui.QMessageBox.Information)
        msg.setStandardButtons(QtGui.QMessageBox.Ok)
        msg.setText("Experiment is complete")
        msg.exec_()

        # Update all displays
        self.main_widget.updateAll()


    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '/home/focal-molography/Experiments')
        self.editFolder.setText(folder)
        self.main_widget.setExperimentFolder(folder)


    def addRow(self, row=[]):
        """
        Default: empty row
        """

        rowPosition = self.table.rowCount()
        self.table.insertRow(rowPosition)

        timeTot = timedelta()
        for i in range(rowPosition):
            t = datetime.datetime.strptime(self.table.item(i,1).text(),'%H:%M:%S')
            delta = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)
            timeTot += delta

        row.insert(0,timeTot)

        for item in row:
            self.table.setItem(rowPosition, row.index(item), QtGui.QTableWidgetItem(str(item)))

        # readjust row size
        for i in range(len(self.horHeaders)):
            if self.horHeaders[i] != 'Mologram':
                self.table.resizeColumnToContents(i)
        # self.table.resizeRowsToContents()


    def setCurrentCell(self, row, column):
        self.currentCell = row, column


    def deleteEntry(self):
        if 'currentCell' in dir(self):
            self.table.removeRow(self.currentCell[0])
            # delete variable in order to avoid second time delete button clicked
            del self.currentCell


    def deleteAllEntries(self):
        while (self.table.rowCount() > 0):
            self.table.removeRow(0)


    @QtCore.pyqtSlot(str)
    def updateStatus(self, status):
        self.textBrowser.append('[%s] %s' % (time.strftime("%X", time.gmtime()), status))


    def moveEntry(self, direction):
        row = self.table.currentRow()
        column = self.table.currentColumn()

        if direction == 'up' and row > 0:
            newRow = row-1
            deleteRow = row+1

        elif direction == 'down' and row < self.table.rowCount()-1:
            newRow = row+2
            deleteRow = row

        else:
            return

        # insert new row and copy entries
        self.table.insertRow(newRow)
        for i in range(self.table.columnCount()):
            self.table.setItem(newRow, i, self.table.takeItem(deleteRow,i))

        # delete old row
        self.table.setCurrentCell(newRow, column)
        self.table.removeRow(deleteRow)


    def createLogEntry(self, folder, info):

        self.log = dict()
        variables = getVariables()
        for variable, description in variables.items():
            if description in info:
                if type(info.get(variable)) == list:
                    self.log[variable] = str(info.get(description))
                else:
                    try:
                        self.log[variable] = eval(info.get(description))
                    except:
                        self.log[variable] = info.get(description)

        createDB(folder + '/data.db', 'logExperiment', getLogExperimentDict())

        self.log['date'] = str(datetime.date.today())

        saveDB(folder + '/data.db', 'logExperiment', self.log)
        changePermissionsPath(folder + '/data.db')

        createDB(folder + '/data.db', 'results', getResultsDict())

        log = loadDB(folder + '/data.db', 'logExperiment')
        if type(log) != list:
            log = [[]]

        self.log['experimentID'] = len(log)

        return self.log


    def getTableValues(self):
        table = []
        table.append(self.horHeaders)
        table[-1].insert(0,'')
        for row in np.arange(self.table.rowCount()):
            table.append([self.verHeaders[row]])
            for column in np.arange(len(self.horHeaders)-1):
                try:
                    table[row+1].append(str(self.table.item(row,column).text()))
                except:
                    table[row+1].append('')

        return table


    def initExperiment(self):
        """
        create folders and define thread
        """

        # Determine where to store results
        self.experimentFolder = self.editFolder.text()
        self.database_path = self.experimentFolder + '/database'

        # Confirm user input and delete experiment information if it is already there.
        if self.confirm_user_input(self.experimentFolder) == -1:
            return -1

        # Create file with experiment parameters for easier analysis later on
        self.main_widget.setExperimentFolder(self.experimentFolder)

        # Create directories if non-exgaussian filteristent
        # definde directories
        directories = [self.experimentFolder, self.database_path]

        for directory in directories:
            if not os.path.exists(directory):
                os.makedirs(directory)

                # add permission (rwxr-xr-x -> 111 101 101)
                changePermissionsPath(directory)


        self.experiment = Experiment(self.main_widget, self.experimentFolder)

        self.connect(
            self.experiment, 
            QtCore.SIGNAL("database_updated"), 
            lambda: self.main_widget.updateImg(Stack=True)
        )

        self.experiment.statusMessage.connect(self.updateStatus)

        self.connect(
            self.experiment, 
            QtCore.SIGNAL("done_experiment"), 
            self.continueExperiment
        )

        self.stopMeasurement.clicked.connect(self.experiment.stop)
        self.pauseMeasurement.clicked.connect(self.experiment.pause)


    def stopExperimentClicked(self):
        if self.experimentRunning:
            # change only if experiment running
            self.stop = True


    def pauseExperimentClicked(self):
        if self.experimentRunning:
            # change only if experiment running
            self.pause = not self.pause


    def confirm_user_input(self, experimentFolder):
        # Test for incorrect user input
        if experimentFolder == '':
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Folder name")
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setStandardButtons(QtGui.QMessageBox.Ok)
            msg.setText("Please select a valid experiment folder")
            msg.exec_()
            return -1

        return 0