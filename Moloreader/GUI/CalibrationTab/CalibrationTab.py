from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui, QtCore
import json
from GUI.MologramSelector import MologramSelector
from SharedLibraries.Database.dataBaseOperations import createDB, saveDB, loadDB, createSQLDict
import numpy as np
import time


class CalibrationTab(QtGui.QWidget):
    def __init__(self,main_widget):
        super(CalibrationTab,self).__init__()

        self.main_widget = main_widget
        self.camera = self.main_widget.setup.camera

        self.initUI()

    def initUI(self):
        reference_positions = self.main_widget.setup.motors.read_starting_positions()


        # define different calibration motors
        self.calibrate_axis = dict()

        hbox = list()
        for key, value in reference_positions.items():
            hbox.append(QtGui.QHBoxLayout())
            self.calibrate_axis[key] = QtGui.QCheckBox('Calibrate {}, Reference: {:.4f} mm'.format(key, value))
            self.calibrate_axis[key].setChecked(True)   # check all
            hbox[-1].addWidget(self.calibrate_axis[key])

        # Define button
        self.calibrate = QtGui.QPushButton()
        self.calibrate.setText('Calibrate')
        self.calibrate.clicked.connect(lambda: self.onCalibrateClicked(goToReference = True))

        self.goToSwitchButton = QtGui.QPushButton('Go to switch')
        self.goToSwitchButton.clicked.connect(lambda: self.onCalibrateClicked(goToReference = False))


        self.currentPositionButton = QtGui.QPushButton('Current Position')
        self.currentPositionButton.clicked.connect(self.getCurrentPosition)
        self.currentPosition = QtGui.QLabel('')

        self.gotoSurface = QtGui.QPushButton('Find surface')
        self.gotoSurface.clicked.connect(self.findSurface)

        self.adjustMS_button = QtGui.QPushButton('Adjust MS')
        self.adjustMS_button.setToolTip('Focus to a mologram in the first mololine')
        self.adjustMS_button.clicked.connect(self.adjustMS)

        self.savePosition = QtGui.QPushButton('Save position as B-1-1 surface (permanent)')
        self.savePosition.clicked.connect(self.onPermRef)

        # Layout
        fbox = QtGui.QFormLayout()
        for hbox_item in hbox:
            fbox.addRow(hbox_item)

        fbox.addRow(self.calibrate)
        fbox.addRow(self.goToSwitchButton)
        fbox.addRow(self.currentPositionButton, self.currentPosition)

        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        vbox.addLayout(fbox)
        vbox.addWidget(self.gotoSurface)
        vbox.addWidget(self.adjustMS_button)
        vbox.addWidget(self.savePosition)


    def getCurrentPosition(self):
        position = self.main_widget.getPosition()
        self.currentPosition.setText(str(position))


    def updateRefs(self):
        """
        updates the reference positions
        """

        reference_positions = self.main_widget.setup.motors.read_starting_positions()
        for key, value in reference_positions.items():
            self.calibrate_axis[key].setText('Calibrate {}, Reference: {:.4f} mm'.format(key, value))


    def onCalibrateClicked(self, goToReference=True):
        """
        Adds checked axes to a string which is then sent to calibration function.
        """
        axis = ''
        for key, item in self.calibrate_axis.items():
            if item.isChecked():
                axis += key

        self.main_widget.calib(axis, goToReference)


    def onPermRef(self, popup = True):
        """
        Executed when reference button is pressed. Sets axis 
        offset to current position
        """

        positions = self.main_widget.getPosition()
        print ('Save reference position ' + str(positions))


        # write to txt file
        file = open('starting_positions.txt','w')
        json.dump(positions, file)
        file.close()

        # write to database
        createDB('startingPositions.db', self.main_widget.setup.chip.mask, createSQLDict(positions))
        saveDB('startingPositions.db', self.main_widget.setup.chip.mask, positions)

        # reset the positions all to zero in the main widget.
        self.main_widget.setPosToZero()
        self.main_widget.updatePos()
        self.main_widget.manualControlTab.savedPositions.onSetSurfaceHeightClicked()

        self.updateRefs()
        self.main_widget.manualControlTab.axisControl.uploadReference()
        self.main_widget.manualControlTab.savedPositions.setPosition('B,1,1')


    def findSurface(self):
        pass

        # needs restrictions for the z-motor !
        
        # print('>> Set camera exposure: ' + str(self.camera.maxExposure))
        # self.camera.set_exposure(self.camera.maxExposure)

        # # move to surface
        # print('>> Move to surface height')
        # self.main_widget.manualControlTab.savedPositions.onGoToSurfaceHeightClicked()

        # time.sleep(1)
        
        # # coarse focus
        # print('>> Coarse focus')
        # self.main_widget.cameraWidget.cameraFeed.focusAndOptimizeIncoupling(numberOfSteps=20, stepSize=10)
        
        # # fine focus
        # print('>> Fine focus')
        # image, moloIntensity = self.main_widget.cameraWidget.cameraFeed.focusAndOptimizeIncoupling(numberOfSteps=20, stepSize=1)

        # # set focus height
        # self.main_widget.manualControlTab.savedPositions.onSetSurfaceHeightClicked()

        # # show image
        # self.main_widget.cameraWidget.cameraFeed.plotImage(image)


    def adjustMS(self):
        pass