# -*- coding: utf-8 -*-
from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui, QtCore
from PIL import Image

import os

from Data_Processing.speckles import findSpeckles
from scipy.ndimage.filters import gaussian_filter, median_filter

# matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.image as mpimg

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib import colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import Data_Processing.MologramProcessing as mologram_processing

from Data_Processing.imageAnalysisBragg import ImageAnalysisBragg
from Data_Acquisition.permission import changePermissionsPath

import auxiliariesMoloreader.constants as constants
from SharedLibraries.Database.dataBaseOperations import saveDB, loadDB, createDB, createSQLDict
from settings import getVariables, getLogExperimentDict, getResultsDict

import json
import time, datetime


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


# class CameraThread(QtCore.QThread):
#     """Wrapper object that the camera and the photodiode can function independently."""

#     def __init__(self,camera):

#         self.camera = camera

#         self.acquireImage = False

#         #self.start()

#         super(CameraThread,self).__init__()

#     def getImage(self,labjack = None):

#         self.labjack = labjack

#         self.image, self.retval = self.camera.get_raw_img(labjack = self.labjack)

#         return self.image,self.retval

#     def run(self):

#         while self.acquireImage:

#             self.image, self.retval = self.camera.get_raw_img(labjack = self.labjack)

#             self.acquireImage = False

#     def __del__(self):

#         self.wait()


class CameraFeed(QtGui.QWidget):
    # Constructor
    def __init__(self,main_widget, cameraControl):

        # Call parent class constructor
        super(CameraFeed, self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        self.show_scale_bar = True
        self.camera = self.main_widget.setup.camera
        self.cameraControl = cameraControl
        self.labJack = self.main_widget.photodiodeTab.labJack # get the labjack module for the photodiode referencing.
        self.dampingCurrentFieldValue = 0
        
        # self.camThread = CameraThread(self.camera)

        self.resetSettings()
        #self.loadBackgrounds()
        self.substractBackgroundImages = False
        self.lowSignalDetection = False
        self.referenceMologram = False
        self.imageName = ''

        # Call child constructor
        self.initUI()

        print('> Initializing image')
        self.initImage()

    """ Initialization of UI """
    def initUI(self):

        self.units = ['binary','physical'] # units in which image is shown
        self.unit = 0

        self.figure  = plt.figure(figsize=(10,10),facecolor='None',edgecolor='None')
        self.canvas  = FigureCanvas(self.figure)

        # create camera feed action
        cameraFeedAction = QtGui.QAction(QtGui.QIcon('./images/camera.png'), 'Update image', self)
        cameraFeedAction.setShortcut('F5')
        cameraFeedAction.setStatusTip('Update image (F5)')
        cameraFeedAction.setToolTip('Update image (F5)')
        cameraFeedAction.triggered.connect(self.updateCameraFeed)

        quicksave = QtGui.QAction(QtGui.QIcon('./images/save.png'), 'Quick save', self)
        quicksave.setShortcut('Space')
        quicksave.setStatusTip('Quick save (Sace)')
        quicksave.setToolTip('Quick save (Space)')
        quicksave.triggered.connect(lambda: self.saveImageAndDataQuickSave(cropImage = 'QuickSave'))

        focus = QtGui.QAction(QtGui.QIcon('./images/focus.png'), 'Focus', self)
        focus.setShortcut('F1')
        focus.setStatusTip('Fine focus (F1)')
        focus.setToolTip('Fine focus (F1)')
        focus.triggered.connect(lambda: self.focusAndOptimizeIncoupling(numberOfSteps=20, stepSize=1))

        self.convertUnitsAction = QtGui.QAction(QtGui.QIcon('./images/convert.png'), 'Convert units', self)
        self.convertUnitsAction.setStatusTip('Convert units')
        self.convertUnitsAction.setToolTip('Convert units to {} units'.format(self.units[not self.unit]))
        self.convertUnitsAction.setCheckable(True)
        self.convertUnitsAction.triggered.connect(self.convertUnits)

        self.toolbarNavigation = NavigationToolbar(self.canvas,self)
        self.toolbar = QtGui.QToolBar()
        self.toolbar.addWidget(self.toolbarNavigation)
        self.toolbar.addAction(self.convertUnitsAction)
        self.toolbar.addAction(quicksave)
        self.toolbar.addAction(focus)
        self.toolbar.addAction(cameraFeedAction)

        self.title = QtGui.QLabel('Parameters in {} units'.format(self.units[self.unit]))
        boldFont = QtGui.QFont()
        boldFont.setBold(True)
        self.title.setFont(boldFont)
        self.signalIntensity_lbl = QtGui.QLabel('Signal intensity')
        self.signalIntensityDisp = QtGui.QLabel('')
        self.referenceIntensity_lbl = QtGui.QLabel('Reference Signal intensity')
        self.referenceIntensityDisp = QtGui.QLabel('')
        self.img_int_lbl = QtGui.QLabel('Image mean')
        self.img_intDisp = QtGui.QLabel('')
        self.PowerOutcoupling_lbl = QtGui.QLabel('PowerOutcoupling (W)')
        self.PowerOutcoupling = QtGui.QLabel('')
        self.bg_int_lbl = QtGui.QLabel('Background mean / std')
        self.bg_intDisp = QtGui.QLabel('')
        self.SNR_lbl = QtGui.QLabel('SNR')
        self.SNRDisp = QtGui.QLabel('')
        self.max_pixel_lbl = QtGui.QLabel('Max / Min. pixel image')
        self.max_pixel = QtGui.QLabel('')
        self.dampingCurrentField_lbl = QtGui.QLabel('Waveguide Damping Nan dB/cm')
        self.dampingCurrentField = QtGui.QLabel('')

        # change color normalization
        self.colorNormalization_lbl = QtGui.QLabel('Color power norm')
        self.colorNormalization = QtGui.QComboBox()
        self.colorNormalization.addItem('1')
        self.colorNormalization.addItem('0.5')
        self.colorNormalization.addItem('0.25')
        self.colorNormalization.setCurrentIndex(1)
        self.colorNormalization.activated.connect(self.changeNormalization)

        self.adjustMaximum_lbl = QtGui.QLabel('')
        self.setMaximumColor = QtGui.QCheckBox('Adjust maximum')
        self.setMaximumColor.clicked.connect(self.changeNormalization)


        # Set layout
        vbox = QtGui.QVBoxLayout()
        vbox2 = QtGui.QVBoxLayout()

        hbox_color_normalization = QtGui.QHBoxLayout()
        hbox_color_normalization.addWidget(self.colorNormalization_lbl)
        hbox_color_normalization.addWidget(self.colorNormalization)
        hbox_color_max = QtGui.QHBoxLayout()
        hbox_color_max.addWidget(self.adjustMaximum_lbl)
        hbox_color_max.addWidget(self.setMaximumColor)

        vbox2.addWidget(self.toolbar)

        vbox2.addLayout(hbox_color_normalization)
        vbox2.addLayout(hbox_color_max)
        vbox2.addWidget(self.canvas)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.title)
        fbox.addRow(self.signalIntensity_lbl, self.signalIntensityDisp)
        fbox.addRow(self.referenceIntensity_lbl, self.referenceIntensityDisp)
        fbox.addRow(self.img_int_lbl,self.img_intDisp)
        fbox.addRow(self.PowerOutcoupling_lbl,self.PowerOutcoupling)
        fbox.addRow(self.bg_int_lbl, self.bg_intDisp)
        fbox.addRow(self.SNR_lbl, self.SNRDisp)
        fbox.addRow(self.max_pixel_lbl, self.max_pixel)
        fbox.addRow(self.dampingCurrentField_lbl,self.dampingCurrentField)
        fbox.setHorizontalSpacing(100)

        vbox.addLayout(vbox2)
        vbox.addLayout(fbox)

        self.setLayout(vbox)

        cid = self.figure.canvas.mpl_connect('button_release_event', self.onRelease)
        cid = self.figure.canvas.mpl_connect('button_press_event', self.onClick)
        cid = self.figure.canvas.mpl_connect('draw_event', self.onReDraw)

    def initImage(self):
        if not self.main_widget.offline_mode:
            # acquire image from camera
            self.img, retval = self.getImage()
            if retval == 0:
                # load an image instead of capture image
                self.img = mpimg.imread('./images/schematic.png')

        else:
            self.img = mpimg.imread('./images/schematic.png')


        # clear and redeclare axes
        plt.cla()
        self.ax = self.figure.add_subplot(111)

        # set up axes
        self.ax.xaxis.set_ticks_position('none')   # tick markers
        self.ax.yaxis.set_ticks_position('none')
        plt.xticks([])                        # labels
        plt.yticks([])
        self.im = plt.imshow(self.img, cmap=plt.cm.inferno,
            vmin=0, vmax=4095, interpolation='none',
            norm=colors.PowerNorm(gamma=1./2.))

        # add colorbar
        ax = plt.gca()
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="2%", pad=0.1)
        cbar = plt.colorbar(self.im, cax = cax1, ticks=np.linspace(0,self.colorDepth,4), format='%.e')
        cbar.ax.tick_params(labelsize=10)

        self.figure.tight_layout()

        # show image
        self.canvas.draw()

    def updateCameraFeed(self):
        """Function that updates the cameraFeed tab"""

        # reset image settings
        self.resetImage()

        # capture image
        image,retval = self.getImage()

        if self.main_widget.cameraWidget.cameraControl.subtractImage.isChecked() == True:
            # subtract image if set true
            self.img = self.subtractImage(image)

        self.PowerOutcoupling.setText(str(self.labJack.PowerOutcoupledMean))

        if retval == 0: return

        self.lowSignalSearch = self.cameraControl.lowSignalSearch.isChecked()
        self.referenceMologram = self.cameraControl.referenceMologram.isChecked()

        self.plotImage()

        return image

    def plotImage(self, image= None):
        # should be something like setImage

        if 'speckles' in dir(self):
            self.ax.lines[-1].remove()
            del self.speckles
            print('Speckles removed')

        # plot in specified units

        if image is not None:
            if self.convertUnitsAction.isChecked():
                image = self.convertPhysical(image)

            self.img = image

        # set image data
        self.im.set_data(self.img)

        self.processImage()

        # update values
        self.updateValues()

        # add scalebar
        self.scale_bar()

        # add the molocircle to more easily visualize the mologram.
        if self.main_widget.cameraWidget.cameraControl.highlightMaximum.isChecked():
            self.molo_circle()

        if self.main_widget.cameraWidget.cameraControl.braggMask.isChecked():
            self.braggMask()

        self.changeNormalization()

        # update the image

        try:
            self.canvas.draw()
            return self.img

        except:
            print('Could not draw image')
            return

    def updateValues(self):

        # update text labels
        if self.units[self.unit] == 'binary':

            self.signalIntensityDisp.setText(str(self.signalIntensity))
            self.referenceIntensityDisp.setText(str(self.referenceIntensity))
            self.img_intDisp.setText(str(self.img_mean))
            self.bg_intDisp.setText('{} / {}'.format(self.bkg_mean, self.bkg_std))
            self.SNRDisp.setText(str(self.SNR))
            self.max_pixel.setText('{} ({}) / {}'.format(np.max(self.img),self.noMaxPixel, np.min(self.img)))
            self.dampingCurrentField.setText('{} dB/cm'.format(self.dampingCurrentFieldValue))

        else:
            self.signalIntensityDisp.setText(str(self.signalIntensityPhys))
            self.referenceIntensityDisp.setText(str(self.referenceIntensityPhys))
            self.img_intDisp.setText(str(self.img_meanPhys))
            self.bg_intDisp.setText('{} / {}'.format(self.bkg_meanPhys, self.bkg_stdPhys))
            self.SNRDisp.setText(str(self.SNRPhys))
            self.max_pixel.setText('{} ({}) / {}'.format(np.max(self.img),self.noMaxPixel, np.min(self.img)))

    def processImage(self):

        converted = False

        if self.units[self.unit] == 'physical':
            print("converted")
            self.convertUnitsImage()
            converted = True

        results = mologram_processing.calc_datapoint_entry(
                self.img,
                algorithm = self.algorithm,
                pixelsize = self.pixelsize,
                Airy_disk_radius = self.Airy_disk_radius,
                lowSignalSearch = self.lowSignalSearch,
                referenceMologram = self.referenceMologram
            )

        signalIntensity, bkg_mean, bkg_std, SNR, referenceIntensity = results

        self.signalIntensity = signalIntensity
        self.bkg_mean = bkg_mean
        self.bkg_std = bkg_std
        self.SNR = SNR
        self.referenceIntensity = referenceIntensity
        self.img_mean = np.mean(self.img)
        self.maxsignal = np.max(self.img)

        self.noMaxPixel = np.sum(self.img == np.max(self.img))

        self.convertUnitsImage()

        results = mologram_processing.calc_datapoint_entry(
                self.img,
                algorithm = self.algorithm,
                pixelsize = self.pixelsize,
                Airy_disk_radius = self.Airy_disk_radius,
                lowSignalSearch = self.lowSignalSearch,
                referenceMologram = self.referenceMologram
            )

        signalIntensityPhys, bkg_meanPhys, bkg_stdPhys, SNRPhys, referenceIntensityPhys = results

        self.signalIntensityPhys = signalIntensityPhys
        self.bkg_meanPhys = bkg_meanPhys
        self.bkg_stdPhys = bkg_stdPhys
        self.SNRPhys = SNRPhys
        self.referenceIntensityPhys = referenceIntensityPhys
        self.img_meanPhys = np.mean(self.img)
        self.maxsignalPhys = np.max(self.img)


        # if the units were changed at the beginning no need to change back, otherwise convert back.
        if not converted:
            self.convertUnitsImage()

    def getImage(self,startTime = 0):
        """gets a new image and saves it in the variable self. img."""

        self.attempts = 0

        image,retval = self.camera.get_raw_img(labjack = self.labJack)

        # if could not capture image
        while retval == False:
            self.attempts += 1
            if self.attempts == 100:
                print('Reconnect camera')
                time.sleep(2)
                self.main_widget.settingsTab.devicesSettings.reconnectCamera()

            if self.attempts > 1000:
                print('Unable to capture image')
                return 0, 0

            image, retval = self.camera.get_raw_img(labjack = self.labJack)

        if self.substractBackgroundImages:
            if 'backgroundImages' in dir(self):
                if '%.1f' % self.camera.exposure in self.backgroundImages.keys():
                    self.backgroundImage = self.backgroundImages['%.1f' % self.camera.exposure].astype(np.int16)
                else:
                    self.backgroundImage = 0
                    print('No background given for this exposure time')


            image = np.abs(image.astype(np.int16) - self.backgroundImage)
            print('background subtracted')

        self.acquisitionTime = time.time() - startTime
        self.img = image

        return image, retval

    def subtractImage(self, image):
        """
        substracts image from the previous one
        """

        if 'oldImage' in dir(self):
            # if an older image exists
            imageOld = self.oldImage

        elif 'img' in dir(self):
            # if an older image exists (only for the first time)
            imageOld = self.img

        else:
            return image

        imageDifference = np.abs(image.astype(np.int16) - imageOld.astype(np.int16))
        self.oldImage = image

        return imageDifference


    def subtractBackground(self, backgroundImage):
        self.oldImage = backgroundImage
        img = self.subtractImage(self.img)
        self.plotImage(img)



    def _binarySearchExposureTime(self, lowerBoundExposureTime=None, upperBoundExposureTime=None,percentageOfMaximumColorDepthUpper = 0.9,percentageOfMaximumColorDepthLower = 0.8):
        """Adjusts the exposure time with the binary search algorithm, we have upper and lower bounds here.

        :param: percentageOfMaximumColorDepthUpper, at what range of the fullwell capacity the maximum pixel should be upper 0.9
        :param: percentageOfMaximumColorDepthLower, at what range of the fullwell capacity the maximum pixel should be lower 0.7
        Hence, the maximum pixel of the image will be in this interval.
        """
        if lowerBoundExposureTime is None:
            # if lowerBoundExposureTime and upperBoundExposureTime not defined, define as camera range
            lowerBoundExposureTime = self.minExposure
            upperBoundExposureTime = self.maxExposure


        # adjust the camera exposure to the middle of the two bounds given.
        exposureTime = np.round((lowerBoundExposureTime + upperBoundExposureTime)/2.,decimals=3)
        print(exposureTime)
        self.camera.set_exposure(exposureTime)

        # get the image directly from the camera class, since we do not want to acquire the photodiode data as well.
        self.img, retval = self.camera.get_raw_img()

        if self.counter > self.max_iterations:
            
            print('Adjusted Exposure binary Search')
            print(exposureTime)
            print(np.max(self.img))

            return exposureTime

        self.counter += 1 # in order not to get caught in an infinite loop. 

        # if there are more than one saturated pixels and the camera's exposure time can
        # still be lowered, then continue search in the lower half interval
        if (np.sum(self.img > self.colorDepth*percentageOfMaximumColorDepthUpper) > 1) \
                and (self.camera.exposure > self.minExposure):
            # decrease exposure time
            # print ('adjust exposure downwards')
            # print('exposure time: ' + str(exposureTime))
            # print('max pixel: ' + str(np.max(self.img)))
            # print('max pixels: ' + str(np.sum(self.img > self.colorDepth*percentageOfMaximumColorDepthUpper)))
            return self._binarySearchExposureTime(lowerBoundExposureTime, exposureTime,percentageOfMaximumColorDepthUpper,percentageOfMaximumColorDepthLower)

        # if there are no saturated pixels and the camera's exposure time can
        # still be increased, then continue search in the upper half interval
        elif (np.max(self.img) > self.colorDepth*percentageOfMaximumColorDepthLower) \
                and (self.camera.exposure < self.maxExposure):
            # print ('adjust exposure upwards')
            # print('exposure time: ' + str(exposureTime))
            # print('max pixel: ' + str(np.max(self.img)))
            # print("hello")
            # print(np.max(self.img))
            # print(upperBoundExposureTime)
            # print(lowerBoundExposureTime)
            # print(exposureTime)

            return self._binarySearchExposureTime(exposureTime, upperBoundExposureTime,percentageOfMaximumColorDepthUpper,percentageOfMaximumColorDepthLower)

        # if the maximum pixel is in the specified interval, then stop.
        else:
            print('Adjusted Exposure binary Search')
            print(exposureTime)
            print(np.max(self.img))

            return exposureTime


    def axisCoarseAdjustments(self,axis,numberOfSteps=10, stepSize=1):

        self.main_widget.set_in_movement(True)

        # positive movement, move up
        print('move up coarse')
        print(numberOfSteps)
        print(stepSize)
        molo_intensity_up, index_up,steps_moved_up = self.moveAndFindMax(axis, stepSize, numberOfSteps)

        # go to starting position.
        print('move back coarse' + str(-stepSize*(steps_moved_up)) + ' steps')
        axis.move_steps_safe(-stepSize*(steps_moved_up))
        print(molo_intensity_up, steps_moved_up)

        # negative movement
        print('move down coarse')
        molo_intensity_down, index_down,steps_moved_down = self.moveAndFindMax(axis, -stepSize, numberOfSteps)

        if molo_intensity_up > molo_intensity_down:

            print('intensity upper move back coarse' + str(stepSize*(steps_moved_down+index_up)) + ' steps')
            axis.move_steps_safe(stepSize*(steps_moved_down + index_up))

            max_molointensity = molo_intensity_up

        else:
            print('intensity lower move back coarse' + str(stepSize*(steps_moved_down-index_down)) + ' steps')
            axis.move_steps_safe(stepSize*(steps_moved_down-index_down))

            max_molointensity = molo_intensity_down

        self.main_widget.updatePos()
        self.main_widget.set_in_movement(False)


    def axisFineAdjustments(self,axis,numberOfSteps=10, stepSize=1):
        """Due to the play of the motors, we need to approach the maximum from below and when it is 90% of the maximum value we had, we need to stop."""


        self.main_widget.set_in_movement(True)  

        # first move 10 steps down
        print('moved' + str(numberOfSteps) + ' steps down')
        axis.move_steps_safe(-numberOfSteps/2)

        # positive movement, move up find the maximum on the curve.
        print('move up fine')
        molo_intensity_up, index_up,steps_moved_up = self.moveAndFindMax(axis, stepSize, numberOfSteps,decreaseMax=numberOfSteps)
        # go to starting position.
        print('move back fine' + str(-stepSize*(steps_moved_up)) + ' steps')
        axis.move_steps_safe(-stepSize*(steps_moved_up))
        print(molo_intensity_up, index_up)

        self.getProcessImageCheckSaturation()

        # start moving up in individual steps until you find a value that is at least 95% as high as the value that you had originally.
        i = 0
        while i < numberOfSteps:
            axis.move_steps_safe(1)
            self.getProcessImageCheckSaturation()
            print(self.signalIntensityPhys)
            print(molo_intensity_up)

            if self.signalIntensityPhys > 0.9*molo_intensity_up:
                break

        self.main_widget.updatePos()
        self.main_widget.set_in_movement(False)


    def saturatedPixelsInImage(self):

        if np.max(self.img) == self.colorDepth:
            return True
        else:
            return False


    def adjustExposure(self,algorithm = 'binary search',percent = 0.9):

        if algorithm == 'binary search':
            self.counter = 0
            self.max_iterations = 20
            # the exposure time adjustement in this way is stupid and can get stuck
            exposureTime = self._binarySearchExposureTime()

        if algorithm == 'percent':
            exposureTime = self.main_widget.setup.camera.exposure
            self.camera.set_exposure(exposureTime*percent)

        return exposureTime


    def coarseFocusZ(self,numberOfSteps=20, stepSize=3):

        self.adjustExposure()

        axis_z = self.main_widget.setup.motors.z

        self.axisCoarseAdjustments(axis_z,numberOfSteps=numberOfSteps, stepSize=stepSize)

        self.getImage()
        self.processImage()


    def fineFocusZ(self,numberOfSteps=20, stepSize=1):

        print('Started Exposure adjustement')
        self.adjustExposure()

        axis_z = self.main_widget.setup.motors.z

        self.axisFineAdjustments(axis_z,numberOfSteps=numberOfSteps, stepSize=stepSize)

        self.getImage()
        self.processImage()


    def adjustCoupling(self):
        """Very fast adjustment of the incoupling."""

        startExposureTime = self.camera.exposure

        # increase pixelclock and exposure, in order to be very fast...
        # self.camera.pixelclock = self.camera.specifications.pixelClockRange[1]
        # self.camera.exposure = self.camera.specifications.maxExposure
        self.camera.set_pixel_clock(104)
        self.camera.set_exposure(79., False)
        print('>> Set camera exposure: ' + str(self.camera.exposure))
        time.sleep(0.5)

        # initialize
        self.motor = self.main_widget.setup.motors.TE

        image, retval = self.camera.get_raw_img()
        startValue = np.mean(image)+np.std(image)

        direction = 1
        maxFound = False

        # set in movement
        self.main_widget.set_in_movement(True)

        # find TE
        axis_TE = self.main_widget.setup.motors.TE

        stepSize = 10
        numberOfSteps = 15
        axis_TE.move_steps_fast(int(-stepSize*numberOfSteps/2))
        print('move back ' + str(int(-stepSize*numberOfSteps/2)) + ' steps')
        index = self._moveAndFindMaxFast(axis_TE, stepSize, numberOfSteps)
        print('move back ' + str(int(-stepSize*(numberOfSteps-index))) + ' steps')
        axis_TE.move_steps_fast(int(-stepSize*(numberOfSteps-index)))

        time.sleep(0.5)
        # fine
        stepSize = 1
        numberOfSteps = 15
        axis_TE.move_steps_fast(int(-stepSize*numberOfSteps/2))
        print('move back ' + str(int(-stepSize*numberOfSteps/2)) + ' steps')
        index = self._moveAndFindMaxFast(axis_TE, stepSize, numberOfSteps)
        print('move back ' + str(int(-stepSize*(numberOfSteps-index))) + ' steps')
        axis_TE.move_steps_fast(int(-stepSize*(numberOfSteps-index)))

        # update
        self.camera.set_exposure(startExposureTime)    # reset camera exposure time
        self.main_widget.set_in_movement(False)
        self.main_widget.updateAll()


    def _moveAndFindMaxFast(self, axis, stepSize, steps, highestBackground=0):
        index = 0
        for i in np.arange(steps)+1:
            axis.move_steps_safe(stepSize)
            image, retval = self.camera.get_raw_img()
            background = np.mean(image)+np.std(image)
            print("moved " + str(stepSize) + " steps: " + str(background))

            if background > highestBackground:
                highestBackground = background
                index = i

        # return best image with the measured molo intensity and its index
        return index

    def optimizeIncoupling(self,numberOfStepsCoarse=10, stepSizeCoarse=1):
        """Optimizes the incoupling if the mologram, the background argument is if one is focusing on the surface. """

        exposureTime = self.main_widget.setup.camera.exposure
       # find TE
        axis_TE = self.main_widget.setup.motors.TE

        if background:
            self.camera.pixelclock = self.camera.specifications.pixelClockRange[1]
            self.camera.exposure = self.camera.specifications.maxExposure
            print('>> Set camera exposure: ' + str(self.camera.exposure))
            time.sleep(0.5)
        # positive movement
        self.axisCoarseAdjustments(axis_TE,numberOfStepsCoarse=10, stepSizeCoarse=3)
        self.axisFineAdjustments(axis_TE,numberOfStepsCoarse=20, stepSizeCoarse=1)

        self.camera.set_exposure(exposureTime)

        self.getImage()
        self.processImage()

    def getProcessImageCheckSaturation(self):

        self.getImage()
        # Check for saturation:
        if self.saturatedPixelsInImage():
            self.adjustExposure(algorithm='percent',percent = 0.2) # to use the percentage algorithm might be too conservative here.
            time.sleep(0.5) # wait for the stupid camera...
            self.getImage()
        self.processImage()


    def moveAndFindMax(self, axis, stepSize, steps, decreaseMax = 4):
        """
        """

        # find maximal focal point
        decreased = 0    # number of steps in a row that decreased
        molo_intensity_old = 0 # start value is zero.
        index_maximum = 0
        offset = 0
        i = 0
        highestIntensity = 0

        while i < steps+1:

            # get an image at the current location.

            self.getProcessImageCheckSaturation()
            molo_intensity = self.signalIntensityPhys


            print("moved " + str(stepSize) + " steps: " + str((self.signalIntensityPhys, self.bkg_meanPhys, self.bkg_stdPhys, self.SNRPhys)))

            if molo_intensity < molo_intensity_old:
                decreased += 1

            if molo_intensity == 0 and highestIntensity > 0 or decreased >= decreaseMax:
                # if molo intensity is 0 but there were images with higher molo intensities
                print("stop moving in this direction")

                return highestIntensity,index_maximum,i

            if molo_intensity > highestIntensity:
                highestIntensity = self.signalIntensityPhys
                decreased = 0
                index_maximum = i + offset # index_maximum of the maximum

            # set old mologram intensity
            molo_intensity_old = self.signalIntensityPhys
            i += 1
            if i == steps and decreased == 0 and molo_intensity != 0:
                # if still increasing, the range is increased.
                offset += i
                i = 0

            # move to next location
            axis.move_steps_safe(stepSize)


        # return best image with the measured molo intensity and its index_maximum
        return highestIntensity, index_maximum,steps

    def centerImage(self, image=None,NoThread = False):
        if image is None:
            image = self.img

        signalIntensity, bkg_mean, bkg_std, SNR,referenceIntensity = mologram_processing.calc_IntAiryDisk(image, lowSignalSearch=self.lowSignalDetection,referenceMologram = self.referenceMologram)

        if signalIntensity != 0:
            image = np.copy(image)

            # calculate the maximum of the image, which must be the molographic center image.
            image = image.astype('float')
            y, x = np.unravel_index(image.argmax(),image.shape)
            # flipping y with respect to x axis (middle of the image)
            y = np.shape(image)[0]-y
            self.click_location = (x,y)
            print('Go to position ' + str(self.click_location))

            if NoThread:
                xdata = x
                ydata = y
                x_pixels = self.npix[0]
                y_pixels = self.npix[1]

                # get position of mouse click and return if it was outside figure
                # try:
                x = (xdata-x_pixels/2)*self.pixelsize/1000 + self.main_widget.setup.motors.x.pos('mm')
                y = -(ydata-y_pixels/2)*self.pixelsize/1000 + self.main_widget.setup.motors.y.pos('mm')
                z = self.main_widget.setup.motors.z.pos('mm')

                self.main_widget.move_to_nothread([x,y,z])
            else:
                self.goToPosition(x,y)


        else:
            print('No focalpoint found')

        self.updateCameraFeed()


    def onClick(self,event):
        self.click_location = (event.xdata, event.ydata)


    def onReDraw(self,event):
        x_min, x_max = self.ax.get_xlim()
        y_max, y_min = self.ax.get_ylim()
        self.scale_bar([x_min, x_max, y_min, y_max])


    def onRelease(self,event):

        if (event.xdata,event.ydata) == self.click_location:
            if event.xdata != None and event.ydata != None:
                # click in the figure
                self.goToPosition(event.xdata, event.ydata)


        if self.toolbarNavigation._active == 'ZOOM':
            try:
                x_min = min(event.xdata, self.click_location[0])
                x_max = max(event.xdata, self.click_location[0])
                y_min = min(event.ydata, self.click_location[1])
                y_max = max(event.ydata, self.click_location[1])

                self.scale_bar([x_min, x_max, y_min, y_max])
            except:
                pass


    def goToPosition(self, xdata, ydata):
        x_pixels = self.npix[0]
        y_pixels = self.npix[1]

        # get position of mouse click and return if it was outside figure
        # try:
        x = (xdata-x_pixels/2)*self.pixelsize
        y = -(ydata-y_pixels/2)*self.pixelsize

        x_min, x_max = self.ax.get_xlim()
        y_max, y_min = self.ax.get_ylim()

        x += self.main_widget.setup.motors.x.pos('um')
        y += self.main_widget.setup.motors.y.pos('um')
        z  = self.main_widget.setup.motors.z.pos('um')

        position = str(x)+','+str(y)+','+str(z)

        #print(position)
        self.main_widget.setup.motors.move_to_position(position,'um,um,um')

        self.main_widget.updateAll()


    def heightForWidth(self, w):
        return w*(6/8)

    def scale_bar(self, screen_dimensions = None):

        # clear plot
        try:
            self.line.remove()
            self.text.remove()
        except Exception:
            pass

        if self.show_scale_bar == False:
            return

        if screen_dimensions is None:
            x_min, x_max = self.ax.get_xlim()
            y_max, y_min = self.ax.get_ylim()
        else:
            x_min, x_max, y_min, y_max = screen_dimensions

        x_dim = (x_max-x_min)*self.pixelsize

        if   x_dim > 1600:
            scale_length = 500
        elif x_dim > 400:
            scale_length = 80
        elif   x_dim > 200:
            scale_length = 40
        elif x_dim > 100:
            scale_length = 20
        elif x_dim > 50:
            scale_length = 10
        elif x_dim > 25:
            scale_length = 5
        elif x_dim > 10:
            scale_length = 2
        else:
            scale_length = 1

        length = scale_length/self.pixelsize

        x_min, x_max = self.ax.get_xlim()
        y_max, y_min = self.ax.get_ylim()

        self.text = Text(
                x_min + 0.95*(x_max-x_min)-length/2,
                y_min + 0.90*(y_max-y_min),
                str(scale_length) + " um",
                ha="center",
                va="center",
                rotation=0,
                size=15,
                color='white'
            )

        x0 = x_min + 0.95*(x_max-x_min)
        y0 = y_min + 0.95*(y_max-y_min)

        x = [x0-length,x0]
        y = [y0,y0]

        self.line = Line2D(x,y,color = 'white', linewidth = 2.0)

        self.ax.add_artist(self.line)
        self.ax.add_artist(self.text)


    def molo_circle(self, neighborhoodSize=1, threshold=0):

        try:
            self.circle.remove()

        except Exception:
            pass

        if self.cameraControl.lowSignalSearch.isChecked() == True:

            img = self.img.astype('float')
            img = median_filter(img, size=(int(2*self.Airy_disk_radius/self.pixelsize),int(2*self.Airy_disk_radius/self.pixelsize)))

            i, j = np.unravel_index(img.argmax(),img.shape)

        else:
            i, j = np.unravel_index(self.img.argmax(),self.img.shape)

        self.circle = plt.Circle((i, j), 20, color='w',fill=False)

        self.ax.add_artist(self.circle)



    def braggMask(self):

        try:
            self.mask_bkg.remove()
            self.mask_sig.remove()

        except Exception:
            pass


        magnification = self.main_widget.setup.camera.specifications.magnification
        imagAnalBragg = ImageAnalysisBragg(
            image = self.img,
            pixelsizeCamera = self.pixelsizeCamera,
            magnification = magnification,
            airy_disk_radius = self.Airy_disk_radius,
            )


        image_raw, x_axis, y_axis = imagAnalBragg.defineXYimageAndAxis(self.img)
        image = imagAnalBragg.maskBackgroundROI(self.img)

        image = np.rot90(image)
        image = np.rot90(image)
        image = np.fliplr(image)

        self.mask_bkg = self.ax.imshow(image)

        image = imagAnalBragg.maskPeak(self.img,self.Airy_disk_radius)

        image = np.rot90(image)
        image = np.rot90(image)
        image = np.fliplr(image)

        self.mask_sig = self.ax.imshow(image)



    def speckleCircles(self, neighborhoodSize=1, threshold=0):
        if 'speckles' in dir(self):
            self.ax.lines[-1].remove()
            del self.speckles
            print('Speckles removed')

        # draw a circle, where the mologram location was determined.
        threshold = 4*np.median(self.img)
        specklePositions = findSpeckles(self.img, neighborhoodSize, threshold)

        x = specklePositions[0]
        y = specklePositions[1]

        self.speckles = self.ax.plot(x,y,'wo')
        self.canvas.draw()

        x_min, x_max = self.ax.get_xlim()
        y_max, y_min = self.ax.get_ylim()

        x_dim = (x_max-x_min)*self.pixelsize
        y_dim = (y_max-y_min)*self.pixelsize

        return len(x), len(x)/(x_dim*y_dim)


    def changeNormalization(self):
        if self.setMaximumColor.isChecked():
            maximum = np.max(self.img)

        else:
            maximum = self.colorDepth
            if self.convertUnitsAction.isChecked():
                maximum = self.convertPhysical(maximum)

        self.max_pixel_lbl.setToolTip('Saturation at {0:.3f}'.format(maximum))
        self.max_pixel.setToolTip('Saturation at {0:.3f}'.format(maximum))

        # check combo entry
        gamma = float(str(self.colorNormalization.currentText()))

        self.im.colorbar.set_norm(colors.PowerNorm(gamma=gamma))
        self.im.colorbar.set_ticks(np.linspace(0,maximum,4))
        self.im.set_norm(norm=colors.PowerNorm(gamma=gamma))
        self.im.set_clim(0,maximum)

        self.canvas.draw()


    def loadBackgrounds(self):
        """Function that loads images to substract the hot pixels of the camera. """
        folder = '/home/focal-molography/Experiments/Background/'

        backgroundImages = dict()
        for file in os.listdir(folder):
            try:
                im = Image.open('{}{}'.format(folder,file))
                im = np.array(np.rot90(im,0))
                exposureTime = file.split('-')[1]

                backgroundImages[exposureTime] = im
            except:
                continue

        self.backgroundImages = backgroundImages

    def saveData(self):
        """Saves one data point to the database (current image is processed)"""

        experimentFolder = self.main_widget.getExperimentFolder()
        databaseFolder = '{}/database'.format(experimentFolder)

        folders = [experimentFolder, databaseFolder]
        for folder in folders:
            if not os.path.exists(folder):
                os.makedirs(folder)
                # add permission (rwxr-xr-x -> 111 101 101)
            changePermissionsPath(folder)

        imageName = self.imageName

        gain = self.gain
        exposureTime = float(self.camera.exposure)

        # calculate relevant parameters and save to database
        date = str(datetime.date.today())
        startTime = time.strftime('%H:%M:%S', time.gmtime(time.time()))
        algorithm = self.algorithm
        Airy_disk_radius = self.Airy_disk_radius

        x_position = self.main_widget.setup.motors.x.pos('mm')
        y_position = self.main_widget.setup.motors.y.pos('mm')

        chipNo = self.main_widget.setup.chip.chipNo
        field, line, moloLine = self.main_widget.setup.chip.correspondingMologram((x_position,y_position))

        acquisitionTime = self.acquisitionTime
        signalIntensity = self.signalIntensity
        bkg_mean = self.bkg_mean
        bkg_std = self.bkg_std
        SNR = self.SNR
        referenceIntensity = self.referenceIntensity
        img_mean = self.img_mean
        maxsignal = self.maxsignal

        signalIntensityPhys = self.signalIntensityPhys
        bkg_meanPhys = self.bkg_meanPhys
        bkg_stdPhys = self.bkg_stdPhys
        referenceIntensityPhys = self.referenceIntensityPhys
        img_meanPhys = self.img_meanPhys
        maxsignalPhys = self.maxsignalPhys
        PowerOutcoupling = self.labJack.PowerOutcoupledMean

        # correct intensity by the measured damping constant
        # check if the table damping constants exists


        # save database
        databaseFile = '{}/data.db'.format(databaseFolder)
        #whatever is in the resultsDict will be stored in the database.
        createDB(databaseFile, 'results', getResultsDict())

        z_position = self.main_widget.setup.motors.z.pos('mm')

        saveDB(databaseFile, 'results', locals())
        print('database updated: {}'.format(databaseFile))


    def saveImage(self,imageName,cropImage):
        """To do: Implement cropping correcly"""

        experimentFolder = self.main_widget.getExperimentFolder()
        imageFolder = '{}/images'.format(experimentFolder)

        folders = [experimentFolder, imageFolder]
        for folder in folders:
            if not os.path.exists(folder):
                os.makedirs(folder)
                # add permission (rwxr-xr-x -> 111 101 101)
            changePermissionsPath(folder)

        imagePath = '{}/{}'.format(imageFolder, imageName)

        # save image
        # metadata
        metadata = {
            'Objective' : self.magnification,
            'Pixelsize camera' : self.pixelsizeCamera,
            'Color depth' : self.colorDepth,
            'Pixel gain' : self.pixelGain,
            'Exposure time' : self.exposureTime,
            'Gain' : self.gain,
            'Resolution' : self.pixelsize,
            'Quantum efficiency' : self.quantumEfficiency,
            'wavelength' : self.wavelength,
            'Signal intensity' : self.signalIntensity,
            'Reference intensity' :self.referenceIntensity,
            'SNR' : self.SNR,
        }

        if cropImage == 'QuickSave':
            # crop if zoomed
            x_min, x_max = self.ax.get_xlim()
            y_min, y_max = self.ax.get_ylim()

            image = self.img[int(y_min):int(y_max),int(x_min):int(x_max)]

        else:

            image = self.img

        # save image
        # to be changed to this class
        if self.camera.save_img(imagePath, image, metadata):
            print('Image saved: {}'.format(imagePath))

    def saveImageAndData(self,imageName,cropImage):

        self.imageName = imageName

        self.saveImage(imageName,cropImage)

        self.saveData()

    def saveImageAndDataQuickSave(self, cropImage = True, chooseImagePath = False):
        # define experiment folder

        experimentFolder = self.main_widget.getExperimentFolder()

        print('Quicksaved Image:')

        
        # must be valid
        if chooseImagePath == True:
            imagePath = QtGui.QFileDialog.getSaveFileName(self, 'Save As...', '/home/focal-molography/Experiments', 'Image files (*)')
            if imagePath == '':
                return -1

        else:
            if experimentFolder == '':
                msg = QtGui.QMessageBox()
                msg.setWindowTitle("Folder name")
                msg.setIcon(QtGui.QMessageBox.Warning)
                msg.setStandardButtons(QtGui.QMessageBox.Ok)
                msg.setText("Please select an experiment folder")
                msg.exec_()
                return -1

        if not chooseImagePath:
            # create folder if not exist

            imageFolder = '{}/images'.format(experimentFolder)
            databaseFolder = '{}/database'.format(experimentFolder)

            if not os.path.exists(imageFolder):
                os.makedirs(imageFolder)

            if not os.path.exists(databaseFolder):
                os.makedirs(databaseFolder)


            if 'quicksaveNo' not in dir(self):
                # make numbering
                files = os.listdir(imageFolder)
                self.quicksaveNo = 0
                for file in files:
                    try:
                        self.quicksaveNo = np.max((int(file.split('_')[-1])+1, self.quicksaveNo))
                    except:
                        pass

            imageName = '{}_{}'.format(self.main_widget.getImageName(),self.quicksaveNo)

            self.quicksaveNo += 1

        changePermissionsPath(experimentFolder)

        self.saveImageAndData(imageName,cropImage)

    def convertUnitsImage(self):
        """
        Converts stored image self.img
        """
        quantumEfficiency = self.quantumEfficiency
        exposureTime = self.camera.exposure*1e-3
        gain = self.gain
        pixelGain = self.pixelGain
        pixelsize = self.pixelsizeCamera # pixelsize of the camera
        wavelength = self.wavelength
        magnification = self.magnification

        if self.units[self.unit] == 'physical':

            # convert to binary units
            phi = self.img / (constants.h*constants.c/wavelength)
            n_p = phi * (pixelsize**2 * exposureTime * (gain+1))/(magnification)**2
            img = n_p * (quantumEfficiency * pixelGain)

            self.img = np.round(img) # round to integers

            self.strExtension = ''

        else:
            # convert to physical units


            # number of photons
            n_p = self.img / (pixelGain * quantumEfficiency)
            # photon flux
            phi = (magnification)**2*n_p / (pixelsize**2 * exposureTime * (gain+1))
            # intensity
            I = phi * (constants.h*constants.c/wavelength)

            self.img = I

            self.strExtension = _translate("Form", " [W/m²]", None)

        # units were switched
        self.unit = not self.unit

    def convertUnits(self):
        """Updates the GUI with the converted units"""

        self.convertUnitsImage()

        self.plotImage()

        self.title.setText('Parameters in {} units {}'.format(self.units[self.unit],self.strExtension))
        self.convertUnitsAction.setToolTip('Convert units to {} units'.format(self.units[not self.unit]))

        # adjust normalization
        if self.setMaximumColor.isChecked():
            self.changeNormalization()


    def convertPhysical(self, img):
        # to be deleted but for this I first have to rewrite parts of the Experiment class

        quantumEfficiency = self.quantumEfficiency
        exposureTime = self.camera.exposure*1e-3    # in SI
        gain = self.gain
        pixelGain = self.pixelGain
        pixelsize = self.pixelsizeCamera # pixelsize of the camera
        wavelength = self.wavelength
        magnification = self.magnification

        # convert to physical units
        # averagedImages (?)
        # number of photons
        n_p = img / (pixelGain * quantumEfficiency)

        # photon flux
        phi = (magnification)**2*n_p / (pixelsize**2 * exposureTime * (gain+1))
        # intensity
        I = phi * (constants.h*constants.c/wavelength)

        return I


    def resetImage(self):
        self.im.set_interpolation('None')

        npix = self.camera.specifications.npix
        self.im.set_extent([0,npix[0],0,npix[1]])

        self.ax.set_aspect(1)


    def resetSettings(self, info = dict()):

        if len(info) == 0 and not self.main_widget.offline_mode:
            # camera specifications
            pixelsizeCamera = self.camera.info['pixel_size'] # in um
            self.magnification = self.main_widget.setup.camera.specifications.magnification # objective
            self.pixelsize = pixelsizeCamera/self.magnification # actual pixelsize
            self.pixelsizeCamera = self.camera.info['pixel_size']*1e-6 # camera pixelsize in SI [m]
            self.pixelGain = self.camera.specifications.pixelGain
            self.gain = self.camera.gain
            self.exposureTime = self.camera.exposure*1e-3 # exposure time in SI
            self.quantumEfficiency = self.camera.specifications.quantumEfficiency
            self.wavelength = float(self.main_widget.settingsTab.devicesSettings.laserWavelength.text())*1e-9
            self.npix = (self.camera.info['max_width'], self.camera.info['max_height'])
            self.colorDepth = self.camera.specifications.colorDepth

            self.minExposure = self.camera.minExposure
            self.maxExposure = self.camera.maxExposure

            self.Airy_disk_radius = 0.5

            if self.main_widget.setup.chip.mask == 'D1C':
                self.algorithm = 'sum over background plus 3 sigma'

            if self.main_widget.setup.chip.mask == 'Bragg':
                self.algorithm = 'Bragg'

        else:
            self.pixelsize = info.get('Resolution',2.2)
            magnification = info.get('Objective', 20)
            self.pixelsizeCamera = self.pixelsize*magnification*1e-6 # camera pixelsize in SI [m]
            self.pixelGain = info.get('Pixel gain',0.3826)
            self.gain = info.get('Gain',0)
            self.exposureTime = info.get('Exposure time', 0)
            self.quantumEfficiency = info.get('Quantum efficiency', 0.48)
            self.wavelength = info.get('Wavelength', 632.8e-9)
            self.npix = info.get('npix', [2560, 1920])
            self.colorDepth = info.get('Color depth', 2**12-1)

            self.minExposure = info.get('minExposure', 0.031)
            self.maxExposure = info.get('maxExposure', 2745)

            self.Airy_disk_radius = info.get('Airy_disk_radius', 0.5)

            self.algorithm = info.get('algorithm', 'sum over background plus 3 sigma')

            print(info)
