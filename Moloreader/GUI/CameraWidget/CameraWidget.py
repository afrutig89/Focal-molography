from PyQt4 import QtGui, QtCore
from GUI.CameraWidget.CameraFeed import CameraFeed
from GUI.CameraWidget.CameraControl import CameraControl
import numpy as np
import time


class CameraWidget(QtGui.QWidget):
	""" Constructor """
	def __init__(self,main_widget):

		# Call parent class constructor
		super(CameraWidget,self).__init__()

		# Assign main layout
		self.main_widget = main_widget

		# Call child constructor
		self.initUI()


	def initUI(self):
		self.cameraWidget_lbl = QtGui.QLabel('Camera Widget')
		self.cameraControl = CameraControl(self.main_widget)
		self.cameraFeed    = CameraFeed(self.main_widget, self.cameraControl)

		vbox = QtGui.QVBoxLayout()
		self.setLayout(vbox)

		vbox.addWidget(self.cameraWidget_lbl)
		vbox.addWidget(self.cameraFeed)
		vbox.addWidget(self.cameraControl)
