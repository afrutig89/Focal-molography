from PyQt4 import QtGui, QtCore
import time
import numpy as np

class CameraControl(QtGui.QWidget):
    """ Constructor """
    def __init__(self,main_widget):

        # Call parent class constructor
        super(CameraControl,self).__init__()
        
        self.main_widget = main_widget

        # Call child constructor
        self.initUI()

    """ Initialization of UI """
    def initUI(self):

        if not self.main_widget.offline_mode:
            min_exposure = self.main_widget.setup.camera.minExposure
            max_exposure = self.main_widget.setup.camera.maxExposure

            exposureTime = self.main_widget.setup.camera.exposure

            # Create slider for exposure
            self.exposure_slider = QtGui.QSlider(QtCore.Qt.Horizontal)
            self.exposure_slider.setMinimum(min_exposure)
            self.exposure_slider.setMaximum(max_exposure)

            self.exposure_slider.setTickInterval(100)
            self.exposure_slider.setTickPosition(QtGui.QSlider.TicksBelow)
            self.exposure_slider.setSingleStep(0.001)
            self.exposure_slider.setValue(exposureTime)

            self.exposure_spinbox = QtGui.QDoubleSpinBox()
            self.exposure_spinbox.setRange(min_exposure, max_exposure)
            self.exposure_spinbox.setKeyboardTracking(False)
            self.exposure_spinbox.setValue(exposureTime)

            # labels
            self.exposure_label = QtGui.QLabel()
            self.exposure_label.setText('{:.2f} ms'.format(exposureTime))

            # connect buttons
            self.exposure_slider.sliderReleased.connect(self.updateExposureSlider)
            self.exposure_spinbox.valueChanged[float].connect(self.update_exposure_spinbox)
        
            hbox_top = QtGui.QHBoxLayout()
            hbox_top.addWidget(self.exposure_slider)
            hbox_top.addWidget(self.exposure_spinbox)
            hbox_top.addWidget(self.exposure_label)


        # Create radio button for auto exposure
        self.lowSignalSearch = QtGui.QCheckBox("Low signal search", self)
        self.subtractBackground = QtGui.QCheckBox("Subtract background",self)
        self.subtractImage = QtGui.QCheckBox("Subtract previous image",self)
        self.highlightMaximum = QtGui.QCheckBox("Highlight maximum", self)
        self.highlightMaximum.clicked.connect(self.highlightMaximumClicked)
        self.referenceMologram = QtGui.QCheckBox("Reference Mologram",self)

        self.braggMask = QtGui.QCheckBox("Bragg mask", self)
        self.braggMask.clicked.connect(self.braggMaskClicked)

        # layout
        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(hbox_top)
        hbox_bot = QtGui.QHBoxLayout()

        # assign widgets to layout
        hbox_bot.addWidget(self.lowSignalSearch)
        hbox_bot.addWidget(self.subtractBackground)
        hbox_bot.addWidget(self.subtractImage)
        hbox_bot.addWidget(self.highlightMaximum)
        hbox_bot.addWidget(self.referenceMologram)
        hbox_bot.addWidget(self.braggMask)

        if self.main_widget.setup.chip.mask != 'Bragg':
            self.braggMask.setVisible(False)

        vbox.addLayout(hbox_bot)
        self.setLayout(vbox)


    def set_exposure(self, value, update=True):
        self.main_widget.setup.camera.set_exposure(self.exposure_slider.value())


    def set_show_im_analysis(self):
        ischecked = self.show_im_analysis.isChecked()
        self.im_analysis_algorithm.setEnabled(ischecked)
        self.update_im_analysis()


    def update_exposure_spinbox(self, value, update=True):
        self.exposure_slider.setValue(value)
        self.exposure_label.setText('{:.2f} ms'.format(value))
        self.main_widget.setup.camera.set_exposure(value)

        if update:
            time.sleep(0.5)
            self.main_widget.updateAll()


    def updateExposureSlider(self):
        value = self.exposure_slider.value()
        self.exposure_spinbox.setValue(value)

    def update_exposure_value(self):
        exposureTime = self.main_widget.setup.camera.exposure
        print(float(exposureTime))
        self.exposure_slider.setValue(exposureTime)
        self.exposure_label.setText('{:.3f} ms'.format(float(exposureTime)))
        self.main_widget.setup.camera.set_exposure(exposureTime)
            

    def update_im_analysis(self):
        self.intensity_val  = 0
        self.mean_pixel_val = 0

        self.intensity_label.setText(str(self.intensity_val))
        self.mean_pixel_label.setText(str(self.mean_pixel_val))


    def highlightMaximumClicked(self):
        if self.highlightMaximum.isChecked():
            self.main_widget.cameraWidget.cameraFeed.molo_circle()

        else:
            try:
                self.main_widget.cameraWidget.cameraFeed.circle.remove()
            
            except Exception:
                pass

    def braggMaskClicked(self):
        if self.braggMask.isChecked():
            self.main_widget.cameraWidget.cameraFeed.braggMask()

        else:
            try:
                self.main_widget.cameraWidget.cameraFeed.mask_bkg.remove()
                self.main_widget.cameraWidget.cameraFeed.mask_sig.remove()
            
            except Exception:
                pass
