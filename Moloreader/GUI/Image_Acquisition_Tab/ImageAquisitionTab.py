from PyQt4 import QtGui

# from GUI.Image_Acquisition_Tab.NoiseRecorder import NoiseRecorder
from GUI.Image_Acquisition_Tab.Stack import Stack
from GUI.Image_Acquisition_Tab.SaveImage import SaveImage
from GUI.Image_Acquisition_Tab.ArrayMeasurements import ArrayMeasurements

# tab for all image acquisition related tasks

class ImageAquisitionTab(QtGui.QWidget):
	def __init__(self,main_widget):
		super(ImageAquisitionTab,self).__init__()

		self.main_widget = main_widget

		self.initUI()

	def initUI(self):
		
		self.stack = Stack(self.main_widget)
		self.arrayMeasurements = ArrayMeasurements(self.main_widget)
		self.saveImage = SaveImage(self.main_widget)

		self.chipNo_lbl = QtGui.QLabel('Chip number')
		self.chipNo = QtGui.QLineEdit(self.main_widget.setup.chip.chipNo)
		self.chipNo.textChanged.connect(self.update)

		self.tabs = QtGui.QTabWidget()
		self.tabs.addTab(self.saveImage,'Save Image')
		self.tabs.addTab(self.arrayMeasurements, 'Array Measurements')
		self.tabs.addTab(self.stack, 'Stack')

		hbox = QtGui.QHBoxLayout()
		hbox.addWidget(self.chipNo_lbl)
		hbox.addWidget(self.chipNo)
		main_vbox  = QtGui.QVBoxLayout()
		main_vbox.addLayout(hbox)
		main_vbox.addWidget(self.tabs)
		self.setLayout(main_vbox)

	def update(self):

		self.main_widget.setup.chip.chipNo = str(self.chipNo.text())



