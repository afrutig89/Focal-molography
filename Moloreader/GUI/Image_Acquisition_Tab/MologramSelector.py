from PyQt4 import QtGui


class MologramSelector(QtGui.QHBoxLayout):
	# class for the mologram selector field in th4e
	def __init__(self):
		super(MologramSelector,self).__init__()

		self.initUI()

	def initUI(self):
		
		# Declare widgets
		self.field_selector = QtGui.QComboBox()
		self.line_selector  = QtGui.QComboBox()
		self.row_selector   = QtGui.QComboBox()

		# Add items to combo boxes
		fields = ('B','C','D','E','F','G')
		num_lines = 4
		num_rows  = 10

		for field in fields:
			self.field_selector.addItem(field)

		for row in range(num_rows):
			self.row_selector.addItem(str(row+1))

		for line in range(num_lines):
			self.line_selector.addItem(str(line+1))

		# Layout
		# hbox = QtGui.QHBoxLayout()
		# self.setLayout(hbox)

		self.addWidget(self.field_selector)
		self.addWidget(self.line_selector)
		self.addWidget(self.row_selector)

	def position(self):
		pos  = '' 
		pos += self.field_selector.currentText() + ','
		pos += self.line_selector.currentText() + ','
		pos += self.row_selector.currentText()

		return pos

	def setEnabled(self,state):
		self.field_selector.setEnabled(state)
		self.line_selector.setEnabled(state)
		self.row_selector.setEnabled(state)