from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui,QtCore
import os
import numpy as np
import time
from Data_Acquisition.permission import changePermissionsPath
from PyQt4.QtGui import QMessageBox
import Data_Processing.MologramProcessing as mologram_processing
from SharedLibraries.Database.dataBaseOperations import saveDB, loadDB, createDB, createSQLDict
from settings import getVariables, getLogExperimentDict, getResultsDict
import datetime, time
from Data_Acquisition.Experiment import Experiment


class ArrayMeasurements(QtGui.QWidget):
    def __init__(self,main_widget):
        super(ArrayMeasurements,self).__init__()

        self.main_widget = main_widget
        self.camera = self.main_widget.setup.camera
        self.cameraControl = main_widget.cameraWidget.cameraControl

        self.initUI()

        # self.auto_name.setChecked(True)
        # self.crop_image.setChecked(True)

    def initUI(self):

        self.chip = self.main_widget.setup.chip

        # declare widgets
        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Destination')
        self.editFolder = QtGui.QLineEdit()

        self.editFolder.setToolTip('Is changed if not single experiment')

        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.browse_f)

        self.save_lbl     = QtGui.QLabel('Save')
        self.blank_lbl     = QtGui.QLabel('')

        # crop the image to 60 x 60 um in order to save diskspace.
        self.save_image         = QtGui.QCheckBox('Save image')
        self.save_image.setChecked(True)
        self.save_image.clicked.connect(self.saveImageChecked)
        self.crop_image         = QtGui.QCheckBox('Crop image to 60 x 60 um')

        self.measure_lbl     = QtGui.QLabel('Measurement')

        self.measureSurface = QtGui.QCheckBox("Measure surface")
        self.measureSurface.setChecked(True)

        self.measureMologram = QtGui.QCheckBox("Measure mologram")
        self.measureMologram.setChecked(True)

        self.measureDamping = QtGui.QCheckBox("Measure damping")
        self.measureDamping.setChecked(False)
        self.measureDamping.stateChanged.connect(self.dampingSelection)

        self.autoExposure = QtGui.QRadioButton("Auto exposure")
        self.autoExposure.setChecked(True)

        self.fixedExposure = QtGui.QRadioButton("Fix exposure")
        self.fixedExposure.setChecked(False)

        self.focus = QtGui.QCheckBox('Focus')
        self.focus.setChecked(True)
        self.center = QtGui.QCheckBox('Center')
        self.center.setChecked(True)
        self.coupling = QtGui.QCheckBox('Adjust Coupling')
        self.coupling.setChecked(True)
        self.couplingline = QtGui.QCheckBox('Adjust Coupling Line') # adjusts the coupling only for every new line.
        self.couplingline.setChecked(True)

        self.fixedExposureEdit = QtGui.QLineEdit()
        self.fixedExposureEdit.setText(str(self.camera.exposure))

        hbox_fields = QtGui.QHBoxLayout()
        self.fieldsTab = QtGui.QTabWidget()
        self.fields = dict()
        for field in np.arange(ord(self.chip.MIN_SET),ord(self.chip.MAX_SET)+1):
            self.fields[chr(field)] = QtGui.QCheckBox('Field ' + chr(field))
            hbox_fields.addWidget(self.fields[chr(field)])
            if chr(field) == 'B': self.fields[chr(field)].setChecked(True)  # check first field
            self.fields[chr(field)].clicked.connect(self.enableTab)

            layoutH = QtGui.QWidget()
            layoutH.setGeometry(QtCore.QRect(25, 30, 400, 25))
            layoutH.setMinimumSize(QtCore.QSize(431, 175))
            y = 25
            for line in np.arange(self.chip.fields.lines)+1:
                x = 25
                for moloLine in np.arange(self.chip.fields.molos)+1:
                    self.fields['{},{},{}'.format(chr(field), line, moloLine)] = QtGui.QCheckBox(str(moloLine),layoutH)
                    self.fields['{},{},{}'.format(chr(field), line, moloLine)].setGeometry(x, y, 40, 17)
                    x += 40
                self.fields['selectLine{},{}'.format(chr(field), line)] = QtGui.QPushButton('Select line ' + str(line), layoutH)
                self.fields['selectLine{},{}'.format(chr(field), line)].setGeometry(x+15, y-2, 150, 22)
                self.fields['selectLine{},{}'.format(chr(field), line)].clicked.connect(self.selectLine)
                y += 30

            self.fields['selectAll{}'.format(chr(field))] = QtGui.QPushButton('Select all', layoutH)
            self.fields['selectAll{}'.format(chr(field))].setGeometry(25, y, 190, 22)
            self.fields['selectAll{}'.format(chr(field))].clicked.connect(self.selectAll)
            self.fields['selectNone{}'.format(chr(field))] = QtGui.QPushButton('Select none', layoutH)
            self.fields['selectNone{}'.format(chr(field))].setGeometry(235, y, 190, 22)
            self.fields['selectNone{}'.format(chr(field))].clicked.connect(self.selectNone)

            self.fieldsTab.addTab(layoutH, 'Field {}'.format(chr(field)))
            self.fieldsTab.setTabEnabled(field-ord(self.chip.MIN_SET), self.fields[chr(field)].isChecked())

        # Create and connect buttons
        self.start = QtGui.QPushButton('Start measurement')
        self.start.clicked.connect(self.startExperiment)
        self.stop = QtGui.QPushButton('Stop measurement')
        self.pause = QtGui.QPushButton('Pause/Continue measurement')
        self.addExperiment = QtGui.QPushButton('Add to experiment')
        self.addExperiment.clicked.connect(self.addToExperiment)


        # layout
        hbox_folder   = QtGui.QHBoxLayout()
        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        hbox_save   = QtGui.QHBoxLayout()
        hbox_save.addWidget(self.save_image)
        hbox_save.addWidget(self.crop_image)

        hbox_measure   = QtGui.QHBoxLayout()
        hbox_measure.addWidget(self.measureSurface)
        hbox_measure.addWidget(self.measureMologram)
        hbox_measure.addWidget(self.measureDamping)

        hbox_measure2   = QtGui.QHBoxLayout()
        hbox_measure2.addWidget(self.autoExposure)
        hbox_measure2.addWidget(self.fixedExposure)
        hbox_measure2.addWidget(self.fixedExposureEdit)

        hbox_measure3 = QtGui.QHBoxLayout()
        hbox_measure3.addWidget(self.focus)
        hbox_measure3.addWidget(self.center)
        hbox_measure3.addWidget(self.coupling)
        hbox_measure3.addWidget(self.couplingline)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.editFolder_lbl, hbox_folder)
        fbox.addRow(self.save_lbl, hbox_save)
        fbox.addRow(self.measure_lbl, hbox_measure)
        fbox.addRow(self.blank_lbl, hbox_measure3)
        fbox.addRow(self.blank_lbl, hbox_measure2)
        fbox.addRow(self.blank_lbl, hbox_fields)
        fbox.addRow(self.blank_lbl)


        hbox2 = QtGui.QHBoxLayout()
        hbox2.addWidget(self.start)
        hbox2.addWidget(self.stop)
        hbox2.addWidget(self.pause)

        # layout
        fbox2 = QtGui.QFormLayout()
        fbox2.addRow(self.fieldsTab)
        fbox2.addRow(hbox2)
        fbox2.addRow(self.addExperiment)

        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(fbox)
        vbox.addLayout(fbox2)
        self.setLayout(vbox)


    def selectAll(self):
        field = chr(self.fieldsTab.currentIndex()+ord(self.chip.MIN_SET))
        for line in np.arange(self.chip.fields.lines)+1:
            for moloLine in np.arange(self.chip.fields.molos)+1:
                self.fields['{},{},{}'.format(field, line, moloLine)].setChecked(True)


    def selectNone(self):
        field = chr(self.fieldsTab.currentIndex()+ord(self.chip.MIN_SET))
        for line in np.arange(self.chip.fields.lines)+1:
            for moloLine in np.arange(self.chip.fields.molos)+1:
                self.fields['{},{},{}'.format(field, line, moloLine)].setChecked(False)

    def selectLine(self):

        # field the same, so take current tab index
        field = chr(self.fieldsTab.currentIndex()+ord(self.chip.MIN_SET))

        # line number complicated since we would always get last line number if sent by lambda function
        line = int(str(self.sender().text()).split(' ')[-1])

        # loop through lines
        for moloLine in np.arange(self.chip.fields.molos)+1:
            self.fields['{},{},{}'.format(field, line, moloLine)].setChecked(not self.fields['{},{},{}'.format(field, line, moloLine)].isChecked())

    def dampingSelection(self):
        """Check all the second lines in order to do the damping measurement, where no molograms are."""

        for field in np.arange(ord(self.chip.MIN_SET),ord(self.chip.MAX_SET)+1):

            field = chr(field) # convert to character
            for moloLine in np.arange(self.chip.fields.molos)+1:
                line = 2
                self.fields['{},{},{}'.format(field, line, moloLine)].setChecked(not self.fields['{},{},{}'.format(field, line, moloLine)].isChecked())


    def enableTab(self):
        for field in np.arange(ord(self.chip.MIN_SET),ord(self.chip.MAX_SET)+1):
            self.fieldsTab.setTabEnabled(field-ord(self.chip.MIN_SET), self.fields[chr(field)].isChecked())


    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file',
                '/home/focal-molography/Experiments/')
        self.editFolder.setText(folder)
        self.main_widget.setExperimentFolder(folder)


    def getMolos(self):
        molosToMeasure = []
        for field in np.arange(ord(self.chip.MIN_SET),ord(self.chip.MAX_SET)+1):
            if not self.fields[chr(field)].isChecked(): continue
            line_counter = 1

            # if damping measurement is selected then measure line number two first.

            if self.measureDamping.isChecked():

                line = 2
                appended = False
                for moloLine in np.arange(self.chip.fields.molos)+1:
                    if not self.fields['{},{},{}'.format(chr(field), line, moloLine)].isChecked(): continue
                    molosToMeasure.append('{},{},{}'.format(chr(field), line, moloLine))
                    appended = True

            for line in np.arange(self.chip.fields.lines)+1:
                appended = False

                if self.measureDamping.isChecked() and line == 2:
                    continue

                # check if line_counter odd number
                if line_counter & 1 == 1:
                    for moloLine in np.arange(self.chip.fields.molos)+1:
                        if not self.fields['{},{},{}'.format(chr(field), line, moloLine)].isChecked(): continue
                        molosToMeasure.append('{},{},{}'.format(chr(field), line, moloLine))
                        appended = True
                else:
                    # reverse interation order of molos for even line number
                    for moloLine in (np.arange(self.chip.fields.molos)+1)[::-1]:
                        if not self.fields['{},{},{}'.format(chr(field), line, moloLine)].isChecked(): continue
                        molosToMeasure.append('{},{},{}'.format(chr(field), line, moloLine))
                        appended = True

                if appended: line_counter += 1

        return molosToMeasure


    def startExperiment(self):

        # initialize
        if self.editFolder.text() == '':
            # no filename given: autoname
            experimentNo = findLastExperiment()+1
            self.editFolder.setText('/home/focal-molography/Experiments/%03d' % experimentNo)

        self.experimentFolder = self.editFolder.text()
        self.main_widget.setExperimentFolder(self.experimentFolder)

        # Create directories if non-exgaussian filteristent
        # definde directories
        self.databaseFolder = self.experimentFolder + '/database'
        self.imageFolder = self.experimentFolder + '/images'
        directories = [self.experimentFolder, self.imageFolder, self.databaseFolder]

        for directory in directories:
            if not os.path.exists(directory):
                os.makedirs(directory)

            # add permission (rwxr-xr-x -> 111 101 101)
            changePermissionsPath(directory)


        info = self.createInfo()
        ID = self.createLogEntry(self.databaseFolder)

        # Thread for recording the Experiment
        self.experiment = Experiment(self.main_widget, self.experimentFolder)
        self.experiment.status.connect(self.highlightMolo)

        self.stop.clicked.connect(self.experiment.stop)
        self.pause.clicked.connect(self.experiment.pause)

        self.experiment.record_data(
                experimentType       = info['Experiment type'],
                mologram            = info['Molograms'],
                measureSurface      = info['Measure surface'],
                measureMologram      = info['Measure mologram'],
                coupling      = info['Adjust Coupling'],
                couplingline      = info['Adjust Coupling Line'],
                ID                 = ID,
                algorithm           = info['Algorithm'],
                focus               = info['Focus'],
                center              = info['Center'],
                autoExposure     = info['Auto exposure'],
                fixedExposure     = info['Fixed exposure'],
                saveImages         = info['Save images'],
                dampingMeasurement = info['dampingMeasurement']
            )

    def checkMolos(self, molos):
        # uncheck all molos
        self.selectNone()

        # check given molos
        for molo in molos:
            self.fields[molo].setChecked(True)

    def highlightMolo(self, statusMessage):
        """
        Highlights the mologram in the array measurements tab.

        :param statusMessage: molo and status given in a list (e.g. ['B,1,1','running'])
        """

        if statusMessage[0] == 'start':
            for key, value in self.fields.items():
                self.fields[key].setStyleSheet("background-color: none")

            return

        molo, status = statusMessage # get this from the signal
        field, line, moloLine = molo.split(',')

        # highlight field that is measured
        self.fieldsTab.setCurrentIndex(ord(field)-ord(self.chip.MIN_SET))
        if status == 'running':
            self.fields[molo].setStyleSheet("background-color: yellow")
        if status == 'done':
            self.fields[molo].setStyleSheet("background-color: green")


    def save(self, image, moloName, extension=''):
        """
        Saves an image
        """

        folder = self.initSave()
        if self.auto_name.isChecked() or self.editName == '':
            # generate name if autonaming is set or edit line empty
            file_name = moloName \
                        + '_exp-' + str(self.main_widget.setup.camera.exposure) + 'ms' \
                        + extension
            self.editName.setText(file_name)

        file = folder + '/' + self.editName.text()

        if self.crop_image.isChecked():
            # crop an image of 60 x 60 um
            if self.main_widget.setup.camera.crop_and_save_img(file, image) == True:
                print('image cropped and saved ' + file)
        else:
            if self.main_widget.setup.camera.save_img(file, image) == True:
                print('image saved ' + file)


    def returnNumberOfCheckedMolos(self):
        molos = 0
        for field in np.arange(ord(self.chip.MIN_SET),ord(self.chip.MAX_SET)+1):
            if not self.fields[chr(field)].isChecked(): continue
            for line in np.arange(self.chip.fields.lines)+1:
                for moloLine in np.arange(self.chip.fields.molos)+1:
                    if self.fields['{},{},{}'.format(chr(field), line, moloLine)].isChecked():
                        molos += 1
        return molos


    def createInfo(self):
        noOfMolos = self.returnNumberOfCheckedMolos()
        molos = self.getMolos()
        measureSurface = self.measureSurface.isChecked()
        measureMologram = self.measureMologram.isChecked()
        autoExposure = self.autoExposure.isChecked()
        if not autoExposure:
            fixedExposure = float(self.fixedExposureEdit.text())
        estimatedTime = noOfMolos*30*(int(measureSurface)+int(measureMologram))    # approx. 30s per measurement
        experimentType = 'Array measurement'
        experimentFolder = self.editFolder.text()
        focus = self.focus.isChecked()
        center = self.focus.isChecked()
        coupling = self.coupling.isChecked()
        couplingline = self.couplingline.isChecked()
        dampingMeasurement = self.measureDamping.isChecked()

        if self.crop_image.isChecked():
            saveImages = 'crop'
        else:
            saveImages = 'original'

        info = dict()
        self.log = dict()

        variables = getVariables()
        for variable, description in variables.items():
            info[description] = locals().get(variable, None)
            if variable in locals():
                if type(locals().get(variable)) == list:
                    self.log[variable] = str(locals().get(variable))
                else:
                    self.log[variable] = locals().get(variable)

        return info

    def createLogEntry(self, folder):

        createDB(folder + '/data.db', 'logExperiment', getLogExperimentDict())
        if 'log' not in dir(self):
            # log dictionary if not exist
            info = self.createInfo()

        self.log['date'] = str(datetime.date.today())

        saveDB(folder + '/data.db', 'logExperiment', self.log)
        changePermissionsPath(folder + '/data.db')

        createDB(folder + '/data.db', 'results', getResultsDict())

        log = loadDB(folder + '/data.db', 'logExperiment')
        if type(log) != list:
            return 1

        return len(log)


    def addToExperiment(self):
        # self.initExperiment()

        info = self.createInfo()

        # delete entries which are none
        experimentEntry = dict()
        for key, value in info.items():
            if info[key] != None:
                experimentEntry[key] = value

        experimentEntry.pop('Experiment folder', None)
        experimentType = experimentEntry.pop('Experiment type', None)

        timeRequiredSeconds = experimentEntry.pop('Estimated time', None)
        timeRequired = time.strftime('%H:%M:%S', time.gmtime(timeRequiredSeconds))
        molos = str(experimentEntry.pop('Molograms'))
        infoStr = str(experimentEntry).replace(", ","\n").replace("{","").replace("}","").replace("'","")

        row = [timeRequired, experimentType, molos, infoStr]
        self.main_widget.experimentTab.addRow(row)


    def saveImageChecked(self):
        self.crop_image.setEnabled(self.save_image.isChecked())


def findLastExperiment():
    experimentFolder = '/home/focal-molography/Experiments/'

    # folder names must begin with a counter
    experimentCounter = []
    for directory in os.listdir(experimentFolder):
        try:
            # try to get integer from the beginning of the filename
            experimentCounter.append(int(directory.split('_')[0]))
        except:
            pass

    return np.max(np.array(experimentCounter))
