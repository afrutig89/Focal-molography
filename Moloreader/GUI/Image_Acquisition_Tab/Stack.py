from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui, QtCore
from Data_Acquisition.Experiment import Experiment
from GUI.MologramSelector import MologramSelector
from SharedLibraries.Database.dataBaseOperations import saveDB, loadDB, createDB, createSQLDict
from Data_Acquisition.permission import changePermissionsPath
from settings import getVariables, getLogExperimentDict, getResultsDict
import os
import sqlite3
from PyQt4.QtGui import QMessageBox
import json
import numpy as np
import datetime, time


class Stack(QtGui.QWidget):
    def __init__(self,main_widget):
        super(Stack,self).__init__()

        self.main_widget   = main_widget
        self.camera = self.main_widget.setup.camera

        self.initUI()
        self.crop_image.setChecked(True)
        self.autoExposure.setChecked(True)


    def initUI(self):
        self.database_path = ''
        self.timeInterval = 0
        self.numCaptures = 0

        # algorithm names
        self.algo_names = []
        self.algo_names.append('sum over background plus 3 sigma')
        self.algo_names.append('sum over background plus 3 sigma (filter)')
        self.algo_names.append('maximum over background plus 3 sigma')
        self.algo_names.append('Bragg')

        # different types of stacks
        self.stack_type_list = ['Time stack', 'z-position', 'Exposure', 'Gain']
        self.time_unit_list = ['ms,' 's', 'min']

        self.mologram_lbl = QtGui.QLabel('Measure')
        self.mologram_pos = MologramSelector(self.main_widget.setup.chip)
        self.mologram_pos_checkbox = QtGui.QCheckBox('Current Position')
        self.mologram_pos_checkbox.clicked.connect(self.enableMologram)
        self.subtractBackground = QtGui.QCheckBox("Subtract background",self)
        self.subtractBackground.setEnabled(False)
        self.subtractImage = QtGui.QCheckBox("Subtract previous image",self)
        self.measureSurface = QtGui.QRadioButton("Surface")
        self.measureMologram = QtGui.QRadioButton("Mologram")
        self.measureMologram.setChecked(True)

        self.mologram_pos_checkbox.setChecked(True)
        self.enableMologram()

        hbox_measure = QtGui.QHBoxLayout()
        hbox_measure.addWidget(self.mologram_pos_checkbox)
        hbox_measure.addWidget(self.subtractBackground)
        hbox_measure.addWidget(self.subtractImage)

        hbox_molo     = QtGui.QHBoxLayout()
        hbox_molo.addLayout(self.mologram_pos)

        measureGroup = QtGui.QButtonGroup(hbox_molo)
        measureGroup.addButton(self.measureSurface)
        measureGroup.addButton(self.measureMologram)
        hbox_molo.addWidget(self.measureMologram)
        hbox_molo.addWidget(self.measureSurface)

        self.previous_lbl = QtGui.QLabel('Before measurement')
        self.focus = QtGui.QCheckBox('Focus')
        self.center = QtGui.QCheckBox('Center')

        # declare widgets
        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Experiment Folder')
        self.editFolder = QtGui.QLineEdit()
        self.editFolder.setToolTip('Is changed if not single experiment')

        self.blank_lbl = QtGui.QLabel('')

        self.time_int_lbl = QtGui.QLabel('Time Interval')
        self.time_int_sbox = QtGui.QDoubleSpinBox(value = 3)
        self.time_units = QtGui.QComboBox()

        self.num_caps_lbl = QtGui.QLabel('Number of Captures')
        self.number_caps = QtGui.QSpinBox(value = 20)

        self.algorithm_lbl = QtGui.QLabel('Intensity Algorithm')
        self.algorithms = QtGui.QComboBox()

        self.stack_type_lbl = QtGui.QLabel('Stack Type')
        self.stack_type = QtGui.QComboBox()
        self.stack_incr_lbl = QtGui.QLabel('Stack Increment')
        self.stack_incr_lbl.setToolTip('z-Stack: Motor will move up have of the distance and then down with the defined increment.\nNote: Stored as steps not um.')
        self.stack_incr_sbox = QtGui.QDoubleSpinBox()
        self.stack_unit = QtGui.QComboBox()

        self.num_avg_lbl = QtGui.QLabel('Number of Avg per capture')
        self.num_avg_lbl.setToolTip('Will be adjusted if required time > Time interval')
        self.num_avg = QtGui.QSpinBox(value = 1)
        self.num_avg.setToolTip('Will be adjusted if required time > Time interval')
        self.num_avg_maximize = QtGui.QCheckBox('Maximum possible within time interval')
        self.num_avg_maximize.clicked.connect(self.numAvgMaxClicked)

        self.save_image         = QtGui.QCheckBox('Save image')
        self.save_image.setChecked(True)
        self.save_image.clicked.connect(self.saveImageChecked)
        self.crop_image         = QtGui.QCheckBox('Crop image to 120 x 120 um')

        self.autoExposure_lbl= QtGui.QLabel('Adjust Exposure')
        self.autoExposure = QtGui.QRadioButton("Auto exposure")
        self.fixedExposure = QtGui.QRadioButton("Fix exposure")
        self.fixedExposureEdit = QtGui.QLineEdit()
        self.fixedExposureEdit.setText(str(self.camera.exposure))
        self.adjustExposure = QtGui.QRadioButton('Adjust -10%')
        # self.adjustExposure.setEnabled(False) # todo

        hbox_exposure = QtGui.QHBoxLayout()
        exposureGroup = QtGui.QButtonGroup(hbox_exposure)
        exposureGroup.addButton(self.autoExposure)
        exposureGroup.addButton(self.fixedExposure)
        exposureGroup.addButton(self.adjustExposure)
        
        hbox_exposure.addWidget(self.autoExposure)
        hbox_exposure.addWidget(self.adjustExposure)
        hbox_exposure.addWidget(self.fixedExposure)
        hbox_exposure.addWidget(self.fixedExposureEdit)


        for name in self.algo_names:
            self.algorithms.addItem(name)

        for name in self.stack_type_list:
            self.stack_type.addItem(name)

        self.set_stack_unit()

        self.number_caps.setRange(0,100000)

        # Create and connect buttons    
        self.start = QtGui.QPushButton('Start measurement')
        self.start.clicked.connect(self.start_f)
        self.start.resize(self.start.sizeHint())

        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.browse_f)
        self.browse.resize(self.browse.sizeHint())

        self.stop = QtGui.QPushButton('Stop measurement')
        self.pause = QtGui.QPushButton('Pause/Continue measurement')
        
        self.addToExperimentButton = QtGui.QPushButton('Add to experiment')
        self.addToExperimentButton.clicked.connect(self.addToExperiment)
        self.stack_type.currentIndexChanged.connect(self.set_stack_unit)
        self.time_units.currentIndexChanged.connect(lambda: self.setIncrementRestrictions(restrict='time'))
        self.stack_unit.currentIndexChanged.connect(lambda: self.setIncrementRestrictions(restrict='stack'))


        # layout
        hbox_folder   = QtGui.QHBoxLayout()
        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        hbox_time     = QtGui.QHBoxLayout()
        hbox_time.addWidget(self.time_int_sbox)
        hbox_time.addWidget(self.time_units)

        hbox_stack    = QtGui.QHBoxLayout()
        hbox_stack.addWidget(self.stack_incr_sbox)
        hbox_stack.addWidget(self.stack_unit)

        hbox_avg      = QtGui.QHBoxLayout()
        hbox_avg.addWidget(self.num_avg)
        hbox_avg.addWidget(self.num_avg_maximize)


        hbox_pre = QtGui.QHBoxLayout()
        hbox_pre.addWidget(self.focus)
        hbox_pre.addWidget(self.center)

        hbox_start    = QtGui.QHBoxLayout()
        hbox_start.addWidget(self.start)
        hbox_start.addWidget(self.stop)
        hbox_start.addWidget(self.pause)

        hbox_save   = QtGui.QHBoxLayout()
        hbox_save.addWidget(self.save_image)
        hbox_save.addWidget(self.crop_image)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.editFolder_lbl, hbox_folder)
        fbox.addRow(self.blank_lbl, hbox_save)
        fbox.addRow(self.previous_lbl, hbox_pre)
        fbox.addRow(self.algorithm_lbl,  self.algorithms)
        fbox.addRow(self.autoExposure_lbl, hbox_exposure)
        fbox.addRow(self.stack_type_lbl, self.stack_type)
        fbox.addRow(self.mologram_lbl, hbox_measure)
        fbox.addRow(self.blank_lbl, hbox_molo)
        fbox.addRow(self.time_int_lbl,   hbox_time)
        fbox.addRow(self.stack_incr_lbl, hbox_stack)
        fbox.addRow(self.num_caps_lbl,   self.number_caps)
        fbox.addRow(self.num_avg_lbl,    hbox_avg)
        fbox.addRow(hbox_start)
        fbox.addRow(self.addToExperimentButton)
        
        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(fbox)
        self.setLayout(hbox)

    def enableMologram(self):
        self.mologram_pos.setEnabled(not self.mologram_pos_checkbox.isChecked())
        self.measureMologram.setEnabled(not self.mologram_pos_checkbox.isChecked())
        self.measureSurface.setEnabled(not self.mologram_pos_checkbox.isChecked())


    def set_stack_unit(self):
        """ Depending on the current stack type, set the right units for the stack units 
        QComboBox. Also disables and enables other widgets in the stack selector box."""
    
        stackType = self.stack_type.currentText()

        self.stack_unit.setEnabled(not stackType == 'Time stack')
        self.stack_incr_sbox.setEnabled(not stackType == 'Time stack')
        self.autoExposure.setEnabled(not (stackType == 'Exposure' or stackType == 'Gain'))
        self.fixedExposure.setEnabled(not (stackType == 'Exposure' or stackType == 'Gain'))
        self.fixedExposureEdit.setEnabled(not (stackType == 'Exposure' or stackType == 'Gain'))
        # self.adjustExposure.setEnabled(not (stackType == 'Exposure' or stackType == 'Gain'))

        self.stack_unit.clear()
        self.time_units.clear()

        if stackType == 'Time stack':
            # time stack
            units_list = ['s','min']
            self.time_int_lbl.setText('Time interval')

        elif stackType == 'z-position':
            # z-stack
            units_list = ['um','steps']
            self.time_int_lbl.setText('Start position (relative)')

        elif stackType == 'Exposure':
            # exposure
            units_list = ['ms']
            self.time_int_lbl.setText('Start exposure')

        elif stackType == 'Gain':
            # exposure
            units_list = ['']
            self.time_int_lbl.setText('Start gain')

        for unit in units_list:
            self.stack_unit.addItem(unit)
            self.time_units.addItem(unit)

        # define increment restrictions
        self.setIncrementRestrictions()


    def setIncrementRestrictions(self, restrict='All'):
        stackType = self.stack_type.currentText()

        if stackType == self.stack_type_list[0]:
            # time stack
            return

        elif stackType == self.stack_type_list[1]:
            # z-stack
            if restrict == 'All' or restrict == 'stack' and self.stack_unit.currentText() != '':
                smallestStep = self.main_widget.setup.motors.z.getSmallestValue(self.stack_unit.currentText())

                self.stack_incr_sbox.setValue(smallestStep)
                self.stack_incr_sbox.setMinimum(smallestStep)
                self.stack_incr_sbox.setMaximum(100*smallestStep)
                self.stack_incr_sbox.setSingleStep(smallestStep)

            if restrict == 'All' or restrict == 'time' and self.time_units.currentText() != '':
                smallestStep = self.main_widget.setup.motors.z.getSmallestValue(self.time_units.currentText())
                self.time_int_sbox.setMinimum(-1000*smallestStep)
                self.time_int_sbox.setMaximum(1000*smallestStep)
                self.time_int_sbox.setSingleStep(smallestStep)
                self.time_int_sbox.setValue(-self.number_caps.value()/2*smallestStep)


        elif stackType == 'Exposure':
            # exposure
            min_exposure = self.main_widget.setup.camera.minExposure
            max_exposure = self.main_widget.setup.camera.maxExposure

            if restrict == 'All' or restrict == 'stack':
                self.stack_incr_sbox.setValue(min_exposure)
                self.stack_incr_sbox.setMinimum(min_exposure)
                self.stack_incr_sbox.setMaximum(max_exposure)
                self.stack_incr_sbox.setSingleStep(1)

            if restrict == 'All' or restrict == 'time':
                self.time_int_sbox.setMinimum(min_exposure)
                self.time_int_sbox.setMaximum(max_exposure)
                self.time_int_sbox.setSingleStep(1)
                self.time_int_sbox.setValue(1)


        elif stackType == 'Gain':
            self.time_int_sbox.setMinimum(0)
            self.time_int_sbox.setMaximum(100)
            self.time_int_sbox.setSingleStep(1)
            self.time_int_sbox.setValue(0)

            self.stack_incr_sbox.setValue(1)
            self.stack_incr_sbox.setSingleStep(1)

        else:
            return


    def confirm_user_input(self, experimentFolder, numCaptures, folderRequired=True):
        # Test for incorrect user input
        if experimentFolder == '' and folderRequired:
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Folder name")
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setStandardButtons(QtGui.QMessageBox.Ok)
            msg.setText("Please select a valid experiment folder")
            msg.exec_()
            return -1

        if numCaptures == 0:
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Experiment Length")
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.setStandardButtons(QtGui.QMessageBox.Ok)
            msg.setText("Please a valib experiment length")
            msg.exec_()
            return -1

        return 0


    def initExperiment(self, folderRequired=True):

        # Determine where to store results
        self.experimentFolder = self.editFolder.text()
        self.image_path = self.experimentFolder + '/images'
        self.database_path = self.experimentFolder + '/database'

        # Confirm user input and delete experiment information if it is already there.
        if self.confirm_user_input(self.experimentFolder, self.number_caps.value(), folderRequired) == -1:
            return -1

        # Create file with experiment parameters for easier analysis later on
        if folderRequired:
            self.main_widget.setExperimentFolder(self.experimentFolder)

            # Create directories if non-exgaussian filteristent
            # definde directories
            directories = [self.experimentFolder, self.image_path, self.database_path]

            for directory in directories:
                if not os.path.exists(directory):
                    os.makedirs(directory)

                    # add permission (rwxr-xr-x -> 111 101 101)
                    changePermissionsPath(directory)


    def createInfo(self):
        variables = getVariables()

        # number of captures
        numCaptures = self.number_caps.value()
        experimentFolder = self.experimentFolder

        if self.num_avg_maximize.isChecked():
            numAvg = float('inf')
        else:
            numAvg = self.num_avg.value()

        experimentType = self.stack_type.currentText()

        # Adjust timeInterval depending on time unit
        variableName = list(variables.keys())[list(variables.values()).index(self.time_int_lbl.text())]
        value = float(self.time_int_sbox.value())

        if self.time_units.currentText() == 'min': value *= 60
        if self.time_units.currentText() == 'ms' : value /= 1000
        
        if self.time_units.currentText() == 'um':value = self.main_widget.setup.motors.z.returnSteps(value,'um')

        print(self.time_units.currentText())
        print(self.main_widget.setup.motors.z.returnSteps(value,'um'))

        if experimentType == 'Gain':
            locals()[variableName] = int(value)
        else:
            locals()[variableName] = value

            print(variableName)
            print(value)


        experimentFolder = self.editFolder.text()

        mologram = self.getMologram()
            
        algorithm = self.algorithms.currentText()
        if experimentType != 'Exposure':
            autoExposure = self.autoExposure.isChecked()
            if not autoExposure:
                fixedExposure = float(self.fixedExposureEdit.text())

        if experimentType != 'Exposure':
            adjustExposure = self.adjustExposure.isChecked()
            if not adjustExposure:
                fixedExposure = float(self.fixedExposureEdit.text())

        if self.save_image.isChecked():
            if self.crop_image.isChecked():
                saveImages = 'crop'
            else:
                saveImages = 'original'
        else:
            saveImages = 'False'

        subtractPreviousImage = self.subtractImage.isChecked()

        if experimentType != self.stack_type_list[0]: #time stack
            stackIncrement = float(self.stack_incr_sbox.value())
            if experimentType == self.stack_type_list[1]: # z-stack
                stackIncrement = self.main_widget.setup.motors.z.returnSteps(stackIncrement,self.stack_unit.currentText())
                if stackIncrement == 0: stackIncrement = 1  # at least 1 step

        info = dict() # dict with description
        self.log = dict() # dict with variables

        for variable, description in variables.items():
            info[description] = locals().get(variable, None)
            if type(locals().get(variable)) == list:
                self.log[variable] = str(locals().get(variable, None))
            else:
                self.log[variable] = locals().get(variable, None)

        return info


    def createLogEntry(self, folder):

        createDB(folder + '/data.db', 'logExperiment', getLogExperimentDict())
        if 'log' not in dir(self):
            # log dictionary if not exist
            info = self.createInfo()

        self.log['date'] = str(datetime.date.today())

        saveDB(folder + '/data.db', 'logExperiment', self.log)
        changePermissionsPath(folder + '/data.db')

        createDB(folder + '/data.db', 'results', getResultsDict())

        log = loadDB(folder + '/data.db', 'logExperiment')
        if type(log) != list:
            return 1

        return len(log)


    def start_f(self):
        """
        Fetch all parameters for experiment from GUI and 
        launch the experiment in a seperate thread
        """

        # init experiment
        if self.main_widget.cameraWidget.cameraControl.referenceMologram.isChecked():
            self.main_widget.plotTab.plot_Reference.setChecked(True)
        else:
            self.main_widget.plotTab.plot_Reference.setChecked(False)
        self.initExperiment()
        info = self.createInfo()
        ID = self.createLogEntry(self.database_path)

        # Thread for recording the Experiment
        self.experiment = Experiment(self.main_widget, self.experimentFolder)

        self.connect(
            self.experiment, 
            QtCore.SIGNAL("database_updated"), 
            lambda: self.main_widget.updateImg(Stack=True)
        )

        self.connect(
            self.experiment, 
            QtCore.SIGNAL("done_experiment"), 
            self.done_experiment
        )

        self.stop.clicked.connect(self.experiment.stop)
        self.pause.clicked.connect(self.experiment.pause)


        # focus
        if self.focus.isChecked():
            if self.main_widget.cameraWidget.cameraFeed.signalIntensity.text() == '0' \
                    or self.main_widget.cameraWidget.cameraFeed.signalIntensity.text() == '':
                # coarse find maximum if no molo intensity given
                self.experiment.focusAndOptimizeIncoupling(numberOfSteps=10, stepSize=10)

            # find maximum fine
            self.experiment.focusAndOptimizeIncoupling()


        # center
        if self.center.isChecked():
            if self.main_widget.cameraWidget.cameraFeed.signalIntensity.text() == '0' \
                    or self.main_widget.cameraWidget.cameraFeed.signalIntensity.text() == '':
                # coarse find maximum if no molo intensity given
                print('No focus found, could not center')
            else:
                # find maximum fine
                self.experiment.centerImage()

        self.experiment.record_data(
                experimentType = self.log['experimentType'],
                mologram = self.log['mologram'],

                numCaptures = self.log['numCaptures'],
                numAvg = self.log['numAvg'],
                timeInterval = self.log['timeInterval'],

                stackIncrement = self.log['stackIncrement'],
                startPosition = self.log['startPosition'],
                startExposure = self.log['startExposure'],
                startGain = self.log['startGain'],

                ID = ID,
                algorithm = self.log['algorithm'],

                autoExposure = self.log['autoExposure'],
                adjustExposure = self.log['adjustExposure'],
                fixedExposure = self.log['fixedExposure'],

                saveImages = self.log['saveImages'],
                subtractPreviousImage = self.log['subtractPreviousImage']
            )


    def numAvgMaxClicked(self):
        if self.num_avg_maximize.isChecked():
            self.num_avg.setEnabled(False)

        else:
            self.num_avg.setEnabled(True)

    """ Once experiment is done or if it stopped unexpectedly, show appropriate message box and update GUI """
    def done_experiment(self):

        # Update all displays
        self.main_widget.updateAll()


    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '/home/focal-molography/Experiments')
        self.editFolder.setText(folder)
        self.main_widget.setExperimentFolder(folder)


    def saveImageChecked(self):
        self.crop_image.setEnabled(self.save_image.isChecked())


    def getHeightPosition(self):
        """
        Returns whether the current position or the set position is at the surface or the mologram
        """
        surface_height = self.main_widget.manualControlTab.savedPositions.heights["surface_height"]
        mologram_height = self.main_widget.manualControlTab.savedPositions.heights["mologram_height"]

        position = self.main_widget.getPosition(absolute=False)
        # set measure
        if self.mologram_pos_checkbox.isChecked():
            # if current position is checked
            if position['Z'] <= surface_height+0.01 \
                    and position['Z'] >= surface_height-0.01:
                measure = 'Surface'

            elif position['Z'] <= mologram_height+0.01 \
                    and position['Z'] >= mologram_height-0.01:
                measure = 'Focal plane'

            else:
                measure = position['Z']
                
        else:
            if self.measureMologram.isChecked(): measure = 'Mologram'
            if self.measureSurface.isChecked(): measure = 'Surface'

        return measure


    def getMologram(self):
        # get postion
        if self.mologram_pos_checkbox.isChecked():
            position = self.main_widget.getPosition(absolute=False)
            field, row, line = self.main_widget.setup.chip.correspondingMologram((position['X'],position['Y']))
            mologram = '{},{},{}'.format(field, row, line)
        else:
            mologram = self.mologram_pos.position()

        return mologram


    def addToExperiment(self):
        rows = [] # rows that will be added in the experiment tab
        self.initExperiment(folderRequired = False)

        # add goto if mologram is selected
        timeRequiredSeconds = 10    # todo: calculation possible?
        timeRequired = time.strftime('%H:%M:%S', time.gmtime(timeRequiredSeconds))

        expType = 'Go to position'

        # get height name and molo name
        measure = self.getHeightPosition()
        molo = self.getMologram()

        # infotext
        infoStr = 'Height: {}'.format(measure)
        # define row
        rows.append([timeRequired, expType, molo, infoStr])

        # focus
        if self.focus.isChecked():
            timeRequiredSeconds = 10    # todo: calculation possible?
            timeRequired = time.strftime('%H:%M:%S', time.gmtime(timeRequiredSeconds))
            expType = 'Focus'
            rows.append([timeRequired, expType, molo, infoStr])


        # center
        if self.center.isChecked() and not self.measureSurface.isChecked():
            timeRequiredSeconds = 5    # todo: calculation possible?
            timeRequired = time.strftime('%H:%M:%S', time.gmtime(timeRequiredSeconds))
            expType = 'Center'
            rows.append([timeRequired, expType, molo, infoStr])


        # stack
        info = self.createInfo()

        # delete entries which are none
        experimentEntry = dict()
        for key, value in info.items():
            if info[key] != None:
                experimentEntry[key] = value

        experimentEntry.pop('Experiment folder', None)
        stackType = experimentEntry.pop('Experiment type',None)
        molo = experimentEntry.pop('Mologram', None)

        try:
            timeRequiredSeconds = int(self.log['numCaptures'])*self.log['timeInterval']
        except:
            timeRequiredSeconds = int(self.log['numCaptures'])*1 # 1s per measurement (todo)
        timeRequired = time.strftime('%H:%M:%S', time.gmtime(timeRequiredSeconds))
        infoStr = str(experimentEntry).replace(", ","\n").replace("{","").replace("}","").replace("'","")
        rows.append([timeRequired, stackType, molo, infoStr])

        for row in rows:
            self.main_widget.experimentTab.addRow(row)
