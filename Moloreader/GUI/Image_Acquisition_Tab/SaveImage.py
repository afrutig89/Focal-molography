from PyQt4 import QtGui,QtCore
import os

class SaveImage(QtGui.QWidget):
    def __init__(self,main_widget):
        super(SaveImage,self).__init__()

        self.main_widget = main_widget
        
        self.initUI()

        self.auto_name.setChecked(True)
        self.crop_image.setChecked(True)

    def initUI(self):

        # declare widgets
        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Destination')
        self.editName_lbl = QtGui.QLabel('Image Name')
        self.save_lbl = QtGui.QLabel('')

        # crop the image to 60 x 60 um in order to save diskspace. 
        
        self.crop_image = QtGui.QCheckBox('Crop image to 60 x 60 um')

        # line edits
        self.editFolder = QtGui.QLineEdit()
        self.editName   = QtGui.QLineEdit()

        # buttons    
        self.save = QtGui.QPushButton('save')
        self.save.clicked.connect(self.save_f)
        self.save.resize(self.save.sizeHint())

        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.browse_f)
        self.browse.resize(self.save.sizeHint())

        # line edit for automatic naming
        self.auto_name = QtGui.QRadioButton('automatic naming')
        self.auto_name.toggled.connect(lambda: self.editName.setEnabled( not(self.auto_name.isChecked()) ) )
        

        # image counter line
        self.image_counter_lbl = QtGui.QLabel('Image Counter')
        self.image_counter   = QtGui.QLineEdit()
        self.image_counter.setText("1")

        # Molocomposition ridges
        self.ridges_lbl = QtGui.QLabel('Ridges')
        self.ridges   = QtGui.QLineEdit()
        self.ridges.setText("NH-Bi")

        # Molocomposition grooves
        self.grooves_lbl = QtGui.QLabel('Grooves')
        self.grooves   = QtGui.QLineEdit()
        self.grooves.setText("NH-PEG")

        # layout
        hbox_folder = QtGui.QHBoxLayout()
        hbox_name   = QtGui.QHBoxLayout()
        hbox_crop_image   = QtGui.QHBoxLayout()
        
        hbox_image_naming   = QtGui.QHBoxLayout()

        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        hbox_name.addWidget(self.editName)
        hbox_name.addWidget(self.auto_name)

        hbox_crop_image.addWidget(self.crop_image)

        hbox_image_naming.addWidget(self.ridges_lbl)
        hbox_image_naming.addWidget(self.ridges)
        hbox_image_naming.addWidget(self.grooves_lbl)
        hbox_image_naming.addWidget(self.grooves)
        hbox_image_naming.addWidget(self.image_counter_lbl)
        hbox_image_naming.addWidget(self.image_counter)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.editFolder_lbl, hbox_folder)
        fbox.addRow(self.editName_lbl, hbox_name)
        fbox.addRow(hbox_crop_image)
        fbox.addRow(hbox_image_naming)
        fbox.addRow(self.save_lbl, self.save)
        
        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(fbox)
        self.setLayout(hbox)

    def save_f(self):
        # to be reimplemented that it accesses the camerafeed.
        if self.editFolder.text() == '':
            self.editFolder.setText('./image_captures')

        if self.auto_name.isChecked():

            counter = self.image_counter.text()
            self.image_counter.setText(str(int(counter) + 1 ))
            file_name = self.main_widget.getImageName()
            file_name += '_' + self.ridges.text() + '_' + self.grooves.text() + '_' + counter
            self.editName.setText(file_name)

        img,retval = self.main_widget.setup.camera.get_raw_img()

        if retval == False:
            QtGui.QMessageBox.warning(None, QtGui.qApp.tr("Cannot Capture Image"),
             QtGui.qApp.tr("Unable to capture image, please try again."),
             QtGui.QMessageBox.Cancel)            

        file = self.editFolder.text()+'/'+self.editName.text()
        if self.crop_image.isChecked():
            # crop an image of 60 x 60 um
            self.main_widget.setup.camera.crop_and_save_img(file, img)
        else:
            self.main_widget.setup.camera.save_img(file, img)

        # add permission (rwxr-xr-x -> 111 101 101)
        os.chmod(file, 0o755)
        os.chmod(self.editFolder.text(), 0o755)

        # fix ownership
        uid = os.environ.get('SUDO_UID')
        gid = os.environ.get('SUDO_GID')
        if uid is not None:
            os.chown(file, int(uid), int(gid))
            os.chown(self.editFolder.text(), int(uid), int(gid))

    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', 
                '/home/focal-molography/Experiments/')

        self.editFolder.setText(folder)

