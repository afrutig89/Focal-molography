from PyQt4 import QtGui, QtCore
from Reader_Control.Axis_Control.Move_motors import Move_to_mologram_thread

import numpy as np
# matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

class MologramField(QtGui.QWidget):

	def __init__(self,main_widget):

		super(MologramField,self).__init__()

		self.main_widget = main_widget

		self.initUI()

		self.in_movement = False

		# initialize thread to do movements
		self.worker_thread = Move_to_mologram_thread(
									self.main_widget.setup.chip,
									self.main_widget.setup.motors
								)
		self.connect(self.worker_thread, QtCore.SIGNAL("done_moving"), self.main_widget.stop_moving)


	def initUI(self):
		# define chip
		self.chip = self.main_widget.setup.chip

		# layout		
		vbox = QtGui.QVBoxLayout()
		self.setLayout(vbox)
		
		# set up canvas
		self.figure = plt.figure(figsize=(3,10),facecolor='None',edgecolor='None')
		self.canvas = FigureCanvas(self.figure)
		vbox.addWidget(self.canvas)

		# image parameters, axes refer to setup axes
		self.x_max = self.chip.width
		self.y_max = self.chip.height

		self.ax = self.figure.add_subplot(111)
		self.ax.set_aspect('equal')
		self.figure.tight_layout()

		# initial display
		self.updateDisplay()	

		cid = self.figure.canvas.mpl_connect('button_press_event', self.onclick)


	def updateDisplay(self):
		self.updatePos()

		# clear previous drawings (gives me an error, todo!)
		try: plt.sca(self.ax)
		except: pass
		plt.cla()

		# draw features
		self.draw_chip()
		self.draw_current_field()	

		# set up axes
		self.ax.xaxis.set_ticks_position('none')   # tick markers
		self.ax.yaxis.set_ticks_position('none')
		
		yticks = [self.chip.getYposition(chr(field),1) for field in np.arange(self.chip.number_of_fields)+ord(self.chip.starting_field_index)]
		yticks += np.zeros(len(yticks)) + self.chip.fields.bottom_margin + self.chip.fields.field_height/2
		yticks_labels = [chr(field) for field in np.arange(self.chip.number_of_fields)+ord(self.chip.starting_field_index)]
		
		plt.xticks([])                        # labels 
		plt.yticks(yticks, yticks_labels)
		plt.axis([0,self.x_max,0,self.y_max]) # axis dimensions
		plt.gca().invert_yaxis()

		# show image
		self.canvas.draw()


	def updatePos(self):
		"""
		Links the current position with the chip position.
		"""

		self.x = self.main_widget.setup.motors.x.pos('mm')
		self.y = self.main_widget.setup.motors.y.pos('mm')
		position = self.x, self.y
		self.field, self.line, self.row = self.main_widget.setup.chip.correspondingMologram(position)

		return position


	def draw_current_field(self):		
		
		# background and current view
		view_size = 2

		# disregard if not in a mologram field
		if self.field == -1:
			return

		# get x,y location of current field
		x = self.chip.fields.side_margin
		y = self.chip.fields.bottom_margin
		y += (ord(self.field) - ord(self.chip.starting_field_index))*self.chip.fields.field_spacing

		self.ax.add_patch(patches.Rectangle(
			(x,y),
			self.chip.fields.field_width,
			self.chip.fields.field_height,
			facecolor='#00CC00'
		))


	def draw_chip(self):

		# chip
		self.ax.add_patch(patches.Rectangle((0,0),self.x_max,self.y_max,facecolor='#F8F8F8'))

		# mologram fields
		for i in range(self.chip.number_of_fields):

			x  = self.chip.fields.side_margin
			y  = self.chip.fields.bottom_margin
			y += i*self.chip.fields.field_spacing

			self.ax.add_patch(patches.Rectangle(
				(x,y),
				self.chip.fields.field_width,
				self.chip.fields.field_height,
				facecolor='#262626'
			))


	def onclick(self,event):

		# don't move if already in movement
		if self.in_movement == True:
			return

		# get position of mouse click and return if it was outside figure
		try:
			x  = event.xdata
			y  = event.ydata
		except Exception:
			return

		# get corresponding mologram indices
		position = (x,y)
		field, line, row = self.main_widget.setup.chip.correspondingMologram(position)
		
		if self.line == -1:
			line = 1
		else:
			line = self.line

		if self.row == -1:
			row = 1
		else:
			row  = self.row

		# got to it
		print('move to {},{},{}'.format(field,line,row))
		self.main_widget.set_in_movement(True)
		self.worker_thread.move_to('{},{},{}'.format(field, line, row))

	def heightForWidth(self, w):
		return w*(57/14)

	def set_in_movement(self,state):
		self.in_movement = state