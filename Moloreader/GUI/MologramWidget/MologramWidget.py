"""
This module initializes the mologram widget: on the left side 
the field with the given molograms and on the right side the 
chip with the different fields.

:author:	Louis Pahlavi
:revised:	Silvio Bischof 31-Jan-2017
"""

from PyQt4 import QtGui, QtCore

from GUI.MologramWidget.MologramLineRow import MologramLineRow
from GUI.MologramWidget.MologramField import MologramField


class MologramWidget(QtGui.QWidget):
	""" Constructor """
	def __init__(self,main_widget):

		# Call parent class constructor
		super(MologramWidget,self).__init__()

		# Assign main layout
		self.main_widget = main_widget

		# Call child constructor
		self.initUI()

	def initUI(self):

		self.mologramLineRow = MologramLineRow(self.main_widget)
		self.mologramField   = MologramField(self.main_widget)

		hbox = QtGui.QHBoxLayout()
		self.setLayout(hbox)

		hbox.addWidget(self.mologramLineRow)
		hbox.addStretch(1)
		hbox.addWidget(self.mologramField)
		hbox.addStretch(1)

	def updateDisplay(self):
		self.mologramLineRow.updateDisplay()
		self.mologramField.updateDisplay()

	def set_in_movement(self,state):		
		self.mologramLineRow.set_in_movement(state)
		self.mologramField.set_in_movement(state)