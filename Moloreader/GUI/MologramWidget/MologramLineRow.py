"""
Mologram widget: sets up the field-view and the chip view with the 
different fields.
"""

import numpy as np
from PyQt4 import QtGui, QtCore
from Reader_Control.Axis_Control.Move_motors import Move_to_mologram_thread

# matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar


class MologramLineRow(QtGui.QWidget):

	def __init__(self,main_widget):

		super(MologramLineRow,self).__init__()

		self.main_widget = main_widget

		self.in_movement = False

		# initialize thread to do movements
		self.worker_thread = Move_to_mologram_thread(
									self.main_widget.setup.chip,
									self.main_widget.setup.motors
								)
		self.connect(self.worker_thread, QtCore.SIGNAL("done_moving"), self.main_widget.stop_moving)

		self.initUI()


	def initUI(self):

		# layout		
		vbox = QtGui.QVBoxLayout()
		self.setLayout(vbox)
		
		# set up canvas
		self.figure = plt.figure(figsize=(10,5),facecolor='None',edgecolor='None')
		self.canvas = FigureCanvas(self.figure)
		vbox.addWidget(self.canvas)

		# initial display
		self.initDisplay()	

		cid = self.figure.canvas.mpl_connect('button_press_event', self.onclick)


	def initDisplay(self):
		# define chip
		self.chip = self.main_widget.setup.chip
		self.updatePos()

		# clear previous drawings
		if 'ax' in dir(self):
			self.ax.cla()
		self.ax = self.figure.add_subplot(111)
		self.ax.set_aspect('equal')

		# draw features
		self.draw_current_field()
		self.draw_current_mologram()

		# set up axes
		plt.axis(
			[-self.chip.fields.hor_spacing/2, self.chip.fields.field_width-self.chip.fields.hor_spacing/2,
			 -self.chip.fields.ver_spacing/2, self.chip.fields.field_height-self.chip.fields.ver_spacing/2]) # axis dimensions

		# invert axis such as B,1,1 is at 0,0
		plt.gca().xaxis.tick_top()
		plt.gca().invert_yaxis()
		self.yticks_labels = plt.gca().get_yticks()

		self.figure.tight_layout()
		# show image
		self.canvas.draw()


	def updateDisplay(self):
		position = self.updatePos()

		y_offset = self.chip.fields.field_spacing*(ord(self.field)-ord(self.chip.starting_field_index))
		yticks_labels = ['%.1f' % (ytick+y_offset) for ytick in self.yticks_labels]
		
		self.ax.set_yticklabels(yticks_labels)

		try:
			self.curr_mologram.remove()
		except Exception:
			pass

		self.draw_current_mologram()

		# show image
		self.canvas.draw()


	def updatePos(self):
		"""
		Saves current position and mologram in object.
		"""

		self.x = self.main_widget.setup.motors.x.pos('mm')
		self.y = self.main_widget.setup.motors.y.pos('mm')
		position = self.x, self.y
		self.field, self.line, self.row = self.main_widget.setup.chip.correspondingMologram(position)

		return position


	def draw_current_mologram(self):

		# define rectangle size
		pixelsize = self.main_widget.setup.camera.specifications.pixelsize
		pixelsize /= 1000 	# convert to mm
		npix = self.main_widget.setup.camera.specifications.npix
		magnification = self.main_widget.setup.camera.specifications.magnification

		view_width  = npix[0]*pixelsize/magnification
		view_height = npix[1]*pixelsize/magnification

		# disregard if not in a mologram field
		if (self.field == -1) or (self.line == -1) or (self.row == -1):
			return

		y_offset = self.chip.fields.field_spacing*(ord(self.field)-ord(self.chip.starting_field_index))

		# get x,y location of current field
		x = self.main_widget.setup.motors.x.pos('mm') - view_width/2
		y = self.main_widget.setup.motors.y.pos('mm') - y_offset - view_height/2

		# x = self.main_widget.setup.chip.getXposition(self.row) - view_width/2
		# y = self.main_widget.setup.chip.getYposition(self.chip.starting_field_index,self.line) - view_height/2

		self.curr_mologram = (patches.Rectangle(
			(x,y),
			view_width,
			view_height,
			facecolor='#0A0A0A',			
			alpha=0.2,
			zorder = 2
		))

		self.ax.add_artist(self.curr_mologram)


	def draw_mologram(self, position, image, zoom = 0.5):

		self.mologram = read_png('./images/{}.png'.format(image))
		# self.mologram = read_png('./images/mologram_rotated.png')

		imagebox = OffsetImage(self.mologram, zoom=zoom)

		ab = AnnotationBbox(
			imagebox, 
			position,
			xybox= None,
			xycoords='data',
			boxcoords="offset points",
			frameon=False
		)
		ab.zorder=1
		
		self.ax.add_artist(ab)


	def draw_current_field(self):

		# draw all molograms in current field
		for i in range(self.chip.fields.lines):
			for j in range(self.chip.fields.molos):
				self.draw_mologram(
						self.chip.fields.molograms[i][j].position,
						image = self.chip.fields.molograms[i][j].image
					)


	def onclick(self,event):

		# if already in movement, don't move
		if self.in_movement == True:
			return

		# get position of mouse click and return if it was outside figure
		try:
			x  = event.xdata
			y  = event.ydata
		except Exception:
			return

		# get corresponding mologram indices
		field, line, row = self.main_widget.setup.chip.correspondingMologram((x,y))

		# in case the current view is not in a valid field, move to the selected mologram in the 'B' field
		if field == -1:
			field = 'B'
		else:
			field = self.field

		x, y = self.chip.cartesian_position('{},{},{}'.format(field, line, row))
		print('{},{},{}: ({},{})'.format(field, line, row, x, y))

		self.worker_thread.move_to('{},{},{}'.format(field, line, row))


	def heightForWidth(self, w):
		return w*(1/2)


	def set_in_movement(self,state):
		""" defines the movement state """

		self.in_movement = state