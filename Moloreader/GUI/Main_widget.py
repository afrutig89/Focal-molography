from PyQt4 import QtGui,QtCore, QtSql
from PyQt4.QtWebKit import QWebView
from PyQt4.QtCore import QUrl
from PyQt4.QtGui import QMessageBox
import time

import sys
sys.path.append('../')
from settings import Chip
from PIL import Image
import json
import numpy as np

from Reader_Control.Setup_System import Setup
from GUI.CameraWidget.CameraWidget import CameraWidget
from GUI.MologramWidget.MologramWidget import MologramWidget
from GUI.ExperimentTab.ExperimentWidget import ExperimentTab
from GUI.Image_Acquisition_Tab.ImageAquisitionTab import ImageAquisitionTab

from GUI.Manual_Motor_Control_Tab.ManualControlTab import ManualControlTab

from GUI.CalibrationTab.CalibrationTab import CalibrationTab

from GUI.PlotTab.PlotTab import PlotTab
from GUI.PhotodiodeTab.PhotodiodeTab import PhotoDiodeTab
from GUI.FigureTab.FigureTab import FigureTab
from GUI.SettingsTab.settingsTab import SettingsTab
from GUI.ProtocolTab.protocolWidget import ProtocolWidget
from GUI.PostProcessingTab.PostProcessingTab import PostProcessingWidget



class MainWidget(QtGui.QWidget):
    # Constructor
    def __init__(self):

        # Call parent class constructor
        super(MainWidget,self).__init__()

        # Initialize member variables        
        self.enable_image_processing = True

        # Determine whether in offline mode
        self.offline_mode = sys.version_info < (3,0)

        # Call child constructor
        self.initUI()
        # self.set_offline_mode()

        # Update image and positions
        self.updateAll()


    # Initialization of GUI
    def initUI(self):
        
        # Create widgets, pass self so they can talk to each other
        print('> Setting up widgets')


        # setup motor, chip
        self.setup = Setup(self)

        # tabs
        self.tabs_bot = QtGui.QTabWidget()
        self.tabs_top = QtGui.QTabWidget()

        if not self.offline_mode:
            self.mologramTab = MologramWidget(self)
            self.manualControlTab = ManualControlTab(self)
            self.experimentTab = ExperimentTab(self)
            self.calibrationTab = CalibrationTab(self)

            # bottom taps
            self.tabs_bot.addTab(self.manualControlTab, 'Manual Motor Control')
            self.tabs_bot.addTab(self.experimentTab, 'Experiment')
            self.tabs_bot.addTab(self.calibrationTab, 'Calibration')

            # top taps
            self.tabs_top.addTab(self.mologramTab,'Mologram Position')

        # documentation
        self.documentationTab = QWebView()
        self.documentationTab.load(QUrl('../documentation/_build/html/index.html'))
        self.experimentsProtocols = QWebView()
        self.experimentsProtocols.load(QUrl('/home/focal-molography/Experiments/_build/html/index.html'))
        
        self.settingsTab = SettingsTab(self)

        # top taps
        self.plotTab = PlotTab(self)
        self.figureTab = FigureTab(self)
        self.photodiodeTab = PhotoDiodeTab(self)
        
        # set up widgets
        if not self.offline_mode:
            self.cameraWidget = CameraWidget(self)
            self.imageAquisitionTab = ImageAquisitionTab(self)
            self.tabs_bot.insertTab(1, self.imageAquisitionTab, 'Image Acquisition')

        else:
            self.cameraWidget = QtGui.QLabel('')

        self.textBrowser = QtGui.QTextEdit() 

        self.postProcessingTab = PostProcessingWidget(self)
        self.protocol = ProtocolWidget(self)

        # bottom taps
        self.tabs_bot.addTab(self.postProcessingTab, 'Image Processing')
        self.tabs_bot.addTab(self.settingsTab, 'Settings')
        self.tabs_bot.addTab(self.protocol, 'Protocol')

        
        
        self.tabs_top.addTab(self.plotTab, 'Database Plots')
        self.tabs_top.addTab(self.figureTab, 'Figures')
        self.tabs_top.addTab(self.photodiodeTab,'Photodiode Feed')

        print('> Done setting up widgets')
    
        # declare layouts
        self.main_hbox  = QtGui.QHBoxLayout()
        left_vbox  = QtGui.QVBoxLayout()
        right_vbox = QtGui.QVBoxLayout()

        top_left_hbox = QtGui.QHBoxLayout()
        btm_left_vbox = QtGui.QVBoxLayout()

        v_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
        v_splitter.addWidget(self.tabs_top)
        v_splitter.addWidget(self.tabs_bot)

        v_splitter2 = QtGui.QSplitter(QtCore.Qt.Vertical)
        v_splitter2.addWidget(self.cameraWidget)
        v_splitter2.addWidget(self.textBrowser)
        self.textBrowser.setVisible(False)

        h_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        h_splitter.addWidget(v_splitter)
        h_splitter.addWidget(v_splitter2)

        self.main_hbox.addWidget(h_splitter)

        self.setLayout(self.main_hbox)

    def setExperimentFolder(self, experimentFolder):
        if experimentFolder == '':
            # no folder chosen
            return

        self.experimentFolder = experimentFolder
        self.settingsTab.experimentSpecifications.editFolder.setText(experimentFolder)

        if not self.offline_mode:
            self.imageAquisitionTab.stack.editFolder.setText(experimentFolder)
            self.imageAquisitionTab.arrayMeasurements.editFolder.setText(experimentFolder)
            self.experimentTab.editFolder.setText(experimentFolder)


    def getExperimentFolder(self):
        if 'experimentFolder' in dir(self):
            return self.experimentFolder
        else:
            return ''


    def ask_offline_mode(self):
        msg = QtGui.QMessageBox()
        msg.setWindowTitle("Offline mode")
        msg.setIcon(QtGui.QMessageBox.Question)
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg.setText("Do you wish to start in offline mode?")
        retval = msg.exec_()

        return retval == QtGui.QMessageBox.Yes

    def set_offline_mode(self):
        """
        Todo!
        """
        self.offline_mode = not self.offline_mode

        # Disable tabs that are not useful in offline mode
        self.tabs_top.setTabEnabled(0, not self.offline_mode)    # Mologram Position
        self.tabs_bot.setTabEnabled(0, not self.offline_mode)    # Manual motor control
        self.tabs_bot.setTabEnabled(1, not self.offline_mode)    # Image Acquisition
        self.tabs_bot.setTabEnabled(2, not self.offline_mode)    # Experiment
        self.tabs_bot.setTabEnabled(3, not self.offline_mode)    # Calibration


    # linked to the main calibration button
    def calib(self, axis='XYZTEMS', goToReference=True):
        """
        Main calibration function. Calibrates the axes that are defined.

        :param axis: axes that are calibrated. Available axes: 'X','Y','Z','TE','MS'.

        :example:

            self.calib('XYZ')
            -> calibrates x, y and z axes
        """

        if not self.offline_mode:
            # calibrate motor and move to reference position
            print('>> Calibrate motors')
            self.setup.motors.calib(axis, goToReference)
            
            if goToReference:
                if 'X' in axis:
                    x = self.setup.chip.getXposition(1)
                    self.setup.motors.x.set_position(x, 'mm')

                if 'Y' in axis:
                    y = self.setup.chip.getYposition('B',1)
                    self.setup.motors.y.set_position(y, 'mm')

                if 'Z' in axis:
                    # set surface position to 0
                    self.setup.motors.z.set_position(0, 'mm')
                    self.manualControlTab.savedPositions.onSetSurfaceHeightClicked()

            # update all
            self.updateAll()

            # text message that calibration is done
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Calibration")
            msg.setIcon(QtGui.QMessageBox.Information)
            msg.setStandardButtons(QtGui.QMessageBox.Ok)

            msg.setText("Calibration is finshed")
            msg.exec_()

    def updatePos(self):
        if not self.offline_mode:
            # save old position
            self.oldPosition = self.getPosition()

            # update position
            self.manualControlTab.axisControl.updatePos()
            self.mologramTab.updateDisplay()

    def setPosToZero(self):
        """When the permanent reference is set then all the axis are set to zero, which corresponds to the position B-1-1"""
        self.setup.motors.x.set_position(0, 'mm')
        self.setup.motors.y.set_position(0, 'mm')
        self.setup.motors.z.set_position(0, 'mm')
        self.setup.motors.TE.set_refposition()
        self.setup.motors.MS.set_refposition()


    def updateImg(self, Stack=False):
        if not self.offline_mode:
            # if not in offline mode
            if self.cameraWidget.cameraControl.referenceMologram.isChecked():
            # if referencing mode is selected
                if Stack == True:
                # plot both signal and reference signal data
                    self.plotTab.plotReference()
                else:
                # simply update the image
                    self.cameraWidget.cameraFeed.updateCameraFeed()

            else:
                if Stack == True:
                    self.plotTab.updateImage()

                else:
                    self.cameraWidget.cameraFeed.updateCameraFeed()


    def updateAll(self):
        self.updatePos()
        self.updateImg()


    def updateAllStack(self):
        # special update function during stack execution, because the camera should not
        # be used to get additional images otherwise the software crashes.
        self.tabs_top.setCurrentIndex(1)
        self.updatePos()
        self.updateImg(Stack=True)


    def toggle_enable_image_processing(self):
        self.enable_image_processing = not(self.enable_image_processing)
        self.tabs_bot.setTabEnabled(4,self.enable_image_processing)
        self.updateImg()


    def redraw(self):
        self.app.processEvents()


    """ Tell the GUI that the current motor is in movemnt or not """
    def set_in_movement(self, state):
        self.manualControlTab.axisControl.enableButtons(not state)
        self.mologramTab.set_in_movement(state)


    def stop_moving(self,updatePositionOnly = False):
        self.set_in_movement(False)
        if updatePositionOnly:
            self.updatePos()
        else:
            self.updateAll()

    def move_to_nothread(self, position, updatePositionOnly = True):
        """
        Moves motors to a certain position. The variable position can be a string (e.g. 'B,3,1')
        or a tuple of len 2 (only x,y) or 3 (x,y and z).
        """
        self.set_in_movement(True)
        chip = self.setup.chip
        motors = self.setup.motors
        if type(position) == str:
            # if position is given as mologram number (e.g. "B,3,1")
            # get x,y,z coordinates of new location
            x, y = chip.cartesian_position(position)
            # z-axis not adjusted
            z = motors.z.pos('mm')

        else:
            if len(position) == 3:
                x, y, z = position
            elif len(position) == 2:
                # z not defined: no movement in z-direction
                x, y = position
                z = motors.z.pos('mm')
            else:
                # wrong format of position
                return

        # go to new position
        delta_x = x - motors.x.pos('mm')
        delta_y = y - motors.y.pos('mm')
        delta_z = z - motors.z.pos('mm')

        print('move {},{},{}'.format(delta_x,delta_y,delta_z))

        motors.x.move_mm(delta_x)
        motors.y.move_mm(delta_y)
        motors.z.move_mm(delta_z)

        self.stop_moving(updatePositionOnly)

    def getPosition(self, absolute=True):
        if not self.offline_mode:
            if absolute == True:
                return self.manualControlTab.axisControl.getPositionAbs()
            if absolute == False: # relative
                return self.manualControlTab.axisControl.getPositionRel()


    def changeMask(self, mask):

        currentTabBot = self.tabs_bot.currentIndex()
        currentTabTop = self.tabs_top.currentIndex()

        self.setup.chip = Chip(mask=mask)
        self.manualControlTab = ManualControlTab(self)
        self.mologramTab = MologramWidget(self)
        self.imageAquisitionTab = ImageAquisitionTab(self)
        self.figureTab = FigureTab(self)

        self.tabs_top.clear()
        self.tabs_top.addTab(self.mologramTab,'Mologram Position')
        self.tabs_top.addTab(self.plotTab, 'Time Stack Plots')
        self.tabs_top.addTab(self.figureTab, 'Figures')
        self.tabs_top.setCurrentIndex(currentTabTop)

        self.tabs_bot.clear()
        self.tabs_bot.addTab(self.manualControlTab, 'Manual Motor Control')
        self.tabs_bot.addTab(self.imageAquisitionTab, 'Image Acquisition')
        self.tabs_bot.addTab(self.experimentTab, 'Experiment')
        self.tabs_bot.addTab(self.postProcessingTab, 'Surface analysis')
        self.tabs_bot.addTab(self.calibrationTab, 'Calibration')
        self.tabs_bot.addTab(self.settingsTab, 'Settings')
        self.tabs_bot.addTab(self.protocol, 'Protocol')
        self.tabs_bot.setCurrentIndex(currentTabBot)

        self.cameraWidget.cameraControl.braggMask.setVisible(self.setup.chip.mask == 'Bragg')


    def getImageName(self):
        """
        Returns image name ([ChipNo]-[field]-[line]-[row]_exp-[exposure]-ms)
        """
        # position
        x = self.setup.motors.x.pos('mm')
        y = self.setup.motors.y.pos('mm')

        field, line, row = self.setup.chip.correspondingMologram((x,y)) # 
        chipNo = self.setup.chip.chipNo
        print(field, line, row)
        # exposure
        exposure = '%.2f' % self.setup.camera.exposure

        # set image name
        image_name = '{}-{}-{}-{}'.format(chipNo,field, line, row)

        return image_name


    def loadImage(self):
        file = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home/focal-molography/Experiments', "Image files (*)")
        if file != '':
            image = Image.open(file)

            try:
                json_acceptable_string = image.tag[270][0].replace("'", "\"")
                info = json.loads(json_acceptable_string)
                self.cameraWidget.cameraFeed.resetSettings(info)

            except:
                # no infos stored
                pass

            self.cameraWidget.cameraFeed.plotImage(np.array(image))


    def saveImage(self):
        self.cameraWidget.cameraFeed.saveImage(cropImage=False, chooseImagePath=True)


    def showTab(self, action):
        if 'documentation' in action.text():
            if action.isChecked():
                self.documentationTab.reload()
                self.tabs_top.insertTab(self.tabs_top.count(), self.documentationTab, 'Documentation')
                self.tabs_top.setCurrentIndex(self.tabs_top.count()-1)
            else:
                documentationTabIndex = self.tabs_top.indexOf(self.documentationTab)
                self.tabs_top.removeTab(documentationTabIndex)

        if 'experiments' in action.text():
            if action.isChecked():
                self.experimentsProtocols.reload()
                self.tabs_top.insertTab(self.tabs_top.count(), self.experimentsProtocols, 'Protocols')
                self.tabs_top.setCurrentIndex(self.tabs_top.count()-1)
            else:
                documentationTabIndex = self.tabs_top.indexOf(self.experimentsProtocols)
                self.tabs_top.removeTab(documentationTabIndex)


    def reloadTabs(self, action):
        self.documentationTab.reload()
        self.experimentsProtocols.reload()
