from PyQt4 import QtGui, QtCore

class ImageButton(QtGui.QPushButton):
    def __init__(self,image_path):
        super(ImageButton, self).__init__()
        icon  = QtGui.QIcon(image_path)
        self.setIcon(icon)