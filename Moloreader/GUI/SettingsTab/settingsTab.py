# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtCore, QtGui
from settings import Chip
import os
import numpy as np
from settings import getLogExperimentDict, getResultsDict, getVariables
from SharedLibraries.Database.dataBaseOperations import saveDB, createDB, loadDB, createSQLDict

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class SettingsTab(QtGui.QWidget):
    def __init__(self,main_widget):
        super(SettingsTab,self).__init__()

        self.main_widget = main_widget
        self.chip = self.main_widget.setup.chip
        if not self.main_widget.offline_mode:
            self.camera = self.main_widget.setup.camera

        self.initUI()


    def initUI(self):

        self.generalSettings = GeneralSettings(self.main_widget)
        self.experimentSpecifications = ExperimentSpecifications(self.main_widget)
        self.devicesSettings = DevicesSettings(self.main_widget)
        self.shortcuts = Shortcuts(self.main_widget)

        self.tabs = QtGui.QTabWidget()
        self.tabs.addTab(self.generalSettings, 'General settings')
        self.tabs.addTab(self.experimentSpecifications, 'Experiment settings')
        self.tabs.addTab(self.devicesSettings, 'Devices')
        self.tabs.addTab(self.shortcuts, 'Shortcuts')

        # box left top
        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        vbox.addWidget(self.tabs)



class GeneralSettings(QtGui.QWidget):
    def __init__(self, main_widget):
        super(GeneralSettings, self).__init__()

        self.main_widget = main_widget

        self.imageSettings = QtGui.QGroupBox('Image Acquisition')
        self.naming_lbl = QtGui.QLabel('Image naming')
        self.naming = QtGui.QLineEdit('{field}-{line}-{moloLine}_exp-{exposureTime}-ms')
        self.naming.setEnabled(False)
        self.imageAcquisition_lbl = QtGui.QLabel('Attributes')
        self.addScalebar = QtGui.QCheckBox('Save scalebar')
        self.addScalebar.setEnabled(False)
        self.addIntensity = QtGui.QCheckBox('Save intensity')
        self.addIntensity.setEnabled(False)
        self.overwriteImages = QtGui.QCheckBox('Overwrite Images')
        self.overwriteImages.setEnabled(False)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.naming_lbl, self.naming)
        fbox.addRow(self.imageAcquisition_lbl, self.addScalebar)
        fbox.addRow(QtGui.QLabel(''), self.addIntensity)
        fbox.addRow(QtGui.QLabel(''), self.overwriteImages)

        self.imageSettings.setLayout(fbox)


        self.groupDatabasePlots = QtGui.QGroupBox('Database plot')

        # variables must be in getVariables()
        self.sortByList = ['experimentID',

                            'field',
                            'line',
                            'moloLine',

                            'exposureTime',
                            'averagedImages',
                            'z_position',

                            'position']

        variables = getVariables()
        self.formGroups_lbl = QtGui.QLabel('Form groups by')
        self.formLines_lbl = QtGui.QLabel('Change line type for')

        self.formGroups = QtGui.QComboBox()
        self.formLines = QtGui.QComboBox()
        self.formLines.addItem('')

        for item in self.sortByList:
            self.formGroups.addItem(variables[item])
            self.formLines.addItem(variables[item])

        self.groupPlots = self.sortByList[self.formGroups.currentIndex()]
        if self.formLines.currentIndex() == 0:
            self.groupLines = ''
        else:
            self.groupLines = self.sortByList[self.formLines.currentIndex()-1]

        self.formGroups.activated.connect(self.formGroupsChanged)
        self.formLines.activated.connect(self.formLinesChanged)

        self.showLegend_lbl = QtGui.QLabel('Show legend')
        self.showLegend = QtGui.QCheckBox('')
        self.showLegend.setChecked(True)
        self.showLegend.clicked.connect(self.showLegendChanged)

        fbox_db = QtGui.QFormLayout()
        fbox_db.addRow(self.formGroups_lbl, self.formGroups)
        fbox_db.addRow(self.formLines_lbl, self.formLines)
        fbox_db.addRow(self.showLegend_lbl, self.showLegend)

        self.groupDatabasePlots.setLayout(fbox_db)

        self.groupBackground = QtGui.QGroupBox('Background')
        self.groupBackground.setEnabled(False)
        self.subtractBackground_lbl = QtGui.QLabel('Subtract background')
        self.subtractBackgroundCamera = QtGui.QRadioButton('Camera noise')
        self.subtractBackgroundCamera.setChecked(True)

        self.subtractBackgroundDefined = QtGui.QRadioButton('From file')
        self.subtractBackgroundDefined_folder = QtGui.QLineEdit()
        self.subtractBackgroundDefined_folder.setEnabled(False)
        self.subtractBackgroundCamera.toggled.connect(self.btnStateChanged)
        self.subtractBackgroundDefined.toggled.connect(self.btnStateChanged)
        # push button
        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.browse_f)
        self.browse.resize(self.browse.sizeHint())
        self.browse.setEnabled(False)

        hbox1 = QtGui.QHBoxLayout()
        hbox1.addWidget(self.subtractBackgroundDefined)
        hbox1.addWidget(self.subtractBackgroundDefined_folder)
        hbox1.addWidget(self.browse)

        fbox_bkg = QtGui.QFormLayout()
        fbox_bkg.addRow(self.subtractBackground_lbl, self.subtractBackgroundCamera)
        fbox_bkg.addRow(QtGui.QLabel(''), hbox1)

        self.groupBackground.setLayout(fbox_bkg)

        # layout
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.imageSettings)
        vbox.addWidget(self.groupDatabasePlots)
        vbox.addWidget(self.groupBackground)

        groupbox = QtGui.QGroupBox('')
        groupbox.setLayout(vbox)

        scroll = QtGui.QScrollArea()
        scroll.setWidget(groupbox)
        scroll.setWidgetResizable(True)
        scroll.setFixedHeight(400)

        layout = QtGui.QVBoxLayout(self)
        layout.addWidget(scroll)

        self.setLayout(layout)


    def formGroupsChanged(self):
        self.groupPlots = self.sortByList[self.formGroups.currentIndex()]
        # update database plots
        self.main_widget.updateImg(Stack=True)


    def formLinesChanged(self):
        if self.formLines.currentIndex() == 0:
            self.groupLines = ''
        else:
            self.groupLines = self.sortByList[self.formLines.currentIndex()-1]
        # update database plots
        self.main_widget.updateImg(Stack=True)


    def showLegendChanged(self):
        self.main_widget.updateImg(Stack=True)


    def browse_f(self):
        file = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home/focal-molography/Experiments')
        self.subtractBackgroundDefined_folder.setText(file)


    def btnStateChanged(self):
        self.subtractBackgroundDefined_folder.setEnabled(self.subtractBackgroundDefined.isChecked())
        self.browse.setEnabled(self.subtractBackgroundDefined.isChecked())


class ExperimentSpecifications(QtGui.QWidget):
    def __init__(self, main_widget):

        super(ExperimentSpecifications, self).__init__()
        self.main_widget = main_widget

        # labels for the form
        self.editFolder_lbl      = QtGui.QLabel('Experiment Folder')
        # line edits and selectors
        self.editFolder           = QtGui.QLineEdit()
        # push button
        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.browse_f)
        self.browse.resize(self.browse.sizeHint())

        # layout
        hbox_folder   = QtGui.QHBoxLayout()
        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        self.experimentNameLabel = QtGui.QLabel()
        self.experimentNameLabel.setText(_translate("Form", "Experiment name", None))
        self.experimentName = QtGui.QLineEdit()
        self.experimentDescriptionLabel = QtGui.QLabel()
        self.experimentDescriptionLabel.setText(_translate("Form", "Experiment description", None))
        self.experimentDescription = QtGui.QTextEdit()
        experimentSpecifications = QtGui.QFormLayout()
        experimentSpecifications.addRow(self.editFolder_lbl, hbox_folder)
        experimentSpecifications.addRow(self.experimentNameLabel, self.experimentName)
        experimentSpecifications.addRow(self.experimentDescriptionLabel, QtGui.QLabel(''))
        experimentSpecifications.addRow(self.experimentDescription)

        # layout
        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(experimentSpecifications)
        self.setLayout(vbox)


    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '/home/focal-molography/Experiments')
        self.editFolder.setText(folder)
        self.main_widget.setExperimentFolder(folder)



class DevicesSettings(QtGui.QWidget):
    def __init__(self, main_widget):
        super(DevicesSettings, self).__init__()

        self.main_widget = main_widget

        fbox1 = QtGui.QFormLayout()
        self.cameraModelLabel = QtGui.QLabel('Camera model')
        self.cameraModel = QtGui.QComboBox()
        self.cameraModel.addItem(_fromUtf8('UI-3480CP-M-GL Rev.2'))
        fbox1.addRow(self.cameraModelLabel, self.cameraModel)

        self.objectiveLabel = QtGui.QLabel('Objective')
        self.objective = QtGui.QComboBox()
        self.objective.activated.connect(self.magnificationComboUpdated)
        self.objective.addItem(_fromUtf8('20x'))
        self.objective.addItem(_fromUtf8('10x'))
        self.objective.addItem(_fromUtf8('40x'))
        self.objective.addItem(_fromUtf8('1x'))
        fbox1.addRow(self.objectiveLabel, self.objective)

        if not self.main_widget.offline_mode:
            self.gainLabel = QtGui.QLabel('Gain')
            self.gain = QtGui.QSpinBox()
            self.gain.setValue(self.main_widget.setup.camera.gain)
            self.gain.setMaximum(self.main_widget.setup.camera.specifications.gain_max)
            self.gain.valueChanged.connect(self.gainChanged)
            fbox1.addRow(self.gainLabel, self.gain)

            self.colorModeLabel = QtGui.QLabel('Color mode')
            self.colorMode = QtGui.QComboBox()
            self.objective.addItem(_fromUtf8(''))

            self.cameraInfoLabel = QtGui.QLabel('Camera info')
            self.cameraInfo = QtGui.QLabel('(?)')
            #toolTipStr = str(self.main_widget.setup.camera.info).replace(", ","\n").replace("{","").replace("}","").replace("b'","").replace("'","")
            self.cameraInfo.setToolTip('not implemented')
            fbox1.addRow(self.cameraInfoLabel, self.cameraInfo)


        self.reconnectCameraButton = QtGui.QPushButton('Reconnect')
        self.reconnectCameraButton.clicked.connect(self.reconnectCamera)
        fbox1.addRow(self.reconnectCameraButton)


        self.motorXModelLabel = QtGui.QLabel('Motor x-axis model')
        self.motorXModel = QtGui.QComboBox()
        self.motorXModel.addItem(_fromUtf8('AM-1524-2R-A-0.25-12.5-55'))

        self.motorYModelLabel = QtGui.QLabel('Motor y-axis model')
        self.motorYModel = QtGui.QComboBox()
        self.motorYModel.addItem(_fromUtf8('AM-1524-2R-A-0.25-12.5-55'))

        self.motorZModelLabel = QtGui.QLabel('Motor z-axis model')
        self.motorZModel = QtGui.QComboBox()
        self.motorZModel.addItem(_fromUtf8('AM-2224-R3-AV-4.8-85'))

        self.motorTEModelLabel = QtGui.QLabel('Motor TE-axis model')
        self.motorTEModel = QtGui.QComboBox()
        self.motorTEModel.addItem(_fromUtf8(""))

        self.motorMSModelLabel = QtGui.QLabel('Motor MS-axis model')
        self.motorMSModel = QtGui.QComboBox()
        self.motorMSModel.addItem(_fromUtf8(""))

        fbox2 = QtGui.QFormLayout()
        fbox2.addRow(self.motorXModelLabel, self.motorXModel)
        fbox2.addRow(self.motorYModelLabel, self.motorYModel)
        fbox2.addRow(self.motorZModelLabel, self.motorZModel)
        fbox2.addRow(self.motorTEModelLabel, self.motorTEModel)
        fbox2.addRow(self.motorMSModelLabel, self.motorMSModel)


        self.laserType_lbl = QtGui.QLabel('Laser type')
        self.laserType = QtGui.QComboBox()
        self.laserType.addItem(_fromUtf8('HeNe Laser'))
        self.laserWavelength_lbl = QtGui.QLabel('Wavelength [nm]')
        self.laserWavelength = QtGui.QLineEdit('632.8')
        self.laserPower_lbl = QtGui.QLabel('Power [mW]')
        self.laserPower = QtGui.QLineEdit('2')

        fbox3 = QtGui.QFormLayout()
        fbox3.addRow(self.laserType_lbl, self.laserType)
        fbox3.addRow(self.laserWavelength_lbl, self.laserWavelength)
        fbox3.addRow(self.laserPower_lbl, self.laserPower)

        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(fbox1)
        vbox.addLayout(fbox3)

        # layout
        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(vbox)
        hbox.addLayout(fbox2)
        self.setLayout(hbox)


    def reconnectCamera(self):
        try:
            # import ids module
            from Reader_Control.Camera_Control.Camera import Camera
            import ids

            # Free all allocated memory for storing images.
            self.main_widget.setup.camera.free_all()
            # Closes open camera
            self.main_widget.setup.camera.close()

            # seting up camera
            print('> Initializing camera')
            self.main_widget.setup.camera =  Camera(
                    starting_exposure = 60,
                    color_mode = ids.ids_core.COLOR_BAYER_16,
                    magnification = 20
                )
        except:
            print('  >> could not connect to camera. Set offline mode.')
            self.main_widget.offline_mode = True



    def gainChanged(self):
        self.main_widget.setup.camera.gain = self.gain.value()
        self.main_widget.cameraWidget.cameraFeed.updateCameraFeed()


    def magnificationComboUpdated(self):
        """
        Updates the magnification and the corresponding image (scalebar)
        """
        magnification = int(str(self.objective.currentText())[:-1]) # get rid of x
        if not self.main_widget.offline_mode:
            self.main_widget.setup.camera.specifications.setMagnification(magnification)
            self.main_widget.cameraWidget.cameraFeed.resetSettings()
            self.main_widget.cameraWidget.cameraFeed.updateCameraFeed()


class Shortcuts(QtGui.QWidget):
    def __init__(self, main_widget):
        super(Shortcuts, self).__init__()

        self.main_widget = main_widget


        self.shortcutsTable = QtGui.QTableWidget()

        shortcuts = loadDB('../settings.db', 'Shortcuts')

        self.shortcutsTable.setRowCount(len(shortcuts))
        self.shortcutsTable.setColumnCount(1)
        self.shortcutsTable.setVerticalHeaderLabels(list(shortcuts.keys()))
        self.shortcutsTable.setHorizontalHeaderLabels(['Shortcut'])
        self.shortcutsTable.resizeColumnsToContents()
        self.shortcutsTable.resizeRowsToContents()

        for key, value in shortcuts.items():
            rowIndex = list(shortcuts.keys()).index(key)

            item = QtGui.QTableWidgetItem()
            self.shortcutsTable.setItem(rowIndex, 0, item)
            item.setText(value)

        self.resetButton = QtGui.QPushButton('Reset')
        self.applyButton = QtGui.QPushButton('Apply')

        # not yet implemented
        self.resetButton.setEnabled(False)
        self.applyButton.setEnabled(False)

        vbox = QtGui.QVBoxLayout()
        # layout
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.shortcutsTable)

        hbox2 = QtGui.QHBoxLayout()
        hbox2.addWidget(self.resetButton)
        hbox2.addWidget(self.applyButton)

        vbox.addLayout(hbox)
        vbox.addLayout(hbox2)
        
        self.setLayout(vbox)

