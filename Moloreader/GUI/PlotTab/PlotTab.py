# -*- coding: utf-8 -*-

from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui,QtCore

# matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT #as NavigationToolbar

from SharedLibraries.Database.dataBaseOperations import loadDB, formGroup
from Data_Acquisition.permission import changePermissionsPath

from settings import getVariables, getLogExperimentDict, getResultsDict
from auxiliariesMoloreader.rstGenerator import rstFile
from Snippets.Curvefitting import getFitFunctions, fit_and_return_para
import Snippets.Curvefitting as fittingModule

import os
import numpy as np


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


# line styles
linestyles = ['solid', 'dashed', 'dashdot', 'dotted']
colormap = ['black', 'red', 'green', 'blue', 'cyan', 'magenta']


class PlotTab(QtGui.QWidget):

    """ Constructor """
    def __init__(self,main_widget):

        # Call parent class constructor
        super(PlotTab,self).__init__()

        # Assign main widget
        self.main_widget = main_widget
        self.groupPlots = main_widget.settingsTab.generalSettings.groupPlots
        self.groupLines = main_widget.settingsTab.generalSettings.groupLines

        # init plot
        self.x = []
        self.y = []

        # Call child constructor
        self.initUI()
        self.initImage()

        self.current_plot = None

    """ Initialization of GUI """
    def initUI(self):
        self.takeSquare = False
        self.takeSquareRoot = False

        # Set layout
        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)
        
        # set up canvas
        self.figure  = plt.figure(figsize=(10,10),facecolor='None',edgecolor='None')
        self.canvas  = FigureCanvas(self.figure)

        self.manipulations_lbl = QtGui.QLabel('Manipulations')
        self.square = QtGui.QPushButton(_translate("Form", "²", None))
        self.square.setCheckable(True)
        self.square.clicked.connect(self.takeSquareClicked)
        self.squareRoot = QtGui.QPushButton(_translate("Form", "√", None))
        self.squareRoot.setCheckable(True)
        self.squareRoot.clicked.connect(self.takeSquareRootClicked)
        self.fit_lbl = QtGui.QLabel('Fit')
        self.fitFunction = QtGui.QComboBox()
        self.fitFunctions = getFitFunctions()
        self.fitFunction.addItems(list(self.fitFunctions.keys()))
        self.fitEquation = QtGui.QLabel('')
        self.fitCoefficients = QtGui.QLabel('')
        self.fitFunction.activated.connect(self.fitFunctionChanged)
        self.plotFit = QtGui.QPushButton('Plot')
        self.plotFit.clicked.connect(self.plotFittedFunction)
        self.plotFit.setVisible(False)

        hbox1 = QtGui.QHBoxLayout()
        hbox1.addWidget(self.square)
        hbox1.addWidget(self.squareRoot)

        self.loadDB = QtGui.QAction(QtGui.QIcon('./images/sql.png'), 'Load database', self)
        self.loadDB.setStatusTip('Load database')
        self.loadDB.setToolTip('Load database')
        self.loadDB.triggered.connect(self.loadDatabase)

        self.saveOnlyPlots = QtGui.QPushButton('Save plots')
        self.saveOnlyPlots.clicked.connect(self.savePlots)
        self.savePlotsWithTable = QtGui.QPushButton('Save plots and tables')
        self.savePlotsWithTable.clicked.connect(lambda: self.savePlots(table=True))

        self.addToProtocol = QtGui.QAction(QtGui.QIcon('./images/log.png'), 'Add to protocol', self)
        self.addToProtocol.setStatusTip('Add to protocol')
        self.addToProtocol.setToolTip('Add to protocol')
        self.addToProtocol.triggered.connect(self.addResultsToProtocol)

        self.plotX_lbl = QtGui.QLabel('x-axis')
        self.plotX = QtGui.QComboBox()
        self.plotX.activated.connect(self.updateImage)

        self.plotY_lbl = QtGui.QLabel('y-axis')
        self.plotY = QtGui.QComboBox()
        self.plotY.activated.connect(self.updateImage)

        self.plot_Reference = QtGui.QCheckBox('Plot reference data')
        self.plot_Reference.clicked.connect(self.plotReference)

        self.plot_Difference = QtGui.QCheckBox('Plot Difference')

        self.curves_lbl = QtGui.QLabel('Curves')
        self.curvebox = QtGui.QVBoxLayout()

        self.toolbar = QtGui.QToolBar()
        self.toolbar.addWidget(NavigationToolbar(self.canvas,self))
        self.toolbar.addWidget(QtGui.QWidget()) # some dummy widget
        self.toolbar.addWidget(self.saveOnlyPlots)
        self.toolbar.addWidget(self.savePlotsWithTable)
        self.toolbar.addAction(self.addToProtocol)
        self.toolbar.addAction(self.loadDB)

        self.plotNumber = QtGui.QLabel('Curve No.')
        self.analyseCurve = QtGui.QComboBox()
        self.analyseCurve.activated.connect(self.set_current_curve)
        self.showCurve = QtGui.QCheckBox('Show curve')
        self.showCurve.setChecked(True)
        self.showCurve.clicked.connect(self.changeVisible)
        self.max_lbl = QtGui.QLabel('Max: ')
        self.max = QtGui.QLabel('')
        self.min_lbl = QtGui.QLabel('Min: ')
        self.min = QtGui.QLabel('')
        self.diff_lbl = QtGui.QLabel('Difference: ')
        self.diff = QtGui.QLabel('')
        self.mean_lbl = QtGui.QLabel('Mean: ')
        self.mean = QtGui.QLabel('')
        self.std_lbl = QtGui.QLabel('Std: ')
        self.std = QtGui.QLabel('')
        self.blank = QtGui.QLabel('')

        self.infoBox = QtGui.QLabel('(?) Curve info')


        self.tabs = QtGui.QTabWidget()

        fbox_top = QtGui.QFormLayout()
        fbox_top.addRow(self.plotNumber,self.analyseCurve)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.showCurve)
        fbox.addRow(self.max_lbl, self.max)
        fbox.addRow(self.min_lbl, self.min)
        fbox.addRow(self.diff_lbl, self.diff)
        fbox.addRow(self.mean_lbl, self.mean)
        fbox.addRow(self.std_lbl, self.std)
        fbox.addRow(self.blank)
        fbox.addRow(self.infoBox)

        qInfos = QtGui.QWidget()
        qInfos.setLayout(fbox)

        fbox2 = QtGui.QFormLayout()
        fbox2.addRow(self.plotX_lbl, self.plotX)
        fbox2.addRow(self.plotY_lbl, self.plotY)
        fbox2.addRow(self.plot_Reference)
        fbox2.addRow(self.plot_Difference)
        # fbox2.addRow(self.curves_lbl, self.curvebox)

        qDisplay = QtGui.QWidget()
        qDisplay.setLayout(fbox2)

        fbox3 = QtGui.QFormLayout()
        fbox3.addRow(self.manipulations_lbl, hbox1)
        fbox3.addRow(self.fit_lbl, self.fitFunction)
        fbox3.addRow(QtGui.QLabel(''), self.fitEquation)
        fbox3.addRow(QtGui.QLabel(''), self.fitCoefficients)
        fbox3.addRow(QtGui.QLabel(''), self.plotFit)

        qModify = QtGui.QWidget()
        qModify.setLayout(fbox3)

        self.tabs.addTab(qInfos,'Infos')
        self.tabs.addTab(qDisplay,'Display')
        self.tabs.addTab(qModify, 'Modify')

        vbox2 = QtGui.QVBoxLayout()
        vbox2.addLayout(fbox_top)
        vbox2.addWidget(self.tabs)

        transformBox = QtGui.QWidget()
        transformBox.setLayout(vbox2)

        # load data if already a path available
        if self.main_widget.getExperimentFolder() != '':
            self.database_path = '{}/database/'.format(self.main_widget.getExperimentFolder())
            if self.database_path != '':
                self.updateImage()
                self.comboUpdated = True
            else:
                self.comboUpdated = False

        else:
            self.database_path = ''
            self.comboUpdated = False
            

        # Assign widget to layout
        vbox.addWidget(self.toolbar)

        hbox = QtGui.QSplitter(QtCore.Qt.Horizontal)
        hbox.addWidget(self.canvas)
        hbox.addWidget(transformBox)

        vbox.addWidget(hbox)

    def initImage(self):

        # clear plot if exist
        if 'ax' in dir(self):
            try:
                plt.sca(self.ax)
            except: pass

        plt.cla()
        # clear and redeclare axes
        self.ax = self.figure.add_subplot(111)

        # plot layout
        plt.xlim([0,1])
        plt.ylim([0,1])
        self.figure.tight_layout()

        # show image
        self.canvas.draw()

    def updateCombo(self):
        if self.database_path != '':

            # clear combobox
            self.plotY.clear()
            self.plotY.setVisible(True)
            
            data = self.data

            if type(data) == list:
                # if multiple entries choose first
                data = data[0]

            # don't add strings
            self.keys = list()
            for key, value in data.items():
                if type(value) != str and key != 'ID':
                    self.keys.append(key)

            self.plotX.addItems(self.keys)
            self.plotY.addItems(self.keys)

            # update combobox
            self.plotX.update()
            self.plotY.update()

            # update index
            indexX = self.plotY.findText('startTime', QtCore.Qt.MatchFixedString)
            if indexX >= 0:
                 self.plotX.setCurrentIndex(indexX)

            indexY = self.plotY.findText('signalIntensity', QtCore.Qt.MatchFixedString)
            if indexY >= 0:
                 self.plotY.setCurrentIndex(indexY)

            self.comboUpdated = True

            # update info text
            log = self.log
            if type(log) == list:
                # if multiple entries choose first
                log = log[0]

            self.logKeys = list(log.keys())


    def updateImage(self):
        # init image
        self.initImage()

        # reset grouping
        self.groupPlots = self.main_widget.settingsTab.generalSettings.groupPlots
        self.groupLines = self.main_widget.settingsTab.generalSettings.groupLines

        # load database
        if self.database_path == '':
            experimentFolder = self.main_widget.getExperimentFolder()
            if experimentFolder == '': return
            else: self.database_path = '{}/database'.format(experimentFolder)

        self.data = loadDB(self.database_path + '/data.db', 'results')
        self.log = loadDB(self.database_path + '/data.db', 'logExperiment')

        if self.data != []:
            # update combo if database not yet known
            if not self.comboUpdated:
                self.updateCombo()

            # data by keys
            if type(self.data) != list:
                # variable must be a list in order to iterate
                self.data = list([self.data])

            # group dictionaries
            grouped = formGroup(self.data, self.groupPlots)

            self.logNumbers = []

            self.analyseCurve.clear()
            self.dicts = []
            for group in grouped:
                self.logNumbers.append(str(group[0]['experimentID']))
                dictByKeys = dict()
                for dataSet in group:
                    if type(dataSet) != dict:
                        # empty database (no entries, but table exists)
                        x = y = 1
                        continue

                    # if not empty, go through list entries
                    for key, value in dataSet.items():
                        if key not in list(dictByKeys.keys()):
                            dictByKeys[key] = []
                        dictByKeys[key].append(value)

                self.dicts.append(dictByKeys)


            # update combo
            self.analyseCurve.addItems(self.logNumbers)

            if 'currentCurve' in dir(self):
                self.analyseCurve.setCurrentIndex(self.analyseCurve.findText(str(self.currentCurve)))
            else:
                self.currentCurve = int(self.analyseCurve.currentText())

            if 'visibleCurves' not in dir(self):
                # define array of booleans if visible or not
                self.visibleCurves = np.ones(len(self.logNumbers), dtype=bool)

            else:
                try:
                    if len(self.visibleCurves) != self.logNumbers:
                        curves = np.ones(len(self.logNumbers), dtype=bool)
                        curves[0:len(self.visibleCurves)] = self.visibleCurves
                        self.visibleCurves = curves
                except:
                    self.resetPlot()
                    self.updateImage()


            # update curve infos
            self.updateSummary()

            # plot data and show canvas
            x_max = y_max = 0
            x_min = y_min = float('inf')

            for i in range(len(self.dicts)):
                if self.visibleCurves[i]:
                    # if set visible

                    if self.groupLines != '':
                        # plot with time on x-axis
                        lines = []
                        for line in self.dicts[i][self.groupLines]:
                            if line not in lines:
                                lines.append(line)

                        for line in lines:
                            indices = np.array(self.dicts[i][self.groupLines]) == line
                            x_temp = np.array(self.dicts[i][str(self.plotX.currentText())])[indices]
                            y_temp = np.array(self.dicts[i][str(self.plotY.currentText())])[indices]

                            # now use only values that exist
                            x_list = []
                            y_list = []
                            for j in range(len(x_temp)):
                                if x_temp[j] != '' and y_temp[j] != '':
                                    x_list.append(float(x_temp[j]))
                                    y_list.append(float(y_temp[j]))

                            if len(x_list) == 0: continue

                            x = np.array(x_list)
                            y = np.array(y_list)


                            if self.takeSquare:
                                y = y**2
                            if self.takeSquareRoot:
                                y = np.sqrt(y)

                            if line == lines[0]:
                                self.points = plt.plot(x, y, 
                                                    linestyle=linestyles[lines.index(line) % len(linestyles)],
                                                    color=colormap[i % len(colormap)],
                                                    label='{}, {}'.format(self.dicts[i][self.groupPlots][0],lines)
                                                )
                            else:
                                self.points = plt.plot(x, y, 
                                                    linestyle=linestyles[lines.index(line) % len(linestyles)],
                                                    color=colormap[i % len(colormap)],
                                                )

                    else:
                        if self.groupPlots not in self.dicts[i]: continue

                        x_temp = np.array(self.dicts[i][str(self.plotX.currentText())])
                        y_temp = np.array(self.dicts[i][str(self.plotY.currentText())])

                        # now use only values that exist
                        x_list = []
                        y_list = []
                        for j in range(len(x_temp)):
                            if x_temp[j] != '' and y_temp[j] != '':
                                x_list.append(float(x_temp[j]))
                                y_list.append(float(y_temp[j]))

                        if len(x_list) == 0: continue

                        x = np.array(x_list)
                        y = np.array(y_list)

                        if self.takeSquare:
                            y = y**2
                        if self.takeSquareRoot:
                            y = np.sqrt(y)

                        self.points = plt.plot(x, y, 
                                                    color=colormap[i % len(colormap)],
                                                    label='{}'.format(self.dicts[i][self.groupPlots][0])
                                                )


                    x_max = np.max((x_max,np.max(x)))
                    x_min = np.min((x_min,np.min(x)))
                    y_max = np.max((y_max,np.max(y)))
                    y_min = np.min((y_min,np.min(y)))


                else:
                    pass

            if self.main_widget.settingsTab.generalSettings.showLegend.isChecked():
                if self.groupLines != '':
                    plt.legend(title='{}, {}'.format(getVariables()[self.groupPlots],getVariables()[self.groupLines]), ncol=int(len(self.dicts)/9)+1) # plot appearance

                else:
                    plt.legend(title=getVariables()[self.groupPlots], ncol=int(len(self.dicts)/9)+1) # plot appearance

            # add titles
            variablesDict = getVariables()
            if self.takeSquare:
                y_max = y_max**2
                ylabel = '{}^2'.format(variablesDict[str(self.plotY.currentText())])
            elif self.takeSquareRoot:
                y_max = np.sqrt(y_max)
                ylabel = '{}^(1/2)'.format(variablesDict[str(self.plotY.currentText())])
            else:
                ylabel = variablesDict[str(self.plotY.currentText())]

            self.ax.set_xlabel(variablesDict[str(self.plotX.currentText())])
            self.ax.set_ylabel(ylabel)
            self.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
            plt.xlim([x_min, x_max])
            plt.ylim([y_min,y_max])

            self.figure.tight_layout()
            self.canvas.draw()

        else:
            return


    def updateSummary(self):
        if 'currentCurve' not in dir(self):
            self.currentCurve = int(str(self.analyseCurve.currentText()))

        if 'visibleCurves' in dir(self):
            self.showCurve.setChecked(self.visibleCurves[self.analyseCurve.findText(str(self.currentCurve))])

        resultsDict = self.dicts[self.analyseCurve.findText(str(self.currentCurve))]
        logDict = loadDB(self.database_path + '/data.db', 'logExperiment', condition = 'where ID = {}'.format(self.currentCurve))

        y = np.array(resultsDict[str(self.plotY.currentText())])
        if self.takeSquare:
            y = y**2
        if self.takeSquareRoot:
            y = np.sqrt(y)

        self.max.setText(str(np.max(y)))
        self.min.setText(str(np.min(y)))
        self.diff.setText(str(np.max(y)-np.min(y)))
        self.mean.setText(str(np.mean(y)))
        self.std.setText(str(np.std(y)))

        variablesDict = getVariables()
        infoBoxStr = ''
        for key in self.logKeys:
            try:
                infoBoxStr += '{}:\t{}\n'.format(variablesDict[key],logDict[key])
            except:
                continue

        self.infoBox.setToolTip(infoBoxStr)
        self.y = y
        self.x = resultsDict[str(self.plotX.currentText())]


    def set_current_plot(self, plot_type):
        self.current_plot = plot_type
        self.main_widget.updateAll()


    def set_current_curve(self):
        self.currentCurve = int(str(self.analyseCurve.currentText()))
        if 'visibleCurves' in dir(self):
            self.showCurve.setChecked(self.visibleCurves[self.analyseCurve.currentIndex()])

        self.updateSummary()
        self.fitFunctionChanged()


    def changeVisible(self):
        if 'visibleCurves' not in dir(self):
            self.visibleCurves = np.ones(len(self.logNumbers),dtype=bool)

        self.visibleCurves[self.analyseCurve.findText(str(self.currentCurve))] = self.showCurve.isChecked()
        self.updateImage()

    def plotReference(self):
        self.initImage()
        self.groupPlots = self.main_widget.settingsTab.generalSettings.groupPlots
        self.groupLines = self.main_widget.settingsTab.generalSettings.groupLines
        # load database
        if self.database_path == '':
            experimentFolder = self.main_widget.getExperimentFolder()
            if experimentFolder == '': return
            else: self.database_path = '{}/database'.format(experimentFolder)

        self.data = loadDB(self.database_path + '/data.db', 'results')
        self.log = loadDB(self.database_path + '/data.db', 'logExperiment')

        if self.data != []:
            # update combo if database not yet known
            if not self.comboUpdated:
                self.updateCombo()

            # data by keys
            if type(self.data) != list:
                # variable must be a list in order to iterate
                self.data = list([self.data])

            # group dictionaries
            grouped = formGroup(self.data, self.groupPlots)

            self.logNumbers = []

            self.analyseCurve.clear()
            self.dicts = []

            for group in grouped:
                self.logNumbers.append(str(group[0]['experimentID']))
                dictByKeys = dict()
                for dataSet in group:
                    if type(dataSet) != dict:
                        # empty database (no entries, but table exists)
                        x = y = 1
                        continue

                    # if not empty, go through list entries
                    for key, value in dataSet.items():
                        if key not in list(dictByKeys.keys()):
                            dictByKeys[key] = []
                        dictByKeys[key].append(value)

                self.dicts.append(dictByKeys)

            self.analyseCurve.addItems(self.logNumbers)
            
            if 'currentCurve' in dir(self):
                self.analyseCurve.setCurrentIndex(self.analyseCurve.findText(str(self.currentCurve)))
            else:
                self.currentCurve = int(self.analyseCurve.currentText())

            if 'visibleCurves' not in dir(self):
                # define array of booleans if visible or not
                self.visibleCurves = np.ones(len(self.logNumbers), dtype=bool)

            else:
                try:
                    if len(self.visibleCurves) != self.logNumbers:
                        curves = np.ones(len(self.logNumbers), dtype=bool)
                        curves[0:len(self.visibleCurves)] = self.visibleCurves
                        self.visibleCurves = curves
                except:
                    self.resetPlot()
                    self.plot_Reference()

            self.updateSummary()
        else:
            return
        if self.plot_Reference.isChecked()==True:
            # plot data and show canvas
            x_max = y_max = 0
            x_min = y_min = float('inf')

            for i in range(len(self.dicts)):
                if self.visibleCurves[i]:
                    # if set visible
                    # plot with time on x-axis
                    x_temp = np.array(self.dicts[i][str(self.plotX.currentText())])
                    y_temp = np.array(self.dicts[i]['signalIntensityPhys'])
                    y_temp_ref = np.array(self.dicts[i]['referenceIntensityPhys'])

                    # now use only values that exist
                    x_list = []
                    y_list = []
                    y_list_ref = []
                    y_list_diff = []

                    for j in range(len(x_temp)):
                        if x_temp[j] != '' and y_temp[j] != '':
                            x_list.append(float(x_temp[j]))
                            y_list.append(float(y_temp[j]))
                            y_list_ref.append(float(y_temp_ref[j]))
                            y_list_diff.append(float(y_temp[j]-y_temp_ref[j]))

                    if len(x_list) == 0: continue

                    x = np.array(x_list)
                    y = np.array(y_list)
                    y_ref = np.array(y_list_ref)
                    y_ref_diff = np.array(y_list_diff)


                    if self.takeSquare:
                        y = y**2
                        y_ref = y_ref**2
                        y_ref_diff = (y-y_ref)**2

                    if self.takeSquareRoot:
                        y = np.sqrt(y)
                        y_ref =np.sqrt(y_ref)
                        y_ref_diff = np.sqrt(y-y_ref)


                    self.points = plt.plot(x, y, 
                                        linestyle='solid',
                                        color='black',
                                        label='Molospot Intensity')
                    #plot referencing data                
                    self.refPoints = plt.plot(x, y_ref, 
                                        linestyle='solid',
                                        color='red',
                                        label='Reference Intensity')
                    
                    if self.plot_Difference.isChecked() == True:
                        self.plot_diff = plt.plot(x,y_ref_diff, 
                                        linestyle='solid',
                                        color='blue',
                                        label='Difference')
                    
                    x_max = np.max((x_max,np.max(x)))
                    x_min = np.min((x_min,np.min(x)))    
                    y_max = max(y_max,max(np.max(y),np.max(y_ref)))
                    y_min = min(y_min,min(np.min(y),np.min(y_ref)))


                else:
                    pass
            
            if self.main_widget.settingsTab.generalSettings.showLegend.isChecked():
                plt.legend()
            # add titles
            variablesDict = getVariables()
            if self.takeSquare:
                y_max = y_max**2
                ylabel = '{}^2'.format(variablesDict[str(self.plotY.currentText())])
            elif self.takeSquareRoot:
                y_max = np.sqrt(y_max)
                ylabel = '{}^(1/2)'.format(variablesDict[str(self.plotY.currentText())])
            else:
                ylabel = variablesDict[str(self.plotY.currentText())]

            self.ax.set_xlabel(variablesDict[str(self.plotX.currentText())])
            self.ax.set_ylabel(ylabel)
            self.ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
            plt.xlim([x_min, x_max])
            plt.ylim([y_min,y_max])

            self.figure.tight_layout()
            self.canvas.draw()
        
        else:
            #If the referencing remains unchecked, update the plot as usual
            self.updateImage()

        
    def savePlots(self, table=False):
        if 'data' not in dir(self): return

        files = list()  # stores all filenames

        data = self.data
        if type(data) == list:
            # if multiple entries choose first
            data = data[0]

        # make list
        if type(self.log) != list:
            self.log = [self.log]


        # create folder if not exist
        maxIndex = 0
        for dirname in os.listdir(self.main_widget.getExperimentFolder()):
            if dirname.split('_')[0] == 'Stack':
                maxIndex = np.max((maxIndex,int(dirname.split('_')[-1])))


        folder = '{}/Stack_{}'.format(self.main_widget.getExperimentFolder(),maxIndex+1)
        if not os.path.exists(folder):
            os.makedirs(folder)

            # add permission (rwxr-xr-x -> 111 101 101)
            changePermissionsPath(folder)

        print(self.keys)
        keys = self.keys
        columns = ['Averaged', 'Maximum', 'Minimum', 'Difference', 'Mean', 'Standard Deviation']

        for key in keys:

            # plot data and show canvas
            x_max = y_max = 0
            x_min = y_min = float('inf')

            cellText = []
            rows = []

            fig, ax = plt.subplots(1,1)

            for i in range(len(self.dicts)):
                if self.visibleCurves[i]:
                    # append key in rows
                    rows.append(str(self.dicts[i][self.groupPlots][0]))

                    x_max = np.max((x_max,np.max(self.dicts[i][str(self.plotX.currentText())])))
                    x_min = np.min((x_min,np.min(self.dicts[i][str(self.plotX.currentText())])))
                    y_max = np.max((y_max,np.max(self.dicts[i][key])))
                    y_min = np.min((y_min,np.min(self.dicts[i][key])))

                    # plot with time on x-axis
                    x = self.dicts[i][str(self.plotX.currentText())]
                    y = np.array(self.dicts[i][key])

                    if self.takeSquare:
                        y = y**2
                    if self.takeSquareRoot:
                        y = np.sqrt(y)

                    ax.plot(x,y,label= str(self.dicts[i][self.groupPlots][0]))

                    cellText.append([self.data[i]['averagedImages'], np.max(y), np.min(y), np.max(y)-np.min(y), np.mean(y), np.std(y)])



            if x_min == x_max or y_min == y_max \
                    or str(self.plotX.currentText()) == key \
                    or key == self.groupPlots:
                # do not save parameters which are constant for all experiments or
                # x and y axis the same or
                # key = experimentID
                continue

            # add titles
            variablesDict = getVariables()
            ax.legend() # plot appearance
            ax.set_xlim([x_min, x_max])
            if self.takeSquare:
                y_max = y_max**2
                ylabel = '{}^2'.format(variablesDict[key])
                title = 'Square of {}'.format(variablesDict[key])
            elif self.takeSquareRoot:
                y_max = np.sqrt(y_max)
                ylabel = '{}^(1/2)'.format(variablesDict[key])
                title = 'Square-root of {}'.format(variablesDict[key])
            else:
                ylabel = variablesDict[key]
                title = variablesDict[key]

            ax.set_ylim([y_min,y_max])
            ax.set_xlabel(variablesDict[str(self.plotX.currentText())])
            ax.set_ylabel(ylabel)
            ax.set_title(title)
            ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

            if table == True:
                fig, axs = plt.subplots(2,1)
                axs[0] = ax

                axs[1].table(cellText = cellText,
                rowLabels = rows,
                colLabels = columns,
                loc = 'center')

                axs[1].axis('tight')
                axs[1].axis('off')

                plt.tight_layout()



            file = '{}/{}.png'.format(folder, key)
            fig.savefig(file)
            plt.close('all')
            changePermissionsPath(file)

            files.append(file)


        return files

    def addResultsToProtocol(self):
        imageFiles = self.savePlots()
        rstfile = rstFile()

        # no absolute paths possible
        experimentFolder = self.main_widget.settingsTab.experimentSpecifications.editFolder.text()
        if experimentFolder == '':
            experimentFolder = os.path.dirname(self.main_widget.getExperimentFolder())

        for imageFile in imageFiles:
            rstfile.addPicture(imageFile.replace(experimentFolder + '/',''),'')

        self.main_widget.protocol.addContent(rstfile.getContent())


    def loadDatabase(self):
        databaseFile = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home/focal-molography/Experiments', "Database files (*.db)")
        if databaseFile != '':

            self.resetPlot()
            # add experiment folder to settings
            folder = os.path.dirname(str(databaseFile))
            self.database_path = folder
            self.main_widget.setExperimentFolder(os.path.dirname(folder))
            self.updateImage()


    def takeSquareClicked(self):
        if self.square.isChecked():
            self.takeSquare = True
            self.takeSquareRoot = False
            self.squareRoot.setChecked(False)

        else:
            self.takeSquare = False

        self.updateImage()


    def takeSquareRootClicked(self):
        if self.squareRoot.isChecked():
            self.takeSquareRoot = True
            self.takeSquare = False
            self.square.setChecked(False)

        else:
            self.takeSquareRoot = False

        self.updateImage()

    def resetPlot(self):

        # reset variables
        if 'currentCurve' in dir(self): del self.currentCurve
        if 'visibleCurves' in dir(self): del self.visibleCurves


    def fitFunctionChanged(self):
        self.updateSummary()

        functionName, fitEquation, coefficients = self.fitFunctions[str(self.fitFunction.currentText())]
        self.fitEquation.setText(fitEquation)

        if len(self.x) > 0:
            method_to_call = getattr(fittingModule, functionName)
            result = fit_and_return_para(method_to_call, self.x, self.y)

            coeffsString = ''
            for i in range(len(coefficients)):
                coeffsString += '\t{} = {}\n'.format(coefficients[i],result[i])

            self.fitCoefficients.setText(coeffsString)
            self.plotFit.setVisible(True)

        else:
            self.plotFit.setVisible(False)

        return functionName, fitEquation, coefficients

    def plotFittedFunction(self):
        functionName, fitEquation, coefficients = self.fitFunctionChanged()
 
        method_to_call = getattr(fittingModule, functionName)
        result = fit_and_return_para(method_to_call, self.x, self.y)

        y = method_to_call(np.array(self.x), *result)

        plt.plot(self.x, y, label='{} fit ({})'.format(str(self.fitFunction.currentText()),str(self.currentCurve)))
        plt.legend()
        self.canvas.draw()


class NavigationToolbar(NavigationToolbar2QT):
    pass