# -*- coding: utf-8 -*-

from PyQt4 import QtGui,QtCore

# matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT #as NavigationToolbar

from Data_Acquisition.permission import changePermissionsPath

from settings import getVariables, getLogExperimentDict, getResultsDict
import pyqtgraph as pg
import u6
import os
import numpy as np
import time


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


def intensityFromVout(R_lambda,R_L,V_out):

    return V_out/(R_lambda*R_L)


class LabJack(QtCore.QThread):

    def __init__(self,main_widget= ''):

        super(LabJack,self).__init__()

        if main_widget != 0:
            self.main_widget = main_widget

        d = u6.U6()
        self.d = d 
        self.d.getCalibrationData()

        self.dataLaser = []
        self.dataInc = []
        self.dataOut = []
        self.dataIncTotal = []
        self.timeAtacquisition = []

        self.laserRef = 0
        self.incRef = 0
        self.outRef = 0

        self.Responsivity = 0.37092 # A/W responsivity of the photodiode at 633 nm

        self.ResistorLaser = 50.0 # resistor of the laser light measurement. (beamsplitter)''
        self.ResistorInc = 500.0 # resistor of the incoupled light measurement.
        self.ResistorOut = 1000.0 # resistor of the outcoupled light measurement.
        

        self.N = 100 # number of datapoints in buffer for averaging. 

        self.display = False
        self.acquire = True
        
        self.PowerOutcoupledMean = 'nan'


    def run(self):
        print("Started Photodiode Feed")

        if self.acquire:
            self.dataLaser = []
            self.dataInc = []
            self.dataOut = []
            self.dataIncTotal = []

        while self.display or self.acquire:

            self.getDataPoint()

            if self.display:

                self.main_widget.photodiodeTab.LaserInt_lbl.setText("Power Laserinput (uW): {0:.3f} Mean/Std N = 100: {1:.3f} / {2:.3f}".format(self.PowerLaser*1e6,np.asarray(self.dataLaser).mean()*1e6,np.asarray(self.dataLaser).std()*1e6))
                
                self.main_widget.photodiodeTab.IncouplInt_lbl.setText("Power Below Incoupling (uW): {0:.3f} Mean/Std N = 100: {1:.3f} / {2:.3f}".format(self.PowerIncoupling*1e6,np.asarray(self.dataInc).mean()*1e6,np.asarray(self.dataInc).std()*1e6))
                self.main_widget.photodiodeTab.OutcouplInt_lbl.setText("Power Below Outcoupling (uW): {0:.3f} Mean/Std N = 100: {1:.3f} / {2:.3f}".format(self.PowerOutcoupling*1e6,np.asarray(self.dataOut).mean()*1e6,np.asarray(self.dataOut).std()*1e6))
                self.main_widget.photodiodeTab.PowerCoupledIn_lbl.setText("Power Coupled in (uW): {0:.3f} Mean/Std N = 100: {1:.3f} / {2:.3f}".format(self.PowerCoupledIn*1e6,np.asarray(self.dataIncTotal).mean()*1e6,np.asarray(self.dataIncTotal).std()*1e6))

                if (len(self.dataLaser)) > self.N:
                    self.dataLaser.pop(0)
                    self.dataInc.pop(0)
                    self.dataOut.pop(0)
                    self.dataIncTotal.pop(0)
                    self.timeAtacquisition.pop(0)
                time.sleep(0.05)

    def getDataPoint(self):

        inputLaser = self.d.getAIN(positiveChannel=0,resolutionIndex=5,gainIndex=1,settlingFactor=0,differential=False) # Read from raw bits from incoupling photodiode
        inCoupling = self.d.getAIN(positiveChannel=1,resolutionIndex=5,gainIndex=1,settlingFactor=0,differential=False) # Read from raw bits from incoupling photodiode
        outCoupling = self.d.getAIN(positiveChannel=2,resolutionIndex=5,gainIndex=1,settlingFactor=0,differential=False) # Read from raw bits from outcoupling photodiode
        timing = time.time()
        alpha = float(self.main_widget.photodiodeTab.waveguideDamping.text())*np.log(10)/10*100 #[damping constant of the waveguide in Np/m]
        distance_propagated = 0.0095 # [m] # the distance between the two coupling gratings is 9.5 mm.
        
        self.PowerLaser = intensityFromVout(self.Responsivity,self.ResistorLaser,abs(np.asarray(inputLaser))) - self.laserRef
        self.PowerIncoupling = intensityFromVout(self.Responsivity,self.ResistorInc,abs(np.asarray(inCoupling))) - self.incRef
        self.PowerOutcoupling = intensityFromVout(self.Responsivity,self.ResistorOut,abs(np.asarray(outCoupling))) - self.outRef
         # actual power in [uW] that is coupled into the waveguide at the location of the coupling grating
        self.PowerCoupledIn = 2*self.PowerOutcoupling*np.exp(alpha*distance_propagated)

        self.dataLaser.append(self.PowerLaser)
        self.dataInc.append(self.PowerIncoupling)
        self.dataOut.append(self.PowerOutcoupling)
        self.dataIncTotal.append(self.PowerCoupledIn)
        self.timeAtacquisition.append(timing)



    def setReferences(self):

        self.laserRef = np.asarray(self.dataLaser).mean()
        self.incRef = np.asarray(self.dataInc).mean()
        self.outRef = np.asarray(self.dataOut).mean()

    def deleteRefdata(self):

        self.laserRef = 0
        self.incRef = 0
        self.outRef = 0

    def getNPhotoDiodeReads(self,N):
        """Function that gets N Outcoupling photodiode reads and returns the them"""

        self.stopDisp()
        self.acquire = False
        # Flush the data
        self.dataLaser = []
        self.dataInc = []
        self.dataOut = []
        self.dataIncTotal = []

        for i in range(N):
            self.getDataPoint()

        return np.asarray(self.dataOut)
 
    # def startAcquiringOutcoupledIntensity(self):

    #     self.acquiredData = np.array(0) # flush the buffer

    #     self.stopDisp()

    #     self.acquire = True

    #     self.start()

    # def stopAcquiringOutcoupledIntensity(self):

    #     self.acquire = False

    #     while self.PowerOutcoupledMean == 'nan':

    #         self.acquiredData = np.asarray(self.dataOut)
    #         self.PowerOutcoupledMean = self.acquiredData.mean()

    #     print(self.PowerOutcoupledMean)

    def stopDisp(self):

        self.display = False

    def startDisp(self):

        self.display = True
        self.start()

    def __del__(self):
        self.wait()


class PhotoDiodeTab(QtGui.QWidget):

    """ Constructor """
    def __init__(self,main_widget):

        # Call parent class constructor
        super(PhotoDiodeTab,self).__init__()

        # Assign main widget
        self.main_widget = main_widget

        # init plot

        # start the connection to the Labjack DAQ System.
        self.labJack = LabJack(main_widget)

        # Call child constructor
        self.initUI()


    """ Initialization of GUI """
    def initUI(self):

        self.hbox2 = QtGui.QHBoxLayout()
        self.vbox = QtGui.QVBoxLayout()

        self.setLayout(self.vbox)

        # p1 = pg.PlotWidget(title='LaserInt')
        # p2 = pg.PlotWidget(title='BelowInCoupl')
        # p3 = pg.PlotWidget(title='BelowOutCoupl')

        # self.curve = p1.plot(pen='y')
        # self.curve1 = p2.plot(pen='y')
        # self.curve2 = p3.plot(pen='y')

        # self.hbox1.addWidget(p1)
        # self.hbox1.addWidget(p2)
        # self.hbox1.addWidget(p3)

        self.waveguideDamping_lbl = QtGui.QLabel('Waveguide Damping:')
        self.waveguideDamping = QtGui.QLineEdit(str(2.51))

        self.hbox2.addWidget(self.waveguideDamping_lbl)
        self.hbox2.addWidget(self.waveguideDamping)

        self.start_btn = QtGui.QPushButton('Display diode feed')
        self.stop_btn = QtGui.QPushButton('Stop display diode feed')
        self.update_ref_btn = QtGui.QPushButton('Reference Photodiodes') # references the photodiodes to zero.
        self.delRef_btn = QtGui.QPushButton('Delete References') # references the photodiodes to zero.
        self.hbox2.addWidget(self.update_ref_btn)
        self.hbox2.addWidget(self.delRef_btn)
        self.hbox2.addWidget(self.start_btn)
        self.hbox2.addWidget(self.stop_btn)

        self.LaserInt_lbl = QtGui.QLabel('Laser power')
        self.IncouplInt_lbl = QtGui.QLabel('Power below incoupling')
        self.OutcouplInt_lbl = QtGui.QLabel('Power below outcoupling')
        self.PowerCoupledIn_lbl = QtGui.QLabel('Power coupled in')

        self.start_btn.clicked.connect(self.labJack.startDisp)
        self.stop_btn.clicked.connect(self.labJack.stopDisp)
        self.update_ref_btn.clicked.connect(self.labJack.setReferences)
        self.delRef_btn.clicked.connect(self.labJack.deleteRefdata)

        self.vbox.addWidget(self.LaserInt_lbl)
        self.vbox.addWidget(self.IncouplInt_lbl)
        self.vbox.addWidget(self.OutcouplInt_lbl)
        self.vbox.addWidget(self.PowerCoupledIn_lbl)
        self.vbox.addLayout(self.hbox2)

        # start the photodiode feed
        self.labJack.start()

    def setupMeasurement(self):

        raise NotImplementedError


class NavigationToolbar(NavigationToolbar2QT):
    pass