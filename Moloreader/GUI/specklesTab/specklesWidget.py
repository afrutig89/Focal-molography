# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SpecklesWidget.ui'
#
# Created: Mon Dec  5 17:15:41 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
import sys
sys.path.append('../')
from Data_Processing.speckles import findSpeckles


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class SpecklesWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget):

        # Call parent class constructor
        super(SpecklesWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        # Call child constructor
        self.setupUi()

    def setupUi(self):
        self.speckleParams = QtGui.QGroupBox('')
        self.label_neighborhoodSize = QtGui.QLabel('Size', self.speckleParams)
        self.neighborhoodSize = QtGui.QLineEdit('100', self.speckleParams)
        self.label_threshold = QtGui.QLabel('Threshold', self.speckleParams)
        self.threshold = QtGui.QLineEdit('0', self.speckleParams)

        self.calculateButton = QtGui.QPushButton('Analyze', self.speckleParams)
        self.calculateButton.clicked.connect(self.analyzeSpeckles)
        self.calculateButton.setToolTip('<font color=black>Analyze Speckles</font>')

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.label_neighborhoodSize, self.neighborhoodSize)
        fbox.addRow(self.label_threshold, self.threshold)
        fbox.addRow(self.calculateButton)

        self.speckleParams.setLayout(fbox)

        self.relatedParameters = QtGui.QGroupBox('')
        self.label_numberOfSpeckles = QtGui.QLabel('Number of speckles', self.relatedParameters)
        self.numberOfSpeckles = QtGui.QLabel(self.relatedParameters)
        self.label_speckleDensity = QtGui.QLabel('Speckle density', self.relatedParameters)
        self.speckleDensity = QtGui.QLabel(self.relatedParameters)
        self.speckleDensity_units = QtGui.QLabel(self.relatedParameters)

        fbox2 = QtGui.QFormLayout()
        fbox2.addRow(self.label_numberOfSpeckles, self.numberOfSpeckles)
        fbox2.addRow(self.label_speckleDensity, self.speckleDensity)

        self.relatedParameters.setLayout(fbox2)

        hbox = QtGui.QHBoxLayout()
        self.setLayout(hbox)

        # box left top
        vbox = QtGui.QVBoxLayout()

        vbox.addWidget(self.speckleParams)
        vbox.addWidget(self.relatedParameters)
        vbox.addStretch()

        hbox.addLayout(vbox)


    def get_values(self):
        return None

    def analyzeSpeckles(self):
        neighborhoodSize = int(self.neighborhoodSize.text())
        threshold = int(self.threshold.text())
        numberOfSpeckles, speckleDensity = self.main_widget.cameraWidget.cameraFeed.speckleCircles(neighborhoodSize, threshold)

        self.numberOfSpeckles.setText(str(numberOfSpeckles))
        units = _translate("Form", "1/μ²", None)
        self.speckleDensity.setText('{} {}'.format(speckleDensity, units))