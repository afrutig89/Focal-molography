# -*- coding: utf-8 -*-


from PyQt4 import QtCore, QtGui
import numpy as np
import os, sys
import time, datetime
sys.path.append('../')
from auxiliariesMoloreader.rstGenerator import rstFile
from Data_Acquisition.permission import changePermissionsPath, changePermissionsRecursive


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class ProtocolWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget):

        # Call parent class constructor
        super(ProtocolWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget
        self.settings = main_widget.settingsTab

        # Call child constructor
        self.setupUi()

    def setupUi(self):

        self.mologramPatterning = MolographicPatterning(self.main_widget)
        self.protocolTab = ProtocolTab(self.main_widget)

        self.tabs = QtGui.QTabWidget()
        self.tabs.addTab(self.mologramPatterning, 'Molographic patterning')
        self.tabs.addTab(self.protocolTab, 'Protocol')

        # box left top
        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        vbox.addWidget(self.tabs)



class ProtocolTab(QtGui.QWidget):
    def __init__(self, main_widget):

        super(ProtocolTab,self).__init__()

        self.main_widget = main_widget

        self.protocol = QtGui.QTextEdit() 
        font = QtGui.QFont()
        font.setFamily('Courier')
        self.protocol.setFont(font)
        self.generateProtocolButton = QtGui.QPushButton('Generate')
        self.generateProtocolButton.clicked.connect(self.generateProtocol)
        self.loadProtocolButton = QtGui.QPushButton('Load protocol')
        self.loadProtocolButton.clicked.connect(self.loadProtocol)
        self.generateHTMLButton = QtGui.QPushButton('Generate HTML file')
        self.generateHTMLButton.clicked.connect(self.generateHTML)
        shortcut = QtGui.QShortcut(QtGui.QKeySequence("Ctrl+S"), self)
        shortcut.activated.connect(self.saveRST)

        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        vbox.addWidget(self.protocol)

        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.generateProtocolButton)
        hbox.addWidget(self.loadProtocolButton)
        hbox.addWidget(self.generateHTMLButton)

        vbox.addLayout(hbox)


    def generateProtocol(self):
        if self.protocol.toPlainText() != '':
            msg = QtGui.QMessageBox()
            msg.setWindowTitle("Generate protocol")
            msg.setIcon(QtGui.QMessageBox.Question)
            msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            msg.setText("Delete current protocol?")
            retval = msg.exec_()
            if retval != QtGui.QMessageBox.Yes: return

        devicesSettings = self.main_widget.settingsTab.devicesSettings

        # experiment settings
        experimentSettings = self.main_widget.settingsTab.experimentSpecifications
        experimentFolder = experimentSettings.editFolder.text()

        # return if no folder defined
        if experimentFolder == '':
            print('no experiment folder defined')
            return

        # generate class and file
        rstfile = rstFile('{}/Report.rst'.format(experimentFolder))

        # add title
        experimentName = experimentSettings.experimentName.text()
        rstfile.addTitle('Report {}'.format(experimentName))
        rstfile.addParagraph('Date: {}\nTime: {}'.format(datetime.date.today(),time.strftime("%H:%M:%S")))

        # add abstract
        experimentDescription = experimentSettings.experimentDescription.toPlainText()
        rstfile.addSubtitle('Abstract')
        rstfile.addParagraph(experimentDescription)

        # experimental
        rstfile.addSubtitle('Experimental')

        # molographic photo patterning
        rstfile.addSubsubtitle('Molographic photo patterning')

        # field list
        mologramPatterning = self.main_widget.protocol.mologramPatterning
        fields = mologramPatterning.getValues()
        rstfile.addFieldList(fields)

        mologramPatterning = self.main_widget.protocol.mologramPatterning

        table = mologramPatterning.getTableValues()
        rstfile.addTable(table, header=True)

        # results & discussion
        rstfile.addSubtitle('Results & discussion')
        rstfile.addParagraph('')

        content = rstfile.getContent()
        self.protocol.setText(content)


    def loadProtocol(self):
        rstFile = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home/focal-molography/Experiments', "Text files (*.rst)")
        if rstFile != '':
            # read rst file
            file = open(rstFile,'r')
            self.protocol.setText(file.read())

            # add experiment folder to settings
            folder = os.path.dirname(rstFile)
            self.settings.experimentSpecifications.editFolder.setText(folder)


    def saveRST(self):
        experimentFolder = self.main_widget.settingsTab.experimentSpecifications.editFolder.text()
        filename = '{}/Report.rst'.format(experimentFolder)

        if not os.path.exists(filename):
            # make index entry if file does not exist
            makeFolder = os.path.dirname(os.path.dirname(filename))
            if 'index.rst' in os.listdir(makeFolder):
                # index.rst should be in the folder above the rst file
                file = open('{}/index.rst'.format(makeFolder),'a')
                experimentFolderName = experimentFolder.split('/')[-1]
                file.write('   {}/Report.rst\n'.format(experimentFolderName))


                changePermissionsPath('{}/index.rst'.format(makeFolder))

        # write rst file
        file = open(filename,'w')
        file.write(self.protocol.toPlainText())

        changePermissionsPath(filename)

        print('RST file saved: {}'.format(filename))



    def generateHTML(self):
        # save rst file
        self.saveRST()

        # experiment settings
        experimentSettings = self.main_widget.settingsTab.experimentSpecifications
        experimentFolder = experimentSettings.editFolder.text()
        makeFolder = os.path.dirname(str(experimentFolder))

        if 'make.bat' in os.listdir(makeFolder):
            os.system('(cd \'{}\' && make clean && make html)'.format(makeFolder))
            changePermissionsRecursive('{}/_build'.format(makeFolder))



    def addContent(self, text):
        content = self.protocol.toPlainText()
        content += text

        self.protocol.setText(content)



class MolographicPatterning(QtGui.QWidget):
    def __init__(self, main_widget):

        super(MolographicPatterning,self).__init__()

        self.main_widget = main_widget

        self.chipLabel = QtGui.QLabel()
        self.chipLabel.setText(_translate("Form", "Chip ID", None))
        self.chipName = QtGui.QLineEdit()

        self.intensityLabel = QtGui.QLabel()
        self.intensityLabel.setText(_translate("Form", "Intensity", None))
        self.intensity = QtGui.QLineEdit()
        self.intensityUnits = QtGui.QLabel()
        self.intensityUnits.setText(_translate("Form", "mW/cm²", None))
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.intensity)
        hbox.addWidget(self.intensityUnits)

        self.maskLabel = QtGui.QLabel()
        self.maskLabel.setText(_translate("Form", "Mask", None))

        self.maskName = QtGui.QLineEdit('')

        self.mask = QtGui.QComboBox()
        self.mask.addItem(_fromUtf8("D1C"))
        self.mask.addItem(_fromUtf8("A1C"))
        self.mask.addItem(_fromUtf8("Bragg"))
        self.mask.currentIndexChanged.connect(self.setMask)

        mask = QtGui.QHBoxLayout()
        mask.addWidget(self.mask)
        mask.addWidget(self.maskName)

        self.molographicPatterningTable = QtGui.QTableWidget()
        self.verHeaders = ["Time [s]",_translate("Form", "Dose [mJ/cm2]", None), "Grooves", "Rigdes"]
        self.horHeaders = [chr(letter) for letter in np.arange(ord(self.main_widget.setup.chip.MIN_SET),ord(self.main_widget.setup.chip.MAX_SET)+1)]

        self.molographicPatterningTable.setColumnCount(len(self.horHeaders))
        self.molographicPatterningTable.setRowCount(len(self.verHeaders))
        self.molographicPatterningTable.setVerticalHeaderLabels(self.verHeaders)
        self.molographicPatterningTable.setHorizontalHeaderLabels(self.horHeaders)

        self.molographicPatterningTable.itemChanged.connect(self.tableItemChanged)

        self.sameRow = QtGui.QCheckBox('Same value for each field when B is changed',)
        self.sameRow.setChecked(True)
        self.sameRow.clicked.connect(self.getTableValues)

        molographicPatterning = QtGui.QFormLayout()
        molographicPatterning.addRow(self.chipLabel, self.chipName)
        molographicPatterning.addRow(self.maskLabel, mask)
        molographicPatterning.addRow(self.intensityLabel, hbox)
        molographicPatterning.addRow(self.molographicPatterningTable)
        molographicPatterning.addRow(self.sameRow)

        # layout
        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(molographicPatterning)
        self.setLayout(hbox)

    def setMask(self):
        self.main_widget.changeMask(mask=self.mask.currentText())


    def tableItemChanged(self, item):
        value = str(self.molographicPatterningTable.item(item.row(),item.column()).text())
        if item.column() == 0 and self.sameRow.isChecked():
            for i in np.arange(len(self.horHeaders)-1)+1:
                otherItem = QtGui.QTableWidgetItem()
                otherItem.setText(value)
                self.molographicPatterningTable.setItem(item.row(), i, otherItem)

        if item.row() == 0 and self.intensity.text() != '':
            # time
            try:
                if self.molographicPatterningTable.item(1, item.column()) is None:
                    otherItem = QtGui.QTableWidgetItem()
                    otherItem.setText('%.1f' % (float(value)*float(self.intensity.text())))
                    self.molographicPatterningTable.setItem(1, item.column(), otherItem)
            except:
                pass

        elif item.row() == 1 and self.intensity.text() != '':
            # dose
            try:
                if self.molographicPatterningTable.item(0, item.column()) is None:
                    otherItem = QtGui.QTableWidgetItem()
                    otherItem.setText('%.1f' % (float(value)/float(self.intensity.text())))
                    self.molographicPatterningTable.setItem(0, item.column(), otherItem)
            except:
                pass


    def getTableValues(self):
        table = []
        table.append(self.horHeaders)
        table[-1].insert(0,'')
        for row in np.arange(len(self.verHeaders)):
            table.append([self.verHeaders[row]])
            for column in np.arange(len(self.horHeaders)-1):
                try:
                    table[row+1].append(str(self.molographicPatterningTable.item(row,column).text()))
                except:
                    table[row+1].append('')

        return table


    def getValues(self):
        values = dict()
        values['Intensity [mW/cm2]'] = self.intensity.text()
        values['Mask'] = self.mask.currentText()
        values['Chip'] = self.chipName.text()

        return values
