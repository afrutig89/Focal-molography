import numpy as np
import sys
import os

from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from auxiliariesMoloreader import constants
from settings import FieldConfiguration
from Snippets.Curvefitting import *
import matplotlib.pylab as plt


def powerAtMoloLocation(distanceToOutcouplingGrating,OutcoupledPower,damping=1.*np.log(10)/10*100,beamWaist = 1e-3):
    """ Calculates the power per unit length at the mololine location, by knowing the damping and the outCoupled Power

    :param damping: [Np/m] (power quantity)
    :param OutcoupledPower: W
    """
    # mologram locations

    delta = damping

    powerAtMoloLocation = 2*OutcoupledPower*np.exp(delta*distanceToOutcouplingGrating)/beamWaist

    return powerAtMoloLocation

# def determineDampingConstantField(data,chip,field,line,to_skip = []):
#     """Determines the damping constant of the waveguide from a linescan of the molographic field.
#
#     :param data: pandas Dataframe of the databasefile saved by the moloreader.
#
#     """
#
#     field = data['field']
#     line = data['line']
#     line = np.asarray(line)
#     moloLine = data['moloLine']
#     img_meanPhys = data['img_meanPhys']
#     PowerOutcoupling = data['PowerOutcoupling']

def determineDampingConstant(img_meanPhys,line = 2,moloFieldConfiguration = None,to_skip = [0, 1, 2],plot= False,name = 'Figure',path = '',ax = None,rejectoutilers = True,ylim=[0.3,1],consoleoutput=True):

    """
    Determines the damping constant of the waveguide by scanning 10 images, if rejectOutliers is true then the fit is performed multiple times and the worst points are rejected.
    :param img_meanPhys: list of the mean intensity on the molography plane for this location
    :param line: integer of which of the (standard 4) lines should be used for the damping constant calculation, default is 2. (line with the 90 rotated molograms)
    :param moloFieldConfiguration: member of class Fieldconfiguration is necessary to calculate the distance to the coupling grating.
    :param to_skip: mologram indices that are to be skipped (bad data points)

    """

    if ax == None:
        fig = plt.figure()
        ax = fig.add_subplot(111)

    if moloFieldConfiguration == None:

        moloFieldConfiguration = FieldConfiguration()

    moloFieldConfiguration.convertSI()

    # loops through the molograms.

    molograms = moloFieldConfiguration.molograms
    img_meanPhyshilf = list(img_meanPhys)
    img_meanPhys = []
    distances = []

    for i,imgMean in enumerate(img_meanPhyshilf):

        if (i+1) in to_skip: # +1 since the mologram indices run from 1-10 by defintion.
            continue

        distances.append(molograms[line-1][i].distanceToIncoupling*1e3) # convert to mm to make the fitting algorithm more robust
        img_meanPhys.append(imgMean)

    distances = np.asarray(distances)

    y = img_meanPhys
    y = y/y[0]  # normalize the data to 1
    distances = distances - distances[0] # zero the distance range

    popt = fit_and_return_para(exp_function_decay_norm,distances,y)

    if rejectoutilers:

        modeldata = exp_function_decay_norm(distances,*popt)
        
        distances,y = rejectOutliers(distances,y,modeldata)

    popt = fit_and_return_para(exp_function_decay_norm,distances,y)

    delta = popt[0] # damping constant in Np/mm
    alpha = popt[0]*10/np.log(10)*10 # damping constant in dB/cm

    if plot:
        # import matplotlib
        # matplotlib.use('AGG')
        # import matplotlib.pylab as plt

        ax.plot(distances,y,'o',label=i)
        x = np.linspace(distances[0],distances[-1],100)
        ax.plot(x,exp_function_decay_norm(np.asarray(x),*popt),'k--',alpha=0.3)
        ax.set_title(r'{} $\alpha$ = {:0.2f} dB/cm'.format(name,alpha))
        ax.set_ylim(ylim)
        ax.set_ylabel(r'norm. int. at FP a.u.')
        ax.set_xlabel(r'prop. distance $\mathrm{[mm]}$')

        fig = ax.figure
        ax.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom='on',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            )
        ax.tick_params(
            axis='y',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            left='on',      # ticks along the bottom edge are off
            right='off',         # ticks along the top edge are off
            )

        if not os.path.exists(path + '/plots'):
            os.makedirs(path + '/plots')

        fig.savefig(path + '/plots/Damping_measurements_' + name + '.png',format='png',dpi=600)
        ax.clear() # clear the axis again for the next plot


    if consoleoutput:
        print('Damping is:')
        print(alpha)
        print(delta*1000)

    return delta*1000.,alpha # Np/m/ dB/cm


def convertPhysicalPostProcess(img,camera,magnification,exposureTime):

    """
    Converts a binary image that was acquired with a certain camera type to an intensity image in W/m^2
    :param camera:, member of CamerasensorModels.
    :param img: binary image of camera
    :param magnification: magnification of the imaging system
    :param exposureTime: exposureTime [ms]
    """

    quantumEfficiency = camera.QE
    exposureTime = exposureTime*1e-3    # in SI
    pixelGain = camera.saturationCap/camera.bits # e-/ADU
    pixelsize = camera.pixelsize # pixelsize of the camera
    wavelength = 632.8e-9
    magnification = magnification

    # convert to physical units
    # number of photons
    n_p = img / (pixelGain * quantumEfficiency)

    # photon flux
    phi = (magnification)**2*n_p / (pixelsize**2 * exposureTime)
    # intensity
    I = phi * (constants.h*constants.c/wavelength)

    return I
