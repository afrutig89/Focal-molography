# -*- coding: utf-8 -*-
"""
Collection of constants needed for molography.

:author: 	Silvio Bischof
:created:	Thu Mar 31 14:34:37 2016
"""

from numpy import pi

c = 299792458.              # speed of light in vacuum
mu_0 = 4 * pi * 1e-7         # permeability
eps_0 = 1. / (c**2 * mu_0)    # electric permittivity

q_e = 1.60217662e-19          # elementary charge
N_A = 6.02214085774e23      # Avogadro constant

h = 6.62606896e-34          # Planck constant
h_bar = h / (2 * pi)        # Dirac constant
k = 1.3806504e-23           # Boltzmann constant

R = k*N_A                   # gas constant
