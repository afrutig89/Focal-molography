"""
This module stores the settings of the different units

:author:	Silvio Bischof
:created:	31-Jan-2017
"""

class CameraSettings(object):
    """
    Camera settings

    :param pixelsize: size of the
    """

    def __init__(self, model = 'UI-3480CP-M-GL Rev.2', magnification = 20):
        super(CameraSettings, self).__init__()

        self.magnification = magnification      # objective
        self.model = model

        if model == 'UI-3480CP-M-GL Rev.2':
            # camera settings
            self.pixelsize = 2.2    # in um
            self.npix = [2560, 1920]
            self.actual_pixelsize = self.pixelsize/self.magnification
            self.colorDepth = 2**12-1   # 12 bits resolution for this camera
            self.pixelClockRange = 4, 96 # in MHz
            self.minExposure = 0.031
            self.maxExposure = 2745
            self.gain_max = 100 # Gain settings 0...49 use analog signal gain; from 50 up, the stronger digital gain is used. High gain settings may cause visible noise
            # in default mode, the gain of the camera is set to 0.
            self.quantumEfficiency = 0.48 # quantum efficiency at 632.8 nm
            self.pixelGain = 0.6118 # 0.3826 was the value measured by Silvio Bischof, here I use the Gain calculated by dividing 12 bit 4095 by the full well capacity  . # 5.272 # measurements by Silvio Bischof.


    def setMagnification(self,magnification,):

        self.magnification = magnification
        self.actual_pixelsize = self.pixelsize/magnification

class Chip:
	def __init__(self, mask='D1C'):

		self.MIN_SET  = 'B'
		self.MAX_SET  = 'G'

		self.width = 14
		self.height = 57

		self.number_of_fields       = 6
		self.starting_field_index   = 'B'

		self.chipNo = 'R305'

		self.mask = mask

		self.fields = FieldConfiguration(mask=mask)


	def correspondingMologram(self, position):
		# split coordinates
		x, y = position # in mm

		# take absolute value since x is measured negatively
		x = float(x)
		y = float(y)

		field = self.mologram_field_index(y)
		line  = self.mologram_line_index(y)
		molo   = self.mologram_molo_index(x)

		return field, line, molo


	def cartesian_position(self, position):

		# split coordinates
		field, line, row = position.split(',')
		line = int(line)
		row  = int(row)

		if (field == -1) or (line == -1) or (row == -1):
			return -1,-1

		x = self.getXposition(row)
		y = self.getYposition(field, line)

		return x, y


	def getXposition(self, molo):
		"""
		B,1,1 is defined as 0
		"""

		x = self.fields.hor_spacing*(molo-1)

		return float(x)


	def getYposition(self, field, line):
		"""
		B,1,1 is defined as 0
		"""

		y = (ord(field) - ord(self.starting_field_index))*self.fields.field_spacing # field
		y += (line-1)*self.fields.ver_spacing

		return float(y)


	def mologram_field_index(self,y):

		# if (y < - self.fields.field_height/2) or (y > self.number_of_fields*self.fields.field_spacing + self.fields.bottom_margin):
		# 	return -1

		# determine mologram field
		y1 = y % self.fields.field_spacing
		y2 = y - y1

		field_number = int(y2 / self.fields.field_spacing)		# determine which field the position is in starting
		field_letter = chr(ord(self.starting_field_index) + field_number)

		return field_letter


	def mologram_line_index(self, y):

		# determine x relative to the start of the mologram field
		y = y + self.fields.ver_spacing/2 	# size of the mologram accounted for
		y = y % self.fields.field_spacing

		# determine if whithin mologram field
		if y > self.fields.field_height:
			return -1

		line_number = int(y / self.fields.ver_spacing) + 1
		return line_number


	def mologram_molo_index(self, x):
		# if (y < self.fields.side_margin) or (y > self.fields.rows*self.fields.hor_spacing + self.fields.side_margin):
		# 	return -1
		x = x + self.fields.hor_spacing/2 	# size of the mologram accounted for
		molo_number = int(x / self.fields.hor_spacing) + 1

		return molo_number


class FieldConfiguration:
	def __init__(self, mask='D1C'):

		self.bottom_margin          = 6
		self.side_margin            = 5

		self.distanceBetweenCouplingGratings = 9 # distance in mm between the two coupling gratings. 
		self.SI = False

		if mask == 'D1C':
			self.field_spacing = 9
			self.field_height  = 2
			self.field_width   = 4

			self.lines        = 4
			self.molos         = 10

			self.hor_spacing = 0.4
			self.ver_spacing = 0.5

			# define molograms
			self.molograms = []
			for i in range(self.lines):
				self.molograms.append([])
				if i == 1:
					radius = 0.4
					hor_spacing = 0.4
					ver_spacing = 0.5
					image = 'mologram_turned'
				elif i == 3:
					radius = 0.3
					hor_spacing = 0.4
					ver_spacing = 0.5
					image = 'mologram'
				else:
					radius = 0.4
					hor_spacing = 0.4
					ver_spacing = 0.5
					image = 'mologram'

				for j in range(self.molos):
					position = [j*self.hor_spacing, i*self.ver_spacing]
					distanceToIncoupling = (self.distanceBetweenCouplingGratings - (self.field_width-self.hor_spacing))/2.+ j*self.hor_spacing
					distanceToOutcoupling = self.distanceBetweenCouplingGratings - distanceToIncoupling
					self.molograms[i].append(MologramConfiguration(
							radius, hor_spacing, ver_spacing, image, position,distanceToOutcoupling,distanceToIncoupling))
		#diameters = [117.3e-6,131.5e-6,148.5e-6,168.7e-6,192.8e-6,221.4e-6,255.4e-6,295.70e-6,343.4e-6,400.0e-6]

		# if mask == 'DecreasingMoloArea1.4':

		# 	self.field_spacing = 9
		# 	self.field_height  = 2
		# 	self.field_width   = 4

		# 	self.lines        = 4
		# 	self.molos         = 10

		# 	self.hor_spacing = 0.4
		# 	self.ver_spacing = 0.5

		# 	# define molograms
		# 	self.molograms = []
		# 	for i in range(self.lines):

		# 		if i == 0:


		# 		if i == 1:
		# 			radius = 0.4
		# 			hor_spacing = 0.4
		# 			ver_spacing = 0.5
		# 			image = 'mologram_turned'
		# 		elif i == 3:
		# 			radius = 0.3
		# 			hor_spacing = 0.4
		# 			ver_spacing = 0.5
		# 			image = 'mologram'
		# 		else:
		# 			radius = 0.4
		# 			hor_spacing = 0.4
		# 			ver_spacing = 0.5
		# 			image = 'mologram'

		# 		for j in range(self.molos):
		# 			position = [j*self.hor_spacing, i*self.ver_spacing]
		# 			self.molograms[i].append(MologramConfiguration(
		# 					radius, hor_spacing, ver_spacing, image, position))

		if mask == 'A1C':
			# todo
			self.field_spacing = 9
			self.field_height  = 2
			self.field_width   = 4

			self.lines        = 4
			self.molos         = 10

			self.hor_spacing = 0.4
			self.ver_spacing = 0.5

			# define molograms
			self.molograms = []
			for i in range(self.lines):
				self.molograms.append([])
				if i == 1:
					radius = 0.4
					image = 'mologram_turned'
				elif i == 3:
					radius = 0.3
					image = 'mologram'
				else:
					radius = 0.4
					image = 'mologram'

				for j in range(self.molos):
					position = [j*self.hor_spacing, i*self.ver_spacing]
					distanceToIncoupling = (self.distanceBetweenCouplingGratings - (self.field_width-self.hor_spacing))/2.+ j*self.hor_spacing
					distanceToOutcoupling = self.distanceBetweenCouplingGratings - distanceToIncoupling
					self.molograms[i].append(MologramConfiguration(
							radius, hor_spacing, ver_spacing, image, position,distanceToOutcoupling,distanceToIncoupling))


		if mask == 'Bragg':	# Bragg
			self.field_spacing = 9
			self.field_height  = 3
			self.field_width   = 4.6

			self.lines         = 3
			self.molos        = 8

			self.hor_spacing = 0.561
			self.ver_spacing = 1.01

			# define molograms
			self.molograms = []
			for i in range(self.lines):
				self.molograms.append([])

				radius = 0.4
				image = 'mologram_rotated'

				for j in range(self.molos):
					position = [j*self.hor_spacing, i*self.ver_spacing]
					distanceToIncoupling = (self.distanceBetweenCouplingGratings - (self.field_width-self.hor_spacing))/2.+ j*self.hor_spacing
					distanceToOutcoupling = self.distanceBetweenCouplingGratings - distanceToIncoupling
					self.molograms[i].append(MologramConfiguration(
							radius, hor_spacing, ver_spacing, image, position,distanceToOutcoupling,distanceToIncoupling))

	def convertSI(self):
		if self.SI == False:

			for i in self.molograms:
				for j in i:
					j.convertSI()

			self.SI = True


class MologramConfiguration:
	def __init__(self, radius=0.400, hor_spacing=0.400, ver_spacing=0.500, image='mologram', position=None,distanceToOutcoupling = None,distanceToIncoupling = None):
		"""
		Saves mologram in an object

		All parameters given in mm
		:param radius: radius of the mologram
		:param hor_spacing: horizontal spacing between molograms
		:param ver_spacing: vertical spacing between molograms
		:param image: Image for mologram
		:param position: position of mologram ([0] vertical (x), [1] horizontal (y))
		"""

		self.radius = radius
		self.hor_spacing = hor_spacing
		self.ver_spacing = ver_spacing
		self.image = image
		self.position = position
		self.distanceToOutcoupling = distanceToOutcoupling
		self.distanceToIncoupling = distanceToIncoupling

		self.SI = False

	# do not use this function with the object that controls the motors in the moloreader framework. 
	def convertSI(self):

		if self.SI == False:
			self.radius = self.radius*1e-3
			self.hor_spacing = self.hor_spacing*1e-3
			self.ver_spacing = self.ver_spacing*1e-3
			self.position = [self.position[0]*1e-3,self.position[1]*1e-3]
			self.distanceToOutcoupling = self.distanceToOutcoupling*1e-3
			self.distanceToIncoupling = self.distanceToIncoupling*1e-3

			self.SI = True





# parameters that will be stored in the parameters database table
logExperimentDict = {
    # general simulation settings
    'ID':'integer primary key AUTOINCREMENT',
    'date' : 'text',
    'startTime' : 'text',
	'algorithm' : 'text',

    # database settings
	'experimentFolder' : 'text',

    # experiment settings
	'experimentType' : 'text',

	# array measurement
	'molos' : 'text',
	'measureSurface' : 'integer',
	'measureMologram' : 'integer',

	# time stack
	'numCaptures' : 'integer',
	'numAvg' : 'integer',
	'timeInterval' : 'real',

	# position stack
	'stackIncrement'  : 'real',

	# image acquisition
	'autoExposure' : 'integer',
	'fixedExposure' : 'real',
	'saveImages' : 'text',

	# general settings
	'pixelsize' : 'real'
}

# I should program that I can use different results dicts and logExperiment dicts easily from the GUI.

resultsDict = {
	'experimentID' : 'integer',
    'ID' : 'integer primary key AUTOINCREMENT',
	'startTime' : 'real',

	'signalIntensity' : 'real',
	'referenceIntensity' : 'real',
	'bkg_mean' : 'real',
	'bkg_std' : 'real',
	'maxsignal':'real',
	'aquisitionTime':'real',
	#'MIExpWeighted' : 'real',
	'signalIntensityPhys' : 'real',
	'referenceIntensityPhys' : 'real',
	'maxsignalPhys':'real',
	'SNR' : 'real',

	'averagedImages' : 'integer',
	'imageName' : 'text',
	'exposureTime' : 'real',
	'gain' : 'integer',

	'chipNo':'text',
	'field' : 'text',
	'line' : 'integer',
	'moloLine' : 'integer',
	'position' : 'text',
	'z_position' : 'real',
	'y_position':'real',
	'x_positon':'real',
	'PowerOutcoupling':'real',
	'img_meanPhys': 'real',
	'bkg_meanPhys': 'real',
	'bkg_stdPhys':'real'
}

resultsDictWGDamp = {

	'chipNo':'text',
	'field':'text',
	'dampingWGdBcm':'real',
	'dampingWGNpm':'real',
}


variables = {	# variable : description
	# general simulation settings
    'date' : 'Date',
    'ID' : 'ID',
    'startTime' : 'Start time',
	'algorithm' : 'Algorithm',

    # database settings
    'saveDatabase' : 'Save database',
	'experimentFolder' : 'Experiment folder',
    'databaseFile' : 'Database file',

    'saveLog' : 'Save log',

    # experiment settings
	'experimentType' : 'Experiment type',

	# array measurement
	'molos' : 'Molograms',
	'noOfMolos' : 'Number of molograms',
	'measureSurface' : 'Measure surface',
	'measureMologram' : 'Measure mologram',
	'estimatedTime' : 'Estimated time',
	'coupling': 'Adjust Coupling',
	'couplingline': 'Adjust Coupling Line',
	'dampingMeasurement':'dampingMeasurement',

	# time stack
	'numCaptures' : 'Number of captures',
	'numAvg' : 'Number of averages',
	'timeInterval' : 'Time interval',

	# position stack
	'stackIncrement'  : 'Stack increment',
	'startPosition' : 'Start position (relative)',
	'startExposure' : 'Start exposure',
	'startGain' : 'Start gain',

	# image acquisition
	'focus' : 'Focus',
	'center' : 'Center',
	'autoExposure' : 'Auto exposure',
	'adjustExposure' : 'Adjust exposure',
	'fixedExposure' : 'Fixed exposure',
	'saveImages' : 'Save images',
	'subtractPreviousImage' : 'Subtract previous image',

	'gain' : 'Gain',

	'experimentID' : 'Experiment ID',
	'mologram' : 'Mologram',


	'z_position' : 'z-position',
	'x_position' : 'x-position',
	'y_position' : 'y-position',
	'position' : 'Position',

	'averagedImages' : 'Averaged images',
	'imagePath' : 'Image path',
	'exposureTime' : 'Exposure time',

	'measTime' : 'Time',
	'signalIntensity' : 'Signal intensity',
	'referenceIntensity' : 'Reference Signal Intensity',
	'img_mean':'Image mean', # mean of the entire image
	'bkg_mean' : 'Background mean',
	'bkg_std' : 'Background standard deviation',
	'maxsignal':'Maximum in Image',
	#'MIExpWeighted' : 'Weighted signal intensity',
	'SNR' : 'Signal-to-noise ratio',

	'field' : 'Field',
	'line' : 'Line',
	'moloLine'	: 'Mololine',

	'pixelsize' : 'Pixelsize',

	'signalIntensityPhys' : 'Signal intensity (W/m2)',
	'referenceIntensityPhys' : 'Reference Signal intensity (W/m2)',
	'img_meanPhys': 'Image mean (W/m2)',
	'bkg_meanPhys':'Background mean (W/m2)',
	'bkg_stdPhys':'Background std (W/m2)',
	'maxsignalPhys':'Maximum in Image (W/m2)',

	# Photodiode Values:
	'PowerOutcoupling':'Power at outcoupling grating (W)'
}


def getLogExperimentDict():
	return logExperimentDict


def getResultsDict():
	return resultsDict

def getWGDampResultsDict():
	return resultsDictWGDamp

def getVariables():
	return variables


if __name__ == '__main__':

	molos = FieldConfiguration().molograms

	for i in molos:
		for j in i:
			print(j.distanceToOutcoupling)
