from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from PyQt4 import QtGui,QtCore, QtSql
import time

from SharedLibraries.Database.dataBaseOperations import saveDB, loadDB, createDB, createSQLDict,loadDB
import numpy as np
import scipy as sp
import Data_Processing.MologramProcessing as mologram_processing
import Data_Acquisition.DataBase as db

import auxiliariesMoloreader.constants as constants
from auxiliariesMoloreader.intensityConversionDamping import determineDampingConstant
from settings import getWGDampResultsDict
from SimulationFramework.auxiliaries.Journal_formats_lib import formatPRX
from Data_Acquisition.permission import changePermissionsRecursive,changePermissionsPath


import matplotlib.pylab as plt

# an experiment should have a chip and a moloreader with which the experiment is conducte and a database to which all the data is saved to.


class Experiment(QtCore.QThread):

	status = QtCore.pyqtSignal(list)
	statusMessage = QtCore.pyqtSignal(str)

	def __init__(self, main_widget, experimentFolder):

		# create an axis for plotting in the thread.
		formatPRX(2,2)
		fig = plt.figure()
		self.ax = fig.add_subplot(111)
		x = np.linspace(0,1,100)
		# Call parent class constructor
		super(Experiment,self).__init__()

		self.exiting = False
		self.main_widget = main_widget
		self.setup = main_widget.setup
		self.camera = main_widget.setup.camera # why is this a camera and not a camera feed, because the camera needs to set_exposure values etc.
		self.cameraControl = main_widget.cameraWidget.cameraControl
		self.cameraFeed = main_widget.cameraWidget.cameraFeed
		self.chipNo = self.main_widget.setup.chip.chipNo
		self.path = experimentFolder

		self.stop_thread  = False
		self.pause_thread = False


	""" Record experiment data as per parameters passed in as arguments """
	def record_data(
		self,
		experimentType,

		imageFolder = 'images',

		# array measurement
		mologram = None,
		measureSurface = None,
		measureMologram = None,
		height = None,
		coupling = None,
		couplingline = None,

		# time stack
		numCaptures = None,
		numAvg = None,
		timeInterval = None,

		# position stack
		stackIncrement = None,
		startPosition = None,
		startExposure = None,
		startGain = None,

		# database
		ID = None,
		algorithm = None,

		# image acquisition
		focus = None,
		center = None,
		autoExposure = None, # boolean, if true, then this performs a binary search in order to find the optimal exposure. 
		adjustExposure = None, # boolean, for realtime timestacks only, this is in order to adjust the exposure if it becomes saturated by steps of 10 %
		fixedExposure = None, # float, value is the exposure time that will be used in order to acquire an image
		saveImages = None, # original,crop and false
		dampingMeasurement = None,

		subtractPreviousImage = False,
	):

		self.experimentType	= experimentType
		self.imageFolder = imageFolder

		self.mologram = mologram
		self.measureSurface	= measureSurface
		self.measureMologram = measureMologram
		self.height = height
		self.coupling = coupling
		self.couplingline = couplingline

		self.numCaptures = numCaptures
		self.numAvg	= numAvg
		self.timeInterval = timeInterval

		self.stackIncrement	= stackIncrement
		self.startPosition = startPosition
		self.startExposure = startExposure
		self.startGain = startGain

		self.ID	= ID
		self.algorithm = algorithm
		self.cameraFeed.algorithm = self.algorithm

		self.focus = focus
		self.center = center
		self.autoExposure	= autoExposure 
		self.adjustExposure = adjustExposure
		self.fixedExposure = fixedExposure
		self.saveImages	= saveImages
		self.dampingMeasurement = dampingMeasurement
		self.subtractPreviousImage = subtractPreviousImage
		print(self.subtractPreviousImage)

		self.lowSignalDetection = False
		self.cameraFeed.lowSignalDetection = self.lowSignalDetection
		self.referenceMologram = self.cameraControl.referenceMologram.isChecked()

		self.start()

	def run(self):

		if self.experimentType == 'Go to position':
			print('>> Go to position {}'.format(self.mologram))
			# move to mologram
			self.main_widget.move_to_nothread(self.mologram)

			if self.height == None:
				pass

			elif self.height != 'Surface':
				# move to surface
				print('>> Move to surface height')
				self.main_widget.manualControlTab.savedPositions.onGoToSurfaceHeightClicked()

			elif self.height != 'Mologram':
				# move to focal plane
				print('>> Move to mologram height')
				self.main_widget.manualControlTab.savedPositions.onGoToMologramHeightClicked()

			else:
				# move to given z-position
				print('>> Move to surface height')
				currentPosition = self.main_widget.setup.motors.z.pos('mm')
				relativeDistance = self.height-currentPosition

				self.main_widget.manualControlTab.axisControl.z.start_moving(
					distance = relativeDistance,
					unit	 = 'mm',
					relative = True
				)


		elif self.experimentType == 'Focus':
			# take image
			image,retval = self.cameraFeed.getImage()
			# calculate mologram intensity. Including reference mologram is selected
			results = mologram_processing.calc_datapoint_entry(image, self.algorithm, lowSignalSearch=self.lowSignalDetection)
			signalIntensity, bkg_mean, bkg_std, SNR, referenceIntensity = results

			# coarse focus only if no mologram present
			if signalIntensity == 0:
				print('>> Coarse focus')
				image, signalIntensity = self.cameraFeed.focusAndOptimizeIncoupling(numberOfSteps=10, stepSize=10)

			# fine focus
			print('>> Fine focus')
			image, signalIntensity = self.cameraFeed.focusAndOptimizeIncoupling(numberOfSteps=20, stepSize=1)


		elif self.experimentType == 'Center':
			# center image
			print('>> Center image')
			self.cameraFeed.centerImage(image)
			self.main_widget.manualControlTab.savedPositions.setPosition('{}'.format(molo))
			self.main_widget.manualControlTab.savedPositions.onSetMologramHeightClicked()


		elif self.experimentType == 'Time stack':
			# time stack
			print('time stack')
			self.timeStack()

		elif self.experimentType == 'z-position':
			# z-stack
			print('z-stack')
			self.zStack()

		elif self.experimentType == 'Exposure':
			print('Exposure stack')
			self.exposureStack()

		elif self.experimentType == 'Gain':
			print('Gain stack')
			self.gainStack()

		elif self.experimentType == 'Array measurement':
			self.arrayMeasurements()

		else:
			print('Experiment type not implemented')

		# change the permission of all the folders such that one can edit the data.
		changePermissionsRecursive(self.path)

		self.stop_thread  = False

		self.emit(QtCore.SIGNAL("done_experiment"))

	def timeStack(self):

		self.main_widget.tabs_top.setCurrentIndex(1)
		position = str(self.main_widget.getPosition())
		field, line, moloLine = self.mologram.split(',')
		startExperiment = time.time()
		exposureTime = self.camera.exposure

		for i in range(self.numCaptures):

			print('Measurement {}/{}'.format(i+1, self.numCaptures))
			img_avg = 0

			# Record start time and starting position
			startTime = time.time()

			# init some variables
			time_averaging = 0

			k = 0
			while k < float(self.numAvg):
				# float in order to take string 'inf' into account

				# if time interval + extra time is over, break
				if time.time() - startTime + (k+1)*time_averaging >= self.timeInterval:
					# time not enough to make another capture
					# todo: adjust database log
					if k == 0:
						print('Error: choose a higher time interval')
						return
					break

				# If stop or pause button was pressed, exit loop
				if self.stop_thread: break
				while self.pause_thread: continue

				# Determine file name for image
				imageName  = self.main_widget.getImageName()
				imageName += '_{}'.format((i+1))
				if float(self.numAvg) > 1:
					imageName += '_({})'.format(k)

				# Try a few times to capture/save
				print('Capturing {}: {} ({})'.format(imageName, (i+1), k))

				img,retval = self.cameraFeed.getImage()

				if self.autoExposure and np.max(img) > 0.975*self.setup.camera.specifications.colorDepth:
				# adjusts exposure when max pix in image > than 0.9*4095 (pix saturation)
					print('>> signal close to saturation --> decrease expTime')
					if self.autoExposure and exposureTime<self.setup.camera.maxExposure*0.5:
					# only do binary search when exposure time is less than maxExposuretime*0.5
						print('>> binary search')
						img, exposureTime = self.adjustExposure(
								self.setup.camera.minExposure, exposureTime)
						self.cameraControl.update_exposure_value()

						# # decrease exposure time by 30% if exposure time over 0.5*maxExposuretime
						# print('>> expTime too high for binary search --> decrease 30%')
						# self.camera.set_exposure(exposureTime*0.7)
						# self.cameraControl.update_exposure_value()


					if self.autoExposure and exposureTime>=self.setup.camera.maxExposure*0.5:
					# decrease exposure time by 30% if exposure time over 0.5*maxExposuretime
						print('>> expTime too high for binary search --> decrease 30%')
						self.camera.set_exposure(exposureTime*0.7)
						self.cameraControl.update_exposure_value()

				if self.autoExposure and np.max(img) < 0.75*self.setup.camera.specifications.colorDepth:
				# adjusts exposure when max pix in image < than 0.6*4095 (pix saturation)
					print('>> signal too low --> increase expTime')
					if self.autoExposure and exposureTime<self.setup.camera.maxExposure*0.5:
					# # only do binary search when exposure time is less than maxExposuretime*0.5
					# 	print('>> binary search')
					# 	img, exposureTime = self.adjustExposure(
					# 							self.setup.camera.minExposure,
					# 							self.setup.camera.maxExposure)
						self.camera.set_exposure(exposureTime*1.3)
						self.cameraControl.update_exposure_value()



					if self.autoExposure and exposureTime>=self.setup.camera.maxExposure*0.5 and exposureTime<self.setup.camera.maxExposure:
					# decrease exposure time by 10% if exposure time over 0.5*maxExposuretime
						print('>> expTime too high for binary search --> increase 10%')
						self.camera.set_exposure(exposureTime*1.1)
						self.cameraControl.update_exposure_value()

				elif not self.autoExposure:
					self.camera.set_exposure(self.fixedExposure)



				if self.adjustExposure:
					while np.max(img) == self.camera.specifications.colorDepth and time.time()-time1 > 1:
						self.camera.set_exposure(exposureTime*0.9)
						self.cameraControl.update_exposure_value()
						img,retval = self.cameraFeed.getImage()
					if np.max(img) < self.camera.specifications.colorDepth*0.8:
						self.cameraFeed.adjustExposure()
						self.cameraControl.update_exposure_value()
					elif np.max(img) < self.camera.specifications.colorDepth:
						self.camera.set_exposure(exposureTime)
						self.cameraControl.update_exposure_value()

				exposureTime = self.camera.exposure

				img_avg += img.astype('int64')	 # average img (redeclare type of image in order to exceed 2^12)

				# # save image
				# if self.saveImages != 'False' and float(self.numAvg) > 1:
				#	 imagePath = '{}/{}/{}'.format(self.path, self.imageFolder, imageName)
				#	 self.saveImage(img, imagePath)

				k += 1
				time_averaging = (time.time()-startTime)/(k+1)


			# If stop or pause button was pressed, exit loop
			if self.stop_thread: break
			while self.pause_thread: continue

			# make average file and name it with base name
			averagedImages=k
			img_avg = img_avg/averagedImages

			if self.subtractPreviousImage:
				if 'img_old' in locals():
					# subtract previous image
					img_diff = np.abs(img_avg - img_old)
					print('image subtracted')
					img_old = img_avg
					# in order to plot and save to database define img_diff as img_avg
					img_avg = img_diff

				else:
					img_old = img_avg
					# Wait until delay is passed
					while i < (self.numCaptures-1) and time.time() - startTime < self.timeInterval:

						# If stop or pause button was pressed, exit loop
						if self.stop_thread: break
						while self.pause_thread: continue

						continue

			# show image
			self.cameraFeed.plotImage(img_avg)
			self.main_widget.updatePos()

			if self.saveImages != 'False':
				imageName  = self.main_widget.getImageName()
				imageName += '_{}'.format((i+1))
				imagePath = '{}/{}/{}'.format(self.path, self.imageFolder, imageName)
				print(self.saveImages)
				self.saveImage(img_avg, imagePath)

			if self.ID != None:
				print('>> Save database entry')
				exposureTime = self.camera.exposure
				pixelsize = self.camera.info['pixel_size']
				# measure the interested paraeters
				signalIntensity, bkg_mean, bkg_std, SNR, referenceIntensity = mologram_processing.calc_datapoint_entry(
																				img_avg,
																				self.algorithm,
																				pixelsize = pixelsize,
																				lowSignalSearch=self.lowSignalDetection,
																				referenceMologram=self.referenceMologram
																			)

				# convert signals to physical values
				signalIntensityPhys = self.cameraFeed.convertPhysical(signalIntensity)
				referenceIntensityPhys = self.cameraFeed.convertPhysical(referenceIntensity)

				experimentID = self.ID
				startTime = startTime-startExperiment	# relative time to experiment start
				self.saveDatabase('results', locals())

			# update camera plot and stack plot
			print("lauched Signal to update GUI")
			self.emit(QtCore.SIGNAL("database_updated"))

			# Wait until delay is passed
			while i < (self.numCaptures-1) and time.time() - (startTime+startExperiment) < self.timeInterval:

				# If stop or pause button was pressed, exit loop
				if self.stop_thread: break
				while self.pause_thread: continue

				continue

	def zStack(self):
		self.main_widget.tabs_top.setCurrentIndex(1)

		# define z-position
		z_start = self.setup.motors.z.pos('um')
		print('Move up {} steps'.format(self.startPosition))
		# start, unit = self.startPosition.split(' ')
		self.setup.motors.z.move_steps_safe(self.startPosition)
		time.sleep(0.5)
		startExperiment = time.time()

		for i in range(self.numCaptures):
			startTime = time.time()
			z_position = self.setup.motors.z.pos('um')-z_start
			position = str(self.main_widget.getPosition())
			field, line, moloLine = self.mologram.split(',')
			print('Measurement {}/{}, z-position: {}'.format(i+1, self.numCaptures, z_position))
			time.sleep(0.2)

			# Determine file name for image
			imageName = self.main_widget.getImageName()
			imageName += '_{}'.format((i+1))
			imageName += '_{}'.format(z_position)

			img_avg = 0

			# init some variables
			time_averaging = 0
			averagedImages = 1
			k = 0
			while k < self.numAvg:

				# If stop or pause button was pressed, exit loop
				if self.stop_thread: break
				while self.pause_thread: continue

				# Try a few times to capture/save
				print('Capturing {}: {} ({})'.format(imageName, (i+1), (k+1)))

				img,retval = self.cameraFeed.getImage()
				if self.autoExposure and np.max(img) == self.setup.camera.specifications.colorDepth:
					# adjust down only when saturated
					img, exposureTime = self.cameraFeed.adjustExposure(
							self.setup.camera.minExposure, exposureTime)
					self.cameraControl.update_exposure_value()

				elif not self.autoExposure:
					self.camera.set_exposure(self.fixedExposure)

				exposureTime = self.camera.exposure

				img_avg += img.astype('int64')	 # average img (redeclare type of image in order to exceed 2^12)

				# # save image
				# if self.saveImages != 'False' and float(self.numAvg) > 1:
				#	 imageName += '_({})'.format(k)
				#	 imagePath = '{}/{}/{}'.format(self.path, self.imageFolder, imageName)
				#	 self.saveImage(img, imagePath)

				k += 1
				time_averaging = (time.time()-startTime)/(k+1)


			# If stop or pause button was pressed, exit loop
			if self.stop_thread: break
			while self.pause_thread: continue

			# make average file and name it with base name
			averagedImages = k+1
			img_avg = img_avg/averagedImages

			# show image
			self.cameraFeed.plotImage(img_avg)
			self.main_widget.updatePos()

			if self.saveImages != 'False':
				imageName  = self.main_widget.getImageName()
				imageName += '_{}'.format(z_position)
				imagePath = '{}/{}/{}'.format(self.path, self.imageFolder, imageName)
				self.saveImage(img_avg, imagePath)


			if self.ID != None:
				print('>> Save database entry')
				signalIntensity, bkg_mean, bkg_std, SNR, referenceIntensity = mologram_processing.calc_datapoint_entry(img_avg, self.algorithm, lowSignalSearch=self.lowSignalDetection)
				signalIntensityPhys = self.cameraFeed.convertPhysical(signalIntensity)
				referenceIntensityPhys=self.cameraFeed.convertPhysical(referenceIntensity)
				experimentID = self.ID
				startTime = startTime-startExperiment	# relative time to experiment start
				self.saveDatabase('results', locals())

			# update camera plot and stack plot
			print("lauched Signal to update GUI")
			self.emit(QtCore.SIGNAL("database_updated"))

			print('Move motor by {} steps'.format(self.stackIncrement))
			self.setup.motors.z.move_steps_safe(self.stackIncrement)
			time.sleep(1)


		# go back to start position
		print('Go back to start position')
		self.setup.motors.z.move_steps_safe(-int(self.stackIncrement*self.numCaptures)-int(self.startPosition))


	def exposureStack(self):
		self.main_widget.tabs_top.setCurrentIndex(1)
		self.camera.set_exposure(self.startExposure)
		position = str(self.main_widget.getPosition())
		field, line, moloLine = self.mologram.split(',')
		startExperiment = time.time()

		for i in range(self.numCaptures):
			exposureTime = self.camera.exposure
			print('Measurement {}/{}, exposure time: {}, pixelclock: {}'.format(i+1, self.numCaptures, exposureTime, self.camera.pixelclock))
			self.cameraControl.update_exposure_value()
			startTime = time.time()

			# Determine file name for image
			imageName = self.main_widget.getImageName()
			imageName += '_{}'.format((i+1))

			img_avg = 0

			# init some variables
			time_averaging = 0
			k = 0
			while k < self.numAvg:
				# float in order to take string 'inf' into account


				# If stop or pause button was pressed, exit loop
				if self.stop_thread: break
				while self.pause_thread: continue

				# Try a few times to capture/save
				print('Capturing {}: {} ({})'.format(imageName, (i+1), k))

				img,retval = self.cameraFeed.getImage()

				img_avg += img.astype('int64')	 # average img (redeclare type of image in order to exceed 2^12)
				k += 1
				time_averaging = (time.time()-startTime)/(k+1)
				time.sleep(0.3)


			# If stop or pause button was pressed, exit loop
			if self.stop_thread: break
			while self.pause_thread: continue

			# make average file and name it with base name
			averagedImages = k+1
			img_avg = img_avg/averagedImages

			if self.subtractPreviousImage:
				if 'img_old' in locals():
					# subtract previous image
					img_diff = np.abs(img_avg - img_old)
					print('image subtracted')
					img_old = img_avg
					# in order to plot and save to database define img_diff as img_avg
					img_avg = img_diff

				else:
					img_old = img_avg
					continue

			# show image
			self.cameraFeed.plotImage(img_avg)

			if self.saveImages != 'False':
				imageName  = self.main_widget.getImageName()
				imageName += '_{}'.format((i+1))
				imagePath = '{}/{}/{}'.format(self.path, self.imageFolder, imageName)
				self.saveImage(img_avg, imagePath)


			if self.ID != None:
				print('>> Save database entry')
				signalIntensity, bkg_mean, bkg_std, SNR, referenceIntensity = mologram_processing.calc_datapoint_entry(img_avg, self.algorithm, lowSignalSearch=self.lowSignalDetection, referenceMologram=self.referenceMologram)
				signalIntensityPhys = self.cameraFeed.convertPhysical(signalIntensity)
				referenceIntensityPhys=self.cameraFeed.convertPhysical(referenceIntensity)
				experimentID = self.ID
				startTime = startTime-startExperiment	# relative time to experiment start
				self.saveDatabase('results', locals())

			# update camera plot and stack plot
			print("lauched Signal to update GUI")
			self.emit(QtCore.SIGNAL("database_updated"))

			if exposureTime + self.stackIncrement < self.setup.camera.maxExposure:
				print('Increment exposure time by {}'.format(self.stackIncrement))
				self.camera.set_exposure(exposureTime + self.stackIncrement)

			else:
				print('No more increase possible')
				break


	def gainStack(self):
		self.main_widget.tabs_top.setCurrentIndex(1)
		self.camera.gain = int(self.startGain)
		exposureTime = self.camera.exposure
		position = str(self.main_widget.getPosition())
		field, line, moloLine = self.mologram.split(',')
		time.sleep(1)
		startExperiment = time.time()

		for i in range(self.numCaptures):
			gain = self.camera.gain
			print('Measurement {}/{}, gain: {}'.format(i+1, self.numCaptures, gain))
			startTime = time.time()

			# Determine file name for image
			imageName = self.main_widget.getImageName()
			imageName += '_{}'.format((i+1))
			imageName += '_{}'.format(gain)

			img_avg = 0

			# init some variables
			time_averaging = 0
			averagedImages = 1
			k = 0
			while k < self.numAvg:
				# float in order to take string 'inf' into account


				# If stop or pause button was pressed, exit loop
				if self.stop_thread: break
				while self.pause_thread: continue

				# Try a few times to capture/save
				print('Capturing {}: {} ({})'.format(imageName, (i+1), k))

				img,retval = self.cameraFeed.getImage()

				img_avg += img.astype('int64')	 # average img (redeclare type of image in order to exceed 2^12)
				k += 1
				time_averaging = (time.time()-startTime)/(k+1)
				time.sleep(0.3)


			# If stop or pause button was pressed, exit loop
			if self.stop_thread: break
			while self.pause_thread: continue

			# make average file and name it with base name
			img_avg = img_avg/averagedImages

			# make average file and name it with base name
			img_avg = img_avg/averagedImages

			if self.subtractPreviousImage:
				if 'img_old' in locals():
					# subtract previous image
					img_diff = np.abs(img_avg - img_old)
					print('image subtracted')
					img_old = img_avg
					# in order to plot and save to database define img_diff as img_avg
					img_avg = img_diff

				else:
					img_old = img_avg
					continue


			# show image
			self.cameraFeed.plotImage(img_avg)

			if self.saveImages != 'False':
				imageName  = self.main_widget.getImageName()
				imageName += '_{}'.format((i+1))
				imagePath = '{}/{}/{}'.format(self.path, self.imageFolder, imageName)
				self.saveImage(img_avg, imagePath)


			if self.ID != None:
				print('>> Save database entry')
				signalIntensity, bkg_mean, bkg_std, SNR, referenceIntensity = mologram_processing.calc_datapoint_entry(img_avg, self.algorithm, lowSignalSearch=self.lowSignalDetection, referenceMologram=self.referenceMologram)
				signalIntensityPhys = self.cameraFeed.convertPhysical(signalIntensity)
				referenceIntensityPhys=self.cameraFeed.convertPhysical(referenceIntensity)
				experimentID = self.ID
				startTime = startTime-startExperiment	# relative time to experiment start
				self.saveDatabase('results', locals())

			# update camera plot and stack plot
			print("lauched Signal to update GUI")
			self.emit(QtCore.SIGNAL("database_updated"))

			print('Increment gain time by {}'.format(self.stackIncrement))
			self.camera.gain = int(gain + self.stackIncrement)



	def arrayMeasurements(self):
		"""This function performs an array measurement.
		"""
		noOfMolos = len(self.mologram)
		i = 0
		startExperiment = time.time()
		experimentID = self.ID
		exposureTime = self.camera.exposure

		#variables used to control coupling line.
		lastline = 0
		lastfield = 0


		# save the variables in case they are changed by the damping measurement
		focus = self.focus
		coupling = self.coupling
		measureSurface = self.measureSurface
		measureMologram = self.measureMologram
		fixedExposure = self.fixedExposure 
		autoExposure = self.autoExposure # boolean, whether to do autoexposure or not.
		center = self.center

		self.status.emit(['start'])
		for molo in self.mologram:

			# If stop or pause button was pressed, exit loop
			if self.stop_thread: break
			while self.pause_thread: continue

			# init measurement
			self.status.emit([molo, 'running']) # status
			field, line, moloLine = molo.split(',')

			# estimate rest of the time
			i += 1
			if i == 1:
				self.statusMessage.emit('Measurement {}/{}'.format(i,noOfMolos))
			else:
				expectedTime = ((time.time() - startExperiment)*(noOfMolos-i+1)/i)
				self.statusMessage.emit('Measurement {}/{}. Expected time remaining: {} min {} s'.format(i,noOfMolos,int(expectedTime/60), round(expectedTime-60*int(expectedTime/60),2)))

			# move to mologram
			self.main_widget.move_to_nothread(molo,updatePositionOnly = True)



			# if the line has changed I optimize the coupling.
################################################################################################
################################################################################################
# adjust coupling line
			if self.couplingline and lastline != molo.split(',')[1] and lastfield != molo.split(',')[0]:

				self.cameraFeed.adjustCoupling()
				lastline = molo.split(',')[1]
				lastfield = molo.split(',')[0]

			print('Current line and Damping Measurement')
			print(lastline)
			print(self.dampingMeasurement)

			if int(lastline) == 2 and self.dampingMeasurement:

				self.focus = False
				self.coupling = False
				self.measureSurface = False
				self.measureMologram = True
				self.fixedExposure = 500. 
				self.autoExposure = False
				self.center = False
				print('Ready for damping measurement')

			else:
				self.focus = focus
				self.coupling = coupling
				self.measureSurface = measureSurface
				self.measureMologram = measureMologram
				self.fixedExposure = fixedExposure 
				self.autoExposure = autoExposure
				self.center = center

			# 1 second sleep to avoid vibrations
			time.sleep(1)

			# If stop or pause button was pressed, exit loop
			if self.stop_thread: break
			while self.pause_thread: continue

################################################################################################
################################################################################################
# adjust coupling
			if self.coupling:
				print('>> adjust coupling fast')
				self.cameraFeed.adjustCoupling()

################################################################################################
################################################################################################
# measure mologram
			if self.measureMologram:

				print('{} mologram'.format(molo))
				position = 'mologram'
				startTime = time.time()-startExperiment # start time measurement

				# preadjust coupling with the algorithm that only looks at mean and stanarddeviation of the image.

				imageAdjusted = False

				# move to mologram height
				print('>> move to mologram height')
				# this function is still in a thread...
				self.main_widget.manualControlTab.savedPositions.onGoToMologramHeightClickedNoThread()
				# 1 second sleep to avoid vibrations
				time.sleep(1)

				# coarse focus on mologram
################################################################################################
################################################################################################
# coarse focus
				if self.focus:

					print('>> Focus on mologram')
					self.cameraFeed.coarseFocusZ() # spans at least 60 um.

				# do the centering
				if self.stop_thread: break
				while self.pause_thread: continue

################################################################################################
################################################################################################
# center
				if self.center:
					print('>> Centered Mologram')
					self.cameraFeed.centerImage(NoThread = True)
					self.main_widget.manualControlTab.savedPositions.setPosition('{}'.format(molo))
					self.main_widget.manualControlTab.savedPositions.onSetMologramHeightClicked()
################################################################################################
################################################################################################
# fine focus
				# do the fine focus on the mologram
				if self.focus:

					print('>> fine focus on mologram')
					self.cameraFeed.fineFocusZ()

################################################################################################
################################################################################################
# set the Exposure
				if self.autoExposure:
					self.cameraFeed.adjustExposure()
				else:
					exposureTime = self.fixedExposure
					self.camera.set_exposure(exposureTime)

					# there is a fixed exposure then set this as the exposure time of the camera.
					print('>> set Exposure')

				# get the final image
				self.cameraFeed.updateCameraFeed()
				self.main_widget.updatePos()

				# save image
				print('>> Save image')
				imageName = '{}_mologram{}'.format(self.main_widget.getImageName(),experimentID)

				self.cameraFeed.saveImageAndData(imageName,cropImage = self.saveImages)

				# set focus height
				if self.cameraFeed.signalIntensityPhys != 0:
					z_position = self.main_widget.setup.motors.z.pos('mm')
					self.main_widget.manualControlTab.savedPositions.onSetMologramHeightClicked()

			# If stop or pause button was pressed, exit loop
			if self.stop_thread: break
			while self.pause_thread: continue

################################################################################################
################################################################################################
# measure the surface
			if self.measureSurface:


				print('>> Measure {} surface'.format(molo))
				position = 'surface'
				startTime = time.time()-startExperiment  # start time measurement

				# move to surface and save z-position
				print('>> Move to surface height')
				self.main_widget.manualControlTab.savedPositions.onGoToSurfaceHeightClickedNoThread()
				# 1 second sleep to avoid vibrations
				time.sleep(1)

################################################################################################
################################################################################################
# focus
				if self.focus:

					# focus on surface
					print('>> Focus on surface')

					self.cameraFeed.coarseFocusZ() # spans at least 60 um.
					self.camera.exposure
					if self.stop_thread: break
					while self.pause_thread: continue
					self.cameraFeed.fineFocusZ()
					self.camera.set_exposure(exposureTime) # set the exposure time back to the value which it was before the focusing started. 


					# set new surface height
					if self.cameraFeed.signalIntensityPhys != 0:
						z_position = self.main_widget.setup.motors.z.pos('mm')
						self.main_widget.manualControlTab.savedPositions.onSetSurfaceHeightClicked()

################################################################################################
################################################################################################
# set the Exposure
				if self.autoExposure:
					self.cameraFeed.adjustExposure()
				else:
					exposureTime = self.fixedExposure
					self.camera.set_exposure(exposureTime)


				self.cameraFeed.updateCameraFeed()
				self.main_widget.updatePos()

				# save image
				imageName = '{}_surface{}'.format(self.main_widget.getImageName(),experimentID)

				self.cameraFeed.saveImageAndData(imageName,cropImage = self.saveImages)

			print('Check whether to do the damping measurement:')
			print(int(line))
			print(moloLine)
			print(int(moloLine))
			print(self.dampingMeasurement)
			print((int(line) == 2 and self.dampingMeasurement and int(moloLine) == 10))

			if (int(line) == 2 and self.dampingMeasurement and int(moloLine) == 10):

				self.calculateAndSaveDampingConstant(field)

			self.status.emit([molo, 'done'])

		self.statusMessage.emit('Measurements done. Time required: {}'.format((time.time()-startExperiment)))

	def adjustImage(self, image, molo):
		# redundant to be deleted!!! once the other stacks than array measurement stacks are updated.
		"""
		:param molo: string 'field,row,line'
		"""
		# center image
		if self.center:
			print('>> Center image')
			self.cameraFeed.centerImageNoThread(image)
			self.main_widget.manualControlTab.savedPositions.setPosition('{}'.format(molo))
			self.main_widget.manualControlTab.savedPositions.onSetMologramHeightClicked()

		if self.autoExposure:
			print('>> Auto exposure')
			self.cameraFeed.adjustExposure()
			self.cameraControl.update_exposure_value()

		elif self.adjustExposure:
			print('>> Adjust exposure by 10%')
			if (np.sum(image == self.camera.specifications.colorDepth) > 1) \
					and (self.camera.exposure > self.camera.minExposure):
				self.camera.set_exposure(0.9*self.camera.exposure)
				self.cameraControl.update_exposure_value()

		else:
			 self.camera.set_exposure(float(self.fixedExposure))
			 self.cameraControl.update_exposure_value()

		return self.camera.exposure

	def calculateAndSaveDampingConstant(self,field):
		"""saves the damping constant"""

		print('>> calculated and saved damping constant')

		# load the right entries from the database

		selection = 'img_meanPhys,moloLine'

		condition = "WHERE line=2 AND field='{}' AND chipNo='{}'".format(field,self.chipNo)

		damping_data = loadDB('{}/database/data.db'.format(self.path),'results',selection,condition,returnDataFrame = True)

		damping_data = damping_data.groupby('moloLine').mean() # if there was more than one damping measurement performed then only take the mean.
		img_meanPhys = damping_data['img_meanPhys']

		# make sure that img_meanPhys is ordered ascending.
		# maybe discard points that are higher than the following one. (biased selection though)

		figureName = self.chipNo + "_" + field

		delta, alpha = determineDampingConstant(img_meanPhys,plot = True,path = self.path,name=figureName,ax=self.ax)

		# save this to a new table in the database

		createDB('{}/database/data.db'.format(self.path), 'waveguideDamping', getWGDampResultsDict())

		values = {
			'chipNo':self.chipNo,
			'field':field,
			'dampingWGNpm':delta,
			'dampingWGdBcm':alpha
		}

		saveDB('{}/database/data.db'.format(self.path), 'waveguideDamping', values)


	# must be removed from here...
	def saveImage(self, img, imagePath):
		if self.saveImages == 'crop':
			if self.camera.crop_and_save_img(imagePath, img):
				print('Image cropped and saved: {}'.format(imagePath))

		elif self.saveImages == 'original':
			if self.camera.save_img(imagePath, img):
				print('Image saved: {}'.format(imagePath))


	def saveDatabase(self, table, values):
		saveDB('{}/database/data.db'.format(self.path), table, values)

	def stop(self):
		self.stop_thread = True

	def pause(self):
		self.pause_thread = not self.pause_thread


	def __del__(self):
		self.exiting = True
		self.wait()
