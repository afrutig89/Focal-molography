from PyQt4 import QtGui,QtCore, QtSql
import sqlite3
import numpy as np


def create_sql_database(column_names,database_path):
	""" Depending on the current algorithm chosen, create an approriate SQL database to store the results from an experiment """
	
	# Create SQL database

	
	
	db  = QtSql.QSqlDatabase.addDatabase('QSQLITE')
	db.setDatabaseName(database_path + '/mologram_data.db')

	if not db.open():
		QtGui.QMessageBox.critical(None, QtGui.qApp.tr("Cannot open database"),
		 QtGui.qApp.tr("Unable to establish a database connection.\n"
			"This issue requires SQLite support. Please read "
			"the Qt SQL driver documentation for information "
			"on how to fix it.\n\n" "Click Cancel to exit."),
		 QtGui.QMessageBox.Cancel)

	# create an array with all the rows necessary

	columns = []
	for i in column_names:

		columns.append(i)
	
	num_columns = len(columns)

	# Add columns to database
	query = QtSql.QSqlQuery()
	query.exec_("CREATE TABLE mologram_data(" + ','.join(columns) + ")")

	db.commit()
	db.close()

def add_entry(new_entry,database_path):

	print( 'New entry: ' + str(new_entry))

	con = sqlite3.connect(database_path + '/mologram_data.db')
	cur = con.cursor()
	cur.execute('INSERT INTO mologram_data VALUES (' + new_entry + ')')
	con.commit()
	con.close()
	



def get_database_content(database_path):

	# Connect to database
	con = sqlite3.connect(database_path + '/mologram_data.db')
	cur = con.cursor()

	# fetch all rows from database
	#os.('echo ".dump" | sqlite old.db | sqlite new.db')

	rows = []
	for row in cur.execute('SELECT * FROM mologram_data ORDER BY key'):
		rows.append(row)


	con.commit()
	# Close connection with SQL databse
	con.close()

	# extract data
	db_data = np.array(rows)

	return db_data

def get_db_column_names(database_path):
	
	con = sqlite3.connect(database_path + '/mologram_data.db')
	cur = con.execute('SELECT * FROM mologram_data')

	# get the number of columns and the field names
	num_fields = len(cur.description)
	field_names = [i[0] for i in cur.description]
	con.commit()
	con.close()

	return field_names

def addColumns(filename, dbname, columnName, values):
	sqlite3.register_adapter(np.ndarray, adapt_array)
	sqlite3.register_converter('array', convert_array)
	
	conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
	c = conn.cursor()
	rows = c.execute("select * from " + dbname)
	if rows > 1 and (type(values) == 'np.array' or type(values) == 'list'):
		# add values to each row
		c.execute("alter table {tn} add column '{cn}' {ct}"\
		.format(tn=dbname, cn=columnName, ct=returnType(values)))
		c.executemany('update ' + dbname + ' set ' + columnName + ' = ?'\
		,((val,) for val in values))
	
	else:
		c.execute("alter table {tn} add column '{cn}' {ct} DEFAULT '{df}'"\
		.format(tn=dbname, cn=columnName, ct=returnType(values), df=values))
	
	conn.commit()
	conn.close()

def deleteColumn(filename, dbname, columnName):
	""" deletes a column from an existing table
	"""
	# load database settings (type array defined)
	sqlite3.register_adapter(np.ndarray, adapt_array)
	sqlite3.register_converter('array', convert_array)
	
	conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
	c = conn.cursor()
	c.execute("select * from " + dbname)
	
	# save column names of the selected table in a list
	fieldnames=[f[0] for f in c.description]
	# remove the column that will be deleted
	fieldnames.remove(columnName)
	
	# generate dictionary (to define corresponding type. Dictionary "params" must be defined above.)
	new_dict = {}	
	for param in fieldnames:
		new_dict[param] = params[param]
	
	# rename table
	c.execute("alter table " + dbname + " rename to " + dbname + "_old")
	# create new table with the new columns
	c.execute('create table ' + dbname + str(new_dict).replace("'","").replace(":","").replace("{"," (").replace("}",")"))
	# copy values from the old table
	sql = "insert into " + dbname + " (" + str(fieldnames).replace("'","")[1:-1] + ")"
	sql += " select " + str(fieldnames).replace("'","")[1:-1]
	sql += " from " + dbname + "_old"
	c.execute(sql)
	
	# delete old table
	c.execute("drop table " + dbname + "_old")
	
	# execute sql command
	conn.commit()
	conn.close()


	

if __name__ == "__main__":

	datab = DataBase()

	column_names = [
		'key INTEGER',
		'time REAL',
		'zPos REAL',
		'molointensity REAL',
		'bkgmean REAL',
		'bkgstd REAL',
		'exposure REAL',
		'MIExpWeighted REAL'
		]

	datab.create_sql_database(column_names,'/home/focal-molography/Desktop')

