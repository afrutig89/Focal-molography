import numpy as np
import scipy as sp
import scipy.stats as ss
import os
import time
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))

from SharedLibraries.Database.dataBaseOperations import loadDB, createDB, saveDB

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib import colors
from scipy.signal import fftconvolve

statisticsDict = {
    'coherentParticles' : 'integer',
    'noncoherentParticles' : 'integer',
    'N_particles' : 'integer',
    'coherentBinding' : 'real',
    'mean' : 'real',
    'median' : 'real',
    'std' : 'real',
    'var' : 'real',
    'skewMean' : 'real',
    'kurtosis' : 'real',
    'SNR' : 'real',
    'maxPixel' : 'real',
    'minPixel' : 'real',
    'coeffOfVariation' : 'real',

    'meanSpectrum' : 'real',
    'stdSpectrum' : 'real',
    'maxSpectrum' : 'real',
    'minSpectrum' : 'real',
    'meanNormalized' : 'real',
    'stdNormalized' : 'real',
    'rangeSpectrum' : 'real',

    'expfit1' : 'real',
    'expfit2' : 'real',

    'moloIntensity' : 'real',
    'bkgMean' : 'real',
    'bkgStd' : 'real',
}

acfDict = {
    'coherentParticles' : 'integer',
    'noncoherentParticles' : 'integer',
    'N_particles' : 'integer',
    'coherentBinding' : 'real',

    'acf' : 'array',
}

def figureDB(result, log, folder='figures', show=False):

    I_sca = result['I_sca']
    N_particles = log['N_particles']
    coherentBinding = log['coherentBinding']
    coherentParticles = log['coherentParticles']
    noncoherentParticles = log['noncoherentParticles']

    maxPixel = np.max(I_sca)
    I_norm = I_sca/maxPixel

    figname = folder + '/N%dp%.2f.png' % (N_particles, coherentBinding)
    title = 'Coherent: ' + str(coherentParticles) + \
                '\nNoncoherent:' + str(noncoherentParticles) + '\n'


    # if figname not in os.listdir('.'):
    screen = defineScreen(log['screenWidth'], log['npix'], log['screenPlane'], log['screenRatio'], log['screenRotation'], log['focalPoint'], log['center'])

    fig = plt.figure(figsize=(12,12))

    plt.subplot(221)
    plt.imshow(I_sca, cmap = plt.cm.inferno, norm = colors.PowerNorm(gamma=1/2.))

    # histogram
    plt.subplot(222)
    # fit
    P = ss.expon.fit(I_norm)
    expfit1, expfit2 = P
    rX = np.linspace(0, 1, 1000)
    rP = ss.expon.pdf(rX, *P)
    try:
        plt.hist(I_norm, bins='auto')
    except: 
        print I_norm
    plt.plot(rX, rP, label='fit: %.3e, %.3e' % (P[0], P[1]))
    plt.plot()
    plt.legend()

    # plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

    # fourier transform (phase as well?)
    f = np.fft.fft2(I_sca)
    fshift = np.fft.fftshift(f)
    magnitude_spectrum = 20*np.log(np.abs(fshift))  
    plt.subplot(223)
    plt.imshow(magnitude_spectrum, cmap = 'gray')

    plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])

    plt.subplot(224)
    try:
        plt.hist(magnitude_spectrum, bins='auto', normed=True)
    except:
        print magnitude_spectrum
    plt.ylim([0,1])

    fig.tight_layout()

    fig.savefig(figname)

    if show: plt.show()

    plt.clf()
    plt.close('all')


def autoCorrelation(results, logs, dbFile='statistics.db'):
    createDB(dbFile, 'correlation', acfDict)

    for i in range(len(results)):
        N_particles = logs[i]['N_particles']
        coherentBinding = logs[i]['coherentBinding']
        coherentParticles = logs[i]['coherentParticles']
        noncoherentParticles = logs[i]['noncoherentParticles']

        # initial autocorrelation value
        acf_0 = np.sum(fftconvolve(results[i]['I_sca'],results[i]['I_sca']))

        acf = []
        for j in range(len(results)):
            # convolve intensities
            temp = fftconvolve(results[i]['I_sca'],results[j]['I_sca'])
            # calculate ACF value
            acf.append(acf_0/np.sum(temp))

        acf = np.array(acf)
        print '%d of %d' % (i, len(results))
        saveDB(dbFile, 'correlation', locals())


def plotAutoCorrelation(dbFile):
    results = loadDB(dbFile, 'correlation')
    N_particles = []
    N_particles_label = []
    N_particles_label.append(str(results[0]['N_particles']))
    for result in results:
        N_particles.append(result['N_particles'])
        if N_particles_label[-1] != str(result['N_particles']):
            N_particles_label.append(str(result['N_particles']))

    groups = formGroup(results, 'N_particles')

    colormap = ['black','red','green','blue','cyan','magenta','black','red','green','blue','cyan','magenta','black','red','green','blue','cyan','magenta']
    for group in groups:
        color = colormap.pop()
        for entry in group:
            if str(entry['N_particles']) in N_particles_label:
                plt.plot(N_particles, entry['acf'], color=color, label=N_particles_label.pop(N_particles_label.index(str(entry['N_particles']))))
            else:
                plt.plot(N_particles, entry['acf'], color=color)

    plt.legend()
    plt.ylim([0,2.5])
    plt.show()


def statisticsDB(result, log, dbFile='statistics.db'):
    I_sca = result['I_sca']
    N_particles = log['N_particles']
    coherentBinding = log['coherentBinding']
    coherentParticles = log['coherentParticles']
    noncoherentParticles = log['noncoherentParticles']

    median = sp.median(I_sca)


    # maxPixel = np.max(I_sca)
    # I_norm = I_sca/maxPixel

    # # fourier transform (phase as well?)
    # f = np.fft.fft2(I_sca)
    # fshift = np.fft.fftshift(f)
    # magnitude_spectrum = 20*np.log(np.abs(fshift))  

    # meanSpectrum = np.mean(magnitude_spectrum)
    # stdSpectrum = np.std(magnitude_spectrum)
    # maxSpectrum = np.max(magnitude_spectrum)
    # minSpectrum = np.min(magnitude_spectrum)
    # rangeSpectrum = maxSpectrum-minSpectrum

    # moloIntensity, bkgMean, bkgStd = calc_IntAiryDisk(I_sca)
    # SNR = moloIntensity / (bkgMean+3*bkgStd)

    if dbFile not in os.listdir('.'):
        createDB(dbFile, 'statistics', statisticsDict)

    saveDB(dbFile, 'statistics', locals())


def formGroup(dictionaries, *args):
    """
    Groups dictionaries according to the arguments given.
    """
    groups = []
    groups.append([dictionaries[0]])

    for i in range(len(dictionaries)):
        added = False
        for group in groups:
            for arg in args:
                if arg not in dir(group[0]) or \
                        arg not in dir(dictionaries[i]):
                    # check if argument is in dictionaries
                    continue
            if (sum([group[0][arg] == dictionaries[i][arg] for arg in args]) == len(args)):
                group.append(dictionaries[i])
                added = True
                break
        if not added:
            groups.append([dictionaries[i]])

    return groups


def plotDatabase2D(database, table, xAxis, yAxis, zAxis):
    """
    Plots all the parameters in a database as a function of the 
    parameter 'xAxis'

    :param database: filename of database
    :param table: name of the table
    :param xAxis: variablename of the axis
    :param yAxis: variablename of the axis
    :param zAxis: variablename of the axis
    """

    # get Axes
    xAxisList = []
    yAxisList = []

    # load entries (list of dictionaries)
    entries = loadDB(database, table)

    # go through entries to find the values of the axes
    for entry in entries:
        # x-axis
        if entry[xAxis] not in xAxisList:
            xAxisList.append(entry[xAxis])
        # y-axis
        if entry[yAxis] not in yAxisList:
            yAxisList.append(entry[yAxis])

    # sort in order to plot afterwards
    xAxisList.sort()
    yAxisList.sort()

    # make meshgrid of the two axes
    X, Y = np.meshgrid(xAxisList, yAxisList)

    # generate z-axis
    Z = np.zeros(X.shape)

    # go through each entry again to generate z at specific positions
    for entry in entries:
        Z[yAxisList.index(entry[yAxis]),xAxisList.index(entry[xAxis])] = entry[zAxis]

    # plot 3d figure
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Plot the surface.
    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                           linewidth=0, antialiased=False)

    # scientific ticklabel
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.ticklabel_format(style='sci', axis='z', scilimits=(0,0))

    # add label
    ax.set_xlabel(xAxis)
    ax.set_ylabel(yAxis)
    ax.set_zlabel(zAxis)

    # show plot
    plt.show()


def plotDatabase(database, table, xAxis, *args):
    """
    Plots all the parameters in a database as a function of the 
    parameter 'xAxis'

    :param database: filename of database
    :param table: name of the table
    :param xAxis: variablename of the axis
    :param args: variablenames to group

    .. example::

        plotDatabase('statistics.db', 'statistics', 
                     'coherentBinding', 'N_particles')
    
    """

    # load entries
    dictionaries = loadDB(database, table)
    groups = formGroup(dictionaries, * args)

    dictByVariables = dict()

    for i in range(len(groups)):
        dictByVariables2 = dict()
        for dictionary in groups[i]:
            for key, value in dictionary.items():
                if key not in dictByVariables2.keys():
                    dictByVariables2[key] = []
                dictByVariables2[key].append(value)

        for key in dictByVariables2.keys():
            if key not in dictByVariables:
                dictByVariables[key] = []
            dictByVariables[key].append(dictByVariables2[key])

    for key, value in dictByVariables.items():
        if key == xAxis: continue
        fig = plt.figure()
        for i in range(len(dictByVariables[key])):
            try:
                plt.plot(dictByVariables[xAxis][i], 
                    dictByVariables[key][i], 
                    label= [arg + ': ' + str(dictByVariables[arg][i][0]) for arg in args])
            except: continue

        plt.xlabel(xAxis)
        plt.ylabel(key)
        plt.title(key + ' vs. ' + xAxis)
        lgd = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.tight_layout()
        if not os.path.exists('figures2/'):
            # create folder figures if not exist
            os.makedirs('figures2/')
        fig.savefig('figures2/' + xAxis + '_' + key + '.png', bbox_inches='tight')


if __name__ == '__main__':

    plotDatabase2D('IdealThickness.db', 'Data', 'n_wg', 'wavelength', 'idealThickness_neffInflection')
    # databaseFile = 'backgroundAnalysis.db'

    # logs = loadDB(databaseFile, 'log')
    # results = loadDB(databaseFile, 'results')

    # autoCorrelation(results, logs, databaseFile)

    # plotAutoCorrelation(databaseFile)

    # createDB('statisticsSmall.db', 'statistics', statisticsDict)

    # i = 0
    # timestart = time.time()
    # for i in range(len(logs)):
    #     expectedTime = ((time.time()-timestart)*(len(logs)-i)/(i+1))

    #     if i == 0:
    #         print 'file ' + str(i+1) + '/' + str(len(logs))
    #     else:
    #         print 'file ' + str(i+1) + '/' + str(len(logs)) \
    #                 + '. ETA: ' + str(expectedTime/60.) + ' min'

    #     # statisticsDB(results[i], logs[i], 'statisticsSmall.db')
    #     if results[i]['I_sca'].all() == 0: continue
    #     figureDB(results[i], logs[i])

    # plotDatabase('statisticsSmall.db', 'statistics', 'coherentBinding', 'N_particles')