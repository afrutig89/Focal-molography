
import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from SharedLibraries.Database.dataBaseOperations import saveDB, createDB, loadDB, createSQLDict

databaseFile = 'settings.db'

os.remove(databaseFile) if os.path.exists(databaseFile) else None

shortcutsDict = {
    'Exit application' : 'Ctrl+Q',
    'Calibrate' : 'Ctrl+K',
    'Open Image...' : 'Ctrl+L',
    'Save As...' : 'Shift+Ctrl+S',
    'Show Documentation' : 'Ctrl+H',
    'Show Protocols' : '',
    'Reload Tabs' : 'Ctrl+F5',

    'Quicksave' : 'Space',
    'Update Image' : 'F5',

    'X neg' : 'Left',
    'X pos' : 'Right',
    'Y neg' : 'Up',
    'Y pos' : 'Down',
    'Z neg' : 'PgUp',
    'Z pos' : 'PgDown',

    'TE neg' : 'Ctrl+Left',
    'TE pos' : 'Ctrl+Right',
    'MS neg' : 'Shift+Left',
    'MS pos' : 'Shift+Right',
}

shortcutsTableName = 'Shortcuts'

shortcutsSQLDict = createSQLDict(shortcutsDict)
createDB(databaseFile, shortcutsTableName, shortcutsSQLDict)
saveDB(databaseFile, shortcutsTableName, shortcutsDict)

imageSettingsDict = {
    'Image Naming' : '{field}-{line}-{moloLine}_exp-{exposureTime}-ms',
    'Save Scalebar' : False,
    'Save Intensity' : False,

}


imageSettingsTableName = 'Image Settings'

imageSettingsSQLDict = createSQLDict(imageSettingsDict)
createDB(databaseFile, imageSettingsTableName, imageSettingsSQLDict)
saveDB(databaseFile, imageSettingsTableName, imageSettingsDict)


plotdatabaseDict = {
    'Form groups by' : 'Experiment ID',
    'Change line type for' : '',
    'Show Legend' : True
}


plotdatabaseDictTableName = 'Database Plot'

plotdatabaseDictSQLDict = createSQLDict(plotdatabaseDict)
createDB(databaseFile, plotdatabaseDictTableName, plotdatabaseDictSQLDict)
saveDB(databaseFile, plotdatabaseDictTableName, plotdatabaseDict)

cameraDict = {
    'model' : 'UI-3480CP-M-GL Rev.2',
    'pixelsize' : 2.2,    # in um
    'npix' : [2560, 1920],
    'colorDepth' : 2**12-1,   # 12 bits
    'pixelClockRange' : (4, 96), # in MHz
    'minExposure' : 0.031,
    'maxExposure' : 2745,
    'gain_max' : 100, # Gain settings 0...49 use analog signal gain; from 50 up, the stronger digital gain is used. High gain settings may cause visible noise
    'quantumEfficiency' : 0.48, # quantum efficiency at 632.8 nm
    'pixelGain' : 0.3826, # 5.272 # measurements
}


cameraDictTableName = 'Cameras'

cameraDictSQLDict = createSQLDict(cameraDict)
createDB(databaseFile, cameraDictTableName, cameraDictSQLDict)
saveDB(databaseFile, cameraDictTableName, cameraDict)

