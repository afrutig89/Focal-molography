from distutils.core import setup

setup(name='focalmolography',
      version='1.0',
      author='Andreas Frutiger, Silvio Bischof, Yves Blickenstorfer',
      author_email='afrutig89@gmail.com',
      install_requires=[
        "numpy", "matplotlib"
      ],
      py_modules=['Moloreader','SharedLibraries','SimulationFrameWork','Snippets','Zeptoreader'],
      )
