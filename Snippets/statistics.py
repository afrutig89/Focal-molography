import numpy as np
import scipy.special


def calcUpperLowerBound(vector,confidence):
    """Calculates the confidence interval of a given vector"""

    vector = np.sort(vector)

    index = int(len(vector)*(1-confidence)/2.+ 0.001)
    print len(vector)
    print len(vector)*(1-confidence)/2.

    if index == 0:
        raise Exception, 'This confidence level is not possible with the provided sample size.'

    index_lb = index-1
    index_ub = -index
    lower_bound = vector[index_lb]
    upper_bound = vector[index_ub]

    return lower_bound,upper_bound

def confidenceforMedian(N,m,vector = ''):
    """estimates the confidence of the median based on a total number of measurements N and an index m for the interval

    .. math:: P\left\{ {{x_m} \leqslant M \leqslant {x_{N - m + 1}}} \right\} = 1 - 2\sum\limits_{k = 0}^{m - 1} {\left( {\begin{array}{*{20}{c}} N \\ k  \end{array}} \right)\frac{1}{{{2^N}}}} \]"""

    assert m >= 1
    assert m < N/2.
    hilf = 0
    for i in range(m):
        hilf += 2*scipy.special.binom(N, i)*1./(2**N)

    if vector == '':
        return 1. - hilf
    else:
        vector = np.sort(vector)
        return 1. - hilf,vector[m - 1],vector[N - m]

def confidence95Median(vector):

    for i in range(len(vector)/2):

        i = i + 1

        conf, low, up = confidenceforMedian(len(vector),i,vector)

        if conf < 0.95:

            conf, low, up = confidenceforMedian(len(vector),i-1,vector)

            break


    return low,up


def confidenceforAnyQuantile(p,N,m,vector = ''):
    # I have not yet understood this formula. 

    assert m >= 1
    assert m < N/2.
    hilf = 0
    for i in range(m):
        hilf += scipy.special.binom(N, i)*p**i*(1-p)**(N-i)

    if vector == '':
        return 1. - hilf
    else:
        vector = np.sort(vector)
        return 1. - hilf,vector[m - 1],vector[N - m]

def minimalSampleSizeForGivenConfidenceLevel(confidence,p):

    """

    :param confidence: confidence level desired (50 % is 0.5)
    :param p: quantile (75 % quantile is 0.75)
    
    """


    return np.ceil(np.log(1 - confidence)/np.log(1 - p))

print minimalSampleSizeForGivenConfidenceLevel(0.75,0.5)

# confidence_interval for 99% percentile. 

