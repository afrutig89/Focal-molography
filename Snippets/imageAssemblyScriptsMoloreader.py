# -*- coding: utf-8 -*-
"""
Created on Fri Aug 05 14:57:51 2016

@author: Andreas
"""
import sys
import sys,os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))))

import matplotlib.pyplot as plt
from matplotlib import colors

import pandas as pd
from PIL import Image
import numpy as np
import matplotlib as mpl
import os
from SimulationFramework.auxiliaries.Journal_formats_lib import formatPRX, removeWhiteSpace
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.patches import Circle
from imageImport import ImportAllTiffs


def generateAndSaveFocalPlaneImageOfEveryImageInFolder(path, gamma=1.):

    names, images = ImportAllTiffs(path + '/')

    for i,image in enumerate(images):

        plotFocalPlaneImage(image,names[i],Noaxis=True,OnlyImage=True,gamma=gamma)




def makeImageMosaic(self):
    """Makes an image mosaic of all the focal plane images of the chip."""
    raise NotImplementedError

def plotFocalPlaneImage(image,Name=None,size=None,pixelsize=0.110,Noaxis=False,OnlyImage=False,gamma=1,path=None,cbarlabel=''):
    """
    
    All size and pixelsize is in um. 

    :param : Noaxis, if true then the axis are off
    """

    fig = plt.figure()

    if not os.path.exists('plots'):
        os.mkdir('plots')

    # plot the intensity at the molospot
    # get the maximum pixel of the image a

    if size == None:

        size = np.array(image.shape)*pixelsize
        img = image
    else:
        i,j = np.unravel_index(image.argmax(),image.shape)
        print((i-round(size[0]/2./pixelsize)))
        img = image[(i-int(size[0]/2./pixelsize)):(i+int(size[0]/2./pixelsize)),(j-int(size[1]/2./pixelsize)):(j+int(size[1]/2./pixelsize))]

    extent = (-img.shape[1]/2.0*pixelsize,img.shape[1]/2.0*pixelsize,-img.shape[0]/2.0*pixelsize,img.shape[0]/2.0*pixelsize)

    
    if OnlyImage:

        fig = removeWhiteSpace(fig)

    axes = plt.imshow(img,
                    cmap = plt.cm.inferno,
                    origin = 'upper',
                    extent = [border for border in extent],
                    norm = colors.PowerNorm(gamma=gamma))

    if not Noaxis:
        plt.xlim([-size[1]/2.,size[1]/2.])
        plt.ylim([-size[0]/2.,size[0]/2.])
        plt.xlabel(r'x [$\mathrm{\mu m}$]')
        plt.ylabel(r'y [$\mathrm{\mu m}$]')

    else:
        plt.gca().set_axis_off()
        plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, 
            hspace = 0, wspace = 0)
        plt.margins(0,0)
        plt.gca().xaxis.set_major_locator(plt.NullLocator())
        plt.gca().yaxis.set_major_locator(plt.NullLocator())
    
    if not cbarlabel == '':

        plt.gca().set_aspect('equal')
        ax = plt.gca()
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="5%", pad=0.1) 
        cbar = plt.colorbar(axes, cax = cax1, ticks=np.linspace(0,np.max(img),4), format='%.0e')
        cbar.ax.tick_params()
        cbar.set_label(cbarlabel)
    
    if path == None:
        plt.savefig('plots/focal plane image' + str(Name) + '.png', format='png', dpi=600)
    else:
        plt.savefig(path + '.png', format='png', dpi=600)

def plotSurfaceImage(image,Name=None,path=None,size = None,pixelsize=0.110,Noaxis= False,vmin=0,vmax=4095,highlightMaximum = False,gamma = 0.25):

    fig = plt.figure()

    if not os.path.exists('plots'):
        os.mkdir('plots')

    # plot the entire image, if no size is specified.

    if size == None:

        size = np.array(image.shape)*pixelsize
        
        img = image

    else:
        i = int(image.shape[0]/2.)
        j = int(image.shape[1]/2.)
        img = image[(i-int(size[0]/2./pixelsize)):(i+int(size[0]/2./pixelsize)),(j-int(size[1]/2./pixelsize)):(j+int(size[1]/2./pixelsize))]

    extent = (-img.shape[1]/2.0*pixelsize,img.shape[1]/2.0*pixelsize,-img.shape[0]/2.0*pixelsize,img.shape[0]/2.0*pixelsize)


    
    #plt.gca().set_aspect('equal')

    if Noaxis:

        fig = removeWhiteSpace(fig)

    axes = plt.imshow(img,
                    cmap = plt.cm.inferno,
                    origin = 'upper',
                    extent = [border for border in extent],
                    norm = colors.PowerNorm(gamma=gamma),vmin=vmin,vmax=vmax)

    

    plt.xlim([-size[1]/2.,size[1]/2.])
    plt.ylim([-size[0]/2.,size[0]/2.])

    if highlightMaximum:

        i,j = np.unravel_index(img.argmax(),img.shape)

        j = j*pixelsize - img.shape[1]/2.0*pixelsize
        i = i*pixelsize - img.shape[0]/2.0*pixelsize

        circ = Circle((j, -i), 20*pixelsize, color='w',fill=False,lw=0.1)
        ax = plt.gca()
        ax.add_patch(circ)

    if Noaxis == False:
        

        ax = plt.gca()
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="5%", pad=0.1) 
        cbar = plt.colorbar(axes, cax = cax1, ticks=np.linspace(0,np.max(img),4), format='%.0e')
        cbar.ax.tick_params(labelsize=10)


    if path == None:

        plt.savefig('plots/surface image' + str(Name) + '.png', format='png', dpi=600)
    else:
        plt.savefig(path + '.png', format='png', dpi=600)

def makeZStack(prepath,ExperimentID,data,Name):
    """makes a Z Stack an array of experimental data images and returns the 
    focal plane image in xy as a numpy array. """

    if not os.path.exists('plots'):
        os.mkdir('plots')

    pixelWidth = 0.110

    z_screen = data.ix[data['experimentID']==ExperimentID]['z_position']
    image_names = data.ix[data['experimentID']==ExperimentID]['imagePath']

    images = []
    for i in image_names:

        # get the relative path
        path = prepath + i.split('/')[-2] + '/' + i.split('/')[-1]

        im = Image.open(path)

        images.append(np.asarray(im))

    # find cross section
    maximum = 0
    i = 0
    for im in images:
        imarray = np.array(im)
        if np.max(imarray) > maximum:
            maximum = np.max(imarray)
            line_ymax = int(imarray.argmax()/imarray.shape[1])+1 # y-axis with the highest values
            line_xmax = imarray[line_ymax].argmax() # x-axis with the highest value
            focalPlaneIndex = i # focal plane
        
        i += 1

    # create z-stack
    I_meas_xz = np.zeros((len(images),imarray.shape[1]))
    
    row = 0
    for im in images:
        imarray = np.array(im)
        I_meas_xz[row] = imarray[line_ymax]
        row += 1

    # take account for multiple maxima (saturation)
    focalPlaneIndex = focalPlaneIndex + int(sum(I_meas_xz[:,line_xmax] == np.max(I_meas_xz[:,line_xmax]))/2)
    
    # create xy-plots
    focalPlaneImage = np.array(images[focalPlaneIndex])
        
    # adjust line_ymax
    line_ymax = focalPlaneImage[:,line_xmax].argmax()
    
    # define screen
    x_screen = pixelWidth*(np.arange(imarray.shape[1])-line_xmax)
    y_screen = pixelWidth*(np.arange(imarray.shape[0])-line_ymax)
    #z_screen = z_screen - z_screen[focalImage]
    
    formatPRX(3,3/2.)
    fig1 = plt.figure()
    CS = plt.contourf(x_screen, z_screen, I_meas_xz, 1000,
                    cmap=plt.cm.inferno,
                    origin = 'upper',
                    norm = colors.PowerNorm(gamma=1./2.))
    cb = plt.colorbar(CS, format='%.e',aspect=50)
    cb.ax.tick_params(labelsize=6)
    plt.xlim(-10,10)
    
    #plt.axes().set_aspect('equal', 'datalim')
    plt.gca().set_aspect('equal', adjustable='box')
    plt.ylim(-30,30)
    plt.savefig('plots/z Stack' + str(Name) + '.png')

    return focalPlaneImage

def makeSNRplot(ExperimentID,data,Name):
    
    formatPRX(3,3)

    SNR = data.ix[data['experimentID']==ExperimentID]['SNR']
    z_screen = data.ix[data['experimentID']==ExperimentID]['z_position']

    plt.figure()
    plt.plot(z_screen,SNR)
    plt.ylabel('SNR')
    plt.xlabel('z distance [um]')

    plt.savefig('plots/SNR' + str(Name) + '.png')


def test_generateAndSaveFocalPlaneEveryImageInFolder():

    generateAndSaveFocalPlaneImageOfEveryImageInFolder(os.path.dirname(os.path.abspath(__file__)),gamma=0.25)

if __name__ == '__main__':

    test_generateAndSaveFocalPlaneEveryImageInFolder()
    

# if not os.path.exists('plots'):
#     os.mkdir('plots')

# data = pd.read_csv(prepath + 'Results_Table_Database.csv')
# Chip = 'C008'
# Experiments = [2,20,23,25,27,29,31]
# Names = ['B-1-1','D-1-3','D-1-1','D-1-5','D-3-5','D-3-3','D-3-1']
# # Experiments = [20]
# # Names = ['D-1-3']

# for j,i in enumerate(Experiments):
#     makeZStack(i,data,Chip + Names[j])
#     makeSNRplot(i,data,Chip + Names[j])


# extract the 
