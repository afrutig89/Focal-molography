# -*- coding: utf-8 -*-

import numpy as np
import sys,os


path = '/Users/andreasfrutiger/Daten/ds_Admin_Andreas/Studium/ETH/PhD/Projects/Focal Molography/Simulation tool Focal Molography/Focal-molography/Moloreader'
sys.path.append(path)
path = '/Users/andreasfrutiger/Daten/ds_Admin_Andreas/Studium/ETH/PhD/Projects/Focal Molography/Simulation tool Focal Molography/Focal-molography/SimulationFramework'
sys.path.append(path)

path = '/Users/andreasfrutiger/Daten/ds_Admin_Andreas/Studium/ETH/PhD/Projects/Focal Molography/Simulation tool Focal Molography/Focal-molography/Snippets'
sys.path.append(path)

import matplotlib.pylab as plt
from auxiliaries.Journal_formats_lib import formatPRX, removeWhiteSpace
from auxiliaries.writeLogFile import loadDB
from auxiliaries import constants

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mologram.mologram import *
from Curvefitting import *
from camerasimulation.CameraSensorModels import ReturnCameraSensorParameters
from settings import FieldConfiguration
from analytical.focalMolography import FocalMolographyAnalytical
from analytical.focalMolography_CMT import FocalMolographyAnalyticalCMT
from binder.Adlayer import MologramProteinAdlayer, ProteinAdlayer
from waveguide.waveguide import SlabWaveguide
from mologram.mologram import Mologram
from waveguidebackground.background import backgroundScatteringWaveguide
from Data_Processing.MologramProcessing import calc_IntAiryDisk
from imageImport import ImportAllTiffs


from imageAssemblyScriptsMoloreader import plotFocalPlaneImage, plotSurfaceImage
from auxiliariesMoloreader.intensityConversionDamping import powerAtMoloLocation,convertPhysicalPostProcess
from scipy import special
from scipy.signal import fftconvolve

################################################################################################
################################################################################################
# This script allows the postprocessing of the intensity from molographic measurements

def filterRadiusAiry(img, radiusAiry, camera, magnification, plot=False):
    """filters the image with a kernel of the shape of the Airy function whereas the location of the first zero is at
    radius Airy

    :param img: img is a 2d-numpy array
    :param radiusAiry: float, in [m], radius of the Airy disk
    :param camera: camera object
    :param magnification: magnification of the imaging system.

    :returns: 2d numpy array of the convoluted image.
    """
    radiusAiryPixels = radiusAiry*magnification/(camera.pixelsize)
    # Transform the Airy function such that the first zero is at radiusAiryPixels.
    AiryFunction = lambda x: (special.jn(1,(x*3.83/radiusAiryPixels))/(x*3.83/radiusAiryPixels))**2

    kernel = np.outer(AiryFunction(np.linspace(-50,50,100)),AiryFunction(np.linspace(-50,50,100)))

    kernel = kernel/np.sum(kernel) # normalization of the kernel. (Is this correct? If I normalize I calculate the average intensity in the airy disk with the convolution)

    # plt.figure()
    # plt.imshow(kernel)
    # plt.show()

    #plot the Airy function
    if plot: 
        formatPRX(8,8)
        fig = plt.figure()
        x = np.linspace(-20,20,100)
        plt.plot(x,AiryFunction(x))
        plt.gca().set_axis_off()
        plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, 
            hspace = 0, wspace = 0)
        plt.margins(0,0)
        plt.xlim([-20,20])
        plt.ylim([-0.01,0.255])
        plt.gca().xaxis.set_major_locator(plt.NullLocator())
        plt.gca().yaxis.set_major_locator(plt.NullLocator())
        fig = removeWhiteSpace(fig)
        #plt.savefig('02plots/Filter_Kernel.png',format='png',dpi=600)
        plt.show()


    img_filtered = fftconvolve(img,kernel,mode='same')

    return img_filtered

def calc_normalized_intensity(damping,diameter,chip='',diagnostics=False,path=''):
    """
    :param damping: damping in dB/cm of the chip
    :param diameter: diameter of the mologram, for simplicity all locations in field are treated equally. Attention!!!!
    
    Calculates the convoluted intensity normalized to a reference power of 0.02 W/m in the waveguide.
    """

    ################################################################################################
    ################################################################################################
    # Definitions

    magnification = 21.78
    camera = ReturnCameraSensorParameters('MT9P031')
    pixelsize =  camera.pixelsize/magnification

    moloField = FieldConfiguration()
    moloField.convertSI()

    # hardcoded size for these molograms
    beamWaist = 1e-3

    ################################################################################################
    ################################################################################################
    # Data Import and unpacking

    dataMolograms = loadDB(path + 'database/data.db','results',returnDataFrame = True)


    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                                      n_c=1., d_f=145e-9, polarization='TE',
                                      inputPower=0.02, wavelength=632.8e-9, attenuationConstant=damping*np.log(10)/10*100)

    field = dataMolograms['field']
    moloLine = dataMolograms['moloLine']
    lines = dataMolograms['line']


    OutcoupledPower = dataMolograms['PowerOutcoupling']
    print OutcoupledPower
    exposureTime = np.asarray(dataMolograms['exposureTime'])

    files =  os.listdir(path + 'images/')
    print files
    ImageNamesImp, imagesImp = ImportAllTiffs(path + 'images/')
    imagesImp = np.asarray(imagesImp)

    print ImageNamesImp

    # reorder the images to match the dataMologram datastructure

    ImageNamesImportedHelp = []

    for j, imageNameImp in enumerate(ImageNamesImp):

        ImageNamesImportedHelp.append(imageNameImp.split('_mologram')[0])

    print 'ImageNamesImp'
    print ImageNamesImportedHelp

    ################################################################################################
    ################################################################################################
    # Preprocessing scripts

    ################################################################################################
    # generate and save image name

    imageNames = []
    for i in dataMolograms.index:
        imageNames.append(str(dataMolograms.ix[i,'chipNo']) + "-" + str(dataMolograms.ix[i,'field']) + "-" + str(dataMolograms.ix[i,'line']) + "-" + str(dataMolograms.ix[i,'moloLine']))

    dataMolograms['imageNames'] = imageNames
    print imageNames


    ################################################################################################
    ################################################################################################
    # get the power at the mologram locations

    refPower = 0.02 # reference Power is 0.02 W/m in the waveguide.
    powerAtMoloLocations = []

    # I would need to average the outcoupled power over the first five molograms.
    for i,line in enumerate(lines):
        powerAtMoloLocations.append(powerAtMoloLocation(moloField.molograms[line-1][moloLine[i]-1].distanceToOutcoupling,OutcoupledPower[i],damping*np.log(10)/10*100,beamWaist=beamWaist))
    dataMolograms['powerAtMoloLocations'] = powerAtMoloLocations

    ImageNamesImportedHelp = np.asarray(ImageNamesImportedHelp)

    images = []
    # all molograms in this analysis have the same size

    # calculate the Airy disk diameter for the filtering. 
    mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=diameter,braggOffset = 25e-6)
    radiusAiry = mologram.calcAiryDiskDiameterAnalytical()/2

    ################################################################################################
    ################################################################################################
    # filter images with Airy disk 
    for i,imageName in enumerate(imageNames):
        print imageName
        print ImageNamesImportedHelp
        print ImageNamesImportedHelp == imageName
        #print imagesImp
        print imagesImp[ImageNamesImportedHelp == imageName][0].shape
        img = filterRadiusAiry(imagesImp[ImageNamesImportedHelp == imageName][0][:,:],radiusAiry, camera, magnification)

        images.append(img)

    ################################################################################################
    ################################################################################################
    # convert images to physical units

    molo_signal_phys_airy_filtered = []

    for i,img in enumerate(images):

        molo_signal_phys_airy_filtered.append(np.max(convertPhysicalPostProcess(img, camera, magnification, exposureTime[i])*refPower/powerAtMoloLocations[i]))

        if diagnostics:
            
            if not os.path.exists('control_processed_maxima'):
                os.makedirs('control_processed_maxima')
            fig = plt.figure()
            conv_img = convertPhysicalPostProcess(img, camera, magnification, exposureTime[i])*refPower/powerAtMoloLocations[i]
            plt.imshow(conv_img)
            ind = np.unravel_index(np.argmax(conv_img, axis=None), conv_img.shape)
            circle1 = plt.Circle((ind[1],ind[0]), 10, color='w', fill=False)
            plt.gca().add_artist(circle1)


            plt.savefig('control_processed_maxima/' + str(i) + '.png')
            plt.close(fig)

        print len(molo_signal_phys_airy_filtered)

    dataMolograms['molo_signal_phys_airy_filtered'] = molo_signal_phys_airy_filtered
    dataMolograms.to_csv(path + 'database/' + chip + 'intensity.csv',sep=';')

    plt.plot(molo_signal_phys_airy_filtered)
    plt.show()


def quantify_coherent_mass_density_air(damping, diameter,chip='',path=''):
    """
    :param damping: damping in dB/cm of the chip
    :param diameter: diameter of the mologram, for simplicity all locations in field are treated equally. Attention!!!!
    
    not the mass density on the molographic footprint, but only on the region of the grooves and the ridges. 
    """

    ################################################################################################
    ################################################################################################
    # Definitions

    magnification = 21.78
    camera = ReturnCameraSensorParameters('MT9P031')
    pixelsize =  camera.pixelsize/magnification

    moloField = FieldConfiguration()
    moloField.convertSI()

    # hardcoded size for these molograms
    beamWaist = 1e-3

    ################################################################################################
    ################################################################################################
    # Data Import and unpacking

    dataMolograms = loadDB(path + 'database/data.db','results',returnDataFrame = True)


    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                                      n_c=1., d_f=145e-9, polarization='TE',
                                      inputPower=0.02, wavelength=632.8e-9, attenuationConstant=damping*np.log(10)/10*100)

    field = dataMolograms['field']
    moloLine = dataMolograms['moloLine']
    lines = dataMolograms['line']


    OutcoupledPower = dataMolograms['PowerOutcoupling']
    print OutcoupledPower
    exposureTime = np.asarray(dataMolograms['exposureTime'])

    files =  os.listdir(path + 'images/')
    print files
    ImageNamesImp, imagesImp = ImportAllTiffs(path + 'images/')
    imagesImp = np.asarray(imagesImp)

    print ImageNamesImp

    # reorder the images to match the dataMologram datastructure

    ImageNamesImportedHelp = []

    for j, imageNameImp in enumerate(ImageNamesImp):

        ImageNamesImportedHelp.append(imageNameImp.split('_mologram')[0])

    print 'ImageNamesImp'
    print ImageNamesImportedHelp

    ################################################################################################
    ################################################################################################
    # Preprocessing scripts

    ################################################################################################
    # generate and save image name

    imageNames = []
    for i in dataMolograms.index:
        imageNames.append(str(dataMolograms.ix[i,'chipNo']) + "-" + str(dataMolograms.ix[i,'field']) + "-" + str(dataMolograms.ix[i,'line']) + "-" + str(dataMolograms.ix[i,'moloLine']))

    dataMolograms['imageNames'] = imageNames
    print imageNames


    ################################################################################################
    ################################################################################################
    # get the power at the mologram locations

    powerAtMoloLocations = []

    # I would need to average the outcoupled power over the first five molograms.
    for i,line in enumerate(lines):
        powerAtMoloLocations.append(powerAtMoloLocation(moloField.molograms[line-1][moloLine[i]-1].distanceToOutcoupling,OutcoupledPower[i],damping*np.log(10)/10*100,beamWaist=beamWaist))
    dataMolograms['powerAtMoloLocations'] = powerAtMoloLocations

    ImageNamesImportedHelp = np.asarray(ImageNamesImportedHelp)

    images = []
    # all molograms in this analysis have the same size

    # calculate the Airy disk diameter for the filtering. 
    mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=diameter,braggOffset = 25e-6)
    radiusAiry = mologram.calcAiryDiskDiameterAnalytical()/2

    ################################################################################################
    ################################################################################################
    # filter images with Airy disk 
    for i,imageName in enumerate(imageNames):
        print imageName
        print ImageNamesImportedHelp
        print ImageNamesImportedHelp == imageName
        #print imagesImp
        print imagesImp[ImageNamesImportedHelp == imageName][0].shape
        img = filterRadiusAiry(imagesImp[ImageNamesImportedHelp == imageName][0][:,:],radiusAiry, camera, magnification)

        images.append(img)

    ################################################################################################
    ################################################################################################
    # convert images to physical units

    physImages = []

    for i,img in enumerate(images):

        physImages.append(convertPhysicalPostProcess(img, camera, magnification, exposureTime[i]))

        print len(physImages)


    ################################################################################################
    ################################################################################################
    # mass on the mologram calculated via standard convolution algorithm. 


    coherent_surface_mass_densities = []
    convoluted_intensity_in_Airy_disk = []

    for i,img in enumerate(physImages):

        # the maximum in the processed image corresponds to the molographic signal.
        # set up the waveguide with the actual power at the mololocation

        waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                                      n_c=1., d_f=145e-9, polarization='TE',
                                      inputPower=powerAtMoloLocations[i], wavelength=632.8e-9, attenuationConstant=damping*np.log(10)/10*100)

        mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=diameter,braggOffset = 25e-6)
        proteinLayer = MologramProteinAdlayer(M_protein=52.8e3, eta=1.) # analyte efficiency is one, since I am interested in the coherent mass density.
        focalMoloAnalytical = FocalMolographyAnalytical(mologram,proteinLayer)
        coherent_surface_mass_densities.append(focalMoloAnalytical.mass_density_on_mologram_from_average_intensity(np.max(img)/2.012)) # I do not correct anymore for the difference in intensity of the numerical simulation vs the experimental result. 
        # factor 1.33 because this is the deviation of the analytical model with the simulation for Air. Factor 2.012 because of the convolution with the Airy disk and not taking the average intensity in the Airy disk
    	
    	convoluted_intensity_in_Airy_disk.append(np.max(img)/2.012)

    dataMolograms['convoluted_intensity_in_Airy_disk'] = convoluted_intensity_in_Airy_disk
    dataMolograms['coherent_surface_mass_densities'] = coherent_surface_mass_densities
    dataMolograms.to_csv(path + 'database/' + chip + 'mass_density.csv',sep=';')

    plt.plot(coherent_surface_mass_densities)
    plt.show()

if __name__ == '__main__':
    
    quantify_coherent_mass_density_air(damping=3.87 , diameter=400e-6) # damping is in dB/cm. 









 