import os
import numpy as np 
from PIL import Image
from matplotlib import colors

def ImportAllTiffs(path,condition = ''):
    """Imports all tifs in a folder and returns their names as a list of strings as well as the data as a list of numpy arrays.
    16 bit images are read correctly. The image library can just not write 16 bit images correctly. 
    """
    
    files =  os.listdir(path)

    imageNames = []
    
    for x in files:
        if x[-3:] == 'tif' or x[-4:] == 'tiff' and condition in x:
            imageNames.append(x)

    imgs = []

    for m in imageNames:

        im_name = m
        im = Image.open(path + im_name)

        imgs.append(np.asarray(im))

    return imageNames,imgs
