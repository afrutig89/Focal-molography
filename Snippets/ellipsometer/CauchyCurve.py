"""
:author:        Pierre-Louis Ronat
:created:       30-Nov-2017
:reference:     Silke Schoen
:description:   The program plots Cauchy curves according to FIRST ellipsometry measurements and saves the result in a .txt file (same folder)
                This script must be saved in dir_path, with the masterfile. It will generate a .txt of all the document and save the plots as png if desired.
"""
import sys, os, re
sys.path.append("../")
import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# USER CONTROLLED SECTION

target_wavelength = 632.8           # in nm
savefig = True
masterfile = 'CauchyMaster.rtf'

# The following section should not be modified
dir_path = "/Users/PL/Documents/ETHZ/PLR Master Thesis/Measurements/ellipsometer/"
if not os.path.exists(dir_path+'Txt'): # Mac
    os.makedirs(dir_path+'Txt')

wavelength = (350,850)
precision = 1000
wavelengthspan = linspace(wavelength[0],wavelength[1],precision)

with open(masterfile) as ma:                                            # Open master file
    lines = ma.read().split("\n")
    #print lines
    header = re.split(r'\t+', lines[8].rstrip('\t'))
    #print header
    nb_val = len(header)
    #print nb_val

    for l in lines[9:]:                                                 # Loop for each line / measurement
        val = re.split(r'\t+', l.rstrip('\t'))
        print val
        if len(val) != nb_val:
            print 'Skipping line: ' + repr(l) + '. Missing information.'
            continue
        if val[len(val)-1] != 'OK\\':
            print 'Skipping line: ' + repr(l) + '. Bad fit.'
            continue
        ID,d,n0,n1,n2,fit = [str(e) for e in val]                       # Store values

        C0 = 1e2
        C1 = 1e7
        n = float(n0) + C0*float(n1)/(wavelengthspan**2)+ C1*float(n2) / (wavelengthspan**4)
        n_target = float(n0) + C0*float(n1)/(target_wavelength**2)+ C1*float(n2) / (target_wavelength**4)

        if savefig == True:
            if not os.path.exists(dir_path+'Fig'): # Mac!!!
                os.makedirs(dir_path+'Fig')

            fig = plt.figure(1,figsize = (7,7),facecolor='white')
            ax = plt.subplot(1,1,1)
            p1 = ax.plot(wavelengthspan, n,'-r',label = 'Cauchy Curve')
            p2 = ax.plot(wavelengthspan,ones(len(wavelengthspan))*n_target,'-k',label = 'n = '+repr(round(n_target,3)) + ' at ' +repr(target_wavelength) + ' nm')
            l1 = ax.legend(loc='upper right')
            plt.title('Cauchy Equation')
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Film index [-]')
            g1 = ax.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
            g2 = ax.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
            ax.minorticks_on()
            start, end = ax.get_xlim()
            ax.xaxis.set_ticks(np.arange(start+25, end+25, 50))

            fig.savefig(dir_path+'Fig/'+ID+'_'+repr(int(round(float(d),0)))+'.png')
            plt.close(fig)

        fname = 'Cauchy_'+ ID + '_' +repr(int(round(float(d),0)))+'.txt'                    # Save as txt
        print(dir_path+'Txt/'+fname)
        f= open(dir_path+'Txt/'+fname,"w+")
        for i in range(0,precision):
            f.write(repr(round(wavelengthspan[i],3))+'\t'+repr(round(n[i],3))+'\t0\n')
        f.close()                                                                           # Loop back
    ma.close()



