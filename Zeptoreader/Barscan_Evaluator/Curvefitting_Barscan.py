# Copyright (c) 18.06.2015 Andreas Frutiger.  All right reserved.

#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.

#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#
# ---------------------------------------------------------------------------

import numpy as np
from scipy.optimize import curve_fit
from pylab import *
from sklearn.metrics import r2_score
from functools import partial


def linearfit(x, a, b):

    return a * x + b

def linearfit_origin(x, a):

    return a * x

def linearfit2d(x,a,b,c):

    return a*x[0] + b*x[1] + c

def quadraticfit(x, a, b, c):

    return a * x ** 2 + b * x + c

def quadraticfitZero(x, a, b):

    return a * x ** 2 + b * x

def quadraticfit2d(x,a,b,c,d,e,f):

    return a*x[0]*x[0] + b*x[1]*x[1] + c*x[0]*x[1] + d*x[0] + e*x[1] + f


def cubicfit(x, a, b, c, d):

    return a * x ** 3 + b * x ** 2 + c * x + d

def cubicfit2d(x,a,b,c,d,e,f,g,h,i,k):

    return a*x[0]*x[0]*x[0] + b*x[1]*x[1]*x[1] + c*x[1]*x[1]*x[0] + d*x[1]*x[0]*x[0] + e*x[0]*x[0] + e*x[1]*x[1] + g*x[0]*x[1] + h*x[0] + i*x[1] + k


def gauss_function(x, a, b, c):

    return a * np.exp(-(x - b) ** 2 / (2 * c ** 2))
#     popt, pcov = curve_fit(gauss_function, x, y, p0 = [max(y), mean(y), sigma(y)])


def exp_function_decay(x, a, d):

    return a * np.exp(-(x)*d)

def exp_function_decay_normalized(x, d):

    return np.exp((x)*d)

def exp_function_decay_Offset(x, a, d, b):

    return a * np.exp(-(x)*d) + b

def exp_function_decay_Int(x, a, d):

    return a * np.exp(-(x)*2/d)

def exp_function_decay_Int_Offset(x, a, d, b):

    return a * np.exp(-(x)*2/d) + b

def exp_function_saturation(x,b):

    return 100*(1.0 - np.exp(-b*x))

def exp_function_saturation_offset(x,b,a):

    return (2000-a)*(1.0 - np.exp(-b*x))+a

def exp_function_saturation_offset2(x,b):

    return (2000-487.43)*(1.0 - np.exp(-b*x))+487.43


def exp_function_increasing(x, a, b,x0):

    return a * np.exp(b*(x))


def fit_and_plot(func, x, y):

    #popt, pcov = curve_fit(func, x, y,p0=[1,314,0.2])
    popt, pcov = curve_fit(func, x, y)
    y2 = func(x, *popt)
    plot(x, y)
    plot(x, y2)

def fit_and_sample(func, x, y,other_sample_interval = 0):
    #popt, pcov = curve_fit(func, x, y, maxfev=10000)
    #popt, pcov = curve_fit(func, x, y,p0=[1.15613936e+04])
    popt, pcov = curve_fit(func, x, y)
    #popt, pcov = curve_fit(func, x, y,p0=[0.003],bounds=([400,-0.0001],[600, 0.004]))
    if other_sample_interval == 0:

        y2 = func(x, *popt)
    else:

        y2 = func(other_sample_interval, *popt)

    return y2


    return y2

def fit_and_sample2(func, x, y,other_sample_interval = 0):

    #popt, pcov = curve_fit(func, x, y, maxfev=10000)
    #popt, pcov = curve_fit(func, x, y, maxfev=10000,p0=[1.15613936e+04])
    #popt, pcov = curve_fit(func, x, y,p0=[0.003],bounds=([400,-0.0001],[600, 0.004]))


    #-----------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------
    # the following code is for one of the figures of the nature biotech paper

    interval = np.linspace(400,600,200)

    pcovv = []
    poptt = []

    for a in interval:

        func1 = partial(func,a=a)
        popt, pcov = curve_fit(func1, x, y,p0=[0.003])
        pcovv.append(pcov)
        poptt.append(popt)

    #print np.argmin(np.asarray(pcovv))
    poptt = np.asarray(poptt)
    #print interval[np.argmin(np.asarray(pcovv))]
    #print poptt[np.argmin(np.asarray(pcovv))]

    popt = [poptt[np.argmin(np.asarray(pcovv))],interval[np.argmin(np.asarray(pcovv))]]

    if len(other_sample_interval) == 1:

        y2 = func(x, *popt)
    else:

        y2 = func(other_sample_interval, *popt)

    return y2


def fit_and_return_para(func, x, y,p0 = 0):


    if p0 == 0:
        popt, pcov = curve_fit(func, x, y)
    else:
        popt, pcov = curve_fit(func, x, y,p0=p0,maxfev=10000)


    return popt

def calc_r2_score(y,modeldata):

    coefficient_of_determination = r2_score(y,modeldata)

    return coefficient_of_determination

def fit_and_sample2d(func,x,y,z):

    X1, X2 = np.meshgrid(x,y)
    size = X1.shape

    # make one dimensional arrays
    x1_1d = X1.reshape((1, np.prod(size)))
    x2_1d = X2.reshape((1, np.prod(size)))

    Z = z.reshape((1, np.prod(size)))



    xdata = np.vstack((x1_1d, x2_1d))

    #print xdata
    #print Z
    popt, pcov = curve_fit(func, xdata, Z[0])
    z_fit = func(xdata, *popt)
    Z_fit = z_fit.reshape(size)

    return Z_fit




# import matplotlib.pyplot as plt
# plt.subplot(1, 3, 1)
# plt.title("Real Function")
# plt.pcolormesh(X1, X2, Z)
# plt.axis(limits)
# plt.colorbar()
# plt.subplot(1, 3, 2)
# plt.title("Function w/ Noise")
# plt.pcolormesh(X1, X2, Z_noise)
# plt.axis(limits)
# plt.colorbar()
# plt.subplot(1, 3, 3)
# plt.title("Fitted Function from Noisy One")
# plt.pcolormesh(X1, X2, Z_fit)
# plt.axis(limits)
# plt.colorbar()

# plt.show()


# y = [3.7932000000000001, 3.7932000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 2.3095000000000003, 2.3095000000000003, 3.3094000000000001, 4.3093000000000004, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 4.3093000000000004, 4.3093000000000004, 3.3094000000000001, 2.3095000000000003, 2.3095000000000003, 4.3093000000000004, 3.3094000000000001, 2.3095000000000003, 2.3095000000000003, 1.3096000000000001, 2.3095000000000003, 4.3093000000000004, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 2.0815000000000001, 3.0813999999999999, 5.0811999999999999, 5.0811999999999999, 4.0812999999999997, 2.0815000000000001, 2.0815000000000001, 2.0815000000000001, 2.0815000000000001, 2.0707, 2.0707, 1.1848000000000001, 1.1848000000000001, 2.0707, 3.0705999999999998, 3.0705999999999998, 2.0815000000000001, 2.0815000000000001, 3.0813999999999999, 3.7932000000000001, 4.0212000000000003, 4.0212000000000003, 4.0212000000000003, 3.0212999999999997, 2.0214000000000003, 3.0212999999999997, 4.0212000000000003, 4.0212000000000003, 3.0212999999999997, 3.3094000000000001, 3.3094000000000001, 2.3095000000000003, 2.0815000000000001, 3.0813999999999999, 5.0811999999999999, 3.7932000000000001, 1.7934000000000001, 3.7932000000000001, 5.7930000000000001, 3.7932000000000001, 1.7934000000000001, 2.7932999999999995, 3.8039999999999998, 3.8039999999999998, 2.3912, 2.3912, 3.8039999999999998, 5.8038000000000007, 5.8038000000000007, 3.8039999999999998, 2.3912, 2.8041, 5.1027999999999993, 5.1844999999999999, 4.4835000000000003, 5.0812999999999997, 7.0918999999999999, 8.8143999999999991, 9.8960000000000008, 9.4939, 10.9884, 14.069800000000001, 18.091000000000001, 18.987699999999997, 26.7697, 25.079600000000003, 21.802599999999998, 18.460900000000002, 18.645800000000001, 19.058700000000002, 19.5425, 20.5532, 19.727399999999999, 19.955400000000001, 21.8843, 21.8735, 20.802700000000002, 19.080200000000001, 17.9986, 16.9879, 13.7925, 12.7818, 10.9884, 9.6788000000000007, 8.8961000000000006, 7.7004999999999999, 6.3909000000000002, 5.6791, 4.4835000000000003, 4.1845999999999997, 3.8856999999999999, 3.8148000000000004, 4.1029, 2.8041, 2.8041, 3.8039999999999998, 5.0319000000000003, 4.032, 5.0319000000000003, 3.0321000000000002, 3.0321000000000002, 5.0319000000000003, 3.8039999999999998, 3.8039999999999998, 3.0321000000000002, 3.0321000000000002, 3.0321000000000002, 4.032, 5.0319000000000003, 5.0319000000000003, 5.0319000000000003, 4.032, 3.0321000000000002, 3.0321000000000002, 3.0321000000000002, 3.0321000000000002, 3.0321000000000002, 3.0212999999999997, 3.0212999999999997, 3.0212999999999997, 2.7932999999999995, 3.7932000000000001, 3.7932000000000001, 2.7932999999999995, 2.7932999999999995, 3.0813999999999999, 2.0815000000000001, 1.1956, 2.0815000000000001, 2.0815000000000001, 3.0813999999999999, 5.0811999999999999, 5.0811999999999999, 3.0813999999999999, 3.0813999999999999, 3.0813999999999999, 4.0812999999999997, 3.0813999999999999, 3.0813999999999999, 4.0812999999999997, 5.0811999999999999, 3.7932000000000001, 2.7932999999999995, 4.7930999999999999, 4.7930999999999999, 4.7930999999999999, 3.0813999999999999, 3.0813999999999999, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 3.3094000000000001, 1.3096000000000001, 1.3096000000000001, 3.3094000000000001, 3.3094000000000001, 2.3095000000000003, 2.0815000000000001, 2.0815000000000001, 2.0815000000000001, 2.3095000000000003, 2.3095000000000003, 1.7117, 1.7117, 1.7117, 1.1848000000000001, 2.0707, 3.0705999999999998, 2.7116000000000002, 2.0707, 2.0815000000000001, 2.0815000000000001, 2.0815000000000001, 2.0815000000000001, 1.7934000000000001, 1.7934000000000001, 2.0815000000000001, 3.0813999999999999, 3.0813999999999999, 2.0815000000000001, 2.0815000000000001, 3.0813999999999999, 3.0813999999999999, 2.0707, 1.1848000000000001, 3.0705999999999998, 4.0705, 3.0705999999999998, 0.71179999999999999, 0.71179999999999999, 1.7117, 0.2989, 0.71179999999999999, 0.71179999999999999, 0.71179999999999999, 1.7117, 1.7117, 1.7117, 1.7117, 2.7116000000000002, 3.7114999999999996, 2.7116000000000002, 0.71179999999999999]
# x = np.linspace(0,1,len(y))
# print x


# popt, pcov = curve_fit(gauss_function, x, y)

# y2 = gauss_function(x,*popt)
# plot(x,y)
# plot(x,y2)
# show()

    # vecfunc = np.vectorize(func)

    # y = vecfunc(timepoints1, values[0], values[1])
