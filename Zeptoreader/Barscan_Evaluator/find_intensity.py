import numpy as np
from scipy import misc
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from skimage.feature import match_template
#from scipy import signal
from scipy.signal import fftconvolve
from scipy.ndimage.filters import gaussian_filter
from scipy.optimize import curve_fit
import csv
import os

def calcIntensityExpandingROI(molo_ROI,radiusPixel):
	"""Implements the algorithm described in the supporting information of the NN manuscript"""

	x_cord,y_cord = np.unravel_index(np.argmax(molo_ROI), molo_ROI.shape)


	# cut the mask for the background ROI

	ring_mask = c_mask(molo_ROI,(x_cord,y_cord),radiusPixel/2.,radiusPixel)

	# plt.imshow(ring_mask,interpolation='none')
	# plt.show()

	background_ROI = molo_ROI[ring_mask]

	mean_bg = background_ROI.mean()
	std_bg = background_ROI.std()

	# substract the background
	molo_ROI = molo_ROI - mean_bg

	signal = 0
	counter = 0

	print 'Intensity expanding ROI:'
	while True:
		additional_contribution_mask = c_mask(molo_ROI,(x_cord,y_cord),counter,counter + 1)
		# plt.figure()
		# plt.imshow(additional_contribution_mask,interpolation='none')
		# plt.show()

		additional_contribution = molo_ROI[additional_contribution_mask]

		print counter
		additional_signal = additional_contribution.sum()
		print additional_signal
		print additional_contribution
		print std_bg
		print len(additional_contribution)


		if additional_signal >= 3*std_bg*len(additional_contribution):

			signal += additional_signal
			counter += 1

		else:
			break

	return signal,mean_bg,std_bg

def c_mask(image, centre, radius1, radius2):
	"""
	Return a boolean mask for a donat with two radii specified by radius1 (inner radius) and radius2 (outer radius) 
	"""

	x, y = np.ogrid[:image.shape[0],:image.shape[1]]
	cy, cx = centre

	# circular mask
	circmask1 = (x-cx)**2 + (y-cy)**2 >= radius1**2
	circmask2 = (x-cx)**2 + (y-cy)**2 < radius2**2

	return np.logical_and(circmask1,circmask2)

def intensityMologramField(im,mologram_locations,pixelsize,radiusMologram=200e-6):

	moloROIs = cut_molograms(im,mologram_locations,pixelsize, radiusMologram=200e-6) # list of images of the size of the molograms

	signals = []
	background_means = []
	background_stds = []

	counter = 0
	# plt.figure()
	for moloROI in moloROIs:

		# plt.imshow(moloROI,interpolation='none')
		# plt.savefig(str(counter) + '.png')

		signal, bg_mean, bg_std = calcIntensityExpandingROI(moloROI,radiusMologram/pixelsize)

		signals.append(signal)
		background_means.append(bg_mean)
		background_stds.append(bg_std)
		counter += 1

	signals = np.array(signals)
	signals = signals.reshape((len(mologram_locations),len(mologram_locations[0])))

	background_means = np.array(background_means)			
	background_means = background_means.reshape((len(mologram_locations),len(mologram_locations[0])))

	background_stds = np.array(background_stds)			
	background_stds = background_stds.reshape((len(mologram_locations),len(mologram_locations[0])))

	return signals, background_means, background_stds

def cut_molograms(im,mologram_locations,pixelsize,radiusMologram=200e-6):

	"""
	Returns a list of cut images with an ROI of 400 um around the molograms
		:param mologram_locations: position of the molographic foci (tuples)
	"""

	radiusPixel = radiusMologram/pixelsize


	cut_molograms = []

	for i in range(len(mologram_locations)):

		for j in range(len(mologram_locations[0])):
			print i
			print j
			print mologram_locations[i][j]
			molo_ROI = im[(mologram_locations[i][j][0] - radiusPixel):(mologram_locations[i][j][0] + radiusPixel),(mologram_locations[i][j][1] - radiusPixel):(mologram_locations[i][j][1] + radiusPixel)]

			x_cord,y_cord = np.unravel_index(np.argmax(molo_ROI), molo_ROI.shape)

			# recut the image in order to make sure that it is really centered
			cut_molograms.append(im[(mologram_locations[i][j][0] - 2*radiusPixel + x_cord):(mologram_locations[i][j][0] + x_cord),(mologram_locations[i][j][1] - 2*radiusPixel  + y_cord):(mologram_locations[i][j][1] + y_cord)])

			
	return cut_molograms