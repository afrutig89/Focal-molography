import numpy as np
from scipy import misc
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from skimage.feature import match_template
from scipy.signal import fftconvolve
from scipy.ndimage.filters import gaussian_filter
from scipy.optimize import curve_fit
from find_intensity import *
import csv
import os
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import pandas as pd
from Curvefitting_Barscan import exp_function_decay_normalized, fit_and_return_para
from matplotlib import colors

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))))
from SimulationFramework.mologram.mologram import Mologram
from SimulationFramework.waveguide.waveguide import SlabWaveguide
from SimulationFramework.analytical.focalMolography import FocalMolographyAnalytical


from SimulationFramework.binder.Adlayer import MologramProteinAdlayer
################################################################################################
################################################################################################
# Description
# This is a software that automatically evaluates the barscans produced by the autohotkey script. The algorithm that is implemented follows exactly the describtion of the nature nanotechnology paper
# In addition to the signal values, the exponential decay along a line is calculated from the backgroung value of the molograms and for a molofield, the average decay length over the three lines is used.
# Positive decay constants are automatically discarded.

# The user needs to click onto the upper left mologram of any field.

def waveguide_power_from_scattering_bg_zeptoreader(I_bg, alpha, a_ani = 0.054):
    """
    Calculates the waveguide power form the scattering bg for the Zeptoreader, the Zeptoreader has the following two camera objectives that form the optical system.
    NIKKOR 35 mm 1:1.4, COMPUTAR 50 mm 1:1.3, the f number of 1.4 corresponds exactly to a numerical aperture of 0.33 which is exactly the numerical aperture of the molograms (Christof Fattinger
    thought of everything... =))
    
    """

    NA = 0.33

    return 4*I_bg/(NA**2*alpha*a_ani)

def convert_coherent_mass_density():
    """calculates the coherent mass density."""
    pass


class FieldConfiguration(object):
    """Baseclass for molofield configuration objects."""
    def __init__(self):
        
        self.radius = []
        self.focallengths = []
        self.horizontal_spacing  = 0
        self.vertical_spacing   = 0
        self.number_of_lines    = 0
        self.number_of_molograms_per_line = 0
        self.lines_to_skip = []
        self.background_positions = [] # line from which to estimate the damping for the mass quanitification from.
        self.field_name = '' # name of the field as a string

    def generateBooleanMask(self,image,starting_x,starting_y,pixelsize):
        """Generates a boolean reference array for an image of a certain size with

        :return : returns a boolean reference array of the same size as the image, whereas the mologram positions are marked as 1.
        """

        bool_reference_array = np.zeros(image_size)

        for i in range(number_of_lines):

            # skip some lines which do not contain molograms. 
            if i in self.lines_to_skip:
                continue

            for j in range(number_of_molograms_per_line ):
                x_pos = starting_x+i*int(self.vertical_spacing/pixelsize)
                y_pos = starting_y+j*int(self.horizontal_spacing/pixelsize)
                bool_reference_array[x_pos,y_pos] = 1.

        return bool_reference_array

    def generatePositions(self,starting_x,starting_y,pixelsize):
        """returns a list of tuples, with the x,y positions of the mologram locations. """

        positions = []
        background_positions = []

        print self.number_of_lines
        skip_counter = 0
        ref_pos_counter = 0

        for i in range(self.number_of_lines):

            # skip some lines which do not contain molograms. 
            if i in self.lines_to_skip:
                
                if i in self.background_positions:
                    background_positions.append([])

                    for j in range(self.number_of_molograms_per_line):
                        print 'generated reference position'
                        x_pos = starting_x + i*int(self.vertical_spacing/pixelsize)
                        y_pos = starting_y + j*int(self.horizontal_spacing/pixelsize)
                        background_positions[ref_pos_counter].append((x_pos,y_pos))
                    ref_pos_counter +=1
                    continue
                else:
                    skip_counter +=1
                    continue

            positions.append([])

            for j in range(self.number_of_molograms_per_line ):
                print 'generated position'
                x_pos = starting_x + i*int(self.vertical_spacing/pixelsize)
                y_pos = starting_y + j*int(self.horizontal_spacing/pixelsize)
                positions[i - skip_counter-ref_pos_counter].append((x_pos,y_pos))

        return positions, background_positions

class StandardFieldConfiguration(FieldConfiguration):


    def __init__(self):

        FieldConfiguration.__init__(self)

        self.radius = [[200e-6]*10,[200e-6]*10, [150e-6]*10]
        self.focallengths = [[900e-6]*10,[900e-6]*10, [900e-6]*10]
        self.horizontal_spacing  = 400e-6
        self.vertical_spacing   = 500e-6
        self.number_of_lines    = 4
        self.number_of_molograms_per_line = 10
        self.lines_to_skip = [1]
        self.background_positions = [1]
        self.field_name = ''

class ReferenceFieldConfiguration(FieldConfiguration):


    def __init__(self):

        FieldConfiguration.__init__(self)

        self.radius = [[200e-6]*5, [200e-6]*5, [150e-6]*5]
        self.focallengths = [[900e-6]*5, [900e-6]*5, [900e-6]*5]
        self.horizontal_spacing  = 800e-6
        self.vertical_spacing   = 500e-6
        self.number_of_lines    = 4
        self.number_of_molograms_per_line = 5
        self.lines_to_skip = [1]
        self.background_positions = [1]
        self.field_name = ''

class InverseMologramFieldConfiguration(FieldConfiguration):

    def __init__(self):

        FieldConfiguration.__init__(self)

        self.horizontal_spacing  = 800e-6
        self.vertical_spacing   = 500e-6
        self.number_of_lines    = 4
        self.number_of_molograms_per_line = 10
        self.lines_to_skip = [1]
        self.radius = [[150e-6]*10, [150e-6]*10, [150e-6]*10]
        self.focallengths = [[900e-6]*10, [900e-6]*10, [900e-6]*10]
        self.background_positions = [1]
        self.field_name = ''


class inputdialogdemo(QWidget):
    def __init__(self, parent = None):
        super(inputdialogdemo, self).__init__(parent)
        global focus
        global manually_select_fields
        global mologram_types
        global path
        global save_path
        global chipSpec
        global cover_medium
        mologram_types = dict()

        layout = QFormLayout()

        self.btn = QPushButton("Select Load Directory")
        self.btn.clicked.connect(self.getDir)
        self.le = QLabel('')
        layout.addRow(self.btn,self.le)

        self.btn1 = QPushButton("Select Save Directory")
        self.btn1.clicked.connect(self.getSaveDir)
        self.le1 = QLabel('')
        self.le1.setText("Same as load directory by default")
        layout.addRow(self.btn1,self.le1)

        # ugly copy paste but no time to do it nice, I will hate myself in the future...
        self.btn2 = QPushButton("B: Mologram Type")
        self.le2 = QLabel('')
        self.btn2.clicked.connect(lambda: self.getMolo('B',self.le2))
        mologram_types['B'] = "Full Mologram"
        self.le2.setText("Full Mologram")
        layout.addRow(self.btn2,self.le2)

        self.btn7 = QPushButton("C: Mologram Type")
        self.le7 = QLabel('')
        self.btn7.clicked.connect(lambda: self.getMolo('C', self.le7))
        mologram_types['C'] = "Full Mologram"
        self.le7.setText("Full Mologram")
        layout.addRow(self.btn7,self.le7)

        self.btn8 = QPushButton("D: Mologram Type")
        self.le8 = QLabel('')
        self.btn8.clicked.connect(lambda: self.getMolo('D', self.le8))
        mologram_types['D'] = "Full Mologram"
        
        self.le8.setText("Full Mologram")
        layout.addRow(self.btn8,self.le8)

        self.btn9 = QPushButton("E: Mologram Type")
        self.le9 = QLabel('')
        self.btn9.clicked.connect(lambda: self.getMolo('E', self.le9))
        mologram_types['E'] = "Full Mologram"
        self.le9.setText("Full Mologram")
        layout.addRow(self.btn9,self.le9)

        self.btn10 = QPushButton("F: Mologram Type")
        self.le10 = QLabel('')
        self.btn10.clicked.connect(lambda: self.getMolo('F', self.le10))
        mologram_types['F'] = "Full Mologram"
        self.le10.setText("Full Mologram")
        layout.addRow(self.btn10,self.le10)

        self.btn11 = QPushButton("G: Mologram Type")
        self.le11 = QLabel('')
        self.btn11.clicked.connect(lambda: self.getMolo('G', self.le11))
        mologram_types['G'] = "Full Mologram"
        self.le11.setText("Full Mologram")
        layout.addRow(self.btn11,self.le11)

        self.btn3 = QPushButton("Chip Specification")
        self.btn3.clicked.connect(self.getChipSpec)
        chipSpec = ''
        self.le3 = QLabel(chipSpec)
        layout.addRow(self.btn3,self.le3)

        self.btn12 = QPushButton("Cover medium")
        self.btn12.clicked.connect(self.get_cover_medium)
        cover_medium = 'Water'
        self.le12 = QLabel(cover_medium)
        layout.addRow(self.btn12,self.le12)



        self.btn4 = QPushButton("Focus")
        self.btn4.clicked.connect(self.getFocus)
        focus = 0
        self.le4 = QLabel('')
        self.le4.setText(str(focus))
        layout.addRow(self.btn4,self.le4)

        self.btn5 = QPushButton("Manually select fields")
        self.btn5.clicked.connect(self.getManual)
        manually_select_fields = ['B','C','D','E','F','G']
        self.le5 = QLabel('')
        self.le5.setText("B,C,D,E,F,G")
        layout.addRow(self.btn5,self.le5)

        self.btn6 = QPushButton("Run")
        self.btn6.clicked.connect(self.close)
        layout.addRow(self.btn6)

        self.setLayout(layout)

        self.setWindowTitle("Barscan Evaluator V2.0 Andreas Frutiger")
        
    def getDir(self):
        global path
        global save_path
        
        path = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        save_path=path
        self.le.setText(path)

    def getSaveDir(self):
        global save_path
        save_path = str(QFileDialog.getExistingDirectory(self, "Select Save Directory"))
            
        self.le1.setText(save_path)
            
    def getMolo(self, field, label):
        """
        :param field: string indicating the field number. 
        """
        global mologram_types
        items = ("Full Mologram", "Reference Mologram", "Inverse Mologram")
        
        mologram_type, ok = QInputDialog.getItem(self, "select input dialog", "Mologram Type", items, 0, False)

        mologram_types[field] = mologram_type # dictionary holding the mologram types of the different fieldss
            
        if ok and items:
            label.setText(mologram_type)

    def getChipSpec(self):
        global chipSpec
        chipSpec, ok = QInputDialog.getText(self, 'Chip Specification', 'Enter the chip condition:')

        
        if ok:
            self.le3.setText(str(chipSpec))

    def get_cover_medium(self):
        global cover_medium

        items = ("Water", "Air")

        cover_medium, ok =  QInputDialog.getItem(self, "select input dialog", "Cover Medium", items, 0, False)

        if ok:
            self.le12.setText(cover_medium)
        
            

    def getManual(self):
        global manually_select_fields
        text, ok = QInputDialog.getText(self, 'User Spec Input Field', 'Enter the fields that need to be verified manually:')
        if ok:
            self.le5.setText(str(text))
            manually_select_fields = list(str(text))

    def getFocus(self):
        global focus
        focus,ok = QInputDialog.getInt(self,"integer input dualog","enter Focus")

        if ok:
            self.le4.setText(str(focus))


def print_on_image(fig1,im,mologram_positions,signals,weighted_signals,index):
    """Function that prints the mologram intensity onto the image in order to display it to the user.
    :param index: index of the subplot.
    """
    ax1 = fig1.add_subplot(6,1,index)

    ax1.imshow(im,cmap='inferno',norm = colors.PowerNorm(gamma=0.25))
    ax1.set_axis_off()

    for i in range(len(mologram_positions)):

        for j in range(len(mologram_positions[0])):

            ax1.text(mologram_positions[i][j][1],mologram_positions[i][j][0],(str(int(round(signals[i][j]/1000))) + 'k'),color='white',fontsize=7)
            try:
                ax1.text(mologram_positions[i][j][1],mologram_positions[i][j][0]-10,(str(int(round(weighted_signals[i][j]/1000))) + 'k'),color='red',fontsize=7)
            except:
                pass

def main():
    # define global variables, why the hell global variables!!!

    global focus
    global manually_select_fields
    global mologram_types
    global path
    global save_path
    global chipSpec
    global cover_medium

    mologram_adlayer = MologramProteinAdlayer(M_protein=52800, eta = 1) # eta is currently one, so the equivalent coherent mass density is calculated.

    app = QApplication(sys.argv)
    ex = inputdialogdemo()
    ex.show()
    app.exec_()
    
    special_file=[]
    starting_x_user=[]
    starting_y_user=[]
    pixelsize = 20 #in micron


    def on_click(event):
        global starting_x,starting_y
        starting_y = event.xdata
        starting_x = event.ydata    
        plt.close()
        global starting_coord
        starting_coord=[starting_x,starting_y]
        return starting_coord
    

################################################################################################
################################################################################################
# settings

    # debugging, if True then intermediate results are saved to a plots folder.

    global debug 

    debug = True
    
    #Zeptoreader_LBB
    subimage_x1 = 280
    subimage_x2 = 500

    subimage_y1 = 40
    subimage_y2 = 450

    pixelsize = 12.5e-6 # pixelsize in m
    

    plt.close()
    files = []



################################################################################################
################################################################################################
# image loading
    # get all the images from the folder and display them in the console
    
    directory_content = os.listdir(path)

    if debug:

        if not os.path.exists(path + '/plots'):
            os.makedirs(path + '/plots')

    for i in directory_content:
        print i
        print focus
        print 'Off_'+ str(focus) + '-focus' in i
        print '.tif' in i
        if os.path.isfile(os.path.join(path,i)) and 'Off_'+str(focus)+'-focus' in i and '.tif' in i:
            files.append(i)
    files = sorted(files, key=lambda x: ord(x[0]))
    print files
    field_counter = 0


    signals = []
    weighted_signals = []
    background_means = []
    background_stds = []
    bad_file = []
    fields = [] # mologram fields supplied
    lines = []
    moloNumber = []

################################################################################################
################################################################################################
# image cutting

    if len(manually_select_fields)!=0:

        special_file=[]
        file_number=0

        for f_1 in files:

            if f_1[0] in manually_select_fields:

                im_ori = cv2.imread(path+'/'+f_1,-1)

                im=im_ori[subimage_x1:subimage_x2,subimage_y1:subimage_y2] # specify the size of the cut image. 
                fig2=plt.figure()                   
                ax2=plt.gca()
                # show the user the image in order to click on the upper left mologram. 
                implot = ax2.imshow(im,cmap='inferno',norm = colors.PowerNorm(gamma=0.25))
                cid = fig2.canvas.callbacks.connect('button_press_event', on_click)

                plt.show()
                starting_x=starting_coord[0]
                starting_y=starting_coord[1]



                starting_x_user.append(starting_x)
                starting_y_user.append(starting_y)
                manually_select_fields.pop(0)
                special_file.append(file_number)
                file_number+=1

            else:

                file_number+=1
                continue

################################################################################################
################################################################################################
# Data Processing

    subplotsize=[8.,12.]
    figuresize=[8.,12.]   
    left = 0.5*(1.-subplotsize[0]/figuresize[0])
    right = 1.-left
    bottom = 0.5*(1.-subplotsize[1]/figuresize[1])
    top = 1.-bottom
    figsize = (figuresize[0],figuresize[1])
    fig1 = plt.figure(figsize = (4,20))
    fig1.subplots_adjust(left = left,right = right,bottom = bottom,top = top)
    cord_counter = 0

    if cover_medium == 'Water':
        cover_medium_n = 1.33
    elif cover_medium == 'Air':
        cover_medium_n = 1.
    else:
        raise RuntimeError("Cover medium not implemented! ")

    alphas = []
    alpha_chip = [] # damping constants of the chip.
    alpha_chip_db = []
    fieldconfigurations = dict()
    for f in files:

        print "Current File Being Processed is: "+ f
        
        im_ori = cv2.imread(path + '/' + f,-1)
        im = im_ori[subimage_x1:subimage_x2,subimage_y1:subimage_y2] #

        # get the field name
        current_field = f[0] # as a convention it is the first letter of the image file name on the harddrive.
 
        if mologram_types[current_field] == 'Reference Mologram':
                
            fieldconfigurations[current_field] = ReferenceFieldConfiguration()

        elif mologram_types[current_field] =='Full Mologram':

            fieldconfigurations[current_field] = StandardFieldConfiguration()

        elif mologram_types[current_field] == 'Inverse Mologram':

            fieldconfigurations[current_field] = InverseMologramFieldConfiguration()
        else:
            raise RuntimeError("This field configuration is not implemented")


        # if the user has specified the coordinates then use these coordinates to cut the images
        if field_counter in special_file:

            starting_x = int(starting_x_user[cord_counter])
            starting_y = int(starting_y_user[cord_counter])
            cord_counter += 1

            # plt.figure()
            # img=plt.imshow(im,cmap='gray')
            # print starting_x
            # print starting_y
            # plt.plot(starting_x,starting_y,ms=100,color = 'r')
            
            # plt.show()

        # if the user has not specified the coordinates then use the automatic recognition. 
        else:

            image_size = im.shape
        
            starting_x = int(image_size[0]/2)- 60   #40
            starting_y = int(image_size[1]/2)- 150  #40

            bool_reference_array = fieldconfigurations[current_field].generateBooleanMask(im,starting_x,starting_y,pixelsize)

            #generate kernel for cross correlation
            
            #Gaussian blur the pattern to mimic the real image

            #reference_array=gaussian_filter(bool_reference_array,sigma=1)

            #use fft convolution to find the best match pattern
            result = fftconvolve(im,bool_reference_array[::-1,::-1],mode='same')
            i,j = np.unravel_index(np.argmax(result), result.shape)

            #since use "same" mode in fftconvolve, return image has the same 
            #size as the original image
            #find how much the pattern needs to be moved to match with the real image
            x_shift = i - int(image_size[0]/2) # i - 383
            y_shift = j - int(image_size[1]/2) # j - 255
            starting_x = starting_x + x_shift
            starting_y = starting_y + y_shift

        print 'Detected the following positions:'
        positions,background_positions = fieldconfigurations[current_field].generatePositions(starting_x,starting_y,pixelsize)
        print positions
        print background_positions


        signal, background_mean, background_std = intensityMologramField(im,positions,pixelsize,radiusMologram=200e-6) # variables returned have shape (lines, number_of_molograms per line)

        
        signals += list(np.ravel(signal)) # list addition

        background_means += list(np.ravel(background_mean))
        background_stds += list(np.ravel(background_std))

        plt.figure()

        # decays on the mologram locations
        decays = []

        # estimate the slopes for all the rows and reject the ones that yield positive slopes
        # calculate the mean slope per field and use this in order to correct for the damping of the mode along the molograms. 
        
        for i,line in enumerate(background_mean):

            line = line/line[0] # normalize the line to the first mologram (background of the first mologram)
            molopos_y = [j[1] for j in positions[i]]

            # estimate starting slope
            p0 = (line[-1] - line[0])/(molopos_y[-1]-molopos_y[0])

            # evtl reject all the fits which have a positive popt
            popt = fit_and_return_para(exp_function_decay_normalized,np.asarray(molopos_y) - molopos_y[0],line,p0)

            plt.plot(np.asarray(molopos_y) - molopos_y[0],line,lw=0,marker='o',label='line' + str(i + 1))

            if popt[0] > 0:
                continue
            else:
                decays.append(popt[0])

        

        decay = np.asarray(decays).mean()
        print 'Estimated the following decay on mologram locations unit is in inverse pixels:'
        print decay
        print 'in dB/cm'
        alpha = -1*decay*1./(pixelsize)*1/np.log(10)/10.
        print alpha
        alphas.append(alpha)
        plt.gca().text(100,1,'{:2f} db/cm'.format(-1*decay*1./(pixelsize)*1/np.log(10)/10.))

        ################################################################################################
        ################################################################################################
        # Damping estimation of the chip
        # cuts circles for the damping estimation of the chip 
        background_for_damping = np.asarray([np.mean(i) for i in cut_molograms(im,background_positions,pixelsize,radiusMologram=200e-6)])

        norm_back_grounds = background_for_damping/background_for_damping[0]
        backgroundpos_y = [i[1] for i in background_positions[0]]
        print background_positions
        print backgroundpos_y
        print norm_back_grounds
        # estimate starting slope
        p0 = (norm_back_grounds[-1] - norm_back_grounds[0])/(backgroundpos_y[-1] - backgroundpos_y[0])
        popt = fit_and_return_para(exp_function_decay_normalized, np.asarray(backgroundpos_y) - backgroundpos_y[0], norm_back_grounds, p0)

        alpha_db_chip = -1*popt[0]*1./(pixelsize)*1/np.log(10)/10.
        alpha_chip.append(alpha_db_chip*np.log(10)/10. * 100) # convert to Np/m
        alpha_chip_db.append(alpha_db_chip)

        if debug:

            plt.plot(np.linspace(0,molopos_y[-1] - molopos_y[0],100),exp_function_decay_normalized(np.linspace(0,molopos_y[-1] - molopos_y[0],100),decay),color='k',alpha=0.3,ls='--')
            plt.legend()
            plt.xlabel('distance a.u.')
            plt.ylabel('intensity normalized')
            plt.savefig(path + '/plots/' + f[0] + '.png', format='png',dpi=600)

        weighted_signals_plot = []
        for i,line in enumerate(signal):

            weighting_factor = 1/exp_function_decay_normalized(np.asarray(molopos_y) - molopos_y[0],decay)

            weighted_signals += list(weighting_factor * line)
            weighted_signals_plot.append(list(weighting_factor * line))

            fields += list(f[0]*len(line))
            lines += list([(i+1)]*len(line))
            moloNumber += list(np.arange(1,len(line) + 1))


        field_counter+=1
        print_on_image(fig1,im,positions,signal,weighted_signals_plot,field_counter)


    # average damping constant of the chip
    alpha_avg = np.median(alpha_chip)
    # power in the waveguide at the mologram positions from the background intensity, important to divide by the area of one pixel.

    power_wg_molo_positions = waveguide_power_from_scattering_bg_zeptoreader(np.asarray(background_means)/(pixelsize*pixelsize),alpha_avg) 



    coherent_mass_densities = []

    # calculate the equivalent coherent mass densities. 
    for i, signal in enumerate(signals):


        mologram_adlayer = MologramProteinAdlayer(M_protein=52800, eta = 1) # eta is currently one, so the equivalent coherent mass density is calculated.
        
        waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                              n_c=cover_medium_n, d_f=145e-9, polarization='TE',
                              inputPower=power_wg_molo_positions[i], wavelength=632.8e-9, attenuationConstant=alpha_avg)

        # To do: Generalize this for any molecular mass and also particles on the mologram. Currently SAv is implemented.
        print fieldconfigurations[fields[i]].focallengths[lines[i]-1][moloNumber[i]-1]
        print 2*fieldconfigurations[fields[i]].radius[lines[i]-1][moloNumber[i]-1]
        print lines[i]

        mologram = Mologram(waveguide, focalPoint=-fieldconfigurations[fields[i]].focallengths[lines[i]-1][moloNumber[i]-1] , diameter=2*fieldconfigurations[fields[i]].radius[lines[i]-1][moloNumber[i]-1],braggOffset = 25e-6)

        focal_molo_analytical = FocalMolographyAnalytical(mologram, mologram_adlayer)

        # convert the total power to the average intensity on the Airy disk.
        I_avg = signal/mologram.calcAiryDiskAreaAnalytical() # average intensity on the Airy disk. 
        coherent_mass_densities.append(focal_molo_analytical.mass_density_on_mologram_from_average_intensity(I_avg, correctBraggArea=True))


    save_path = str(save_path)
    chipSpec = str(chipSpec)
    fig1.savefig(save_path + '/'+ chipSpec + '.png',bbox_inches='tight',dpi=600)
        
    data = {
        'signals':signals,
        'weighted_signals':weighted_signals,
        'equivalent coherent mass densities not bragg corrected':coherent_mass_densities,
        'background mean':background_means,
        'background std':background_stds,
        'fields': fields,
        'lines':lines,
        'moloNumber':moloNumber
    }

    data = pd.DataFrame(data)

    data.to_csv(save_path + '/' + chipSpec + '.csv',sep = ';')
    # to do: export field names
    data_damping = {
        'alpha_dB_molos':alphas,
        'alpha_chip_db':alpha_chip_db
    }

    data_damping = pd.DataFrame(data_damping)

    data_damping.to_csv(save_path + '/' + chipSpec + '_damping.csv',sep = ';')



main()

        #print_on_image('Reference Mologram',signal)
