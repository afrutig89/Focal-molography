﻿# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('TkAgg')
import numpy as np
import os
import sys
# import cv2
from PIL import Image #use instead of cv2, as it is readily available

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import LogFormatter
from matplotlib.ticker import LogLocator
from matplotlib.colors import LogNorm
#from skimage.feature import match_template
from scipy.signal import fftconvolve
from scipy.ndimage.filters import gaussian_filter
from barscan_HH.find_intensity import find_intensity
from barscan_HH.exponential_weight import exponential_weight
from barscan_HH.save_csv import save_csv

try:
    from PyQt5.QtCore import *
    from PyQt5.QtWidgets import QApplication, QWidget, QFormLayout, QPushButton, QLineEdit, QCheckBox, QFileDialog, QInputDialog, QMessageBox, QLabel
except:
    from PyQt4.QtCore import *
    from PyQt4.QtGui import QApplication, QWidget, QFormLayout, QPushButton, QLineEdit, QCheckBox, QFileDialog, QInputDialog, QMessageBox, QLabel
    print("No PyQt5 found - falling back to PyQt4.")


import pickle #for saving GUI content
from functools import cmp_to_key #for filelist sorting
import locale #for filelist sorting

#Verwende PIL und XML, um Exif-Daten aus dem Header der Tif-Dateien zu ziehen
import xml.etree.ElementTree as ET

swversion = "barscan v2018-05-15"
debug = False
algo = "signalMinusBg" 
#algo = "expandingRoi"

class inputdialogdemo(QWidget):
    def __init__(self, parent = None):
        super(inputdialogdemo, self).__init__(parent)
        global focus
        global user_input_field
        global mologram_type
        global path
        global save_path
        global chipSpec
        global normalize_to_header_data
        global logscale
        global max_radius
        global algo
  
        layout = QFormLayout()
        
        if os.path.exists('defaults.dat'):
            with open ('defaults.dat', 'rb') as fp:
                itemlist = pickle.load(fp)
        else:
            itemlist = ['output', 180, 'A', True, True, 0]
        chipSpec, focus, user_input_field, normalize_to_header_data, logscale, max_radius = itemlist
        
        self.label= QLabel("Algorithm used: "+("Signal minus bg" if algo=="signalMinusBg" else "Expanding ROI"))
        layout.addRow(self.label)
        
        if algo=="signalMinusBg":
            self.btn6 = QPushButton("Signal radius (px)")
            self.btn6.setToolTip("Radius of integration radius around focus center for signal determination")
        if algo=="expandingRoi":
            self.btn6 = QPushButton("Max signal radius (px)")
            self.btn6.setToolTip("0 to disable")
        self.btn6.clicked.connect(self.getRadius)
        self.le6 = QLineEdit()
        self.le6.setPlaceholderText(str(max_radius))
        self.le6.setInputMask("99")
        self.le6.textChanged.connect(self.getRadiusText)
        layout.addRow(self.btn6,self.le6)
             

        self.btn = QPushButton("Select Load Directory")
        self.btn.clicked.connect(self.getDir)
        self.le = QLineEdit()
        layout.addRow(self.btn,self.le)

        self.btn1 = QPushButton("Select Save Directory")
        self.btn1.clicked.connect(self.getSaveDir)
        self.le1 = QLineEdit()
        self.le1.setPlaceholderText("Same as load directory by default")
        layout.addRow(self.btn1,self.le1)
        
        self.btn3 = QPushButton("Output file prefix")
        self.btn3.setToolTip("Filename for results and overview image.")
        self.btn3.clicked.connect(self.getChipSpec)
        self.le3 = QLineEdit()
        self.le3.setPlaceholderText(str(chipSpec))
        self.le3.textChanged.connect(self.getChipSpecText)
        layout.addRow(self.btn3,self.le3)
        
        self.btn2 = QPushButton("Mologram Type")
        self.btn2.clicked.connect(self.getMolo)
        mologram_type="Full Mologram"
        self.le2 = QLineEdit()
        self.le2.setPlaceholderText("Full Mologram")
        layout.addRow(self.btn2,self.le2)

        self.btn4 = QPushButton("Focus")
        self.btn4.clicked.connect(self.getFocus)
        self.le4 = QLineEdit()
        self.le4.setPlaceholderText(str(focus))
        self.le4.setInputMask("999")
        self.le4.textChanged.connect(self.getFocusText)
        layout.addRow(self.btn4,self.le4)

        self.btn5 = QPushButton("Fields for manual focus")
        self.btn5.setToolTip("e.g. 'BCE' or empty for None")
        self.btn5.clicked.connect(self.getManual)
        self.le5 = QLineEdit()
        self.le5.setPlaceholderText(str(user_input_field))
        self.le5.textChanged.connect(self.getManualText)
        layout.addRow(self.btn5,self.le5)

        
        self.chkBox1 = QCheckBox('Normalize signal using header data (experimental!)', self)
        if normalize_to_header_data:        
            self.chkBox1.setCheckState(Qt.Checked)
        else:
            self.chkBox1.setCheckState(Qt.Unchecked)
        self.chkBox1.setTristate(False)
        self.chkBox1.stateChanged.connect(self.checkNormalize)
        layout.addRow(self.chkBox1)
        
        self.chkBox2 = QCheckBox('Logarithmic color scale', self)
        if logscale:
            self.chkBox2.setCheckState(Qt.Checked)
        else:
            self.chkBox2.setCheckState(Qt.Unchecked)
        self.chkBox2.setTristate(False)
        self.chkBox2.stateChanged.connect(self.checkLogScale)
        layout.addRow(self.chkBox2)

        self.btn6 = QPushButton("Run - please select load directory first")
        self.btn6.setDisabled(True)
        self.btn6.clicked.connect(self.close)
        layout.addRow(self.btn6)


        self.setLayout(layout)

        self.setWindowTitle(swversion)
        
    def checkNormalize(self, state):
        global normalize_to_header_data
        if state == Qt.Checked:
            normalize_to_header_data = True
        else:
            normalize_to_header_data = False
            
    def checkLogScale(self, state):
        global logscale
        if state == Qt.Checked:
            logscale = True
        else:
            logscale = False
        
    def getDir(self):
        global path
        global save_path
        FileDialog=QFileDialog()
        path = str(FileDialog.getExistingDirectory(self, "Select Directory"))
        save_path=path
        self.le.setText(path)
        #allow script to be run only after setting the load directory
        self.btn6.setText("Run")
        self.btn6.setEnabled(True)

    def getSaveDir(self):
        global save_path
        save_path = str(QFileDialog.getExistingDirectory(self, "Select Save Directory"))
            
        self.le1.setText(save_path)
            
    def getMolo(self):
        global mologram_type
        items = ("Full Mologram", "Reference Mologram")
        
        mologram_type, ok = QInputDialog.getItem(self, "select input dialog", "Mologram Type", items, 0, False)
            
        if ok and items:
            self.le2.setText(mologram_type)

    def getChipSpec(self):
        global chipSpec
        chipSpec, ok = QInputDialog.getText(self, 'Chip Specification', 'Enter the chip condition:')
        if ok:
            self.le3.setText(str(chipSpec))
            
    def getChipSpecText(self):
        global chipSpec
        chipSpec = str(self.le3.text())

    def getFocus(self):
        global focus
        focus,ok = QInputDialog.getInt(self,"integer input dualog","enter Focus")
        if ok:
            self.le4.setText(str(focus))
            
    def getFocusText(self):
        global focus
        focus = int(self.le4.text())        
        
    def getManual(self):
        global user_input_field
        text, ok = QInputDialog.getText(self, 'User Spec Input Field', 'Enter the fields that need to be verified manually:')
        if ok:
            self.le5.setText(str(text))
            user_input_field=str(text)
                        
    def getManualText(self):
        global user_input_field
        user_input_field = str(self.le5.text())
        
    def getRadius(self):
        global max_radius
        if algo=="signalMinusBg":
            value, ok = QInputDialog.getInt(self, 'Signal Radius', 'Enter the signal radius:',
                                            min=0, max=100)
        if algo=="expandingRoi":
            value, ok = QInputDialog.getInt(self, 'Maximum Radius', 'Enter the maximum signal radius (0 to disable):',
                                            min=0, max=100)
        if ok:
            self.le6.setText(str(value))
            max_radius=int(value)
            
    def getRadiusText(self):
        global max_radius
        max_radius = int(self.le6.text())        

def main(): 
    global focus
    global user_input_field
    global mologram_type
    global path
    global save_path
    global chipSpec
    global normalize_to_header_data
    global logscale
    global algo
    global pixelsize
    #global starting_x,starting_y
    
    #dict for transmission@635nm of grey filters on different readers
    #JH: data from offset corrected PD voltages 2018-04-20
    transmission={}
    transmission["Z3_JH"]={}
    transmission["Z3_JH"][0.001] = 0.00152
    transmission["Z3_JH"][0.01]  = 0.0117
    transmission["Z3_JH"][0.025] = 0.0340
    transmission["Z3_JH"][0.05]  = 0.0479
    transmission["Z3_JH"][0.1]   = 0.0959
    transmission["Z3_JH"][0.25]  = 0.250
    transmission["Z3_JH"][0.5]   = 0.423
    transmission["Z3_JH"][1]     = 1
    #dummy values for ETH until real values are known
    transmission["Z1_ETH"]={}
    transmission["Z1_ETH"][0.001] = 0.001
    transmission["Z1_ETH"][0.01]  = 0.01
    transmission["Z1_ETH"][0.025] = 0.025
    transmission["Z1_ETH"][0.05]  = 0.05
    transmission["Z1_ETH"][0.1]   = 0.1
    transmission["Z1_ETH"][0.25]  = 0.25
    transmission["Z1_ETH"][0.5]   = 0.5
    transmission["Z1_ETH"][1]     = 1
    #dummy values for unknown readers
    transmission["unknown"]={}
    transmission["unknown"][0.001] = 0.001
    transmission["unknown"][0.01]  = 0.01
    transmission["unknown"][0.025] = 0.025
    transmission["unknown"][0.05]  = 0.05
    transmission["unknown"][0.1]   = 0.1
    transmission["unknown"][0.25]  = 0.25
    transmission["unknown"][0.5]   = 0.5
    transmission["unknown"][1]     = 1
    
    #PD offset in mV
    pd_offset={}
    pd_offset["Z3_JH"]=32
    pd_offset["Z1_ETH"]=0 #dummy value
    pd_offset["unknown"]=0 #dummy value
    
    app = QApplication(sys.argv)
    ex = inputdialogdemo()
    ex.show()
    app.exec_()
    
    #save gathered GUI values to file       
    itemlist = chipSpec, focus, user_input_field, normalize_to_header_data, logscale, max_radius
    with open('defaults.dat', 'wb') as fp:
        pickle.dump(itemlist, fp)
        
    if mologram_type == 'Reference Mologram':
        col_pitch_px = 65
        col_num = 5
    else:
        col_pitch_px = 32.5
        col_num = 10
        
    row_pitch_px=40  #zeilenabstand ist 500 um, 40 px
        
    pixelsize=12.5 #in micron
        
    special_file=[]
    starting_x_user=[]
    starting_y_user=[]

    def on_click(event):
        global starting_x,starting_y
        starting_x = event.xdata
        starting_y = event.ydata
        plt.close(fig2)
        global starting_coord
        starting_coord=[starting_x,starting_y]
        return starting_coord
    
    files = []

    directory_content = os.listdir(path)

    for i in directory_content:
        if os.path.isfile(os.path.join(path,i)) and 'Off_'+str(focus)+'-focus' in i and '.tif' in i:
            files.append(i)
    files=sorted(files, key=cmp_to_key(locale.strcoll)) #key=lambda x: ord(x[0]))
    print ("Files used after filtering by focus:")
    print (files)
    fields=[]
    field_counter=0
    save_lr_coords=[]
    save_tb_coords=[]
    save_signal=[]
    save_signal_radius=[]
    save_weighted_signal=[]
    save_background=[]
    save_background_sigma=[]
    bad_file=[]
    save_comment=[]
    save_comment.append("Normalisiert auf Headerdaten: "+str(normalize_to_header_data))
    
    imlimits=[280,520,40,480] #cut image to size, limits are ytop, ybottom, xleft, xright
    if len(user_input_field)!=0:
        special_file=[]
        file_number=0
        for f_1 in files:
                if f_1[0] in user_input_field: #Check, ob erster Buchstabe des Dateinamens in user_input_field enthalten ist
                    img = Image.open(path+'/'+f_1)
                    im_ori = np.array(img)
                    #im_ori = cv2.imread(path+'/'+f_1,-1) # old cv import
                    im=im_ori[imlimits[0]:imlimits[1], imlimits[2]:imlimits[3]]  
                    fig2=plt.figure()                   
                    ax2=plt.gca()
                    #only display upper half and left third for higher clicking accuracy
                    implot = ax2.imshow((im+1)[0:int(im.shape[0]/2),0:int(im.shape[1]/3)],
                                             cmap='CMRmap', norm=LogNorm())
                    cid = fig2.canvas.callbacks.connect('button_press_event', on_click)
                    plt.show()
                    starting_x=starting_coord[0]
                    starting_y=starting_coord[1] #erhalte Startkoordinaten von imshow zurueck
                    starting_x_user.append(starting_x)
                    starting_y_user.append(starting_y)
                    #user_input_field.pop(0)
                    special_file.append(file_number)
                    file_number+=1
                else:
                    file_number+=1
                    continue

    cord_counter=0
    #setup main canvas
    figuresize=[10.,30.]    #13 18
    subplotsize=[9,17]
    left = 0.2*(1.-subplotsize[0]/figuresize[0])
    right = 1.-0.2*(subplotsize[0]/figuresize[0])
    bottom = 0.5*(1.-subplotsize[1]/figuresize[1])
    top = 1.-bottom
    fig1=plt.figure( figsize=(figuresize[0],figuresize[1]) )
    fig1.subplots_adjust(left=left,right=right,bottom=bottom,top=top)
    
    darkcorrectionwarning = True
    exposurewarning = True #for only showing warning windows once

    for f in files:
        print ("Current File Being Processed is: "+ f)
        
        imagefilepath=os.path.join(path,f)        
        img = Image.open(imagefilepath)
        im_ori = np.asfarray(img)
        # im_ori = cv2.imread(path+'/'+f,-1)
        im=im_ori[imlimits[0]:imlimits[1], imlimits[2]:imlimits[3]]
        if np.max(im) == 65535:
            print ("=== WARNUNG = UEBERBELICHTETE PIXEL ===")
            fig3,ax3 = plt.subplots(figsize=(15,10))
            myimg = ax3.imshow(im, cmap="Greys_r", vmax=65534, interpolation="none")
            myimg.cmap.set_over('r')
            myimg.set_clim(0, 65534)
            ax3.set_title('Warnung, ueberbelichtete Pixel (rot)')
            save_path=str(save_path)
            fig3.savefig(os.path.join(save_path,os.path.splitext(f)[0]+'_Belichtungswarnung.png'),bbox_inches='tight',dpi=150)
            fig3.clf()
            if exposurewarning:
                w = QWidget()
                QMessageBox.critical(w, "Belichtungswarnung", "Datei:\n%s\n\nBild ist ueberbelichtet, bitte neue Aufnahme anfertigen." % (f, ))
                w.show()
                w.destroy()
                exposurewarning = False
    
        #==============================
        #header import
        #==============================
        if img.tag[272][0]=="ZeptoREADER SN: 1014":
            reader="Z1_ETH"
        elif img.tag[272][0]=="ZeptoREADER SN: 1034":
            reader="Z3_JH"
        else:
            reader="unknown"
            print ("Warning: Current Zeptoreader is not included in database.\nNominal filter values are used.")
        print("ZEPTOReader: %s" % reader)
            
        header = img.tag[270][0] #Feld 270 im TIF-Header enthaelt XML-Daten des Zeptoreaders
        xmlroot = ET.fromstring(header) #parse as XML Tree into xmlroot
        darkcurrentsubtraction = str(xmlroot.find('DarkCurrentSubtraction').get('value'))
        exposuretime=float(xmlroot.find('ExposureTime').get('value'))
        exposuretime_unit=str(xmlroot.find('ExposureTime').get('unit')) #in ms?
        grayfilter=float(xmlroot.find('GrayFilter').get('value'))
        couplingvalue=float(xmlroot.find('CouplingValue').get('value'))
        couplinggrayfilter=float(xmlroot.find('CouplingGrayFilter').get('value'))
        
        if darkcorrectionwarning and np.percentile(im_ori, 5) == 0:
            w = QWidget()
            QMessageBox.critical(w, "Warnung: Dark Current Subtraction fehlerhaft", 
                                 "Zu viele Pixel mit Wert 0.\nBilder bitte erneut ohne Dark Current Subtraction aufnehmen.")
            w.show()
            w.destroy()  
            darkcorrectionwarning = False
            
        if float(couplinggrayfilter)>0.20:
            w = QWidget()
            QMessageBox.critical(w, "Warnung: Kopplungseffizienz schlecht)", "Datei:\n%s\n\nKopplungsfilter zu gross: Filter %.3f\nLeistungsnormierung moeglicherweise aufgrund unvollstaendiger Auskopplung fehlerhaft." % (f, couplinggrayfilter))
            w.show()
            w.destroy()
            
        if exposuretime_unit != "ms":
            w = QWidget()
            QMessageBox.critical(w, "Fehler bei Auslesen der Header-Daten", "Einheit der Belichtungszeit ist nicht 'ms'. Bitte Entwickler kontaktieren.")
            w.show()
            w.destroy()
       
        pd_voltage = couplingvalue*float(couplinggrayfilter) - pd_offset[reader] #in mV
        #couplingvalue has to be renormalized, because the zeptoreaders only take the nominal grayfilter value into account
        pd_voltage_during_exposure = pd_voltage * transmission[reader][grayfilter] / transmission[reader][couplinggrayfilter]
        correction_value = exposuretime/1E3*pd_voltage_during_exposure #1E3 for ms to s. 
        # Normalize to 1s, PD voltage at coupling time (mV), and transmission ratio of coupling to current grayfilter
        # typical values: 0.1 (s) * 1000 (mV) * 0.001 / 0.05 = ca. 2
        print ("Belzeit: %d %s, Koppl.filter: %.3f, Kopplung: %g, Graufilter: %.3f,\nPhotodiode korrigiert(mV)*Belichtungszeit(s): %.3g" % 
               (exposuretime, exposuretime_unit, couplinggrayfilter, couplingvalue, grayfilter, correction_value))
        if normalize_to_header_data:
            im = im / correction_value

        if field_counter in special_file:
            starting_x=int(starting_x_user[cord_counter])
            starting_y=int(starting_y_user[cord_counter])
            cord_counter+=1    
        else:
            image_size=im.shape
            backgroud_mean=np.mean(im)
            backgroud_std=np.std(im)
            mask_signal=backgroud_mean+3*backgroud_std
            bool_reference_array = np.zeros(image_size)
            starting_x = int(image_size[1]/2)- 120  #40  
            starting_y = int(image_size[0]/2)- 90   #40
            #generate pattern for cross correlation.
            for i in range(0,4):
                for j in range(0,col_num):
                    if i==1:
                        continue
                    else:
                        bool_reference_array[starting_y+i*row_pitch_px,starting_x+int(j*col_pitch_px)]=mask_signal
            #Gaussian blur the pattern to mimic the real image
            #reference_array=gaussian_filter(bool_reference_array,sigma=1)
            #use fft convolution to find the starting coordinates
            im_for_convolve=im-gaussian_filter(im, 30) #subtract gaussian average of image from image --> bg removal before convolve
            result = fftconvolve(im_for_convolve,bool_reference_array[::-1,::-1],mode='same')
            result = gaussian_filter(result, 3) #gaussian filtering to make detection more robust
            i,j = np.unravel_index(np.argmax(result), result.shape)
            #shift to match original image coordinates
            x_shift = j - int(image_size[1]/2)
            y_shift = i - int(image_size[0]/2)
            
            #diagnostic convolve result show
            if debug:
                fig2=plt.figure()                   
                ax2=plt.gca()
                implot = ax2.imshow(result,cmap='CMRmap')
                plt.show()
            
            starting_x=starting_x+x_shift
            starting_y=starting_y+y_shift
            
        print ("X,Y Start: %d, %d px" % (starting_x, starting_y)) #diagnostic

        def print_on_image(mologram_type,lr_coords,tb_coords,signal,signal_radius,index,filename_str):
            global logscale
            ax1 = fig1.add_subplot(6,1,index)
            def sizeof_fmt(num, suffix=''):
                for unit in ['','k','M','G','T','P']: 
                    #kilo, Mega, Giga, Tera, Peta, Exa (ganz unten)
                    if abs(num) < 1000.0:
                        if abs(num) < 100.0:
                            return "%3.1f%s%s" % (num, unit, suffix)
                        else:
                            return "%3.0f%s%s" % (num, unit, suffix) #werte ueber 100 werden ohne nachkommast. ausgegeben
                    num /= 1000.0
                return "%.1f%s%s" % (num, 'E', suffix)
    
            if logscale:
                #+1 um bei komplett genulltem Hintergrund weisses Bild zu vermeiden
                im_on_ax=ax1.imshow(im+1,cmap='CMRmap', norm=LogNorm(), interpolation="none", vmin=np.percentile(im+1,5))
                cb=fig1.colorbar(im_on_ax, ax=ax1, shrink=0.66, ticks = LogLocator(subs=(1.0,2.0,5.0) ))
                cb.formatter = LogFormatter(base=10, labelOnlyBase=False)
                cb.update_ticks()
            else:
                im_on_ax=ax1.imshow(im,cmap='CMRmap', interpolation="none")
                cb=fig1.colorbar(im_on_ax, ax=ax1, shrink=0.66)
            #ax1.set_axis_off()
            ax1.plot(0, 0, "-", alpha=0, label="%s\nRed Xs mark spots without signal\nDashed circles mark bg ROIs" % f)
            ax1.legend(bbox_to_anchor=(0.012, 0.98), bbox_transform=ax1.transAxes, fontsize=6, loc="upper left", markerscale=0)
            
            i_counter=0
            for i in range(0,4):
                j_counter=0
                for j in range(0,col_num):
                    if i==1:
                        continue
                    else:
                        #intensity_string=str(int(round(signal[i_counter][j_counter]/1000)))+'k'
                        intensity_string= sizeof_fmt(signal[i_counter][j_counter])
                        circ_x=lr_coords[i_counter][j_counter]
                        circ_y=tb_coords[i_counter][j_counter]
                        text_x = circ_x - 15
                        text_y = circ_y - 15 

                        if signal[i_counter][j_counter]==0:
                            ax1.scatter(circ_x, circ_y, marker="X", color="darkred")
                        else:
                            ax1.text(text_x, text_y, intensity_string, color='white',fontsize=6)
                            ax1.add_patch(patches.Circle((circ_x, circ_y), signal_radius[i_counter][j_counter], lw=0.5,edgecolor='cyan',facecolor='none'))
                            ax1.add_patch(patches.Circle((circ_x, circ_y), 150/pixelsize, lw=0.35, ls="--", 
                                                         edgecolor='darkred',facecolor='none'))
                            ax1.add_patch(patches.Circle((circ_x, circ_y), 250/pixelsize, lw=0.35, ls="--", 
                                                         edgecolor='darkred',facecolor='none'))
                        j_counter+=1
                if i!=1:
                    i_counter+=1
                    
        def print_only_image(index,filename_str):
            global logscale
            ax1 = fig1.add_subplot(6,1,index)
            if logscale:
                #+1 um bei komplett genulltem Hintergrund weisses Bild zu vermeiden
                im_on_ax=ax1.imshow(im+1,cmap='CMRmap', norm=LogNorm(), interpolation="none", vmin=np.percentile(im+1,5))
                cb=fig1.colorbar(im_on_ax, ax=ax1, shrink=0.66, ticks = LogLocator(subs=(1.0,2.0,5.0) ))
                cb.formatter = LogFormatter(base=10, labelOnlyBase=False)
                cb.update_ticks()
            else:
                im_on_ax=ax1.imshow(im,cmap='CMRmap', interpolation="none") 
                cb=fig1.colorbar(im_on_ax, ax=ax1, shrink=0.66)
            #ax1.set_axis_off()
            ax1.plot(0, 0, "-", alpha=0, label="%s\nFocus detection failed." % f)
            ax1.legend(bbox_to_anchor=(0.012, 0.98), bbox_transform=ax1.transAxes, fontsize=6, loc="upper left", markerscale=0)                    
                    
        if (starting_x < 10) or \
            ((starting_x + (col_num-1)*col_pitch_px) > im.shape[1] - 10) or \
            ((starting_y) < 10) or \
            ((starting_x + 3*row_pitch_px) > im.shape[0] - 10):
                print ("Fokussuche fuer Feld %s fehlgeschlagen. Bitte manuelle Fokusbestimmung verwenden." % f[0])
                field_counter+=1
                print_only_image(field_counter, f)
                save_comment.append("%s; Fokussuche fuer Feld %s fehlgeschlagen. Feld uebersprungen." % (f, f[0]))
                continue
        

        intensity_result=find_intensity(im, mologram_type, starting_x, starting_y, 
                                        pixelsize, max_radius, algorithm=algo)

        lr_coords=intensity_result['lr_coords']
        tb_coords=intensity_result['tb_coords']
        signal= intensity_result['signal']
        signal_radius = intensity_result['signal_radius']
        background=intensity_result['background']
        background_sigma=intensity_result['background_sigma'] 
        
        save_lr_coords.append(lr_coords)
        save_tb_coords.append(tb_coords)
        save_signal.append(signal)
        save_signal_radius.append(signal_radius)
        save_background.append(background)
        save_background_sigma.append(background_sigma)
        weighted_signal = exponential_weight(signal)
        save_weighted_signal.append(weighted_signal)
        save_comment.append("%s; Belzeit: %d %s; Kopplung: %g; Graufilter (Kopplung): %.3f; Graufilter (Aufnahme): %.3f; Photodiode korrigiert (mV)*Exposure (s): %g" 
                            % (f,exposuretime,exposuretime_unit, couplingvalue, couplinggrayfilter, grayfilter, correction_value))
        field_counter+=1
        fields.append(f[0])
        print_on_image(mologram_type,lr_coords,tb_coords,signal,signal_radius,field_counter,f)

    save_path=str(save_path)
    chipSpec=str(chipSpec)
    fig1.tight_layout()
    fig1.savefig(save_path+'/'+chipSpec+("_logscale" if logscale else '')+'.jpg',bbox_inches='tight',dpi=200,pad_inches=0.1)
    plt.close('all')
        
    save_signal_radius = [radius_in_pixels*pixelsize for radius_in_pixels in save_signal_radius] #skaliere mit pixelradius in micron
    #save as csv file. Bad field is taken into account
    #print save_path,chipSpec
    save_comment.append("Script version: %s" % swversion)
    save_comment.append("Algorithm: %s" % algo)
    save_comment.append("Maximum signal radius (px): %d" % max_radius)
    save_comment.append("ZeptoREADER Software - dark current subtraction: %s" % darkcurrentsubtraction)

    save_csv(save_path,chipSpec, mologram_type, save_signal, save_weighted_signal, save_signal_radius, save_background,save_background_sigma,
             save_lr_coords,save_tb_coords,
             fields,bad_file,save_comment)

main()

        #print_on_image('Reference Mologram',signal)
