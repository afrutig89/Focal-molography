import csv
try:
    from PyQt5.QtCore import *
    from PyQt5.QtWidgets import QApplication, QWidget, QFormLayout, QPushButton, QLineEdit, QCheckBox, QFileDialog, QInputDialog, QMessageBox, QLabel
except:
    from PyQt4.QtCore import *
    from PyQt4.QtGui import QApplication, QWidget, QFormLayout, QPushButton, QLineEdit, QCheckBox, QFileDialog, QInputDialog, QMessageBox, QLabel


def save_csv(save_path,chipSpec,mologram_type,save_signal,save_weighted_signal,save_signal_radius,save_background,save_background_sigma,
             save_lr_coords,save_tb_coords,
             fields,bad_file=0,comment=''):
    #added signal_radius and background_sigma to saving procedure
    returns_path = save_path
    Field=fields
    #vorher ['B','C','D','E','F','G']
    #Field=Field[0:number_of_field]
    Line=['1','3','4']
    if mologram_type=='Reference Mologram':
        Molo=[str(w)for w in list(range(1,5+1))]
        offset=15
    else:
        Molo=[str(w)for w in list(range(1,10+1))]
        offset=30
    try:
        with open(returns_path+'/'+chipSpec+'.csv', 'w', newline='') as fp: #was ,'wb' in py2, has to be 'w' in py3
            a = csv.writer(fp)
            a.writerow(["sep=,",])
            a.writerow(["Field","Row","Mologram","Signal","Signal Exponented Weighted","Signal Radius (micron)","Background","Background_Sigma",
                        "x","y"])
            F_counter=0
            skip=0
            for F in Field:
                L_counter=0
                for L in Line:
                    M_counter=0
                    for M in Molo:
                        if F_counter in bad_file:
                            a.writerow([F+" is a bad field"])
                            skip+=1
                            continue
                        else:
                            a.writerow([F,L,M,save_signal[int(F_counter-skip/offset)][L_counter][M_counter],
                                save_weighted_signal[int(F_counter-skip/offset)][L_counter][M_counter],
                                "%.2f" % save_signal_radius[int(F_counter-skip/offset)][L_counter][M_counter],
                                save_background[int(F_counter-skip/offset)][L_counter][M_counter], 
                                save_background_sigma[int(F_counter-skip/offset)][L_counter][M_counter],
                                save_lr_coords[int(F_counter-skip/offset)][L_counter][M_counter],
                                save_tb_coords[int(F_counter-skip/offset)][L_counter][M_counter]
                                ])
                        M_counter+=1
                    L_counter+=1
                F_counter+=1
            if comment!='':
                for line in comment:
                    a.writerow([line])
    except IOError:
        w = QWidget()
        QMessageBox.critical(w, "I/O Fehler", "csv-Ergebnisdatei kann nicht geoeffnet werden. Eventuell ist die Datei noch in einem anderen Programm geoeffnet?")
        w.show()
        w.destroy()