"""
This module takes in 
1.an image
2.mologram type: either Full Mologram or Reference Mologram. Will generate pattern differently
3.the x coordinate of the top left mologram
4.the y coordinate of the top left mologram
5.the pixel size.
and calculate the intensity of each mologram using the expanding ROI algorithm.
This module is called by barscan_evaluator.py but can be use independently.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter

global array
debug_signal_search = False

def find_intensity(im,mologram_type,starting_x,starting_y,pixelsize,max_radius, algorithm="expandingRoi"):
    '''
    algorithm: "expandingRoi" or "signalMinusBg"
    '''
    global array
                
    roi_size=7
    row_pitch_px=40  #zeilenabstand ist 500 um, 40 px
    if mologram_type == 'Reference Mologram':
        col_pitch_px = 65
        col_num = 5
    else:
        col_pitch_px = 32.5
        col_num = 10

    def gen_mask(input_mat,a,b,r_o,r_in = 0):
        global array
        # generate the doughtnut shaped mask. By default, the generated mask does not have a hole
        y,x = np.ogrid[-b:input_mat.shape[0]-b, -a:input_mat.shape[1]-a]
        if r_in == 0:
            mask = x*x + y*y < r_o*r_o
        else:
            mask = (x*x +y*y < r_o*r_o) & (x*x + y*y >= r_in*r_in)
            #was mask = (x*x +y*y <= r_o*r_o) & (x*x + y*y > r_in*r_in) - radius 1 wurde nie erreicht!
        array = np.zeros(im.shape)
        array[mask] = 1    
        # 2018-04-17 program fails if bg is 0 everywhere, because it only returns ring_mask!=0
        #ring_mask = np.multiply(array,input_mat)
        #return ring_mask[ring_mask!=0]
        return input_mat[np.nonzero(array)]
    
    def get_ring(input_mat,a,b,r):
        global array
        # generate the ring shaped mask.
        y,x = np.ogrid[-b:input_mat.shape[0]-b, -a:input_mat.shape[1]-a]
        mask = np.isclose(x*x + y*y, r*r) #float comparisions fail otherwise
        array = np.zeros(im.shape)
        array[mask] = 1
        return input_mat[np.nonzero(array)]

    def sizeof_fmt(num, suffix=''):
        for unit in ['','k','M','G','T','P']: 
            #kilo, Mega, Giga, Tera, Peta, Exa (weiter unten)
            if abs(num) < 1000.0:
                if abs(num) < 100.0:
                    return "%3.1f%s%s" % (num, unit, suffix)
                else:
                    return "%3.0f%s%s" % (num, unit, suffix) #werte ueber 100 werden ohne nachkommast. ausgegeben
            num /= 1000.0
        return "%.1f%s%s" % (num, 'E', suffix)
    
    im_for_centering = np.asfarray(im)-gaussian_filter(im, 30)
    signal=[]
    signal_radius=[]
    background=[]
    background_sigma=[]
    lr_coords=[] #leftright
    tb_coords=[] #topbottom
            
    for i in range(0,4):
        if i==1:
            # skip the second row because there is nothing
            continue
        for j in range(0,col_num):
            # Cut out the mologram regions based on the location of the top left mologram.
            # roi is exclusively used to determine center of mass for better focus location
            # all other data is taken directly from "im"
            roi = im_for_centering[(starting_y+i*row_pitch_px-roi_size):(starting_y+i*row_pitch_px+roi_size),
                    (starting_x+int(j*col_pitch_px)-roi_size):(starting_x+int(j*col_pitch_px)+roi_size)].copy()
            
            #arg des ersten Vorkommens des Maximums bestimmen, mit unravel_index auf Tupel y,x umwandeln 
            #--> Koordinate des Maximums relativ zur roi
            y_cord,x_cord = np.unravel_index(np.argmax(roi), roi.shape)
            if np.isnan(x_cord):
                x_cord = roi.shape[1]/2
                y_cord = roi.shape[0]/2
                print ("No coordinates found, defaulting to center of ROI")
            #Koordinate relativ zum Ursprungsbild berechnen
            x_cord=int(x_cord+starting_x+int(j*col_pitch_px)-roi_size)
            y_cord=int(y_cord+starting_y+i*row_pitch_px-roi_size)

            #hintergrund in kreisring bestimmen
            ring_mask = gen_mask(im,x_cord,y_cord,250/pixelsize,150/pixelsize)
            mean_bg=np.mean(ring_mask)
            std_bg=np.std(ring_mask)
            
            x_, y_ = np.mgrid[-20:20, -20:20]
            r_ = np.sqrt(x_**2 + y_**2)
            list_of_radii = np.unique(r_)
            
            if algorithm == "expandingRoi":
                #perform the expanding ROI algorithm
                if debug_signal_search:
                    plt.figure()
                    plt.imshow(im, cmap="Greys_r")
                    plt.colorbar()
                    plt.scatter(x_cord, y_cord)
                #erster Testpixel - Zentrum
                test=im[y_cord,x_cord]-mean_bg
                flag=True
                counter=0
                signal_area=0
                pixelcount=1
                while(flag==True):
                    if (test > 3*std_bg*pixelcount) and ((list_of_radii[counter]<=max_radius) or max_radius==0):
                    # if the contribution of the new layer is larger than 3*std*pixelcount, consider it as a successful addition to current signal
                        counter+=1
                        signal_area += test #testintensitaet nach erfolgreichem vergleich mit stdabw. zu signal addieren
                        test_mask = get_ring(im,x_cord,y_cord,list_of_radii[counter]) #signal des naechsten rings berechnen
                        if debug_signal_search:
                            print("Current radius: %.2f px" % list_of_radii[counter])
                            print(test_mask)
                        test_mask=np.subtract(test_mask,mean_bg) #hintergrund pixelweise abziehen
                        pixelcount=len(test_mask)
                        test=np.sum(test_mask)
                        if debug_signal_search:
                            #create rgba array for overlay from mask array data
                            overlay=np.asfarray(np.dstack((np.ones(array.shape),
                                                           np.zeros(array.shape),
                                                           np.zeros(array.shape),
                                                           array*0.3)))
                            plt.imshow(overlay)
                    else:
                        flag=False
                if debug_signal_search:                        
                    plt.xlim((x_cord-roi_size,x_cord+roi_size))
                    plt.ylim((y_cord+roi_size,y_cord-roi_size))
                    plt.show()
                signal_radius.append(list_of_radii[counter])
                    
            elif algorithm=="signalMinusBg":
                counter=0
                signal_temp =  gen_mask(im,x_cord,y_cord,max_radius) #get signal in circle of certain radius
                signal_temp -= mean_bg #subtract mean of bg
                signal_area = np.sum(signal_temp)
                signal_radius.append(max_radius)
                
            lr_coords.append(x_cord)
            tb_coords.append(y_cord)
            signal.append(signal_area)
            
            background.append(mean_bg)
            background_sigma.append(std_bg)
            #print ("x:%3d,y:%3d,r:%5.2f, Signal: %8.3g, Bg: %8.3g, Bg RMS: %8.3g" % (x_cord,y_cord,list_of_radii[counter],signal_area,mean_bg,std_bg))
                
    lr_coords=np.array(lr_coords)
    lr_coords=lr_coords.reshape((3,col_num))
    tb_coords=np.array(tb_coords)
    tb_coords=tb_coords.reshape((3,col_num))
    print('Row 1: '+' '.join('{:>5}'.format(sizeof_fmt(k)) for k in signal[:10])
        +' Mean:{:>6}'.format(sizeof_fmt(np.mean(signal[:10]))) )
    print('Row 3: '+' '.join('{:>5}'.format(sizeof_fmt(k)) for k in signal[10:20])
        +' Mean:{:>6}'.format(sizeof_fmt(np.mean(signal[10:20]))) )
    print('Row 4: '+' '.join('{:>5}'.format(sizeof_fmt(k)) for k in signal[20:])
        +' Mean:{:>6}'.format(sizeof_fmt(np.mean(signal[20:]))) )
    signal=np.array(signal)
    signal=signal.reshape((3,col_num))    
    signal_radius=np.array(signal_radius)
    signal_radius=signal_radius.reshape((3,col_num))
    background=np.array(background)            
    background=background.reshape((3,col_num))
    background_sigma=np.array(background_sigma)
    background_sigma=background_sigma.reshape((3,col_num))
    return {'lr_coords':lr_coords, 'tb_coords':tb_coords, 'signal':signal, 'signal_radius':signal_radius, 'background':background, 'background_sigma':background_sigma }