'''
This helper method is used to fit an exponential decay function to a dataset and correct each point using exponential decaying function.
Takes in a list, or array
Returns the weighted signal
'''

import numpy as np
from scipy.optimize import curve_fit


def exponential_weight(input):
	def func(t,a,b):
		return a*np.exp(-b*t)

	exp_coef=[]
	output=[]
	for list in input:
		#x=np.linspace(0,999,len(list))
		x=np.linspace(20,320,len(input[0]))
		y=list
		try:
			popt,pcov=curve_fit(func,x,y,[10000,-0.004],maxfev=10000)
			output.append(np.multiply(list,np.exp(np.multiply(popt[1],x))))
		except RuntimeError:
			pass
		
	return output