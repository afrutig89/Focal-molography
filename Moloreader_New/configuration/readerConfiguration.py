# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

# This file should specify the reader configuration
# Event loop

from SharedLibraries.Database.variableContainers import VariableContainer

class Configuration(VariableContainer):

	_DB = _conf




class ExperimentConfiguration(Configuration):

	def __init__(self):

		self.MoloReader = 0
		self.chip = 0
		self.gui = 0

class Experiment(object):

	def __init__(self):

		self.experimentConfiguration = 0
		self.description = 0

class ExperimentStack(list):

	def __init__(self):


class MoloReaderConfiguration(Configuration):




class MoloReader(object):

	"""Base class, defines the interface for different readers"""
	def __init__(self):
		super(MoloReader, self).__init__()

		self.axes = 0
		self.camera = 0
		self.settings = 0


if __name__ == '__main__':
    main()