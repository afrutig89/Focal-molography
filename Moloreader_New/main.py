#!/usr/bin/python3

from PyQt4 import QtGui
from GUI.Main_window import MainWindow
import sys

def main():
    # open window
    app = QtGui.QApplication(sys.argv)
    w = MainWindow(app)
    w.show()
    app.exec_()
    del w

# Event loop
if __name__ == '__main__':
    main()