"""
Main window of the Moloreader software
"""


import os
import subprocess
from PyQt4 import QtGui, QtCore
from GUI.Main_widget import MainWidget
import time


class MainWindow(QtGui.QMainWindow):
    def __init__(self,app):
        # Call parent constructor
        super(MainWindow, self).__init__()    

        self.app = app
        
        # Call child constructor
        self.initUI()

        
    def initUI(self):
        # set style
        QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))
        
        # Create widgets
        self.main_widget = MainWidget()
        self.setCentralWidget(self.main_widget)

        # create close window action
        exitAction = QtGui.QAction(QtGui.QIcon('./images/exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        # create calibration action
        calAction = QtGui.QAction(QtGui.QIcon('./images/gear.png'), 'Calibrate', self)
        calAction.setShortcut('Ctrl+K')
        calAction.setStatusTip('Calibrate Setup')

        if not self.main_widget.offline_mode:    
            calAction.triggered.connect(self.main_widget.calib)

        loadImage = QtGui.QAction(QtGui.QIcon('./images/image_file.png'), 'Load image', self)
        loadImage.setShortcut('Ctrl+L')
        loadImage.setStatusTip('Load image')
        loadImage.triggered.connect(self.main_widget.loadImage)

        saveImage = QtGui.QAction(QtGui.QIcon('./images/save.png'), 'Save As...', self)
        saveImage.setShortcut('Ctrl+Shift+S')
        saveImage.setStatusTip('Save (full) image')
        saveImage.triggered.connect(self.main_widget.saveImage)

        showDocumentation = QtGui.QAction(QtGui.QIcon('./images/help.png'), 'Show documentation', self, checkable = True)
        showDocumentation.setStatusTip('Show documentation')
        showDocumentation.setToolTip('<font color=black>Documentation</font>')
        showDocumentation.setShortcut('Ctrl+H')

        showProtocols = QtGui.QAction('Show experiments', self, checkable = True)
        showProtocols.setStatusTip('Show experiments')

        reloadTabs = QtGui.QAction('Reload pages', self)
        reloadTabs.setStatusTip('Reload pages')
        reloadTabs.setShortcut('Ctrl+F5')
        reloadTabs.triggered.connect(self.main_widget.reloadTabs)

        self.offlineModeButton = OfflineModeWidget(self.main_widget)

        restart = QtGui.QAction(QtGui.QIcon('./images/restart.png'), 'Restart', self)
        restart.setStatusTip('Restart program')
        restart.triggered.connect(self.restartGUI)

        # Create a status bar
        self.statusBar()

        # Menu bar
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(calAction)
        fileMenu.addAction(loadImage)
        fileMenu.addAction(saveImage)
        fileMenu.addAction(exitAction)

        fileMenu = menubar.addMenu('&View')
        fileMenu.addAction(showDocumentation)
        fileMenu.addAction(showProtocols)
        fileMenu.addSeparator()
        fileMenu.addAction(reloadTabs)

        fileMenu.triggered.connect(self.main_widget.showTab)

        # Toolbar
        toolbar = self.addToolBar('Toolbar')
        toolbar.addAction(exitAction)
        toolbar.addAction(showDocumentation)
        toolbar.addAction(calAction)
        toolbar.addAction(loadImage)
        toolbar.addAction(saveImage)

        spacer = QtGui.QWidget()
        spacer.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        toolbar.addWidget(spacer)

        toolbar.addWidget(QtGui.QLabel('Offline Mode'))
        toolbar.addWidget(self.offlineModeButton)
        toolbar.addAction(restart)

        # Set Geometry, title, icon and show window
        self.setGeometry(300, 300, 1920, 1080)
        self.setWindowTitle('Focal Molography GUI')
        self.setWindowIcon(QtGui.QIcon('./images/microscope.png'))    
        #self.show()


    def restartGUI(self):
        try:
            self.main_widget.setup.camera.free_all()
            self.main_widget.setup.camera.close()
        except:
            pass

        self.close()

        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(os.path.dirname(abspath))
        mainFile = "{}/main.py".format(dname.replace(' ','\ '))
        subprocess.call("sudo python3 {}".format(mainFile), shell=True)



class OfflineModeWidget(QtGui.QCheckBox):
    def __init__(self, main_widget):
            QtGui.QCheckBox.__init__(self, main_widget)
            self.setStyleSheet("background-color: transparent;\n" + 
                      "color: rgb(0,0,0);\n")

            self.main_widget = main_widget
            #set default check as False 
            self.setChecked(self.main_widget.offline_mode)
            self.setEnabled(True)
            self._enable = True

    #overrite mouse event
    def mousePressEvent(self, *args, **kwargs):
            #tick on and off set here
            if self.isChecked():
                self.setChecked(False)
            else:
                self.setChecked(True)

            self.main_widget.set_offline_mode()
            return QtGui.QCheckBox.mousePressEvent(self, *args, **kwargs)


    def paintEvent(self,event):

            #just setting some size aspects
            self.setMinimumHeight(20)
            self.setMinimumWidth(70)
            self.setMaximumHeight(26)
            self.setMaximumWidth(90)

            painter = QtGui.QPainter()
            painter.begin(self)

            #smooth curves
            painter.setRenderHint(QtGui.QPainter.Antialiasing)

            #for the on off font
            font  = QtGui.QFont()
            font.setFamily("Arial")
            font.setPixelSize(14)
            painter.setFont(font)        

            #change the look for on/off
            if self.isChecked():
                #blue fill
                brush = QtGui.QBrush(QtGui.QColor(140,0,0),style=QtCore.Qt.SolidPattern)
                painter.setBrush(brush)

                #rounded rectangle as a whole
                painter.drawRoundedRect(0,0,self.width()-2,self.height()-2, \
                                   self.height()/2,self.height()/2)

                #white circle/button instead of the tick mark
                brush = QtGui.QBrush(QtGui.QColor(0,0,0),style=QtCore.Qt.SolidPattern)
                painter.setBrush(brush)
                painter.drawEllipse(self.width()-self.height(),0,self.height()-2,self.height()-2)

                #on text
                painter.drawText(self.width()/4,self.height()/1.5, "On")

            else:
                #gray fill
                brush = QtGui.QBrush(QtGui.QColor(150,150,150),style=QtCore.Qt.SolidPattern)
                painter.setBrush(brush)

                #rounded rectangle as a whole
                painter.drawRoundedRect(0,0,self.width()-2,self.height()-2, \
                                   self.height()/2,self.height()/2)

                #white circle/button instead of the tick but in different location
                brush = QtGui.QBrush(QtGui.QColor(0,0,0),style=QtCore.Qt.SolidPattern)
                painter.setBrush(brush)
                painter.drawEllipse(0,0,self.height()-2,self.height()-2)

                #off text
                painter.drawText(self.width()/2,self.height()/1.5, "Off")