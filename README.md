Simulation Framework for diffractometric biosensors
========

This is a framework to simulate diffractometric biosensors on planar asymmetric waveguides. 


Documentation
-------------
The documentation can be found here.