from dataBaseOperations import createDB, loadDB, saveDB

# remark to myself: Apply section 8.11 in the python cookbook to reimplement this datastructures

__all__ = ['Configuration','VariableContainer','VariableContainerList']

class VariableContainer(object):
    """An abstract class to store database information and key, value pair of parameters to be stored in the database"""

    _dB = dict()
    _table_name = ''

    def __init__(self):

        super(VariableContainer,self).__init__()

        self._values = dict() # dictionary of variable names (keys) and variable values (values) should all be initialized to 0

        self._save = dict() # dictionary to save whether or not to save the variables in the database.

        for key in self._DB.keys():

            if self._DB[key] == 'real' or self._DB[key] == 'complex'or self._DB[key] == 'array' or self._DB[key] == 'integer':
                self._values[key] = 0

            # skip because the primary key is set automatically in the database.
            if self._DB[key] == 'integer primary key AUTOINCREMENT':
                continue

            if self._DB[key] == 'text':
                self._values[key] = ''


    def setValues(self,values):
        """Values is a dictionary of key value pairs"""

        for key in self._values.keys():
            if key in values.keys():
                self._values[key] = values[key]


    def returnDBPar(self):

        return self._DB

    def getValues(self):

        return self._values

    def createDB(self, databasefile):

        self.databasefile = databasefile

        createDB(databasefile,self._table_name,self.returnDBPar())

    def saveDB(self):
        if not hasattr(self, 'databasefile'):
            raise Exception('No database file associated with Variable container.')

        saveDB(self.databasefile, self._table_name, self.getValues())

    def loadDB(self,databasefile):

        self.databasefile = databasefile
        print(loadDB(self.databasefile, self._table_name))
        self.setValues(loadDB(self.databasefile, self._table_name))

    def deleteTable(self):

        deleteTable(self.databasefile, self._table_name)


class Configuration(VariableContainer):
    """A class in order to store different experimental configuration can be chained, for each configuration object a new table is created in the database
        in order to easily find the information."""

    def __init__(self, configuration, table_name):

        super(Configuration,self).__init__()

        for key in self._DB.keys():

            if self._DB[key] == 'configuration':

                self._values[key] = key

        if configuration is not None:

            self.setValues(configuration)

        self.table_name = table_name

    def createDB(self,databasefile):

        self.databasefile = databasefile

        _DB = dict(self._DB) # copy dictionary in order not to alter the first one. 

        # to store configuration objects properly (just their strings but the configuration class) also call all child configuration objects.
        for key, value in _DB.items():

            if value == 'configuration':

                _DB[key] = 'text'

                self._values[key].configuration.createDB(databasefile)

        if self.table_name == '':
            raise RuntimeError('No tablename associated with Variable Container class')

        createDB(self.databasefile, self.table_name, _DB)

    def saveDB(self):

        _values = dict(self.getValues()) # copy dictionary in order not to alter the first one. 

        # to store configuration objects properly (their just strings but the configuration class) also call all child configuration objects.
        for key, value in self.returnDBPar().items():

            if value == 'configuration':

                _values[key] = key

                self._values[key].configuration.saveDB(self.databasefile)

        if self.table_name == '':
            raise RuntimeError('No tablename associated with Variable Container class')

        saveDB(self.databasefile, self.table_name, _values)


class VariableContainerList(list):
    """An base class for list of Variable container objects. (Not abstract)"""

    def __init__(self,containerlist=''):

        if containerlist != '':
            self._containerlist = []

            for container in containerlist:
                self._containerlist.append(container)

        else:
            if not hasattr(self,'_containerlist'):
                self._containerlist = list() # A list of Variable Container objects

        super(VariableContainerList,self).__init__()

    def __getitem__(self,key):

        return self._containerlist[key]

    def __setitem__(self,key,item):

        self._containerlist[key] = item

    def __iter__(self):

        self.current_index = 0

        return self

    def next(self):

        if self.current_index + 1 > len(self._containerlist):
            raise StopIteration
        else:
            self.current_index += 1
            return self._containerlist[self.current_index-1]

    def __str__(self):


        return str(type(self))


    def append(self,container):

        self._containerlist.append(container)

    def returnDBPar(self):

        DB = dict()

        for container in self._containerlist:

            DB.update(container.returnDBPar())

        return DB

    def setValues(self,values):

        for container in self._containerlist:

            container.setValues(values)

    def getValues(self):
        """returns a dictionary of all the key value pairs of every container in the container list."""

        values = dict()

        for container in self._containerlist:

            values.update(container.getValues())

        return values

    def createDB(self,databasefile):

        self.databasefile = databasefile

        for container in self._containerlist:

            container.createDB(databasefile)
            

    def saveDB(self):

        if not hasattr(self,'databasefile'):
            raise Exception('No database file associated with Variable container.')

        for container in self._containerlist:

            container.saveDB()

    def loadDB(self, databasefile):

        self.databasefile = databasefile

        for container in self._containerlist:

            container.loadDB(self.databasefile)

    def deleteTable(self):

        for container in self._containerlist:

            container.deleteTable()