# -*- coding: utf-8 -*-
"""
This module provides IO functions in order to store and load data.

:author:   Silvio
:revised:  2016-10-31 09:53:10 sibischo
"""

__author__ = "Silvio Bischof"
__revision__ = "2016-10-31 09:53:10 sibischo"

import os
import sqlite3
import numpy as np
import io
import sys
import pandas as pd


def namestr(obj, namespace):
    """ returns the name of the variables. This function failes if two variables have the same value (both will be listed). It is used for the function 'writeVarsToFile'.

    .. seealso:: writeVarsToFile
    """
    return [name for name in namespace if namespace[name] is obj]

def writeVarsToFile(filename, namespace, *args):
    """ stores variables to a file.


    :param filename: name of the file
    :param namespace: variables stored in the workspace (globals())
    :param *args: all the variables that are saved
    :type filename: string
    :type namespace: dict

    :example:
    writeVarsToFile('example.txt', globals(), var1, var2)

    .. note:: structs and objects cannot be saved with this method

    """
    f = open(filename, 'w')
    for var in args:
        f.write("%s: %s\n" % (namestr(var, namespace),repr(var)))

    f.close()

def checkFolder(folder):
    """ checks if a folder already exists. If not, it is created together with a counter file

    :param folder: folder name
    :type folder: string
    :returns: counter of the plots
    :rtype: int
    """
    if not os.path.exists(folder):
        os.makedirs(folder)
        return 1

    else:
        configFile = open('config.py','r').read()
        ini = configFile.find("counter")+10
        rest = configFile[ini:]
        search_enter = rest.find('\n')
        counter = int(rest[:search_enter])
        return str(counter+1)

def appendValue(filename, value):
    """ appends value of a variable to a file

    :param filename: name of the file
    :param value: value to store
    :type filename: string
    """
    f = open(filename, 'a')
    f.write("%s\n" % value)

    f.close()

def adapt_array(arr):
    """ returns a sqlite3 binary code in order to store an array into a database.

    :param arr: array
    :type arr: ndarray
    :returns: sqlite3 binary text
    """
    out = io.BytesIO()
    np.save(out, arr)
    out.seek(0)
    return sqlite3.Binary(out.read())

def convert_array(text):
    """ returns the corresponding array for a sqlite3 binary code.

    :param text: array
    :type text: ndarray
    :returns: array stored in a database (as binary text)
    :rtype: ndarray
    """
    out = io.BytesIO(text)
    out.seek(0)
    try:
        return np.load(out)
    except:
        return np.array([])


def adapt_complex(c):
    return str(c)

def convert_complex(text):
    return complex(text)

def returnType(var):
    """ returns the type of a variable

    :param var: arbitrary variable
    :returns: type of a variable
    :rtype: string
    """
    if type(var) == str:
        return 'text'
    if type(var) == float \
            or isinstance(var, np.float64):
        return 'real'

    if type(var) == complex \
            or type(var) == np.complex128:
        return 'complex'
    if type(var) == np.ndarray:
        return 'array'
    if type(var) == list:
        return 'list'
    if type(var) == int or type(var) == bool:
        return 'integer'


def writeAsSQL(listOrDict):
    """
    returns a list as a sql-string i.e. braces and brackets are
    replaced by parenthesis. Use listOrDict.keys() if only keys
    are needed.

    :param listOrDict: a list or dictionary of an arbitrary length
    :returns: string where braces and brackets are replaced by
    parenthesis
    :rtype: string
    """
    if type(listOrDict) == dict:
        sqlStr = ''
        for key, value in listOrDict.items():
            sqlStr += ', \'{}\' {}'.format(key, value)

        sqlStr = '({})'.format(sqlStr[2:])

    else:
        sqlStr = str(listOrDict).replace(":","").replace("'","")
        sqlStr = str(listOrDict).replace("["," (").replace("]",")")

    return sqlStr


def createSQLDict(dictionary):
    """function that converts a dictionary into an SQL dictionary"""
    SQLDict = dict()
    for key, value in dictionary.items():
        SQLDict[key] = returnType(value)

    return SQLDict


def createDB(filename, dbname, parameters):
    """ creates a database with given parameter-names

    :param filename: name of the file (suffix .db)
    :param dbname: name of the database in the file
    :param parameters: parameters which are to stored.
    :type filename: string
    :type dbname: string
    :type parameters: list
    """

    # Converts np.array to TEXT when inserting
    sqlite3.register_adapter(np.ndarray, adapt_array)
    sqlite3.register_adapter(complex, adapt_complex)
    # Converts TEXT to np.array when selecting
    sqlite3.register_converter("array", convert_array)
    sqlite3.register_converter("complex", convert_complex)


    conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)

    parametersSQL = writeAsSQL(parameters)
    sql = 'create table if not exists \'%s\' %s' % (dbname, parametersSQL)
    conn.execute(sql)

    conn.commit()
    conn.close()

def saveDB(filename, dbname, variables):
    """This function is not general and only works with the molography simulation tool. saves the variables to a given database. Table needs to be
    already created.

    :param filename: name of the database file (.db)
    :param dbname: name of the table in the database
    :param variables: variables stored during the simulations (globals())
    :type filename: string
    :type dbname: string
    :type parameters: dict
    """

    # Converts np.array to TEXT when inserting
    sqlite3.register_adapter(np.ndarray, adapt_array)
    # Converts TEXT to np.array when selecting
    sqlite3.register_converter("array", convert_array)
    # Converts np.array to TEXT when inserting
    sqlite3.register_adapter(complex, adapt_complex)
    # Converts TEXT to np.array when selecting
    sqlite3.register_converter("complex", convert_complex)

    conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)

    c = conn.cursor()
    c.execute('select * from \'' + dbname + '\'')
    fieldnames = [f[0] for f in c.description if f[0] != 'ID']

    liste = []
    fieldnamesSQL = writeAsSQL(fieldnames)
    sql = 'insert into \'' + dbname + '\'' + fieldnamesSQL + " values ("
    for param in fieldnames:
        sql += "?,"
        if param in variables:
            if returnType(variables.get(param,'')) == 'array':
                liste.append(np.array(variables.get(param,np.array([]))))
            else:
                liste.append(variables.get(param,''))
        else:
            liste.append('')

    sql = sql[0:len(sql)-1] + ")"

    conn.execute(sql, liste)
    conn.commit()
    conn.close()


def loadDB(filename, dbname, selection='*', condition='',returnDataFrame = False):
    """loads the variables in a given database. Specific parameters
    under given conditions can be loaded (e.g. just one parameter
    from the last entry).

    :param filename: name of the database file (.db)
    :param dbname: name of the table in the database
    :param selection: use if only single parameters are to be loaded (eg. "I_back")
    :param condition: add a certain condition (e.g. "WHERE coherentParticles>5000")
    :param returnDataFrame: boolean, if True then not a list of dictionaries is returned but a pandasDataFrame
    :type filename: string
    :type dbname: string
    :type selection: string
    :type condition: string
    :returns: a list of dictionary with the values stored in the database
    in the case of multiple entries. In the case of just one entry, only
    a dictionary is returned an in the case of a single request, only the
    corresponding value is returned.
    :rtype: list of dicts
    """

    sqlite3.register_adapter(np.ndarray, adapt_array)
    sqlite3.register_converter('array', convert_array)

    conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
    c = conn.cursor()
    sql = 'select %s from \'%s\' %s' % (selection, dbname, condition)
    try:
        rows = c.execute(sql)
    except:
        # not able to load data
        print('Not able to load data, SQL: {}'.format(sql))
        return []

    fieldnames = [f[0] for f in c.description]

    entries = []
    for row in rows:

        if len(row) == 0:
            print('no such entry found')
            return

        else:
            i=0
            entry = dict()
            for param in fieldnames:
                entry[param] = row[i]
                i += 1

            entries.append(entry)

    conn.close()
    if len(entries) == 1:
        if len(entries[0]) == 1:
            # return value if only one parameter
            return entries[0].get(entries[0].keys()[0])
        else:
            # return dictionary if only one row
            return entries[0]
    else:
        # return list of dictionaries

        if returnDataFrame:

            entries = pd.DataFrame.from_records(entries)
            return entries

        else:

            return entries



def addColumns(filename, dbname, columnName, values):
    """ adds a column in an existing database

    :param filename: name of the file (suffix .db)
    :param dbname: name of the database in the file
    :param columnName: name of the column that should be added
    :param values: values for the entries, either an array or a single value that is used for all entries
    :type filename: string
    :type dbname: string
    :type columnName: string
    :type values: list
    """

    sqlite3.register_adapter(np.ndarray, adapt_array)
    sqlite3.register_converter('array', convert_array)

    conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
    c = conn.cursor()
    rows = c.execute('select * from \'' + dbname + '\'')
    if rows > 1 and (type(values) == 'np.array' or type(values) == 'list'):
        # add values to each row
        c.execute("alter table {tn} add column '{cn}' {ct}"\
        .format(tn=dbname, cn=columnName, ct=returnType(values)))
        c.executemany('update ' + dbname + ' set ' + columnName + ' = ?'\
            ,((val,) for val in values))

    else:
        c.execute("alter table {tn} add column '{cn}' {ct} DEFAULT '{df}'"\
        .format(tn=dbname, cn=columnName, ct=returnType(values), df=values))

    conn.commit()
    conn.close()


def deleteColumn(filename, dbname, columnName):
    """ deletes a column from an existing table
    Revise!

    :param filename: name of the file (suffix .db)
    :param dbname: name of the database in the file
    :param columnName: name of the column that should be added
    :type filename: string
    :type dbname: string
    :type columnName: string
    """

    # load database settings (type array defined)
    sqlite3.register_adapter(np.ndarray, adapt_array)
    sqlite3.register_converter('array', convert_array)

    conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
    c = conn.cursor()

    oldDict = loadDB(filename, dbname)
    if type(oldDict) == list:
        # make dictionary if multiple entries
        oldDict = oldDict[0]

    newDict = oldDict.pop(columnName, None)

    # save column names of the selected table in a list
    fieldnames = newDict.keys()
    for param in fieldnames:
        newDict[param] = returnType(newDict[param])

    dictSQL = writeAsSQL(newDict)
    fieldnamesSQL = writeAsSQL(fieldnames)

    # rename table
    c.execute("alter table %s rename to %s_old" % (dbname, dbname))
    # create new table with the new columns
    c.execute('create table %s %s' % (dbname, dictSQL))
    # copy values from the old table
    sql = "insert into %s (%s)" % (dbname, fieldnamesSQL)
    sql += " select %s" % fieldnamesSQL
    sql += " from %s_old" % dbname
    c.execute(sql)

    # delete old table
    c.execute("drop table " + dbname + "_old")

    # execute sql command
    conn.commit()
    conn.close()

def deleteRow(filename, dbname, condition):
    """ deletes a row from an existing table

    :param filename: name of the file (suffix .db)
    :param dbname: name of the database in the file
    :param columnName: name of the column that should be added
    :type filename: string
    :type dbname: string
    :type columnName: string
    """

    # load database settings (type array defined)
    sqlite3.register_adapter(np.ndarray, adapt_array)
    sqlite3.register_converter('array', convert_array)

    conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
    c = conn.cursor()
    sql = 'delete from \'' + dbname + '\' where ' + condition
    c.execute(sql)

    # execute sql command
    conn.commit()
    conn.close()


def formGroup(dictionaries, *args):
    """
    Groups dictionaries according to the arguments given.
    """
    groups = []
    groups.append([dictionaries[0]])

    for i in range(len(dictionaries)):
        # for all items in the list
        added = False
        for group in groups:
            # for each group in the list groups
            for arg in args:
                if arg not in dir(group[0]) or \
                        arg not in dir(dictionaries[i]):
                    # check if argument is in dictionaries
                    continue
            if (sum([group[0][arg] == dictionaries[i][arg] for arg in args]) == len(args)):
                group.append(dictionaries[i])
                added = True
                break
        if not added:
            groups.append([dictionaries[i]])

    return groups

def getTableNames(filename):

    conn = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
    res = conn.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = []
    for name in res:
        tables.append(name[0])

    conn.close()
    return tables

if __name__ == '__main__':

    loadDB('writeLogFileTestFiles/data.db', 'results',returnDataFrame = True)
