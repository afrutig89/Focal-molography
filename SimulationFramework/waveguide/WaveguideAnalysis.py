"""
WaveguideAnalysis class. Only works for Dielectric slab waveguides, TE mode)

:author:       Pierre-Louis Ronat
:revised:      10-Nov-2017
:reference:    Marcuse, Kogelnik, Tamir
"""
import sys
sys.path.append("../")
import numpy as np
from numpy import *
from scipy.optimize import minimize
from scipy import integrate
import auxiliaries.constants as constants
from materials.materials import IsotropicMaterial
import cmath
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from waveguide import Waveguide
from waveguide import SlabWaveguide


class WaveguideAnalysis(SlabWaveguide,IsotropicMaterial):
    """
    This is a class that calculates the E field intensity at a given point, effective refractive index, variation of effective refractive indexes (sensitivity), and other possible properties that depends on the thickness of the waveguide.
    x is the propagation direction of the mode and y is the in plane coordinate and z the out of plane coordinate.
    
    :params: waveguide
    
    """
    def __init__(self, name, n_s, n_f, n_c, d_f, polarization, inputPower, wavelength, attenuationConstant = 0, mode = 0, x_0 = 0,media=0):
        """
        """
        Waveguide.__init__(self, name = name,
            n_c=n_c, thickness = d_f, polarization = polarization,
            inputPower = inputPower, wavelength=wavelength)
        # Try catch because materials database migth not exist but should be checked as default in order not to break compatibility.
        try:
            IsotropicMaterial.__init__(self, name, wavelength)
        except:
            IsotropicMaterial.__init__(self, name, wavelength,n_f)

        self.n_s = n_s
        self.n_f = n_f
        self.d_f = d_f
        self.x_0 = x_0
        self.m = mode   # note: only zeroth order implemented
        self.attenuationConstant = attenuationConstant  # in Np/m

        # sehr unsuber, redo
        self.calcPropagationConstants(wavelength)
        self.calcEffThickness(wavelength)


    def plotIntensity(self,index,d_cutoff,zpoint=0,saveFigure=False):
        """The function calculates the intensity at a given zpoint (default: surface, zpoint = 0) for index iterations, starting for a thickness of value d_cutoff. The zpoint is given in nm.
        """
        zpointm = zpoint * 1e-9
        Intensities = []
        Thicknesses = []

        flag = False
        span = 700e-9
        for i in range(0, index):

            d_f = d_cutoff + i*1e-9;
            if zpointm > abs(span):
                print("Error: zpoint is greater than dimension span.")
                return
            waveguide = SlabWaveguide(
                        name = self.name, n_s = self.n_s, n_f = self.n_f,
                        n_c = self.n_c, d_f = d_f, polarization = 'TE',
                        inputPower = effectivePowerperUnitLength, wavelength = self.wavelength,
                        attenuationConstant = attenuationConstant,
                        mode = 0)

            # DEBUG TOOL
            # attrs = vars(waveguide)
            # print("Object attributes")
            # print ', '.join("%s: %s" % item for item in attrs.items())

            z = np.linspace(-span, span, 1001)
            y = np.zeros(len(z))
            xx = np.zeros(len(z))
            # Searching the index for the z index
            scale = (span)/500
            offset = 500
            cursor = round(offset + zpointm/scale)

            E_y = waveguide.E_y(self.wavelength,xx[cursor],y[cursor],z[cursor])

            Intensity = 1/2.*abs(self.n_f)*abs(E_y*np.conj(E_y))/constants.Z_w

            Intensities.append(Intensity)
            Thicknesses.append(d_f)


        Thicknesses[:] = [x * 1e9 for x in Thicknesses]
        Intensities[:] = [x / max(Intensities) for x in Intensities]
        index_max = max(xrange(len(Intensities)), key=Intensities.__getitem__) + round(d_cutoff*1e9)
        info = 'Max intensity value at = ' + repr(index_max) + ' nm'
        print(info)
        fig = plt.figure(1,figsize = (7,7),facecolor='white')
        ax = plt.subplot(1,1,1)
        p1 = ax.plot(Thicknesses,Intensities,'-r')
        plt.title("Intensity at %s nm for %s iterations" %(zpoint,index))
        plt.xlabel('Thickness [nm]')
        plt.ylabel('Normalized Intensity [-]')
        g1 = ax.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
        g2 = ax.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
        ax.minorticks_on()

        if saveFigure != False:
            fig.savefig('../plots/' + 'Test' + self.polarization + '.png', dpi = 600, format = 'png')
        else:
            plt.show()


    def plotSensitivity(self,index,d_cutoff,saveFigure=False):
        """The function calculates the dNeff / dncover for index iterations, starting for a thickness of value d_cutoff. The zpoint is given in nm.
        """
        Sensitivities = []
        Thicknesses = []
        for i in range(0, index):
            d_f = d_cutoff + i*1e-9;
            waveguide = SlabWaveguide(
                        name = self.name, n_s = self.n_s, n_f = self.n_f,
                        n_c = self.n_c, d_f = d_f, polarization = 'TE',
                        inputPower = effectivePowerperUnitLength, wavelength = self.wavelength,
                        attenuationConstant = attenuationConstant,
                        mode = 0)
            Sensitivities.append(waveguide.calcSensitivity())
            Thicknesses.append(d_f)

        Thicknesses[:] = [x * 1e9 for x in Thicknesses]
        index_max = max(xrange(len(Sensitivities)), key=Sensitivities.__getitem__) + round(d_cutoff*1e9)
        info = 'Max sensitivity value at = ' + repr(index_max) + ' nm'
        print(info)
        fig = plt.figure(1,figsize = (7,7),facecolor='white')
        ax = plt.subplot(1,1,1)
        p1 = ax.plot(Thicknesses,Sensitivities,'-r')
        plt.title("Sensitivity analysis for %s iterations" %(index))
        plt.xlabel('Thickness [nm]')
        plt.ylabel('Normalized dN/dnc [-]')
        g1 = ax.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
        g2 = ax.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
        ax.minorticks_on()

        if saveFigure != False:
            fig.savefig('../plots/' + 'Test' + self.polarization + '.png', dpi = 600, format = 'png')
        else:
            plt.show()



    def Dispersion(self, configuration = None, display = True, saveFigure=False):
        """ Dispersion relation: waveguide thickness vs effective refractive index.
            The function draws the curve for TE and TM mode.
            Reference: Integrated Optics, Tamir, 2nd Corrected and Updated Edition.
            Equation: 2.1.16-2.1.21 + 2.1.25
        """
        k = 2*np.pi/self.wavelength
        nf = self.n_f
        ns = self.n_s
        nc = self.n_c
        N = linspace(self.n_s,self.n_f,1000)
        b = (N**2-ns**2)/(nf**2-ns**2)
        aTE = (ns**2-nc**2)/(nf**2-ns**2)
        aTM = (nf**4/nc**4)*((ns**2-nc**2)/(nf**2-ns**2))
        np.warnings.filterwarnings('ignore')    #removing the 0 division warning
        F0TE = (np.arctan(np.sqrt(b/(1-b)))+np.arctan(np.sqrt((b+aTE)/(1-b))))/(sqrt(1-b)*k*sqrt(nf**2-ns**2))
        F0TM = (np.arctan(np.sqrt(b/(1-b)))+np.arctan(np.sqrt((b+aTM)/(1-b))))/(sqrt(1-b)*k*sqrt(nf**2-ns**2))
        cutoffF0TE = np.arctan(np.sqrt(aTE))/(k*sqrt(nf**2-ns**2))
        cutoffF0TM = np.arctan(np.sqrt(aTM))/(k*sqrt(nf**2-ns**2))
        print("Cutoff 0 TE = "+repr(round(cutoffF0TE*1e9,1)) + " nm")
        print("Cutoff 0 TM = "+repr(round(cutoffF0TM*1e9,1)) + " nm")
        F1TE = (np.arctan(np.sqrt(b/(1-b)))+np.arctan(np.sqrt((b+aTE)/(1-b)))+np.pi)/(sqrt(1-b)*k*sqrt(nf**2-ns**2))
        F1TM = (np.arctan(np.sqrt(b/(1-b)))+np.arctan(np.sqrt((b+aTM)/(1-b)))+np.pi)/(sqrt(1-b)*k*sqrt(nf**2-ns**2))
        cutoffF1TE = (np.arctan(np.sqrt(aTE))+np.pi)/(k*sqrt(nf**2-ns**2))
        cutoffF1TM = (np.arctan(np.sqrt(aTM))+np.pi)/(k*sqrt(nf**2-ns**2))
        print("Cutoff 1 TE = "+repr(round(cutoffF1TE*1e9,1)) + " nm")
        print("Cutoff 1 TM = "+repr(round(cutoffF1TM*1e9,1)) + " nm")
        np.warnings.filterwarnings('always')
        if display == True:
            fig = plt.figure(1,figsize = (7,7),facecolor='white')
            ax = plt.subplot(1,1,1)
            p1 = ax.plot(F0TE*1e9,N,'-r',label = 'TE, m0')
            p2 = ax.plot(F0TM*1e9,N,'-g',label = 'TM, m0')
            p3 = ax.plot(F1TE*1e9,N,'--r',label = 'TE, m1')
            p4 = ax.plot(F1TM*1e9,N,'--g',label = 'TM, m1')
            l1 = ax.legend(loc='upper left')
            plt.xlim((0,cutoffF1TM*1e9+100))
            plt.ylim((self.n_s,self.n_f))

            wave = int(self.wavelength *1e9)
            plt.title("Eigenvalue Equation for n_f = %s at %s nm" %(round(self.n_f,2),wave))
            plt.xlabel('Thickness [nm]')
            plt.ylabel('Effective Refractive Index [-]')
            g1 = ax.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
            g2 = ax.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
            ax.minorticks_on()
            plt.show()

        if saveFigure != False:
            fig.savefig('../plots/' + 'Test' + self.polarization + '.png')

    def plotCutoffThickness(self, limitLow, limitHigh):
        """The function calculates the cutoff thicknesses of the waveguide, at a given wavelength, for different refractive index value.
        """
        nf_array = linspace(limitLow,limitHigh,10)
        thickness = []
        for i in range(0, 10):
            nf = nf_array[i]
            Waveguide = SlabWaveguide(
                        name = 'Silicon Nitride', n_s = glassSubstrate.n, n_f = nf,
                        n_c = aqueousMedium.n, d_f = 150e-9, polarization = 'TE',
                        inputPower = effectivePowerperUnitLength, wavelength = wavelength,
                        attenuationConstant = attenuationConstant,
                        mode = 0)
            t = Waveguide.cutoffThickness()
            thickness.append(t)
            print (repr(round(nf,2)) + "    " + repr(round(t*1e9,1)))


if __name__ == '__main__':
    # Primary Attributes
    n_f = 1.9
    index = 500
    wavelength = 633e-9
    # Secondary Attributes
    inputPower = 2e-3
    beamWaist = 1e-3
    couplingEfficiency = 0.3
    effectivePowerperUnitLength = inputPower * couplingEfficiency/(beamWaist)
    attenuationConstant = 0.8 * np.log(10)/10. * 100
    glassSubstrate = IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521)
    aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33)

    Waveguides = WaveguideAnalysis(
                        name = 'Silicon Nitride', n_s = glassSubstrate.n, n_f = n_f,
                        n_c = aqueousMedium.n, d_f = 150e-9, polarization = 'TE',
                        inputPower = effectivePowerperUnitLength, wavelength = wavelength,
                        attenuationConstant = attenuationConstant,
                        mode = 0)
    Waveguides.Dispersion()
