import numpy as np
import matplotlib.pylab as plt
from scipy import integrate
import pandas as pd
import sys

sys.path.append('../')
sys.path.append('../../')
from waveguide import SlabWaveguide
from materials.materials import IsotropicMaterial
from auxiliaries.Journal_formats_lib import formatPRX
from interfaces.configuration import Media


if __name__ == '__main__':

	#-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	# Data of the Lumerical Simulation
	
	E_X = pd.read_csv('E_X.txt',delimiter=',')

	z = E_X.ix[:,0]*1e-6
	E_X = E_X.ix[:,1]


	P = pd.read_csv('Power_Y.txt',delimiter=',')

	Power = np.asarray(P[' Re(Py)'])

	Total_Line_Power = integrate.simps(Power,z) # there is something wrong here...
	print Total_Line_Power

	#-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	# Data of the waveguide class

	d_f = 145e-9
	inputPower = 2e-3
	beamWaist = 1e-3
	couplingEfficiency = 1
	
	effectivePowerperUnitLength = Total_Line_Power
	wavelength = 632.8e-9
	attenuationConstant = 0
	glassSubstrate = IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521)
	aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33	)
	# TE
	waveguide = SlabWaveguide(
					name = 'Tantalum Pentoxide', n_s = glassSubstrate.n, n_f = 2.118, 
					n_c = aqueousMedium.n, d_f = d_f, polarization = 'TE', 
					inputPower = effectivePowerperUnitLength, wavelength = wavelength,
					attenuationConstant = attenuationConstant,
					mode = 0)

	waveguide.calcWaveguideProperties(1.7,632.8e-9)

	configuration = Media([np.inf,0,-d_f,-np.inf],[aqueousMedium, waveguide, glassSubstrate])

	print waveguide.powerFromFields(0)
	print waveguide.power(0)

	y = np.zeros(len(z))
	x = np.zeros(len(z))
	E_analytical = waveguide.E_y(wavelength,x,y,z)

	#-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	
	# Compare the electromagnetic fields of numerical simulation and analytical equations

	formatPRX()
	fig = plt.figure()
	ax = fig.add_subplot(111)

	waveguide.drawMedia(ax,configuration,E_analytical,labeling=False)
	plt.plot(E_X,z*1e9,label="numerical")
	plt.plot(E_analytical,z*1e9,label='analytical')
	ax.text(0.7,200,'Power {:0.1E} [W/m]'.format(waveguide.power(0)))
	plt.legend()

	plt.savefig('LumericalValidationOfWaveguideClass.png',format='png')



