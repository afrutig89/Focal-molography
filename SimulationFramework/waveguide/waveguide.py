"""
Waveguide classes. Consists

- Asymmetric slab waveguide
- Optical fibers

.. todo:: display in different directions
.. todo:: display_2D

:author:       Silvio Bischof
:revised:      7-Dec-2016
:reference:    Marcuse
"""


import sys
sys.path.append("../")
import numpy as np
from numpy import *
from scipy.optimize import minimize
from scipy import integrate
import auxiliaries.constants as constants
from materials.materials import IsotropicMaterial
import cmath
import matplotlib.pyplot as plt
import matplotlib.patches as patches


class Waveguide():
    """This is a class that calculates the eigenmodes of a planar waveguide. Most of the formulas are implemented from Marcuse 1974 - Theory of dielectric optical waveguides. 
    # checked by AF works for TE_0 and TM_0 mode.
    x is the propagation direction of the mode and y is the in plane coordinate and z the out of plane coordinate. 

        :params: waveguide is a struct with the following fields:
        :params waveguide.n_s: refractive index of the substrate
        :params waveguide.n_f: refractive index of guiding film
        :params waveguide.n_c: refractive index of cover
        :params waveguide.d_f: thickness of the waveguide e(nm)
        :params waveguide.rho: polarization of the mode of the waveguide ('TE', 'TM')
        :params waveguide.inputPower: input power coupled into the waveguidde in W/m
        :params waveguide.attenuationConstant: power propagation loss of the waveguide in [Np/m] to do the conversion from dB/cm multiply by: np.log(10)/10. * 100 Np/m (this is a power quantity, the real definition of a neper would be for the fields)
        :params waveguide.m: mode number of the wave, Solver only works for the zeroth order mode so far. 
    """

    def __init__(self, name, n_c, 
            thickness, polarization, 
            inputPower, wavelength):

        self.name = name
        self.n_c = n_c
        self.thickness = thickness
        self.polarization = polarization
        self.inputPower = inputPower
        self.wavelength = wavelength
        if polarization == 'TE':
            self.rho = 0
        elif polarization == 'TM':
            self.rho = 1

        self.k = 2.*np.pi/wavelength # free space propagation constant.


class SlabWaveguide(Waveguide, IsotropicMaterial):
    """This is a class that calculates the eigenmodes of a planar waveguide. Most of the formulas are implemented from Marcuse 1974 - Theory of dielectric optical waveguides. 
    # checked by AF works for TE_0 and TM_0 mode.
    x is the propagation direction of the mode and y is the in plane coordinate and z the out of plane coordinate. 

        :param waveguide.n_s: refractive index of the substrate
        :param waveguide.n_f: refractive index of guiding film
        :param waveguide.n_c: refractive index of cover
        :param waveguide.d_f: thickness of the waveguide e(nm)
        :param waveguide.rho: polarization of the mode of the waveguide ('TE', 'TM')
        :param waveguide.inputPower: input power coupled into the waveguidde in W/m (per unit length)
        :param waveguide.attenuationConstant: propagation loss of the waveguide in [Np/m] to do the conversion from dB/cm multiply by: np.log(10)/10. * 100
        :param waveguide.m: mode number of the wave, Solver only works for the zeroth order mode so far. 
        :param x_0: position of the grating coupler (since attenuation in x-direction is considered)
    
    """

    def __init__(self, name, n_s, n_f, n_c, d_f, polarization, inputPower, wavelength, attenuationConstant = 0, mode = 0, x_0 = 0,media=0,a_ani = 0.054):
        """ Standard constructor
        """
        Waveguide.__init__(self, name = name, 
            n_c=n_c, thickness = d_f, polarization = polarization,
            inputPower = inputPower, wavelength=wavelength)
        # Try catch because materials database migth not exist but should be checked as default in order not to break compatibility. 
        try:
            IsotropicMaterial.__init__(self, name, wavelength)
        except:
            IsotropicMaterial.__init__(self, name, wavelength,n_f)

        self.n_s = n_s
        self.n_f = n_f
        self.d_f = d_f
        self.x_0 = x_0
        self.m = mode   # note: only zeroth order implemented
        self.attenuationConstant = attenuationConstant  # in Np/m 

        # sehr unsuber, redo
        self.calcPropagationConstants(wavelength)
        self.calcEffThickness(wavelength)

    @classmethod
    def standardWaveguide(cls):
        """Constructor for the standard Ta2O5 waveguide in Air"""

        attenuationConstant = 2.51 * np.log(10)/10. * 100

        return cls(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                              n_c=1., d_f=145e-9, polarization='TE',
                              inputPower=0.02, wavelength=632.8e-9, attenuationConstant=attenuationConstant)

    def getRefractiveIndexes(self):

        return self.n_s,self.n_f,self.n_c,self.N

        
    def calcPropagationConstants(self, wavelength):
        """
        calculation of the propagation constants beta, kappa, gamma 
        and delta.

        :reference: equations, 1.2-13-15 Marcuse
        """
        # checked by AF, verfied with equations, 1.2-13-15 in Marcuse
        wavenumber = 2*pi/wavelength
        self.wavelength = wavelength
        
        if 'N' not in dir(self):
            self.N = self.calcEffRefractiveIndex((self.n_f+self.n_s)/2,wavelength)  # (self.n_f+self.n_s)/2 starting condition for the optimization algorithm. 
            
        self.beta = self.N*wavenumber
        self.kappa = cmath.sqrt(self.n_f**2 * wavenumber **2-self.beta**2)
        self.gamma = cmath.sqrt(self.beta**2 - self.n_s**2 * wavenumber**2)
        self.delta = cmath.sqrt(self.beta**2 - self.n_c**2 * wavenumber**2)


    def calcEffRefractiveIndex(self,N_ini,wavelength):
        """solves the eigenvalue equation to obtain the effective 
        refractive index.
        
        :reference: Optical grating coupler biosensors, Janos Voeroes, Biomaterials 2002.
        """
        # checked by AF, works for the zeroth order modes, Equation is from Janos Voeroes, Optical grating coupler biosensors, Biomaterials 2002.
        self.wavelength = wavelength
        calc_N = lambda N: np.abs(np.arctan(np.sqrt((N**2-self.n_s**2)/ \
            (self.n_f**2 - N**2))*(self.n_f/self.n_s)**(2*self.rho)) + \
            np.arctan(np.sqrt((N**2-self.n_c**2)/(self.n_f**2-N**2)) * \
            (self.n_f/self.n_c)**(2*self.rho)) - 2*np.pi/wavelength*np.sqrt(self.n_f**2 - N**2) *\
            self.d_f - np.pi*self.m);

        res = minimize(calc_N,N_ini,method='Nelder-Mead',tol=1*10**(-6))
        self.N = res.x

        return self.N[0]


    def calcEffThickness(self,wavelength):
        """calculates the effective thickness

        :reference: Equations 4.3 - 4.5, Tiefenthaler, Lukosz 1989
        """
        # checked by AF, Equations are from Tiefenthaler, Lukosz 1989, Equations 4.3 - 4.5
        self.deltazs = wavelength/(2*np.pi)*1/(np.sqrt(self.N**2-self.n_s**2))*((self.N/self.n_f)**2+(self.N/self.n_s)**2-1)**(-1*self.rho);
        self.deltazc = wavelength/(2*np.pi)*1/(np.sqrt(self.N**2-self.n_c**2))*((self.N/self.n_f)**2+(self.N/self.n_c)**2-1)**(-1*self.rho);
        self.t_eff = self.deltazs + self.deltazc + self.d_f; # tested: 330 in_stead of 329 nm as described in Christof's Paper. 
        
        return self.t_eff


    def calcWaveguideProperties(self,N_ini,wavelength):

        print ("effective Index: " + str(self.calcEffRefractiveIndex(N_ini,wavelength)))
        print ("effective Thickness: " + str(self.calcEffThickness(wavelength)))
        

    def cutoffThicknessTE(self, mode = 0):
        """
        .. math:: d_c = \\left(\\arctan \\left[\\sqrt{\\frac{n_s^2-n_c^2}{n_f^2-n_s^2}}] + \\nu \\pi\\right)/\\kappa

        :reference: eq. 1.3-42 Marcuse
        """ 

        # if 'kappa' not in dir(self):
        #     self.calcPropagationConstants(self.wavelength)

        # d = (arctan((self.n_s**2-self.n_c**2)**(1/2.) / \
        #         (self.n_f**2-self.n_s**2)**(1/2.)) + mode*pi)/self.kappa


        kappa_d = (arctan((self.n_s**2-self.n_c**2)**(1/2.) / \
                (self.n_f**2-self.n_s**2)**(1/2.)) + mode*pi)
        gamma = lambda kappa: np.sqrt((self.n_f**2-self.n_s**2)* (2*np.pi/self.wavelength)**2 - kappa**2)
        delta = lambda kappa: np.sqrt((self.n_f**2-self.n_c**2)* (2*np.pi/self.wavelength)**2 - kappa**2)
        calc_kappa = lambda kappa: kappa * (gamma(kappa)+delta(kappa)/kappa**2-gamma(kappa)*delta(kappa))- np.tan(kappa_d) 
        res = minimize(calc_kappa,self.kappa,method='Nelder-Mead',tol=1*10**(-6))
        kappa = res.x
        d = kappa_d / kappa

        return d


    def calcPenetrationDepth(self):
        """
        Returns the penetration depth in the cover and the substrate.

        .. math::   \\Delta z_s = \\frac{\\lambda}{2\\pi} \\left(N^2 - n_s^2\\right)^{-1/2}
        .. math::   \\Delta z_c = \\frac{\\lambda}{2\\pi} \\left(N^2 - n_c^2\\right)^{-1/2}

        :returns: tuple of penetration depth in the cover and substrate (Deltaz_fc, Deltaz_fs)

        :reference: Eqs. 4.4, 4.5 Sensitivity of grating couplers as integrated-optical chemical sensors (K. Tiefenthaler and W. Lukosz)
        """

        if self.polarization == 'TE':
            deltaz_f0c = (self.wavelength/(2*pi))*(self.N**2-self.n_c**2)**(-1/2.)
            deltaz_f0s = (self.wavelength/(2*pi))*(self.N**2-self.n_s**2)**(-1/2.)

        if self.polarization == 'TM':
            deltaz_f0c = (self.wavelength/(2*pi))*(self.N**2-self.n_c**2)**(-1/2.) \
                            * ((self.N/self.n_f)**2 + (self.N/self.n_c)**2 - 1)
            deltaz_f0s = (self.wavelength/(2*pi))*(self.N**2-self.n_s**2)**(-1/2.) \
                            * ((self.N/self.n_f)**2 + (self.N/self.n_s)**2 - 1)

        return deltaz_f0c, deltaz_f0s


    def calcSensitivity(self):
        """
        Calculation of the sensitivity of the effective refractive index with respect to cover index changes.

        .. math:: \\frac{\\partial N}{\\partial n_c} = \\frac{n_c}{N} \\frac{n_f^2-N^2}{n_f^2-n_c^2} \\frac{\\Delta z_{f,c}}{d_{eff}}\\left[2 \\left(\\frac{N}{n_c}\\right)^2-1\\right]^\\rho

        :reference: Eq. 4.14 Sensitivity of grating couplers as integrated-optical chemical sensors (K. Tiefenthaler and W. Lukosz)
        """
        deltaz_f0c = self.calcPenetrationDepth()[0]
        dNdn0 = self.n_c/self.N * (self.n_f**2-self.N**2)/(self.n_f**2-self.n_c**2) \
                    * (deltaz_f0c/self.t_eff) * (2*(self.N/self.n_c)**2-1)**self.rho

        return dNdn0


    def surfaceScatteringAttenuation(self, sigma_12, sigma_23=None):
        """
        sigma12 is the standarddeviation of the surface roughness not the variance.
        :reference: Sec. 6.1.1 Integrated Optics - Theory and Technology (Robert G. Hunsperger), 6th Edition

        .. math:: \\alpha_s = A^2 \\left(\\frac{1}{2} \\frac{\\cos^3 \\theta_m}{\\sin \\theta_m} \\right) \\left(\\frac{1}{d+(1/\\gamma)+(1/\\delta)} \\right)

        
        .. math:: A = \\frac{4\\pi}{\\lambda_2} (\\sigma_{12}^2 + \\sigma_{23}^2)^{(1/2)}

        .. math:: A = \\frac{4\\pi\\cdot N}{\\lambda} (\\sigma_{12}^2 + \\sigma_{23}^2)^{(1/2)}

        .. math:: \\theta_1 = arccos\\left( \\frac{N}{n_f}\\right)

        .. math:: \\theta_m = \\frac{\\pi}{2}-\\theta_1

        :math:`{\\lambda_2 }` is the wavelength in the waveguide.
        
        """

        if sigma_23 == None: sigma_23 = sigma_12

        A = 4*np.pi * self.N/self.wavelength * np.sqrt(sigma_12**2+sigma_23**2)
        theta_1 = np.arccos(self.N/self.n_f)
        theta_m = np.pi/2.-theta_1

        alpha_s = A**2 * (1/2.* np.cos(theta_m)**3/np.sin(theta_m)) * (1 / (self.d_f+(1/self.gamma)+ (1/self.delta)))

        return alpha_s


    def specularlyReflectedBeam(self, P_i, sigma):
        """
        To revise! Is this the power that is reflected within the waveguide?

        :reference: Sec. 6.1.1 Integrated Optics - Theory and Technology (Robert G. Hunsperger), 6th Edition

        .. math:: P_T = P_i \\exp\\left[-\\left(\\frac{4\\pi\\sigma}{\\lambda_2} \\cos\\theta_m' \\right)^2\\right]
        
        """

        lambda_2 = self.wavelength/self.N # wavelength in the guiding layer
        theta_1 = np.arccos(self.N/self.n_f)
        theta_m = np.pi/2.-theta_1
        P_t = P_i * np.exp(-(4*np.pi*sigma/lambda_2 * np.cos(theta_m))**2)

        return P_t


    def E_x(self, wavelength, x, y, z):
        """ Calculates the electric field of a waveguide in x-direction. (Propagation direction)
        z=0 is defined as the top border of the waveguide facing the cover medium. 
        """
        
        angularFrequency = 2*pi/wavelength * constants.c
        
        # define top of waveguide as z=0
        [X_below, Y_below, Z_below] = self.return_xyz_below(x, y, z)
        [X_inside,Y_inside,Z_inside] = self.return_xyz_inside(x, y, z)
        [X_above,Y_above,Z_above] = self.return_xyz_above(x, y, z)
        
        if (self.gamma is None):
            self.calcPropagationConstants(wavelength)
            
        if self.rho == 0:
            E_below = 0 * Z_below
            E_inside = 0 * Z_inside
            E_above = 0 * Z_above

        elif self.rho == 1:
            # Marcuse, attention the coordinate system is different. This is E_z in Marcuse.
            # Checked AF, Equations 1.3-57-59 Marcuse
            E_above = 1j * self.delta / (self.n_c**2 * angularFrequency * constants.eps_0) * self.beta/abs(self.beta) * self.coefC(X_above) * np.exp(-self.delta * (Z_above)) # ok
            E_inside = 1j * self.kappa / (self.n_f**2 * angularFrequency * constants.eps_0) * self.beta/abs(self.beta) * (self.coefC(X_inside) * np.sin(self.kappa * (Z_inside)) - self.coefD(X_inside) * np.cos(self.kappa * Z_inside))
            E_below = -1j * self.gamma / (self.n_s**2 * angularFrequency * constants.eps_0) * self.beta/abs(self.beta) * (self.coefC(X_below) * np.cos(self.kappa * self.d_f) - self.coefD(X_below) * np.sin(self.kappa * self.d_f)) * np.exp(self.gamma * (Z_below+self.d_f))
            
        else:
            print('no self.rho selected')
            return 0

        return np.hstack([E_below,E_inside,E_above])
        
        
    def E_y(self, wavelength, x, y, z):
        """ Calculates the electric field of a waveguide in y-direction. 
        z=0 is defined as the top border of the waveguide
        """
                
        # define top of waveguide as z=0
        [X_below, Y_below, Z_below] = self.return_xyz_below(x, y, z)
        [X_inside,Y_inside,Z_inside] = self.return_xyz_inside(x, y, z)
        [X_above,Y_above,Z_above] = self.return_xyz_above(x, y, z)
        
        if (self.gamma is None):
            self.calcPropagationConstants(wavelength)
            
        if self.rho == 0:
            # checked AF, Marcuse 1.3-16-18
            E_above = self.coefA(X_above) * np.exp(-self.delta * Z_above)
            E_inside = self.coefA(X_inside)* np.cos(self.kappa * Z_inside) + self.coefB(X_inside) * np.sin(self.kappa * Z_inside)
            E_below = (self.coefA(X_below) * np.cos(self.kappa * self.d_f) - self.coefB(X_below) * np.sin(self.kappa*self.d_f)) * np.exp(self.gamma * (Z_below + self.d_f))
            
        elif self.rho == 1:
            E_below = 0 * Z_below
            E_inside = 0 * Z_inside
            E_above = 0 * Z_above
            
        else:
            print('no self.rho selected')
            return 0
            
        return np.hstack([E_below,E_inside,E_above])
    
    
    def E_z(self, wavelength, x, y, z):
        """ Calculates the electric field of a waveguide in x-direction. 
        z=0 is defined as the top border of the waveguide
        """
        
        angularFrequency = 2*pi/wavelength * constants.c
         
        # define top of waveguide as z=0
        [X_below, Y_below, Z_below] = self.return_xyz_below(x, y, z)
        [X_inside,Y_inside,Z_inside] = self.return_xyz_inside(x, y, z)
        [X_above,Y_above,Z_above] = self.return_xyz_above(x, y, z)
        
        if (self.gamma is None):
            self.calcPropagationConstants(wavelength)
        
        if self.rho == 0:
            E_below = 0 * Z_below
            E_inside = 0 * Z_inside
            E_above = 0 * Z_above
            
        elif self.rho == 1:
            # attention the coordinate system is different than in Marcuse.
            # checked by AF, Marcuse Equations 1.3-54-56
            E_above = self.beta / (self.n_c**2 * angularFrequency * constants.eps_0) * self.beta/abs(self.beta) * self.coefC(X_above) * np.exp(-self.delta * Z_above)
            E_inside = self.beta / (self.n_f**2 * angularFrequency * constants.eps_0) * self.beta/abs(self.beta) * (self.coefC(X_inside) * np.cos(self.kappa * Z_inside)  + self.coefD(X_inside) * np.sin(self.kappa * Z_inside))
            E_below = self.beta / (self.n_s**2 * angularFrequency * constants.eps_0) * self.beta/abs(self.beta) * (self.coefC(X_below) * np.cos(self.kappa * self.d_f) - self.coefD(X_below) * np.sin(self.kappa * self.d_f)) * np.exp(self.gamma * (Z_below + self.d_f))
            
        else:
            print('no self.rho selected')
            return 0
            
        return np.hstack([E_below,E_inside,E_above])
        
    def coefA(self,x):
        """
        :reference: Equation 1.3-47 in Marcuse
        """
        # checked AF, Equation 1.3-47 in Marcuse
        angularFrequency = 2*pi/self.wavelength * constants.c

        return np.sqrt(4*self.kappa**2 * angularFrequency * constants.mu_0 * self.power(x) / (abs(self.beta) * (self.d_f + 1/self.gamma + 1/self.delta) * (self.kappa**2 + self.delta**2)))
    
    def coefB(self,x):
        """
        :reference: Equation 1.3-22 in Marcuse
        """
        # checked AF, Equation 1.3-22 in Marcuse
        return -self.coefA(x) *  self.delta / self.kappa
    
    def coefC(self,x):
        """
        :reference: Equation 1.3-73 in Marcuse
        """
        # checked AF, Equation 1.3-73 in Marcuse
        angularFrequency = 2*pi/self.wavelength * constants.c
        return np.sqrt(4 * angularFrequency * constants.eps_0* self.power(x) * self.n_f**2 * self.n_c**4 * self.kappa**2 / abs(self.beta)\
             / ((self.n_c**4 *self.kappa**2 + self.n_f**4 * self.delta**2)\
                * (self.d_f \
                    + (self.n_f**2 *self.n_s**2 * (self.kappa**2 + self.gamma**2)/(self.gamma * (self.n_s**4 *self.kappa**2 + self.n_f**4*self.gamma**2))) \
                    + (self.n_f**2 *self.n_c**2 * (self.kappa**2 + self.delta**2)/(self.delta   * (self.n_c**4 *self.kappa**2 + self.n_f**4*self.delta**2)))   ))  )

    def coefD(self,x):
        """
        :reference: Equation 1.3-61 in Marcuse
        """
        # checked AF, Equation 1.3-61 in Marcuse
        return -self.coefC(x) * self.n_f**2 * self.delta / (self.n_c**2 * self.kappa)

    def power(self,x):
        """Returns the power at a position x in the waveguide along the propagation direction of the mode. """
        # checked by AF, this was wrong. P is the total power per unit length in the waveguide. The coefficient C is directly calculated from this. 
        #angularFrequency = 2*pi/self.wavelength * constants.c
        #return self.beta/(2*angularFrequency*constants.mu_0) * self.d_f * self.inputPower * np.exp(-2*self.attenuationConstant * x)
        
        return self.inputPower * np.exp(-self.attenuationConstant * (x+self.x_0))

    def intensity_on_waveguide_surface(self):
        """returns the intensity on the waveguide surface, calculated according to:
            
        .. math:: {I_0} = \\frac{{{n_c}}}{{2{Z_w}}}{A^2} = {n_c}\\frac{{2\\left( {n_{\\mathrm{f}}^2 - {N^2}} \\right)}}{{N{t_{{\\mathrm{eff}}}}\\left( {n_{\\mathrm{f}}^2 - n_{\\mathrm{c}}^2} \\right)}}{P_{{\\mathrm{wg}}}}
        
        """

        n_c = self.n_c
        n_s = self.n_s
        n_f = self.n_f
        N = self.N
        t_eff = self.t_eff

        return n_c*2.*(n_f**2 - N**2)/(N*t_eff*(n_f**2-n_c**2))*self.inputPower

    def intensity_on_waveguide_surface_power_in_wg_ratio(self):
        """
            computes the ratio of the intensity on the waveguide surface and the power in the waveguide.
        """

        return self.intensity_on_waveguide_surface()/self.inputPower

    def powerFromFields(self,x,zmin=-700e-9,zmax=700e-9):
        """Function that calculates the power from the fields calculated by the Eigenmode analysis"""

        z = np.linspace(zmin, zmax, 1000)
        y = np.zeros(len(z))
        xx = np.zeros(len(z))

        ones = np.ones(len(z))
        # refractive index
        n = np.hstack([ones[z < -self.d_f]*self.n_s,ones[(z >= -self.d_f) & (z < 0)]*self.n_f, ones[z >= 0]*self.n_c])

        E_x = self.E_x(self.wavelength,xx,y,z)
        E_y = self.E_y(self.wavelength,xx,y,z)
        E_z = self.E_z(self.wavelength,xx,y,z)

        EE = E_x*np.conj(E_x) + E_y*np.conj(E_y)  +  E_z*np.conj(E_z)

        Intensity = 1/2.*abs(n)*abs(EE)/constants.Z_w

        power = integrate.simps(Intensity,z)

        return power * np.exp(-self.attenuationConstant * (x+self.x_0))
        
    def return_xyz_below(self, x, y, z):

        X_below = x[z < -self.d_f]
        Y_below = y[z < -self.d_f]
        Z_below = z[z < -self.d_f]
        return X_below, Y_below, Z_below
                
    def return_xyz_inside(self, x, y, z):
        X_inside = x[(z >= -self.d_f) & (z < 0)]
        Y_inside = y[(z >= -self.d_f) & (z < 0)]
        Z_inside = z[(z >= -self.d_f) & (z < 0)]
        
        return X_inside, Y_inside, Z_inside
                
    def return_xyz_above(self, x, y, z):
        X_above = x[z >= 0]
        Y_above = y[z >= 0]
        Z_above = z[z >= 0]
        
        return X_above, Y_above, Z_above
        
    def display(self, x, y, z, configuration = None, saveFigure=False,fig = False):
        """Displays the field intensity of the TE and TM mode along 
        the z-direction.
        """
        
        # determine direction
        # TODO!

        if not fig:
            fig = plt.figure()
        
        # TE Case
        if self.rho == 0:
            E_plot = self.E_y(self.wavelength, x, y, z)
            plt.title('TE field distribution')
            

        # TM Case, E_x is zero since it points in the propagation 
        # direction of the wave.
        if self.rho == 1:
            E_plot = self.E_z(self.wavelength, x, y, z)
            plt.title('TM field distribution')

        
        if configuration != None:

            ax = fig.add_subplot(111)
            self.drawMedia(ax,configuration,E_plot)
            
            
        plt.plot(E_plot.real,z*1e9)
        
        if saveFigure != False:
            fig.savefig('../plots/' + saveFigure + self.polarization + '.png')
        else:
            plt.show()

    def singleModeThickness(self):
        """Retruns the single mode thickness for all the other parameters of this waveguide

        :reference: Adams: An introduction to Optical Waveguides p. 31 Eqn 2.88 and 2.89
        or
        :refernce: Marcuse Theory of Dielectric Waveguides p 13 Eqn 1.3-41 and 1.3-42
        .. math :: d = {{\\arctan \\left( {{{\\left( {{{n_s^2 - n_c^2} \\over {n_f^2 - n_s^2}}} \\right)}^{1/2}}} \\right) + \\pi } \\over {{{2\\pi } \\over \\lambda }{{\\left( {n_f^2 - n_s^2} \\right)}^{1/2}}}}
        """

        dCutoff_mode1 = (np.arctan(np.sqrt((self.n_s**2-self.n_c**2)/(self.n_f**2-self.n_s**2))) + np.pi)\
                        /(2*np.pi/self.wavelength * np.sqrt(self.n_f**2-self.n_s**2))


        return dCutoff_mode1

    
    def figureOfMeritWGExperimental(self,a_ani):
        """Calculates the figure of merit for a waveguide used in focal molography with experimentally determined anisotropic scattering coefficient and total damping constant. The higher this value, the better the waveguide. 

        .. math:: FO{M_{{\\mathrm{WG}}}} = \\frac{{{n_{\\mathrm{c}}}\\left( {n_{\\mathrm{f}}^2 - {N^2}} \\right)}}{{N{t_{{\\mathrm{eff}}}}\\left( {n_{\\mathrm{f}}^2 - n_{\\mathrm{c}}^2} \\right){\\lambda ^4}{a_{{\\mathrm{ani}}}}{\\alpha _{{\\mathrm{sca}}}}}}}

        """
        n_s,n_f,n_c,N = self.getRefractiveIndexes()
        t_eff = self.t_eff
        alpha = self.attenuationConstant

        return n_c*(n_f**2-N**2)/(N*t_eff*(n_f**2-n_c**2)*a_ani*alpha)/(self.wavelength**4)

    def drawMedia(self,ax,configuration,E_plot,labeling= True):

        n_max = 1.0001
        d_min = inf

        for medium in configuration.media:
            # find the medium with the maximum refractive index
            n_max = max(n_max,medium.n)
            # find the thinnest medium
            d_min = min(d_min,abs(medium.boundaries[1]-medium.boundaries[0]))
    
        
        # first medium defined as being below the chip
        if configuration.media[0].boundaries[0] == -inf:

            for medium in configuration.media:
                d = min(abs(medium.boundaries[1]-medium.boundaries[0]),5*d_min)*1e9
                p = patches.Rectangle(
                    (0,max(medium.boundaries[0],-5*d_min)*1e9), max(E_plot), d,
                    color = (180/255.,230/255.,246/255.),
                    alpha = (medium.n-1)/(n_max-1)
                )

                ax.add_patch(p)

                if labeling:
                    ax.text(0.5,max(medium.boundaries[0],-5*d_min)+d_min/2,medium.name + ' (n = ' + str(medium.n) + ')', horizontalalignment='center')
        
        # first medium defined as being above the chip   
        else:
            for medium in configuration.media:
                d = min(abs(medium.boundaries[1] - medium.boundaries[0]),5*d_min)*1e9
                p = patches.Rectangle(
                    (0,max(medium.boundaries[1],-5*d_min)*1e9), max(E_plot), d,
                    color = (180/255.,230/255.,246/255.),
                    alpha = (medium.n-1)/(n_max-1)
                )

                ax.add_patch(p)
                if labeling:
                    ax.text(0.5*max(E_plot),(max(medium.boundaries[1],-5*d_min)+d_min/5)*1e9,medium.name + ' (n = ' + str(medium.n) + ')', horizontalalignment='center')

        ax.set_xlim(0,np.max(E_plot))
        ax.set_ylim(-5*d_min*1e9,medium.boundaries[0]+d)
        ax.set_ylabel('z [nm]')

        if self.rho ==0:
            ax.set_xlabel(r'$E_y$ [ $\frac{V}{m}$]')

        if self.rho ==1:
            ax.set_xlabel(r'$E_z$  [ $\frac{V}{m}$ ]')
        
    def save(self, x, y, z, configuration = None, saveFigure='field_distribution'):
        self.display(x,y,z,configuration, saveFigure=saveFigure)

    def display_2D(self, x, y, z, configuration = None):
        raise NotImplementedError

    def P_cover(self):
        """calculates the power in the cover
        :reference: MJ Adams An Introduction to Optical Waveguides p.32 Eqn: 2.95a

        .. math :: {P_{{\\rm{cover}}}} = {\\beta  \\over {2\\omega {\\mu _0}}}{{{A^2}} \over {2\\delta }}


        """
        P_cover = self.beta/(2. * 2*np.pi/self.wavelength  * constants.c* constants.mu_0) * self.coefA(0)**2 / (2* self.delta)
        return P_cover

    def P_film(self):
        """calculates the power in the film
        :reference: MJ Adams An Introduction to Optical Waveguides p.32 Eqn: 2.95b

        .. math :: {P_{{\\rm{film}}}} = {\\beta  \\over {2\\omega {\\mu _0}}}{{{A^2}} \\over 2}{{\\left( {{\\kappa ^2} + {\\delta ^2}} \\right)} \\over {{\\kappa ^2}}}\\left( {{d_f} + {\\gamma  \\over {{\\kappa ^2} + {\\gamma ^2}}} + {\\delta  \\over {{\\kappa ^2} + {\\delta ^2}}}} \\right)


        """


        P_film = self.beta/(2. * 2*np.pi/self.wavelength  * constants.c* constants.mu_0)\
                * self.coefA(0)**2 /2. * (self.kappa**2 + self.delta**2)/(self.kappa**2)\
                 *(self.d_f + self.gamma/(self.kappa**2+self.gamma**2) + self.delta / (self.kappa**2+self.delta**2) )
        return P_film


    def P_substrate(self):
        """calculates the power in the substrate
        :reference: MJ Adams An Introduction to Optical Waveguides p. Eqn: p.32 Eqn: 2.95a

        .. math :: {P_{{\\rm{substrate}}}} = {\\beta  \\over {2\\omega {\\mu _0}}}{{{A^2}} \\over {2\\gamma }}{{\\left( {{\\kappa ^2} + {\\delta ^2}} \\right)} \\over {\\left( {{\\gamma ^2} + {\\kappa ^2}} \\right)}}

        """

        P_substrate = self.beta/(2. * 2*np.pi/self.wavelength  * constants.c* constants.mu_0) * self.coefA(0)**2 / (2* self.gamma)\
                    *(self.kappa**2 + self. delta**2) / (self.gamma**2 + self.kappa**2)
        return P_substrate

    def asymmetryParameterJ(self):
        """Calculates the asymmetry parameter of the waveguide according to:

        .. math:: J = \\cos \\left( {{t_{\\mathrm{f}}}\\beta \\frac{{{{\\left( {n_{\\mathrm{f}}^2 - {N^2}} \\right)}^{1/2}}}}{N}} \\right) + \\frac{{{{\\left( {{N^2} - n_{\\mathrm{c}}^2} \\right)}^{1/2}}}}{{{{\\left( {n_{\\mathrm{f}}^2 - {N^2}} \\right)}^{1/2}}}}\\sin \\left( {{t_{\\mathrm{f}}}\\beta \\frac{{{{\\left( {n_{\\mathrm{f}}^2 - {N^2}} \\right)}^{1/2}}}}{N}} \\right)"""

        t_f = self.thickness
        beta = self.beta
        n_s,n_f,n_c,N = self.getRefractiveIndexes()

        return np.cos(t_f*beta*np.sqrt(n_f**2-N**2)/N) \
            + np.sqrt(N**2-n_c**2)/np.sqrt(n_f**2-N**2) \
            * np.sin(t_f*beta*np.sqrt(n_f**2-N**2)/N)

if __name__ == '__main__':

    from interfaces.configuration import Media

    waveguide = SlabWaveguide.standardWaveguide()
    attenuationConstant = 7.76*np.log(10)/10*100

    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                              n_c=1.33, d_f=145e-9, polarization='TE',
                              inputPower=0.02, wavelength=632.8e-9, attenuationConstant=attenuationConstant)
    print 'Figure of merit:'
    print waveguide.figureOfMeritWGExperimental(0.054)

    
    # d_f=146e-9
    # inputPower = 2e-3
    # beamWaist = 1e-3
    # couplingEfficiency = 0.3
    # effectivePowerperUnitLength = inputPower * couplingEfficiency/(beamWaist)
    # wavelength = 632.8e-9
    # attenuationConstant = 0.8 * np.log(10)/10. * 100
    # glassSubstrate = IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521)
    # aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33)
    # filmMedium = IsotropicMaterial(name='Tantalum Pentoxide', n_0=2.117)
    # # TE
    # waveguide = SlabWaveguide(
    #                 name = 'Tantalum Pentoxide', n_s = glassSubstrate.n, n_f = 2.117, 
    #                 n_c = aqueousMedium.n, d_f = d_f, polarization = 'TE', 
    #                 inputPower = effectivePowerperUnitLength, wavelength = wavelength,
    #                 attenuationConstant = attenuationConstant,
    #                 mode = 0)

    # print('Tantalum Pentoxide', glassSubstrate.n, 2.117, 
    #                 aqueousMedium.n, d_f, 'TE', 
    #                 effectivePowerperUnitLength, wavelength,
    #                 attenuationConstant)

    # # these powers need to be roughly equivalent for the field calculation to be correct
    # print waveguide.powerFromFields(0)
    # print waveguide.power(0)
                    
    # configuration = Media([inf,0,-d_f,-inf],[aqueousMedium, filmMedium, glassSubstrate])

    # waveguide.N = waveguide.calcEffRefractiveIndex(1.7,632.8e-9)
    # print "Effective Refractive Index:"
    # print waveguide.N
    # waveguide.calcWaveguideProperties(1.7,632.8e-9)
    
    # z = np.linspace(-700e-9, 700e-9, 1000)
    # y = np.zeros(len(z))
    # x = np.zeros(len(z))
    
    # print(waveguide.E_y(wavelength, np.array([0]), np.array([0]), np.array([0]))**2)
    # waveguide.display(x, y, z, configuration)

    # print "total power: ", waveguide.powerFromFields(0)
    # print "power abocve [%]: ", waveguide.powerFromFields(0,0,700e-9)/waveguide.powerFromFields(0)
    # print "power in [%]: ", waveguide.powerFromFields(0,-d_f,0)/waveguide.powerFromFields(0)
    # print "power below [%]: ", waveguide.powerFromFields(0,-700e-9,-d_f)/waveguide.powerFromFields(0)

    # print "single mode thickness: ", waveguide.singleModeThickness()


    # TM
    # waveguide = SlabWaveguide(
    #                 name = 'Tantalum Pentoxide', n_s = glassSubstrate.n, n_f = 2.117, 
    #                 n_c = aqueousMedium.n, d_f = d_f, polarization = 'TM', 
    #                 inputPower = effectivePowerperUnitLength, wavelength = wavelength,
    #                 attenuationConstant = attenuationConstant,
    #                 mode = 0)

    # waveguide.N = waveguide.calcEffRefractiveIndex(1.7,635e-9)
    # waveguide.calcWaveguideProperties(1.7,635e-9)
    
    # waveguide.display(x, y, z, configuration)



    d_f=145e-9

    d_f=900e-9

    inputPower = 2e-3
    beamWaist = 1e-3
    couplingEfficiency = 0.3
    effectivePowerperUnitLength = inputPower * couplingEfficiency/(beamWaist)
    wavelength = 632.8e-9
    attenuationConstant = 0.8 * np.log(10)/10. * 100
    glassSubstrate = IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521)
    aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33)
    filmMedium = IsotropicMaterial(name='Tantalum Pentoxide', n_0=1.9)
    # TE

    waveguide = SlabWaveguide(
                    name = 'Tantalum Pentoxide', n_s = glassSubstrate.n, n_f = 1.9,
                    n_c = aqueousMedium.n, d_f = d_f, polarization = 'TE', 
                    inputPower = effectivePowerperUnitLength, wavelength = wavelength,
                    attenuationConstant = attenuationConstant,
                    mode = 0)

    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                              n_c=1., d_f=145e-9, polarization='TE',
                              inputPower=1, wavelength=632.8e-9, attenuationConstant=attenuationConstant)
    print "Waveguide properties in air:"
    print waveguide.asymmetryParameterJ()
    waveguide.calcWaveguideProperties(1.7,632.8e-9)

    print waveguide.t_eff
    print waveguide.N

    attenuationConstant = 2.51 * np.log(10)/10. * 100
    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                              n_c=1.33, d_f=145e-9, polarization='TM',
                              inputPower=0.02, wavelength=632.8e-9, attenuationConstant=attenuationConstant)
    print "Waveguide properties in water:"
    print waveguide.asymmetryParameterJ()
    waveguide.calcWaveguideProperties(1.7,632.8e-9)
    print waveguide.t_eff
    print waveguide.N


    print('Tantalum Pentoxide', glassSubstrate.n, 2.117, 
                    aqueousMedium.n, d_f, 'TE', 
                    effectivePowerperUnitLength, wavelength,
                    attenuationConstant)

    # these powers need to be roughly equivalent for the field calculation to be correct
    print waveguide.powerFromFields(0)
    print waveguide.power(0)
                    
    configuration = Media([inf,0,-d_f,-inf],[aqueousMedium, filmMedium, glassSubstrate])
    print ('single mode thickness:', waveguide.singleModeThickness())
    waveguide.N = waveguide.calcEffRefractiveIndex(1.7,632.8e-9)
    print "Effective Refractive Index:"
    print waveguide.N
    waveguide.calcWaveguideProperties(1.7,632.8e-9)
    
    z = np.linspace(-700e-9, 700e-9, 1000)
    y = np.zeros(len(z))
    x = np.zeros(len(z))
    
    print(waveguide.E_y(wavelength, np.array([0]), np.array([0]), np.array([0]))**2)
    waveguide.display(x, y, z, configuration)

    print "total power: ", waveguide.powerFromFields(0)
    print "power abocve [%]: ", waveguide.powerFromFields(0,0,700e-9)/waveguide.powerFromFields(0)
    print "power in [%]: ", waveguide.powerFromFields(0,-d_f,0)/waveguide.powerFromFields(0)
    print "power below [%]: ", waveguide.powerFromFields(0,-700e-9,-d_f)/waveguide.powerFromFields(0)

    print "single mode thickness: ", waveguide.singleModeThickness()

    d_f_array = np.linspace(60,180,100)*1e-9
    P=[]
    sens =[]
    for d_f in d_f_array:
        waveguide = SlabWaveguide(
                        name = 'Tantalum Pentoxide', n_s = glassSubstrate.n, n_f = 2.117, 
                        n_c = aqueousMedium.n, d_f = d_f, polarization = 'TE', 
                        inputPower = effectivePowerperUnitLength, wavelength = wavelength,
                        attenuationConstant = attenuationConstant,
                        mode = 0)

        # waveguide.N = waveguide.calcEffRefractiveIndex(1.7,635e-9)
        # waveguide.calcWaveguideProperties(1.7,635e-9)
        
        # waveguide.display(x, y, z, configuration)
        P.append(waveguide.P_film())
        sens.append(waveguide.calcSensitivity())


    plt.plot(d_f_array,np.asarray(P)/waveguide.powerFromFields(0))
    plt.show()
    plt.plot(d_f_array,sens)
    plt.show()
       # d_f=900e-9
    # inputPower = 2e-3
    # beamWaist = 1e-3
    # couplingEfficiency = 0.3
    # effectivePowerperUnitLength = inputPower * couplingEfficiency/(beamWaist)
    # wavelength = 632.8e-9
    # attenuationConstant = 0.8 * np.log(10)/10. * 100
    # glassSubstrate = IsotropicMaterial(name='Fused Silica', n_0=1.457)
    # aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33)
    # filmMedium = IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521)
    # # TE
    # waveguide = SlabWaveguide(
    #                 name = 'Glass Substrate (D 263)', n_s = glassSubstrate.n, n_f =  filmMedium.n, 
    #                 n_c = aqueousMedium.n, d_f = d_f, polarization = 'TE', 
    #                 inputPower = effectivePowerperUnitLength, wavelength = wavelength,
    #                 attenuationConstant = attenuationConstant,
    #                 mode = 0)

    # print('Glass Substrate (D 263)', glassSubstrate.n, filmMedium.n, 
    #                 aqueousMedium.n, d_f, 'TE', 
    #                 effectivePowerperUnitLength, wavelength,
    #                 attenuationConstant)

    # # these powers need to be roughly equivalent for the field calculation to be correct
    # print waveguide.powerFromFields(0)
    # print waveguide.power(0)
                    
    # configuration = Media([inf,0,-d_f,-inf],[aqueousMedium, filmMedium, glassSubstrate])

    # waveguide.N = waveguide.calcEffRefractiveIndex(1.7,632.8e-9)
    # print "Effective Refractive Index:"
    # print waveguide.N
    # waveguide.calcWaveguideProperties(1.7,632.8e-9)
    
    # z = np.linspace(-d_f-1500e-9, 1500e-9, 10000)
    # y = np.zeros(len(z))
    # x = np.zeros(len(z))
    
    # print(waveguide.E_y(wavelength, np.array([0]), np.array([0]), np.array([0]))**2)
    # waveguide.display(x, y, z, configuration)

    # # print "total power: ", waveguide.powerFromFields(0,-1500e-9-d_f,1500e-9)
    # # print "total power: ", waveguide.P_cover()
    # # print "sub",  waveguide.P_substrate()
    # # print "fiml", waveguide.P_film()
    # print "power abocve [%]: ", waveguide.powerFromFields(0,0,1000e-9)/waveguide.powerFromFields(0,-2500e-9,1000e-9)
    # print "power petenration depth:", waveguide.powerFromFields(0,0,1/waveguide.delta)/waveguide.powerFromFields(0,-2500e-9,1000e-9)
    # print "power penetrtiion depth2:", waveguide.powerFromFields(0,0,2/(waveguide.delta))/waveguide.powerFromFields(0,-2500e-9,1000e-9)
    # print "power penetrtiion depth3:", waveguide.powerFromFields(0,0,3/(waveguide.delta))/waveguide.powerFromFields(0,-2500e-9,1000e-9)

