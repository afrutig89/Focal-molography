import sys
sys.path.append("../")
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import auxiliaries.constants as constants 


V = np.array([0,1,2])
def result(x):
    def integrand(E):

        del1, del2, R, fE, fEeV = 1.0,2.0,1.0,1.0,1.0
        e = 1.602176565*10**-19
        a = E/( np.sqrt( E**2 - del1**2 ) )
        b = ( E+ e*x )/(np.sqrt( ( E + e*x )**2) - del2**2)
        c = fE-fEeV
        d = 1/(e*R) # integration constant
        return a * b * c
    return integrate.quad(integrand, -np.inf, np.inf)

I = result(V)