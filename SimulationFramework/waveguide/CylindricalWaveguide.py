"""
Caclculation of a Cylindrical Waveguide

.. todo:: adapt with Silvio
.. todo:: make eigenvaluesolver nicer
.. todo:: make effective thickness nicer


:Author: Yves Blickenstorfer
:Revised: 3-Jan-2017
:Reference: Snyder, Love, Optical Waveguide Theory
:Reference: Tong, Sumetsky, Subwavelength and Nanometer Diameter Optical Fibers
"""
import sys
sys.path.append("../")
import numpy as np
import scipy.special as special
import matplotlib.pyplot as plt
from scipy import integrate
import auxiliaries.constants as constants 






class CylindricalWaveguide(object):
    """This is a class that with in which all important features of a cylindrical waveguide
    are calculated. Such as: propagation constants, effective refractive index, Electric Field,
    Magnetic field

    :param thickness: diameter of the waveguide
    :type thickness: float
    :param n_wg: refractive index of the waveguide 
    :type n_wg: float
    :param n_medium: refractive index of the surrounding medium
    :type n_medium: float
    :param wavelength: wavelegnth (in vacuum) of the light
    :type wavelength: float
    :param inputPower: input power of the light
    :type inputPower: float
    :param nu: mode number
    :type nu: int = 1
    :param calcMethod: how should the eigenvalue equation be solved can be numerically, numerically accurate, analythically
    :type calcMethod: string (calcMethod ='authomatic')


    In addition to the imput variables initialize are are wavenumber, angular frequency and radius:

    .. math:: {a_1} = {2 \\pi \\over \\lambda}
    .. math:: {\\omega} = {kc_0}
    .. math:: {a} = {d \\over 2}

    the V number (also called waveguide or fiber parameter and the profile height parameter

    .. math:: V = {{2\\pi a} \\over \\lambda }{\\left( {n_{{\\rm{wg}}}^2 - n_{{\\rm{medium}}}^2} \\right)^{1/2}}
    .. math:: \\Delta  = {1 \\over 2}\\left( {1 - {{n_{{\\rm{wg}}}^2} \\over {n_{{\\rm{medium}}}^2}}} \\right)

    :Reference: Love, Snyder, Optical Waveguide Theory, p. 227 , eqn 11-47 and 11-48

    and parameters that are needed for the calculation of the electric field

    .. math:: {F_1} = {\\left( {{{UW} \\over V}} \\right)^{1/2}}{{{b_1} + \\left( {1 - 2\\Delta } \\right){b_2}} \\over \\nu }
    .. math:: {F_2} = {\\left( {{{UW} \\over V}} \\right)^{1/2}}{\\nu  \\over {{b_1} + {b_2}}}
    .. math:: {b_1} = {1 \\over {2U}}\\left( {{{{{\\rm{J}}_{\\nu  - 1}}\\left( U \\right)} \\over {{{\\rm{J}}_\\nu }\\left( U \\right)}} - {{{{\\rm{J}}_{\\nu  + 1}}\\left( U \\right)} \\over {{{\\rm{J}}_\\nu }\\left( U \\right)}}} \\right)
    .. math:: {b_2} =  - {1 \\over {2W}}\\left( {{{{{\\rm{K}}_{\\nu  - 1}}\\left( W \\right)} \\over {{{\\rm{K}}_\\nu }\\left( W \\right)}} + {{{{\\rm{K}}_{\\nu  + 1}}\\left( W \\right)} \\over {{{\\rm{K}}_\\nu }\\left( W \\right)}}} \\right)
    .. math:: {a_1} = {{({F_2} - 1)} \\over 2}
    .. math:: {a_2} = {{({F_2} + 1)} \\over 2}
    .. math:: {a_3} = {{({F_1} - 1)} \\over 2}
    .. math:: {a_4} = {{({F_1} + 1)} \\over 2}
    .. math:: {a_5} = {{({F_1} - 1 + 2\\Delta )} \\over 2}
    .. math:: {a_6} = {{({F_1} + 1 - 2\\Delta )} \\over 2}

    :Reference: Optical Waveguide Theory, Snyder, Love, p. 250 , Table 12-3
        


    """



    def __init__(self, thickness, n_wg, n_medium, wavelength, inputPower, nu = 1, polarization = 'HE',calcMethod = 'authomatic'):



        self.wavelength = wavelength

        self.k = 2*np.pi / wavelength
        self.angularFrequency = self.k * constants.c


        self.inputPower = inputPower
        self.n_wg = n_wg
        self.n_medium = n_medium
        self.calcMethod = calcMethod


        self.nu = nu


        self.d = thickness
        self.a = self.d / 2  #fiber radius



        self.V = (np.pi * self.d)/ self.wavelength * np.sqrt((self.n_wg**2 \
                - self.n_medium**2))  #Reference: Optical Waveguide Theory, Snyder, Love, p 227 eqn 11-47

        self.Delta = 1/2. * (1 - self.n_medium**2 / n_wg**2)     #Reference: Snyder, Love, Optical Waveguide Theory, page 227, eqn 11-48

        self.eigenValueSolver()



        #refernece: Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3

        b_1 = 1/(2 * self.U) \
            * ( special.jv(self.nu -1,self.U) / special.jv(self.nu,self.U)\
            - special.jv(self.nu + 1,self.U) / special.jv(self.nu,self.U))
        b_2 = -1/(2 * self.W)\
            * ( special.kv(self.nu-1,self.W) / special.kv(self.nu,self.W)\
            + special.kv(self.nu +1,self.W)    / special.kv(self.nu,self.W) )

        self.b_1 = b_1
        self.b_2 = b_2


        #refernece: Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3

        self.F_1 = (self.U * self.W / self.V)**2 \
                * (b_1 + (1-2*self.Delta)*b_2) / self.nu
        self.F_2 = (self.V / (self.U * self.W))**2 * self.nu / (b_1 + b_2) 

        self.a_1 = (self.F_2 -1) /2.
        self.a_2 = (self.F_2 + 1) / 2
        self.a_3 = (self.F_1 -1) / 2
        self.a_4 = (self.F_1 + 1) /2
        self.a_5 = (self.F_1 - 1 + 2 * self.Delta) / 2
        self.a_6 = (self.F_1 + 1 - 2 * self.Delta) / 2


        self.normalized = self.normalize()



        # self.value = self.normalized*self.a * np.pi * self.angularFrequency*constants.eps_0\
        # *( (2* self.a_1**2 *special.jv(0,self.U)**2 \
        # + 2* self.a_2**2 *special.jv(2,self.U)**2) / special.jv(1,self.U)**2\
        # + self.U**2 / (self.a**2 * self.beta**2)* self.n_medium**2/self.n_wg**2)
       


##################################################
####Methods to solve the eigenvalue equations
###################################################
    

    def eigenValueSolver(self):
        """This is a function that solves the eigenvalue equation and calculates the Porpagation Parameters
        and the effective refractive index. The propagation parameters are:

        

        .. math:: U = a{\\left( {{k^2}n_{{\\rm{wg}}}^2 - {\\beta ^2}} \\right)^{1/2}} = a\\kappa 
        .. math:: W = a{\\left( {{\\beta ^2} - {k^2}n_{{\\rm{wg}}}^2} \\right)^{1/2}} = a\\gamma 


        :Reference: Love, Snyder, Optical Waveguide Theory, p.227 , eqn 11-49




        """


        self.kappa = lambda beta: np.sqrt(np.square(self.k) \
                * np.square(self.n_wg) - np.square(beta))



        if  (self.calcMethod != 'numercally acurate' \
            and  self.calcMethod != 'analythically' \
            and (self.V > np.pi * 600e-9 / 1530e-9 * np.sqrt(self.n_wg**2 - self.n_medium**2))) \
            or self.calcMethod == 'numercally':
            print 'numercally'
            
            self.epsilon_fiber = self.n_wg**2 * constants.eps_0
            self.epsilon_water = self.n_medium**2 * constants.eps_0
            self.kappa = lambda beta: np.sqrt(np.square(self.k) \
                * np.square(self.n_wg) - np.square(beta))
            self.gamma = lambda beta: np.sqrt(np.square(beta) -\
             np.square(self.k) * np.square(self.n_medium))

            N = np.linspace(self.n_medium+10e-6*self.n_medium,self.n_wg\
                -10e-6*self.n_wg,10000)
            beta_list = N * self.k  # a list of trial eigenvalues beta
            #the eigenvalue equation is rearanged so it should be equal to 0 if for the correct eigenvalue in the funcion f
            #thus the error says for each eigenvalue beta how close it is to zero and thus the real value of the beta
            
            error = self.f(beta_list)

    

            self.beta = self.findRightMin(error,beta_list) #calls a function to find the right mimimum of the error array and the corresponding beta
            self.gamma = self.gamma(self.beta)
    
        elif self.calcMethod != 'analythically'\
            and ( self.V <= np.pi * 600e-9 / 1530e-9 * np.sqrt(self.n_wg**2 - self.n_medium**2)\
            and self.V > np.pi * 400e-9 / 1530e-9 * np.sqrt(self.n_wg**2 - self.n_medium**2) ) \
            or self.calcMethod == 'numercally acurate':


            print 'numercally acurate'
            self.epsilon_fiber = self.n_wg**2 * constants.eps_0
            self.epsilon_water = self.n_medium**2 * constants.eps_0
            self.kappa = lambda beta: np.sqrt(np.square(self.k) \
                * np.square(self.n_wg) - np.square(beta))
            self.gamma = lambda beta: np.sqrt(np.square(beta) -\
             np.square(self.k) * np.square(self.n_medium))

            N = np.linspace(self.n_medium+10e-6*self.n_medium,self.n_wg\
                -10e-6*self.n_wg,100000)
            beta_list = N * self.k  # a list of trial eigenvalues beta
            #the eigenvalue equation is rearanged so it should be equal to 0 if for the correct eigenvalue in the funcion f
            #thus the error says for each eigenvalue beta how close it is to zero and thus the real value of the beta
            
            error = self.f(beta_list)

    

            self.beta = self.findRightMin(error,beta_list) #calls a function to find the right mimimum of the error array and the corresponding beta
            self.gamma = self.gamma(self.beta)



        elif self.V <= np.pi * 400e-9 / 1530e-9 * np.sqrt(self.n_wg**2 - self.n_medium**2) \
                or self.calcMethod == 'analythically':
        # #analythical appoximation works for small fibers
        #reference Tong, Sumetsky, Subwavelength and Nanometer Diameter Optical Fibers, p. 20 eqn: 2.10, 2.11
            print 'analythically'       
            self.gamma = 1.123 / self.a \
                        * np.exp(\
                        (self.n_wg**2 + self.n_medium**2) / (8*self.n_medium**2)\
                        - ((self.n_wg**2 + self.n_medium**2) \
                        / (self.n_medium**2 * (self.n_wg**2 - self.n_medium**2))
                        * self.wavelength**2 / (2* np.pi * self.a)**2) )



            self.beta = (2 * np.pi * self.n_medium) / self.wavelength \
                        + (self.gamma **2 * self.wavelength) \
                        / (4 * np.pi * self.n_medium)



        

        
        self.kappa = self.kappa(self.beta)

        self.U = self.a *np.sqrt((2*np.pi / self.wavelength)**2 * self.n_wg**2 \
                - self.beta**2)   #Reference: Optical Waveguide Theory, Snyder, Love, p 227 eqn 11-49
        self.W = self.a *np.sqrt( self.beta**2 \
                - (2*np.pi / self.wavelength)**2 * self.n_medium**2 )   #Reference: Optical Waveguide Theory, Snyder, Love, p 227 eqn 11-49




        self.n_eff = self.beta / self.k
        



        return 1






    def findRightMin(self, error, beta_list):
        """helper function used to solve the eigenvalue equation"""


        #the eigenvalue corresponding to the latest 0 corresponds to the eignevale which is searched for

        reversed_error = error[::-1] #reverses the error array
        try:
            start = np.where(reversed_error<0)[0][0] #finds the first negative value
        except:
            print('no negative value')
            return 0


        #from the first negative value the mimum is found by the assumption that the function is continously increasing
        #in this case the function can be foud by lookin at the absolute value as soon as it increase the function has passed the 0 position
        reversed_beta_list = beta_list[::-1]
        for i in range(start,len(reversed_error)):
            if i == len(reversed_error):
                print('no minimum found')
                break
            if abs(reversed_error[i])<abs(reversed_error[i+1]):
                return reversed_beta_list[i]

        return nan


    def f(self,beta):
        """
        helper function to to solve the eigenvalue equation
        uses diedrich mercuse light transmission optics p.296 eq.8.2-49
        but it is reformed in order that one side is zero """

        
        term1 = (self.epsilon_fiber/self.epsilon_water\
        * self.a*self.gamma(beta)**2/self.kappa(beta)\
        * self.derivative_bessel(self.nu,self.a * self.kappa(beta))\
        /special.jv(self.nu,self.a * self.kappa(beta))\
        + 1j *self.a*self.gamma(beta)\
        * self.derivative_hankel(self.nu,1j*self.a*self.gamma(beta))\
        /special.hankel1(self.nu,1j*self.a *self.gamma(beta)) )


        
        term2 =(self.a*self.gamma(beta)**2/self.kappa(beta)\
        * self.derivative_bessel(self.nu,self.a * self.kappa(beta))/special.jv(self.nu,self.a * self.kappa(beta))\
        + 1j *self.a*self.gamma(beta)\
        * self.derivative_hankel(self.nu,1j*self.a*self.gamma(beta))/special.hankel1(self.nu,1j*self.a *self.gamma(beta)) )

        
        term3 = (self.nu * (self.epsilon_fiber/self.epsilon_water-1)\
        * beta * self.k * self.n_medium/self.kappa(beta)**2)**2

        return term1 * term2 - term3


    def derivative_bessel(self,nu,z):
        return special.jv(nu-1,z)-nu/z *special.jv(nu,z)

    def derivative_hankel(self,nu,z):
        return special.hankel1(nu-1,z)-nu/z * special.hankel1(nu,z)









#####################################
#Electric Field
######################################
    
    #inside waveguide

    def  E_r(self,r,phi):
        """Calculates the electric field in radial direaction inside the waveguide at 
        a certain location in polar coordiates

        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        

        .. math:: {E_r} = {{{a_1}{{\\rm{J}}_{\\nu  - 1}}\\left( {U{r \\over a}} \\right) + {a_2}{{\\rm{J}}_{\\nu  + 1}}\\left( {U{r \\over a}} \\right)} \\over {{{\\rm{J}}_{{\\kern 1pt} \\nu }}\\left( U \\right)}}{f_\\nu }\\left( \\phi  \\right)

        :Reference: Optical Waveguide Theory, Snyder, Love, p. 250 , Table 12-3
        """

        E = - (self.a_1 * special.jv(self.nu -1,self.U * r /self.a)\
             + self.a_2*special.jv(self.nu + 1,self.U * r / self.a) )\
              / special.jv(self.nu,self.U) \
              * np.sin(self.nu * phi) ** (self.nu%2) \
              * np.cos(self.nu * phi) ** ((self.nu + 1)%2)  #if odd nu sin, if even nu cos







        return np.sqrt(self.normalized) * E


    def  E_phi(self,r,phi):
        """Calculates the electric field in angular direaction inside the waveguide at 
        a certain location in polar coordinates

        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]


        .. math:: {E_\\phi } =  - {{{a_1}{{\\rm{J}}_{\\nu  - 1}}\\left( {U{r \\over a}} \\right) - {a_2}{{\\rm{J}}_{\\nu  + 1}}\\left( {U{r \\over a}} \\right)} \\over {{{\\rm{J}}_{{\\kern 1pt} \\nu }}\\left( U \\right)}}{g_\\nu }\\left( \\phi  \\right)
        :Reference: Optical Waveguide Theory, Snyder, Love, p. 250 , Table 12-3
        """



        E = - (self.a_1 * special.jv(self.nu-1,self.U * r /self.a)\
             - self.a_2 * special.jv(self.nu + 1,self.U * r / self.a) )\
              / special.jv(self.nu,self.U) \
              * np.cos(self.nu * phi) ** (self.nu%2) \
              * (-np.sin(self.nu * phi)) ** ((self.nu + 1)%2) #if odd nu cos, if even nu -sin
        return np.sqrt(self.normalized) * E


    def  E_z(self,r,phi):
        """Calculates the electric field in propagation direaction inside the waveguide at
        a certain location in polar coordinates

        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: {E_z} = {{ - {\\rm{i}}U} \\over {a\\beta }}{{{{\\rm{J}}_\\nu }\\left( {U{r \\over a}} \\right)} \\over {{{\\rm{J}}_\\nu }\\left( U \\right)}}{f_\\nu }\\left( \\phi  \\right)

        :Refernece: Optical Waveguide Theory, Snyder, Love, p. 250 , Table 12-3
        """


        E = - 1j * self.U / (self.a * self.beta) \
            * special.jv(self.nu, self.U*r / self.a) / special.jv(self.nu,self.U) \
            * np.sin(self.nu * phi) ** (self.nu%2) \
            * np.cos(self.nu * phi) ** ((self.nu + 1)%2)  #if odd nu sin, if even nu cos
        return np.sqrt(self.normalized) * E



    #evanescent field

    def  Eev_r(self,r,phi):

        """Calculates the evanescent electric field in radial direaction outside the waveguide at
        a certain location in polar coordinates

        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: E_r^{{\\rm{ev}}} =  - {U \\over W}{{{a_1}{{\\rm{K}}_{\\nu  - 1}}\\left( {W{r \\over a}} \\right) - {a_2}{{\\rm{K}}_{\\nu  + 1}}\\left( {W{r \\over a}} \\right)} \\over {{{\\rm{K}}_{{\\kern 1pt} \\nu }}\\left( U \\right)}}{f_\\nu }\\left( \\phi  \\right)

        :Refernece: Optical Waveguide Theory, Snyder, Love, p. 250 , Table 12-3
        """



        E = - self.U / self.W \
             * (self.a_1 * special.kv(self.nu-1,self.W * r /self.a)\
             - self.a_2*special.kv(self.nu+1,self.W * r / self.a) )\
             / special.kv(self.nu,self.W) \
             * np.sin(self.nu * phi) ** (self.nu%2)\
              * np.cos(self.nu * phi) ** ((self.nu + 1)%2)  #if odd nu sin, if even nu cos

        return np.sqrt(self.normalized) * E




    def  Eev_phi(self,r,phi):
        """Calculates the evanescent electric field in angluar direaction outside the waveguide at
        a certain location in polar coordinates

        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: E_\\phi ^{{\\rm{ev}}} =  - {U \\over W}{{{a_1}{{\\rm{K}}_{\\nu  - 1}}\\left( {W{r \\over a}} \\right) + {a_2}{{\\rm{K}}_{\\nu  + 1}}\\left( {W{r \\over a}} \\right)} \\over {{{\\rm{K}}_{{\\kern 1pt} \\nu }}\\left( W \\right)}}{g_\\nu }\\left( \\phi  \\right)

        :Reference: Optical Waveguide Theory, Snyder, Love, p. 250 , Table 12-3
        """

        E = - self.U / self.W \
            *(self.a_1 * special.kv(self.nu -1,self.W * r /self.a)\
             + self.a_2*special.kv(self.nu + 1,self.W * r / self.a) )\
             / special.kv(self.nu,self.W)\
             * np.cos(self.nu * phi) ** (self.nu%2) \
             * (-np.sin(self.nu * phi)) ** ((self.nu + 1)%2) #if odd nu cos, if even nu -sin
        return np.sqrt(self.normalized) * E



    def  Eev_z(self,r,phi):
        """Calculates the evanescent electric field in propagation direaction outside the waveguide at
        a certain location in polar coordinates


        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: E_z^{{\\rm{ev}}} = {{ - {\\rm{i}}U} \\over {a\\beta }}{{{{\\rm{K}}_\\nu }\\left( {W{r \\over a}} \\right)} \\over {{{\\rm{K}}_\\nu }\\left( W \\right)}}{f_\\nu }\\left( \\phi  \\right)

        :Reference: Optical Waveguide Theory, Snyder, Love, p. 250 , Table 12-3
        """

        E = - 1j * self.U / (self.a * self.beta) \
            * special.kv(self.nu, self.W*r / self.a) / special.kv(self.nu,self.W) \
            * np.sin(self.nu * phi) ** (self.nu%2) \
            * np.cos(self.nu * phi) ** ((self.nu + 1)%2)   #if odd nu sin, if even nu cos
        return np.sqrt(self.normalized) * E


#####################################
#Magnetic Field
######################################

    #inside waveguide

    def H_r(self,r,phi):
        """Calculates the magnetic field in radial direaction inside the waveguide at
        a certain location in polar coordinates


        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: {H_r} = {\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over \\beta }{{{a_3}{{\\rm{J}}_{\\nu  - 1}}\\left( {U{r \\over a}} \\right) - {a_4}{{\\rm{J}}_{\\nu  + 1}}\\left( {U{r \\over a}} \\right)} \\over {{{\\rm{J}}_{{\\kern 1pt} \\nu }}\\left( U \\right)}}{g_\\nu }\\left( \\phi  \\right)

        :Reference: Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3
        """
        H = np.sqrt(constants.eps_0 / constants.mu_0 ) \
            * self.k * self.n_wg**2 /self.beta \
            * ( self.a_3 * special.jv(self.nu-1,self.U * r / self.a) \
               - self.a_4 * special.jv(self.nu+1,self.U * r / self.a) )\
            / special.jv(self.nu, self.U) \
            * np.cos(self.nu * phi) ** (self.nu%2) \
             * (-np.sin(self.nu * phi)) ** ((self.nu + 1)%2)   #if odd nu cos, if even nu sin

        return np.sqrt(self.normalized) * H



    def H_phi(self,r,phi):
        """Calculates the magnetic field in angular direaction inside the waveguide at
        a certain location in polar coordinates


        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: {H_{phi}} =  - {\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over \\beta }{{{a_3}{{\\rm{J}}_{\\nu  - 1}}\\left( {U{r \\over a}} \\right) + {a_4}{{\\rm{J}}_{\\nu  + 1}}\\left( {U{r \\over a}} \\right)} \\over {{{\\rm{J}}_{{\\kern 1pt} \\nu }}\\left( U \\right)}}{f_\\nu }\\left( \\phi  \\right)

        :Reference:: Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3
        """
        H = - np.sqrt(constants.eps_0 / constants.mu_0 ) \
            * self.k * self.n_wg**2 /self.beta \
            * ( self.a_3 * special.jv(self.nu-1,self.U * r / self.a) \
               + self.a_4 * special.jv(self.nu+1,self.U * r / self.a) )\
            / special.jv(self.nu, self.U) \
            * np.sin(self.nu * phi) ** (self.nu%2) \
            * np.cos(self.nu * phi) ** ((self.nu + 1)%2)   #if odd nu sin, if even nu cos

        return np.sqrt(self.normalized) * H 


    def H_z(self,r,phi):
        """Calculates the magnetic field in propagation direaction inside the waveguide at
        a certain location in polar coordinates


        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: {H_z} =  - {\\rm{i}}{\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{U{F_2}} \\over {ka}}{{{{\\rm{J}}_\\nu }\\left( {U{r \\over a}} \\right)} \\over {{{\\rm{J}}_\\nu }\\left( U \\right)}}{g_\\nu }\\left( \\phi  \\right)

        :Reference:: Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3
        """

        H = - 1j * np.sqrt(constants.eps_0 / constants.mu_0 ) \
            * self.U * self.F_2 / (self.k * self.a) \
            * special.jv(self.nu,self.U * r / self.a) \
            / special.jv(self.nu,self.U) \
            * np.cos(self.nu * phi) ** (self.nu%2) \
             * (-np.sin(self.nu * phi)) ** ((self.nu + 1)%2)   #if odd nu cos, if even nu sin

        return np.sqrt(self.normalized) * H 


    #evanescent field

    def Hev_r(self,r,phi):
        """Calculates the evanescent magnetic field in radial direaction outside the waveguide at
        a certain location in polar coordinates


        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: H_r^{{\\rm{ev}}} = {\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over \\beta }{U \\over W}{{{a_5}{{\\rm{K}}_{\\nu  - 1}}\\left( {W{r \\over a}} \\right) + {a_6}{{\\rm{K}}_{\\nu  + 1}}\\left( {W{r \\over a}} \\right)} \\over {{{\\rm{K}}_{{\\kern 1pt} \\nu }}\\left( W \\right)}}{g_\\nu }\\left( \\phi  \\right)

        :Reference: Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3
        """

        H = np.sqrt(constants.eps_0 / constants.mu_0 ) \
            * self.k * self.n_wg**2 * self.U / (self.beta * self.W) \
            * ( self.a_5 * special.kv(self.nu-1,self.W * r / self.a) \
               + self.a_6 * special.kv(self.nu+1,self.W * r / self.a) )\
            / special.kv(self.nu, self.W) \
            * np.cos(self.nu * phi) ** (self.nu%2) \
             * (-np.sin(self.nu * phi)) ** ((self.nu + 1)%2)   #if odd nu cos, if even nu sin


        return np.sqrt(self.normalized) * H



    def Hev_phi(self,r,phi):
        """Calculates the evanescent magnetic field in angular direaction outside the waveguide at
        a certain location in polar coordinates


        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: H_\\phi ^{{\\rm{ev}}} =  - {\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over \\beta }{U \\over W}{{{a_5}{{\\rm{K}}_{\\nu  - 1}}\\left( {W{r \\over a}} \\right) - {a_6}{{\\rm{K}}_{\\nu  + 1}}\\left( {W{r \\over a}} \\right)} \\over {{{\\rm{K}}_{{\\kern 1pt} \\nu }}\\left( W \\right)}}{f_\\nu }\\left( \\phi  \\right)

        :Reference:  Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3
        """

        H = - np.sqrt(constants.eps_0 / constants.mu_0 ) \
            * self.k * self.n_wg**2 * self.U / (self.beta * self.W) \
            * ( self.a_5 * special.kv(self.nu-1,self.W * r / self.a) \
               - self.a_6 * special.kv(self.nu+1,self.W * r / self.a) )\
            / special.kv(self.nu, self.W) \
            * np.sin(self.nu * phi) ** (self.nu%2) \
            * np.cos(self.nu * phi) ** ((self.nu + 1)%2)   #if odd nu sin, if even nu cos

        return np.sqrt(self.normalized) * H 


    def Hev_z(self,r,phi):
        """Calculates the evanescent magnetic field in propagation direaction outside the waveguide at
        a certain location in polar coordinates


        :param r: radial coordinate
        :type r: float[:]
        :param phi: angular coordinate
        :type phi: float[:]

        .. math:: {H_z} =  - {\\rm{i}}{\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{U{F_2}} \\over {ka}}{{{{\\rm{K}}_\\nu }\\left( {W{r \\over a}} \\right)} \\over {{{\\rm{K}}_\\nu }\\left( W \\right)}}{g_\\nu }\\left( \\phi  \\right)

        :Reference: Optical Waveguide Theory, Snyder, Love , p. 250 , Table 12-3
        """
        H = - 1j * np.sqrt(constants.eps_0 / constants.mu_0 ) \
            * self.U * self.F_2 / (self.k * self.a) \
            * special.jv(self.nu,self.W * r / self.a) \
            / special.jv(self.nu,self.W) \
            * np.cos(self.nu * phi) ** (self.nu%2) \
             * (-np.sin(self.nu * phi)) ** ((self.nu + 1)%2)   #if odd nu cos, if even nu sin

        return np.sqrt(self.normalized) * H 


#####################################
#Pointing Vector
#######################################

#only the z component is non-zero


    def normalize(self):
        """This function is used to normalize the Power and the fields corresponding to the
        input power

        .. math:: {N_{{\\rm{wg}}}} = {{\\pi {a^2}} \\over 2}{\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over {\\beta {\\rm{J}}_\\nu ^2\\left( U \\right)}}\\left( {{a_1}{a_3}\\left( {{\\rm{J}}_{\\nu  - 1}^2\\left( U \\right) - {\\rm{J}}_\\nu ^2\\left( U \\right){\\rm{J}}_{\\nu  - 2}^2\\left( U \\right)} \\right) + \\left( {{a_2}{a_4}{\\rm{J}}_{\\nu  + 1}^2\\left( U \\right){\\rm{ - }}{{\\rm{J}}_\\nu }\\left( U \\right){{\\rm{J}}_{\\nu  + 2}}\\left( U \\right)} \\right)} \\right)
        .. math:: {N_{{\\rm{ev}}}} =  - {{\\pi {a^2}} \\over 2}{\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over {\\beta K_\\nu ^2\\left( W \\right)}}{{{U^2}} \\over {{W^2}}}\\left( {{a_1}a5\\left( {{\\rm{K}}_{\\nu  - 1}^2\\left( W \\right) - K_\\nu ^2\\left( W \\right){\\rm{K}}_{\\nu  - 2}^2\\left( W \\right)} \\right) + \\left( {{a_2}{a_6}{\\rm{K}}_{\\nu  + 1}^2\\left( W \\right){\\rm{ - }}{{\\rm{K}}_\\nu }\\left( W \\right){{\\rm{K}}_{\\nu  + 2}}\\left( W \\right)} \\right)} \\right)

        :Refernece: Love, Sneider, Optical Waveguide Theory, p. 256 Table 12-5        """


        N_wg = np.pi * self.a**2 / 2 * np.sqrt(constants.eps_0/constants.mu_0) \
                *self.k * self.n_wg**2 \
                / (self.beta * special.jv(self.nu, self.U)**2) \
                * (self.a_1 * self.a_3 * (special.jv(self.nu-1, self.U)**2 \
                                         -special.jv(self.nu,self.U)\
                                             * special.jv(self.nu-2, self.U))\
                    + self.a_2 * self.a_4 * (special.jv(self.nu+1, self.U)**2 \
                                             -special.jv(self.nu,self.U)\
                                             * special.jv(self.nu+2, self.U)))


        N_ev = - np.pi*self.a**2 / 2 * np.sqrt(constants.eps_0/constants.mu_0) \
                *self.k * self.n_wg**2 \
                 / (self.beta * special.kv(self.nu, self.W)**2) \
                * self.U**2 / self.W**2 \
                * (self.a_1 * self.a_5 * (special.kv(self.nu-1, self.W)**2 \
                                         -special.kv(self.nu,self.W)\
                                             * special.kv(self.nu-2, self.W))\
                    + self.a_2 * self.a_6 * (special.kv(self.nu+1, self.W)**2 \
                                             -special.kv(self.nu,self.W)\
                                             * special.jv(self.nu+2, self.W)))


        return self.inputPower / (N_ev + N_wg)





    def S_z(self,r,phi):
        """This function calculates the Poynting Vector in porpagation direction inside the waveguide(in all other directions the 
        Poynting Vector is zero) at a certain location in polar coordinates
        for the HE not the EH polarization
        

        :param r: radial coordinate
        :type r: float
        :param phi: angular coordinate
        :type phi: float

        .. math:: {S_z} = {1 \\over 2}{\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over {\\beta {\\rm{J}}_\\nu ^2\\left( U \\right)}}\\left( {{a_1}{a_3}{\\rm{J}}_{\\nu  - 1}^2\\left( {U{r \\over a}} \\right) + {a_2}{a_4}{\\rm{J}}_{\\nu  + 1}^2\\left( {U{r \\over a}} \\right) + {{1 - {F_1}{F_2}} \\over 2}{{\\rm{J}}_{\\nu  - 1}}\\left( {U{r \\over a}} \\right){{\\rm{J}}_{\\nu  + 1}}\\left( {U{r \\over a}} \\right)\\cos \\left( {2\\nu \\phi } \\right)} \\right)


        :Reference: Love, Sneider, Optical Waveguide Theory, p. 256 Table 12-5
        """

        S =  1/2. * np.sqrt(constants.eps_0 / constants.mu_0) \
            * self.k*self.n_wg**2 / (self.beta*special.jv(self.nu,self.U)**2) \
            *( self.a_1 * self.a_3 * special.jv(self.nu-1, self.U*r/self.a)**2 \
             + self.a_2 * self.a_4 * special.jv(self.nu+1, self.U*r/self.a)**2 \
             + (1 - self.F_1*self.F_2) / 2 \
                 * special.jv(self.nu-1, self.U*r/self.a) \
                 * special.jv(self.nu+1, self.U*r/self.a) \
                 * np.cos(2*phi)  )

        return self.normalized * S






    def Sev_z(self,r,phi):
        """This function calculates the evanecent Poynting Vector in porpagation direction outside the waveguide (in all other directions the 
        Poynting Vector is zero) at a certain location in polar coordinates
        for the HE not the EH polarization
        

        :param r: radial coordinate
        :type r: float
        :param phi: angular coordinate
        :type phi: float

        .. math:: S_z^{{\\rm{ev}}} = {1 \\over 2}{\\left( {{{{\\varepsilon _0}} \\over {{\\mu _0}}}} \\right)^{1/2}}{{kn_{{\\rm{wg}}}^2} \\over {\\beta K_\\nu ^2\\left( W \\right)}}{{{U^2}} \\over {{W^2}}}\\left( {{a_1}{a_5}{\\rm{K}}_{\\nu  - 1}^2\\left( {W{r \\over a}} \\right) + {a_2}{a_6}{\\rm{K}}_{\\nu  + 1}^2\\left( {W{r \\over a}} \\right) - {{1 - 2\\Delta  - {F_1}{F_2}} \\over 2}{{\\rm{K}}_{\\nu  - 1}}\\left( {W{r \\over a}} \\right){{\\rm{K}}_{\\nu  + 1}}\\left( {W{r \\over a}} \\right)\\cos \\left( {2\\nu \\phi } \\right)} \\right)



        :Reference: Love, Sneider, Optical Waveguide Theory, p. 256 Table 12-5
        """

        S = 1/2. * np.sqrt(constants.eps_0 / constants.mu_0) \
            * self.k*self.n_wg**2 / (self.beta*special.kv(self.nu,self.W)**2) \
            * self.U**2 / self.W**2 \
            *( self.a_1 * self.a_5 * special.kv(self.nu-1, self.W*r/self.a)**2 \
             + self.a_2 * self.a_6 * special.kv(self.nu+1, self.W*r/self.a)**2 \
             - (1 - 2*self.Delta - self.F_1*self.F_2) / 2 \
                 * special.kv(self.nu-1, self.W*r/self.a) \
                 * special.kv(self.nu+1, self.W*r/self.a) \
                 * np.cos(2*phi)  )





        return self.normalized * S







#############################################
###additional propertgies of a waveguide
##################################################

    def numberOfModes(self):
        """Function that returns the number of modes allowed in the waveguide
        this is only valid for large V

        .. math:: {V^2 \\over 2}
    
        """




        return self.V**2 / 2

    def singleModeThickness(self):
        """Function that returns the thickness at which the waveguide would be
        single mode for this input frequency. All thickness below this value resut in single mode

        .. math:: {d_{{\\rm{sm}}}} = {{2.4\\lambda } \\over {\\pi \\sqrt {n_{wg}^2 - n_{medium}^2} }}

        

        :Reference: Tong, Sumetsky, Subwavelength and Nanometer Diameter Optical Fibers p.18 eqn 2.7

        """

        return 2.4 * self.wavelength / (np.pi * np.sqrt(self.n_wg**2 - self.n_medium**2))


    def singleModeWavelength(self):
        """Function that returns the wavlength at which the waveguide would be
        single mode for this thickness. All wavelength above this value result in single mode



        .. math:: {\\lambda _{{\\rm{sm}}}} = {{\\pi d\\sqrt {n_{wg}^2 - n_{medium}^2} } \\over {2.4}}


        :Reference: Tong, Sumetsky, Subwavelength and Nanometer Diameter Optical Fibers p.18 eqn 2.7


        """

        return (np.pi * self.d * np.sqrt(self.n_wg**2 - self.n_medium**2)) / 2.4





    def d_eff(self,calc = 'numerically',stepsize = 1e-9):

        """
        Returns the effective thickness of a cyrcular waveguide
        calculation takes a while but can be sped up by decreasing
        the accuracy and increasing the stepsize
        it could further be sped up by implementing the Marcues Formula
        https://www.rp-photonics.com/mode_radius.html

        :param calc: determies wether it should be calculated analythically or numerically analythical is not valid for V<1
        :type calc: string (numerically)

        :param stepsize: the step size in each numerical operation 
        :type stepsize: float = 1e-9
        
        :Reference: Tong, Sumetsky, Subwavelength and Nanometer Diameter Optical Fibers p. 24
        """

        if calc == 'numerically':

            #the r is needed for the integration
            fun = lambda r,phi: self.S_z(r,phi) * r
            fun_ev = lambda r,phi: self.Sev_z(r,phi) * r

            #power inside the waveguide
            power_in = integrate.dblquad(\
                            fun,0,2*np.pi,lambda x: 0, lambda x: self.a)[0]
            
            #initializing for the loop
            power = power_in
            aeff = self.a
          
            
            #in this case deff is larger than the thickness of the fiber and one has to integrate over the evanescent field
            #deff is the radius in which 0.865 of the power is confined
            if power_in / self.inputPower < 0.865:
                #as long as the power confined in the diameter is smaller tahn 0.865 the effective diameter must be larger
                while power / self.inputPower < 0.865:
                    aeff = aeff + stepsize
                    power = power_in \
                            + integrate.dblquad(\
                            fun_ev,0,2*np.pi,lambda x: self.a, lambda x: aeff)[0]

            #in this case deff issmaller than the thickness of the fiber and one has to integrate
            elif power_in / self.inputPower > 0.865: 
                #as long as the power confined in the diameter is  larger than 0.865 the effective diameter must be smaler
                while power / self.inputPower > 0.865:
                    aeff = aeff - stepsize
                    power = power_in \
                            + integrate.dblquad(\
                             fun,0,2*np.pi,lambda x: self.a, lambda x: aeff)[0]

        deff = 2 * aeff 


        if calc == 'analythically':
            deff = 2 * self.a * (0.65 + 1.619 / self.V**(3./2) + 2.879 / self.V**6)               

        return deff







if __name__ == '__main__':
    sys.path.insert(0, 'C:\Users\\blyve\Documents\PHD\GitLab\Focal-molography\\')
    import materials.getData as RefInd  


    wavelength = 405e-9
    inputPower = 0.04 #[W]
    n_wg = 1.45
    # n_medium = RefInd.get_data('C:\Users\\blyve\Documents\PHD\GitLab\Focal-molography\materials\database\main\H2O\Hale.yml',wavelength*1e6)[0].real
    n_medium = RefInd.get_data('..\materials\database\main\H2O\Hale.yml',wavelength*1e6)[0].real
    print n_medium
    # n_medium = 1.33
    d =1440e-9
    r_in = np.linspace(0,d/2.,100)
    r_ev = np.linspace(d/2.,d/2.+200e-9,100)
    phi = np.linspace(0,np.pi*2.,100)

    print 0.6**2/0.001**2
 

    wg = CylindricalWaveguide(d,n_wg,n_medium,wavelength,inputPower)
    print 'number of modes', wg.numberOfModes()
    print 'thickness single mode', wg.singleModeThickness()
    print wg.V
    # print wg.d_eff()
    # Intensity = wg.Eev_r(r,phi)*np.conjugate(wg.Hev_r(r,phi))+wg.Eev_phi(r,phi)*np.conjugate(wg.Hev_phi(r,phi)) +wg.Eev_z(r,phi)*np.conjugate(wg.Hev_z(r,phi))

    # Intensity_ev= 1/(2.*constants.Z_w) *(np.abs(wg.Eev_r(r,phi)*np.conjugate(wg.Eev_r(r,phi))) + np.abs(wg.Eev_phi(r,phi)*np.conjugate(wg.Eev_phi(r,phi))) + np.abs(wg.Eev_z(r,phi)*np.conjugate(wg.Eev_z(r,phi))) ) 
    # plt.plot(phi, Intensity )ds
    phi = 1
    dr = np.linspace(0,d/2.,100)
    plt.plot(dr,wg.S_z(dr,phi))
    plt.show()
    print '0', wg.S_z(0,phi)
    print 'd', wg.S_z(d/2.,phi)
    print '0/d', wg.S_z(d/2.,phi)/wg.S_z(0,phi)

    # print 'coreRadius', d/2
    # print 'angularFrequency', wg.angularFrequency
    # print 'beta', wg.beta
    # print 'gamma', wg.gamma
    # print 'kappa', wg.kappa
    # print 'n_wg', n_wg
    # print 'n_medium', n_medium
    # print 'nu', wg.nu
    # print 'inputPower', wg.inputPower

    # # plt.plot(r_in, wg.S_z(r_in,phi))
    # # print(wg.normalized)
    # # plt.show()

    # # print(wg.S_z(d/2,phi),wg.Sev_z(d/2,phi))
    # # print(wg.S_z(r_in,phi))
    # # print(wg.S_z(0,phi),wg.S_z(2e-4,phi))

    # plt.plot(r_in,wg.E_r(r_in,phi))
    # plt.plot(r_out, wg.Eev_r(r_out,phi))
    # # plt.plot(r_out, wg.Sev_z(r_out,phi))
    # # plt.plot(r_in, 1/2. * (wg.E_r(r_in,phi) * wg.H_phi(r_in,phi) - wg.E_phi(r_in,phi) * wg.H_r(r_in,phi)))
    # # plt.plot(r_out , 1/2. * (wg.Eev_r(r_out,phi) * wg.Hev_phi(r_out,phi) - wg.Eev_phi(r_out,phi) * wg.Hev_r(r_out,phi)))
    # plt.show()
    # # # fun = lambda r,phi: wg.E_phi(r,phi)*wg.E_phi(r,phi)

    # # # print 'p', integrate.dblquad(fun,0,2*np.pi,lambda x: 0, lambda x: d/2)[0]

    # # fun = lambda r, phi: wg.S_z(r,phi) * r
    # # fun2 = lambda r,phi: wg.Sev_z(r,phi) * r
    # # P_in = integrate.dblquad(fun, 0,2*np.pi,lambda x: 0, lambda x: d/2)[0] 
    # # print np.pi*(d/2.)**2
    # # print 'P_in', P_in
    # # print 'normalization', wg.normalized
    # # P_out = integrate.dblquad(fun2,0,2*np.pi,lambda x: d/2, lambda x: 10e-5)[0]
    # # print P_in + P_out

    
    # print wg.numberOfModes()
    # print wg.singleModeThickness()
    # print wg.singleModeWavelength()

    # options = {'limit':100}
    # P_in = integrate.nquad(wg.S_z,[[0,d/2],[0,2*np.pi]],opts=[options,options])[0]
    # P_out = integrate.nquad(wg.S_z,[[d/2,5e-5],[0,2*np.pi]],opts=[options,options])[0]
    

    # print(P_in + P_out)
    # plt.show()




