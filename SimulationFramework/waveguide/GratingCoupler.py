import numpy as np
from scipy.optimize import minimize
import math
import matplotlib.pylab as plt

import sys
sys.path.append("../")
from waveguide import SlabWaveguide

"""
:author:       Andreas Frutiger
:revised:      10-Jan-2017
:reference:    PhD Thesis Florian Kehl, ETH Zuerich

"""

class Coupling_Grating(object):

	"""This is a class that calculates the coupling angle of a given grating coupler for a certain mode and laser wavelength.
		:params waveguide: SlabWaveguide object
		:params readout_wl:	Wavelength [m]
		:params Lambda: Period of the coupling grating"""

	def __init__(self,waveguide,readout_wl,Lambda):

		self.waveguide = waveguide;
		self.readout_wl = readout_wl
		self.Lambda = Lambda


	def calcCouplAngle(self,m,n_c=1):
		"""This formula calculates the coupling angle of a grating coupler on a waveguide for a given order and mode.

		:params m: order of the radiation mode.
		:params n_c: default 1, is the cover index.

		.. math:: \\theta  = \\arcsin \\left( \\frac{\\beta _n}{k_n} \\right) = \\arcsin \\left( \\frac{N}{n_c} + \\frac{m \\cdot \\lambda}{\\Lambda n_c} \\right)

		.. math:: {\\beta _n} = {\\beta _0} + {{2 \\cdot m \\cdot \\pi } \\over \\Lambda }

		.. math:: {\\beta _0} = N \\cdot {k_n}

		.. math:: {k_n} = {{2 \\cdot \\pi } \\over \\lambda }n_c

		"""

		k_n = 2*np.pi/self.readout_wl*n_c
		beta_0 = self.waveguide.N*k_n


		beta_n = beta_0 + 2*m*np.pi/self.Lambda

		self.theta = np.arcsin(beta_n/k_n)

		return self.theta

	def calcCouplPeriod(self,theta,n_c=1,m=1):
		"""This formula calculates the period of a grating coupler on a waveguide for a given coupling mode. Theta is given in radians. Return the grating period in m.

		:params theta: coupling angle in radians.
		:params n_c: default 1, is the cover index.
		:params m: order of the radiation mode (default first order)

		"""
		self.Lambda = m * self.waveguide.wavelength / (self.waveguide.N-n_c*np.sin(theta))
		return self.Lambda

	def calcCouplEfficiencyGF(self,t_m,Coupling_length):
		"""Calculates the coupling efficiency for a certain coupling length and a depth t_m of the coupling grating.
		
		.. math:: \\frac{{{P_{diff}}}}{{{P_{in}}}} = \\frac{{{\\pi ^2}}}{{4{\\lambda ^2}}}\\frac{{{t_m}^2}}{{{{\\left( {1 - \\frac{{{N^2}}}{{{n_c}^2}} + 2\\frac{N}{{{n_c}}}\\varphi  - {\\varphi ^2}} \\right)}^{\\frac{1}{2}}}}}\\frac{{{{\\left( {{n_m}^2 - {n_c}^2} \\right)}^2}}}{{N{n_c}}}\\frac{{\\left( {{n_f}^2 - {N^2}} \\right)}}{{\\left( {{n_f}^2 - {n_c}^2} \\right)}}\\frac{L}{{{t_{eff}}}}

		"""
		N = self.waveguide.N
		n_c = self.waveguide.n_c
		n_f =self.waveguide.n_f
		n_s = self.waveguide.n_s

		n_m = self.waveguide.n_f
		wl = self.readout_wl
		t_eff = self.waveguide.calcEffThickness(wl)
		Lambda = self.Lambda

		print Lambda

		phi = wl/(Lambda*n_c)

		C_GF = 1/(N*n_c) * \
		(n_f**2-N**2)/(n_f**2-n_c**2)* \
		1./np.sqrt(1-N**2/n_c**2+2*N/n_c*phi-phi**2)

		return (n_m**2-n_c**2)**2*C_GF*np.pi**2/(4*wl**2)*t_m**2*(Coupling_length/t_eff)

	def plotAngleVsPeriod(self, angle_min,angle_max):
		"""This function evaluates the variation of the grating period between angle_min and angle_max.

		:params angle_min: lower angle, in degrees
		:params angle_max: upper angle, in degrees

		"""
		periods=[]
		inde = 0
		theta = np.radians(-20)
		angles = np.linspace(angle_min,angle_max,1000)
		while inde < len(angles) :
			radAngle = np.radians(angles[inde])
			period = Grating.calcCouplPeriod(radAngle)
			periods.append(period)
			inde = inde + 1
		periods[:] = [x * 1e9 for x in periods]
		fig = plt.figure(1,figsize = (7,7),facecolor='white')
		p1 = plt.plot(angles,periods,'-r')
	   	plt.title("Angle variation at n_f = "+ repr(Grating.waveguide.n_f) + " and d = " + repr(round(Grating.waveguide.d_f*1e9,2))+" nm at " + repr(wavelength*1e9) + " nm")
	   	plt.xlabel('Angles (deg)')
	   	plt.ylabel('Grating Coupler Period [nm]')
	   	g1 = plt.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
	   	g2 = plt.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
	   	plt.minorticks_on()
	   	plt.show()

	def plotNVsPeriod(self, nf_min, nf_max):
		"""This function evaluates the variation of the grating period between nf_min and nf_max. The coupling angle is set by default at -20 deg, and the film thickness at 150 nm.

		:params nf_min: film lower refractive index
		:params nf_max: film upper refractive index

		"""
		deg = -20
		theta = np.radians(deg)
		n_fs = np.linspace(n_min,n_max,1000)
		thickness = 150e-9

		periods = []
		Ns = []
		inde = 0
		while inde < len(n_fs) :
			var = n_fs[inde]
			WG = SlabWaveguide(name='Silicon Nitride', n_s=1.521, n_f=var, n_c=1.33, d_f=thickness,wavelength = 405e-9,polarization='TE', mode=0,inputPower=0.3*2e-3,)
			Grating = Coupling_Grating(WG,wavelength,357e-9)
			period = Grating.calcCouplPeriod(theta)
			periods.append(period)
			Ns.append(Grating.waveguide.N)
			inde = inde + 1
		periods[:] = [x * 1e9 for x in periods]
		fig = plt.figure(1,figsize = (7,7),facecolor='white')
		p1 = plt.plot(n_fs,periods,'-r')
	   	plt.title("N_f variation at theta = "+ repr(deg) + " deg and d = " + repr(thickness*1e9)+" nm")
	   	plt.xlabel('n_f')
	   	plt.ylabel('Grating Coupler Period [nm]')
	   	g1 = plt.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
	   	g2 = plt.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
	   	plt.minorticks_on()
	   	plt.show()

	   	fig = plt.figure(1,figsize = (7,7),facecolor='white')
		p1 = plt.plot(Ns,periods,'-r')
	   	plt.title("N_eff variation at theta = "+ repr(deg) + " deg and d = " + repr(thickness*1e9)+" nm")
	   	plt.xlabel('N_eff')
	   	plt.ylabel('Grating Coupler Period [nm]')
	   	g1 = plt.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
	   	g2 = plt.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
	   	plt.minorticks_on()
	   	plt.show()

   	def plotDVsPeriod(self, d_min, d_max):
   		"""This function evaluates the variation of the grating period between nf_min and nf_max. The coupling angle is set by default at -20 deg, and the film refractive index at 1.9.

		:params d_min: film lower thickness, in m
		:params d_max: film upper thickness, in m

		"""
		deg = -20
   		nf = 1.9
		theta = np.radians(deg)
		d_fs = np.linspace(d_min,d_max,1000)

		periods = []

		inde = 0
		while inde < len(d_fs) :
			var = d_fs[inde]
			WG = SlabWaveguide(name='Silicon Nitride', n_s=1.521, n_f=nf, n_c=1.33, d_f=var,wavelength = 405e-9,polarization='TE', mode=0,inputPower=0.3*2e-3,)
			Grating = Coupling_Grating(WG,wavelength,357e-9)
			period = Grating.calcCouplPeriod(theta)
			periods.append(period)
			inde = inde + 1
		periods[:] = [x * 1e9 for x in periods]
		d_fs[:] = [x * 1e9 for x in d_fs]
		fig = plt.figure(1,figsize = (7,7),facecolor='white')
		p1 = plt.plot(d_fs,periods,'-r')
	   	plt.title("Thickness variation at theta = "+ repr(deg) + " deg and n_f = " + repr(nf)+ " nm")
	   	plt.xlabel('Film Thickness d_f [nm]')
	   	plt.ylabel('Grating Coupler Period [nm]')
	   	g1 = plt.grid(b=True, which='major', color='k', linestyle='-', linewidth=0.5)
	   	g2 = plt.grid(b=True, which='minor', color='k', linestyle='-', linewidth=0.2)
	   	plt.minorticks_on()
	   	plt.show()



if __name__ == '__main__':

	wavelength = 532e-9
	grating_period = 386e-9
	ta2O5_refractive_index = 2.14 # green
	#ta2O5_refractive_index = 2.117 # red

	WG = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=ta2O5_refractive_index, n_c=1.33, d_f=145e-9,wavelength = wavelength,polarization='TE', mode=0,inputPower=0.3*2e-3,)

	WG.calcEffRefractiveIndex(1.7,wavelength)


	Grating = Coupling_Grating(WG,wavelength,grating_period)

	print('The possible outcoupling periods')
	print(Grating.calcCouplPeriod(-89.999/180*np.pi,n_c=1.33))
	print(Grating.calcCouplPeriod(89.999/180*np.pi,n_c=1.33))

	print('Coupling angle')
	print(Grating.calcCouplAngle(-1)*360/(2*np.pi))

	print(Grating.calcCouplEfficiencyGF(12e-9,336e-6))


	#added part by Pierre-Louis Ronat

	wavelength = 633e-9
	n_f = 1.9

	WG = SlabWaveguide(name='Silicon Nitride', n_s=1.521, n_f=n_f, n_c=1, d_f=150e-9,wavelength = wavelength,polarization='TE', mode=0,inputPower=0.3*2e-3,)

	WG.calcEffRefractiveIndex(1.55,wavelength)

	Grating = Coupling_Grating(WG,wavelength,357e-9)

	#Grating.plotAngleVsPeriod(-60,20)
	#Grating.plotNVsPeriod(1.8,2)
	#Grating.plotDVsPeriod(100e-9,200e-9)

