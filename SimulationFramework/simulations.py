# -*- coding: utf-8 -*-
from os import sys, path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import numpy as np
from numpy import *
import scipy as sp
import numpy.matlib
from SharedLibraries.Database.dataBaseOperations import saveDB,loadDB

# constants and materials
import auxiliaries.constants as constants           # constants
# dipole field calculations
from dipole.dipoleField import calcField, potentialsUpper, potentialsLower
from dipole.calculationMethods import *
from dipole.plots import plotParameter, findCoordinates
# polarization and dipole moments
import dipole.dipole as dipole
# configuration setup, Fresnel formulae, Snells law
from interfaces.configuration import Media, Configuration
# electromagnetic fields, phase-shifts
import auxiliaries.fields as fields
from binder.concentrationConverter import formatMass, formatConcentration, formatSurfaceMassModulation
# limit of detection calculations
from waveguidebackground.background import backgroundScatteringWaveguide
from mologram.generatePattern import placeIncScatterer  # coherent pattern
from mologram.generateMask import Mask
from waveguide.waveguide import SlabWaveguide           # waveguide calculations
from mologram.mologram import Mologram              # focal molography functions
from auxiliaries.screen import defineScreen
from auxiliaries.airy import findAiryDiskDiameter
from materials.materials import Particle, IsotropicMaterial, PlaneMaterial
from imageProcessing.MologramProcessing import calc_IntAiryDisk, c_mask
from variables import FieldSimulationResults,commonKeys
from binder.Biosensor import MologramBiosensor
from binder.Adlayer import ProteinAdlayer
# time and date
import datetime
import time
import warnings
# cuda stuff
import ctypes
from numpy.ctypeslib import ndpointer
import matplotlib.pyplot as plt
from matplotlib import colors
from auxiliaries.plotformat import firePlotMologram,scatteredIntensityPlot,scattererPlotMologram
from auxiliaries.Journal_formats_lib import formatPRX
import logging # class for writing a logfile of the simulation errors and status
import os
from mologram.mologram import diameterGivenNA_F_n,numericalApertureGivenF_D_n,focalDistanceGivenNA_D_n


import math


class StructureOfArraysBinderProperties():
    """Container class that stores all the positions, dipole moments, whether particles are coherent or not whether they actually landed on the ridges or the grooves (
    also incoherently placed particles land on the ridges) the name of each particle, the permittivity of the particle and the radius of the particle.

    """

    def __init__(self):

        self.dipolePositions = np.array(0)
        self.dipoleMoments = np.array(0)
        # coherently placed scatterers are 1, incoherently placed scatterers
        # are 0.
        self.coh_inc = np.array(0)
        #self.grooves_vs_rigdes = np.array(0)
        self.bindernames = np.array('')
        self.eps_r = np.array(0)
        self.Radius = np.array(0)

class ParticleType(object):
    """Stores only the variables for placing the particles on the mologram."""

    def __init__(self):
        self.name = 0
        self.radius = 0
        self.dispersity = 0
        self.Ncoh = 0
        self.Nincoh = 0
        self.z_min = 0
        self.z_max = 0
        self.placement = 0
        self.seed = 0
        self.eps_r = 0

        self.remainingNcoh = 0 # for very large Simulations if the binders need to be computed serially.
        self.remainingNincoh = 0 # for very large Simulations if the binders need to be computed serially.

    def __str__(self):

        return "Name: " + str(self.name) + " Radius: " + str(self.radius) + \
        " Ncoh: " + str(self.Ncoh) + " Nincoh: " + str(self.Nincoh) + " z_min: " + str(self.z_min) \
        + " z_max " + str(self.z_max) + " Placement: " + str(self.placement) + " Seed: " + str(self.seed) + "eps_r: " + str(self.eps_r)


def unwrapBinderparameters(particlePar,interactionPar,mologram):
    """
    unwraps the binder parameters object and calculates coherent and incoherent particle numbers all other values are converted to these, from these then in turn all
    other values are calculated and saved in the database

    numbers from concentration and K_D if they are not equal to zero. This function is independent and can be used from outside.

    :param particlePar: ParticleParameter() object that stores the details of the particle that interacts with the molographic surface.
    :param interactionPar:  MologramBiosensorInteractionParameter() object that stores the details of the interaction with the molographic biosensor.
    :param mologram: Mologram() object

    :return ParticleTypes: returns a list of particle types to be placed on the mologram.
    """

    # there is a surface interaction present. 
    if interactionPar.getValues()['analyte_c'] != 0:

        logging.info('Started an surface interaction simulation')

        analyte_Ncoh = particlePar.getValues()['analyte_Ncoh']
        analyte_Nincoh = particlePar.getValues()['analyte_Nincoh']
        NSB_Ncoh = particlePar.getValues()['NSB_Ncoh']
        NSB_Nincoh = particlePar.getValues()['NSB_Nincoh']

        assert (analyte_Ncoh == 0 and analyte_Nincoh == 0), 'Analyte particle numbers were specified, please only specify a K_D and a concentration'
        assert (NSB_Ncoh == 0 and NSB_Nincoh == 0), 'NSB particle numbers were specified, please only specify a K_D and a concentration'


        biosensor = MologramBiosensor(mologram,interactionPar)

        boundParticles = {
            'analyte_Ncoh':biosensor.cohAnalyteN()[0],
            'analyte_Nincoh':biosensor.inCohAnalyteN()[0],
            'NSB_Ncoh':biosensor.cohNSBN()[0],
            'NSB_Nincoh':biosensor.inCohNSBN()[0]
            }

        particlePar.setValues(boundParticles)

    ParticleTypes = []

    print "unwrapped Binder parameters"

    for binder in particlePar:

        # it is important to always use the getValues function, because the binder properties are constantly updated in this function here.

        particle = Particle(binder.getValues()[binder.name + '_name'], mologram.waveguide.wavelength, '')

        if binder.getValues()[binder.name + '_surfaceMassModulation'] != 0 or binder.getValues()[binder.name + '_commonMassDensity'] != 0 or binder.getValues()[binder.name + '_coherentSurfaceMassDensity'] != 0:

            # the default particle numbers are zero, important otherwise the variable is not reset after each loop iteration.
            Ncoh = 0
            Nincoh = 0

            assert (binder.getValues()[binder.name + '_totalCoherentProteinMass'] == 0 and binder.getValues()[binder.name + '_totalInCoherentProteinMass'] == 0 and binder.getValues()[binder.name + '_Ncoh'] == 0 and binder.getValues()[binder.name + '_Nincoh'] == 0)

            # if coherentSurface mass density is given, then convert it to a surfaceMassModulation such that the rest of the script works.
            if binder.getValues()[binder.name + '_coherentSurfaceMassDensity'] != 0:

                raise NotImplementedError('coherent surface mass density is not implemented!')

                logging.debug('coherentSurfaceMassDensity defined')

                # if hasattr(particle,'mass'):
                #     delta = binder.getValues()[binder.name + '_z_max'] + binder.getValues()[binder.name + '_Radius']
                #     protein_adlayer = ProteinSurfaceMassModulationAdlayer(M_protein = particle.mass,Gamma_coh = binder.getValues()[binder.name + '_coherentSurfaceMassDensity'],delta=delta,A_ridges=mologram.AreaWithoutBragg()/2*1e6,A_molo=mologram.Area()*1e6,mologram_type = binder.getValues()[binder.name + '_placement'])
                # else:
                #     raise NotImplementedError

                # particleParValues = {
                #     binder.name + '_surfaceMassModulation':float(protein_adlayer.Gamma_Delta)
                # }

                # binder.setValues(particleParValues)

            logging.debug('surfaceMassModulation Defined')
            logging.debug(binder.getValues()[binder.name + '_surfaceMassModulation'])

            surfaceMassModulation = binder.getValues()[binder.name + '_surfaceMassModulation']
            commonMassDensity = binder.getValues()[binder.name + '_commonMassDensity']

            if hasattr(particle,'mass'):

                delta = binder.getValues()[binder.name + '_z_max'] + binder.getValues()[binder.name + '_Radius']
                if binder.getValues()[binder.name + '_placement'] == 'Ridges (Sinusoidal)':
                    Gamma = surfaceMassModulation*np.sqrt(2)/2.*mologram.AreaWithoutBragg()/2./mologram.Area() # assuming a sinusoidal surface mass density modulation (PTP value)
                else:
                    Gamma = surfaceMassModulation*mologram.AreaWithoutBragg()/2./mologram.Area() # assuming a canonical surface mass density modulation

                protein_layer = ProteinAdlayer(M_protein = particle.mass,Gamma=Gamma,delta=delta)
                Ncoh = protein_layer.N_scat*1e6*mologram.Area()
                protein_layer = ProteinAdlayer(M_protein = particle.mass,Gamma=commonMassDensity,delta=delta)

                if binder.getValues()[binder.name + '_seed']:
                    Nincoh = protein_layer.N_scat*1e6*mologram.Area()
                    totalCoherentProteinMass = float(Gamma*1e6*mologram.Area())

                    particleParValues = {
                        binder.name + '_totalCoherentProteinMass':float(Gamma*1e6*mologram.Area()),
                        binder.name + '_totalInCoherentProteinMass':float(commonMassDensity*1e6*mologram.Area()),
                        binder.name + '_coherentSurfaceMassDensity':float(totalCoherentProteinMass/1e6/mologram.Area())
                    }


                else:
                    Nincoh = protein_layer.N_scat*1e6*mologram.AreaWithoutBragg() # 1e6 is necessary because N_scat is in /mm2
                    totalCoherentProteinMass = float(Gamma*1e6*mologram.Area())
                    particleParValues = {
                        binder.name + '_totalCoherentProteinMass':float(Gamma*1e6*mologram.Area()),
                        binder.name + '_totalInCoherentProteinMass':float(commonMassDensity*1e6*mologram.AreaWithoutBragg()),
                        binder.name + '_coherentSurfaceMassDensity':float(totalCoherentProteinMass/1e6/mologram.Area())
                    }

                binder.setValues(particleParValues)

                numberOfParticles = {
                    binder.name + '_Ncoh':Ncoh,
                    binder.name + '_Nincoh':Nincoh
                }

                binder.setValues(numberOfParticles)

            else:
                raise NotImplementedError

        elif binder.getValues()[binder.name + '_totalCoherentProteinMass'] != 0 or binder.getValues()[binder.name + '_totalInCoherentProteinMass'] != 0:

            # the default particle numbers are zero, important otherwise the variable is not reset after each loop iteration.
            Ncoh = 0
            Nincoh = 0

            assert (binder.getValues()[binder.name + '_surfaceMassModulation'] == 0)

            logging.debug('totalCoherentProteinMass')

            totalCoherentProteinMass = binder.getValues()[binder.name + '_totalCoherentProteinMass']
            totalInCoherentProteinMass = binder.getValues()[binder.name + '_totalInCoherentProteinMass']

            particle = Particle(binder.getValues()[binder.name + '_name'], mologram.waveguide.wavelength, '')

            if hasattr(particle,'mass'):

                delta = binder.getValues()[binder.name + '_z_max'] + binder.getValues()[binder.name + '_Radius']
                protein_layer = ProteinAdlayer(particle.mass,Gamma=totalCoherentProteinMass,delta=delta)
                Ncoh = protein_layer.N_scat
                protein_layer = ProteinAdlayer(particle.mass,Gamma=totalInCoherentProteinMass,delta=delta)
                Nincoh = protein_layer.N_scat

                if binder.getValues()[binder.name + '_seed']:
                    commonMassDensity = totalInCoherentProteinMass/1e6/mologram.Area() # pg/mm^2
                else:
                    commonMassDensity = totalInCoherentProteinMass/1e6/mologram.AreaWithoutBragg() # pg/mm^2

                if binder.getValues()[binder.name + '_placement'] == 'Ridges (Sinusoidal)':
                    surfaceMassModulation = totalCoherentProteinMass/1e6/(mologram.AreaWithoutBragg()/2)*np.sqrt(2.)
                else:
                    surfaceMassModulation = totalCoherentProteinMass/1e6/(mologram.AreaWithoutBragg()/2)

                particleParValues = {
                    binder.name + '_surfaceMassModulation':float(surfaceMassModulation),
                    binder.name + '_commonMassDensity':float(commonMassDensity),
                    binder.name + '_coherentSurfaceMassDensity':float(totalCoherentProteinMass/1e6/mologram.Area())
                }

                binder.setValues(particleParValues)

                numberOfParticles = {
                    binder.name + '_Ncoh':Ncoh,
                    binder.name + '_Nincoh':Nincoh
                }

                binder.setValues(numberOfParticles)

            else:
                raise NotImplementedError
            # set the calculated parameters.


        particle = ParticleType()

        particle.name = binder.getValues()[binder.name + '_name']
        particle.radius = binder.getValues()[binder.name + '_Radius']

        Ncoh = binder.getValues()[binder.name + '_Ncoh']
        Nincoh = binder.getValues()[binder.name + '_Nincoh']

        # add molecular shot noise to the binder number if this is
        # specified.
        if binder.getValues()[binder.name + '_MSN']:

            logging.info(binder.name + ' subject to molecular shot noise')

            Ncoh = np.random.poisson(Ncoh)
            Nincoh = np.random.poisson(Nincoh)

        else:
            pass

        # add particle size dispersity to the particle radius if this is specified
        if binder.getValues()[binder.name + '_dispersity']:

            logging.info(binder.name + ' subject to particle size dispersity')

            particle.dispersity =  binder.getValues()[binder.name + '_dispersity']

        else:
            pass

        # to do: Now I would need to calculate the other parameters from the Ncoh_wMSN and save this to the database object. 

        # set the actual shot noise parameters

        # this is just for the eps_r or the refractive index, the rest is already set up in the config file
        materialParticle = Particle(name=particle.name,
                                        wavelength=wavelength,
                                        radius = particle.radius)

        particle.eps_r = materialParticle.eps_r

        particle.Ncoh = Ncoh
        particle.Nincoh = Nincoh
        particle.z_min = binder.getValues()[binder.name + '_z_min']
        particle.z_max = binder.getValues()[binder.name + '_z_max']
        particle.placement = binder.getValues()[binder.name + '_placement']
        particle.seed = binder.getValues()[binder.name + '_seed']

        ParticleTypes.append(particle)

    logging.debug("_________________________________")
    for i in ParticleTypes:
        logging.debug(i)


    logging.info('Binder parameters unwrapped')

    return ParticleTypes

# class BiasFieldSimulation(FieldSimulation):
#     """A simulation that performs a field simulation with a bias field and dipole distribution supplied as a simulation ID."""

#     def __init__(self):

#         raise NotImplementedError



class FieldSimulation(object):
    """Main file to execute a field simulation on the mologram.

    :param (FieldSimulationInputs) Simulation parameters: VariableContainerList object specifying the inputs for the simulation,
        plots is a dictionary of which plots should be produced, if the name is in plots the corresponding plot is produced with the name specified.
    :param (dict) saveDict: dictionary, all the keys of this dictionary will be stored in the database, otherwise all the values are stored.

    """

    def __init__(self, SimulationParameters, textBrowser='', resultsSignal='', plots='',saveDict=''):

        self.simInput = SimulationParameters

        self.textBrowser = textBrowser
        self.resultsSignal = resultsSignal

        self.results = FieldSimulationResults()

        self.plots = plots

        self.saveDict = saveDict

        # setup the simulation environment, creates the waveguide, mologram,screen and background screen.

        self.SetupSimulationEnvironment()

        logging.info('Simulation environment set up')

        self.boundDipoles = StructureOfArraysBinderProperties()

    def startSimulation(self):

        # time of the simulation
        self.startTime = time.clock()

        # place the scatterers

        # generates an array of coherent scatters and an array of incoherent
        # scatterers

        logging.info('Simulation started')

        # returns a list of the different binders, takes shot noise and all the nasty stuff
        # into account.

        self.binders = unwrapBinderparameters(self.simInput.particlePar,self.simInput.interactionPar,self.mologram)

        # places all the particles on the mologram.

        # I need the option to specify an experiment ID and load whatever is in there.

        self.largeSimulation = False
        self.skip_binder = []

        # go through the binders, all binders that have more than 1000000 have to be skipped.
        # if this is higher than 1 million scatterers begin to split it.
        # to split the scattering calculation is easy but the extinction not.

        for i in self.binders:

            self.skip_binder.append(False)

            if i.Ncoh > 1000000. or i.Nincoh > 1000000:

                self.skip_binder.append(True)

            if i.Ncoh > 1000000.:

                i.remainingNcoh = i.Ncoh

                print "large simulation particles are splitted into chunks of 1000000, ATTENTION: Total Extinction is not calculated properly."
                self.largeSimulation = True


            if i.Nincoh > 1000000.:

                i.remainingNincoh = i.Nincoh

                print "large simulation particles are splitted into chunks of 1000000, ATTENTION: Total Extinction is not calculated properly."
                self.largeSimulation = True

        if self.largeSimulation:

            logging.info('Large Simulation')
            npix = self.simInput.screenPar.getValues()['npix']
            tot_E_sca = np.zeros((3,npix*npix),dtype=np.complex128)
            tot_E_bg = np.zeros((3,npix*npix),dtype=np.complex128)

            binders = self.binders

            for i in binders:

                while i.Ncoh != 0 or i.Nincoh !=0:

                    self.binders = [i] # must be a list because otherwise the particle placement fails.

                    print i.Ncoh
                    print i.remainingNcoh

                    if i.remainingNincoh > 1000000.:
                        i.remainingNincoh = remainingNincoh - 1000000.
                        i.Nincoh = 1000000.
                    elif i.remainingNincoh != 0:
                        i.Nincoh = i.remainingNincoh
                        i.remainingNincoh = 0
                    else:
                        i.remainingNincoh = 0
                        i.Nincoh = 0

                    if i.remainingNcoh > 1000000.:
                        i.remainingNcoh = i.remainingNcoh - 1000000.
                        i.Ncoh = 1000000.
                    elif i.remainingNcoh != 0:
                        i.Ncoh = i.remainingNcoh
                        i.remainingNcoh = 0
                    else:
                        i.remainingNcoh = 0
                        i.Ncoh = 0

                    self.boundDipoles = self.placeAllParticles()


                    self.executeSingleSimulation()

                    tot_E_sca += self.E_sca
                    tot_E_bg += self.E_bg

            self.E_sca = tot_E_sca
            self.E_bg = tot_E_bg

            Z_w = sqrt(constants.mu_0/self.configuration.Media.returnMedium(self.mologram.focalPoint).epsilon)

            self.I_sca = 1/(2*Z_w) * np.reshape(sum(self.E_sca*conj(self.E_sca), axis=0).real,(npix, npix))
            self.I_bg = 1/(2*Z_w) * np.reshape(sum(self.E_bg*conj(self.E_bg), axis=0).real,(npix, npix))


        else: # execute a single simulation if the total particle number is below 1000000

            if self.simInput.generalSettings.getValues()['CorrSimulation'] != 0 and self.largeSimulation == False:
                # do not newly place the particles that where already placed.
                # this does not work for large simulations

                self.boundDipoles = self.placeAllParticles() # place the additional particles.

                CorrSimulation = loadDB(self.simInput.generalSettings.getValues()['databaseFile'], 'results', condition='WHERE ID==' + str(self.simInput.generalSettings.getValues()['CorrSimulation']))


                self.boundDipoles.dipolePositions = np.hstack((self.boundDipoles.dipolePositions,CorrSimulation['dipolePositions']))
                self.boundDipoles.bindernames = np.hstack((self.boundDipoles.bindernames,CorrSimulation['bindernames']))
                self.boundDipoles.coh_inc = np.hstack((self.boundDipoles.coh_inc,CorrSimulation['coh_inc']))
                self.boundDipoles.eps_r = np.hstack((self.boundDipoles.eps_r,CorrSimulation['eps_r']))
                self.boundDipoles.Radius = np.hstack((self.boundDipoles.Radius,CorrSimulation['Radius']))

            else:
                self.boundDipoles = self.placeAllParticles()

            #if self.simInput.generalSettings.getValues()['biasSimulation'] != 0:

                # load the simulation ID
                # load the number of coherent scatterers
                # assumption is that the bias is much larger than whatever else is added.

            # here I would need to throw away the bias, if the field has already been calculated before.

            self.executeSingleSimulation()

        # waveguide background intensity at the focal point, here I would need to take the extinction of the scatterers into account.

        self.I_0, self.Z_w = incidentIntensity(self.configuration)

        # save the excitation field if this is desired.

        if self.simInput.waveguidePar.getValues()['Waveguidebackground'] != 0:

            self.I_bwg_const = calcWGBackground(self.configuration, self.mologram)
            # load the waveguide background from the database
            path = os.path.dirname(__file__)
            wgBG = loadDB(path + '/waveguidebackground/data/waveguideBG.db', 'results', condition='WHERE ID==' + str(self.simInput.waveguidePar.getValues()['Waveguidebackground']))
            self.I_bwg = wgBG['I_sca']
            self.I_bwg = self.I_bwg_const*self.I_bwg/self.I_bwg.mean() # lift the background simulation on the background levels that are actually expected.

            self.I_tot = self.I_sca + self.I_bwg
            self.I_bg_tot = self.I_bg + self.I_bwg
        else:
            self.I_bwg_const = 0
            self.I_tot = self.I_sca
            self.I_bg_tot = self.I_bg

        # the intensity noise I could also just include here...

        self.I_signal, self.I_molo, self.SNR, self.bkg_mean, self.bkg_std = self.postProcessing(
            self.I_sca,self.I_bg,self.simInput.screenPar.getValues(), self.screen)

        self.saveResults()

    def executeSingleSimulation(self):

        # calculate the excitation field at the dipole positions.


        # should allow for the binding curve and needs to be recalculate for
        # every added bunch of particles.

        if self.simInput.generalSettings.getValues()['Extinction']:
            # add option to to use an leaking parameter for faster calculation when many scatterers are on the mologram
            self.E_exc, self.E_exc_mean = self.calcExcitationFieldWithExtinction(
                self.configuration, self.mologram)
        else:
            self.E_exc, self.E_exc_mean = self.calcExcitationFieldWithoutExtinction()

        # dipole moments
        self.dipoleMoments = dipole.staticDipoleMoment(
            self.boundDipoles.Radius, self.boundDipoles.eps_r, self.configuration.Media.returnMedium(1).eps_r, self.E_exc)

        # it would be better to have access to the indiviual contributions here...
        # like this it also does not allow the bias to be treated by itself...

        timer = time.time()

        self.E_sca, self.I_sca = calcScattering(self.configuration, self.boundDipoles.dipolePositions, self.dipoleMoments,
                                                self.screen, self.Phi, self.molograms, self.simInput.generalSettings.getValues()["calculationMethod"])

        # calculate a second intensity distribution far away from the focal spot in order to calculate
        # the background from nonspecifically bound molecules etc.

        if self.simInput.generalSettings.getValues()['BackgroundScreen']:

            self.E_bg, self.I_bg = calcScattering(self.configuration, self.boundDipoles.dipolePositions, self.dipoleMoments,
                                                    self.background_screen, self.Phi, self.mologram, self.simInput.generalSettings.getValues()["calculationMethod"])

        else:
            self.E_bg = 0
            self.I_bg = 0

        logging.info('Field calculation time:' + str(time.time() - timer))

    def saveResults(self):

        runningTime = time.clock() - self.startTime
        # make a data directory if that one does not exist yet.
        if not os.path.exists('data'):
            os.makedirs('data')

        results = {

            'runningTime': runningTime,

            'dipolePositions':self.boundDipoles.dipolePositions, # array of the positions of all the different binders (contains three arrays (x,y,z))
            'bindernames':self.boundDipoles.bindernames, # array of the names of all the different binders
            'coh_inc':self.boundDipoles.coh_inc, # boolean 1 Particle is placed coherently, 0 particle is placed incoherently.
            'dipoleMoments':self.boundDipoles.dipoleMoments, # array of the dipole moments of all the different binders
            'eps_r':self.boundDipoles.eps_r, # array of the permittivity of all the different binders
            'Radius':self.boundDipoles.Radius, # array of the radii of all the different binders

            # calculations
            # actual input power, with laser fluctuations included.
            'inputPower': self.inputPower,
            # intensity per unit length impinging onto the mologram at the
            # surface of the waveguide.
            'I_0': self.I_0,
            # scattered field in the focal plane (without waveguide background)
            'E_sca': self.E_sca,
            # scattered intensity in the focal plane (without waveguide
            # background)
            'I_sca': self.I_sca,
            'I_tot': self.I_tot,  # waveguide background + scattered field.
            # excitation field at the scatterer positions.
            'I_bg': self.I_bg, # intensity on the background_screen without waveguide background.
            'I_bg_tot':self.I_bg_tot, # intensity on the background screen with the waveguide background.
            'E_bg':self.E_bg, # field on the background screen
            'E_exc': self.E_exc, # excitation field at the scatterer positions
            # background intensity due to the waveguide impurities.
            'I_bwg': self.I_bwg_const, # background intensity of the waveguide.
            'I_signal': self.I_signal,  # scattered intensity in the focal point.
            'I_molo': self.I_molo,  # scattered intensity averaged over the Airy disk
            # Radius of the Airy disk, which is calculated from the simulation
            # and fed to calc_Int_AiryDisk
            'AiryDiskRadius': self.AiryDiskRadius,
            # numpy array of the screen locations for plotting.
            'screen': self.screen,
            'SNR': self.SNR,  # SNR calculated by calc_Int_AiryDisk calculated by calc_Int_AiryDisk
            'bkg_mean': self.bkg_mean,  # mean background calculated by calc_Int_AiryDisk
            'bkg_std': self.bkg_std,  # std background calculated by calc_Int_AiryDisk
        }

        # save the desired results as plots.
        if type(self.plots) is dict:
            self.producePlots()

        # only save the results that are desired to be stored.
        # check what is to be stored in the database.


        if type(self.saveDict) is not dict:
            self.results.setValues(results)
        else:
            self.results.setValues(commonKeys(results,self.saveDict))

        # if type(self.saveDict) is not dict:
        #     print hello
        # do the same for the logdict here.

        # save everything to the database

        # TO DO: load bias from biassimulation. 

        # if self.simInput.generalSettings.getValues()['biasSimulation']:
        #     self.simInput.createDB(self.simInput.generalSettings.getValues()[
        #                            'databaseFile'], 'biaslog')
        #     self.simInput.saveDB()

        #     self.results.createDB(self.simInput.generalSettings.getValues()[
        #                               'databaseFile'], 'biasresults')
        #     self.results.saveDB()

        #     logging.info('Saved Bias Simulation')

        # else:

        if self.simInput.generalSettings.getValues()['saveLog']:
            self.simInput.createDB(self.simInput.generalSettings.getValues()[
                                   'databaseFile'])
            self.simInput.saveDB()

        if self.simInput.generalSettings.getValues()['saveDatabase']:
            self.results.createDB(self.simInput.generalSettings.getValues()[
                                  'databaseFile'])
            self.results.saveDB()

        logging.info('Saved Simulation results')



        # emit the results for the GUI
        if self.resultsSignal != '':
            self.resultsSignal.emit(self.results.getValues())

        # save to database should also be here, cause I have everything to
        # perform this functionality.

    def producePlots(self):
        """Function that allow for the creation of publication ready plots. plots is a dictionary of the names of the plots that should be produced in this simulation."""


        if 'Excitation_intensity' in self.plots.keys(): # plot that shows the intensity at the scatterer locations. 

            assert self.largeSimulation == False, "This function only works for small simulations."

            formatPRX()
            fig = plt.figure()
            x_sca, y_sca, z_sca = self.boundDipoles.dipolePositions
            logging.debug('Excitation Intensity plot:')

            logging.debug(x_sca)
            logging.debug(y_sca)
            logging.debug(self.E_exc.real[1])

            firePlotMologram(x_sca, y_sca, self.E_exc.real[1], fig, self.plots['Excitation_intensity'])

        if 'scattererPlot' in self.plots.keys(): 

            assert self.largeSimulation == False, "This function only works for small simulations."

            formatPRX()
            fig = plt.figure()
            x_sca, y_sca, z_sca = self.boundDipoles.dipolePositions
            scattererPlotMologram(x_sca, y_sca, fig, self.plots['scattererPlot'])

        if 'Scattered_intensity' in self.plots.keys(): # plot of the scattered intensity on the screen.

            formatPRX()
            fig = plt.figure()
            scatteredIntensityPlot(self.I_tot,self.screen,self.mologram.focalPoint,fig,self.plots['Scattered_intensity'])

        if 'Scattered_intensity_plus_Airy_disk' in self.plots.keys(): # plot of the scattered intensity on the screen together with the analytical Airy disk.

            formatPRX(4,4)
            fig = plt.figure()
            ax = fig.add_subplot(111)
            scatteredIntensityPlot(self.I_tot,self.screen,self.mologram.focalPoint,fig,self.plots['Scattered_intensity'],save=False,colorbar=False)
            airy_disk = plt.Circle((0,0),self.AiryDiskRadius*1e6,color='w',fill=False)
            ax.add_artist(airy_disk)
            plt.savefig(self.plots['Scattered_intensity_plus_Airy_disk'] + '.png',dpi=600,format='png')



        if 'Scattered_intensityBackground' in self.plots.keys():

            assert self.simInput.generalSettings.getValues()['BackgroundScreen'] == 1, "This function requires a background screen."

            formatPRX()
            fig = plt.figure()
            scatteredIntensityPlot(self.I_bg_tot,self.background_screen,self.mologram.focalPoint,fig,self.plots['Scattered_intensityBackground'])

        if 'scattererPlotLarge' in self.plots.keys():

            assert self.largeSimulation == False, "This function only works for small simulations."

            formatPRX()
            fig = plt.figure()
            x_sca, y_sca, z_sca = self.boundDipoles.dipolePositions
            scattererPlotMologram(x_sca, y_sca, fig, self.plots['scattererPlotLarge'],size = 1,alpha=1)


        logging.info('Produced the plots')

    def placeAllParticles(self):
        """
        place particles of different types on the mologram and returns a StructureOfArrayOfBinderProperties

        """

        mologram = self.mologram
        configuration = self.configuration

        wavelength = configuration.waveguide.wavelength

        x_sca_all = []
        y_sca_all = []
        z_sca_all = []
        #grooves_vs_ridges_all = []
        coh_inc = []
        bindernames = []
        Radii = []
        materialParticle_eps_r = []


        for particle in self.binders:

            Name = particle.name
            
            Radius = particle.radius

            Ncoh = particle.Ncoh
            Nincoh = particle.Nincoh

            z_min = particle.z_min
            z_max = particle.z_max
            Placement = particle.placement
            seedBragg = particle.seed

            logging.debug('Seed Bragg')
            logging.debug(seedBragg)

            # place Rayleigh-scatterer (seeding of mologram)

            if Ncoh != 0:
                # placing of the coherent scatterers
                x_sca, y_sca, z_sca, grooves_vs_ridges = mologram.placeScatterer(
                    int(Ncoh), Placement, 1., z_min, z_max, seedBragg)  # coherent scatterers
                x_sca_all += list(x_sca)
                y_sca_all += list(y_sca)
                z_sca_all += list(z_sca)
                #grooves_vs_ridges_all += list(grooves_vs_ridges)
                # coherently placed scatterers are 1.
                coh_inc += list(np.ones((len(x_sca))))
                bindernames += [Name for i in range(len(x_sca))]
                # used to speed up dipole moment calculation
                if particle.dispersity:
                    Radii += [(particle.dispersity*np.random.randn() + particle.radius) for i in range(len(x_sca))]
                else:
                    Radii += [Radius for i in range(len(x_sca))]
                materialParticle_eps_r += [
                    particle.eps_r for i in range(len(x_sca))]
  
            if Nincoh != 0:
                # placing of the incoherent scatterers

                # use silvio's function for the incoherent placement.

                x_sca, y_sca, z_sca = placeIncScatterer(Nincoh, z_min,z_max, w=mologram.width, h=mologram.height, center=mologram.center, masks=mologram.mask,seedBragg=seedBragg)
                x_sca_all += list(x_sca)
                y_sca_all += list(y_sca)
                z_sca_all += list(z_sca)
                #grooves_vs_ridges_all += list(grooves_vs_ridges)
                # incoherently placed scatterers are 0.
                coh_inc += list(np.zeros((len(x_sca))))
                bindernames += [Name for i in range(len(x_sca))]
                # used to speed up dipole moment calculation
                if particle.dispersity:
                    Radii += [(particle.dispersity*np.random.randn() + particle.radius) for i in range(len(x_sca))]
                else:
                    Radii += [Radius for i in range(len(x_sca))]
                materialParticle_eps_r += [
                    particle.eps_r for i in range(len(x_sca))]

        boundDipoles = StructureOfArraysBinderProperties()

        boundDipoles.dipolePositions = np.vstack(
            (np.asarray(x_sca_all), np.asarray(y_sca_all), np.asarray(z_sca_all)))
        boundDipoles.coh_inc = np.asarray(coh_inc)
        #boundDipoles.grooves_vs_rigdes = np.asarray(grooves_vs_ridges_all)
        boundDipoles.bindernames = np.asarray(bindernames)
        boundDipoles.Radius = np.asarray(Radii)
        boundDipoles.eps_r = np.asarray(materialParticle_eps_r)

        logging.info('Particles placed')

        return boundDipoles

    def calcExcitationFieldWithoutExtinction(self):
        """returns the excitation field at the scatterer positions with only the exponential damping due to the roughness of the waveguide. """

        x_sca, y_sca, z_sca = self.boundDipoles.dipolePositions
        wavelength = self.configuration.waveguide.wavelength
        waveguide = self.configuration.waveguide

        E_exc = np.vstack([waveguide.E_x(wavelength, x_sca, y_sca, z_sca),
                           waveguide.E_y(wavelength, x_sca, y_sca, z_sca),
                           waveguide.E_z(wavelength, x_sca, y_sca, z_sca)])

        E_exc_mean = np.mean(E_exc[1]).real

        logging.info('Excitation field calculated')

        return E_exc,E_exc_mean


    def calcExcitationFieldWithExtinctionApproximative(self):
        """calculates the extinction field in an approximate manner by assuming the different particles placed uniformly on the mologram"""
        raise NotImplementedError


    def calcExcitationFieldWithExtinction(self, configuration, mologram):
        """ I would need to check, whether Energy conservation is fulfilled. After updating the architecture of the framework, this function needs to
        be tested.

        TO DO: z-dependency of the extinction is not yet accounted for.

        returns the field at the scatterer positions after the extinction of the particles have to be taken into account.

        """
        # to be updated for coherent scatterers!!!!
        # loop through all different kinds of particles, discriminate coherent
        # and noncoherent particles...

        # excitation field at the scatterer positions, already accounting for
        # the decay due to waveguide propagation loss.

        x_sca, y_sca, z_sca = self.boundDipoles.dipolePositions

        coh_inc = self.boundDipoles.coh_inc
        waveguide = configuration.waveguide
        wavelength = configuration.waveguide.wavelength
        binderNames = self.boundDipoles.bindernames
        # to know where to insert the calculated fields of the orignal vector.
        original_indices = np.arange(len(x_sca))
        k_0 = 2*pi/wavelength

        # excitation field of the waveguide at the scatterer positions (only
        # waveguide propagation loss is included)
        E_exc = np.vstack([waveguide.E_x(wavelength, x_sca, y_sca, z_sca),
                           waveguide.E_y(wavelength, x_sca, y_sca, z_sca),
                           waveguide.E_z(wavelength, x_sca, y_sca, z_sca)])

        # extinction of the scatterer (TODO: better if split into parts), of
        # the different binder types.

        extinctionCrossSections = []
        for particle in self.binders:

            extinctionCrossSections.append(dipole.extinctionCrossSection(k_0*configuration.Media.returnMedium(z_sca[0]).n,
                                                                         particle.radius, particle.eps_r, configuration.Media.returnMedium(z_sca[0]).eps_r))

        # sort the particles according to their x position, start with the first particle. N_adsorbed is an array of the particles before a particle
        # i.

        # I should start dividing the molographic structure into lines in y and calculate the extinction for each line, then it would also work for
        # rectangular molograms.

        # divide the mologram in y slices.
        y_min, y_max = (-mologram.height/2., mologram.height/2.)
        # bins should be in the order of 1 micron. To small bins cause trouble.
        N_bins = mologram.height/1.e-6
        # create a hundred bins (maybe I should rather choose a fixed bin
        # size.)
        bins = np.linspace(y_min, y_max, N_bins)

        permutation = np.argsort(y_sca)

        x_sca = x_sca[permutation]
        y_sca = y_sca[permutation]
        coh_inc = coh_inc[permutation]
        binderNames = binderNames[permutation]
        E_exc = E_exc[:, permutation]
        original_indices = original_indices[permutation]

        ind = np.digitize(y_sca, bins)

        # get the y-bins that actually have particles.
        filled_bins = set(ind)

        indices = []
        bin_indices = np.where(np.diff(np.digitize(y_sca, bins)) == 1)[0]

        # assemble tuples of the bin_indices of the bin edges in the y_sca
        # vector.
        for i, j in enumerate(bin_indices):

            if i == 0:
                # +1 is necessary due to the nature of the diff function.
                indices.append((0, bin_indices[i]+1))
            else:
                indices.append((bin_indices[i-1]+1, bin_indices[i]+1))

        indices.append((bin_indices[-1], len(y_sca)))

        # loop through all the lines that actually have particles.
        E_exc_ext = np.zeros((3, len(x_sca)))
        m = 0
        for i in indices:

            # get all the particles and their properties from a line.
            x_line = x_sca[i[0]:i[1]]
            y_line = y_sca[i[0]:i[1]]
            coh_inc_line = coh_inc[i[0]:i[1]]
            binderNames_line = binderNames[i[0]:i[1]]
            E_exc_line = E_exc[:, i[0]:i[1]]
            original_indices_line = original_indices[i[0]:i[1]]

            # sort the particles in x
            permutation = np.argsort(x_line)

            x_line = x_line[permutation]
            y_line = y_line[permutation]
            coh_inc_line = coh_inc_line[permutation]
            binderNames_line = binderNames_line[permutation]
            E_exc_line = E_exc_line[:, permutation]
            original_indices_line = original_indices_line[permutation]

            N_coh = np.zeros(len(self.binders))
            N_incoh = np.zeros(len(self.binders))

            exponent = np.zeros(len(x_line))

            m += 1

            for j, x in enumerate(x_line):
                # x is the location of the scatterer.

                # it is the same for circular and disk shaped molograms because
                # otherwise the exponent below would also need to be dependent
                # on y in the case of the circle, like this the two effects
                # cancel.
                A_segment = (y_max-y_min)/N_bins*(x + mologram.radius)



                for k, particle in enumerate(self.binders):

                    V_segment = A_segment*(particle.z_max+particle.radius-(particle.z_min-particle.radius))

                    # intensity is attenuated, therefore the factor 1/2.
                    # I neglect dependency of the extinction on z position,
                    # otherwise it gets complicated like hell
                    # I removed the square factor of the N_coh again, because it is not correct.
                    exponent[j] += -1/2.*(N_coh[k] + N_incoh[k])/V_segment * \
                        extinctionCrossSections[k]*(x + mologram.radius)

                # add the current dipole to either the coherent or incoherent
                # number of its kind.

                for k, DipoleType in enumerate(self.binders):

                    if coh_inc_line[j] == 1 and DipoleType.name == binderNames_line[j]:

                        N_coh[k] += 1

                    if coh_inc_line[j] == 0 and DipoleType.name == binderNames_line[j]:

                        N_incoh[k] += 1

            # this is fucking slow operation but I am too tired to think of
            # anything better
            E_exc_ext[:, original_indices_line] = E_exc_line * np.exp(exponent)

        E_exc_ext_mean = np.mean(E_exc_ext[1]).real

        logging.info('Excitation field calculated')

        return E_exc_ext, E_exc_ext_mean

        # old, but slightly faster implementation of Silvio

        # # I need to have an N_absorbed coh and N_absorbed_incoh of every particle and line
        # N_absorbed = numpy.matlib.repmat(np.argsort(np.argsort(x_sca)),3,1) #
        # repmat makes a 3 x 1 vector for the field calculation, I do not know
        # why there are two argsorts...

        # # vector of the area of the segment before the particle's position.
        # A_segment = mologram.radius**2/2.*(2*np.arccos(-x_sca/mologram.radius)-np.sin(2*(np.arccos(-x_sca/mologram.radius))))

        # # vector of the volume of the segment before the particle's position.
        # V_segment = A_segment*np.max((2*analyte_Radius,z_max +
        # analyte_Radius)) # the sheet were the particles are distributed.
        # Either 2*r if z_max + r is below this value.

        # # vector of the field extinction at the particle's positions.
        # E_exc = E_exc *
        # np.exp(-1/2.*N_absorbed/V_segment*extinctionCrossSection*(x_sca+mologram.radius))
        # # intensity is attenuated, therefore the factor 1/2.
        # only for TE (!), particle extinction included

        # # output should be the field vector at the particles position.


    def postProcessing(self, I_sca, I_bg,screenPar, screen):

        ###########################################
        # Calculation of some relevant parameters #
        ###########################################

        locals().update(screenPar)

        if (npix != 1):
            with warnings.catch_warnings():
                # find Airy disk diameter (numerically)
                # self.AiryDiskRadius = findAiryDiskDiameter(
                #     screen, screenPlane, I_sca, focalPoint)/2.

                # if the AiryDiskIs too smeared out, we do a mistake here
                self.AiryDiskRadius = self.mologram.calcAiryDiskDiameterAnalytical()/2
                # if self.AiryDiskRadius[0] > 1e-6:

                #     self.AiryDiskRadius[0] = 1e-6

                # signal (scattered signal at the focal point)
                if 'x' in screenPlane:
                    idx1 = int(npix/2 + center[0]/screenWidth)
                if screenPlane[0] == 'y':
                    idx1 = int(npix/2 + center[1]/screenWidth)  # /screenRatio
                if screenPlane[1] == 'y':
                    idx2 = int(npix/2 + center[1]/screenWidth)
                if 'z' in screenPlane:
                    idx2 = int(npix/2)

                I_signal = I_sca[idx1, idx2]

                warnings.simplefilter("ignore", category=RuntimeWarning)

                I_molo, bkg_mean, bkg_std, SNR = calc_IntAiryDisk(
                    I_sca, I_bg,pixelsize=screenWidth/npix, Airy_disk_radius=self.AiryDiskRadius)

        else:
            # only one pixel -> parameters cannot be calculated
            I_signal = float(I_sca)
            I_molo = float('Nan')
            bkg_mean = float('Nan')
            bkg_std = float('Nan')
            SNR = float('Nan')
            AiryDiskRadius = [float('Nan'), float('Nan')]

        return I_signal, I_molo, SNR, bkg_mean, bkg_std

        # if a function is provided to store the simulation results the results
        # are stored according to

    def SetupSimulationEnvironment(self):
        """Sets up the waveguide, the mologram and the screen and interpolates the dipole potentials."""

        # setup configuration (waveguide + media)

        self.configuration = self.build_configuration(
            self.simInput.waveguidePar.getValues())

        # setup mologram
        self.mologram = self.build_mologram(
            self.simInput.mologramPar.getValues(), self.configuration)
        self.simInput.setValues(self.mologram.getValues())

        # setup screen, 
        if self.simInput.screenPar.getValues()['screen_z_position'] == -1:

            self.screen = self.build_screen(self.simInput.screenPar.getValues(), self.mologram.focalPoint*self.mologram.direction)

            self.simInput.screenPar.setValues({'screen_z_position':self.mologram.focalPoint*self.mologram.direction})


        else:
            self.screen = self.build_screen(
                self.simInput.screenPar.getValues(), self.simInput.screenPar.getValues()['screen_z_position'])


        # setup the background screen

        self.background_screen = self.build_screen(
            self.simInput.screenPar.getValues(), self.mologram.focalPoint,background = True)

        # set up potential
        self.Phi = self.interpolatePotentials(self.configuration, self.screen)



    def build_configuration(self, waveguideParameters):
        """function that builds the layered dielectrics and waveguide."""

        globals().update(waveguideParameters)
        # media setup

        substrateMedium = PlaneMaterial(substrate,
                                        wavelength=wavelength,
                                        thickness=700e-6)  # todo: dependency thickness

        filmMedium = PlaneMaterial(film,
                                   wavelength=wavelength,
                                   thickness=thickness)

        coverMedium = IsotropicMaterial(cover,
                                        wavelength=wavelength)

        media = Media([inf, 0, -thickness, -inf],
                      [coverMedium, filmMedium, substrateMedium])

        # account for laser fluctuations.
        fluctuations = waveguideParameters['Laserfluctuations']


        if fluctuations == 0:
            self.inputPower = inputPower
            logging.info('No Laserfluctuations')
        else:
            self.inputPower = inputPower + \
                np.random.normal(0, inputPower*fluctuations)
            logging.info('Laserfluctuations' + str(fluctuations))

        # waveguide setup

        # calculate the power per unit length in the waveguide.
        powerperUnitLength = self.inputPower/(beamWaist)
        effectivePowerperUnitLength = couplingEfficiency * powerperUnitLength

        waveguide = SlabWaveguide(name=film, n_s=substrateMedium.n, n_f=filmMedium.n,
                                  n_c=coverMedium.n, d_f=thickness, polarization=polarization,
                                  inputPower=effectivePowerperUnitLength,  # important: power per m
                                  wavelength=wavelength,
                                  attenuationConstant=propagationLoss, mode=mode, x_0=gratingPosition)

        configuration = Configuration(media, waveguide)

        return configuration

    def build_mologram(self, mologramPar, configuration):

        globals().update(mologramPar)

        # mologram setup
        
        mologram = Mologram(configuration.waveguide, focalPoint = focalPoint, diameter= diameter,NA = NA,xshift=xshift, braggOffset = braggOffset,shape=shape, aspectRatio=aspectRatio,fieldSimulation = True)

        # update the mologramPar


        return mologram

    def build_screen(self, screenPar, z,background = False):

        globals().update(screenPar)

        if background: # create the screen for the background calculation
            screen = defineScreen(screenWidth, npix, screenPlane, screenRatio, screenRotation, z, centerBackground)
        else:
            screen = defineScreen(screenWidth, npix, screenPlane, screenRatio, screenRotation, z, center)

        return screen

    def interpolatePotentials(self, configuration, screen):

        # potentials: to speed up simulation, potentials are calculated here
        # and interpolated in the field calculations
        theta_intp = np.linspace(0, 30, 100)
        theta_intp = theta_intp/180.*pi

        wavelength = configuration.waveguide.wavelength
        refractiveIndices = configuration.Media.returnRefractiveIndices()
        thickness = configuration.waveguide.thickness

        k_0 = 2*pi/wavelength
        if (screen[2] < 0).all():
            Phi = potentialsLower(k_0, np.zeros(
                100), theta_intp, refractiveIndices, thickness)
        else:
            Phi = potentialsUpper(k_0, np.zeros(
                100), theta_intp, refractiveIndices, thickness)

        return Phi


def calcWGBackground(configuration, mologram):
    """calculates the background scattering into the focal spot, depending on the attentuation
    constant of the waveguide."""

    waveguide = configuration.waveguide
    wavelength = configuration.waveguide.wavelength
    focalPoint = mologram.focalPoint

    # background from waveguide surface scattering
    # inputPower is effective power per unit length, is just not correctly
    # implemented.
    I_bwg = backgroundScatteringWaveguide(
        waveguide.inputPower, waveguide.attenuationConstant, focalPoint, D=mologram.radius*2, x_0=waveguide.x_0,n=mologram.n_m)

    return I_bwg


def incidentIntensity(configuration):

    waveguide = configuration.waveguide
    # wave impedance at the focal point
    # returns the cover medium.
    Z_w = sqrt(constants.mu_0/configuration.Media.returnMedium(1e-6).epsilon)

    # calculate a few waveguide parameters
    E_0 = [waveguide.E_x(wavelength, np.array([0]), np.array([0]), np.array([0])),
           waveguide.E_y(wavelength, np.array(
               [0]), np.array([0]), np.array([0])),
           waveguide.E_z(wavelength, np.array([0]), np.array([0]), np.array([0]))]

    # intensity per unit length impinging onto the mologram on the surface of
    # the waveguide.
    I_0 = (1/(2*Z_w))*(sum(E_0[0]*np.conj(E_0[0])+E_0[1]
                           * np.conj(E_0[1])+E_0[2]*np.conj(E_0[2]))).real

    return I_0, Z_w


def calcScattering(configuration, dipolePositions, dipoleMoments, screen, Phi, mologram, calculationMethod, textBrowser):
    """
    This function can be used if the screen, the mologram and the waveguide are already defined. Therefore
    it can be used to accelerate everything. Only returns the scattered field and intensity.

    :param E_sca: scattered field
    :param I_sca: scattered Intensity
    """
    wavelength = configuration.waveguide.wavelength
    waveguide = configuration.waveguide
    thickness = configuration.waveguide.thickness

    k_0 = 2*pi/wavelength
    # returns the cover medium.
    Z_w = sqrt(constants.mu_0 /
               configuration.Media.returnMedium(mologram.focalPoint).epsilon)
    x_sca, y_sca, z_sca = dipolePositions
    # print np.min(x_sca)
    # print np.max(x_sca)
    # print dipoleMoments
    # print Phi

    refractiveIndices = np.array(
        [medium.n for medium in configuration.Media.media])
    # print refractiveIndices

    # phase at the scatterer positions
    phase = fields.calcPhasePlaneXY(x_sca, y_sca, k_0*waveguide.N)

    # electric field calculation (parallelized calculation)
    # number of parallel processes (somehow arbitrary)
    num_process = np.min([1000, 1+len(dipolePositions[0])/100])

    if(calculationMethod == "parallelPool"):    # use if unix OS
        E_sca = parallelCalculationInterface(dipolePositions, dipoleMoments, phase, screen,
                                             refractiveIndices, thickness, k_0, Phi, num_process, textBrowser)  # use multiple cores
        logging.info('Used calculationMethod Parallel Pools')
        logging.debug(E_sca.dtype)

    elif(calculationMethod == "splitProcess"):  # use if windows OS
        E_sca = splitProcessInterfaces(dipolePositions, dipoleMoments, phase,
                                       screen, refractiveIndices, thickness, k_0, Phi, num_process, textBrowser)
        logging.info('Used calculationMethod splitProcess')

    elif(calculationMethod == "Cuda"):  # use if you're cool enough
        logging.info('Used calculationMethod CUDA')
        path = os.path.dirname(__file__)
        lib = ctypes.cdll.LoadLibrary(path + '/GPU_molo_lib.so')
        gpu_function = lib.moloGPU  # using the c-function
        gpu_function.restype = None
        gpu_function.argtypes = [ctypes.c_float, ctypes.c_float, ctypes.c_float,  # dTheta, k_n, k_1
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # Real fields
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # Imag fields
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # Real Phi
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # Imag Phi
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # real polar
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # imag polar
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # screen
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float),  # refractive indices, phases,
                                 ndpointer(ctypes.c_float), ndpointer(
                                     ctypes.c_float), ndpointer(ctypes.c_float),  # dipole x,y,z
                                 ctypes.c_int, ctypes.c_int, ctypes.c_int]  # n dipoles, n_pixx, n_pixy

        d_exec = time.time()
        Phi_0, Phi_1, Phi_2 = Phi
        rP0, rP1, rP2 = np.asarray(
            [Phi_0.real, Phi_1.real, Phi_2.real]).astype(np.float32)
        iP0, iP1, iP2 = np.asarray(
            [Phi_0.imag, Phi_1.imag, Phi_2.imag]).astype(np.float32)
        x, y, z = dipolePositions.astype(np.float32)
        px, py, pz = dipoleMoments
        r_px, r_py, r_pz = np.asarray(
            [px.real, py.real, pz.real]).astype(np.float32)
        i_px, i_py, i_pz = np.asarray(
            [px.imag, py.imag, pz.imag]).astype(np.float32)
        n = np.asarray(refractiveIndices).astype(np.float32)
        sc_x, sc_y, sc_z = screen
        sc_x = sc_x.flatten()
        sc_y = sc_y.flatten()
        sc_z = sc_z.flatten()
        sc_z = sc_z.astype(np.float32)
        sc_x = sc_x.astype(np.float32)
        sc_y = sc_y.astype(np.float32)
        phase = phase[0].astype(np.float32)
        dTheta = np.float32((np.pi/6./100))
        k_n = np.float32(n[2]*k_0)
        k_1 = np.float32(n[0]*k_0)
        E_x = np.zeros(np.shape(sc_x), dtype=np.float32)
        E_y = np.zeros(np.shape(sc_x), dtype=np.float32)
        E_z = np.zeros(np.shape(sc_x), dtype=np.float32)
        E_xi = np.zeros(np.shape(sc_x), dtype=np.float32)
        E_yi = np.zeros(np.shape(sc_x), dtype=np.float32)
        E_zi = np.zeros(np.shape(sc_x), dtype=np.float32)
        ndip = len(x)
        npixx = npix
        npixy = npix

        gpu_function(dTheta, k_n, k_1, E_x, E_y, E_z, E_xi, E_yi, E_zi, rP0, rP1, rP2, iP0, iP1, iP2, r_px,
                     r_py, r_pz, i_px, i_py, i_pz, sc_x, sc_y, sc_z, n, phase, x, y, z, ndip, npixx, npixy)  # magic

        E_x = E_x.astype(np.float)
        E_y = E_y.astype(np.float)
        E_z = E_z.astype(np.float)
        E_xi = E_xi.astype(np.float)
        E_yi = E_yi.astype(np.float)
        E_zi = E_zi.astype(np.float)
        k_1 = k_0*n[0]
        eps_m = n[0]**2 * constants.eps_0
        prefac = (1/(4*pi*eps_m) * k_1**2)

        E_sca_real = prefac * np.array([E_x, E_y, E_z])
        E_sca_imag = prefac * np.array([E_xi, E_yi, E_zi])

        E_sca = E_sca_real + E_sca_imag*1j # complex number
        I_sca = 1/(2*Z_w)*prefac**2*np.reshape(E_x*E_x+E_y*E_y+E_z *
                                            E_z+E_xi*E_xi+E_yi*E_yi+E_zi*E_zi, (npixx, npixy))
        d_exec = d_exec-time.time()

    else:
        E_sca = calcField(dipolePositions, dipoleMoments, phase,
                          screen, refractiveIndices, thickness, k_0, Phi)

    if(calculationMethod != "Cuda"):
        I_sca = 1/(2*Z_w) * sum(E_sca*conj(E_sca), axis=0).real
        # convert nan to zeros
        I_sca = np.nan_to_num(I_sca)

    return E_sca, I_sca


def checkNans(dictionary, textBrowser=''):
    """
    Function that checks if there are nan's in some variable and emits the result to the QLineEdit() Object textbrowser

    Use >> checkNans(locals(), textBrowser)
    """
    keys = []
    for key, value in dictionary.items():
        try:
            if sum([np.isnan(value)]) > 0:
                keys.append(key)

        except:
            continue

    keys.sort()
    if len(keys) != 0:
        if textBrowser != '':
            textBrowser.emit(
                ['<b>warning: The following variables contain nan\'s:\n{}<b>'.format(keys)])
        else:
            print 'warning: The following variables contain nan\'s:\n{}'.format(keys)

# def run_simulation(parameters):
#     """
#     :param parameters: The parameters of the simulation as a dictionary
#     :param textBrowser: some sort of signal of which I do not really know what it does.
#     :param resultsSignal: signal to call the function updateResults in the MainWidget class, the locals() variable dictionary is returned.
#     :param (boolean)sweepScatters: default False, if True, scatterers are added sequentlially in order to simulate a binding curve.


#     """

#     if textBrowser != '': textBrowser.emit([0,float('inf')])
#     # settings

#     # generate mologram
#     warnings.simplefilter("ignore", category=RuntimeWarning)

#     # # calculate a few Mologram parameters
#     # NA = diameter*configuration.returnMedium(focalPoint).n/(2*abs(focalPoint))  # numerical aperture

#     # this needs to be a for loop that runs through the binders accounts for molecular shot noise

#     # calculate properties for the particle NEED TO ACCOUNT FOR DIFFERENT PARTICLES

#     # perform multiple simulations if the binder number is too large.


#     # sweep scatterers
#     if sweepScatterers == False:
#         iterations = 1
#     else:
#         # for video or binding experiment...
#         iterations = len(x_sca_all)

#     timeRequired = time.time()

#     for i in np.linspace(len(x_sca_all),1,iterations, dtype=int)[::-1]:

#         if iterations > 1: time.sleep(2-(timeRequired-time.time()))

#         # update positions
#         x_sca = x_sca_all[:i]
#         y_sca = y_sca_all[:i]
#         z_sca = z_sca_all[:i]
#         bind_no_bind = bind_no_bind_all[:i]
#         textBrowser.emit(['{} Particles'.format(len(x_sca))])

#         # required !
#         mologram.dipolePositions = x_sca, y_sca, z_sca
#         mologram.boundParticles = bind_no_bind < analyte_coherentBinding

#         z_avg = np.mean(z_sca)
#         r_avg = np.mean(np.sqrt((np.abs(focalPoint)+z_sca)**2+x_sca**2+y_sca**2))


#         # here I could call the function calcScattering

#         E_sca, I_sca = calcScattering(wavelength,dipolePositions,screen,Phi,waveguide,mologram,configuration,materialParticle,calculationMethod,textBrowser)


#         if textBrowser != '':
#             textBrowser.emit([100, 0])
#         else:
#             print ("100%")

#     return self.results.getValues()
