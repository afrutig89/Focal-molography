# -*- coding: utf-8 -*-
from os import sys, path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import os
from PyQt4 import QtGui, QtCore
from PyQt4.QtWebKit import QWebView
from PyQt4.QtCore import QUrl
# import subprogress
from GUI.Main_widget import MainWidget
from GUI.worker import WorkerObject
from SharedLibraries.Database.dataBaseOperations import checkFolder, loadDB, createDB
from variables import FieldSimulationInputs
import webbrowser
import subprocess

class MainWindow(QtGui.QMainWindow, QtCore.QObject):

    signalStatus = QtCore.pyqtSignal(str)

    def __init__(self, app):
        # Call parent constructor
        super(MainWindow, self).__init__()

        self.app = app
        
        # Call child constructor
        self.initUI()
        
    def initUI(self):

        # load defaul values
        if not 'default.db' in os.listdir('database'):
            createDB('database/default.db', 'log', FieldSimulationInputs.returnDBPar())

        default_values = loadDB('database/default.db','log')

        QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))
        
        # Create widgets
        main_widget = MainWidget(self.app, default_values)
        self.setCentralWidget(main_widget)
        self.main_widget = main_widget

        # Setup the worker object and the worker_thread.
        self.worker = WorkerObject(main_widget)
        self.worker_thread = QtCore.QThread() # Launch a new thread. 
        self.worker.moveToThread(self.worker_thread)
        self.worker_thread.start()
        self.worker.signalStatus.connect(self.updateStatusBar) # function for simulation progress visualization. 
        self.worker.results.connect(self.main_widget.updateResultsGUI)
        # self.worker.signalStatus.connect(self.main_widget.updateProgressbar)
        self.app.aboutToQuit.connect(self.forceWorkerQuit)

        # create close window action
        exitAction = QtGui.QAction(QtGui.QIcon('GUI/images/exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)
        exitAction.setToolTip('<font color=black>Exit</font>')

        # create open help action
        helpAction = QtGui.QAction(QtGui.QIcon('GUI/images/help.png'), 'Documentation', self)
        helpAction.setShortcut('Ctrl+H')
        helpAction.setStatusTip('Open Documentation')
        helpAction.triggered.connect(lambda: self.main_widget.openHelp('index.html'))
        helpAction.setToolTip('<font color=black>Documentation</font>')

        # create save action
        saveDefaults = QtGui.QAction(QtGui.QIcon('GUI/images/save.png'), 'Save as default', self)
        saveDefaults.setShortcut('Ctrl+S')
        saveDefaults.setStatusTip('Save Settings')
        saveDefaults.triggered.connect(main_widget.saveDefaults)
        saveDefaults.setToolTip('<font color=black>Save Settings</font>')

        # generate summary file (rst)
        exportSummary = QtGui.QAction('Export summary', self)
        exportSummary.setStatusTip('Export summary')
        exportSummary.triggered.connect(main_widget.generateRST)

        # create load action
        loadAction = QtGui.QAction('Inspect Database', self)
        loadAction.setShortcut('Ctrl+L')
        loadAction.setStatusTip('Inspect Database')
        loadAction.setToolTip('<font color=black>Inspect Database</font>')
        loadAction.triggered.connect(main_widget.openLoadSettingsWindow)

        loadExperiment = QtGui.QAction('Load Experiment', self)
        loadExperiment.setStatusTip('Load Settings')
        loadExperiment.setToolTip('<font color=black>Load Experiments</font>')
        loadExperiment.triggered.connect(main_widget.loadExperiment)

        # run simulation action
        runSimulationAction = QtGui.QAction(QtGui.QIcon('GUI/images/play.png'), 'Run Simulation', self)
        runSimulationAction.setShortcut('F5')
        runSimulationAction.setStatusTip('Run Simulation')
        runSimulationAction.triggered.connect(self.worker.startSimulation)
        runSimulationAction.setToolTip('<font color=black>Run Simulation (F5)</font>')

        # run simulation action
        runSimulationFilmAction = QtGui.QAction('Make Video (Single Particles)', self)
        runSimulationFilmAction.setStatusTip('Sweep Scatterers')
        runSimulationFilmAction.triggered.connect(lambda: self.worker.startSimulation(sweepScatterers = True))
        runSimulationFilmAction.setToolTip('<font color=black>Make Video</font>')

        # run simulation action
        stopSimulationAction = QtGui.QAction(QtGui.QIcon('GUI/images/stop.png'), 'Abort Simulation', self)
        stopSimulationAction.setShortcut('Ctrl+U')
        stopSimulationAction.setStatusTip('Abort Simulation')
        stopSimulationAction.triggered.connect(self.forceWorkerReset)
        stopSimulationAction.setToolTip('<font color=black>Abort Simulation</font>')

        # show/hide bars
        showParameterBar = QtGui.QAction('Parameter Bar', self, checkable = True)
        showParameterBar.setChecked(True)
        showParameterBar.setShortcut('')
        showParameterBar.triggered.connect(main_widget.showParameterBar)
        showParameterBar.setStatusTip('Parameter Bar')

        showTabBar = QtGui.QAction('Tab Bar', self, checkable = True)
        showTabBar.setChecked(True)
        showTabBar.setShortcut('')
        showTabBar.triggered.connect(main_widget.showTabBar)
        showTabBar.setStatusTip('Tab Bar')

        showLog = QtGui.QAction('Log', self, checkable = True)
        showLog.setChecked(True)
        showLog.setShortcut('')
        showLog.triggered.connect(main_widget.showLog)
        showLog.setStatusTip('Log')

        showProgressBar = QtGui.QAction('Progressbar', self, checkable = True)
        showProgressBar.setShortcut('')
        showProgressBar.triggered.connect(main_widget.showProgressBar)
        showProgressBar.setStatusTip('Progressbar')

        compareTab = QtGui.QAction('Compare Tab', self)
        compareTab.setShortcut('')
        compareTab.triggered.connect(main_widget.addTab)
        compareTab.setStatusTip('Compare Tab')

        analyticalTab = QtGui.QAction('Analytical Tab', self, checkable = True)
        analyticalTab.setShortcut('')
        analyticalTab.setStatusTip('Analytical Tab')

        showExperimentTab = QtGui.QAction('Show Experiment Tab', self, checkable = True)
        showExperimentTab.setStatusTip('Show Experiment Tab')

        showSweepTab = QtGui.QAction('Show Sweep Tab', self, checkable = True)
        showSweepTab.setStatusTip('Show Sweep Tab')

        plotMologramTab = QtGui.QAction('Mologram', self, checkable = True)
        plotMologramTab.setShortcut('')
        plotMologramTab.setStatusTip('Plot mologram')

        openMaterialsWindow = QtGui.QAction('Materials', self)
        openMaterialsWindow.triggered.connect(main_widget.openMaterialsWindow)


        # Create a status barcouplingEfficiency
        self.statusBar = QtGui.QStatusBar()
        self.setStatusBar(self.statusBar)

        restart = QtGui.QAction(QtGui.QIcon('GUI/images/restart.png'), 'Restart', self)
        restart.setStatusTip('Restart program')
        restart.triggered.connect(self.restartGUI)

        # Menu bar
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(saveDefaults)
        fileMenu.addAction(loadAction)
        fileMenu.addSeparator()
        fileMenu.addAction(loadExperiment)
        fileMenu.addSeparator()
        fileMenu.addAction(exportSummary)
        fileMenu.addAction(exitAction)

        runMenu = menubar.addMenu('&Run')
        runMenu.addAction(runSimulationAction)
        runMenu.addAction(runSimulationFilmAction)

        viewMenu = menubar.addMenu('&View')
        viewMenu.addAction(showParameterBar)
        viewMenu.addAction(showTabBar)
        viewMenu.addAction(showLog)
        viewMenu.addSeparator()
        viewMenu.addAction(showExperimentTab)
        viewMenu.addAction(showSweepTab)
        viewMenu.addSeparator()
        viewMenu.addAction(compareTab)
        viewMenu.addAction(analyticalTab)
        viewMenu.addAction(plotMologramTab)

        viewMenu.triggered.connect(self.main_widget.showTab)

        # todo
        settingsMenu = menubar.addMenu('&Preferences')
        settingsMenu.addAction(QtGui.QAction('General settings', self))
        settingsMenu.addAction(QtGui.QAction('Keys', self))
        settingsMenu.addAction(openMaterialsWindow)
        settingsMenu.addAction(QtGui.QAction('Shortcuts', self))

        helpMenu = menubar.addMenu('&Help')
        helpMenu.addAction(helpAction)

        # Toolbar
        toolbar = self.addToolBar('Toolbar')
        toolbar.addAction(runSimulationAction)
        toolbar.addAction(stopSimulationAction)
        toolbar.addAction(saveDefaults)
        toolbar.addSeparator()
        toolbar.addAction(helpAction)
        toolbar.addAction(exitAction)

        spacer = QtGui.QWidget()
        spacer.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        toolbar.addWidget(spacer)

        toolbar.addAction(restart)

        self.progressbar = QtGui.QProgressBar(self)
        self.progressbar.hide()
        self.statusBar.addPermanentWidget(self.progressbar)


        # Set Geometry, title, icon and show window
        self.setGeometry(300, 300, 1920, 1080)
        self.setWindowTitle('Focal Molography Software')
        self.setWindowIcon(QtGui.QIcon('GUI/images/microscope.png')) 
        self.showMaximized()


    def restartGUI(self):

        self.close()

        abspath = os.path.abspath(__file__)
        mainFile = "{}/mainGUI.py".format(os.path.dirname(abspath).replace(' ','\ '))
        subprocess.call("{} {}".format(sys.executable, mainFile), shell=True)

    def showParameterBar(self):
        if self.main_widget.parameterBar.isVisible():
            self.main_widget.parameterBar.hide()
            self.showParameterBar.setText("Show Parameterbar")
        else:
            self.main_widget.parameterBar.show()
            self.showParameterBar.setText("Hide Parameterbar")


    def forceWorkerReset(self):
        if self.worker_thread.isRunning():
            self.worker_thread.terminate()
            self.worker_thread.wait()

            self.main_widget.updateStatus('simulation aborted')

            self.statusBar.showMessage('simulation aborted')
            self.progressbar.setValue(0)
            
            self.worker_thread.start()


    def forceWorkerQuit(self):
        if self.worker_thread.isRunning():
            self.worker_thread.terminate()
            self.worker_thread.wait()


    def updateStatusBar(self, progress):
        if len(progress) == 1:
            # only status message
            self.main_widget.updateStatus(progress[0])


        if len(progress) > 1:
            if not self.progressbar.isVisible():
                self.progressbar.show()

            self.statusBar.showMessage("Estimated time remaining: %.1fs" % progress[1])
            self.progressbar.setValue(progress[0])

            if len(progress) > 2:
                self.main_widget.updateStatus(progress[2])

            if progress[0] == 100.:
                self.statusBar.showMessage("Simulation done.")
                self.progressbar.hide()


    def closeEvent(self, event):
        settings, settingsSave = self.main_widget.settingsTab.getValues()
        if settings['saveLog']:
            txtFile = str(settings['databaseFile']).replace('.db','.txt')
            f = open(txtFile, 'a')
            f.write(self.main_widget.textBrowser.toPlainText() + '\n\n')
            f.close()


def start():
    app = QtGui.QApplication(sys.argv)
    w = MainWindow(app)
    app.exec_()
    del w

# Event loop
if __name__ == '__main__':
    start()



