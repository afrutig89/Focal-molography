# -*- coding: utf-8 -*-

from PyQt4 import QtGui, QtCore

import os
import sys
sys.path.append('../../')
from auxiliaries.writeLogFile import loadDB
from dipole.plots import plotParameter, findCoordinates
from imageProcessing.speckles import findSpeckles, averageSpeckleDimension, averageSpeckleSize

# matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import matplotlib.image as mpimg

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT# as NavigationToolbar

from matplotlib.backend_bases import cursors as mplCursors
from matplotlib.patches import Ellipse

from imageProcessing.MologramProcessing import calc_datapoint_entry

from skimage.util import random_noise
from mpl_toolkits.mplot3d import Axes3D

import numpy as np


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class PlotWidget(QtGui.QWidget):
    """Class that plots the intensity in the focal spot. """
    def __init__(self,main_widget):

        # Call parent class constructor
        super(PlotWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        # Call child constructor
        self.initUI()
        self.initImage()

    """ Initialization of UI """
    def initUI(self):

        self.figure  = plt.figure(figsize=(10,10),facecolor='None',edgecolor='None')
        self.canvas  = FigureCanvas(self.figure)

        # toolbar
        # run simulation action
        axis1 = str(self.main_widget.screenTab.screenPlane.currentText()[0])
        axis2 = str(self.main_widget.screenTab.screenPlane.currentText()[1])

        plotAiryDisk_x = QtGui.QAction(axis1, self)
        plotAiryDisk_x.setStatusTip('Plot Cross-Section in '+axis1+'-Axis')
        plotAiryDisk_x.triggered.connect(self.plotX)
        plotAiryDisk_x.setToolTip('<font color=black>Plot Cross-Section in '+axis1+'-Axis</font>')

        plotAiryDisk_y = QtGui.QAction(self.main_widget.screenTab.screenPlane.currentText()[1], self)
        plotAiryDisk_y.setStatusTip('Plot Cross-Section in '+axis2+'-Axis')
        plotAiryDisk_y.triggered.connect(self.plotY)
        plotAiryDisk_y.setToolTip('<font color=black>Plot Cross-Section in '+axis2+'-Axis</font>')

        plotAiryDisk_xy = QtGui.QAction(self.main_widget.screenTab.screenPlane.currentText(), self)
        plotAiryDisk_xy.setStatusTip('Plot Contour')
        plotAiryDisk_xy.triggered.connect(self.plotXY)
        plotAiryDisk_xy.setToolTip('<font color=black>Contour Plot</font>')

        # change color normalization
        self.colorNormalization_lbl = QtGui.QLabel('Color power norm')
        self.colorNormalization = QtGui.QComboBox()
        self.colorNormalization.addItem('1')
        self.colorNormalization.addItem('0.5')
        self.colorNormalization.addItem('0.25')
        self.colorNormalization.setCurrentIndex(1)
        self.colorNormalization.activated.connect(self.changeNormalization)

        plotAiryDisk_3d = QtGui.QAction('3D', self)
        plotAiryDisk_3d.setStatusTip('3D plot')
        plotAiryDisk_3d.triggered.connect(self.plot3d)
        plotAiryDisk_3d.setToolTip('<font color=black>3D Plot</font>')

        self.toolbar = QtGui.QToolBar()
        self.toolbar.addWidget(NavigationToolbar(self.canvas, self))
        # here goes the left one
        self.toolbar.addWidget(QtGui.QWidget())
        # some dummy actions
        self.toolbar.addWidget(self.colorNormalization_lbl)
        self.toolbar.addWidget(self.colorNormalization)
        self.toolbar.addAction(plotAiryDisk_x)
        self.toolbar.addAction(plotAiryDisk_y)
        self.toolbar.addAction(plotAiryDisk_xy)
        self.toolbar.addAction(plotAiryDisk_3d)
        # and the right one

        self.molography_calculations = QtGui.QGroupBox()
        self.molography_calculations.setGeometry(QtCore.QRect(35, 550, 956, 141))
        self.molography_calculations.setMinimumSize(QtCore.QSize(350, 146))
        self.molography_calculations.setObjectName("mologram_calculations")

        self.signalIntensity_lbl = QtGui.QLabel(self.molography_calculations)
        self.signalIntensity_lbl.setGeometry(QtCore.QRect(25, 20, 186, 17))
        self.signalIntensity_lbl.setObjectName("signalIntensity_lbl")
        self.signalIntensity_lbl.setText(_translate("Form", "Molo Intensity (W/m²)", None))

        self.signalIntensity = QtGui.QLabel(self.molography_calculations)
        self.signalIntensity.setGeometry(QtCore.QRect(250, 20, 286, 17))
        self.signalIntensity.setObjectName("signalIntensity")

        self.bg_int_lbl = QtGui.QLabel(self.molography_calculations)
        self.bg_int_lbl.setGeometry(QtCore.QRect(25, 50, 220, 17))
        self.bg_int_lbl.setObjectName("bg_int_lbl")
        self.bg_int_lbl.setText(_translate("Form", "Background Mean / Std (W/m²)", None))

        self.bg_int = QtGui.QLabel(self.molography_calculations)
        self.bg_int.setGeometry(QtCore.QRect(250, 50, 286, 17))
        self.bg_int.setObjectName("bg_int")

        self.SNR_label = QtGui.QLabel(self.molography_calculations)
        self.SNR_label.setGeometry(QtCore.QRect(25, 80, 186, 17))
        self.SNR_label.setObjectName("SNR_label")
        self.SNR_label.setText('SNR')

        self.SNR = QtGui.QLabel(self.molography_calculations)
        self.SNR.setGeometry(QtCore.QRect(250, 80, 286, 17))
        self.SNR.setObjectName("SNR")

        self.max_pixel_lbl = QtGui.QLabel(self.molography_calculations)
        self.max_pixel_lbl.setGeometry(QtCore.QRect(25, 110, 206, 17))
        self.max_pixel_lbl.setObjectName("max_pixel_lbl")
        self.max_pixel_lbl.setText(_translate("Form", "Max. / Min. Intensity (W/m²)", None))

        self.max_pixel = QtGui.QLabel(self.molography_calculations)
        self.max_pixel.setGeometry(QtCore.QRect(250, 110, 286, 17))
        self.max_pixel.setObjectName("max_pixel")


        main_box = QtGui.QHBoxLayout()

        # set up canvas
        v_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
        v_splitter.addWidget(self.toolbar)
        v_splitter.addWidget(self.canvas)
        v_splitter.addWidget(self.molography_calculations)

        main_box.addWidget(v_splitter)

        self.setLayout(main_box)

    def initImage(self):
        img = mpimg.imread('GUI/images/Illustrative_Figure_Molography.png')

        # clear and redeclare axes
        plt.cla()
        self.ax = self.figure.add_subplot(111)
        self.axis = 0    # contour plot (1/2 for x/y plots)

        # set up axes
        self.ax.xaxis.set_ticks_position('none')   # tick markers
        self.ax.yaxis.set_ticks_position('none')
        plt.xticks([])                        # labels
        plt.yticks([])
        self.img = plt.imshow(img,
            cmap=plt.cm.jet,
            vmin=0, vmax=4096,
            interpolation='none',
            norm=colors.PowerNorm(gamma=1./2.))

        self.figure.tight_layout()

        # show image
        self.canvas.draw()

    def newImage(self, screen, intensity, focalPoint):

        self.screen = screen
        self.intensity = intensity
        self.focalPoint = focalPoint

        self.updateImage()


    def plotXY(self):
        self.updateImage(0)

    def plotX(self):
        self.updateImage(1)

    def plotY(self):
        self.updateImage(2)

    def plot3d(self):
            plt.clf()
            self.ax = self.figure.add_subplot(111, projection='3d')
            screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(self.screen, self.focalPoint)
            z=self.intensity.real
            x=screen_1*1e6
            y=screen_2*1e6
            self.img = self.ax.plot_surface(x,y,z)
            self.canvas.draw()

    def updateImage(self, axis = 0, image = []):
        if not 'screen' in dir(self):
            self.initImage()

        else:
            gamma = float(str(self.colorNormalization.currentText()))

            plt.clf()
            self.ax = self.figure.add_subplot(111)
            # self.img.set_data(intensity)


            if image == []: image = self.intensity

            parameter = image
            screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(self.screen, self.focalPoint)


            if axis == 0:
                # todo: axes are probably wrong with imshow, but its faster. fix this
                extent = (np.min(screen_1),np.max(screen_1),np.min(screen_2),np.max(screen_2))
                self.img = plt.imshow(parameter,
                                cmap = plt.cm.inferno,
                                origin = 'upper',
                                extent = [border*1e6 for border in extent],
                                norm = colors.PowerNorm(gamma=gamma))

                plt.xlabel(axis1)
                plt.ylabel(axis2)
                plt.axes().set_aspect('equal')

                ax = plt.gca()
                divider = make_axes_locatable(ax)
                cax1 = divider.append_axes("right", size="5%", pad=0.1)
                self.cbar = plt.colorbar(self.img, cax = cax1, ticks=np.linspace(0,np.max(parameter),4), format='%.0e')
                self.cbar.ax.tick_params(labelsize=10)

            else:
                if axis == 1:
                    if (np.diff(screen_1[0,:]) == 0).all():
                        x = screen_1[:,0]
                        plt.plot(x*1e6,parameter[:,idx1])
                    else:
                        x = screen_1[0,:]
                        plt.plot(x*1e6,parameter[idx1,:])
                    plt.xlabel(axis1)
                elif axis == 2:
                    if (np.diff(screen_2[0,:]) == 0).all():
                        x = screen_2[:,0]
                        plt.plot(x*1e6,parameter[:,idx2])
                    else:
                        x = screen_2[0,:]
                        plt.plot(x*1e6,parameter[idx2,:])
                    plt.xlabel(axis2)
                    plt.xlim([np.min(x)*1e6,np.max(x)*1e6])

                plt.ylabel('Irradiance')
                plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

            self.figure.tight_layout()
            self.canvas.draw()



    def plotSpeckles(self, neighborhoodSize, threshold):
        if not 'screen' in dir(self):
            self.initImage()
        else:
            plt.clf()
            # set up axes
            self.ax = self.figure.add_subplot(111)
            self.img = plotParameter(
                self.screen, self.focalPoint,
                self.intensity, axis = 0,
                levels = 10
            )
            if threshold == 'mean':
                threshold = np.mean(self.intensity)
            else:
                threshold = float(threshold)

            # screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(self.screen, self.focalPoint)
            screenWidth = float(self.main_widget.screenTab.screenWidth.text()) # in um
            npix = float(self.main_widget.screenTab.npix.text())
            screenRatio = float(self.main_widget.screenTab.axisRatio1.text()) \
                          / float(self.main_widget.screenTab.axisRatio1.text())
            center = np.array([float(self.main_widget.screenTab.center_x.text()),
                                float(self.main_widget.screenTab.center_y.text())])

            specklePositions = findSpeckles(self.intensity, neighborhoodSize, threshold)
            avgSpeckleSize = averageSpeckleSize(self.intensity, specklePositions)*1e3
            avgSpeckleDimension = averageSpeckleDimension(self.intensity, specklePositions) * screenWidth/npix

            x = specklePositions[0] * screenWidth/npix - 1/2.*screenRatio*screenWidth + center[0]
            y = specklePositions[1] * screenWidth/npix - 1/2./screenRatio*screenWidth + center[1]

            ax = plt.axes()
            ax.plot(x,y, 'wo')

            # TODO: colorbar disappears

            self.figure.tight_layout()
            self.canvas.draw()

            return len(x), len(x)/screenWidth**2, avgSpeckleDimension, avgSpeckleSize


    def updateValues(self, image):
        # calculate molo intensity

        results = calc_datapoint_entry(
                image,
                algorithm = 'sum over background plus 3 sigma'
            )

        signalIntensity, bkg_mean, bkg_std, SNR = results
        noMaxPixel = np.sum(self.img == np.max(image))

        # update text labels
        self.signalIntensity.setText(str(signalIntensity))
        self.bg_int.setText('{} / {}'.format(bkg_mean, bkg_std))
        self.SNR.setText(str(SNR))
        self.max_pixel.setText('{} ({}) / {}'.format(np.max(image),noMaxPixel, np.min(image)))

    def addNoise(self, mode, **kwargs):
        """
        Function to add random noise of various types to a floating-point image.
        Parameters
        ----------
        image : ndarray
            Input image data. Will be converted to float.
        mode : str
            One of the following strings, selecting the type of noise to add:
            - 'gaussian'  Gaussian-distributed additive noise.
            - 'localvar'  Gaussian-distributed additive noise, with specified
                          local variance at each point of `image`
            - 'poisson'   Poisson-distributed noise generated from the data.
            - 'salt'      Replaces random pixels with 1.
            - 'pepper'    Replaces random pixels with 0 (for unsigned images) or
                          -1 (for signed images).
            - 's&p'       Replaces random pixels with either 1 or `low_val`, where
                          `low_val` is 0 for unsigned images or -1 for signed
                          images.
            - 'speckle'   Multiplicative noise using out = image + n*image, where
                          n is uniform noise with specified mean & variance.
        clip : bool
            If True (default), the output will be clipped after noise applied
            for modes `'speckle'`, `'poisson'`, and `'gaussian'`. This is
            needed to maintain the proper image data range. If False, clipping
            is not applied, and the output may extend beyond the range [-1, 1].
        mean : float
            Mean of random distribution. Used in 'gaussian' and 'speckle'.
            Default : 0.
        var : float
            Variance of random distribution. Used in 'gaussian' and 'speckle'.
            Note: variance = (standard deviation) ** 2. Default : 0.01
        local_vars : ndarray
            Array of positive floats, same shape as `image`, defining the local
            variance at every image point. Used in 'localvar'.
        amount : float
            Proportion of image pixels to replace with noise on range [0, 1].
            Used in 'salt', 'pepper', and 'salt & pepper'. Default : 0.05
        salt_vs_pepper : float
            Proportion of salt vs. pepper noise for 's&p' on range [0, 1].
            Higher values represent more salt. Default : 0.5 (equal amounts)
        Returns
        -------
        out : ndarray
            Output floating-point image data on range [0, 1] or [-1, 1] if the
            input `image` was unsigned or signed, respectively.
        Notes
        -----
        Speckle, Poisson, Localvar, and Gaussian noise may generate noise outside
        the valid image range. The default is to clip (not alias) these values,
        but they may be preserved by setting `clip=False`. Note that in this case
        the output may contain values outside the ranges [0, 1] or [-1, 1].
        Use this option with care.
        Because of the prevalence of exclusively positive floating-point images in
        intermediate calculations, it is not possible to intuit if an input is
        signed based on dtype alone. Instead, negative values are explicity
        searched for. Only if found does this function assume signed input.
        Unexpected results only occur in rare, poorly exposes cases (e.g. if all
        values are above 50 percent gray in a signed `image`). In this event,
        manually scaling the input to the positive domain will solve the problem.
        The Poisson distribution is only defined for positive integers. To apply
        this noise type, the number of unique values in the image is found and
        the next round power of two is used to scale up the floating-point result,
        after which it is scaled back down to the floating-point image range.
        To generate Poisson noise against a signed image, the signed image is
        temporarily converted to an unsigned image in the floating point domain,
        Poisson noise is generated, then it is returned to the original range.
        """

        image = self.intensity
        mode = mode.lower()

        allowedtypes = self.main_widget.backgroundTab.allowedNoiseTypes()
        kwdefaults = self.main_widget.backgroundTab.kwdefaultsFunc()
        allowedkwargs = self.main_widget.backgroundTab.allowedkwargsFunc()

        for key in kwargs:
            if key not in allowedkwargs[allowedtypes[mode]]:
                raise ValueError('%s keyword not in allowed keywords %s' %
                                 (key, allowedkwargs[allowedtypes[mode]]))

        # Set kwarg defaults
        for kw in allowedkwargs[allowedtypes[mode]]:
            kwargs.setdefault(kw, kwdefaults[kw])

        if mode == 'gaussian':
            noise = np.random.normal(kwargs['mean'], kwargs['std'],
                                     image.shape)
            out = image + noise

        elif mode == 'localvar':
            # Ensure local variance input is correct
            if kwargs['local_vars'] <= 0:
                raise ValueError('All values of `local_vars` must be > 0.')

            # Safe shortcut usage broadcasts kwargs['local_vars'] as a ufunc
            out = image + np.random.normal(0, kwargs['local_vars'] ** 0.5)

        elif mode == 'poisson':
            # Determine unique values in image & calculate the next power of two
            vals = len(np.unique(image))
            vals = 2 ** np.ceil(np.log2(vals))

            # Generating noise for each unique value in image.
            out = np.random.poisson(image * vals) / float(vals)


        elif mode == 'salt':
            # Re-call function with mode='s&p' and p=1 (all salt noise)
            out = random_noise(image, mode='s&p',
                               amount=kwargs['amount'], salt_vs_pepper=1.)

        elif mode == 'pepper':
            # Re-call function with mode='s&p' and p=1 (all pepper noise)
            out = random_noise(image, mode='s&p',
                               amount=kwargs['amount'], salt_vs_pepper=0.)

        elif mode == 's&p':
            out = image.copy()
            p = kwargs['amount']
            q = kwargs['salt_vs_pepper']
            flipped = np.random.choice([True, False], size=image.shape,
                                       p=[p, 1 - p])
            salted = np.random.choice([True, False], size=image.shape,
                                      p=[q, 1 - q])
            peppered = ~salted
            out[flipped & salted] = 1
            out[flipped & peppered] = 0

        elif mode == 'speckle':
            noise = np.random.normal(kwargs['mean'], kwargs['std'],
                                     image.shape)/np.mean(image)
            out = image + image * noise

        elif mode == 'none':
            out = self.intensity

        self.updateImage(axis=0, image = out)
        self.updateValues(out)


    def changeNormalization(self):

        maximum = np.max(self.intensity)

        # check combo entry
        gamma = float(str(self.colorNormalization.currentText()))

        self.cbar.set_norm(colors.PowerNorm(gamma=gamma))
        self.cbar.set_ticks(np.linspace(0,maximum,4))
        self.img.set_norm(norm=colors.PowerNorm(gamma=gamma))

        self.canvas.draw()


    def saveFigure(self, screen, intensity, mologram, markersize, figureDir):

        fig, ax = plt.subplots(1,2, figsize=(17.4,8))
        #try:
        maximum = np.max(self.intensity)
        gamma = 1/2.

        cax = ax[0].imshow(intensity,
                            cmap = plt.cm.inferno,
                            origin = 'upper',
                            norm = colors.PowerNorm(gamma=gamma))
        ax[0].axis('off')

        divider = make_axes_locatable(ax[0])
        cax1 = divider.append_axes("right", size="5%", pad=0.1)
        cbar = plt.colorbar(cax,
                            cax = cax1,
                            format='%.0e')
        cbar.ax.tick_params(labelsize=10)

        cbar.set_norm(colors.PowerNorm(gamma=gamma))
        cbar.set_ticks(np.linspace(0,maximum,4))
        cax.set_norm(norm=colors.PowerNorm(gamma=gamma))
        cax.set_clim(0,maximum)

        # mologram
        x_sca, y_sca, z_sca = mologram.dipolePositions

        # only for TE!
        # # colored scatterers
        # self.ax.scatter(x_sca*1e6, y_sca*1e6, c=mologram.bind_no_bind, cmap = 'hot')

        ax[1].set_xlim(-mologram.radius*1e6, mologram.radius*1e6)
        ax[1].set_ylim(-mologram.radius*1e6, mologram.radius*1e6)
        ax[1].set_xticks(np.linspace(-mologram.radius*1e6, mologram.radius*1e6,9))
        ax[1].set_yticks(np.linspace(-mologram.radius*1e6, mologram.radius*1e6,9))

        # marker size in the right size (given in meters)
        markerSizeFigure = linewidthFromDataUnits(markersize*1e6, ax[1])

        # ridges
        ax[1].scatter(x_sca[mologram.boundParticles]*1e6, y_sca[mologram.boundParticles]*1e6, s = markerSizeFigure, color='green')
        # grooves
        ax[1].scatter(x_sca[mologram.boundParticles == False]*1e6, y_sca[mologram.boundParticles == False]*1e6, s = markerSizeFigure, color='red')

        # label number of particles
        ax[1].text(mologram.radius*1e6-70, -mologram.radius*1e6+30, 'Ridges:')
        ax[1].text(mologram.radius*1e6-25, -mologram.radius*1e6+30, '{}'.format(np.sum(mologram.boundParticles)))
        ax[1].text(mologram.radius*1e6-70, -mologram.radius*1e6+20, 'Grooves:')
        ax[1].text(mologram.radius*1e6-25, -mologram.radius*1e6+20, '{}'.format(np.sum(mologram.boundParticles == False)))
        ax[1].text(mologram.radius*1e6-70, -mologram.radius*1e6+10, 'Total:')
        ax[1].text(mologram.radius*1e6-25, -mologram.radius*1e6+10, '{}'.format(len(y_sca)))

        plt.tight_layout()

        # save figure
        if not os.path.exists(figureDir):
            os.makedirs(figureDir)

        # find counter
        if 'quicksaveNo' not in dir(self):
            # make numbering
            files = os.listdir(figureDir)
            self.quicksaveNo = 0
            for file in files:
                try:
                    self.quicksaveNo = np.max((int(file.split('_')[-1].replace('.png','')), self.quicksaveNo))
                except:
                    pass

        self.quicksaveNo += 1
        fig.savefig('{}/simulation_{}.png'.format(figureDir,self.quicksaveNo), bbox_inches='tight')

        self.main_widget.updateStatus("figure saved")
        #except:

         #   self.main_widget.updateStatus("figure could not be saved")



def linewidthFromDataUnits(linewidth, axis, reference='y'):
    """
    Convert a linewidth in data units to linewidth in points.

    :param linewidth: Linewidth in data units of the respective reference-axis
    :type linewidth: float
    :param axis: The axis which is used to extract the relevant transformation data (data limits and size must not change afterwards)
    :type axis: matplotlib axis
    :param reference: The axis that is taken as a reference for the data width. Possible values: 'x' and 'y'. Defaults to 'y'.
    :type reference: string
    :returns: Linewidth in points
    :rtype: float

    """
    fig = axis.get_figure()
    if reference == 'x':
        length = fig.bbox_inches.width * axis.get_position().width
        value_range = np.diff(axis.get_xlim())
    elif reference == 'y':
        length = fig.bbox_inches.height * axis.get_position().height
        value_range = np.diff(axis.get_ylim())

    # Convert length to points
    length *= 72.
    # Scale linewidth to value range
    return linewidth * (length / value_range)


class NavigationToolbar(NavigationToolbar2QT):
    # only display the buttons we need
    # run simulation action

    toolitems = [t for t in NavigationToolbar2QT.toolitems if t[0] in ('Home', 'Pan', 'Zoom', 'Save')]

    # def mouse_move(self, event):
    #     pass
