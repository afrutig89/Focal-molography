import numpy as np
import matplotlib.pylab as plt
import scipy as sci
import pandas as pd
from PyQt4 import QtCore, QtGui
from auxiliaries.writeLogFile import loadDB
from materials.getData import get_data


class particleParameterWidget:
    """Base class for the parameters of a binder object that binds to a mologram."""

    def __init__(self,main_widget,default_values):

        self.main_widget = main_widget

        if not hasattr(self,'name'):
            self.name = 'particle'
        if not hasattr(self,'Label'):
            self.Label = 'Particle'

        self.Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(default_values)


    def setupUi(self,default_values):

        self.particleParameters = QtGui.QGroupBox(self.Form)
        self.particleParameters.setTitle(self.Label + ' Parameter')

        particle_params = QtGui.QFormLayout()

        self.material_lbl = QtGui.QLabel(self.Label + ' Material')

        self.particleMaterials = QtGui.QComboBox()

        self.default_particle = default_values[self.name + '_name']

        # load the available particle materials from the database. There must always be at least one Particle material in the database. 
    
        self.particles = loadDB('materials/materials.db', 'Particles')
        self.currentParticle = self.particles[0]
        i = 0
        for particle in self.particles:
            if particle['name'] == self.default_particle:
                self.currentParticle = particle
                currentIndex = i
            self.particleMaterials.addItem("")
            self.particleMaterials.setItemText(i,particle['name'])
            i += 1
        self.particleMaterials.setCurrentIndex(currentIndex)
        self.currentParticle['refractiveIndex'] = get_data('materials' + self.currentParticle['file'], default_values['wavelength']*1e6)[0]


        self.buttonMaterials = QtGui.QPushButton('...')
        self.buttonMaterials.clicked.connect(self.main_widget.openMaterialsWindow)

        self.particleMaterials.currentIndexChanged.connect(self.selectionChange)

        hbox1 = QtGui.QHBoxLayout()
        hbox1.addWidget(self.particleMaterials)
        hbox1.addWidget(self.buttonMaterials)
        particle_params.addRow(self.material_lbl,hbox1)

        self.particleRadius_lbl = QtGui.QLabel(self.Label + ' Radius [nm]')
        self.particleRadius = QtGui.QLineEdit('0')

        if self.currentParticle['radius'] != '':
            self.particleRadius.setText(str(self.currentParticle['radius']*1e9))
            self.particleRadius.setEnabled(False)
        else:
            self.particleRadius.setText(str(default_values[self.name + '_Radius']*1e9))

        particle_params.addRow(self.particleRadius_lbl,self.particleRadius)
        
        # number of coherent and incoherent binders. The noncoherent binders exhibit shot noise for ridges and grooves. 
        self.radioButton_1 = QtGui.QRadioButton()
        self.radioButton_1.setChecked(True)
        self.radioButton_1.toggled.connect(self.onChange)
        self.coherentBinding_lbl = QtGui.QLabel('# Coh/Non-coh')
        self.coherentParticles = QtGui.QLineEdit('0')
        self.noncoherentParticles = QtGui.QLineEdit()

        hbox2 = QtGui.QHBoxLayout()
        hbox2.addWidget(self.radioButton_1)
        hbox2.addWidget(self.coherentParticles)
        hbox2.addWidget(self.noncoherentParticles)


        particle_params.addRow(self.coherentBinding_lbl,hbox2)      


        # fixed total number of analyte molecules on surface, whereas a certain fraction is coherently arranged. 
        # self.radioButton_2 = QtGui.QRadioButton()
        # self.radioButton_2.toggled.connect(self.onChange)
        # self.numberOfParticles_lbl = QtGui.QLabel('# tot/ Coh Bind Probability [%]:')
        # self.numberOfParticles = QtGui.QLineEdit('0')
        # self.numberOfParticles.setEnabled(False)
        # self.coherentBinding = QtGui.QLineEdit('0')
        # self.coherentBinding.setEnabled(False)

        # hbox2 = QtGui.QHBoxLayout()
        # hbox2.addWidget(self.radioButton_2)
        # hbox2.addWidget(self.numberOfParticles)
        # hbox2.addWidget(self.coherentBinding)

        # particle_params.addRow(self.numberOfParticles_lbl,hbox2)


        # # fixed total amount of analyte per square mm the probability states how much of it is coherently arranged. 
        # self.radioButton_3 = QtGui.QRadioButton()
        # self.radioButton_3.toggled.connect(self.onChange)
        # self.particleDensity_lbl = QtGui.QLabel('# tot/mm2 / p_coh [%]:')
        # self.particleDensity = QtGui.QLineEdit()
        # self.particleDensity.setEnabled(False)
        # self.cohBindProbDensity = QtGui.QLineEdit()
        # self.cohBindProbDensity.setEnabled(False)

        # hbox2 = QtGui.QHBoxLayout()
        # hbox2.addWidget(self.radioButton_3)
        # hbox2.addWidget(self.particleDensity)
        # hbox2.addWidget(self.cohBindProbDensity)

        # particle_params.addRow(self.particleDensity_lbl,hbox2)

        # Fourth radiobutton for K_D and concentration. 
        self.radioButton_4 = QtGui.QRadioButton()
        self.radioButton_4.toggled.connect(self.onChange)
        self.K_D_lbl = QtGui.QLabel('K_D [M] / conc [M]:')
        self.K_D = QtGui.QLineEdit('0')
        self.K_D.setEnabled(False)
        self.conc = QtGui.QLineEdit('0')
        self.conc.setEnabled(False)
        self.RecDen_lbl = QtGui.QLabel('RecDensity Rid / Grov [sites/m^2]:')
        self.RecDenRid = QtGui.QLineEdit('0')
        self.RecDenRid.setEnabled(False)
        self.RecDenGrov = QtGui.QLineEdit('0')
        self.RecDenGrov.setEnabled(False)

        hbox2 = QtGui.QHBoxLayout()
        hbox2.addWidget(self.radioButton_4)
        hbox2.addWidget(self.K_D)
        hbox2.addWidget(self.conc)

        particle_params.addRow(self.K_D_lbl,hbox2)

        hbox2 = QtGui.QHBoxLayout()
        hbox2.addWidget(self.RecDenRid)
        hbox2.addWidget(self.RecDenGrov)

        particle_params.addRow(self.RecDen_lbl,hbox2)

        self.placement_lbl = QtGui.QLabel('Placement')
        self.placement = QtGui.QComboBox()
        self.placement.addItem("")
        self.placement.addItem("")
        self.placement.addItem("")

        particle_params.addRow(self.placement_lbl,self.placement)

        self.z_min_lbl = QtGui.QLabel('Minimum Vertical [nm]')
        self.z_min = QtGui.QLineEdit('0')

        particle_params.addRow(self.z_min_lbl,self.z_min)


        self.z_max_lbl = QtGui.QLabel('Maximum Vertical [nm]')
        self.z_max = QtGui.QLineEdit('0')

        particle_params.addRow(self.z_max_lbl,self.z_max)

        self.seedBragg_lbl = QtGui.QLabel('Seed Bragg area')
        self.seedBragg = QtGui.QCheckBox()
        self.seedBragg.setChecked(False)

        self.MSN_lbl = QtGui.QLabel('Molecular shot noise')
        self.MSN = QtGui.QCheckBox() # Molecular shot noise
        self.MSN.setChecked(False)  

        particle_params.addRow(self.seedBragg_lbl,self.seedBragg)
        particle_params.addRow(self.MSN_lbl,self.MSN)

        self.particleRefIndex_lbl = QtGui.QLabel(self.Label + ' Refractive Index')
        self.particleRefIndex = QtGui.QLabel()

        particle_params.addRow(self.particleRefIndex_lbl,self.particleRefIndex)

        self.particleParameters.setLayout(particle_params)

        return self.particleParameters

    def retranslateUi(self,values):

        # N_Particles = values[self.name + '_N']
        # coherentBinding = values[self.name + '_coherentBinding']
        coherentParticles = values[self.name + '_Ncoh']
        noncoherentParticles = values[self.name + '_Nincoh']

        self.coherentParticles.setText(str(coherentParticles))
        self.noncoherentParticles.setText(str(noncoherentParticles))
        self.K_D.setText(str(values[self.name + '_K_D']))
        self.conc.setText(str(values[self.name + '_c']))
        self.RecDenRid.setText(str(values[self.name + '_RecDenRid']))
        self.RecDenGrov.setText(str(values[self.name + '_RecDenGrov']))
        # self.numberOfParticles.setText(str(N_Particles))
        # self.coherentBinding.setText(str(coherentBinding*1e2))


        self.placement.setItemText(0, "Ridges (Rectangular)")
        self.placement.setItemText(2, "Ridges (Sinusoidal)")
        self.placement.setItemText(1, "Mololines")
        placementIndex = self.placement.findText(values[self.name + '_placement'])
        self.placement.setCurrentIndex(placementIndex)
        self.z_min.setText(str(values[self.name + '_z_min']*1e9))
        self.z_max.setText(str(values[self.name + '_z_max']*1e9))

        n_p = self.currentParticle['refractiveIndex']

        self.particleRefIndex.setText('({0:.2f} {1} {2:.2f}i)'.format(n_p.real, '+-'[n_p.imag < 0], abs(n_p.imag)))
        
    def selectionChange(self, i):
        
        self.currentParticle = self.particles[i]
        
        if self.currentParticle['radius'] != '':
            self.particleRadius.setText(str(self.currentParticle['radius']*1e9))
            self.particleRadius.setEnabled(False)
        else:
            self.particleRadius.setEnabled(True)

        self.wavelength = float(self.main_widget.waveguideTab.wavelength.text())
        particleRefIndex = get_data('materials' + self.currentParticle['file'], self.wavelength*1e-3)[0]
        self.particleRefIndex.setText('({0:.2f} {1} {2:.2f}i)'.format(particleRefIndex.real, '+-'[particleRefIndex.imag < 0], abs(particleRefIndex.imag)))
        self.currentParticle['refractiveIndex'] = particleRefIndex

    def getValues(self):
        """returns a dictionary of the particle parameters."""

        if self.radioButton_1.isChecked():
            coherentParticles = int(self.coherentParticles.text())
            noncoherentParticles = int(self.noncoherentParticles.text())
            # N_Particles = coherentParticles + noncoherentParticles
            # self.numberOfParticles.setText(str(N_Particles))
            # coherentBinding = 1/2.*(1+float(coherentParticles)/(coherentParticles+noncoherentParticles))
            # self.coherentBinding.setText("%.1f" % (coherentBinding*100))

        # elif self.radioButton_2.isChecked():
        #     N_Particles = int(self.numberOfParticles.text())
        #     coherentBinding = float(self.coherentBinding.text())*1e-2
        #     coherentParticles = int((2*coherentBinding-1)*N_Particles)
        #     noncoherentParticles = N_Particles-coherentParticles

        # elif self.radioButton_3.isChecked():
        #     area = np.pi*(float(self.diameter.text())*1e-3/2.)**2 # area, does not work for rectangular molograms yet. 
        #     N_Particles = int(area*float(self.particleDensity.text()))
        #     coherentBinding = float(self.cohBindProbDensity.text())*1e-2
        #     coherentParticles = int((2*coherentBinding-1)*N_Particles)
        #     noncoherentParticles = N_Particles-coherentParticles

        particlePar = {
                self.name + '_name' : str(self.currentParticle['name']),
                self.name + '_Radius' : float(self.particleRadius.text())*1e-9,
                self.name + '_RefractiveIndex' : complex(self.currentParticle['refractiveIndex']),
                self.name + '_Ncoh' : coherentParticles,
                self.name + '_Nincoh': noncoherentParticles, 
                self.name + '_z_min' : float(self.z_min.text())*1e-9,
                self.name + '_z_max' : float(self.z_max.text())*1e-9,
                self.name + '_placement' : str(self.placement.currentText()),
                self.name + '_seed' : self.seedBragg.isChecked(),
                self.name + '_K_D' : float(str(self.K_D.text())),
                self.name + '_c' : float(str(self.conc.text())),
                self.name + '_RecDenRid': float(str(self.RecDenRid.text())),
                self.name + '_RecDenGrov': float(str(self.RecDenGrov.text()))
            }

        return particlePar

    def setValues(self,values):

        self.currentParticle['name'] = str(values[self.name + '_name'])
        self.particleRadius.setText(str(values[self.name + '_Radius']*1e9))
        self.z_min.setText(str(values[self.name + '_z_min']*1e9))
        self.z_max.setText(str(values[self.name + '_z_max']*1e9))
        self.placement.setCurrentIndex(self.placement.findText(str(values[self.name + '_placement'])))
        self.seedBragg.setChecked(values[self.name + '_seed'])


    def updateParameters(self):

        radius = self.particleRadius.text()
        self.particleMaterials.clear()

        self.particles = loadDB('materials/materials.db', 'Particles')
        self.currentParticle = self.particles[0]
        i = 0
        for particle in self.particles:
            if particle['name'] == self.default_particle:
                self.currentParticle = particle
                currentIndex = i
            self.particleMaterials.addItem("")
            self.particleMaterials.setItemText(i, particle['name'])
            i += 1

        self.particleMaterials.setCurrentIndex(currentIndex)
        self.particleRadius.setText(radius) # reload input radius (since selection changed)


    def onChange(self):

        self.coherentParticles.setEnabled(self.radioButton_1.isChecked())
        self.noncoherentParticles.setEnabled(self.radioButton_1.isChecked())
        # self.numberOfParticles.setEnabled(self.radioButton_2.isChecked())
        # self.coherentBinding.setEnabled(self.radioButton_2.isChecked())
        # self.particleDensity.setEnabled(self.radioButton_3.isChecked())
        # self.cohBindProbDensity.setEnabled(self.radioButton_3.isChecked())
        self.K_D.setEnabled(self.radioButton_4.isChecked())
        self.conc.setEnabled(self.radioButton_4.isChecked())
        self.RecDenRid.setEnabled(self.radioButton_4.isChecked())
        self.RecDenGrov.setEnabled(self.radioButton_4.isChecked())


class analyteParameterWidget(particleParameterWidget):

    def __init__(self,main_widget,default_values):

        self.name = 'analyte'
        self.Label = 'Analyte'

        particleParameterWidget.__init__(self,main_widget,default_values)


class NSBParameterWidget(particleParameterWidget):

    def __init__(self,main_widget,default_values):

        self.name = 'NSB'
        self.Label = 'NSB'

        particleParameterWidget.__init__(self,main_widget,default_values)


        


if __name__ == '__main__':
    main()
