# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SpecklesWidget.ui'
#
# Created: Mon Dec  5 17:15:41 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
import sys
sys.path.append('../')
from imageProcessing.speckles import findSpeckles


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class SpecklesWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget, values):

        # Call parent class constructor
        super(SpecklesWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(Form, values)

    def setupUi(self, Form, values):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(463, 307)
        self.speckleParams = QtGui.QGroupBox(Form)
        self.speckleParams.setGeometry(QtCore.QRect(30, 30, 441, 211))
        self.speckleParams.setMinimumSize(QtCore.QSize(441, 151))
        self.speckleParams.setObjectName(_fromUtf8("speckleParams"))
        self.label_neighborhoodSize = QtGui.QLabel(self.speckleParams)
        self.label_neighborhoodSize.setGeometry(QtCore.QRect(25, 30, 101, 17))
        self.label_neighborhoodSize.setObjectName(_fromUtf8("label_neighborhoodSize"))
        self.neighborhoodSize = QtGui.QLineEdit(self.speckleParams)
        self.neighborhoodSize.setGeometry(QtCore.QRect(190, 25, 181, 27))
        self.neighborhoodSize.setObjectName(_fromUtf8("neighborhoodSize"))
        self.label_threshold = QtGui.QLabel(self.speckleParams)
        self.label_threshold.setGeometry(QtCore.QRect(25, 65, 151, 17))
        self.label_threshold.setObjectName(_fromUtf8("label_threshold"))
        self.threshold = QtGui.QLineEdit(self.speckleParams)
        self.threshold.setGeometry(QtCore.QRect(190, 65, 181, 27))
        self.threshold.setObjectName(_fromUtf8("threshold"))

        self.calculateButton = QtGui.QPushButton(self.speckleParams)
        self.calculateButton.setGeometry(QtCore.QRect(20, 100, 401, 27))
        self.calculateButton.setObjectName(_fromUtf8("calculateButton"))
        self.calculateButton.setStatusTip('Analyze Speckles')
        self.calculateButton.clicked.connect(self.analyzeSpeckles)
        self.calculateButton.setToolTip('<font color=black>Analyze Speckles</font>')

        self.relatedParameters = QtGui.QGroupBox(Form)
        self.relatedParameters.setGeometry(QtCore.QRect(35, 550, 956, 141))
        self.relatedParameters.setMinimumSize(QtCore.QSize(350, 166))
        self.relatedParameters.setObjectName(_fromUtf8("screen_parameters"))
        self.label_numberOfSpeckles = QtGui.QLabel(self.relatedParameters)
        self.label_numberOfSpeckles.setGeometry(QtCore.QRect(25, 30, 186, 17))
        self.label_numberOfSpeckles.setObjectName(_fromUtf8("label_numberOfSpeckles"))
        self.numberOfSpeckles = QtGui.QLabel(self.relatedParameters)
        self.numberOfSpeckles.setGeometry(QtCore.QRect(190, 30, 186, 17))
        self.numberOfSpeckles.setObjectName(_fromUtf8("numberOfSpeckles"))
        self.label_speckleDensity = QtGui.QLabel(self.relatedParameters)
        self.label_speckleDensity.setGeometry(QtCore.QRect(25, 65, 186, 17))
        self.label_speckleDensity.setObjectName(_fromUtf8("label_speckleDensity"))
        self.speckleDensity = QtGui.QLabel(self.relatedParameters)
        self.speckleDensity.setGeometry(QtCore.QRect(190, 65, 186, 17))
        self.speckleDensity.setObjectName(_fromUtf8("speckleDensity"))
        self.speckleDensity_units = QtGui.QLabel(self.relatedParameters)
        self.speckleDensity_units.setGeometry(QtCore.QRect(385, 65, 186, 17))
        self.speckleDensity_units.setObjectName(_fromUtf8("speckleDensity_units"))
        self.label_averageSize = QtGui.QLabel(self.relatedParameters)
        self.label_averageSize.setGeometry(QtCore.QRect(25, 100, 186, 17))
        self.label_averageSize.setObjectName(_fromUtf8("label_averageSize"))
        self.averageSize = QtGui.QLabel(self.relatedParameters)
        self.averageSize.setGeometry(QtCore.QRect(190, 100, 186, 17))
        self.averageSize.setObjectName(_fromUtf8("averageSize"))
        self.averageSize_units = QtGui.QLabel(self.relatedParameters)
        self.averageSize_units.setGeometry(QtCore.QRect(385, 100, 186, 17))
        self.averageSize_units.setObjectName(_fromUtf8("averageSize_units"))
        self.label_averageHeight = QtGui.QLabel(self.relatedParameters)
        self.label_averageHeight.setGeometry(QtCore.QRect(25, 135, 186, 17))
        self.label_averageHeight.setObjectName(_fromUtf8("label_averageHeight"))
        self.averageHeight = QtGui.QLabel(self.relatedParameters)
        self.averageHeight.setGeometry(QtCore.QRect(190, 135, 186, 17))
        self.averageHeight.setObjectName(_fromUtf8("averageHeight"))
        self.averageHeight_units = QtGui.QLabel(self.relatedParameters)
        self.averageHeight_units.setGeometry(QtCore.QRect(385, 135, 186, 17))
        self.averageHeight_units.setObjectName(_fromUtf8("averageHeight_units"))

        self.retranslateUi(Form, values)
        QtCore.QMetaObject.connectSlotsByName(Form)

        hbox = QtGui.QHBoxLayout()
        self.setLayout(hbox)

        # box left top
        vbox = QtGui.QVBoxLayout()

        vbox.addWidget(self.speckleParams)
        vbox.addWidget(self.relatedParameters)
        vbox.addStretch()

        hbox.addLayout(vbox)

    def retranslateUi(self, Form, values):
        Form.setWindowTitle(_translate("Form", "Form", None))
        self.speckleParams.setTitle(_translate("Form", "Parameters", None))
        self.label_neighborhoodSize.setText(_translate("Form", "Size", None))
        self.neighborhoodSize.setText(_translate("Form", "16", None))
        self.label_threshold.setText(_translate("Form", "Threshold", None))
        self.threshold.setText(_translate("Form", "0", None))
        self.relatedParameters.setTitle(_translate("Form", "Related Parameters", None))
        self.label_numberOfSpeckles.setText(_translate("Form", "Number of Speckles", None))
        self.label_speckleDensity.setText(_translate("Form", "Speckle Density", None))
        self.label_averageSize.setText(_translate("Form", "Average Size", None))
        self.label_averageHeight.setText(_translate("Form", "Average Height", None))
        self.calculateButton.setText(_translate("Form", "Analyze", None))

    def get_values(self):
        return None

    def analyzeSpeckles(self):
        neighborhoodSize = int(self.neighborhoodSize.text())
        threshold = str(self.threshold.text())
        numberOfSpeckles, speckleDensity, avgSpeckleDimension, avgSpeckleSize = self.main_widget.plotWidget.plotSpeckles(neighborhoodSize, threshold)
        self.numberOfSpeckles.setText(str(numberOfSpeckles))
        self.speckleDensity.setText(str(speckleDensity))
        self.speckleDensity_units.setText(_translate("Form", "1/μ²", None))
        self.averageSize.setText(str(avgSpeckleDimension))
        self.averageSize_units.setText(_translate("Form", "μ²", None))
        self.averageHeight.setText(str(avgSpeckleSize))
        self.averageHeight_units.setText(_translate("Form", "mW/m²", None))

    