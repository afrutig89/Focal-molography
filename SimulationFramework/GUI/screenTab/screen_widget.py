# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'screenWidget.ui'
#
# Created: Mon Dec  5 17:15:41 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
from camerasimulation.CameraSensorModels import ReturnCameraSensorNames, ReturnCameraSensorParameters

from camerasimulation.CameraSensorSimulation import CameraSensorSimulation

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class ScreenWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget, values):

        # Call parent class constructorSc
        super(ScreenWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(Form, values)

    def setupUi(self, Form, values):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(463, 307)
        self.screen_params = QtGui.QGroupBox(Form)
        self.screen_params.setGeometry(QtCore.QRect(30, 30, 441, 211))
        self.screen_params.setMinimumSize(QtCore.QSize(441, 211))
        self.screen_params.setObjectName(_fromUtf8("screen_params"))
        self.label_screenWidth = QtGui.QLabel(self.screen_params)
        self.label_screenWidth.setGeometry(QtCore.QRect(25, 30, 101, 17))
        self.label_screenWidth.setObjectName(_fromUtf8("label_screenWidth"))
        self.label_screenPlane = QtGui.QLabel(self.screen_params)
        self.label_screenPlane.setGeometry(QtCore.QRect(25, 65, 151, 17))
        self.label_screenPlane.setObjectName(_fromUtf8("label_screenPlane"))
        self.label_screenWidth_unit = QtGui.QLabel(self.screen_params)
        self.label_screenWidth_unit.setGeometry(QtCore.QRect(385, 30, 66, 17))
        self.label_screenWidth_unit.setObjectName(_fromUtf8("label_screenWidth_unit"))
        self.screenPlane = QtGui.QComboBox(self.screen_params)
        self.screenPlane.setGeometry(QtCore.QRect(190, 60, 181, 27))
        self.screenPlane.setObjectName(_fromUtf8("screenPlane"))
        self.screenPlane.addItem(_fromUtf8(""))
        self.screenPlane.addItem(_fromUtf8(""))
        self.screenPlane.addItem(_fromUtf8(""))
        self.label_screenAxisRatio = QtGui.QLabel(self.screen_params)
        self.label_screenAxisRatio.setGeometry(QtCore.QRect(25, 100, 151, 17))
        self.label_screenAxisRatio.setObjectName(_fromUtf8("label_screenAxisRatio"))
        self.screenWidth = QtGui.QLineEdit(self.screen_params)
        self.screenWidth.setGeometry(QtCore.QRect(190, 25, 181, 27))
        self.screenWidth.setObjectName(_fromUtf8("screenWidth"))
        self.screenWidth.returnPressed.connect(self.update_values)
        self.axisRatio1 = QtGui.QLineEdit(self.screen_params)
        self.axisRatio1.setGeometry(QtCore.QRect(190, 95, 81, 27))
        self.axisRatio1.setObjectName(_fromUtf8("axisRatio1"))
        self.axisRatio2 = QtGui.QLineEdit(self.screen_params)
        self.axisRatio2.setGeometry(QtCore.QRect(290, 95, 81, 27))
        self.axisRatio2.setObjectName(_fromUtf8("axisRatio2"))
        self.label_slash1 = QtGui.QLabel(self.screen_params)
        self.label_slash1.setGeometry(QtCore.QRect(278, 100, 16, 17))
        self.label_slash1.setObjectName(_fromUtf8("label_slash1"))
        self.label_center = QtGui.QLabel(self.screen_params)
        self.label_center.setGeometry(QtCore.QRect(25, 135, 151, 17))
        self.label_center.setObjectName(_fromUtf8("label_center"))
        self.label_center_2 = QtGui.QLabel(self.screen_params)
        self.label_center_2.setGeometry(QtCore.QRect(25, 170, 151, 17))
        self.label_center_2.setObjectName(_fromUtf8("label_center_2"))
        self.center_x = QtGui.QLineEdit(self.screen_params)
        self.center_x.setGeometry(QtCore.QRect(190, 130, 81, 27))
        self.center_x.setObjectName(_fromUtf8("center_x"))
        self.label_slash2 = QtGui.QLabel(self.screen_params)
        self.label_slash2.setGeometry(QtCore.QRect(278, 135, 16, 17))
        self.label_slash2.setObjectName(_fromUtf8("label_slash2"))
        self.center_y = QtGui.QLineEdit(self.screen_params)
        self.center_y.setGeometry(QtCore.QRect(290, 130, 81, 27))
        self.center_y.setObjectName(_fromUtf8("center_y"))
        self.label_center_unit = QtGui.QLabel(self.screen_params)
        self.label_center_unit.setGeometry(QtCore.QRect(385, 135, 66, 17))
        self.label_center_unit.setObjectName(_fromUtf8("label_center_unit"))
        self.npix = QtGui.QLineEdit(self.screen_params)
        self.npix.setGeometry(QtCore.QRect(190, 170, 181, 27))
        self.npix.setObjectName(_fromUtf8("npix"))
        self.npix.returnPressed.connect(self.update_values)

        self.relatedParameters = QtGui.QGroupBox(Form)
        self.relatedParameters.setGeometry(QtCore.QRect(35, 550, 956, 141))
        self.relatedParameters.setMinimumSize(QtCore.QSize(350, 146))
        self.relatedParameters.setObjectName(_fromUtf8("screen_parameters"))
        self.label_resolution = QtGui.QLabel(self.relatedParameters)
        self.label_resolution.setGeometry(QtCore.QRect(25, 30, 186, 17))
        self.label_resolution.setObjectName(_fromUtf8("label_resolution"))
        self.resolution = QtGui.QLabel(self.relatedParameters)
        self.resolution.setGeometry(QtCore.QRect(190, 30, 186, 17))
        self.resolution.setObjectName(_fromUtf8("resolution"))


        # set up the camera parameters
        self.camera_params = QtGui.QGroupBox(Form)
        self.camera_params.setTitle('Camera Parameters')

        self.cameras = QtGui.QComboBox()

        cameramodels = ReturnCameraSensorNames()

        for i,j in enumerate(cameramodels):
            self.cameras.addItem(j)

        self.cameras.currentIndexChanged.connect(self.update_values)

        self.cameras.setCurrentIndex(0)

        camparams = QtGui.QFormLayout()

        self.camRes_lbl = QtGui.QLabel('Resolution:')
        self.camRes = QtGui.QLabel()
        self.camFR_lbl = QtGui.QLabel('FRS:')
        self.camFR = QtGui.QLabel()
        self.campixelsize_lbl = QtGui.QLabel('Pixel size [um]:')
        self.campixelsize = QtGui.QLabel()
        self.QE_lbl = QtGui.QLabel('Quantum Efficiency [%]')
        self.QE = QtGui.QLabel()
        self.camrNoise_lbl = QtGui.QLabel('Readout Noise (e-):')
        self.camrNoise = QtGui.QLabel()
        self.camSatCap_lbl = QtGui.QLabel('Saturation capacity (e-):')
        self.camSatCap = QtGui.QLabel()
        self.camGain_lbl = QtGui.QLabel('Camera Gain (e-)/ADU')
        self.camGain = QtGui.QLabel()

        self.usecam_params_lbl = QtGui.QLabel('Use camera pixel size in Sim:')
        self.usecam_params = QtGui.QCheckBox()
        self.usecam_params.stateChanged.connect(self.update_values)

        self.simCamAquisiton_btn = QtGui.QPushButton()
        self.simCamAquisiton_btn.setText('Sim Cam Aquisition')
        self.simCamAquisiton_btn.clicked.connect(self.SimulateAquisition)

        self.simMultipleCamAquisiton_btn = QtGui.QPushButton()
        self.simMultipleCamAquisiton_btn.setText('Sim Cam Aquisition Multiple')
        self.simMultipleCamAquisiton_btn.clicked.connect(self.SimulateMultipleAquisitions)
        #self.usecam_params.setCheckState(True) 


        camparams.addRow(self.camRes_lbl,self.camRes)
        camparams.addRow(self.camFR_lbl,self.camFR)   
        camparams.addRow(self.campixelsize_lbl,self.campixelsize)
        camparams.addRow(self.QE_lbl,self.QE)
        camparams.addRow(self.camrNoise_lbl,self.camrNoise)
        camparams.addRow(self.camSatCap_lbl,self.camSatCap)
        camparams.addRow(self.usecam_params_lbl,self.usecam_params)
        camparams.addRow(self.camGain_lbl,self.camGain)
        
        vbox1 = QtGui.QVBoxLayout()
        vbox1.addWidget(self.cameras)
        vbox1.addLayout(camparams)

        self.camera_params.setLayout(vbox1)


        # Imaging system parameters
        self.img_sys_params = QtGui.QGroupBox(Form)
        self.img_sys_params.setTitle('Imaging System Parameters')
        img_sys = QtGui.QFormLayout()

        self.magnification_lbl = QtGui.QLabel('Magnification:')
        self.magnification = QtGui.QLineEdit('20')
        self.magnification.returnPressed.connect(self.update_values)

        self.exposureTime_lbl = QtGui.QLabel('Exposure Time [ms]:')
        self.exposureTime = QtGui.QLineEdit('1200')
        self.exposureTime.returnPressed.connect(self.update_values)

        img_sys.addRow(self.magnification_lbl,self.magnification)
        img_sys.addRow(self.exposureTime_lbl,self.exposureTime)
        img_sys.addRow(self.simCamAquisiton_btn)
        img_sys.addRow(self.simMultipleCamAquisiton_btn)

        vbox2 = QtGui.QVBoxLayout()
        vbox2.addLayout(img_sys)
        self.img_sys_params.setLayout(vbox2)

        self.retranslateUi(Form, values)
        QtCore.QMetaObject.connectSlotsByName(Form)

        hbox = QtGui.QHBoxLayout()
        self.setLayout(hbox)


        # Camera simulation control
        self.camera_sim = QtGui.QGroupBox(Form)
        self.camera_sim.setTitle('Camera Simulation')

        # Requirements:
        # get simulated image from the database (Load widget)
        # simulate the acquisition of this image with the camera, do all the mologram processing.
        # store the result to a new table in the database, called simulated image captures
        # allow the same image to be captured multiple times in order to simulate the noise that is introduced by the camera.






        # box left top
        vbox = QtGui.QVBoxLayout()

        vbox.addWidget(self.screen_params)
        vbox.addWidget(self.relatedParameters)
        vbox.addWidget(self.camera_params)
        vbox.addWidget(self.img_sys_params)
        vbox.addStretch()

        hbox.addLayout(vbox)
        # hbox.addLayout(box)

    def retranslateUi(self, Form, values):
        Form.setWindowTitle(_translate("Form", "Form", None))
        self.screen_params.setTitle(_translate("Form", "Screen", None))
        self.label_screenWidth.setText(_translate("Form", "Width", None))
        self.label_screenPlane.setText(_translate("Form", "Plane", None))
        self.label_screenWidth_unit.setText(_translate("Form", "um", None))
        self.screenPlane.setItemText(0, _translate("Form", "xy", None))
        self.screenPlane.setItemText(1, _translate("Form", "xz", None))
        self.screenPlane.setItemText(2, _translate("Form", "yz", None))
        screenPlaneIndex = self.screenPlane.findText(values['screenPlane'])
        self.screenPlane.setCurrentIndex(screenPlaneIndex)
        self.label_screenAxisRatio.setText(_translate("Form", "Axis Ratio", None))
        self.screenWidth.setText(_translate("Form", str(values['screenWidth']*1e6), None))
        self.axisRatio1.setText(_translate("Form", str(values['screenRatio']), None))
        self.axisRatio2.setText(_translate("Form", "1", None))
        self.label_slash1.setText(_translate("Form", "/", None))
        self.label_center.setText(_translate("Form", "Center (x/y)", None))
        self.label_center_2.setText(_translate("Form", "Pixel per Axis", None))
        self.label_center_unit.setText(_translate("Form", "um", None))
        self.center_x.setText(_translate("Form", str(values['center'][0]), None))
        self.label_slash2.setText(_translate("Form", "/", None))
        self.center_y.setText(_translate("Form", str(values['center'][1]), None))
        self.npix.setText(_translate("Form", str(values['npix']), None))

        self.relatedParameters.setTitle(_translate("Form", "Related Parameters", None))
        self.label_resolution.setText(_translate("Form", "Resolution", None))
        resolution = float(self.screenWidth.text())*1e-6/float(self.npix.text())
        self.resolution.setText(str(resolution*1e9) + ' nm')


    def get_values(self):

        try:
            screenPar = {
                'screenWidth' : float(self.screenWidth.text())*1e-6,
                'screenPlane' : str(self.screenPlane.currentText()),
                'screenRatio' : float(self.axisRatio1.text())/float(self.axisRatio2.text()),
                'screenRotation' : np.array([0,0]), # necessary?
                'center' : np.array([float(self.center_x.text())*1e-6, float(self.center_y.text())*1e-6]),
                'npix' : int(float(self.npix.text()))
            }
            return screenPar
        except:
            return None

    def setValues(self,values):
        """loads the values from a dictionary into the UI.

        :param values: dictionary with the key value pairs of the parameters.
        """

        self.screenWidth.setText(str(values['screenWidth']*1e6))
        self.screenPlane.setCurrentIndex(self.screenPlane.findText(str(values['screenPlane'])))
        self.axisRatio1.setText(str(values['screenRatio']))
        self.axisRatio2.setText('1')
        self.center_x.setText(str(values['center'][0]*1e6))
        self.center_y.setText(str(values['center'][1]*1e6))
        self.npix.setText(str(values['npix']))



    def update_values(self):
        
        currCam = ReturnCameraSensorParameters(self.cameras.currentText())

        self.camRes.setText(str(currCam.resolution))
        self.camFR.setText(str(currCam.framerate))
        self.campixelsize.setText(str(currCam.pixelsize*1e6))
        self.QE.setText(str(currCam.QE*1e2))
        self.camrNoise.setText(str(currCam.rNoise))
        self.camSatCap.setText(str(currCam.saturationCap))
        self.camGain.setText(str(currCam.camGain))


        if self.usecam_params.isChecked():
            
            # self.screenWidth.setText(str(float(self.npix.text())*float(self.campixelsize.text())/float(self.magnification.text())))

            self.npix.setText(str(round(float(self.screenWidth.text())*1e-6/(float(self.campixelsize.text())*1e-6)*float(self.magnification.text()))))


            self.resolution.setText(str(float(self.campixelsize.text())*1e-6/float(self.magnification.text())*1e9) + 'nm')

        else:
            resolution = float(self.screenWidth.text())*1e-6/float(self.npix.text())
            self.resolution.setText(str(resolution*1e9) + ' nm')



    def SimulateAquisition(self,loadfromDB=False):
        """Simulates an acquisition of the simulated image, with the specified camera, if loadfromDB is false the image of the last simulation is processed."""

        # load the parameters from the database.
        if loadfromDB:
            pass

        else:
            I_sca = self.main_widget.results['I_sca']
            pixelsizeMoloPlane = self.main_widget.parameters.screen['screenWidth']/self.main_widget.parameters.screen['npix'] # pixelsize in the moloplane.

        self.camSim = CameraSensorSimulation(ReturnCameraSensorParameters(self.cameras.currentText()))

        I_sca_bn = self.camSim.SimulateImageCapture(I_sca,pixelsizeMoloPlane,float(self.magnification.text()),self.main_widget.parameters.waveguide['wavelength'],\
        float(self.exposureTime.text())*1e-3) # scattered intensity in binary format. 


        # update the plotwidet

        self.main_widget.plotWidget.newImage(
                        self.main_widget.results['screen'],
                        I_sca_bn, 
                        self.main_widget.parameters.mologram['focalPoint']
                    )



        # process the data with the molo extraction algorithm. 


    def SimulateMultipleAquisitions(self):

        I_sca = self.main_widget.results['I_sca']
        pixelsizeMoloPlane = self.main_widget.parameters.screen['screenWidth']/self.main_widget.parameters.screen['npix']

        self.camSim = CameraSensorSimulation(ReturnCameraSensorParameters(self.cameras.currentText()))

        I_sca_bn = self.camSim.processSingleImage(I_sca,pixelsizeMoloPlane,float(self.magnification.text()),self.main_widget.parameters.waveguide['wavelength'],\
        float(self.exposureTime.text())*1e-3,self.main_widget.results['AiryDiskRadius'],\
        database=self.main_widget.parameters.settings['databaseFile'],ID_results=1) # scattered intensity in binary format. 




if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

