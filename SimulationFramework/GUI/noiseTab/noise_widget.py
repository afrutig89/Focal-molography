# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'background_widget.ui'
#
# Created: Mon Dec  5 13:14:30 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from waveguide.waveguide import SlabWaveguide
from auxiliaries.writeLogFile import loadDB
from materials.getData import get_data, get_range
import numpy as np

from GUI.particleWidget.particle_widget import NSBParameterWidget


class NoiseWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget, default_values):

        # Call parent class constructor
        super(NoiseWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(Form, default_values)

    def setupUi(self, Form, default_values):

        # Noise source Tab
        self.noiseSourcesGroup = QtGui.QGroupBox(Form)
        self.noiseSourcesGroup.setTitle('Noise sources')

        self.waveguideLosses_cb = QtGui.QCheckBox('Waveguide losses')
        self.nonSpecificBinding_cb = QtGui.QCheckBox('Non-Specific Binding')
        self.laserFluctuations_cb = QtGui.QCheckBox('Laserfluctions [%]')
        self.laserFluctuations = QtGui.QLineEdit()

        fluctuationsHbox = QtGui.QHBoxLayout()
        fluctuationsHbox.addWidget(self.laserFluctuations_cb)
        fluctuationsHbox.addWidget(self.laserFluctuations)

        noiseSourcesBox = QtGui.QVBoxLayout()
        noiseSourcesBox.addWidget(self.waveguideLosses_cb)
        noiseSourcesBox.addWidget(self.nonSpecificBinding_cb)
        noiseSourcesBox.addLayout(fluctuationsHbox)

        self.noiseSourcesGroup.setLayout(noiseSourcesBox)


        # Waveguide losses group (should be moved to waveguide widget...)

        # self.waveguideLossesGroup = QtGui.QGroupBox(Form)
        # self.waveguideLossesGroup.setGeometry(QtCore.QRect(30, 30, 446, 150))
        # self.waveguideLossesGroup.setMinimumSize(QtCore.QSize(400, 150))
        # self.waveguideLossesGroup.setObjectName(_fromUtf8("waveguideLossesGroup"))
        # self.scatteringEqualsPropagationLoss = QtGui.QRadioButton(self.waveguideLossesGroup)
        # self.scatteringEqualsPropagationLoss.setGeometry(QtCore.QRect(25, 30, 401, 22))
        # self.scatteringEqualsPropagationLoss.setChecked(True)
        # self.scatteringEqualsPropagationLoss.setObjectName(_fromUtf8("scatteringEqualsPropagationLoss"))
        # self.scatteringEqualsPropagationLoss.clicked.connect(self.btnstate)
        # self.scatteringLossProfile = QtGui.QRadioButton(self.waveguideLossesGroup)
        # self.scatteringLossProfile.setGeometry(QtCore.QRect(25, 65, 211, 22))
        # self.scatteringLossProfile.setObjectName(_fromUtf8("scatteringLossProfile"))
        # self.scatteringLossProfile.clicked.connect(self.btnstate)
        # self.labelProfileRMS = QtGui.QLabel(self.waveguideLossesGroup)
        # self.labelProfileRMS.setGeometry(QtCore.QRect(60, 100, 80, 17))
        # self.labelProfileRMS.setObjectName(_fromUtf8("labelProfileRMS"))
        # self.labelProfileRMSUnit = QtGui.QLabel(self.waveguideLossesGroup)
        # self.labelProfileRMSUnit.setGeometry(QtCore.QRect(280, 100, 66, 17))
        # self.labelProfileRMSUnit.setObjectName(_fromUtf8("labelProfileRMSUnit"))
        # self.profileSTD = QtGui.QLineEdit(self.waveguideLossesGroup)
        # self.profileSTD.setGeometry(QtCore.QRect(190, 95, 81, 27))
        # self.profileSTD.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        # self.profileSTD.setObjectName(_fromUtf8("profileSTD"))
        # self.profileSTD.returnPressed.connect(self.updateBackground)
        # self.profileSTD.setEnabled(False)

        # self.backgroundCalculations = QtGui.QGroupBox(Form)
        # self.backgroundCalculations.setMinimumSize(QtCore.QSize(400, 146))
        # self.backgroundCalculations.setGeometry(QtCore.QRect(30, 480, 411, 176))
        # self.backgroundCalculations.setObjectName(_fromUtf8("backgroundCalculations"))
        # self.update_mologram_2 = QtGui.QPushButton(self.backgroundCalculations)
        # self.update_mologram_2.setGeometry(QtCore.QRect(840, 135, 98, 27))
        # self.update_mologram_2.setObjectName(_fromUtf8("update_mologram_2"))
        # self.scatteringAttenuation_lbl = QtGui.QLabel('Scattering loss attenuation', self.backgroundCalculations)
        # self.scatteringAttenuation_lbl.setGeometry(QtCore.QRect(25, 30, 211, 17))
        # self.scatteringAttenuation = QtGui.QLabel(self.backgroundCalculations)
        # self.scatteringAttenuation.setGeometry(QtCore.QRect(250, 30, 211, 17))
        # self.scatteringAttenuation_units = QtGui.QLabel(self.backgroundCalculations)
        # self.scatteringAttenuation_units.setGeometry(QtCore.QRect(385, 30, 166, 17))
        # self.scatteringAttenuation_units.setObjectName(_fromUtf8("d_eff_units"))
        # self.specularlyReflectedPower_lbl = QtGui.QLabel('Specularly reflected power', self.backgroundCalculations)
        # self.specularlyReflectedPower_lbl.setGeometry(QtCore.QRect(25, 65, 211, 17))
        # self.specularlyReflectedPower = QtGui.QLabel(self.backgroundCalculations)
        # self.specularlyReflectedPower.setGeometry(QtCore.QRect(250, 65, 211, 17))
        # self.specularlyReflectedPower_units = QtGui.QLabel(self.backgroundCalculations)
        # self.specularlyReflectedPower_units.setGeometry(QtCore.QRect(385, 65, 166, 17))
        # self.specularlyReflectedPower_units.setObjectName(_fromUtf8("d_eff_units"))
        

        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        # Noise to image Widget

        self.addNoiseImageGroup = QtGui.QGroupBox(Form)

        self.noiseTypes_lbl = QtGui.QLabel()
        self.noiseTypes = QtGui.QComboBox()
        self.noiseTypes.currentIndexChanged.connect(self.noiseSourceChanged)

        allowedkwargs = self.allowedkwargsFunc()

        self.attributes = list()
        self.varMax = 0
        for key, var in allowedkwargs.items():
            self.varMax = np.max((self.varMax, len(var)))

        for i in range(self.varMax):
            self.attributes.append([QtGui.QLabel(), QtGui.QLineEdit()])

        self.applyButton = QtGui.QPushButton()
        self.applyButton.clicked.connect(self.applyImageNoise)


        fbox = QtGui.QFormLayout()
        fbox.addRow(self.noiseTypes_lbl, self.noiseTypes)
        for i in range(self.varMax):
            fbox.addRow(self.attributes[i][0],self.attributes[i][1])

        fbox.addRow(self.applyButton)

        self.addNoiseImageGroup.setLayout(fbox)

        self.retranslateUi(Form, default_values)
        QtCore.QMetaObject.connectSlotsByName(Form)

        hbox = QtGui.QHBoxLayout()
        self.setLayout(hbox)

        # box left top
        vbox = QtGui.QVBoxLayout()

        vbox.addWidget(self.noiseSourcesGroup)
        vbox.addWidget(self.addNoiseImageGroup)
        vbox.addStretch()

        # # # box right top
        # box_2 = QtGui.QFormLayout()
        # box_2.addWidget(self.field_distribution)

        hbox.addLayout(vbox)

        # hbox.addLayout(box_2)

    def retranslateUi(self, Form, values):

        noiseTypes = self.allowedNoiseTypes()
        self.noiseTypes.clear()
        for noiseType, value in noiseTypes.items():
            self.noiseTypes.addItem(noiseType)


    def getValues(self):
        """
        returns a dictionary of the Noise parameters
        """
        self.waveguideLosses_cb = QtGui.QCheckBox('Waveguide losses')
        self.nonSpecificBinding_cb = QtGui.QCheckBox('Non-Specific Binding')
        self.laserFluctuations_cb = QtGui.QCheckBox('Laserfluctions [%]')
        self.laserFluctuations = QtGui.QLineEdit()


        # Background parameters. 
        NoisePar = {
            'scatteringLoss' : self.waveguideLosses_cb.isChecked(),
            'nonspecificBindingNoise' : self.nonSpecificBinding_cb.isChecked(),
            'laserFluctuationsNoise' : self.laserFluctuations_cb.isChecked(),
            'laserFluctuationsMean' : float(str(self.laserFluctiations.text())),
        }

        return NoisePar

    def setValues(self,values):

        raise NotImplementedError()


# functions used for the implementation of the Image Noise Tab.

    def noiseSourceChanged(self):
        noiseType = str(self.noiseTypes.currentText())
        allowedkwargs = self.allowedkwargsFunc(self.allowedNoiseTypes(noiseType))
        defaultkwargs = self.kwdefaultsFunc()

        for i in range(self.varMax):
            if len(allowedkwargs) > i:
                self.attributes[i][0].setVisible(True)
                self.attributes[i][1].setVisible(True)
                self.attributes[i][0].setText(allowedkwargs[i])
                self.attributes[i][1].setText(str(defaultkwargs[allowedkwargs[i]]))
            else:
                self.attributes[i][0].setVisible(False)
                self.attributes[i][1].setVisible(False)
                self.attributes[i][0].setText('')


    def applyImageNoise(self):
        noiseType = str(self.noiseTypes.currentText())
        kwargs = dict()
        for attribute in self.attributes:
            if attribute[0].isVisible() == False: continue
            try:
                kwargs[str(attribute[0].text())] = float(str(attribute[1].text()))
            except:
                print('Wrong inputs')

        self.main_widget.plotWidget.addNoise(noiseType, **kwargs)



    def allowedNoiseTypes(self, key=None):
        allowedtypes = {
            'gaussian': 'gaussian_values',
            'localvar': 'localvar_values',
            'poisson': 'poisson_values',
            'salt': 'sp_values',
            'pepper': 'sp_values',
            's&p': 's&p_values',
            'speckle': 'gaussian_values',
            'none': 'none'}

        if key==None: return allowedtypes
        else: return allowedtypes[key]

    def kwdefaultsFunc(self, key=None):
        kwdefaults = {
            'mean': 0.,
            'std': 0.01**(1/2.),
            'amount': 0.05,
            'salt_vs_pepper': 0.5,
            'local_vars': 0.01}

        if key == None: return kwdefaults
        else: return kwdefaults[key]

    def allowedkwargsFunc(self, key=None):

        allowedkwargs = {
            'gaussian_values': ['mean', 'std'],
            'localvar_values': ['local_vars'],
            'sp_values': ['amount'],
            's&p_values': ['amount', 'salt_vs_pepper'],
            'poisson_values': [],
            'none' : []}

        if key == None: return allowedkwargs
        else: return allowedkwargs[key]


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

