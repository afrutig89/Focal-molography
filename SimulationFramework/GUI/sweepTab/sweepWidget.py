# -*- coding: utf-8 -*-

from PyQt4 import QtGui,QtCore
import sys
sys.path.append('../../')
from auxiliaries.writeLogFile import saveDB, loadDB, createDB, createSQLDict
from auxiliaries.pareto import paretoList
from GUI.worker import WorkerObject

import os
from PyQt4.QtGui import QMessageBox
import numpy as np
from datetime import timedelta
import time, datetime
import ast


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class SweepWidget(QtGui.QWidget):
    def __init__(self, main_widget, values):
        super(SweepWidget, self).__init__()

        self.main_widget = main_widget
        self.simulationList = []
        self.simulationRunning = False
        self.stopAll = False
        self.stopSimulation = False
        self.pause = False

        self.initUI(values)


    def initUI(self, values):

        # Setup the worker object and the worker_thread.
        self.worker = WorkerObject(self.main_widget)
        self.worker_thread = QtCore.QThread()
        self.worker.moveToThread(self.worker_thread)
        self.worker_thread.start()
        self.worker.signalStatus.connect(self.updateStatusBar)
        self.worker.results.connect(self.main_widget.updateResultsGUI)

        self.sweepGroup = QtGui.QGroupBox('Sweep')
        self.parameter_lbl = QtGui.QLabel('Parameter')
        self.parameter = QtGui.QComboBox()
        self.parameter.currentIndexChanged.connect(self.parameterChanged)
        self.fromTo_lbl = QtGui.QLabel('From / To')
        self.startValue = QtGui.QLineEdit()
        self.endValue = QtGui.QLineEdit()
        self.unit = QtGui.QLabel()
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.startValue)
        hbox.addWidget(self.endValue)
        hbox.addWidget(self.unit)
        self.amount_lbl = QtGui.QLabel('Amount')
        self.amount = QtGui.QLineEdit()
        self.scale_lbl = QtGui.QLabel('Scale')
        self.scale = QtGui.QComboBox()
        self.scale.addItem('Linear')
        self.scale.addItem('Logarithmic')
        self.addSweepButton = QtGui.QPushButton('Add Sweep')
        self.addSweepButton.clicked.connect(self.addSweep)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.parameter_lbl, self.parameter)
        fbox.addRow(self.fromTo_lbl,hbox)
        fbox.addRow(self.amount_lbl, self.amount)
        fbox.addRow(self.scale_lbl, self.scale)
        fbox.addRow(self.addSweepButton)

        self.sweepGroup.setLayout(fbox)

        self.simulationsGroup = QtGui.QGroupBox('Simulations')
        self.table = QtGui.QTableWidget()
        self.table.cellClicked.connect(self.setCurrentCell)

        self.add = QtGui.QPushButton('Add')
        self.add.setToolTip('<font color=black>Adds currents inputs</font>')
        self.add.clicked.connect(self.addRow)
        self.delete = QtGui.QPushButton('Delete')
        self.delete.clicked.connect(self.deleteEntry)
        self.deleteAll = QtGui.QPushButton('Delete all')
        self.deleteAll.clicked.connect(self.deleteAllEntries)
        self.moveUp = QtGui.QPushButton('Move up')
        self.moveUp.clicked.connect(lambda: self.moveEntry('up'))
        self.moveDown = QtGui.QPushButton('Move down')
        self.moveDown.clicked.connect(lambda: self.moveEntry('down'))

        self.start = QtGui.QPushButton('Start Simulation')
        self.start.clicked.connect(self.startSimulation)
        self.start.resize(self.start.sizeHint())
        self.stopSimulation = QtGui.QPushButton('Stop Simulation')
        self.stopAll = QtGui.QPushButton('Stop All')
        self.stopAll.clicked.connect(self.stopAllClicked)
        self.pauseSimulation = QtGui.QPushButton('Pause/Continue Simulation')
        self.pauseSimulation.clicked.connect(self.pauseSimulationClicked)

        self.databaseParams = QtGui.QGroupBox()
        self.databaseParams.setGeometry(QtCore.QRect(30, 30, 531, 261))
        self.databaseParams.setMinimumSize(QtCore.QSize(431, 430))
        self.databaseParams.setObjectName(_fromUtf8("databaseParams"))
        self.databaseParams.setTitle(_translate("Form", "Database", None))
        self.label_folder = QtGui.QLabel(self.databaseParams)
        self.label_folder.setGeometry(QtCore.QRect(25, 30, 101, 17))
        self.label_folder.setObjectName(_fromUtf8("label_folder"))
        self.label_folder.setText(_translate("Form", "Folder", None))
        self.databaseFolder = QtGui.QLineEdit(self.databaseParams)
        self.databaseFolder.setGeometry(QtCore.QRect(190, 25, 181, 27))
        self.databaseFolder.setObjectName(_fromUtf8("databaseFolder"))
        self.databaseFolder.setText(_translate("Form", os.path.dirname(values['databaseFile']), None))
        self.browse = QtGui.QPushButton('...', self.databaseParams)
        self.browse.setGeometry(QtCore.QRect(385, 25, 40, 27))
        self.browse.clicked.connect(self.browse_f)
        self.label_filename = QtGui.QLabel(self.databaseParams)
        self.label_filename.setGeometry(QtCore.QRect(25, 70, 151, 17))
        self.label_filename.setObjectName(_fromUtf8("label_filename"))
        self.label_filename.setText(_translate("Form", "Filename", None))
        self.databaseFilename = QtGui.QLineEdit(self.databaseParams)
        self.databaseFilename.setGeometry(QtCore.QRect(190, 65, 181, 27))
        self.databaseFilename.setObjectName(_fromUtf8("databaseFilename"))
        self.databaseFilename.setText(_translate("Form", values['databaseFile'].split('/')[-1], None))
        self.label_description = QtGui.QLabel(self.databaseParams)
        self.label_description.setGeometry(QtCore.QRect(25, 110, 151, 17))
        self.label_description.setObjectName(_fromUtf8("label_description"))
        self.label_description.setText(_translate("Form", "Description", None))
        self.description = QtGui.QLineEdit(self.databaseParams)
        self.description.setGeometry(QtCore.QRect(190, 105, 181, 27))
        self.description.setObjectName(_fromUtf8("databaseFile"))
        self.description.setText(_translate("Form", "Default", None))
        self.label_parameters = QtGui.QLabel(self.databaseParams)
        self.label_parameters.setGeometry(QtCore.QRect(25, 150, 151, 17))
        self.label_parameters.setObjectName(_fromUtf8("label_parameters"))
        self.label_parameters.setText(_translate("Form", "Parameters", None))
        
        self.checkBox = []
        self.checkBoxValues = []
        x = [40, 230]
        y = 170
        for key in self.main_widget.simResults.getValues().keys():
            if key == 'ID' or key not in values: continue
            self.checkBox.append(QtGui.QCheckBox(self.databaseParams))
            self.checkBox[-1].setGeometry(QtCore.QRect(x[len(self.checkBox) % 2 ==0], y + ((len(self.checkBox)-1) / 2)*25, 175, 22))
            self.checkBox[-1].setChecked(values[key])
            self.checkBox[-1].setObjectName(_fromUtf8("checkBox"))
            self.checkBox[-1].setText(_translate("Form", key, None))
            self.checkBoxValues.append(key)

        self.stop  = False
        self.pause = False

        # layout
        hbox_table = QtGui.QHBoxLayout()

        vbox_table_buttons = QtGui.QVBoxLayout()
        vbox_table_buttons.addWidget(self.add)
        vbox_table_buttons.addWidget(self.delete)
        vbox_table_buttons.addWidget(self.deleteAll)
        vbox_table_buttons.addWidget(self.moveUp)
        vbox_table_buttons.addWidget(self.moveDown)

        hbox_table.addWidget(self.table)
        hbox_table.addLayout(vbox_table_buttons)

        hbox_stop = QtGui.QHBoxLayout()
        hbox_stop.addWidget(self.stopAll)
        hbox_stop.addWidget(self.stopSimulation)
        hbox_stop.addWidget(self.pauseSimulation)

        fbox = QtGui.QFormLayout()
        fbox.addRow(hbox_table)
        fbox.addRow(self.start)
        fbox.addRow(hbox_stop)

        self.simulationsGroup.setLayout(fbox)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.sweepGroup)
        vbox.addWidget(self.simulationsGroup)
        vbox.addWidget(self.databaseParams)
        self.setLayout(vbox)

        self.retranslateUi()


    def retranslateUi(self):
        # fill parameter combo
        params = []
        self.keys = []
        # mologram parameter
        for param, paramInfos in sweepParameters.items():
            self.keys.append(param)
            params.append(paramInfos[0])

        # sort alphabetic
        sort = np.array(params).argsort()
        self.parameter.addItems(np.array(params)[sort])
        self.keys = np.array(self.keys)[sort]

        # update units
        self.parameterChanged()


    def parameterChanged(self):
        index = self.parameter.currentIndex()
        key = self.keys[index]
        self.unit.setText(sweepParameters[key][2])

        if key == 'coherentParticles' or key == 'noncoherentParticles':
            self.main_widget.mologramTab.radioButton_1.setChecked(True)

        elif key == 'N_particles' or key == 'coherentBinding':
            self.main_widget.mologramTab.radioButton_2.setChecked(True)

        elif key == 'particleDensity':
            self.main_widget.mologramTab.radioButton_3.setChecked(True)



    def addSweep(self):
        index = self.parameter.currentIndex()
        key = self.keys[index]

        # read start and stop and make unit conversion
        start = float(self.startValue.text())/sweepParameters[key][1]
        stop = float(self.endValue.text())/sweepParameters[key][1]
        num = int(self.amount.text())

        if self.scale.currentText() == 'Linear':
            sweep = np.linspace(start, stop, num)

        elif self.scale.currentText() == 'Logarithmic':
            sweep = np.logspace(np.log10(start), np.log10(stop), num)

        parameters = self.main_widget.getParametersAsDicts(sweep = True)

        # look for given parameter in parameter attributes
        for attribute in dir(parameters):
            if type(getattr(parameters,attribute)) == dict:
                # type must be a dictionary
                if key in getattr(parameters,attribute):
                    # sweep parameter if key found
                    for value in sweep:
                        getattr(parameters,attribute)[key] = value
                        self.addRow(parameters)


    def addRow(self, parameters = False):
        """
        Default: empty row
        """

        rowPosition = self.table.rowCount()
        self.table.insertRow(rowPosition)

        if 'horHeaders' not in dir(self):
            self.horHeaders = []

        if parameters == False:
            parameters = self.main_widget.getParametersAsDicts(sweep = True)

        for attribute in dir(parameters):
            if type(getattr(parameters,attribute)) == dict:
                if attribute not in self.horHeaders:
                    self.horHeaders.append(attribute)
                    self.table.setColumnCount(len(self.horHeaders))
                    self.table.setHorizontalHeaderLabels(self.horHeaders)

                self.table.setItem(
                        rowPosition, 
                        self.horHeaders.index(attribute), 
                        QtGui.QTableWidgetItem(str(getattr(parameters,attribute)))
                    )

    def startSimulation(self):
        self.simulationRunning = True
        self.simulationNo = 0
        self.continueSimulation()


    def continueSimulation(self):
        # select next line
        if self.simulationNo == self.table.rowCount() \
                or self.stopAll == True:
            # return if no rows left or the simulation stopped
            self.main_widget.update == True
            self.stopAll = False
            self.stopSimulation = False
            return

        if self.stopSimulation == True:
            self.simulationNo += 1
            self.stopSimulation = False
            self.continueSimulation()
            return

        while self.pause:
            # pause
            time.sleep(0.5)


        # increment simulation number
        self.table.selectRow(self.simulationNo)

        # load parameters
        parameters = dict()
        for horHeader in self.horHeaders:
            parameters[horHeader] = eval(str(self.table.item(self.simulationNo, self.horHeaders.index(horHeader)).text()).replace('array','np.array'))


        self.parameters = SimulationInputValues()
        self.parameters.waveguide = parameters['waveguide']
        self.parameters.mologram = parameters['mologram']
        self.parameters.particle = parameters['particle']
        self.parameters.screen = parameters['screen']
        self.parameters.settings, self.parameters.settingsSave = self.getValues()

        self.simulationNo += 1
        self.main_widget.updateStatus("Simulation {}/{}".format(self.simulationNo,self.table.rowCount()))

        self.worker.startSimulation(self.parameters)


    def setCurrentCell(self, row, column):
        self.currentCell = row, column


    def deleteEntry(self):
        if 'currentCell' in dir(self):
            self.table.removeRow(self.currentCell[0])
            # delete variable in order to avoid second time delete button clicked
            del self.currentCell



    def deleteAllEntries(self):
        while (self.table.rowCount() > 0):
            self.table.removeRow(0)


    def moveEntry(self, direction):
        row = self.table.currentRow()
        column = self.table.currentColumn()

        if direction == 'up' and row > 0:
            newRow = row-1
            deleteRow = row+1

        elif direction == 'down' and row < self.table.rowCount()-1:
            newRow = row+2
            deleteRow = row

        else:
            return

        # insert new row and copy entries
        self.table.insertRow(newRow)
        for i in range(self.table.columnCount()):
            self.table.setItem(newRow, i, self.table.takeItem(deleteRow,i))

        # delete old row
        self.table.setCurrentCell(newRow, column)
        self.table.removeRow(deleteRow)


    def stopSimulationClicked(self):
        if self.simulationRunning:
            # change only if experiment running
            self.stopSimulation = True


    def pauseSimulationClicked(self):
        if self.simulationRunning:
            # change only if experiment running
            self.pause = not self.pause


    def stopAllClicked(self):
        self.stopAll = True
        self.worker.stop()
        self.worker_thread.quit()
        self.worker_thread.wait()


    def updateStatusBar(self, progress):
        if len(progress) == 1:
            # only status message
            self.main_widget.updateStatus(progress[0])

        if len(progress) > 2:
            self.main_widget.updateStatus(progress[2])

        if progress[0] == 100.:
            self.continueSimulation()


    def getValues(self):

        settingsPar = dict()

        # database file
        settingsPar['databaseFile'] = '{}/{}'.format(self.databaseFolder.text(), self.databaseFilename.text())
        # description
        settingsPar['description'] = str(self.description.text())
        settingsPar['saveDatabase'] = True
        settingsPar['saveLog'] = False

        settingsPar['startTime'] = time.strftime("%H:%M:%S")
        settingsPar['date'] = str(datetime.date.today())
        settingsPar['start'] = time.time()

        settingsPar['saveFigure'] = 0 # don't save if displayed

        # to implement
        if self.main_widget.settingsTab.method.currentText() == "Split Process (Windows)":
            settingsPar['calculationMethod'] = 'splitProcess'
        elif self.main_widget.settingsTab.method.currentText() == "Parallel Cores (Unix)":
            settingsPar['calculationMethod'] = 'parallelPool'
        else:
            settingsPar['calculationMethod'] = 'Cuda'


        # check buttons
        settingsSave = dict()
        checkBoxValues = self.checkBoxValues[::-1]
        for checkedBox in self.checkBox:
            checked = checkedBox.isChecked()
            settingsSave[checkBoxValues.pop()] = int(checked)

        return settingsPar, settingsSave


    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '/home/focal-molography/')
        self.databaseFolder.setText(folder)
# not nice that I need those dictionaries again, but adjustments are quite easy and 
# it helps the clarity of the program

sweepParameters = {
    # mologram
    'focalPoint': ['Focal point', 1e6, 'um'],
    'diameter': ['Mologram diameter', 1e6, 'um'],
    'N_particles': ['Number of particles', 1, ''],
    'coherentBinding': ['Coherent binding', 100, '%'],
    'particleDensity': ['Particle density', 1e6, '#/mm2'],
    'z_min': ['Minimum vertical distance', 1e9, 'nm'],
    'z_max': ['Maximum vertical distance', 1e9, 'nm'],
    'braggOffset': ['Bragg offset', 1e6, 'um'],
    'particleRadius' : ['Particle radius', 1e9, 'nm'],

    # waveguide
    'wavelength' : ['Wavelength', 1e9, 'nm'],
    'inputPower' : ['Input power', 1e3, 'mW'],
    'beamWaist' : ['Beam waist', 1e3, 'mm'],
    'couplingEfficiency':['Coupling efficiency', 1e2, '%'],
    'gratingPosition' : ['Grating position', 1e3, 'mm'],
    'propagationLoss': ['Propagation loss', np.log(10)/10. * 100, 'dB/cm'],
    'thickness' : ['Waveguide thickness', 1e9, 'nm'],

    # noise analysis
    # to do

    # camera simuation
    # to do

    # binding curve
    # to do
}