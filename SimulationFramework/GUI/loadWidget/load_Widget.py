# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'loadWidget.ui'
#
# Created: Fri Jan  6 10:05:54 2017
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import os
import sys
sys.path.append('../../')
import sqlite3
from auxiliaries.writeLogFile import loadDB, deleteRow
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT #as NavigationToolbar

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class LoadWidget(QtGui.QWidget):
    """Widget that allows to display and plot the content of a database. """

    def __init__(self, main_widget, databasePath = 'database/default.db'):
        super(LoadWidget, self).__init__()

        DatabaseWidget = QtGui.QWidget()

        # Initialize member variables       
        self.main_widget = main_widget
        self.databasePath = databasePath

        # Call child constructor
        self.setupUi(DatabaseWidget)
        self.center()


    def setupUi(self, DatabaseWidget):
        DatabaseWidget.setObjectName(_fromUtf8("DatabaseWidget"))
        #DatabaseWidget.resize(1857, 1649)
        self.setGeometry(QtCore.QRect(800, 800, 1500, 800))

        self.databaseGroup = QtGui.QGroupBox(DatabaseWidget)

        # display the database content in the TableWidget.
        self.database = QtGui.QTableWidget()
        self.database.cellClicked.connect(self.setCurrentCell)
        self.database.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.database.setColumnCount(100)

        self.fileCombo = QtGui.QComboBox()
        # selector of which database table to display.
        self.dbTableCombo = QtGui.QComboBox()
        self.dbTableCombo.addItem(_fromUtf8(""))
        self.dbTableCombo.addItem(_fromUtf8(""))
        self.dbTableCombo.currentIndexChanged.connect(self.loadData)

        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Experiment Folder')
        # line edits and selectors
        self.editFolder = QtGui.QLineEdit()
        # push button
        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.loadDatabase)
        self.browse.resize(self.browse.sizeHint())

        # layout
        hbox_folder = QtGui.QHBoxLayout()
        hbox_folder.addWidget(self.editFolder_lbl)
        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.dbTableCombo, hbox_folder) # table and path selector
        fbox.addRow(self.database) # database data table display

        self.databaseGroup.setLayout(fbox)

        self.loadGroup = QtGui.QGroupBox(DatabaseWidget)
        self.loadLog = QtGui.QCheckBox()
        self.loadLog.setChecked(True)
        self.loadResults = QtGui.QCheckBox()
        self.loadResults.setChecked(True)
        self.buttonBox = QtGui.QDialogButtonBox()
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))

        self.buttonBox.accepted.connect(self.getint)
        self.buttonBox.rejected.connect(self.close)

        fbox2 = QtGui.QFormLayout()
        fbox2.addRow(self.loadLog, self.loadResults)
        fbox2.addRow(self.buttonBox)

        self.loadGroup.setLayout(fbox2)

        # change the titles of the user interface. (Would be for Language Translations)
        self.retranslateUi(DatabaseWidget)

        QtCore.QMetaObject.connectSlotsByName(DatabaseWidget)
        self.setWindowTitle('Load Widget')

        hbox = QtGui.QVBoxLayout()
        self.setLayout(hbox)

        self.loadData()
        self.database.resizeColumnsToContents()

        hbox.addWidget(self.databaseGroup)
        hbox.addWidget(self.loadGroup)
        

    def getint(self):
        """Loads the selected entry from the database and updates the main widget

        :return parameters,results: returns two dicts, parameters and results from the Query.
        
        """
        
        if not 'currentRow' in dir(self):
            num,ok = QtGui.QInputDialog.getInt(self, "load parameter", "enter the index")
            if not ok: return
        
        else:
            num = self.currentRow

        # get the parameters for the selected simulation, get the simulation with the same ID from the other table.
        if self.dbTableCombo.currentText() == 'log':
            
            logParameters = loadDB(self.databasePath, 'log')[num]

            # get the results of the selected simulation. 
            results = loadDB(self.databasePath, 'results',condition='WHERE ID==' + str(logParameters['ID']))
        
        if self.dbTableCombo.currentText() == 'results':
            results = loadDB(self.databasePath, 'results')[num]

            logParameters = loadDB(self.databasePath, 'log',condition='WHERE ID==' + str(results['ID']))


        # update the parameters in the relevant tabs. 
        self.main_widget.waveguideTab.setValues(logParameters)
        self.main_widget.mologramTab.setValues(logParameters)
        self.main_widget.screenTab.setValues(logParameters)
    
        self.main_widget.getParameters() 

        # I need to convert the results to the right datatypes
        print "hello1"
        print results
        self.main_widget.updateResults(results)
        print "hello"
        self.close()

        

        return logParameters,results

    def setCurrentCell(self, row, column):
        self.currentRow = row
        self.currentColumn = column
  

    def loadData(self):

        self.data_entries = loadDB(self.databasePath, str(self.dbTableCombo.currentText()))
        # data by keys
        if type(self.data_entries) != list:
            # variable must be a list in order to iterate
            self.data_entries = list([self.data_entries])

        names=self.data_entries[0].keys()
        self.database.setRowCount(len(self.data_entries))
        self.database.setColumnCount(len(names))
        self.database.setHorizontalHeaderLabels(names)
        # fill in table
        entry_id=0
        for entry in self.data_entries: 
            item_id=0
            for i in entry.keys():
                self.database.setItem(entry_id, item_id, QtGui.QTableWidgetItem(str(entry[i])))
                item_id+=1
            entry_id+=1

    def retranslateUi(self, DatabaseWidget):
        DatabaseWidget.setWindowTitle(_translate("DatabaseWidget", "Database", None))
        self.loadGroup.setTitle(_translate("DatabaseWidget", "Load Parameters", None))
        self.loadLog.setText(_translate("DatabaseWidget", "Load Settings", None))
        self.loadResults.setText(_translate("DatabaseWidget", "Load Results", None))
        self.databaseGroup.setTitle(_translate("DatabaseWidget", "Database", None))
        self.dbTableCombo.setItemText(0, _translate("DatabaseWidget", "log", None))
        self.dbTableCombo.setItemText(1, _translate("DatabaseWidget", "results", None))
        self.editFolder.setText(self.databasePath)

    def center(self):
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())


    def loadDatabase(self):
        databaseFile = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home/focal-molography/Experiments', "Database files (*.db)")
        if databaseFile != '':
            self.databasePath = str(databaseFile)
            self.editFolder.setText(self.databasePath)
            self.loadData()


class LoadPlotWidget(LoadWidget):
    """A load widget that also has plotting functionalities"""
    def __init__(self,main_widget, databasePath = 'database/default.db'):
        super(LoadWidget, self).__init__()
        DatabaseWidget = QtGui.QWidget()

        # Initialize member variables       
        self.main_widget = main_widget
        self.databasePath = databasePath
        # Call child constructor
        self.setupUi(DatabaseWidget)
        self.center()

    def setupUi(self,DatabaseWidget):

        DatabaseWidget.setObjectName(_fromUtf8("DatabaseWidget"))
        DatabaseWidget.resize(1857, 1649)


        self.databaseGroup = QtGui.QGroupBox(DatabaseWidget)

        # display the database content in the TableWidget.
        self.database = QtGui.QTableWidget()
        self.database.cellClicked.connect(self.setCurrentCell)
        self.database.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

        self.plot_x = QtGui.QPushButton('X Axis')
        self.plot_x.clicked.connect(self.loadForPlot_x)
        self.plot_x.resize(self.plot_x.sizeHint())

        self.plot_y = QtGui.QPushButton('Y Axis')
        self.plot_y.clicked.connect(self.loadForPlot_y)
        self.plot_y.resize(self.plot_y.sizeHint())

        self.plot_confirm = QtGui.QPushButton('Plot')
        self.plot_confirm.clicked.connect(self.plot_data)
        self.plot_confirm.resize(self.plot_confirm.sizeHint())

        self.figure  = plt.figure(figsize=(10,5),facecolor='None',edgecolor='None')
        self.canvas  = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar2QT(self.canvas, self)


        self.fileCombo = QtGui.QComboBox()
        # selector of which database table to display.
        self.dbTableCombo = QtGui.QComboBox()
        self.dbTableCombo.addItem(_fromUtf8(""))
        self.dbTableCombo.addItem(_fromUtf8(""))
        self.dbTableCombo.currentIndexChanged.connect(self.loadData)

        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Experiment Folder')
        # line edits and selectors
        self.editFolder = QtGui.QLineEdit()
        # push button
        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.loadDatabase)
        self.browse.resize(self.browse.sizeHint())

        # layout
        hbox_folder = QtGui.QHBoxLayout()
        hbox_folder.addWidget(self.editFolder_lbl)
        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        self.plotBox = QtGui.QHBoxLayout()
        self.plotBox.addWidget(self.plot_x)
        self.plotBox.addWidget(self.plot_y)
        self.plotBox.addWidget(self.plot_confirm)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.dbTableCombo, hbox_folder) # table and path selector
        fbox.addRow(self.database) # database data table display
        fbox.addRow(self.plotBox) # plot control buttons
        fbox.addRow(self.toolbar) # plot toolbar
        fbox.addRow(self.canvas) # matplotlib plot canvas


        self.databaseGroup.setLayout(fbox)

        self.plotSingleGroup = QtGui.QGroupBox(DatabaseWidget)
        self.plotSingleGroup.setGeometry(QtCore.QRect(470, 330, 371, 141))
        self.plotSingleGroup.setObjectName(_fromUtf8("plotSingleGroup"))
        self.label = QtGui.QLabel(self.plotSingleGroup)
        self.label.setGeometry(QtCore.QRect(20, 30, 66, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.plotSingleGroup)
        self.label_2.setGeometry(QtCore.QRect(20, 60, 66, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.comboBox = QtGui.QComboBox(self.plotSingleGroup)
        self.comboBox.setGeometry(QtCore.QRect(100, 30, 261, 27))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox_2 = QtGui.QComboBox(self.plotSingleGroup)
        self.comboBox_2.setGeometry(QtCore.QRect(100, 60, 261, 27))
        self.comboBox_2.setObjectName(_fromUtf8("comboBox_2"))
        self.plotButton = QtGui.QPushButton(self.plotSingleGroup)
        self.plotButton.setGeometry(QtCore.QRect(260, 100, 98, 27))
        self.plotButton.setObjectName(_fromUtf8("plotButton"))

        self.loadGroup = QtGui.QGroupBox(DatabaseWidget)
        self.loadLog = QtGui.QCheckBox()
        self.loadLog.setChecked(True)
        self.loadResults = QtGui.QCheckBox()
        self.loadResults.setChecked(True)
        self.buttonBox = QtGui.QDialogButtonBox()
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))

        self.buttonBox.accepted.connect(self.getint)
        self.buttonBox.rejected.connect(self.close)

        fbox2 = QtGui.QFormLayout()
        fbox2.addRow(self.loadLog, self.loadResults)
        fbox2.addRow(self.buttonBox)

        self.loadGroup.setLayout(fbox2)

        self.retranslateUi(DatabaseWidget)

        QtCore.QMetaObject.connectSlotsByName(DatabaseWidget)
        self.setWindowTitle('Load Widget')

        hbox = QtGui.QVBoxLayout()
        self.setLayout(hbox)

        self.loadData()
        self.database.resizeColumnsToContents()

        hbox.addWidget(self.databaseGroup)
        hbox.addWidget(self.loadGroup)
        self.setGeometry(QtCore.QRect(800, 800, 1500, 800))


    def retranslateUi(self, DatabaseWidget):
        DatabaseWidget.setWindowTitle(_translate("DatabaseWidget", "Database", None))
        self.plotSingleGroup.setTitle(_translate("DatabaseWidget", "Plot Single Parameter", None))
        self.label.setText(_translate("DatabaseWidget", "x-Axis:", None))
        self.label_2.setText(_translate("DatabaseWidget", "y-Axis:", None))
        self.plotButton.setText(_translate("DatabaseWidget", "Plot", None))
        self.loadGroup.setTitle(_translate("DatabaseWidget", "Load Parameters", None))
        self.loadLog.setText(_translate("DatabaseWidget", "Load Settings", None))
        self.loadResults.setText(_translate("DatabaseWidget", "Load Results", None))
        self.databaseGroup.setTitle(_translate("DatabaseWidget", "Database", None))
        self.dbTableCombo.setItemText(0, _translate("DatabaseWidget", "log", None))
        self.dbTableCombo.setItemText(1, _translate("DatabaseWidget", "results", None))
        self.editFolder.setText(self.databasePath)


    def loadForPlot_x(self):
        if not 'currentColumn' in dir(self):
            text,ok = QtGui.QInputDialog.getText(self, "X axis", "enter the name as String: ")
            x_extracted=loadDB(self.databasePath, str(self.dbTableCombo.currentText()),text)
            self.x_data=[]
            self.x_label=text
            for entry in x_extracted:
                self.x_data.extend(entry.values())
            if not ok: return
        else:
            col = self.currentColumn
            self.x_data=[]       
            self.x_label=self.data_entries[0].keys()[col]
            for entry in self.data_entries: 
                self.x_data.append(entry.values()[col])

            QtGui.QMessageBox.information(self, "Message",'X axis havs been selected')
        print self.x_data
    
    def loadForPlot_y(self):
        if not 'currentColumn' in dir(self):
            text,ok = QtGui.QInputDialog.getText(self, "Y axis", "enter the name as String: ")
            self.y_data=loadDB(self.databasePath, str(self.dbTableCombo.currentText()), text)
            y_extracted=loadDB(self.databasePath, str(self.dbTableCombo.currentText()),text)
            self.y_data=[]
            self.y_label=text
            for entry in y_extracted:
                self.y_data.extend(entry.values())
            if not ok: return
            
        else:
            col = self.currentColumn
            self.y_data=[]       
            self.y_label=self.data_entries[0].keys()[col]      
            for entry in self.data_entries: 
                self.y_data.append(entry.values()[col])
            QtGui.QMessageBox.information(self, "Message",'Y axis havs been selected')
        print self.y_data

    
    def plot_data(self):
        plt.cla() # that is really bad practice and leads to untractable errors. 
        ax = self.figure.add_subplot(111)
        ax.plot(self.x_data,self.y_data,'-')
        ax.set_xlabel(self.x_label)
        ax.set_ylabel(self.y_label)
        self.canvas.draw()

if __name__ == "__main__":
    pass
