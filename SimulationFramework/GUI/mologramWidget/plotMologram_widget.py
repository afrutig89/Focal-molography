from PyQt4 import QtGui, QtCore

# matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import matplotlib.image as mpimg

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT# as NavigationToolbar

from matplotlib.backend_bases import cursors as mplCursors
from matplotlib.patches import Ellipse

import sys
sys.path.append('../')
from mologram.generatePattern import generateMologram_cf
import numpy as np


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class PlotMologramWidget(QtGui.QWidget):
    """Class for plotting the particle distribution on a mologram."""
    def __init__(self,main_widget):

        # Call parent class constructor
        super(PlotMologramWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        # Call child constructor
        self.initUI()

    """ Initialization of UI """
    def initUI(self):

        self.figure  = plt.figure(figsize=(10,10),facecolor='None',edgecolor='None')
        self.canvas  = FigureCanvas(self.figure)
        
        self.showLinesButton = QtGui.QAction('Show lines', self, checkable = True)
        self.showLinesButton.setStatusTip('Show lines')
        self.showLinesButton.setToolTip('<font color=black>Show lines</font>')
        self.showLinesButton.triggered.connect(self.showLines)

        self.toolbar = QtGui.QToolBar()
        self.toolbar.addWidget(NavigationToolbar(self.canvas, self))
        self.toolbar.addWidget(QtGui.QWidget())
        # some dummy actions
        self.toolbar.addAction(self.showLinesButton)

        main_box = QtGui.QHBoxLayout()

        v_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
        v_splitter.addWidget(self.toolbar)
        v_splitter.addWidget(self.canvas)

        main_box.addWidget(v_splitter)

        self.setLayout(main_box)

        cid = self.figure.canvas.mpl_connect('resize_event', self.onReDraw)


    def newImage(self, mologram):
        """Draws the mologram in the Widget

        :param mologram: instance of mologram class.

        """

        # clear plot if exist
        if 'ax' in dir(self):
            try:
                self.ax.cla()
            except: pass

        self.mologram = mologram
        x_sca, y_sca, z_sca = mologram.dipolePositions

        self.ax = self.figure.add_subplot(111)

        # only for TE!
        # # colored scatterers
        # self.ax.scatter(x_sca*1e6, y_sca*1e6, c=mologram.bind_no_bind, cmap = 'hot')

        # ridges
        self.ax.scatter(x_sca[mologram.boundParticles]*1e6, y_sca[mologram.boundParticles]*1e6, color='green')
        # grooves
        self.ax.scatter(x_sca[mologram.boundParticles == False]*1e6, y_sca[mologram.boundParticles == False]*1e6, color='red')

        self.ax.axis('equal')
        self.ax.set_xlim(-mologram.radius*1e6, mologram.radius*1e6)
        self.ax.set_ylim(-mologram.radius*1e6, mologram.radius*1e6)

        self.ax.text(mologram.radius*1e6-60, -mologram.radius*1e6+30, 'Ridges:')
        self.ax.text(mologram.radius*1e6-15, -mologram.radius*1e6+30, '{}'.format(np.sum(mologram.boundParticles)))
        self.ax.text(mologram.radius*1e6-60, -mologram.radius*1e6+20, 'Grooves:')
        self.ax.text(mologram.radius*1e6-15, -mologram.radius*1e6+20, '{}'.format(np.sum(mologram.boundParticles == False)))
        self.ax.text(mologram.radius*1e6-60, -mologram.radius*1e6+10, 'Total:')
        self.ax.text(mologram.radius*1e6-15, -mologram.radius*1e6+10, '{}'.format(len(y_sca)))

        self.figure.tight_layout()

        self.canvas.draw()

    def showLines(self):
        """Displays the molographic lines in the plot and is called upon the button 'Show lines' is checked. """
        if 'mologram' not in dir(self):
            return

        if not self.showLinesButton.isChecked():
            # delete lines
            x_min, x_max = self.ax.get_xlim()
            y_min, y_max = self.ax.get_ylim()
            self.newImage(self.mologram)
            self.ax.set_xlim([x_min, x_max])
            self.ax.set_ylim([y_min, y_max])
            self.canvas.draw()

            return

        mologram = self.mologram
        f = abs(mologram.focalPoint)
        n_m = mologram.n_m
        N_eff = mologram.waveguide.N
        wavelength = mologram.wavelength
        
        xlim = np.array(self.ax.get_xlim())*1e-6
        ylim = np.array(self.ax.get_ylim())*1e-6

        mologram_seed_parameters = generateMologram_cf(mologram, mologram.j_max-mologram.j_min)
        x_lines, y_lines, cdfs, lengths, valid_j = mologram_seed_parameters

        # convert to um
        x_lines *= 1e6
        y_lines *= 1e6
        deltas = mologram.calcLineDistances()[(valid_j-valid_j[0]).astype(int)]*1e6

        for i in range(y_lines.shape[1]-1):
            try:
                # lines outside the mologram range are nan, therefore we need a try
                self.ax.fill_betweenx(y_lines[i], x_lines[i]-deltas[i]/4.,x_lines[i]+deltas[i]/4.,color='r',alpha=0.1)
                self.ax.fill_betweenx(-y_lines[i], x_lines[i]-deltas[i]/4.,x_lines[i]+deltas[i]/4.,color='r',alpha=0.1)
            except:
                continue

        self.canvas.draw()


    def onReDraw(self,event):
        pass
        # if self.showLinesButton.isChecked():
        #     # reset image
        #     x_min, x_max = self.ax.get_xlim()
        #     y_min, y_max = self.ax.get_ylim()
        #     self.newImage(self.mologram)
        #     self.ax.set_xlim([x_min, x_max])
        #     self.ax.set_ylim([y_min, y_max])
        #     self.canvas.draw()

        #     # plot lines
        #     self.showLines()


class NavigationToolbar(NavigationToolbar2QT):
    # only display the buttons we need
    # run simulation action
    
    toolitems = [t for t in NavigationToolbar2QT.toolitems if t[0] in ('Home', 'Pan', 'Zoom', 'Save')]

    # def mouse_move(self, event):
    #     pass