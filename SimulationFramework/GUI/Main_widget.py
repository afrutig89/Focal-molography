# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created: Thu Dec  1 16:12:26 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtWebKit import QWebView
from PyQt4.QtCore import QUrl
from PyQt4.QtGui import QMessageBox
import numpy as np
import math
import os
import time
from waveguideTab.waveguideWidget import WaveguideWidget
from mologramTab.mologram_widget import MologramWidget
from mologramWidget.plotMologram_widget import PlotMologramWidget
from noiseTab.noise_widget import NoiseWidget
from screenTab.screen_widget import ScreenWidget
from settingsTab.settings_widget import SettingsWidget
from parameterTab.parameterWidget import ParameterWidget
from plotWidget.plot_widget import PlotWidget
from analyticalWidget.analyticalWidget import AnalyticalWidget
from GUI.materialWidget.materials_widget import MaterialsWidget
from GUI.loadWidget.load_Widget import LoadWidget,LoadPlotWidget
from GUI.sweepTab.sweepWidget import SweepWidget
from experimentTab.experimentTab import ExperimentWidget

import time, datetime
import sys
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.image as mpimg
sys.path.append('../')
from variables import FieldSimulationInputs,FieldSimulationResults,CameraSimulationInputs,CameraSimulationResults, commonKeys
from auxiliaries.writeLogFile import saveDB, createDB, loadDB, createSQLDict, deleteRow
from analysis.analytical import analytical
from auxiliaries.rstGenerator import rstFile

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class MainWidget(QtGui.QWidget, QtCore.QThread, QtCore.QObject):
    """Class that holds all widgets of the simulation application. """
    def __init__(self, app, default_values):

        # Call parent class constructor
        super(MainWidget,self).__init__()

        # Initialize member variables       
        self.app = app
        self.update = True
        self.simInputs = FieldSimulationInputs()    # class to store the simulation input parameters  
        self.simResults = FieldSimulationResults()                          # class to store the results of the last simulation if it was not a sweep.
        self.camSimInputs = CameraSimulationInputs()
        self.camSimRes = CameraSimulationResults()
        # Call child constructor
        self.initUI(default_values)



    # Initialization of GUI
    def initUI(self, default_values):

        # Create widgets, pass self so they can talk to each other

        self.textBrowser = QtGui.QTextEdit()
        self.textBrowser.installEventFilter(self)

        # set up widgets
        self.mologramTab    = MologramWidget(self, default_values)
        self.waveguideTab   = WaveguideWidget(self, default_values)
        self.noiseTab       = NoiseWidget(self, default_values)
        self.screenTab      = ScreenWidget(self, default_values)
        self.settingsTab    = SettingsWidget(self, default_values)
        self.experimentTab  = ExperimentWidget(self)
        self.sweepTab       = SweepWidget(self, default_values)


        self.parameterBar   = ParameterWidget(self, default_values)
        self.mologramWidget = PlotMologramWidget(self)
        self.plotWidget     = PlotWidget(self)
        self.analyticalTab  = AnalyticalWidget(self)

        # documentation
        self.documentationTab = QWebView()
        self.documentationTab.load(QUrl('./documentation/_build/html/index.html'))


        self.tabs_top = QtGui.QTabWidget()
        self.tabs_top.addTab(self.waveguideTab, 'Waveguide')
        self.tabs_top.addTab(self.mologramTab, 'Mologram')
        self.tabs_top.addTab(self.screenTab, 'Screen')
        self.tabs_top.addTab(self.noiseTab, 'Noise')
        self.tabs_top.addTab(self.settingsTab, 'Settings')

        self.tabs2_top = QtGui.QTabWidget()
        self.tabs2_top.addTab(self.plotWidget,'Intensity')
        self.tabs2_top.tabCloseRequested.connect(self.removeTab)
        self.tabs2_top.currentChanged.connect(self.onChange)
        self.tabs2_top.setTabsClosable(True)

        self.tabs3_top = QtGui.QTabWidget()
        self.tabs3_top.addTab(self.parameterBar, 'Parameters')
        self.tabs3_top.tabCloseRequested.connect(self.removeTabPar)
        self.tabs3_top.currentChanged.connect(self.onChange)
        self.tabs3_top.setTabsClosable(True)


        # declare layouts
        main_vbox  = QtGui.QVBoxLayout()

        h1_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        h1_splitter.addWidget(self.tabs_top)
        h1_splitter.addWidget(self.tabs2_top)

        v_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
        v_splitter.addWidget(h1_splitter)
        v_splitter.addWidget(self.textBrowser)

        h_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        h_splitter.addWidget(v_splitter)
        h_splitter.addWidget(self.tabs3_top)

        main_vbox.addWidget(h_splitter)

        self.setLayout(main_vbox)

    def getParameters(self,params=False):
        """Collect the parameters from the user interface"""
        self.simInputs.waveguidePar.setValues(self.waveguideTab.getValues()) # this should not be handled by the GUI but rather by the individual classes. 
        print self.waveguideTab.getValues()
        self.simInputs.mologramPar.setValues(self.mologramTab.get_values()[0])
        self.simInputs.bindersPar[0].setValues(self.mologramTab.get_values()[1]) # analyte parameters
        self.simInputs.bindersPar[1].setValues(self.mologramTab.get_values()[2]) # NSB parameters
        self.simInputs.screenPar.setValues(self.screenTab.get_values())
        self.simInputs.generalSettings.setValues(self.settingsTab.getValues()[0]) # saveSettings are not included anymore. 

        if params == False:
            
            try:

                self.simInputs.waveguidePar.setValues(self.waveguideTab.getValues()) # this should not be handled by the GUI but rather by the individual classes. 
                self.simInputs.mologramPar.setValues(self.mologramTab.get_values()[0])
                self.simInputs.bindersPar[0].setValues(self.mologramTab.get_values()[1]) # analyte parameters
                self.simInputs.bindersPar[1].setValues(self.mologramTab.get_values()[2]) # NSB parameters
                self.simInputs.screenPar.setValues(self.screenTab.get_values())
                self.simInputs.generalSettings.setValues(self.settingsTab.getValues()[0]) # saveSettings are not included anymore. 

            except:
                return False
        else:
            self.simInputs = params

        # check whether the inputs are physically plausible. 
        if self.checkInputs(self.simInputs):

            return self.simInputs
        
        else:

            return False

    def saveParametersToDB(self):
        """Saves all parametes stored in the self.simInput variable into the database."""
        try:
            createDB(self.simInput.settings['databaseFile'], 'log', log_dict())
        except:
            #QtGui.QMessageBox('Please specify a folder for the simulation data')
            self.updateStatus('Simulation NOT started. Please specify a folder location in the Settings Tab!')

        saveDB(self.simInput.settings['databaseFile'], 'log', self.simInput.return_all_parameters())
        ID = loadDB(self.simInput.settings['databaseFile'], 'log', selection = 'ID', condition = 'ORDER BY ID DESC LIMIT 1')
        self.simInput.settings['ID'] = ID
        createDB(self.simInput.settings['databaseFile'], 'results', simulation_dict())
        self.updateStatus('finished simulation (ID: %d) ...' % ID)
        self.ID = ID

        if os.path.getsize(self.simInput.settings['databaseFile']) > 1e9:
            # file is larger than a GB
            self.updateStatus('Database file has reached 1GB, a new file will be created next simulation.')
            defaultValues = loadDB('database/default.db', 'log')
            defaultValuesSQL = createSQLDict(defaultValues)
            if '_' in self.simInput.settings['databaseFile']:
                newName = self.simInput.settings['databaseFile'][:-3].split('_')
                defaultValues['databaseFile'] = newName[0] + '_' + str(int(newName[-1])+1) + '.db'
            else:
                defaultValues['databaseFile'] = self.simInput.settings['databaseFile'][:-3] + '_1.db'
            if 'default.db' in os.listdir('database'): os.remove('database/default.db')
            createDB('database/default.db', 'log', defaultValuesSQL)
            saveDB('database/default.db', 'log', defaultValues)
            self.settingsTab.databaseFile.setText(defaultValues['databaseFile'].split('/')[-1])

    def checkInputs(self, fieldSimulationInputs):
        """
            :params:
            Should be in the variable container class.
        """
        checked = True

        # check waveguide
        waveguideParams = fieldSimulationInputs.waveguidePar.getValues()

        if (waveguideParams['wavelength'] < 400e-9 
                or waveguideParams['wavelength'] > 900e-9):
            self.updateStatus('Warning: Wavelength not in the visible range.')

        if (waveguideParams['couplingEfficiency'] < 0
                or waveguideParams['couplingEfficiency'] > 1):
            self.updateStatus('Error: Coupling efficiency must be between 0 and 100%.')
            checked = False

        if waveguideParams['mode'] != 0:
            self.updateStatus('Error: higher modes than the fundamental not implemented, yet.')

        # todo -> cutoff frequencies
        if waveguideParams['thickness'] > 1e-6:
            self.updateStatus('Error: Incorrect waveguide thickness')
            checked = False


        settings =  fieldSimulationInputs.generalSettings.getValues()
        # check settings
        if '.db' not in settings['databaseFile']:
            self.updateStatus('Error: Wrong database name. File must end with .db')
            checked = False

        if settings['databaseFile'] == 'default.db':
            self.updateStatus('Error: Database name protected. Please change.')
            checked = False

        return checked

    def updateResultsGUI(self, results):
        """Function that updates the GUI with the simulation results and saves it to a dictionary if parameters.settings['saveDatabase'] is true.

        :param results: an output from the run_simulations function in simulation.py (locals() variable)

        """
        self.results = results # save the results of the last simulation. 

        parameters = self.simInputs
        

        if not self.sweepTab.simulationRunning:

            # update image
            self.updateStatus('update plots and results')

            # update analytical calculations
            #self.analyticalTab.updateValues(analytical(parameters), column='analytical')

            try:
                self.plotWidget.newImage(
                        results['screen'],
                        results['I_sca'], 
                        parameters.mologramPar.getValues()['focalPoint']
                    )
            except:
                pass

            # update calculated values
            self.plotWidget.signalIntensity.setText("%.3e" % results.get('I_signal',float('nan')))
            self.plotWidget.bg_int.setText("%.2e / %.2e + %.2e" % (results.get('bkg_mean', float('nan')), results.get('bkg_std', float('nan')), results.get('I_bwg', float('nan'))))
            self.plotWidget.SNR.setText("%.2f" % results['SNR'])
            self.plotWidget.max_pixel.setText("%.2e / %.2e" % (np.max(results.get('I_sca',float('nan'))).real,np.min(results.get('I_sca',float('nan')).real)))

            # update waveguide parameters
            self.waveguideTab.update_waveguide()
            self.waveguideTab.wbg.setText('%.3e' % (results.get('I_bwg',float('nan'))))

            # update mologram parameters
            self.mologramTab.NA.setText(str(results['NA']))
            self.mologramTab.AiryDiskRadius.setText("(%.2e,%.2e)" % (results.get('AiryDiskRadius',(float('nan'),float('nan')))[0], results.get('AiryDiskRadius',(float('nan'),float('nan')))[1]))
            
            # update screen parameters
            self.screenTab.update_values()

            # update analytical tab
            # if self.tabs2_top.indexOf(self.analyticalTab) != 0:
            #     self.analyticalTab.updateValues(results, column='simulation')

            # update mologram plot if open
            if self.tabs2_top.indexOf(self.mologramWidget) == 1:
                self.mologramWidget.newImage(results['mologram'])

        else:
            # This is a quick fix if the Signal contains nan values, needs to be changed!!
            # It should abort the simulation
            if math.isnan(results['I_signal']):
                # repeat
                # if parameters.settings['saveDatabase']:
                #     # does not work! todo!
                #     deleteRow(self.simInput.settings['databaseFile'],'log', condition='ID = \'{}\''.format(self.ID))
                
                self.updateStatus('ATTENTION, angles for scatterers are in the forbidden range!!!')

                
                return

        timeRequired = time.time()-parameters.settings['start']
        self.updateStatus('Simulation done. Time required: %.2fs' % (timeRequired))

        # save the simulation parameters to the database
        if parameters.settings['saveDatabase']:

            self.saveParametersToDB()


        # save the simulation results to the database
        if parameters.settings['saveDatabase']:
            try:
                saveDict = simulation_dict(parameters.settingsSave) # get the parameters that should be stored in the database from the dictionary. 
                saveDict = commonKeys(results, saveDict)
                saveDB(self.simInput.settings['databaseFile'], 'results', saveDict)
            except:
                err = sys.exc_info()[0]
                self.updateStatus('Could not save to database. {}'.format(err))


        if parameters.settings['saveFigure']:
            figureDir = '{}/figures/'.format(os.path.dirname(self.simInput.settings['databaseFile']))
            self.plotWidget.saveFigure(results['screen'], results['I_sca'], results['mologram'], markersize = parameters.particle['particleRadius']*200, figureDir = figureDir)



    @QtCore.pyqtSlot(str)
    def updateStatus(self, status):
        self.textBrowser.append('[%s] %s' % (time.strftime("%X", time.localtime()), status))
        # set text cursor to end of text editor
        cursor = self.textBrowser.textCursor()
        cursor.setPosition(len(self.textBrowser.toPlainText()))
        self.textBrowser.setTextCursor(cursor)


    def eventFilter(self, widget, event):
        if (event.type() == QtCore.QEvent.KeyPress and
            widget is self.textBrowser):
            key = event.key()
            if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
                line = self.textBrowser.toPlainText().split('\n')[-1]
                equation = line.split('>> ')
                if len(equation) > 1:
                    try:
                        globals().update(self.returnParameters())
                        globals().update(self.results)
                    except:
                        pass

                    try:
                        self.textBrowser.append('\t{}'.format(eval(str(equation[1]))))
                    except Exception as err:
                        if 'invalid syntax' in err:
                            try:
                                exec(str(equation[1]))
                                self.textBrowser.append('\tDone')
                            except:
                                self.textBrowser.append('\t{}'.format(err))


            elif key == QtCore.Qt.Key_Up:
                lines = self.textBrowser.toPlainText().split('\n')
                for line in lines:
                    if len(line.split('>> ')) > 1:
                        lastLine = line

                try:
                    self.textBrowser.append(lastLine)
                    cursor = self.textBrowser.textCursor()
                    cursor.setPosition(len(self.textBrowser.toPlainText()))
                    self.textBrowser.setTextCursor(cursor)

                    return True # avoid going up with the cursor
                except:
                    pass

            elif key == QtCore.Qt.Key_Right:
                self.textBrowser.append('>> ')
                cursor = self.textBrowser.textCursor()
                cursor.setPosition(len(self.textBrowser.toPlainText()))
                self.textBrowser.setTextCursor(cursor)

                return True # avoid going up with the cursor

            elif key == QtCore.Qt.Key_Tab:
                line = self.textBrowser.toPlainText().split('\n')[-1]
                if len(line.split('>> ')) > 1:
                    globals().update(self.results)
                    globals().update(self.returnParameters())
                    var = str(line).split(' ')[-1].split('*')[-1].split('/')[-1].split('+')[-1].split('-')[-1]
                    completedVar = ''
                    for key in globals().keys():
                        if var in key:
                            if completedVar == '':
                                completedVar = key
                            else:
                                return True

                    # get text
                    text = self.textBrowser.toPlainText().split('\n')[:-1]
                    text.append(line.replace(var,completedVar))

                    # clear and update text
                    self.textBrowser.clear()
                    for line in text:
                        self.textBrowser.append(str(line))

                    return True



        return QtGui.QWidget.eventFilter(self, widget, event)


    def returnParameters(self):
        """returns a dictionary of the parameters defined in the different tabs of the Main Widget that are saved in the log table in the database. """
        logDict = dict()
        logDict.update(self.waveguideTab.getValues())
        logDict.update(self.mologramTab.get_values()[0])
        logDict.update(self.mologramTab.get_values()[1])
        logDict.update(self.screenTab.get_values())
        settings, settingsSave = self.settingsTab.getValues()
        logDict.update(settings)
        logDict.update(settingsSave)
        logDict.update(self.backgroundTab.getValues())

        # self.textBrowser.append("[%s] update parameters" % time.strftime("%X", time.gmtime()))
        return logDict


    def getParametersAsDicts(self, sweep = False): 
        """returns the simulation parameters as a Parameters object."""           
        parameters = SimulationInputValues()
        parameters.waveguide = self.waveguideTab.getValues()
        parameters.mologram = self.mologramTab.get_values()[0]
        parameters.particle = self.mologramTab.get_values()[1]
        parameters.screen = self.screenTab.get_values()
        if not sweep:
            # sweep has different settings
            settings, settingsSave = self.settingsTab.getValues()
            parameters.settings = settings
            parameters.settingsSave = settingsSave

        else:
            parameters.settings = None
            parameters.settingsSave = None

        return parameters


    def loadExperiment(self):
        """This function opens a window that let the user select an experiment and load the data into the UI."""
        databaseFile = self.settingsTab.getValues()[0]['databaseFile']
        self.loadWidget = LoadWidget(self, databaseFile)
        window = self.loadWidget
        window.show()

    def updateParameters(self):
        self.mologramTab.updateParameters()
        self.waveguideTab.update_waveguide()

    def saveDefaults(self):
        """Saves the current parameters used in the different tabs as defaults"""
        logDict = self.returnParameters()

        # create logDict for SQL (types of values)
        logDictSQL = createSQLDict(logDict)

        try:
            # save to db
            # delete default file if exist
            if 'default.db' in os.listdir('database'): os.remove('database/default.db')
            # create a new a save entries
            createDB('database/default.db', 'log', logDictSQL)
            saveDB('database/default.db', 'log', logDict)
            self.textBrowser.append("[%s] Inputs saved as defaults." % time.strftime("%X", time.gmtime()))

        except:
            self.textBrowser.append("[%s] Defaults could not be saved. Please check inputs." % time.strftime("%X", time.gmtime()))

        # update parameter bar
        self.parameterBar.updateParameters(logDict)


    def onChange(self,i): #changed!
        try:
            self.tabs2_top.setCurrentIndex(i) 
            self.tabs3_top.setCurrentIndex(i) 
        except:
            pass

    def showParameterBar(self):
        if self.parameterBar.isVisible():
            self.parameterBar.hide()
        else:
            self.parameterBar.show()
            logDict = self.returnParameters()
            self.parameterBar.updateParameters(logDict)
            
            
    def showTabBar(self):
        if self.tabs_top.isVisible():
            self.tabs_top.hide()
        else:
            self.tabs_top.show()


    def showLog(self):
        if self.textBrowser.isVisible():
            self.textBrowser.hide()
        else:
            self.textBrowser.show()


    def showProgressBar(self):
        if self.progressbar.isVisible():
            self.progressbar.hide()
        else:
            self.progressbar.show()

    def addTab(self):
        if 'ID' not in dir(self): 
            self.updateStatus('No simulation results to compare')
            return

        self.tabs2_top.setTabText(0, 'Intensity (ID:' + str(self.ID) + ')')
        self.plotWidget = PlotWidget(self)
        self.tabs2_top.insertTab(0, self.plotWidget,'Intensity')

        self.tabs3_top.setTabText(0, 'ID:' + str(self.ID))
        default_values = self.returnParameters()
        self.parameterBar = ParameterWidget(self, default_values)
        self.tabs3_top.insertTab(0, self.parameterBar,'Parameters')
        

    def removeTab(self, index):
        if index == 0:
            self.updateStatus('Main Tab not removable')
            return

        widget = self.tabs2_top.widget(index)
        if widget is not None:
            widget.deleteLater()
        self.tabs2_top.removeTab(index)


    def showTab(self, action):
        if 'Intensity' in action.text():
            if action.isChecked():
                self.tabs2_top.insertTab(self.tabs2_top.count(), self.plotWidget, 'Intensity')
                self.tabs2_top.setCurrentIndex(self.tabs2_top.count()-1)
            else:
                plotWidgetIndex = self.tabs2_top.indexOf(self.plotWidget)
                self.tabs2_top.removeTab(plotWidgetIndex)

        if 'Analytical' in action.text():
            if action.isChecked():
                self.tabs2_top.insertTab(self.tabs2_top.count(), self.analyticalTab, 'Analytical')
                self.tabs2_top.setCurrentIndex(self.tabs2_top.count()-1)
            else:
                analyticalTabIndex = self.tabs2_top.indexOf(self.analyticalTab)
                self.tabs2_top.removeTab(analyticalTabIndex)

        if 'Mologram' in action.text():
            if action.isChecked():
                self.tabs2_top.insertTab(self.tabs2_top.count(), self.mologramWidget, 'Mologram')
                self.tabs2_top.setCurrentIndex(self.tabs2_top.count()-1)
                if 'results' in dir(self):
                    self.mologramWidget.newImage(self.results['mologram'])
                else:
                    pass
            else:
                mologramIndex = self.tabs2_top.indexOf(self.mologramWidget)
                self.tabs2_top.removeTab(mologramIndex)

        if 'Experiment' in action.text():
            if action.isChecked():
                self.tabs_top.insertTab(self.tabs_top.count(), self.experimentTab, 'Experiment')
                self.tabs_top.setCurrentIndex(self.tabs_top.count()-1)

            else:
                experimentTabIndex = self.tabs_top.indexOf(self.experimentTab)
                self.tabs_top.removeTab(experimentTabIndex)

        if 'Sweep' in action.text():
            if action.isChecked():
                self.tabs_top.insertTab(self.tabs_top.count(), self.sweepTab, 'Sweep')
                self.tabs_top.setCurrentIndex(self.tabs_top.count()-1)

            else:
                sweepTabIndex = self.tabs_top.indexOf(self.sweepTab)
                self.tabs_top.removeTab(sweepTabIndex)

    def removeTabPar(self, index):
        if index == 0:
            self.updateStatus('Main Tab not removable')
            return

        widget = self.tabs3_top.widget(index)
        if widget is not None:
            widget.deleteLater()
        self.tabs3_top.removeTab(index)


    def openLoadSettingsWindow(self):
        # copy material
        databaseFile = self.settingsTab.getValues()[0]['databaseFile']
        self.loadWidget = LoadPlotWidget(self, databaseFile)
        window = self.loadWidget
        window.show()


    def openMaterialsWindow(self):
        # copy material
        self.materialsWidget = MaterialsWidget(self)
        window = self.materialsWidget
        window.show()


    def openHelp(self, page = 'index.html'):
        page = './documentation/_build/html/{}'.format(page)
        print(page)

        try:
            # reload if QWbView is defined
            self.documentationTab.load(QUrl(page))
        except:
            # documentation
            self.documentationTab = QWebView()
            self.documentationTab.load(QUrl(page))
            
        self.tabs2_top.insertTab(self.tabs2_top.count(), self.documentationTab, 'Documentation')
        self.tabs2_top.setCurrentIndex(self.tabs2_top.count()-1)

    def generateRST(self):
        if 'ID' not in dir(self): return

        rstfile = rstFile('../database/summaries/Summary ID {}.rst'.format(self.ID))

        rstfile.addTitle('Summary simulation {} on {} at {}'.format(self.ID,self.simInput.settings['date'],self.simInput.settings['startTime']))

        ####################
        ###   Settings   ###
        ####################
        rstfile.addSubtitle('Settings')

        # general settings
        rstfile.addSubsubtitle('General settings')
        settings, settingsSave = self.settingsTab.getValues()
        rstfile.addFieldList(settings)

        # waveguide
        rstfile.addSubsubtitle('Waveguide settings')
        waveguideSettings = self.waveguideTab.getValues()
        rstfile.addFieldList(waveguideSettings)

        # mologram
        rstfile.addSubsubtitle('Mologram settings')
        mologramSettings = dict()
        mologramSettings.update(self.mologramTab.get_values()[0])
        mologramSettings.update(self.mologramTab.get_values()[1])
        rstfile.addFieldList(mologramSettings)

        # screen
        rstfile.addSubsubtitle('Screen settings')
        screenSettings = self.screenTab.get_values()
        rstfile.addFieldList(screenSettings)


        ###################
        ###   Results   ###
        ###################

        rstfile.addSubtitle('Results')

        results = self.analyticalTab.getValues()
        results.insert(0, self.analyticalTab.horHeaders)
        rstfile.addTable(results)

        rstfile.generateFile()
        self.updateStatus('RST file generated')

