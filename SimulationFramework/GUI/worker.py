from PyQt4 import QtCore
from simulations import FieldSimulation


class WorkerObject(QtCore.QThread):
    """Creates a thread that executes a simulation object. 

    :param main_widget: An instance of the class MainWidget
    :param signalStatus: a function object that updates the simulation progress in the MainWidget class
    :param results: a function object that launches the function to store the results. 
        """
    signalStatus = QtCore.pyqtSignal(list)
    results = QtCore.pyqtSignal(dict)

    def __init__(self, main_widget):
        QtCore.QThread.__init__(self)
        self.main_widget = main_widget

    def __del__(self):
        self.wait()

    def startSimulation(self, params=False, sweepScatterers=False):
        """Starts a new simulation by executing the QThread with the start() function.
        
        :param (boolean)sweepScatters: default False, if True, scatterers are swept in order to create a video animation.

        """

        self.params = self.main_widget.getParameters(params)

        self.sweepScatterers = sweepScatterers
        if self.params == False:
            self.main_widget.updateStatus('Simulation NOT started. Please check input parameters.')
        else:
            self.start()

    @QtCore.pyqtSlot(dict)
    def run(self):
        FieldSimulation(self.params, self.signalStatus, self.results, self.sweepScatterers)