# -*- coding: utf-8 -*-


from PyQt4 import QtCore, QtGui
import sys
import numpy as np
sys.path.append('../../')
from auxiliaries.writeLogFile import loadDB
from materials.getData import get_data

from GUI.particleWidget.particle_widget import analyteParameterWidget,NSBParameterWidget


class MologramWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget, default_values):

        # Call parent class constructor
        super(MologramWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(Form, default_values)

    def setupUi(self, Form, default_values):
        
        Form.setObjectName("Form")

        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        # define the mologram parameter widget

        self.mologram_parameters = QtGui.QGroupBox(Form)
        self.mologram_parameters.setTitle('Mologram Parameters')

        molo_params = QtGui.QFormLayout()

        self.focalPoint_lbl = QtGui.QLabel('Focal Point [um]')
        self.focalPoint = QtGui.QLineEdit()
        self.diameter_lbl = QtGui.QLabel('Diameter/Lenght [um]')
        self.diameter = QtGui.QLineEdit()
        self.xshift_lbl = QtGui.QLabel('x offset [um]')
        self.xshift = QtGui.QLineEdit('0')
        self.shape_lbl = QtGui.QLabel('Molo shape')
        self.shape = QtGui.QComboBox()
        self.shape.addItem("disk")
        self.shape.addItem("rectangular")
        self.shape.currentIndexChanged.connect(self.onChange)
        self.aspectRatio_lbl = QtGui.QLabel('Aspect Ratio')
        self.aspectRatio_lbl.setEnabled(False)
        self.aspectRatio = QtGui.QLineEdit('1')
        self.aspectRatio.setEnabled(False)

        self.BraggOffset_lbl = QtGui.QLabel('Bragg Offset [um]')
        self.BraggOffset = QtGui.QLineEdit()

        molo_params.addRow(self.focalPoint_lbl,self.focalPoint)
        molo_params.addRow(self.diameter_lbl,self.diameter)
        molo_params.addRow(self.xshift_lbl,self.xshift)
        molo_params.addRow(self.shape_lbl,self.shape)
        molo_params.addRow(self.aspectRatio_lbl,self.aspectRatio)
        molo_params.addRow(self.BraggOffset_lbl,self.BraggOffset)


        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        # Binder Parameter Tab

        self.tabs_binderparameters = QtGui.QTabWidget()
        
        # analyteWidget
        self.anaWidget = analyteParameterWidget(self.main_widget,default_values)

        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        # NSB binding widget

        self.NSBWidget = NSBParameterWidget(self.main_widget,default_values)

        self.tabs_binderparameters.addTab(self.anaWidget.particleParameters,'Analyte Parameter')
        self.tabs_binderparameters.addTab(self.NSBWidget.particleParameters,'NSB Parameter')

        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        # calculated parameter box

        self.mologram_calculations = QtGui.QGroupBox(Form)
        self.mologram_calculations.setTitle('Calculated Parameters')

        calc_params = QtGui.QFormLayout()

        self.NA_lbl = QtGui.QLabel('Numerical Aperture (NA)')
        self.NA = QtGui.QLabel('')

        calc_params.addRow(self.NA_lbl,self.NA)

        self.AiryDiskRadius_lbl = QtGui.QLabel('Airy Disk Radius (x,y)')
        self.AiryDiskRadius = QtGui.QLabel()

        calc_params.addRow(self.AiryDiskRadius_lbl,self.AiryDiskRadius)

        hbox = QtGui.QHBoxLayout()
        self.setLayout(hbox)

        # box left top
        vbox = QtGui.QVBoxLayout()

        self.mologram_parameters.setLayout(molo_params)
        
        self.mologram_calculations.setLayout(calc_params)
        vbox.addWidget(self.mologram_parameters)
        vbox.addWidget(self.tabs_binderparameters)
        vbox.addWidget(self.mologram_calculations)
        vbox.addStretch()

        hbox.addLayout(vbox)

        self.retranslateUi(Form,default_values)                                        


    def updateParameters(self):
        # extern function

        self.anaWidget.updateParameters()

 
    def retranslateUi(self, Form, values):

        # mologram parameters
        self.focalPoint.setText(str(values['focalPoint']*1e6))
        self.diameter.setText(str(values['diameter']*1e6))
        self.BraggOffset.setText(str(values['braggOffset']*1e6))

        # analyte parameters
        self.anaWidget.retranslateUi(values)
        self.NSBWidget.retranslateUi(values)

    def get_values(self):
        """
        returns a dictionary of the mologram parameters that are currently displayed in the UI.

        To be rewritten...
        """
        analytePar = self.anaWidget.getValues()
        NSBPar = self.NSBWidget.getValues()

        try:
            # should allow to specify a concentration and a K_D value.
            # this all belongs to the analyte widget
            

            mologramPar = {
                'focalPoint' : float(self.focalPoint.text())*1e-6,
                'diameter' : float(self.diameter.text())*1e-6,
                'braggOffset' : float(self.BraggOffset.text())*1e-6,
                'shape': str(self.shape.currentText()),
                'xshift': float(str(self.xshift.text()))*1e-6,
                'aspectRatio': float(str(self.aspectRatio.text()))
            }

            # Analyte parameter 
            

            return mologramPar, analytePar,NSBPar
            
        except:
            return None
    
    def setValues(self,values):
        """loads the values from a dictionary into the UI.

        :param values: dictionary with the key value pairs of the parameters.

        To do: Particle number update.
        """

        # update the mologram parameters
        self.focalPoint.setText(str(values['focalPoint']*1e6))
        self.diameter.setText(str(values['diameter']*1e6))
        self.xshift.setText(str(values['xshift']*1e6))
        self.BraggOffset.setText(str(values['braggOffset']*1e6))

        # update the analyte parameters
        self.anaWidget.setValues(values)
        # update the NSB parameters
        self.NSBWidget.setValues(values)



    def onChange(self):

        self.aspectRatio.setEnabled(str(self.shape.currentText()) == 'rectangular')
        self.aspectRatio_lbl.setEnabled(str(self.shape.currentText()) == 'rectangular')

if __name__ == "__main__":
    
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = WaveguideTab()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

