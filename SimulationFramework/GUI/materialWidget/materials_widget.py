# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'materials.ui'
#
# Created: Fri Dec  9 17:50:35 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import pyqtgraph as pg
import os
import sys
import numpy as np
from input_dialog import AddMaterial
sys.path.append('../../')
from materials.getData import get_range, get_data
import auxiliaries.constants as constants
from input_dialog import AddMaterial, CopyMaterial
from auxiliaries.writeLogFile import loadDB, deleteRow
from dipole.dipole import scatteringCrossSection

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


pg.setConfigOption('background','w')
pg.setConfigOption('foreground','k')


class MaterialsWidget(QtGui.QWidget):
    def __init__(self, MainWindow):
        super(MaterialsWidget, self).__init__()

        Form = QtGui.QWidget()

        # Initialize member variables       
        self.MainWindow = MainWindow

        # Call child constructor
        self.setupUi(Form)

    def setupUi(self, Form):

        Form.setObjectName(_fromUtf8("Materials"))
        Form.resize(1007, 765)

        self.centralwidget = QtGui.QWidget(Form)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.properties = QtGui.QGroupBox(self.centralwidget)
        self.properties.setGeometry(QtCore.QRect(30, 520, 941, 211))
        self.properties.setMinimumSize(QtCore.QSize(941, 241))
        self.properties.setObjectName(_fromUtf8("properties"))
        self.label_5 = QtGui.QLabel(self.properties)
        self.label_5.setGeometry(QtCore.QRect(530, 60, 136, 17))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_6 = QtGui.QLabel(self.properties)
        self.label_6.setGeometry(QtCore.QRect(530, 90, 171, 17))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(self.properties)
        self.label_7.setGeometry(QtCore.QRect(530, 120, 171, 17))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_8 = QtGui.QLabel(self.properties)
        self.label_8.setGeometry(QtCore.QRect(530, 150, 171, 17))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.label_name = QtGui.QLabel(self.properties)
        self.label_name.setGeometry(QtCore.QRect(30, 30, 66, 17))
        self.label_name.setObjectName(_fromUtf8("label_name"))
        self.name = QtGui.QLabel(self.properties)
        self.name.setGeometry(QtCore.QRect(200, 30, 270, 17))
        self.name.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.name.setText(_fromUtf8(""))
        self.name.setObjectName(_fromUtf8("name"))
        self.label_type = QtGui.QLabel(self.properties)
        self.label_type.setGeometry(QtCore.QRect(30, 90, 66, 17))
        self.label_type.setObjectName(_fromUtf8("label_type"))
        self.type = QtGui.QLabel(self.properties)
        self.type.setGeometry(QtCore.QRect(200, 90, 270, 17))
        self.type.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.type.setText(_fromUtf8(""))
        self.type.setObjectName(_fromUtf8("type"))
        self.label_material = QtGui.QLabel(self.properties)
        self.label_material.setGeometry(QtCore.QRect(30, 60, 66, 17))
        self.label_material.setObjectName(_fromUtf8("label_material"))
        self.material = QtGui.QLabel(self.properties)
        self.material.setGeometry(QtCore.QRect(200, 60, 270, 17))
        self.material.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.material.setText(_fromUtf8(""))
        self.label_size = QtGui.QLabel(self.properties)
        self.label_size.setGeometry(QtCore.QRect(30, 120, 66, 17))
        self.label_size.setObjectName(_fromUtf8("label_size"))
        self.material.setObjectName(_fromUtf8("material"))
        self.size = QtGui.QLabel(self.properties)
        self.size.setGeometry(QtCore.QRect(200, 120, 270, 17))
        self.size.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.size.setText(_fromUtf8(""))
        self.size.setObjectName(_fromUtf8("size"))
        self.label_mass = QtGui.QLabel(self.properties)
        self.label_mass.setGeometry(QtCore.QRect(30, 150, 121, 17))
        self.label_mass.setObjectName(_fromUtf8("label_mass"))
        self.mass = QtGui.QLabel(self.properties)
        self.mass.setGeometry(QtCore.QRect(200, 150, 270, 17))
        self.mass.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.mass.setText(_fromUtf8(""))
        self.mass.setObjectName(_fromUtf8("mass"))
        self.label_file = QtGui.QLabel(self.properties)
        self.label_file.setGeometry(QtCore.QRect(30, 180, 121, 17))
        self.label_file.setObjectName(_fromUtf8("label_mass"))
        self.file = QtGui.QLabel(self.properties)
        self.file.setGeometry(QtCore.QRect(200, 180, 470, 17))
        self.file.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.file.setText(_fromUtf8(""))
        self.file.setObjectName(_fromUtf8("mass"))
        self.label_wavelength = QtGui.QLabel(self.properties)
        self.label_wavelength.setGeometry(QtCore.QRect(530, 30, 101, 17))
        self.label_wavelength.setObjectName(_fromUtf8("label_wavelength"))
        self.wavelength = QtGui.QLineEdit(self.properties)
        self.wavelength.setGeometry(QtCore.QRect(705, 25, 113, 27))
        self.wavelength.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.wavelength.setObjectName(_fromUtf8("wavelength"))
        self.wavelength.returnPressed.connect(self.update_properties)
        self.label_wavelength_unit = QtGui.QLabel(self.properties)
        self.label_wavelength_unit.setGeometry(QtCore.QRect(825, 30, 66, 17))
        self.label_wavelength_unit.setObjectName(_fromUtf8("label_wavelength_unit"))
        self.extinctionCoefficient = QtGui.QLabel(self.properties)
        self.extinctionCoefficient.setGeometry(QtCore.QRect(705, 90, 113, 17))
        self.extinctionCoefficient.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.extinctionCoefficient.setText(_fromUtf8(""))
        self.extinctionCoefficient.setObjectName(_fromUtf8("extinctionCoefficient"))
        self.refractiveIndexProp = QtGui.QLabel(self.properties)
        self.refractiveIndexProp.setGeometry(QtCore.QRect(705, 60, 113, 17))
        self.refractiveIndexProp.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.refractiveIndexProp.setText(_fromUtf8(""))
        self.refractiveIndexProp.setObjectName(_fromUtf8("refractiveIndex"))
        self.relativePermittivity = QtGui.QLabel(self.properties)
        self.relativePermittivity.setGeometry(QtCore.QRect(705, 150, 113, 17))
        self.relativePermittivity.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.relativePermittivity.setText(_fromUtf8(""))
        self.relativePermittivity.setObjectName(_fromUtf8("relativePermittivity"))
        self.absorptionCoefficient = QtGui.QLabel(self.properties)
        self.absorptionCoefficient.setGeometry(QtCore.QRect(705, 120, 113, 17))
        self.absorptionCoefficient.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.absorptionCoefficient.setText(_fromUtf8(""))
        self.absorptionCoefficient.setObjectName(_fromUtf8("absorptionCoefficient"))
        self.doneButton = QtGui.QPushButton(self.properties)
        self.doneButton.setGeometry(QtCore.QRect(800, 200, 140, 27))
        self.doneButton.setObjectName(_fromUtf8("doneButton"))
        self.doneButton.clicked.connect(self.close)

        self.groupBox_DielectricFunction = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_DielectricFunction.setGeometry(QtCore.QRect(525, 30, 481, 481))
        self.groupBox_DielectricFunction.setMinimumSize(QtCore.QSize(481, 481))
        self.groupBox_DielectricFunction.setObjectName(_fromUtf8("groupBox_DielectricFunction"))
        self.dielectricFunction = pg.PlotWidget(self.groupBox_DielectricFunction)
        self.dielectricFunction.setGeometry(QtCore.QRect(25, 40, 431, 391))
        self.dielectricFunction.setObjectName(_fromUtf8("dielectricFunction"))
        self.dielectricFunction.setLabel('bottom', 'Wavelength', units='m')
        self.dielectricFunction.setLabel('left', 'Refractive Index')
        self.refractiveIndexReal = QtGui.QCheckBox(self.groupBox_DielectricFunction)
        self.refractiveIndexReal.setGeometry(QtCore.QRect(40, 445, 80, 22))
        self.refractiveIndexReal.setObjectName(_fromUtf8("refractiveIndexReal"))
        self.refractiveIndexReal.setChecked(True)
        self.refractiveIndexReal.stateChanged.connect(self.update_plot)
        self.refractiveIndexImag = QtGui.QCheckBox(self.groupBox_DielectricFunction)
        self.refractiveIndexImag.setGeometry(QtCore.QRect(110, 445, 97, 22))
        self.refractiveIndexImag.setObjectName(_fromUtf8("refractiveIndexImag"))
        self.refractiveIndexImag.stateChanged.connect(self.update_plot)
        self.refractiveIndexLogX = QtGui.QCheckBox(self.groupBox_DielectricFunction)
        self.refractiveIndexLogX.setGeometry(QtCore.QRect(180, 445, 97, 22))
        self.refractiveIndexLogX.setObjectName(_fromUtf8("refractiveIndexLogX"))
        self.refractiveIndexLogX.stateChanged.connect(self.update_plot)
        self.refractiveIndexLogY = QtGui.QCheckBox(self.groupBox_DielectricFunction)
        self.refractiveIndexLogY.setGeometry(QtCore.QRect(270, 445, 97, 22))
        self.refractiveIndexLogY.setObjectName(_fromUtf8("refractiveIndexLogY"))
        self.refractiveIndexLogY.stateChanged.connect(self.update_plot)
        self.checkBox_eV = QtGui.QCheckBox(self.groupBox_DielectricFunction)
        self.checkBox_eV.setGeometry(QtCore.QRect(360, 445, 97, 22))
        self.checkBox_eV.setObjectName(_fromUtf8("checkBox_eV"))
        self.checkBox_eV.stateChanged.connect(self.update_plot)
        self.label_resolution = QtGui.QLabel(self.groupBox_DielectricFunction)
        self.label_resolution.setGeometry(QtCore.QRect(320, 15, 91, 17))
        self.label_resolution.setObjectName(_fromUtf8("label_resolution"))
        self.resolution_up = QtGui.QPushButton(self.groupBox_DielectricFunction)
        self.resolution_up.setGeometry(QtCore.QRect(405, 13, 21, 21))
        self.resolution_up.setObjectName(_fromUtf8("resolution_up"))
        self.resolution_up.clicked.connect(lambda: self.update_resolution(self.resolution_up))
        self.resolution_down = QtGui.QPushButton(self.groupBox_DielectricFunction)
        self.resolution_down.setGeometry(QtCore.QRect(430, 13, 20, 21))
        self.resolution_down.setObjectName(_fromUtf8("resolution_down"))
        self.resolution_down.clicked.connect(lambda: self.update_resolution(self.resolution_down))

        self.groupBox_Materials = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_Materials.setGeometry(QtCore.QRect(30, 30, 481, 481))
        self.groupBox_Materials.setMinimumSize(QtCore.QSize(481, 481))
        self.groupBox_Materials.setObjectName(_fromUtf8("groupBox_Materials"))
        self.buttonAdd = QtGui.QPushButton(self.groupBox_Materials)
        self.buttonAdd.setGeometry(QtCore.QRect(30, 405, 140, 27))
        self.buttonAdd.setObjectName(_fromUtf8("buttonAdd"))
        self.buttonAdd.clicked.connect(self.addMaterial)
        self.buttonEdit = QtGui.QPushButton(self.groupBox_Materials)
        self.buttonEdit.setGeometry(QtCore.QRect(175, 405, 140, 27))
        self.buttonEdit.setObjectName(_fromUtf8("buttonEdit"))
        self.buttonEdit.setEnabled(False)

        self.comboBoxShow = QtGui.QComboBox(self.groupBox_Materials)
        self.comboBoxShow.setGeometry(QtCore.QRect(310, 12, 151, 25))
        self.comboBoxShow.setObjectName(_fromUtf8("comboBoxShow"))
        self.comboBoxShow.addItem(_fromUtf8(""))
        self.comboBoxShow.addItem(_fromUtf8(""))
        self.comboBoxShow.addItem(_fromUtf8(""))
        self.comboBoxShow.addItem(_fromUtf8(""))
        self.comboBoxShow.addItem(_fromUtf8(""))
        self.comboBoxShow.currentIndexChanged.connect(self.selectionChange)
        self.comboBoxCopy = QtGui.QComboBox(self.groupBox_Materials)
        self.comboBoxCopy.setGeometry(QtCore.QRect(175, 440, 285, 27))
        self.comboBoxCopy.setObjectName(_fromUtf8("comboBoxCopy"))
        self.comboBoxCopy.addItem(_fromUtf8(""))
        self.comboBoxCopy.addItem(_fromUtf8(""))
        self.comboBoxCopy.addItem(_fromUtf8(""))
        self.comboBoxCopy.addItem(_fromUtf8(""))
        self.buttonCopy = QtGui.QPushButton(self.groupBox_Materials)
        self.buttonCopy.setGeometry(QtCore.QRect(30, 440, 140, 27))
        self.buttonCopy.setObjectName(_fromUtf8("buttonCopy"))
        self.buttonCopy.clicked.connect(self.copyMaterial)
        self.materials = QtGui.QTreeWidget(self.groupBox_Materials)
        self.materials.setGeometry(QtCore.QRect(30, 40, 431, 361))
        self.materials.setProperty("showDropIndicator", True)
        self.materials.setRootIsDecorated(True)
        self.materials.setObjectName(_fromUtf8("materials"))
        self.materials.headerItem().setText(0, _fromUtf8("1"))
        self.materials.header().setVisible(False)
        self.updateList()
        self.materials.connect(self.materials, QtCore.SIGNAL("itemClicked(QTreeWidgetItem *, int)"), self.materialSelected)

        self.buttonDelete = QtGui.QPushButton(self.groupBox_Materials)
        self.buttonDelete.setGeometry(QtCore.QRect(320, 405, 140, 27))
        self.buttonDelete.setObjectName(_fromUtf8("buttonDelete"))
        self.buttonDelete.clicked.connect(self.deleteMaterial)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.groupBox_Materials)
        hbox.addWidget(self.groupBox_DielectricFunction)

        vbox.addLayout(hbox)
        vbox.addWidget(self.properties)

        self.setWindowTitle('Materials')

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Form", None))
        self.properties.setTitle(_translate("Form", "Properties", None))
        self.label_5.setText(_translate("Form", "Refractive Index", None))
        self.label_6.setText(_translate("Form", "Extinction Coefficient", None))
        self.label_7.setText(_translate("Form", "Absorption Coefficient", None))
        self.label_8.setText(_translate("Form", "Relative Permittivity", None))
        self.label_type.setText(_translate("Form", "Type", None))
        self.label_mass.setText(_translate("Form", "Molecular Mass", None))
        self.label_name.setText(_translate("Form", "Name", None))
        self.label_material.setText(_translate("Form", "Material", None))
        self.label_size.setText(_translate("Form", "Diameter", None))
        self.label_file.setText(_translate("Form", "File", None))
        self.label_wavelength.setText(_translate("Form", "Wavelength", None))
        self.wavelength.setText(_translate("Form", "632.8", None))
        self.label_wavelength_unit.setText(_translate("Form", "nm", None))
        self.groupBox_DielectricFunction.setTitle(_translate("Form", "Dielectric Function", None))
        self.refractiveIndexReal.setText(_translate("Form", "n", None))
        self.refractiveIndexImag.setText(_translate("Form", "k", None))
        self.refractiveIndexLogX.setText(_translate("Form", "logX", None))
        self.refractiveIndexLogY.setText(_translate("Form", "logY", None))
        self.checkBox_eV.setText(_translate("Form", "eV", None))
        self.label_resolution.setText(_translate("Form", "Resolution:", None))
        self.resolution_up.setText(_translate("Form", "+", None))
        self.resolution_down.setText(_translate("Form", "-", None))
        self.groupBox_Materials.setTitle(_translate("Form", "Materials", None))
        self.buttonAdd.setText(_translate("Form", "Add", None))
        self.buttonEdit.setText(_translate("Form", "Edit", None))
        self.buttonDelete.setText(_translate("Form", "Delete", None))
        self.doneButton.setText(_translate("Form", "Done", None))
        self.comboBoxShow.setItemText(0, _translate("Form", "All", None))
        self.comboBoxShow.setItemText(1, _translate("Form", "Particles", None))
        self.comboBoxShow.setItemText(2, _translate("Form", "Cover Medium", None))
        self.comboBoxShow.setItemText(3, _translate("Form", "Film Medium", None))
        self.comboBoxShow.setItemText(4, _translate("Form", "Substrate Medium", None))
        self.comboBoxCopy.setItemText(0, _translate("Form", "Particles", None))
        self.comboBoxCopy.setItemText(1, _translate("Form", "Cover Medium", None))
        self.comboBoxCopy.setItemText(2, _translate("Form", "Film Medium", None))
        self.comboBoxCopy.setItemText(3, _translate("Form", "Substrate Medium", None))
        self.buttonCopy.setText(_translate("Form", "Copy to", None))

    def materialSelected (self, item):
        if str(self.comboBoxShow.currentText()) == 'All':
            # find material in the database
            path = []
            while item is not None:
                path.append(str(item.text(0)))
                item = item.parent()
            file = '/database/' + '/'.join(reversed(path)) + '.yml'
            self.name.setText(path[0])
            self.type.setText(path[-1])
            self.material.setText('')
            self.size.setText('')
            self.mass.setText('')

        else:
            material = loadDB('materials/materials.db',
                        str(self.comboBoxShow.currentText()),
                        condition = "where name = '" + str(item.text(0)) + "'")

            if material['file'] != '':
                file = '' + material['file']
            else:
                file = ''
            self.name.setText(material['name'])
            self.type.setText(str(self.comboBoxShow.currentText()))
            if 'mass' in dir(material):
                if material['mass'] != '':
                    self.mass.setText(str(material['mass']*1e-3) + " kDa")
                else:
                    self.mass.setText('')
                self.material.setText(material['material'])
                if material['radius'] != '':
                    self.size.setText(str(material['radius']*2e9) + " nm")

            else:
                self.size.setText('')


        self.file.setText(file)

        if os.path.isfile(file):
            # if file exists, read
            wavelengthRange = get_range(file)

            self.points = 500   # reload resolution
            wl = np.linspace(wavelengthRange[0], wavelengthRange[1], self.points)
            refractiveIndex = np.array(get_data('materials' + file, wl))

            # store parameters temporarily
            self.wl = wl
            self.refractiveIndex = refractiveIndex

            # prepare plots
            colormap = [(0,0,0),(255,0,0),(0,255,0),(0,0,255),(0,255,255)]
            colormap = colormap[::-1]

            self.dielectricFunction.clear()
            # checked boxes
            # set x axis
            if self.checkBox_eV.isChecked():
                xAxis = constants.h*constants.c/(wl[::-1]*1e-6*constants.q_e)
                self.dielectricFunction.setLabel('bottom', 'Photon Energy', units='eV')
            else:
                xAxis = wl*1e-6

            self.dielectricFunction.setLogMode(
                self.refractiveIndexLogX.isChecked(),
                self.refractiveIndexLogY.isChecked()
            )
            if self.refractiveIndexReal.isChecked():
                self.dielectricFunction.plot(xAxis, refractiveIndex.real, pen = {'color' : colormap.pop()})

            if self.refractiveIndexImag.isChecked():
                self.dielectricFunction.plot(xAxis, refractiveIndex.imag, pen = {'color' : colormap.pop()})

        else:
            self.dielectricFunction.clear()

        self.update_properties()


    def update_properties(self):
        file = self.file.text()

        # find properties
        if os.path.isfile(file):
            self.label_7.setText('')
            self.absorptionCoefficient.setText('')
            
            self.refractiveIndex = np.array(get_data('materials' + file, self.wl))

            # single values for given wavelength
            # complex refractive index
            if type(self.wavelength.text()) != float:
                self.wavelength.setText('632.8')
            try:
                n = get_data('materials' + file, np.array([float(self.wavelength.text())*1e-3]))[0]

                self.refractiveIndexProp.setText("%.2f" % n.real)
                self.extinctionCoefficient.setText("%.2f" % n.imag)

                # relative permittivity
                eps_r = n**2
                self.relativePermittivity.setText("%.2f + i%.2f" % (eps_r.real, eps_r.imag))
            except ValueError:
                self.refractiveIndexProp.setText("Change wavelength")
                self.extinctionCoefficient.setText("")
                self.relativePermittivity.setText("")

            if str(self.comboBoxShow.currentText()) == 'Particles' \
                    and str(self.size.text()) != '':
                self.label_7.setText('Scattering Efficiency')
                Q_sca = scatteringCrossSection(
                    2*np.pi/(float(self.wavelength.text())*1e-9),
                    float(str(self.size.text())[:-3])*0.5e-9,
                    n, 1.33)
                self.absorptionCoefficient.setText("%.3e" % Q_sca.real)

        else:
            self.refractiveIndexProp.setText('')
            self.extinctionCoefficient.setText('')

            self.relativePermittivity.setText('')


    def update_plot(self, resolution=0):
        if resolution != 0:
            self.points = max(self.points+resolution,100)
            self.wl = np.linspace(np.min(self.wl), np.max(self.wl), self.points)
            self.refractiveIndex = np.array(get_data(self.file.text(), self.wl))

        if self.checkBox_eV.isChecked():
            self.dielectricFunction.setLabel('bottom', 'Photon Energy', units='eV')
            if 'wl' in dir(self):
                xAxis = constants.h*constants.c/(self.wl[::-1]*1e-6*constants.q_e)
        else:
            self.dielectricFunction.setLabel('bottom', 'Wavelength', units='m')
            if 'wl' in dir(self):
                xAxis = self.wl*1e-6

        self.dielectricFunction.setLogMode(
            self.refractiveIndexLogX.isChecked(),
            self.refractiveIndexLogY.isChecked()
        )
        if 'xAxis' in locals():
            self.dielectricFunction.clear()

            # plots
            colormap = [(0,0,0),(255,0,0),(0,255,0),(0,0,255),(0,255,255)]
            colormap = colormap[::-1]

            if self.refractiveIndexReal.isChecked():
                self.dielectricFunction.plot(xAxis, self.refractiveIndex.real, pen = {'color' : colormap.pop()})

            if self.refractiveIndexImag.isChecked():
                self.dielectricFunction.plot(xAxis, self.refractiveIndex.imag, pen = {'color' : colormap.pop()})

    def update_resolution(self, button):
        if not 'points' in dir(self):
            pass

        else:
            self.update_plot(resolution = float(button.text() + "500"))

    def addMaterial(self):
        # copy material
        self.add = AddMaterial(self)
        window = self.add
        window.show()

    def copyMaterial(self):
        # copy material
        self.copy = CopyMaterial(self)
        window = self.copy
        window.show()

    def deleteMaterial(self):
        # delete entry
        if self.comboBoxShow.currentText() == 'All':
            self.openMaterialsWindow()
            msg = QtGui.QMessageBox()
            msg.setText("Warning: Material from database cannot be deleted.")
            msg.setWindowTitle("Warning")
            msg.setIcon(QtGui.QMessageBox.Warning)
            msg.exec_()
            return

        else:
            deleteRow('materials/materials.db',
                        str(self.comboBoxShow.currentText()),
                        'name = \'' + str(self.name.text()) + '\'')
            self.updateList(str(self.comboBoxShow.currentText()))

    def center(self):
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())

    def getValues(self):
        """
        returns a dictionary of the waveguide parameters
        """
        materialPar = {
            'name' : self.name.text(),
            'type' : self.comboBoxCopy.currentText(),
            'file' : self.file.text(),
        }

        return materialPar

    def selectionChange(self):
        self.updateList(str(self.comboBoxShow.currentText()))


    def updateList(self, filter = None):
        self.materials.clear()

        if filter == None or filter == 'All':
            # load all materials from database
            folder = 'materials/database'
            topLevelCount = 0
            for shelf in os.listdir(folder):
                if '.' in shelf: continue
                item_0 = QtGui.QTreeWidgetItem(self.materials)
                self.materials.topLevelItem(topLevelCount).setText(0, _translate("Form", shelf, None))
                # make top level bold
                font = QtGui.QFont()
                font.setBold(True)
                font.setWeight(75)
                item_0.setFont(0, font)

                firstLevelCount = 0
                for book in sorted(os.listdir(folder + '/' + shelf)):
                    item_1 = QtGui.QTreeWidgetItem(item_0)
                    self.materials.topLevelItem(topLevelCount).child(firstLevelCount).setText(0, _translate("Form", book.replace('.yml',''), None))

                    if '.' in book:
                        firstLevelCount += 1
                        continue

                    secondLevelCount = 0
                    for page in sorted(os.listdir(folder + '/' + shelf + '/' + book)):
                        item_2 = QtGui.QTreeWidgetItem(item_1)
                        self.materials.topLevelItem(topLevelCount).child(firstLevelCount).child(secondLevelCount).setText(0, _translate("Form", page.replace('.yml',''), None))
                        # increment secondLevelCount
                        if '.' in page:
                            secondLevelCount += 1
                            continue

                        thirdLevelCount = 0
                        for word in sorted(os.listdir(folder + '/' + shelf + '/' + book + '/' + page)):
                            item_3 = QtGui.QTreeWidgetItem(item_2)
                            self.materials.topLevelItem(topLevelCount).child(firstLevelCount).child(secondLevelCount).child(thirdLevelCount).setText(0, _translate("Form", word.replace('.yml',''), None))
                            # increment secondLevelCount
                            thirdLevelCount += 1

                        secondLevelCount += 1

                    # increment firstLevelCount
                    firstLevelCount += 1

                # increment topLevelCount
                topLevelCount += 1

        else:
            try:
                listMedia = loadDB('materials/materials.db',
                            str(self.comboBoxShow.currentText()))
                if type(listMedia) != list:
                    listMedia = [listMedia]
                topLevelCount = 0
                for medium in listMedia:
                    item_0 = QtGui.QTreeWidgetItem(self.materials)
                    self.materials.topLevelItem(topLevelCount).setText(0, _translate("Form", medium['name'], None))
                    topLevelCount += 1
            except:
                pass


    def closeEvent(self, event):
        self.MainWindow.updateParameters()
