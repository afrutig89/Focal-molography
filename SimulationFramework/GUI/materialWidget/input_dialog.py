# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'inputDialog.ui'
#
# Created: Mon Dec 12 16:10:31 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
from numpy import pi
import os
import sys
sys.path.append('../../')
from variables import particleDB, mediumDB
from auxiliaries.writeLogFile import createDB, saveDB
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class AddMaterial(QtGui.QWidget):
    def __init__(self, materialWidget):
        super(AddMaterial, self).__init__()

        Form = QtGui.QWidget()

        # Initialize member variables
        self.materialWidget = materialWidget

        # Call child constructor
        self.setupUi(Form)

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Add a Material"))
        Form.resize(515, 264)
        self.input = QtGui.QGroupBox(Form)
        self.input.setGeometry(QtCore.QRect(0, 515, 0, 264))
        self.input.setMinimumSize(QtCore.QSize(515, 264))
        self.input.setObjectName(_fromUtf8("input"))

        self.label_name = QtGui.QLabel(self.input)
        self.label_name.setGeometry(QtCore.QRect(30, 30, 66, 17))
        self.label_name.setObjectName(_fromUtf8("label_name"))
        self.label_type = QtGui.QLabel(self.input)
        self.label_type.setGeometry(QtCore.QRect(30, 90, 66, 17))
        self.label_type.setObjectName(_fromUtf8("label_type"))
        self.label_material = QtGui.QLabel(self.input)
        self.label_material.setGeometry(QtCore.QRect(30, 60, 66, 17))
        self.label_material.setObjectName(_fromUtf8("label_material"))
        self.label_coefficients = QtGui.QLabel(self.input)
        self.label_coefficients.setGeometry(QtCore.QRect(30, 150, 141, 17))
        self.label_coefficients.setObjectName(_fromUtf8("label_coefficients"))
        self.label_range = QtGui.QLabel(self.input)
        self.label_range.setGeometry(QtCore.QRect(30, 120, 141, 17))
        self.label_range.setObjectName(_fromUtf8("label_range"))
        self.label_formula = QtGui.QLabel(self.input)
        self.label_formula.setGeometry(QtCore.QRect(30, 180, 121, 17))
        self.label_formula.setObjectName(_fromUtf8("label_formula"))
        self.name = QtGui.QLineEdit(self.input)
        self.name.setGeometry(QtCore.QRect(210, 28, 270, 21))
        self.name.setObjectName(_fromUtf8("name"))
        self.material = QtGui.QLineEdit(self.input)
        self.material.setGeometry(QtCore.QRect(210, 58, 270, 21))
        self.material.setObjectName(_fromUtf8("material"))
        self.type = QtGui.QComboBox(self.input)
        self.type.setGeometry(QtCore.QRect(210, 88, 270, 21))
        self.type.setObjectName(_fromUtf8("type"))
        for i in range(4):
            self.type.addItem(_fromUtf8(""))
        self.range = QtGui.QLineEdit(self.input)
        self.range.setGeometry(QtCore.QRect(210, 118, 270, 21))
        self.range.setObjectName(_fromUtf8("range"))
        self.calcDiameterButton = QtGui.QPushButton(self.input)
        self.calcDiameterButton.setGeometry(QtCore.QRect(480, 118, 20, 20))
        self.calcDiameterButton.setObjectName(_fromUtf8("buttonAdd"))
        self.calcDiameterButton.clicked.connect(self.calcDiameter)
        self.calcDiameterButton.setVisible(False)
        self.coefficients = QtGui.QLineEdit(self.input)
        self.coefficients.setGeometry(QtCore.QRect(210, 148, 270, 21))
        self.coefficients.setObjectName(_fromUtf8("coefficients"))
        self.formula = QtGui.QComboBox(self.input)
        self.formula.setGeometry(QtCore.QRect(210, 178, 270, 21))
        self.formula.setObjectName(_fromUtf8("formula"))
        for i in np.arange(12):
            self.formula.addItem(_fromUtf8(""))
            self.formula.setItemText(i, _translate("Form", "Formula " + str(i+1), None))

        self.formula.currentIndexChanged.connect(self.formulaChange)
        self.saveButton = QtGui.QPushButton(self.input)
        self.saveButton.setGeometry(QtCore.QRect(290, 220, 91, 27))
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.saveButton.clicked.connect(self.saveVariables)
        self.closeButton = QtGui.QPushButton(self.input)
        self.closeButton.setGeometry(QtCore.QRect(390, 220, 91, 27))
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.closeButton.clicked.connect(self.close)

        r,g,b,a=self.palette().base().color().getRgbF()

        self.figure = Figure(edgecolor=(r,g,b), facecolor=(r,g,b))
        self.figure.set_size_inches(500/80., 80/80.)
        self.canvas = FigureCanvas(self.figure)


        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        vbox.addWidget(self.input)
        vbox.addWidget(self.canvas, alignment = QtCore.Qt.AlignHCenter)

        self.formulaChange()
        self.setWindowTitle('Add Material')

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Form", None))
        self.label_name.setText(_translate("Form", "Name", None))
        self.label_type.setText(_translate("Form", "Type", None))
        self.label_material.setText(_translate("Form", "Material", None))
        self.label_coefficients.setText(_translate("Form", "Coefficients", None))
        self.label_range.setText(_translate("Form", "Range (nm)", None))
        self.label_formula.setText(_translate("Form", "Formula", None))
        self.saveButton.setText(_translate("Form", "Save", None))
        self.closeButton.setText(_translate("Form", "Cancel", None))
        self.calcDiameterButton.setText(_translate("MainWindow", "=", None))
        self.calcDiameterButton.setToolTip('Feijter\'s formula for water<br>(mass required; proteins as spheres)')
        self.type.setItemText(0, _translate("Form", "Particles", None))
        self.type.setItemText(1, _translate("Form", "Cover Medium", None))
        self.type.setItemText(2, _translate("Form", "Film Medium", None))
        self.type.setItemText(3, _translate("Form", "Substrate Medium", None))


    def formulaChange(self):
        self.calcDiameterButton.setVisible(False)
        self.label_coefficients.setText(_translate("Form", "Coefficients", None))
        self.label_range.setText(_translate("Form", "Range [nm]", None))

        if str(self.formula.currentText()) == 'Formula 1':
            # mathText=r'$X_k = \sum_{n=0}^{N-1} x_n . e^{\frac{-i2\pi kn}{N}}$'
            mathText = r'${n} ^ {2} -1= {C}_{1} + \sum_{n=2}^{N} \frac{{B}_{n} {\lambda} ^ {2}}{{\lambda} ^ {2} - {C}_{n+1}^{2}}$'

        elif str(self.formula.currentText()) == 'Formula 2':
            mathText = r'${n} ^ {2} -1= {C}_{1} + \sum_{n=2}^{N} \frac{{B}_{n} {\lambda} ^ {2}}{{\lambda} ^ {2} - {C}_{n+1}}$'

        elif str(self.formula.currentText()) == 'Formula 3':
            mathText = r'${n } ^ {2} = {C }_{1} + \sum_{n=2}^{N} {C }_{n} {\lambda } ^ {{C}_{n+1}}$'

        elif str(self.formula.currentText()) == 'Formula 4':
            mathText = r'${n } ^ {2} = {C}_{1} + \frac{{C }_{2} {\lambda} ^ {{C}_{3}} }{{\lambda} ^ {2} - {{C}_{4}} ^ {{C}_{5}}} + \frac{{C }_{6} {\lambda} ^ {{C}_{7}} }{{\lambda} ^ {2} - {{C}_{8}} ^ {{C}_{9}}} + \sum_{n=10}^{16} {C }_{n} {\lambda } ^ {{C}_{n+1}}$'

        elif str(self.formula.currentText()) == 'Formula 5':
            mathText = r'${n } ^ {2} = {C }_{1} + \sum_{n=2}^{N} {C }_{n} {\lambda } ^ {{C}_{n+1}}$'

        elif str(self.formula.currentText()) == 'Formula 6':
            mathText = r'$n-1= {C}_{1} + \sum_{n=2}^{N} \frac{{C}_{n}}{{C}_{n+1} - {\lambda} ^ {-2}}$'

        elif str(self.formula.currentText()) == 'Formula 7':
            mathText = r'$n = {C}_{1} + \frac{{C}_{2}}{{\lambda} ^ {2} -  0.028 } + {C}_{3} {\left (\frac{1}{{\lambda} ^ {2} -0.028} \right )} ^ {2} + {C}_{4} {\lambda} ^ {2} + {C}_{5} {\lambda} ^ {4} + {C}_{6} {\lambda} ^ {6}$'

        elif str(self.formula.currentText()) == 'Formula 8':
            mathText = r'$\frac{{n} ^ {2} -1}{{n} ^ {2} +2} = {C}_{1} + \frac{{C}_{2} {\lambda} ^ {2}}{{\lambda} ^ {2} - {C}_{3}} + {C}_{4} {\lambda} ^ {2}$'

        elif str(self.formula.currentText()) == 'Formula 9':
            mathText = r'${n} ^ {2} = {C}_{1} + \frac{{C}_{2}}{{\lambda} ^ {2} - {C}_{3}} + \frac{{C}_{4} (\lambda- {C}_{5} )}{{(\lambda- {C}_{5} )} ^ {2} + {C}_{6}}$'

        elif str(self.formula.currentText()) == 'Formula 10':
            mathText = r'$\varepsilon_{r}^{^{(b)}}(\omega)=\sum_{j=1}^{k}\frac{f_{j}\omega_{p}^{2}}{(\omega_{j}^{2}-\omega^{2})+i\gamma_j\omega}$'

        elif str(self.formula.currentText()) == 'Formula 11':
            self.label_coefficients.setText(_translate("Form", "Molecular Mass (kDa)", None))
            self.label_range.setText(_translate("Form", "Diameter (nm)", None))
            self.calcDiameterButton.setVisible(True)
            mathText = r'$n = n_0 + \left(\frac{dn}{dc}\right) \rho_R$'

        elif str(self.formula.currentText()) == 'Formula 12':
            mathText = r'$n = \mathrm{const.}$'

        self.formulaDisplay = self.displayFormula(mathText)


    def displayFormula(self, mathText):

        text=self.figure.suptitle(
            mathText,
            x=0.0,
            y=1.0,
            horizontalalignment='left',
            verticalalignment='top',
            size=QtGui.qApp.font().pointSize()*1.6)
        self.canvas.draw()


    def saveVariables(self):
        if str(self.formula.currentText()) == 'Formula 11':
            # write file
            file = 'materials/database/user-defined/' \
                             + str(self.material.text()) + '/' \
                             + str(self.name.text()) + '.yml'

            if not os.path.exists(os.path.dirname(file)):
                # create if not existing
                try:
                    os.makedirs(os.path.dirname(file))
                except:
                    print "wrong inputs"
            else:
                print "error"

            with open(file, "w") as f:
                comment = "# This file is part of the focal molography software"
                comment += " and user-defined.\n"
                f.write("\nDATA:")
                f.write("\n  - type: " + str(self.formula.currentText()).lower())
                if str(self.range.text()) != '':
                    f.write("\n    radius: " + str(float(self.range.text())*0.5e-9))
                f.write("\n    mass: " + str(float(self.coefficients.text())*1e3))

        elif str(self.formula.currentText()) == 'Formula 10':
            pass

        else:
            # write file
            file = 'materials/database/user-defined/' \
                             + str(self.material.text()) + '/' \
                             + str(self.name.text()) + '.yml'

            if not os.path.exists(os.path.dirname(file)):
                # create if not existing
                try:
                    os.makedirs(os.path.dirname(file))
                except:
                    print "wrong inputs"

            with open(file, "w") as f:
                comment = "# This file is part of the focal molography software"
                comment += " and user-defined.\n"
                f.write("\nDATA:")
                f.write("\n  - type: " + str(self.formula.currentText()).lower())
                if str(self.range.text()) != '':
                    f.write("\n    range: " + str(self.range.text()))
                f.write("\n    coefficients: " + str(self.coefficients.text()))




        # save in database
        if str(self.type.currentText()) == 'Particles':
            try:
                particleRadius = float(self.range.text())/2e9
            except:
                particleRadius = ''

            try:
                particleMass = float(self.coefficients.text())*1e3 # conversion to Da
            except:
                particleMass = ''

            variables = {
                'name' : str(self.name.text()),
                'material' : str(self.material.text()),
                'radius' : particleRadius,
                'mass' : particleMass,
                'file' : file,
            }

            createDB('materials/materials.db', 'Particles', particleDB())

        else:
            createDB('materials/materials.db', str(self.type.currentText()), mediumDB())
            variables = {
                'name' : str(self.name.text()),
                'material' : str(self.material.text()),
                'file' : file
            }

        saveDB('materials/materials.db', str(self.type.currentText()), variables)


        # close window
        self.close()


    def calcDiameter(self):

        try:
            Particle = RefrIndexOfLayerOfScatterers(
                M_protein = float(self.coefficients.text())*1e3)
            self.range.setText(str((3/(4*pi)*Particle.volumeProtein())**(1/3.)*2e9))
        except:
            # todo popup window
            pass


class CopyMaterial(QtGui.QWidget):
    def __init__(self, materialWidget):
        super(CopyMaterial, self).__init__()

        Form = QtGui.QWidget()

        # Initialize member variables
        self.materialWidget = materialWidget

        # Call child constructor
        self.setupUi(Form)

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Add a Material"))
        Form.resize(515, 364)
        self.input = QtGui.QGroupBox(Form)
        self.input.setGeometry(QtCore.QRect(0, 515, 0, 264))
        self.input.setMinimumSize(QtCore.QSize(515, 264))
        self.input.setObjectName(_fromUtf8("input"))

        self.label_name = QtGui.QLabel(self.input)
        self.label_name.setGeometry(QtCore.QRect(30, 30, 66, 17))
        self.label_name.setObjectName(_fromUtf8("label_name"))
        self.label_type = QtGui.QLabel(self.input)
        self.label_type.setGeometry(QtCore.QRect(30, 90, 66, 17))
        self.label_type.setObjectName(_fromUtf8("label_type"))
        self.label_material = QtGui.QLabel(self.input)
        self.label_material.setGeometry(QtCore.QRect(30, 60, 66, 17))
        self.label_material.setObjectName(_fromUtf8("label_material"))
        self.label_mass = QtGui.QLabel(self.input)
        self.label_mass.setGeometry(QtCore.QRect(30, 150, 141, 17))
        self.label_mass.setObjectName(_fromUtf8("label_mass"))
        self.label_size = QtGui.QLabel(self.input)
        self.label_size.setGeometry(QtCore.QRect(30, 120, 141, 17))
        self.label_size.setObjectName(_fromUtf8("label_size"))
        self.label_file = QtGui.QLabel(self.input)
        self.label_file.setGeometry(QtCore.QRect(30, 180, 121, 17))
        self.label_file.setObjectName(_fromUtf8("label_file"))
        self.name = QtGui.QLineEdit(self.input)
        self.name.setGeometry(QtCore.QRect(210, 28, 270, 21))
        self.name.setObjectName(_fromUtf8("name"))
        self.material = QtGui.QLineEdit(self.input)
        self.material.setGeometry(QtCore.QRect(210, 58, 270, 21))
        self.material.setObjectName(_fromUtf8("material"))
        self.type = QtGui.QLineEdit(self.input)
        self.type.setGeometry(QtCore.QRect(210, 88, 270, 21))
        self.type.setObjectName(_fromUtf8("type"))
        self.type.setEnabled(False)
        self.diameter = QtGui.QLineEdit(self.input)
        self.diameter.setGeometry(QtCore.QRect(210, 118, 250, 21))
        self.diameter.setObjectName(_fromUtf8("diameter"))
        self.calcDiameterButton = QtGui.QPushButton(self.input)
        self.calcDiameterButton.setGeometry(QtCore.QRect(460, 118, 20, 20))
        self.calcDiameterButton.setObjectName(_fromUtf8("buttonAdd"))
        self.calcDiameterButton.clicked.connect(self.calcDiameter)
        self.molecularMass = QtGui.QLineEdit(self.input)
        self.molecularMass.setGeometry(QtCore.QRect(210, 148, 270, 21))
        self.molecularMass.setObjectName(_fromUtf8("molecularMass"))
        self.file = QtGui.QLineEdit(self.input)
        self.file.setGeometry(QtCore.QRect(210, 178, 270, 21))
        self.file.setObjectName(_fromUtf8("file"))
        self.saveButton = QtGui.QPushButton(self.input)
        self.saveButton.setGeometry(QtCore.QRect(290, 220, 91, 27))
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.saveButton.clicked.connect(self.saveVariables)
        self.closeButton = QtGui.QPushButton(self.input)
        self.closeButton.setGeometry(QtCore.QRect(390, 220, 91, 27))
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.closeButton.clicked.connect(self.close)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)

        vbox.addWidget(self.input)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Form", None))
        self.label_name.setText(_translate("Form", "Name", None))
        self.label_type.setText(_translate("Form", "Type", None))
        self.label_material.setText(_translate("Form", "Material", None))
        self.label_mass.setText(_translate("Form", "Molecular Mass (kDa)", None))
        self.label_size.setText(_translate("Form", "Diameter (nm)", None))
        self.label_file.setText(_translate("Form", "File", None))
        self.saveButton.setText(_translate("Form", "Save", None))
        self.closeButton.setText(_translate("Form", "Cancel", None))
        self.calcDiameterButton.setText(_translate("MainWindow", "=", None))
        self.calcDiameterButton.setToolTip('Feijter\'s formula for water<br>(mass required; proteins as spheres)')

        values = self.materialWidget.getValues()
        self.name.setText(values['name'])
        self.type.setText(values['type'])
        self.file.setText(values['file'])


    def saveVariables(self):
        try:
            particleRadius = float(self.diameter.text())/2e9
        except:
            particleRadius = ''

        try:
            particleMass = float(self.molecularMass.text())*1e3 # conversion to Da
        except:
            particleMass = ''

        variables = {
            'name' : str(self.name.text()),
            'material' : str(self.material.text()),
            'radius' : particleRadius,
            'mass' : particleMass,
            'file' : str(self.file.text()),
        }
        # todo: check if name is not used

        createDB('materials/materials.db',
                    str(self.type.text()), particleDB())
        saveDB('materials/materials.db',
                    str(self.type.text()), variables)

        self.close()

        # except:
        #     # todo popup window
        #     print 'Invalid Inputs'

    def calcDiameter(self):

        try:
            Particle = RefrIndexOfLayerOfScatterers(
                M_protein = float(self.molecularMass.text())*1e3)
            self.diameter.setText(str((3/(4*pi)*Particle.volumeProtein())**(1/3.)*2e9))
        except:
            # todo popup window
            pass
