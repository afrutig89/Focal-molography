# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'waveguideTab.ui'
#
# Created: Fri Dec  2 13:48:14 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
from numpy import log, sqrt
import sys
sys.path.append('../../')
from waveguide.waveguide import SlabWaveguide
from auxiliaries.writeLogFile import loadDB

from materials.getData import get_data, get_range
import auxiliaries.constants as constants           # constants
from waveguidebackground.background import backgroundScatteringWaveguide               # limit of detection calculations



class WaveguideWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget, default_values):

        # Call parent class constructor
        super(WaveguideWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(Form, default_values)

    def setupUi(self, Form, default_values):

        # input plane wave parameters
        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        self.inputWave = QtGui.QGroupBox(Form)
        inputWave_params = QtGui.QFormLayout()

        self.inputWave.setTitle('Input Plane Wave')
 
        self.wavelength_lbl = QtGui.QLabel('Wavelength [nm]')
        self.wavelength = QtGui.QLineEdit()
        self.wavelength.setText(str(default_values['wavelength']*1e9))
        
        self.inputPower_lbl = QtGui.QLabel('Input Power [mW]')
        self.inputPower = QtGui.QLineEdit()
        self.inputPower.setText(str(default_values['inputPower']*1e3))

        self.beamWaist_lbl = QtGui.QLabel('Beam Waist [mm]')
        self.beamWaist = QtGui.QLineEdit()
        self.beamWaist.setText(str(default_values.get('beamWaist', 1)*1e3))


        inputWave_params.addRow(self.wavelength_lbl,self.wavelength)
        inputWave_params.addRow(self.inputPower_lbl,self.inputPower)
        inputWave_params.addRow(self.beamWaist_lbl,self.beamWaist)

        self.inputWave.setLayout(inputWave_params)

        # waveguide media setup
        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        self.media = QtGui.QGroupBox(Form)
        self.media.setTitle('Media')

        media_param = QtGui.QFormLayout()

        # Cover
        self.coverMaterial_lbl = QtGui.QLabel('Cover')
        self.coverMaterial = QtGui.QComboBox()
        self.defaultCover = default_values['cover']

        self.cover = loadDB('materials/materials.db', 'Cover Medium')
        
        if type(self.cover) != list: self.cover = [self.cover]
        
        self.currentCover = self.cover[0]
        
        currentIndex = 0
        i = 0
        
        for cover in self.cover:
            if cover['name'] == self.defaultCover:
                self.currentCover = cover
                currentIndex = i
            self.coverMaterial.addItem("")
            self.coverMaterial.setItemText(i, cover['name'])
            i += 1
        self.coverMaterial.setCurrentIndex(currentIndex)
        self.currentCover['refractiveIndex'] = get_data('materials' + self.currentCover['file'], default_values['wavelength']*1e6)[0]
        self.coverRefractiveIndex = QtGui.QLabel('n = {:.3f}'.format(self.currentCover['refractiveIndex']))
      
        self.coverMaterial.currentIndexChanged.connect(self.coverChange)


        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.coverMaterial)
        hbox.addWidget(self.coverRefractiveIndex)

        media_param.addRow(self.coverMaterial_lbl,hbox)
        
        # Film
        self.filmMaterial_lbl = QtGui.QLabel('Film Material')
        self.filmMaterial = QtGui.QComboBox()
        self.defaultFilm = default_values['film']

        self.film = loadDB('materials/materials.db', 'Film Medium')
        if type(self.film) != list: self.film = [self.film]
        self.currentFilm = self.film[0]
        currentIndex = 0
        i = 0

        for film in self.film:
            if film['name'] == self.defaultFilm:
                self.currentFilm = film
                currentIndex = i
            self.filmMaterial.addItem("")
            self.filmMaterial.setItemText(i,film['name'])
            i += 1

        self.filmMaterial.setCurrentIndex(currentIndex)
        self.currentFilm['refractiveIndex'] = get_data('materials' + self.currentFilm['file'], default_values['wavelength']*1e6)[0]
        self.filmRefractiveIndex = QtGui.QLabel('n = {:.3f}'.format(self.currentFilm['refractiveIndex']))
        self.filmMaterial.currentIndexChanged.connect(self.filmChange)


        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.filmMaterial)
        hbox.addWidget(self.filmRefractiveIndex)

        media_param.addRow(self.filmMaterial_lbl,hbox)

        # Substrate
        self.substrateMaterial_lbl = QtGui.QLabel('Substrate')
        self.substrateMaterial = QtGui.QComboBox()
        self.defaultSubstrate = default_values['substrate']

        self.substrate = loadDB('materials/materials.db', 'Substrate Medium')
        if type(self.substrate) != list: self.substrate = [self.substrate]
        self.currentSubstrate = self.substrate[0]
        currentIndex = 0
        i = 0
        for substrate in self.substrate:
            if substrate['name'] == self.defaultSubstrate:
                self.currentSubstrate = substrate
                currentIndex = i
            self.substrateMaterial.addItem("")
            self.substrateMaterial.setItemText(i,substrate['name'])
            i += 1

        self.substrateMaterial.setCurrentIndex(currentIndex)
        self.currentSubstrate['refractiveIndex'] = get_data('materials' + self.currentSubstrate['file'], default_values['wavelength']*1e6)[0]
        self.substrateRefractiveIndex = QtGui.QLabel('n = {:.3f}'.format(self.currentSubstrate['refractiveIndex']))

        self.substrateMaterial.currentIndexChanged.connect(self.substrateChange)


        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.substrateMaterial)
        hbox.addWidget(self.substrateRefractiveIndex)

        media_param.addRow(self.substrateMaterial_lbl,hbox)

        self.media.setLayout(media_param)

        # grating coupler parameters
        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        self.coupling = QtGui.QGroupBox(Form)
        self.coupling.setTitle('Coupling')
        
        coupling_params = QtGui.QFormLayout()
        
        self.couplingEfficiency_lbl = QtGui.QLabel('Coupling Efficiency [%]')
        self.couplingEfficiency = QtGui.QLineEdit()
        self.couplingEfficiency.setText(str(default_values['couplingEfficiency']*1e2))
    
        self.polarization_lbl = QtGui.QLabel('Polarization')

        self.polarization = QtGui.QComboBox()
        self.polarization.addItem("TE")
        self.polarization.addItem("TM")
        if default_values['polarization'] == 'TM':
            self.polarization.setCurrentIndex(1)
        
        self.gratingPosition_lbl = QtGui.QLabel('Grating Position [mm]')
        self.gratingPosition = QtGui.QLineEdit()
        self.gratingPosition.setText(str(default_values.get('gratingPosition', 0)*1e3))

        coupling_params.addRow(self.couplingEfficiency_lbl,self.couplingEfficiency)
        coupling_params.addRow(self.polarization_lbl,self.polarization)
        coupling_params.addRow(self.gratingPosition_lbl,self.gratingPosition)

        self.coupling.setLayout(coupling_params)

        # Waveguide properties
        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        self.waveguideProperties = QtGui.QGroupBox(Form)
        self.waveguideProperties.setTitle('Waveguide')

        wgP = QtGui.QFormLayout()

        self.propagationLoss_lbl = QtGui.QLabel('Propagation loss [dB/cm]')
        self.propagationLoss = QtGui.QLineEdit()
        self.propagationLoss.setText(str(default_values['propagationLoss']/ log(10)*10. / 100))
        self.propagationLoss.returnPressed.connect(self.update_waveguide)

        self.varSurfaceRoughness_lbl = QtGui.QLabel('Variance Surface Roughness [nm]')
        self.varSurfaceRoughness = QtGui.QLineEdit()

        self.filmThickness_lbl = QtGui.QLabel('Film Thickness [nm]')
  
        self.filmThickness = QtGui.QLineEdit()
        self.filmThickness.setText(str(default_values['thickness']*1e9))

        wgP.addRow(self.propagationLoss_lbl,self.propagationLoss)
        wgP.addRow(self.filmThickness_lbl,self.filmThickness)

        self.waveguideProperties.setLayout(wgP)

        # Calculated Parameters
        # ------------------------------------------------------------------------------------------------
        # ------------------------------------------------------------------------------------------------
        self.calculations_waveguide = QtGui.QGroupBox(Form)
        self.calculations_waveguide.setTitle('Calculated Parameters')

        calc_wg = QtGui.QFormLayout()

        self.n_eff_lbl = QtGui.QLabel('Effective Refractive Index')
        self.n_eff = QtGui.QLabel('')

        self.d_eff_lbl = QtGui.QLabel('Effective Thickness [nm]')
        self.d_eff = QtGui.QLabel('')
        
        
        self.wbg_lbl = QtGui.QLabel('Waveguide Background [W/m2]')
        self.wbg = QtGui.QLabel('')

        self.dNdn0_lbl = QtGui.QLabel('Differential sensitivity dN/dn_c')
        self.dNdn0 = QtGui.QLabel('')
        self.calculateWG_btn = QtGui.QPushButton('Calculate Waveguide')
        self.calculateWG_btn.pressed.connect(self.update_waveguide)

        calc_wg.addRow(self.n_eff_lbl,self.n_eff)
        calc_wg.addRow(self.d_eff_lbl,self.d_eff)
        calc_wg.addRow(self.wbg_lbl,self.wbg)
        calc_wg.addRow(self.dNdn0_lbl,self.dNdn0)
        calc_wg.addRow(self.calculateWG_btn)

        self.calculations_waveguide.setLayout(calc_wg)

        
        hbox = QtGui.QHBoxLayout()
        self.setLayout(hbox)

        # box left top
        vbox = QtGui.QVBoxLayout()

        vbox.addWidget(self.inputWave)
        vbox.addWidget(self.media)
        vbox.addWidget(self.coupling)
        vbox.addWidget(self.waveguideProperties)
        vbox.addWidget(self.calculations_waveguide)
        vbox.addStretch()

        # # # box right top
        # box = QtGui.QFormLayout()
        # box.addWidget(self.field_distribution)

        hbox.addLayout(vbox)
        # hbox.addLayout(box)


    def coverChange(self, i):
        self.currentCover = self.cover[i]
        wavelength = float(self.wavelength.text())*1e-3 # in um
        self.currentCover['refractiveIndex'] = get_data('materials' + self.currentCover['file'], wavelength)[0]
        self.coverRefractiveIndex.setText('n = {:.3f}'.format(self.currentCover['refractiveIndex']))
        

    def filmChange(self, i):
        self.currentFilm = self.film[i]
        wavelength = float(self.wavelength.text())*1e-3 # in um
        self.currentFilm['refractiveIndex'] = get_data('materials' + self.currentFilm['file'], wavelength)[0]
        self.filmRefractiveIndex.setText('n = {:.3f}'.format(self.currentFilm['refractiveIndex']))


    def substrateChange(self, i):
        self.currentSubstrate = self.substrate[i]
        wavelength = float(self.wavelength.text())*1e-3 # in um
        self.currentSubstrate['refractiveIndex'] = get_data('materials' + self.currentSubstrate['file'], wavelength)[0]
        self.substrateRefractiveIndex.setText('n = {:.3f}'.format(self.currentSubstrate['refractiveIndex']))
        

    def getValues(self):
        """
        returns a dictionary of the waveguide parameters
        """
        wavelength = float(self.wavelength.text())*1e-9
        n_c = complex(get_data('materials' + self.currentCover['file'], wavelength*1e6)[0]).real
        n_f = complex(get_data('materials' + self.currentFilm['file'], wavelength*1e6)[0]).real
        n_s = complex(get_data('materials' + self.currentSubstrate['file'], wavelength*1e6)[0]).real

        try:
            waveguidePar = {
                'wavelength' : wavelength,
                'inputPower' : float(self.inputPower.text())*1e-3,
                'beamWaist' : float(self.beamWaist.text())*1e-3,
                'couplingEfficiency' : float(self.couplingEfficiency.text())*1e-2,
                'gratingPosition' : float(self.gratingPosition.text())*1e-3,
                'polarization' : str(self.polarization.currentText()),
                'thickness' : float(self.filmThickness.text())*1e-9,
                'propagationLoss' : float(self.propagationLoss.text())* log(10)/10. * 100,
                'cover' : str(self.coverMaterial.currentText()),
                'n_c' : n_c,
                'film' : str(self.filmMaterial.currentText()),
                'n_f' : n_f,
                'substrate' : str(self.substrateMaterial.currentText()),
                'n_s' : n_s,
                'mode' : 0 # Framework can anyways only calculate the zeroth order modes correctly. 
            }

        except:
            return None

        return waveguidePar

    def setValues(self,values):
        """loads the values from a dictionary into the UI.

        :param values: dictionary with the key value pairs of the parameters.
        
        """

        self.wavelength.setText(str(values['wavelength']*1e9))
        self.inputPower.setText(str(values['inputPower']*1e3))
        self.beamWaist.setText(str(values['beamWaist']*1e3))
        self.couplingEfficiency.setText(str(values['couplingEfficiency']*1e2))
        self.gratingPosition.setText(str(values['gratingPosition']*1e3))
        self.polarization.setCurrentIndex(self.polarization.findText(str(values['polarization'])))
        self.filmThickness.setText(str(values['thickness']*1e9))
        self.propagationLoss.setText(str(values['propagationLoss']*10./log(10)/100))
        self.coverMaterial.setCurrentIndex(self.coverMaterial.findText(str(values['cover'])))
        self.filmMaterial.setCurrentIndex(self.filmMaterial.findText(str(values['film'])))
        self.substrateMaterial.setCurrentIndex(self.substrateMaterial.findText(str(values['substrate'])))


    def update_waveguide(self):
        globals().update(self.getValues())
        globals().update(self.main_widget.mologramTab.get_values()[0])

        beamWaist = float(self.beamWaist.text())*1e-3 # mW to W transformation
        powerperUnitLength = inputPower/(beamWaist) # calculate the power per unit length in the waveguide. 

        effectivePowerperUnitLength = couplingEfficiency * powerperUnitLength

        waveguide = SlabWaveguide(
                name = film,
                n_s = n_s,
                n_f = n_f,
                n_c = n_c,
                d_f = thickness,
                polarization = polarization,
                inputPower = effectivePowerperUnitLength,
                wavelength = wavelength,
                attenuationConstant = propagationLoss,
                mode = 0
            )

        N_eff = waveguide.N
        d_eff = waveguide.calcEffThickness(wavelength)
        dNdn0 = waveguide.calcSensitivity()
        
        # calculate the background intensity
        
        I_bwg = backgroundScatteringWaveguide(effectivePowerperUnitLength, propagationLoss, focalPoint, D=diameter)

        self.wbg.setText('%.3e' % (I_bwg))
        self.n_eff.setText("%.3f" % N_eff)
        self.d_eff.setText("%.3f" % (d_eff*1e9))
        self.dNdn0.setText("%.3f" % (dNdn0))

# needs to be included from the Noise Tab
    def updateBackground(self):
        """returns scattering attenuation (in Np/m) and reflected power"""

        profileSTD = float(self.profileSTD.text())*1e-9
        sigma = profileSTD
        alpha_s = waveguide.surfaceScatteringAttenuation(sigma) * 10 / (np.log(10)*100) # 10 because intensity
        self.scatteringAttenuation.setText('{:.2f}'.format(alpha_s))
        self.scatteringAttenuation_units.setText('dB/cm')

        reflectedPower = waveguide.specularlyReflectedBeam(inputPower * couplingEfficiency, sigma)
        self.specularlyReflectedPower.setText('{:.2e}'.format(reflectedPower))
        self.specularlyReflectedPower_units.setText('W')

        return alpha_s, reflectedPower


    def calculateCutoff(self):
        globals().update(self.getValues())
        globals().update(self.main_widget.mologramTab.get_values()[0])

        beamWaist = float(self.beamWaist.text())*1e-3 # mW to W transformation
        powerperUnitLength = inputPower/(beamWaist) # calculate the power per unit length in the waveguide. 

        effectivePowerperUnitLength = couplingEfficiency * powerperUnitLength

        waveguide = SlabWaveguide(
                name = film,
                n_s = n_s,
                n_f = n_f,
                n_c = n_c,
                d_f = thickness,
                polarization = polarization,
                inputPower = effectivePowerperunitLength,
                wavelength = wavelength,
                attenuationConstant = propagationLoss,
                mode = 0
            )

        self.main_widget.textBrowser.append('Cutoff of the 0th and 1st modes are d = ({:.1f},{:.1f}) nm'.format(waveguide.cutoffThickness(0).real*1e9,waveguide.cutoffThickness(1).real*1e9))



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = WaveguideTab()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

