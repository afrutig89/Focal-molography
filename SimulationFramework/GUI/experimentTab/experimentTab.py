# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import numpy as np
import sys, os
from PIL import Image
sys.path.append('../')
from auxiliaries.writeLogFile import loadDB
# matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)



class ExperimentWidget(QtGui.QWidget):
    def __init__(self, main_widget):
        super(ExperimentWidget, self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        # Call child constructor
        self.setupUi()

    def setupUi(self):
        # labels for the form
        self.editFolder_lbl = QtGui.QLabel('Experiment Folder')
        # line edits and selectors
        self.editFolder = QtGui.QLineEdit()
        # push button
        self.browse = QtGui.QPushButton('browse')
        self.browse.clicked.connect(self.browse_f)
        self.browse.resize(self.browse.sizeHint())

        # layout
        hbox_folder   = QtGui.QHBoxLayout()
        hbox_folder.addWidget(self.editFolder)
        hbox_folder.addWidget(self.browse)

        # self.filter = QtGui.QGroupBox('Filter')
        # self.medianFilter = QtGui.QPushButton('Median Filter')
        # self.gaussianFilter = QtGui.QPushButton('Gaussian Filter')
        # self.otsuFilter = QtGui.QPushButton('Otsu Filter')
        # self.filter.setEnabled(False)
        # fbox = QtGui.QFormLayout()
        # fbox.addRow(self.medianFilter)
        # fbox.addRow(self.gaussianFilter)
        # fbox.addRow(self.otsuFilter)

        # self.filter.setLayout(fbox)

        self.makezStackButton = QtGui.QPushButton('z-stack')
        self.makezStackButton.clicked.connect(self.makezStack)
        self.makeVideoButton = QtGui.QPushButton('Video')
        self.makeVideoButton.clicked.connect(self.makeVideo)
        self.makeVideoButton.setEnabled(False)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.editFolder_lbl, hbox_folder)
        fbox.addRow(self.makezStackButton)
        fbox.addRow(self.makeVideoButton)


        # box left top
        vbox = QtGui.QVBoxLayout()

        # vbox.addWidget(self.filter)
        vbox.addLayout(fbox)
        vbox.addStretch()

        self.setLayout(vbox)


    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '/home/focal-molography/Experiments')
        self.editFolder.setText(folder)


    def makezStack(self):
        """Make a z-Stack of an experiment that was acquired by the Moloreader."""
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '/home/focal-molography/Experiments')
        if folder != '':
            # load database if exist
            print(folder)
            if os.path.isfile('{}/database/data.db'.format(os.path.dirname(folder))):
                print('Load database ...')
                # db should be one folder above
                data = loadDB('{}/database/data.db'.format(os.path.dirname(folder)), 'results')
                filenames = list()
                z_positions = list()
                for entry in data:
                    if folder in entry['imagePath']:
                        filenames.append(entry['imagePath'])
                        z_positions.append(entry['z_position'])

                # sort images
                z_positions = np.array(z_positions)
                sort = z_positions.argsort()
                z_positions = z_positions[sort]
                filenames = np.array(filenames)[sort]

            else:
                # sort images by date generated
                mtime = lambda f: os.stat(os.path.join(folder, f)).st_mtime
                filenames = ['{}/{}'.format(folder, filename) for filename in list(sorted(os.listdir(folder), key=mtime))]


            # load images and save in list
            print('Load images ...')

            images = []
            i = 0
            maximum = 0
            focalImage = 0

            for filename in filenames:
                try:
                    image = Image.open(filename)
                    imarray = np.array(image)
                    # rotation required?
                    images.append(imarray)

                    if np.max(imarray) > maximum:
                        # image with highest intensity
                        maximum = np.max(imarray)
                        focalImage = i # focal plane

                    i += 1

                except:
                    continue


            print('{} images loaded'.format(i))

            # find focal point
            focalPlane = images[focalImage]

            x_max = int(focalPlane.argmax()/focalPlane.shape[1])+1 # y-axis with the highest values
            y_max = focalPlane[x_max].argmax() # x-axis with the highest value

            # create z-stack
            I_meas_xz = np.zeros((len(images),images[0].shape[1]))
            
            row = 0
            for im in images:
                I_meas_xz[row] = im[x_max]
                row += 1
            
            # take account for multiple maxima (saturation)
            focalImage = focalImage + int(sum(I_meas_xz[:,y_max] == np.max(I_meas_xz[:,y_max]))/2)


            # number of pixels in x-direction
            # this part should be replaced with data stored in the database or image
            pixelsizeCamera = self.main_widget.setup.camera.info['pixel_size'] # in um
            magnification = self.main_widget.setup.camera.specifications.magnification

            npix = images[0].shape[0]
            x_range = npix
            z_range = (np.max(z_positions)-np.min(z_positions))/pixelsizeCamera*magnification

            # plot stack
            self.main_widget.cameraWidget.cameraFeed.im.set_data(I_meas_xz)
            self.main_widget.cameraWidget.cameraFeed.im.set_interpolation('spline36')
            self.main_widget.cameraWidget.cameraFeed.im.set_extent([0,x_range,0,z_range])

            self.main_widget.cameraWidget.cameraFeed.ax.set_aspect(z_range/x_range)
            self.main_widget.cameraWidget.cameraFeed.scale_bar()
            self.main_widget.cameraWidget.cameraFeed.img = I_meas_xz
            self.main_widget.cameraWidget.cameraFeed.canvas.draw()


    def makeVideo(self):
        pass