# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'parameters.ui'
#
# Created: Fri Dec  2 12:39:31 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class ParameterWidget(QtGui.QWidget):
    """ Constructor """
    def __init__(self, main_widget, values):

        # Call parent class constructor
        super(ParameterWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(Form, values)


    def setupUi(self, Form, values):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(403, 553)

        self.treeWidget = QtGui.QTreeWidget(Form)
        self.treeWidget.setGeometry(QtCore.QRect(0, 0, 500, 651))
        self.treeWidget.setEditTriggers(QtGui.QAbstractItemView.DoubleClicked|QtGui.QAbstractItemView.EditKeyPressed)
        self.treeWidget.setObjectName(_fromUtf8("treeWidget"))
        ## Waveguide
        item_0 = QtGui.QTreeWidgetItem(self.treeWidget)

        # Input Wave
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        # Coupling
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        # Media
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        
        ## Mologram
        item_0 = QtGui.QTreeWidgetItem(self.treeWidget)
        # particles
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_2 = QtGui.QTreeWidgetItem(item_1)
        item_2.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        
        ## Background
        item_0 = QtGui.QTreeWidgetItem(self.treeWidget)
        
        ## Screen
        item_0 = QtGui.QTreeWidgetItem(self.treeWidget)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)

        self.retranslateUi(Form, values)
        QtCore.QMetaObject.connectSlotsByName(Form)

        vbox = QtGui.QVBoxLayout()
        self.setLayout(vbox)
        vbox.addWidget(self.treeWidget)

    def retranslateUi(self, Form, values):
        Form.setWindowTitle(_translate("Form", "Form", None))

        # N_particles = values['N_particles']
        # coherentBinding = values['coherentBinding']
        # coherentParticles = int((2*coherentBinding-1)*N_particles)
        # noncoherentParticles = N_particles-coherentParticles

        # self.treeWidget.headerItem().setText(0, _translate("Form", "Parameter", None))
        # self.treeWidget.headerItem().setText(1, _translate("Form", "Value", None))
        # self.treeWidget.headerItem().setText(2, _translate("Form", "Units", None))
        # self.treeWidget.setColumnWidth(0,170)
        # self.treeWidget.setColumnWidth(1,100)
        # self.treeWidget.setColumnWidth(2,70)
        # __sortingEnabled = self.treeWidget.isSortingEnabled()
        # self.treeWidget.setSortingEnabled(False)
        # self.treeWidget.topLevelItem(0).setText(0, _translate("Form", "Waveguide", None))
        # self.treeWidget.topLevelItem(0).child(0).setText(0, _translate("Form", "Input Wave", None))
        # self.treeWidget.topLevelItem(0).child(0).child(0).setText(0, _translate("Form", "Wavelength", None))
        # self.treeWidget.topLevelItem(0).child(0).child(0).setText(1, _translate("Form", str(values['wavelength']*1e9), None))
        # self.treeWidget.topLevelItem(0).child(0).child(0).setText(2, _translate("Form", "nm", None))
        # self.treeWidget.topLevelItem(0).child(0).child(1).setText(0, _translate("Form", "Input Power", None))
        # self.treeWidget.topLevelItem(0).child(0).child(1).setText(1, _translate("Form", str(values['inputPower']*1e3), None))
        # self.treeWidget.topLevelItem(0).child(0).child(1).setText(2, _translate("Form", "mW", None))
        # self.treeWidget.topLevelItem(0).child(1).setText(0, _translate("Form", "Coupling", None))
        # self.treeWidget.topLevelItem(0).child(1).child(0).setText(0, _translate("Form", "Efficiency", None))
        # self.treeWidget.topLevelItem(0).child(1).child(0).setText(1, _translate("Form", str(values['couplingEfficiency']*1e2), None))
        # self.treeWidget.topLevelItem(0).child(1).child(0).setText(2, _translate("Form", "%", None))
        # self.treeWidget.topLevelItem(0).child(1).child(1).setText(0, _translate("Form", "Polarization", None))
        # self.treeWidget.topLevelItem(0).child(1).child(1).setText(1, _translate("Form", values['polarization'], None))
        # self.treeWidget.topLevelItem(0).child(1).child(2).setText(0, _translate("Form", "Highest Mode", None))
        # self.treeWidget.topLevelItem(0).child(1).child(2).setText(1, _translate("Form", str(values['mode']), None))
        # self.treeWidget.topLevelItem(0).child(2).setText(0, _translate("Form", "Media", None))
        # self.treeWidget.topLevelItem(0).child(2).child(0).setText(0, _translate("Form", "Cover Medium", None))
        # self.treeWidget.topLevelItem(0).child(2).child(0).setText(1, _translate("Form", "Aqueous Medium", None))
        # self.treeWidget.topLevelItem(0).child(2).child(1).setText(0, _translate("Form", "Film", None))
        # self.treeWidget.topLevelItem(0).child(2).child(1).setText(1, _translate("Form", values['film'], None))
        # self.treeWidget.topLevelItem(0).child(2).child(2).setText(0, _translate("Form", "Substrate", None))
        # self.treeWidget.topLevelItem(0).child(2).child(2).setText(1, _translate("Form", values['substrate'], None))
        # self.treeWidget.topLevelItem(0).child(3).setText(0, _translate("Form", "Thickness", None))
        # self.treeWidget.topLevelItem(0).child(3).setText(1, _translate("Form", str(values['thickness']*1e9), None))
        # self.treeWidget.topLevelItem(0).child(3).setText(2, _translate("Form", "nm", None))
        # self.treeWidget.topLevelItem(0).child(4).setText(0, _translate("Form", "Propagation Loss", None))
        # self.treeWidget.topLevelItem(0).child(4).setText(1, _translate("Form", str(values['propagationLoss'] /(100. * np.log(10)) * 10), None))
        # self.treeWidget.topLevelItem(0).child(4).setText(2, _translate("Form", "db/cm", None))
        # self.treeWidget.topLevelItem(1).setText(0, _translate("Form", "Mologram", None))
        # self.treeWidget.topLevelItem(1).child(0).setText(0, _translate("Form", "Particles", None))
        # self.treeWidget.topLevelItem(1).child(0).child(0).setText(0, _translate("Form", "Material", None))
        # self.treeWidget.topLevelItem(1).child(0).child(0).setText(1, _translate("Form", values['particleName'], None))
        # self.treeWidget.topLevelItem(1).child(0).child(1).setText(0, _translate("Form", "Amount", None))
        # self.treeWidget.topLevelItem(1).child(0).child(1).setText(1, _translate("Form", str(N_particles), None))
        # self.treeWidget.topLevelItem(1).child(0).child(2).setText(0, _translate("Form", "Coherent Binding", None))
        # self.treeWidget.topLevelItem(1).child(0).child(2).setText(1, _translate("Form", str(coherentBinding*1e2), None))
        # self.treeWidget.topLevelItem(1).child(0).child(2).setText(2, _translate("Form", "%", None))
        # self.treeWidget.topLevelItem(1).child(0).child(3).setText(0, _translate("Form", "Coherent", None))
        # self.treeWidget.topLevelItem(1).child(0).child(3).setText(1, _translate("Form", str(coherentParticles), None))
        # self.treeWidget.topLevelItem(1).child(0).child(4).setText(0, _translate("Form", "Noncoherent", None))
        # self.treeWidget.topLevelItem(1).child(0).child(4).setText(1, _translate("Form", str(noncoherentParticles), None))
        # self.treeWidget.topLevelItem(1).child(1).setText(0, _translate("Form", "Focal Point", None))
        # self.treeWidget.topLevelItem(1).child(1).setText(1, _translate("Form", str(values['focalPoint']*1e6), None))
        # self.treeWidget.topLevelItem(1).child(1).setText(2, _translate("Form", "um", None))
        # self.treeWidget.topLevelItem(1).child(2).setText(0, _translate("Form", "Diameter", None))
        # self.treeWidget.topLevelItem(1).child(2).setText(1, _translate("Form", str(values['diameter']*1e6), None))
        # self.treeWidget.topLevelItem(1).child(2).setText(2, _translate("Form", "um", None))
        # self.treeWidget.topLevelItem(1).child(3).setText(0, _translate("Form", "Bragg Offset", None))
        # self.treeWidget.topLevelItem(1).child(3).setText(1, _translate("Form", str(values['braggOffset']*1e6), None))
        # self.treeWidget.topLevelItem(1).child(3).setText(2, _translate("Form", "um", None))
        # self.treeWidget.topLevelItem(1).child(4).setText(0, _translate("Form", "Placement", None))
        # self.treeWidget.topLevelItem(1).child(4).setText(1, _translate("Form", values['placement'], None))
        # self.treeWidget.topLevelItem(1).child(5).setText(0, _translate("Form", "Min. Distance", None))
        # self.treeWidget.topLevelItem(1).child(5).setText(1, _translate("Form", str(values['z_min']*1e9), None))
        # self.treeWidget.topLevelItem(1).child(5).setText(2, _translate("Form", "nm", None))
        # self.treeWidget.topLevelItem(1).child(6).setText(0, _translate("Form", "Max. Distance", None))
        # self.treeWidget.topLevelItem(1).child(6).setText(1, _translate("Form", str(values['z_max']*1e9), None))
        # self.treeWidget.topLevelItem(1).child(6).setText(2, _translate("Form", "nm", None))
        # self.treeWidget.topLevelItem(2).setText(0, _translate("Form", "Background", None))
        # self.treeWidget.topLevelItem(3).setText(0, _translate("Form", "Screen", None))
        # self.treeWidget.topLevelItem(3).child(0).setText(0, _translate("Form", "Width", None))
        # self.treeWidget.topLevelItem(3).child(0).setText(1, _translate("Form", str(values['screenWidth']*1e6), None))
        # self.treeWidget.topLevelItem(3).child(0).setText(2, _translate("Form", "um", None))
        # self.treeWidget.topLevelItem(3).child(1).setText(0, _translate("Form", "Plane", None))
        # self.treeWidget.topLevelItem(3).child(1).setText(1, _translate("Form", values['screenPlane'], None))
        # self.treeWidget.topLevelItem(3).child(2).setText(0, _translate("Form", "Axis Ratio", None))
        # self.treeWidget.topLevelItem(3).child(2).setText(1, _translate("Form", str(values['screenRatio']), None))
        # self.treeWidget.setSortingEnabled(__sortingEnabled)

    def updateParameters(self, parameters):
        root = self.treeWidget.invisibleRootItem()
        childCount = root.childCount()

        for i in range(childCount):
            item0 = root.child(i)
            if str(item0.text(0)) in parameterDictionary:
                if parameterDictionary[str(item0.text(0))] in parameters:
                    item0.setText(1, str(scaleUnits(item0.text(2))*parameters[parameterDictionary[str(item0.text(0))]]))

            for m in range(item0.childCount()):
                item1 = item0.child(m)
                if str(item1.text(0)) in parameterDictionary:
                    if parameterDictionary[str(item1.text(0))] in parameters:
                        item1.setText(1, str(scaleUnits(item1.text(2))*parameters[parameterDictionary[str(item1.text(0))]]))

                for n in range(item1.childCount()):
                    item2 = item1.child(n)
                    if str(item2.text(0)) in parameterDictionary:
                        if parameterDictionary[str(item2.text(0))] in parameters:
                            item2.setText(1, str(scaleUnits(item2.text(2))*parameters[parameterDictionary[str(item2.text(0))]]))

def scaleUnits(units):
    # returns the right scaling according to the units given 
    if len(units) == 0:
        return 1
    elif units[0] == 'm':
        return 1e3
    elif units[0] == 'u':
        return 1e6
    elif units[0] == 'n':
        return 1e9
    elif units[0] == '%':
        return 100
    elif units[0] == 'dB/cm':
        return 10/(100. * np.log(10))
    else:
        return 1

parameterDictionary = {
    # waveguide
    'Wavelength' : 'wavelength',
    'Input Power' : 'inputPower',

    'Efficiency' : 'couplingEfficiency',
    'Polarization' : 'polarization',
    'Highest Mode' : 'mode',

    'Cover Medium' : 'cover',
    'Film' : 'film',
    'Substrate' : 'substrate',

    'Thickness' : 'thickness',
    'Propagation Loss' : 'propagationLoss',

    # mologram
    'Material' : 'particleName',
    'Amount' : 'N_particles',
    'Coherent' : 'coherentParticles',
    'Noncoherent' : 'noncoherentParticles',
    'Coherent Binding' : 'coherentBinding',

    'Focal Point' : 'focalPoint',
    'Diameter' : 'diameter',
    'Bragg Offset' : 'braggOffset',
    'Placement' : 'placement',
    'Min. Distance' : 'z_min',
    'Max. Distance' : 'z_max',

    # background (todo)

    # screen
    'Width' : 'screenWidth',
    'Plane' : 'screenPlane',
    'Axis Ratio' : 'screenRatio',
    'x left' : 'xlim',
    'y left' : 'ylim',
}

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

