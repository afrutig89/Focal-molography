from PyQt4 import QtGui, QtCore

import os
import sys
sys.path.append('../../')
from auxiliaries.writeLogFile import loadDB
from auxiliaries import constants
from dipole.plots import plotParameter, findCoordinates

# matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import matplotlib.image as mpimg

from matplotlib.lines import Line2D
from matplotlib.text import Text

from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from matplotlib._png import read_png

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT# as NavigationToolbar

from matplotlib.backend_bases import cursors as mplCursors
from matplotlib.patches import Ellipse

import numpy as np


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class AnalyticalWidget(QtGui.QWidget):
    # Constructor
    def __init__(self,main_widget):

        # Call parent class constructor
        super(AnalyticalWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        # Call child constructor
        self.initUI()

    """ Initialization of UI """
    def initUI(self):

        self.horHeaders = ['Analytical','Simulation','Difference', 'Ratio']
        self.table = QtGui.QTableWidget()
        self.table.setColumnCount(len(self.horHeaders))
        self.table.setRowCount(len(analyticalDict))
        self.table.setObjectName(_fromUtf8("tableWidget"))
        self.table.setHorizontalHeaderLabels(self.horHeaders)

        self.verHeaders = []
        self.verHeaders2 = [] # this will be used to find the corresponding row
        for key, value in analyticalDict.items():
            self.verHeaders2.append(key)
            if value[2] != '':
                # if units are defined
                self.verHeaders.append('{} [{}]'.format(value[0],value[2]))
            else:
                self.verHeaders.append(value[0])

        self.table.setVerticalHeaderLabels(self.verHeaders)

        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()

        main_box = QtGui.QHBoxLayout()

        # set up canvas
        v_splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
        v_splitter.addWidget(self.table)

        main_box.addWidget(v_splitter)

        self.setLayout(main_box)


    def updateValues(self, resultsDictionary, column='analytical'):
        if column == 'analytical': columnIndex = 0
        elif column == 'simulation': columnIndex = 1

        for key, value in resultsDictionary.items():
            if key in analyticalDict.keys():
                rowIndex = self.verHeaders2.index(key)

                item = QtGui.QTableWidgetItem()
                self.table.setItem(rowIndex, columnIndex, item)
                try:
                    # if value is numeric
                    try:
                        # if array, tuple or list, take first element
                        item.setText(_translate("Form", str(value[0]*analyticalDict[key][1]), None))
                    except:
                        item.setText(_translate("Form", str(value*analyticalDict[key][1]), None))
                except:
                    # value is not numeric
                    item.setText(_translate("Form", str(value), None))

                # update difference and ratio
                itemDifference = QtGui.QTableWidgetItem()
                self.table.setItem(rowIndex, 2, itemDifference)
                itemRatio = QtGui.QTableWidgetItem()
                self.table.setItem(rowIndex, 3, itemRatio)
                try:
                    valueAnalytical = float(self.table.item(rowIndex,0).text())
                    valueSimulation = float(self.table.item(rowIndex,1).text())
                    itemDifference.setText(_translate("Form", str(valueAnalytical-valueSimulation), None))
                    itemRatio.setText(_translate("Form", str(valueAnalytical/valueSimulation), None))
                except:
                    pass

            else: continue

    def getValues(self):
        rows = self.table.rowCount()
        columns = self.table.columnCount()
        values = []
        for row in range(rows):
            values.append([])
            for column in range(columns):
                values[row].append(self.table.item(row,column).text())

        return values



analyticalDict = {
    # 'waveguide': 'Waveguide',
    'k_0' : ['Wave vector', 1, '1/m'],
    'z_avg' : ['Average particle height', 1e9, 'nm'],
    'refractiveIndex' : ['Refractive Index', 1, ''],
    'absorptionCrossSection' : ['Absorption cross section', 1e20, "pm2"],
    'extinctionCrossSection' : ['Extinction cross section', 1e20, 'pm2'],
    'scatteringCrossSection' : ['Scattering cross section', 1e20, 'pm2'],
    'r_avg' : ['Average distance to focalpoint', 1e6, 'um'],
    'theta' : ['Maximum angle', 180/np.pi, 'deg'],
    'V' : ['Particle volume', 1e27, 'nm3'],
    'E_exc_mean' : ['Mean excitation field', 1, 'V/m'],
    'alpha' : ['Dipole polarizability', 1e6/(4*np.pi*constants.eps_0), 'cgs'],
    'p' : ['Static dipole moment', 1e21*constants.c, 'D'],
    'P_0' : ['Dipole radiation', 1, 'W'],
    'I_0' : ['Dipole intensity', 1, 'W/m2'],
    'I_SB' : ['Specific Binding', 1, 'W/m2'],
    'I_NSB' : ['Mean NSB', 1, 'W/m2'],
    'I_signal' : ['Signal Intensity', 1, 'W/m2'],
    'SNR' : ['SNR', 1, ''],

    # parameters of paper Fattinger
    'c_R' : ['Concentration ridges', 1e-6, 'g/ml'],
    'c_G' : ['Concentration grooves', 1e-6, 'g/ml'],
    'n_R' : ['Refractive index ridges', 1, ''],
    'n_G' : ['Refractive index grooves', 1, ''],
    'Lambda_R' : ['Wavelength in ridges', 1e9, 'nm'],
    'Lambda_G' : ['Wavelength in grooves', 1e9, 'nm'],
    'A_molo' : ['Leakage parameter mologram', 1, 'm2'],
    'A_scat' : ['Leakage parameter waveguide', 1, 'm2'],
    'Gamma_Delta' : ['Surface mass-density modulation', 1, 'm2'],
}