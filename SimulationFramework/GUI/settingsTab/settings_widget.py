# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'settingsWidget.ui'
#
# Created: Thu Dec  8 15:05:45 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import os, sys
import numpy as np
sys.path.append('../../')
import time, datetime

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class SettingsWidget(QtGui.QWidget):
    """Generates the SettingsWidget"""
    def __init__(self, main_widget, default_values):
        """ Values are the default values loaded from the database: database/default.db."""

        # Call parent class constructor
        super(SettingsWidget,self).__init__()

        # Assign main layout
        self.main_widget = main_widget

        Form = QtGui.QWidget()

        # Call child constructor
        self.setupUi(Form, default_values)

    def setupUi(self, Form, default_values):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(471, 417)

        # generate the General Settings Tab, I need to reprogram this UI.
        self.generalSettings = QtGui.QGroupBox(Form)
        self.generalSettings.setGeometry(QtCore.QRect(30, 30, 261, 200))
        self.generalSettings.setMinimumSize(QtCore.QSize(431, 170))
        self.generalSettings.setObjectName(_fromUtf8("generalSettings"))
        
        self.save_database = QtGui.QLabel(self.generalSettings)
        self.save_database.setGeometry(QtCore.QRect(25, 30, 101, 17))
        self.save_database.setObjectName(_fromUtf8("save_database"))
        
        self.saveLog = QtGui.QCheckBox(self.generalSettings)
        self.saveLog.setGeometry(QtCore.QRect(190, 25, 175, 22))
        self.saveLog.setChecked(default_values['saveLog'])
        self.saveLog.setObjectName(_fromUtf8("saveLog"))
        
        self.saveSimulations = QtGui.QCheckBox(self.generalSettings)
        self.saveSimulations.setGeometry(QtCore.QRect(190, 55, 175, 22))
        self.saveSimulations.setChecked(default_values['saveDatabase'])
        self.saveSimulations.setObjectName(_fromUtf8("saveLog"))
        
        self.saveFigure = QtGui.QCheckBox(self.generalSettings)
        self.saveFigure.setGeometry(QtCore.QRect(190, 85, 175, 22))
        self.saveFigure.setChecked(default_values['saveFigure'])
        self.saveFigure.setObjectName(_fromUtf8("saveFigure"))
        
        self.saveFigureSettings = QtGui.QPushButton("...", self.generalSettings)
        self.saveFigureSettings.setGeometry(QtCore.QRect(385, 85, 40, 27))
        self.saveFigureSettings.setObjectName(_fromUtf8("saveFigureSettings"))
        self.saveFigureSettings.clicked.connect(lambda: self.openFigureSettingsWindow(window='figure'))
        self.label_method = QtGui.QLabel(self.generalSettings)
        self.label_method.setGeometry(QtCore.QRect(25, 125, 181, 17))
        self.label_method.setObjectName(_fromUtf8("label_method"))
        self.method = QtGui.QComboBox(self.generalSettings)
        self.method.setGeometry(QtCore.QRect(190, 120, 181, 27))
        self.method.setObjectName(_fromUtf8("method"))
        
        # generate the Database Tab
        self.databaseParams = QtGui.QGroupBox(Form)
        self.databaseParams.setGeometry(QtCore.QRect(30, 30, 531, 261))
        self.databaseParams.setMinimumSize(QtCore.QSize(431, 430))
        self.databaseParams.setObjectName(_fromUtf8("databaseParams"))
        self.label_folder = QtGui.QLabel(self.databaseParams)
        self.label_folder.setGeometry(QtCore.QRect(25, 30, 101, 17))
        self.label_folder.setObjectName(_fromUtf8("label_folder"))
        self.label_folder.setText(_translate("Form", "Folder", None))
        self.databaseFolder = QtGui.QLineEdit(self.databaseParams)
        self.databaseFolder.setGeometry(QtCore.QRect(190, 25, 181, 27))
        self.databaseFolder.setObjectName(_fromUtf8("databaseFolder"))
        self.databaseFolder.setText(_translate("Form", '/home/gimmedatcudaspeedmmmh/Downloads', None))
        self.browse = QtGui.QPushButton('...', self.databaseParams)
        self.browse.setGeometry(QtCore.QRect(385, 25, 40, 27))
        self.browse.clicked.connect(self.browse_f)
        self.label_filename = QtGui.QLabel(self.databaseParams)
        self.label_filename.setGeometry(QtCore.QRect(25, 70, 151, 17))
        self.label_filename.setObjectName(_fromUtf8("label_filename"))
        self.databaseFilename = QtGui.QLineEdit(self.databaseParams)
        self.databaseFilename.setGeometry(QtCore.QRect(190, 65, 181, 27))
        self.databaseFilename.setObjectName(_fromUtf8("databaseFilename"))
        self.label_description = QtGui.QLabel(self.databaseParams)
        self.label_description.setGeometry(QtCore.QRect(25, 110, 151, 17))
        self.label_description.setObjectName(_fromUtf8("label_description"))
        self.description = QtGui.QLineEdit(self.databaseParams)
        self.description.setGeometry(QtCore.QRect(190, 105, 181, 27))
        self.description.setObjectName(_fromUtf8("databaseFile"))
        self.label_parameters = QtGui.QLabel(self.databaseParams)
        self.label_parameters.setGeometry(QtCore.QRect(25, 150, 151, 17))
        self.label_parameters.setObjectName(_fromUtf8("label_parameters"))
        
        self.checkBox = []
        self.checkBoxValues = []
        x = [40, 230]
        y = 170
        # generate the Checkboxes for the parameters that are in the simulation_dict defined in the file variables.py
        for key in self.main_widget.simResults.returnDBPar().keys():
            if key == 'ID' or key not in default_values: continue
            self.checkBox.append(QtGui.QCheckBox(self.databaseParams))
            self.checkBox[-1].setGeometry(QtCore.QRect(x[len(self.checkBox) % 2 ==0], y + ((len(self.checkBox)-1) / 2)*25, 175, 22))
            self.checkBox[-1].setChecked(default_values[key])
            self.checkBox[-1].setObjectName(_fromUtf8("checkBox"))
            self.checkBox[-1].setText(_translate("Form", key, None))
            self.checkBoxValues.append(key)

        self.retranslateUi(Form, default_values)
        QtCore.QMetaObject.connectSlotsByName(Form)

        hbox = QtGui.QHBoxLayout()
        self.setLayout(hbox)

        # box left top
        vbox = QtGui.QVBoxLayout()

        vbox.addWidget(self.generalSettings)
        vbox.addWidget(self.databaseParams)
        vbox.addStretch()

        # # # box right top
        # box = QtGui.QFormLayout()
        # box.addWidget(self.field_distribution)

        hbox.addLayout(vbox)

    def retranslateUi(self, Form, default_values):
        """Function sets the proper title for the UI form. """
        self.generalSettings.setTitle(_translate("Form", "Generel Settings", None))
        self.save_database.setText(_translate("Form", "Save", None))
        self.saveLog.setText(_translate("Form", "Log", None))
        self.saveSimulations.setText(_translate("Form", "Simulations", None))
        self.saveFigure.setText(_translate("Form", "Figure", None))
        self.label_method.setText(_translate("Form", "Calculation Method", None))
        self.method.addItem(_fromUtf8(""))
        self.method.addItem(_fromUtf8(""))
        self.method.addItem(_fromUtf8(""))
        self.method.setItemText(0, _translate("Form", "Split Process (Windows)", None))
        self.method.setItemText(1, _translate("Form", "Parallel Cores (Unix)", None))
        self.method.setItemText(2, _translate("Form", "Cuda (GPU)", None))
        if default_values['calculationMethod'] == 'parallelPool':
            self.method.setCurrentIndex(1)
        elif default_values['calculationMethod'] == 'Cuda':
            self.method.setCurrentIndex(2)
        self.databaseParams.setTitle(_translate("Form", "Database", None))
        self.label_filename.setText(_translate("Form", "Filename", None))
        self.databaseFilename.setText(_translate("Form", default_values['databaseFile'].split('/')[-1], None))
        self.label_description.setText(_translate("Form", "Description", None))
        self.description.setText(_translate("Form", "Default", None))
        self.label_parameters.setText(_translate("Form", "Parameters", None))

        
    def getValues(self):

        settingsPar = dict()

        # database file
        
        settingsPar['databaseFile'] = '{}/{}'.format(self.databaseFolder.text(), self.databaseFilename.text())
        
        # description
        settingsPar['description'] = str(self.description.text())
        settingsPar['saveDatabase'] = self.saveSimulations.isChecked()
        settingsPar['saveLog'] = self.saveLog.isChecked()

        settingsPar['startTime'] = time.strftime("%H:%M:%S")
        settingsPar['date'] = str(datetime.date.today())
        settingsPar['start'] = time.time()

        settingsPar['saveFigure'] = self.saveFigure.isChecked()

        # to implement
        if self.method.currentText() == "Split Process (Windows)":
            settingsPar['calculationMethod'] = 'splitProcess'
        elif self.method.currentText() == "Parallel Cores (Unix)":
            settingsPar['calculationMethod'] = 'parallelPool'
        else:
            settingsPar['calculationMethod'] = 'Cuda'


        # check buttons
        settingsSave = dict()
        checkBoxValues = self.checkBoxValues[::-1]
        for checkedBox in self.checkBox:
            checked = checkedBox.isChecked()
            settingsSave[checkBoxValues.pop()] = int(checked)

        return settingsPar, settingsSave



    def browse_f(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self, 'Open file', '')
        self.databaseFolder.setText(folder)


    def openSettingsWindow(self, window):
        if window == 'figure':
            self.figureSettingsWidget = FigureSettings(self)
            window = self.figureSettingsWidget
            window.show()
        if window == 'video':
            self.videoSettingsWidget = VideoSettings(self)
            window = self.videoSettingsWidget
            window.show()


class FigureSettings(QtGui.QWidget):
    def __init__(self, settingsWidget):
        super(FigureSettings, self).__init__()

        FigureSettingsWidget = QtGui.QWidget()

        # Initialize member variables       
        self.settingsWidget = settingsWidget

        self.valueChangedTime = time.time()-1.
        # Call child constructor
        self.setupUi(FigureSettingsWidget)
        self.center()


    def setupUi(self, FigureSettingsWidget):
        FigureSettingsWidget.setObjectName(_fromUtf8("FigureSettingsWidget"))
        FigureSettingsWidget.resize(300, 200)

        self.numberOfPlots_lbl = QtGui.QLabel('Number of Plots')
        self.numberOfPlots = QtGui.QSpinBox(value = 2)
        self.numberOfPlots.valueChanged.connect(lambda: self.numberValueChanged(button='plots'))
        self.numberOfRows_lbl = QtGui.QLabel('Number of Rows')
        self.numberOfRows = QtGui.QSpinBox(value = 1)
        self.numberOfRows.valueChanged.connect(self.numberValueChanged)
        self.numberOfColumns_lbl = QtGui.QLabel('Number of Columns')
        self.numberOfColumns = QtGui.QSpinBox(value = 2)
        self.numberOfColumns.valueChanged.connect(self.numberValueChanged)
        self.colormap_lbl = QtGui.QLabel('Colormap')
        self.colormap = QtGui.QComboBox()

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.numberOfPlots_lbl, self.numberOfPlots)
        fbox.addRow(self.numberOfRows_lbl, self.numberOfRows)
        fbox.addRow(self.numberOfColumns_lbl, self.numberOfColumns)
        fbox.addRow(self.colormap_lbl, self.colormap)
 
        # set up canvas
        self.figure, self.ax = plt.subplots(
                                        self.numberOfRows.value(), 
                                        self.numberOfColumns.value(),
                                        facecolor='None',
                                        edgecolor='None'
                                    )

        self.canvas = FigureCanvas(self.figure)
        self.figure.canvas.mpl_connect('button_press_event', self.onClick)


        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(fbox)
        vbox.addWidget(self.canvas)
        self.setLayout(vbox)

        self.setWindowTitle('Figure Settings')
        self.setGeometry(QtCore.QRect(800, 800, 1500, 800))


    def numberValueChanged(self, button=''):
        if time.time()-self.valueChangedTime < 0.1: return
        self.valueChangedTime = time.time()

        if button == 'plots':
            number = self.numberOfPlots.value()
            columns = self.numberOfColumns.value()
            self.numberOfRows.setValue(number/columns)

        else:
            rows = self.numberOfRows.value()
            self.numberOfRows.setValue(rows)
            columns = self.numberOfColumns.value()
            self.numberOfPlots.setValue(rows*columns)

        self.initImage()


    def initImage(self):
        self.canvas.figure.clf()
        plt.clf()
        plt.cla()
        self.figure.clear()

        self.figure, self.ax = plt.subplots(
                                        self.numberOfRows.value(), 
                                        self.numberOfColumns.value(),
                                        facecolor='None',
                                        edgecolor='None'
                                    )

        # plt.subplots_adjust(wspace=0,hspace=0)


        # # turn off axis label
        # for i in range(self.ax.shape[0]):
        #     # column
        #     if len(self.ax.shape) == 1:
        #         self.ax[i].xaxis.set_ticklabels([])
        #         self.ax[i].yaxis.set_ticklabels([])
        #         self.ax[i].set_xticks([])
        #         self.ax[i].set_yticks([])

        #     else:
        #         for j in range(self.ax.shape[1]):
        #             # row
        #             self.ax[i, j].xaxis.set_ticklabels([])
        #             self.ax[i, j].yaxis.set_ticklabels([])
        #             self.ax[i, j].set_xticks([])
        #             self.ax[i, j].set_yticks([])

        # plt.subplots_adjust(left=0.0, right=1.0, top=1.0, bottom=0.0)

        # show image
        self.canvas = FigureCanvas(self.figure)
        self.canvas.draw()
    

    def onClick(self, event):
        if event.inaxes is not None:
            axis = event.inaxes.get_axes()
            print (axis.get_geometry()[2]-1)


    def center(self):
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())





class VideoSettings(QtGui.QWidget):
    def __init__(self, settingsWidget):
        super(VideoSettings, self).__init__()

        VideoSettingsWidget = QtGui.QWidget()
        # Initialize member variables       
        self.settingsWidget = settingsWidget


        self.setupUi(VideoSettingsWidget)
        self.center()


    def setupUi(self, VideoSettingsWidget):
        FigureSettingsWidget.setObjectName(_fromUtf8("FigureSettingsWidget"))
        FigureSettingsWidget.resize(300, 200)

        self.figureSize_lbl = QtGui.QLabel('Size (w,h)')
        self.figureSize = QtGui.QLineEdit()
        self.markerSize_lbl = QtGui.QLabel('Marker Size (Factor of Particle Size)')
        self.markerSize = QtGui.QLineEdit()
        self.maximum_lbl = QtGui.QLabel('Maximum (W/m2)')
        self.maximum = QtGui.QLineEdit()
        self.colorNorm_lbl = QtGui.QLabel('Color Normalization')
        self.colorNorm = QtGui.QLineEdit()
        self.colormap_lbl = QtGui.QLabel('Colormap')
        self.colormap = QtGui.QComboBox()
        self.numberOfSteps_lbl = QtGui.QLabel('Number of Steps')
        self.numberOfSteps = QtGui.QLineEdit()

        self.buttonBox = QtGui.QDialogButtonBox()
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))

        self.buttonBox.accepted.connect(self.saveVideoSettings)
        self.buttonBox.rejected.connect(self.close)

        fbox = QtGui.QFormLayout()
        fbox.addRow(self.figureSize_lbl, self.figureSize)
        fbox.addRow(self.markerSize_lbl, self.markerSize)
        fbox.addRow(self.maximum_lbl, self.maximum)
        fbox.addRow(self.colorNorm_lbl, self.colorNorm)
 
        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(fbox)
        vbox.addWidget(self.buttonBox)
        self.setLayout(vbox)

        self.setWindowTitle('Video Settings')
        # self.setGeometry(QtCore.QRect(800, 800, 1500, 800))


    def saveVideoSettings(self):
        pass

    def center(self):
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())