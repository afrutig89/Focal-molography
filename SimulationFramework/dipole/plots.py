# -*- coding: utf-8 -*-
"""
Different kinds of plots of the electric field and intensity.

@ author:   Silvio
@ created:  Wed Mar 2 16:18:17 2016
"""
from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import numpy as np
from numpy import *
from mologram.generatePattern import plotPattern

from SharedLibraries.Database.dataBaseOperations import *
# plot tools
import matplotlib.pyplot as plt
from matplotlib import colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

# time and date
import datetime


def plotIntensity(screen, intensity, dipolePositions, focalPoint):
    """plots and saves the scatterer pattern and the corresponding field and intensity at the screen.
    
    :param screen: screen where the fields / intensity has been calculated
    :param intensity: intensity calculated at the screen
    :param dipolePositions: positions of the dipole
    :param focalPoint: position of the focal point (3D-vector)
    :param c: counter for the plot
    :param type: choose "contour" or "screen" to either plot the contour or the screen of the intensity
    :param dpi: dots per inch
    """
    x_sca, y_sca, z_sca = dipolePositions

    # pattern
    fig = plt.figure(figsize=figSizeQuadruple)
    
    plt.subplot(221)
    plotPattern(x_sca, y_sca, markersize=3/np.log10(size(dipolePositions)))
    
    # intensity
    plt.subplot(222)
    plotParameter(screen, focalPoint, intensity.real, 'Focus Spot ')
    
    plt.subplot(223)
    plotParameter(screen, focalPoint, intensity.real, 'Cross Section ', axis=1)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    
    plt.subplot(224)
    plotParameter(screen, focalPoint, intensity.real, 'Cross Section ', axis=2)
    
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    fig.tight_layout()


    return fig
    

def plotParameter(screen, focalPoint, parameter, name='', axis=0, levels=1000):
    """plots and saves the scatterer pattern and the corresponding field and intensity at the screen.
    
    :param screen: screen where the fields / intensity has been calculated
    :param intensity: intensity calculated at the screen
    :param dipolePositions: positions of the dipole
    :param focalPoint: position of the focal point (3D-vector)
    :param c: counter for the plot
    :param type: choose "contour" or "screen" to either plot the contour or the screen of the intensity
    :param dpi: dots per inch
    """
    
    # returns the two screens (one axis is equal to 0)
    parameter = parameter.real
    screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(screen, focalPoint)
    
    # parameter
    if axis == 0:
        # CS = plt.contourf(screen_1*1e6, screen_2*1e6, parameter, 
        #                 levels = np.max(parameter.real)*np.linspace(0,1,levels)**2,
        #                 cmap=plt.cm.inferno,
        #                 origin = 'upper',
        #                 norm = colors.PowerNorm(gamma=1/2.))

        extent = (np.min(screen_1),np.max(screen_1),np.min(screen_2),np.max(screen_2))
        CS = plt.imshow(parameter, 
                        cmap = plt.cm.inferno,
                        origin = 'upper',
                        extent = [border*1e6 for border in extent],
                        norm = colors.PowerNorm(gamma=1/2.))
        
        plt.xlabel(axis1)
        plt.ylabel(axis2)
        plt.axes().set_aspect('equal')
        if name != '':
            plt.title(name + " " + title)
        
        ax = plt.gca()
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="5%", pad=0.1)
        cbar = plt.colorbar(CS, cax = cax1, ticks=np.linspace(0,np.max(parameter.real),4), format='%.0e')
        cbar.ax.tick_params(labelsize=10) 

    else:
        if axis == 1:
            if (np.diff(screen_1[0,:]) == 0).all():
                x = screen_1[:,0]
                plt.plot(x*1e6,parameter[:,idx1])
            else:
                x = screen_1[0,:]
                plt.plot(x*1e6,parameter[idx1,:])
            plt.xlabel(axis1)
        elif axis == 2:
            if (np.diff(screen_2[0,:]) == 0).all():
                x = screen_2[:,0]
                plt.plot(x*1e6,parameter[:,idx2])
            else:
                x = screen_2[0,:] 
                plt.plot(x*1e6,parameter[idx2,:])
            plt.xlabel(axis2)
            plt.xlim([np.min(x)*1e6,np.max(x)*1e6])
            
        plt.ylabel('Irradiance')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        if name != '':
            plt.title(name + " " + title)

    return plt


def findCoordinates(screen, focalPoint=0):
    """
    Returns the axes values and names for a specific focal point. This 
    function is needed since the variable 'screen' is a meshgrid, 
    therefore one does not know which coordinate is constant. This 
    allows plotting of every axis without taking care of the screen.

    :param screen: meshgrid of screen (3 axes, one constant)
    :param focalPoint: focal point of the mologram
    :returns: a 2d meshgrid, the name of the axes, the index of the 
    focal point such as the title of a plot.
    """
    screen_x, screen_y, screen_z = screen
    tolerance = 1e-15
    if sum(screen_x)/size(screen_x) > screen_x[0][0]-tolerance and sum(screen_x)/size(screen_x) < screen_x[0][0]+tolerance :
        axis1 = r"y in $\rm{\mu}$m"
        idx1 = (np.abs(screen_y[:,0])).argmin()
        axis2 = r"z in $\rm{\mu}$m"
        idx2 = (np.abs(screen_z[0,:]-focalPoint)).argmin()
        title = "at x = " + str(int(1e6*screen_x[0][0]))
        z = True
        return screen_y, axis1, idx1, screen_z, axis2, idx2, title, z
        
    elif sum(screen_y)/size(screen_y) > screen_y[0][0]-tolerance and sum(screen_y)/size(screen_y) < screen_y[0][0]+tolerance:
        axis1 = r"x in $\rm{\mu}$m"
        idx1 = (np.abs(screen_x[:,0])).argmin()
        axis2 = r"z in $\rm{\mu}$m"
        idx2 = (np.abs(screen_z[0,:]-focalPoint)).argmin()
        title = "at y = " + str(int(1e6*screen_y[0][0]))
        z = True
        return screen_x, axis1, idx1, screen_z, axis2, idx2, title, z

    elif sum(screen_z)/size(screen_z) > screen_z[0][0]-tolerance and sum(screen_z)/size(screen_z) < screen_z[0][0]+tolerance:
        axis1 = r"x in $\rm{\mu}$m"
        idx1 = (np.abs(screen_x[0,:])).argmin()
        axis2 = r"y in $\rm{\mu}$m"
        idx2 = (np.abs(screen_y[:,0])).argmin()
        title = "at z = " + str(int(1e6*screen_z[0][0]))
        z = False
        return screen_x, axis1, idx1, screen_y, axis2, idx2, title, z
        
    else:
        axis1 = r"x in $\rm{\mu}$m"
        idx1 = (np.abs(screen_x[0,:])).argmin()
        axis2 = r"y in $\rm{\mu}$m"
        idx2 = (np.abs(screen_y[:,0])).argmin()
        title = ""
        z = False
        return screen_x, axis1, idx1, screen_y, axis2, idx2, title, z



if __name__ == '__main__':
    # todo
    pass