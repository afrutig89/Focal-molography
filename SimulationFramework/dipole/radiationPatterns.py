# -*- coding: utf-8 -*-
"""
Calculates the power distributed into different parts of the configuration (upper half space, waveguide, lower half space with allowed and forbidden zones (evanescent waves)).

Based on section 10.6 Novotny

@author: Silvio
"""

import numpy as np
from numpy import pi, conj
from scipy.integrate import quad
import cmath
import sys
sys.path.append("../")
from interfaces.fresnelFormulae import reflectionCoefficientSingleLayer, transmissionCoefficientSingleLayer,criticalAngle

def P_tot(dipoleMoment, z_0, k_0, delta, refractiveIndices):
    """
    Equation that describes the normalized rate of energy dissipation. 
    Novotny Eq. 10.26
    
    .. math:: \\frac{P}{{{P_0}}} = 1 + \\frac{{{\\mu _x}^2 + {\\mu _y}^2}}{{{p^2}}}\\frac{3}{4}\\int\\limits_0^\\infty  {\\operatorname{Re} \\left\\{ {\\frac{s}{{{s_z}}}\\left[ {{r^s} - {s_z}^2{r^p}} \\right]{e^{2i{k_1}{z_0}{s_z}}}} \\right\\}ds}  + \\frac{{{\\mu _z}^2}}{{{p^2}}}\\frac{3}{2}\\int\\limits_0^\\infty  {\\operatorname{Re} \\left\\{ {\\frac{{{s^3}}}{{{s_z}}}{r^p}{e^{2i{k_1}{z_0}{s_z}}}} \\right\\}ds} 

    Checked AF: This function is implemented correctly. It remains to be tested, whether the subfunction are correct.  
    """
    n_1, n_f, n_n = refractiveIndices
    eps_1, eps_f, eps_n = refractiveIndices**2
    k_1, k_f, k_n = refractiveIndices*k_0
    p_x, p_y, p_z = dipoleMoment
    # absolute value of the dipole moment. 
    abs_p = np.sqrt(p_x**2 + p_y**2 + p_z**2)
    
    s_z = lambda s: cmath.sqrt(1 - s**2)
    k_z1 = lambda s: k_1 * cmath.sqrt(1-s**2)
    k_z2 = lambda s: k_f * cmath.sqrt(1-s**2)
    k_z3 = lambda s: k_n * cmath.sqrt(1-s**2)

    # checked AF:
    r_p12 = lambda s: (eps_f*k_z1(s) - eps_1*k_z2(s))/(eps_f*k_z1(s)+eps_1*k_z2(s))
    r_s12 = lambda s: (k_z1(s)-k_z2(s))/(k_z1(s)+k_z2(s))
    r_p23 = lambda s: (eps_f*k_z2(s)-eps_1*k_z3(s))/(eps_f*k_z2(s)+eps_1*k_z3(s))
    r_s23 = lambda s: (k_z2(s)-k_z3(s))/(k_z2(s)+k_z3(s))
    
    # 10.20 Novotny checked AF:
    r_p = lambda s: (r_p12(s)+r_p23(s)*np.exp(2j*k_z2(s)*delta))/(1+r_p12(s)*r_p23(s)*np.exp(2j*k_z2(s)*delta))
    r_s = lambda s: (r_s12(s)+r_s23(s)*np.exp(2j*k_z2(s)*delta))/(1+r_s12(s)*r_s23(s)*np.exp(2j*k_z2(s)*delta))

    # checked AF:
    integrand_1 = lambda s: (s/s_z(s)*(r_s(s)-s_z(s)**2*r_p(s))*np.exp(2j*k_1*z_0*s_z(s))).real
    integrand_2 = lambda s: (s**3/s_z(s) * r_p(s) * np.exp(2j*k_1*z_0*s_z(s))).real
    
    P_planeWaves = 1 + (p_x**2+p_y**2)/abs_p**2 * 3/4. * \
            quad(integrand_1,0,1)[0] + \
            p_z**2/abs_p**2 * 3/2. * \
            quad(integrand_2,0,1)[0]
            
    P_evanescentWaves = 1 + (p_x**2+p_y**2)/abs_p**2 * 3/4. * \
            quad(integrand_1,1,np.inf)[0] + \
            p_z**2/abs_p**2 * 3/2. * \
            quad(integrand_2,1,np.inf)[0]
    
    return (P_planeWaves, P_evanescentWaves)
    
def PUpperHalfspace(dipoleMoment, z_0, k_0, delta, refractiveIndices):
    """
    Novotny Eq. 10.48
    """
    k_1 = refractiveIndices[0]*k_0
    p_x, p_y, p_z = dipoleMoment
    abs_p = np.sqrt(p_x**2 + p_y**2 + p_z**2)
    
    s_z = lambda s: cmath.sqrt(1-s**2)
    r_p = lambda s: reflectionCoefficientSingleLayer(delta, k_1, np.arcsin(s), refractiveIndices)[0]
    r_s = lambda s: reflectionCoefficientSingleLayer(delta, k_1, np.arcsin(s), refractiveIndices)[1]

    integrand_1 = lambda s: s*s_z(s)*r_p(s)*conj(r_p(s)) + s/s_z(s)*r_s(s)*conj(r_s(s))
    integrand_2 = lambda s: ((s*s_z(s)*r_p(s) - s/s_z(s)*r_s(s))*np.exp(2*1j*k_1*z_0*s_z(s))).real
    integrand_3 = lambda s: s**3/s_z(s)*r_p(s)*conj(r_p(s))**2
    integrand_4 = lambda s: (s**3/s_z(s)*r_p(s) * np.exp(2*1j*k_1*z_0*s_z(s)))
    P_upper = (p_x**2+p_y**2)/abs_p**2 * \
                    (1/2.+3/8.*quad(integrand_1,0,1)[0] - 3/4.*quad(integrand_2,0,1)[0]) + \
                    p_z**2/abs_p**2 * \
                    (1/2. + 3/4.*quad(integrand_3,0,1)[0] + 3/2.*quad(integrand_4,0,1)[0])
    return P_upper
    
def PLowerHalfspace(dipoleMoment, z_0, k_0, delta, refractiveIndices):
    """
    Novotny Eq. 10.49
    """
#    n_1, n_f, n_n = refractiveIndices
#    eps_1, eps_f, eps_n = refractiveIndices**2
#    k_1, k_f, k_n = refractiveIndices*k_0
#    p_x, p_y, p_z = dipoleMoment
#    abs_p = np.sqrt(p_x**2 + p_y**2 + p_z**2)
#    
#    s_z_tilde = lambda s: cmath.sqrt(1-s**2).imag
#    t_p = lambda s: transmissionCoefficientSingleLayer(delta, k_1, np.arcsin(n_1/n_n*s), refractiveIndices)[0]
#    t_s = lambda s: transmissionCoefficientSingleLayer(delta, k_1, np.arcsin(n_1/n_n*s), refractiveIndices)[1]
#    
#    integrand_1 = lambda s: s*cmath.sqrt(1-(n_1/n_n)**2*s**2) * \
#                            (t_p(s)*conj(t_p(s)) + t_s(s)*conj(t_s(s))/((1-s**2)*conj((1-s**2)))) * \
#                            np.exp(-2*k_1*z_0*s_z_tilde(s))
#    integrand_2 = lambda s: s**3 * cmath.sqrt(1-(n_1/n_n)**2*s**2) * \
#                            t_p(s)*conj(t_p(s))/((1-s**2)*conj((1-s**2))) * \
#                            np.exp(-2*k_1*z_0*s_z_tilde(s))
#                            
#    P_lower = 3/8.*(p_x**2+p_y**2)/abs_p**2 * eps_n/eps_1 * n_1/n_n * \
#                quad(integrand_1,0,n_n/n_1)[0] + \
#                3/4.*p_z**2/abs_p**2 * eps_n/eps_1 * n_1/n_n * \
#                quad(integrand_2,0,n_n/n_1)[0]

    P_lower = PAllowedLight(dipoleMoment, k_0, delta, refractiveIndices) + \
                PForbiddenLight(dipoleMoment, z_0, k_0, delta, refractiveIndices)
                
    return P_lower
    
def PAllowedLight(dipoleMoment, k_0, delta, refractiveIndices):
    """
    Novotny Eq. 10.50
    """
    n_1, n_f, n_n = refractiveIndices
    eps_1, eps_f, eps_n = refractiveIndices**2
    k_1, k_f, k_n = refractiveIndices*k_0
    p_x, p_y, p_z = dipoleMoment
    abs_p = np.sqrt(p_x**2 + p_y**2 + p_z**2)
    
    t_p = lambda s: transmissionCoefficientSingleLayer(delta, k_1, np.arcsin(n_1/n_n*s), refractiveIndices)[0]
    t_s = lambda s: transmissionCoefficientSingleLayer(delta, k_1, np.arcsin(n_1/n_n*s), refractiveIndices)[1]
    
    integrand_1 = lambda s: s*cmath.sqrt(1-(n_1/n_n)**2*s**2) * \
                            (t_p(s)*conj(t_p(s)) + t_s(s)*conj(t_s(s))/(1-s**2))
    integrand_2 = lambda s: s**3 * cmath.sqrt(1-(n_1/n_n)**2*s**2) * \
                            t_p(s)*conj(t_p(s))/(1-s**2)
                     
    # avoid s=1 (singularity)
    P_allowed = 3/8.*(p_x**2+p_y**2)/abs_p**2 * eps_n/eps_1 * n_1/n_n * \
                quad(integrand_1,0,0.99)[0] + \
                3/4.*p_z**2/abs_p**2 * eps_n/eps_1 * n_1/n_n * \
                quad(integrand_2,0,0.99)[0]
                
    return P_allowed 

    
def PForbiddenLight(dipoleMoment, z_0, k_0, delta, refractiveIndices):

    n_1, n_f, n_n = refractiveIndices
    eps_1, eps_f, eps_n = refractiveIndices**2
    k_1, k_f, k_n = refractiveIndices*k_0
    p_x, p_y, p_z = dipoleMoment
    abs_p = np.sqrt(p_x**2 + p_y**2 + p_z**2)

    t_p = lambda s: transmissionCoefficientSingleLayer(delta, k_1, np.arcsin(n_1/n_n*s), refractiveIndices)[0]
    t_s = lambda s: transmissionCoefficientSingleLayer(delta, k_1, np.arcsin(n_1/n_n*s), refractiveIndices)[1]
    
    integrand_1 = lambda s: s * cmath.sqrt(1-(n_1/n_n)**2*s**2) * \
                            (t_p(s)*conj(t_p(s)) + t_s(s)*conj(t_s(s))/(s**2-1)) * \
                            np.exp(-2*k_1*z_0*cmath.sqrt(s**2-1))
    integrand_2 = lambda s: s**3 * cmath.sqrt(1-(n_1/n_n)**2*s**2) * \
                            t_p(s)*conj(t_p(s))/(s**2-1) * \
                            np.exp(-2*k_1*z_0*cmath.sqrt(s**2-1))
                            
    # avoid s=1 (singularity)
    P_forbidden = 3/8.*(p_x**2+p_y**2)/abs_p**2 * eps_n/eps_1 * n_1/n_n * \
                quad(integrand_1,1.01,n_n/n_1)[0] + \
                3/4.*p_z**2/abs_p**2 * eps_n/eps_1 * n_1/n_n * \
                quad(integrand_2,1.01,n_n/n_1)[0]
                
    return P_forbidden



    
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    # wavelength = 488e-9
    # k_0 = 2*pi/wavelength
    # dipoleMoment = np.array([[np.sqrt(3)/2],[0],[1/2.]])
    # permittivities = np.array([1, 5, 2.25])
    # refractiveIndices = np.sqrt(permittivities)
    # delta = np.linspace(0,0.5*wavelength,20)
    # h = 80e-9


    # P_a = np.zeros(len(delta))
    # P_f = np.zeros(len(delta))
    # for i in range(len(delta)):
    #     P_a[i] = PAllowedLight(dipoleMoment, k_0, delta[i], refractiveIndices)
    #     P_f[i] = PForbiddenLight(dipoleMoment, h, k_0, delta[i], refractiveIndices)
        
    # plt.plot(delta/wavelength, P_a, delta/wavelength, P_f)
    # plt.ylim([0,1])
    # #plt.show()
    
    ################################################################################################
    ################################################################################################
    # Define waveguide and substrate properties
    d_f = 145e-9     
    wavelength = 632.8e-9
    k_0 = 2*pi/wavelength
    dipoleMoment = np.array([[0],[1],[0]])
    permittivities = np.array([1.518**2, 2.117**2, 1.521**2])
    refractiveIndices = np.sqrt(permittivities)
    h = 5e-9
    
    ################################################################################################
    ################################################################################################
    # Calculate absolute contributions
    
    P_planeWaves, P_evanescentWaves = P_tot(dipoleMoment, h, k_0, d_f, refractiveIndices)
    P_tot = P_planeWaves + P_evanescentWaves
    P_upper = PUpperHalfspace(dipoleMoment, h, k_0, d_f, refractiveIndices)
    P_lower = PLowerHalfspace(dipoleMoment, h, k_0, d_f, refractiveIndices)
    P_m = P_tot - (P_upper+P_lower)
    P_allowed = PAllowedLight(dipoleMoment, k_0, d_f, refractiveIndices)
    P_forbidden = PForbiddenLight(dipoleMoment, h, k_0, d_f, refractiveIndices)
    
    print("P = " + str(P_tot))
    print("P_planeWaves = " + str(P_planeWaves) + " ("+ str((P_planeWaves/P_tot*100)[0]) + "%)")  
    print("P_evanescentWaves = " + str(P_evanescentWaves) + " (" + str((P_evanescentWaves/P_tot*100)[0]) + "%)")  
    
    print('Individual contributions to P_tot:')
    print("P_upper = " + str(P_upper) + " (" + str((P_upper/P_tot*100)[0]) + "%)")
    print("P_allowed = " + str(P_allowed) + " (" + str((P_allowed/P_tot*100)[0]) + "%)")
    print("P_forbidden = " + str(P_forbidden) + " (" + str((P_forbidden/P_tot*100)[0]) + "%)")
    print("P_m = " + str(P_m) + " (" + str((P_m/P_tot*100)[0]) + "%)")


    print("P_lower = " + str(P_lower) + " (" + str((P_lower/P_tot*100)[0]) + "%)")
    # print("P_allowed = " + str(P_allowed) + " (" + str((P_allowed/P_lower*100)[0]) + "%)")

    ################################################################################################
    ################################################################################################
    # Anisotropy of the scattering:

    print "Critical angles:"
    print criticalAngle(1.521,1.)*360./(2*np.pi)
    # Anisotropy of P_Allowed:
    isotropic_contribution = 2*criticalAngle(1.521,1.)/(2*np.pi)*P_tot

    anisotropic_contribution = P_allowed*2*criticalAngle(1.521,1.)/np.pi

    print ("Anisotropy P_allowed = " + str(anisotropic_contribution/isotropic_contribution))
    print ("Anisotropy Substrate induced:" + str((2*criticalAngle(1.521,1.)/np.pi/(2*np.pi))/(2*criticalAngle(1.521,1.)/(2*np.pi))))
    # Anisotropy of P_upper

    isotropic_contribution = np.pi/(2*np.pi)
    anisotropic_contribution = P_upper/P_tot

    print ("Anisotropy P_upper = " + str(anisotropic_contribution/isotropic_contribution))






    