# -*- coding: utf-8 -*-
"""
Dipole emission near planar interfaces according to Novotny and Hecht.

:Reference: Lukas Novotny and Bert Hecht. Dipole emission near 
planar interfaces. In Principles of Nano-Optics. Cambridge 
University Press, 2006.

:author:   Silvio
:created:  Thu Mar 31 15:51:53 2016
"""


from __future__ import print_function, division, absolute_import
from numpy import *
import numpy as np
import sys
sys.path.append("../")
from interfaces.fresnelFormulae import reflectionCoefficientSingleLayer, transmissionCoefficientSingleLayer
import matplotlib.pyplot as plt
import auxiliaries.constants as constants

def calc_intensity_single_dipole(dipole_position, dipole_moment, r, refractive_indices, waveguide_thickness, k_0):
    """
    :param dipole_position: 3 x 1 numpy array of x,y,z coordinates
    :param dipole_moment: 3 x 1 numpy array
    :param r: 3 x 1 numpy array of the position where to evaluate the field
    :param refractive_indices: 3 x 1 numpy array of the refractive indices of the media (medium1 (top) - medium3 (bottom))
    :param waveguide_thickness: float, thickness of the waveguide
    :param k_0: float, wave number in air

    :returns: float of the intensity at position r
    
    """

    if r[2] > 0:
        Z_w = np.sqrt(constants.mu_0)/refractive_indices[0]
    elif (r[2] + waveguide_thickness) < 0:
        Z_w = np.sqrt(constants.mu_0)/refractive_indices[2]
    else:   
        raise RuntimeError('You are in the waveguide!')

    E_sca = calc_field_single_dipole(dipole_position, dipole_moment, r, refractive_indices, waveguide_thickness, k_0)

    return 1/(2*Z_w) * sum(E_sca*np.conj(E_sca), axis=0).real


def calc_field_single_dipole(dipole_position, dipole_moment, r, refractive_indices, waveguide_thickness, k_0):
    """
    :param dipole_position: 3 x 1 numpy array of x,y,z coordinates
    :param dipole_moment: 3 x 1 numpy array
    :param r: 3 x 1 numpy array of the position where to evaluate the field
    :param refractive_indices: 3 x 1 numpy array of the refractive indices of the media (medium1 (top) - medium3 (bottom))
    :param waveguide_thickness: float, thickness of the waveguide
    :param k_0: float, wave number in air

    :returns: 3 x 1 numpy array of the field values at the position r
    
    """
    dipole_position = np.asarray(dipole_position).reshape(3,1)
    dipole_moment = np.asarray(dipole_moment).reshape(3,1)
    phase = np.zeros((1, 1))
    screen = np.vstack(np.meshgrid(r[0], r[1], r[2])).reshape(3, 1, 1)

    return calcField(dipole_position, dipole_moment, phase, screen, refractive_indices, thickness=waveguide_thickness, k_0=k_0)


def calcField(dipolePositions, dipoleMoments, 
                phase, screen, refractiveIndices, thickness, k_0, Phi=None):
    """calculates the field of a dipole located at dipolePositions at 
    the observer position r (point of observation). 

    :param dipolePositions: dipole positions (3d vector x,y,z)
    :param dipoleMoments: dipole moments (3d vector x,y,z)
    :param phase: phase for the different dipole positions
    :param screen: screen as meshgrid
    :param refractiveIndices: refractive indices of the media (medium1 (top) - medium3 (bottom))
    :param thickness: layer (waveguide) thickness
    :param k_0: wave number in air
    :param Phi: vector of the dipole potentials for interpolation.
    :returns: 3d field vector at the screen

    .. note:: Only the farfield is considered.
    :reference:: Equation 10.32 Novotny
    """

    dipolePositions_x, dipolePositions_y, dipolePositions_z = dipolePositions    
    screen_x, screen_y, screen_z = screen
    
    n_dipoles = dipolePositions.shape[1]
    npix_x = screen_x.shape[0]
    npix_y = screen_x.shape[1]
    
    # nomenclature of Novotnys Principles Of Nano-Optics applied
    n_1, n_f, n_n = refractiveIndices

    #distance values in x,y,z from dipole to screen pixel
    #dist_x_all_dipoles_all_pix = screen repeated n_dipole times -(minus) position_of_dipole tiled as big as screen
    dist_x_all_dipoles_all_pix = np.tile(screen_x,(n_dipoles,1,1))-np.repeat(dipolePositions_x, npix_x*npix_y).reshape((n_dipoles,npix_x,npix_y))
    dist_y_all_dipoles_all_pix = np.tile(screen_y,(n_dipoles,1,1))-np.repeat(dipolePositions_y, npix_x*npix_y).reshape((n_dipoles,npix_x,npix_y))
    dist_z_all_dipoles_all_pix = np.tile(screen_z,(n_dipoles,1,1))

    dipolePositions_z = np.repeat(dipolePositions_z, npix_x*npix_y).reshape((n_dipoles,npix_x,npix_y))
    phase = np.repeat(phase, npix_x*npix_y).reshape((n_dipoles,npix_x,npix_y))
    
    # dipole moments
    dipoleMoments_x = np.repeat(dipoleMoments[0,:], npix_x*npix_y).reshape((n_dipoles,npix_x,npix_y))
    dipoleMoments_y = np.repeat(dipoleMoments[1,:], npix_x*npix_y).reshape((n_dipoles,npix_x,npix_y))
    dipoleMoments_z = np.repeat(dipoleMoments[2,:], npix_x*npix_y).reshape((n_dipoles,npix_x,npix_y))

    # radial distance, polar radius and polar angle
    r_for_all_dipoles_all_pix = np.sqrt((dist_x_all_dipoles_all_pix)**2+(dist_y_all_dipoles_all_pix)**2+(dist_z_all_dipoles_all_pix)**2)
    rho_for_all_dipoles_all_pix = np.sqrt((dist_x_all_dipoles_all_pix)**2+(dist_y_all_dipoles_all_pix)**2)
    phi = arctan2(dist_y_all_dipoles_all_pix,dist_x_all_dipoles_all_pix)  
    
    k_1 = k_0*n_1
    
    if (screen_z > 0).all():
        # polar angle (k to the xy-plane)
        theta = arctan2(rho_for_all_dipoles_all_pix, dist_z_all_dipoles_all_pix)

        if Phi == None:

            # exact calculations
            Phi = potentialsUpper(k_0, dipolePositions_z, theta, refractiveIndices, thickness)
            Phi_1 = Phi[0]
            Phi_2 = Phi[1]
            Phi_3 = Phi[2]

        else:
            # calculate potential by interpolation
            theta_intp = np.linspace(0,30,len(Phi[0])) # do not change!
            theta_intp = theta_intp/180.*pi

            Phi_1 = (np.interp(theta,theta_intp,Phi[0].real)+1j*np.interp(theta,theta_intp,Phi[0].imag))*exp(1j*k_1*dipolePositions_z*cos(theta))
            Phi_2 = (np.interp(theta,theta_intp,Phi[1].real)+1j*np.interp(theta,theta_intp,Phi[1].imag))*exp(1j*k_1*dipolePositions_z*cos(theta))
            Phi_3 = (np.interp(theta,theta_intp,Phi[2].real)+1j*np.interp(theta,theta_intp,Phi[2].imag))*exp(1j*k_1*dipolePositions_z*cos(theta))
            
        k_j = k_1
    
    elif (screen_z < 0).all():
        # polar angle (dipole radiation angle differs from the direct poin-to-point angle)
        theta = arctan2(rho_for_all_dipoles_all_pix, abs(dist_z_all_dipoles_all_pix))
        k_n = k_0*n_n

        if Phi == None:
            # no interpolation (comment following 6 lines if active)
            Phi = potentialsLower(k_0, dipolePositions_z, theta, refractiveIndices, thickness)
            Phi_1 = Phi[0]
            Phi_2 = Phi[1]
            Phi_3 = Phi[2]
        else:
            # calculate potential by interpolation
            theta_intp = np.linspace(0,30,len(Phi[0])) # do not change!
            theta_intp = theta_intp/180.*pi
            
            s_z = np.sqrt((n_1/n_n)**2-sin(theta)**2)
            Phi_1 = (np.interp(theta,theta_intp,Phi[0].real)+1j*np.interp(theta,theta_intp,Phi[0].imag)) * exp(1j*k_n * (dipolePositions_z*s_z))
            Phi_2 = (np.interp(theta,theta_intp,Phi[1].real)+1j*np.interp(theta,theta_intp,Phi[1].imag)) * exp(1j*k_n * (dipolePositions_z*s_z))
            Phi_3 = (np.interp(theta,theta_intp,Phi[2].real)+1j*np.interp(theta,theta_intp,Phi[2].imag)) * exp(1j*k_n * (dipolePositions_z*s_z))
        
        k_j = k_n
        
    else:
        return

    eps_m = n_1**2 * constants.eps_0 # epsilon in upper half space
    expfac = 1/(4*pi*eps_m) * k_1**2/r_for_all_dipoles_all_pix * \
        (exp(1j*(k_j*r_for_all_dipoles_all_pix + phase)))   # exponential factor
    
    E_theta = expfac*((dipoleMoments_x*cos(phi)+dipoleMoments_y*sin(phi))*cos(theta)*Phi_2-dipoleMoments_z*sin(theta)*Phi_1)
    E_phi = expfac*(-(dipoleMoments_x*sin(phi)-dipoleMoments_y*cos(phi))*Phi_3)
    E_r = np.zeros(E_phi.shape)
    
    # in cartesian coordinates (simple summation possible)
    E_x = sin(theta)*cos(phi)*E_r + cos(theta)*cos(phi)*E_theta - sin(phi)*E_phi
    E_y = sin(theta)*sin(phi)*E_r + cos(theta)*sin(phi)*E_theta + cos(phi)*E_phi
    E_z = cos(theta)*E_r - sin(theta)*E_theta

    E_tot = np.array([sum(E_x,axis=0),sum(E_y,axis=0),sum(E_z,axis=0)])
    
    return E_tot
    

def partialProjector(
                dipolePositions, dipoleMoments, phase, screen, 
                refractiveIndices, thickness, k_0, Phi, num_process, 
                running_index):
    """
    Splits the array of the dipole positions in order to avoid memory 
    errors and to speed up the simulations (parallel pooling possible).

    :param dipolePositions: positions of the dipoles (ndarray: 3 x n)
    :param dipoleMoments: dipole moments corresponding to the positions
    :param phase: phase at the given positions
    :param screen: screen that is investigated
    :param refractiveIndices: refractive indices of the given media (cover, film, substrate)
    :param thickness: thickness of the waveguide
    :param k_0: wave vector in free space
    :param Phi: potentials
    :param num_process: amount of processes the calculations shall be split in
    :param running_index: the current index for the calculations
    """

    n_dipoles = dipolePositions.shape[1]

    beginning = int((float(running_index))/num_process*n_dipoles)
    min1 = int((running_index+1)*n_dipoles/num_process)
    ending = np.min([min1,n_dipoles])
    #slices the dipoles from which to comptue the intensity in as many chunks as there are parallel processes. 
    #the ending has to be safe... If one has 13 dipoles, that last chunk contains 1 dipole.
    return calcField(
        dipolePositions[:,beginning:ending], dipoleMoments[:,beginning:ending], 
        phase[:,beginning:ending], screen, refractiveIndices, thickness, k_0, Phi)

    
def potentialsUpper(k_0, z_dipole, theta, refractiveIndices, thickness):
    """calculates the potentials for the upper half space when dipoles 
    lie on a optical interface

    :params k_0: wave number in free space
    :param z_dipole: z-components of the dipoles
    :param theta: outgoing angle of the dipole field
    :param refractiveIndices: refractive indices of the cover, film and substrate
    :param thickness: thickness of the waveguide
    :returns: potentials in the lower half space

    :reference: Equations 10.33-10.35 Novotny
    """

    n_1, n_f, n_n = refractiveIndices
    
    r_p, r_s = reflectionCoefficientSingleLayer(thickness, k_0, theta, refractiveIndices)

    k_1 = k_0 * n_1
    
    # potentials for upper half-spaces
    Phi_1 = exp(-1j*k_1*z_dipole*cos(theta))+r_p*exp(1j*k_1*z_dipole*cos(theta))
    Phi_2 = exp(-1j*k_1*z_dipole*cos(theta))-r_p*exp(1j*k_1*z_dipole*cos(theta))
    Phi_3 = exp(-1j*k_1*z_dipole*cos(theta))+r_s*exp(1j*k_1*z_dipole*cos(theta))

    return Phi_1, Phi_2, Phi_3
    
def potentialsLower(k_0, z_dipole, theta, refractiveIndices, thickness):
    """calculates the potentials for the lower half space when dipoles 
    lie on a optical interface.

    :params k_0: wave number in free space
    :param z_dipole: z-components of the dipoles
    :param theta: outgoing angle of the dipole field
    :param refractiveIndices: refractive indices of the cover, film and substrate
    :param thickness: thickness of the waveguide
    :returns: potentials in the upper half space

    :reference: Equation 10.36-10.38 (mistake in book corrected -> Eq. 10.37 sign)
    """

    n_1, n_f, n_n = refractiveIndices
    
    theta2 = arcsin(n_n/n_f*sin(theta))     # angle in waveguide
    theta1 = arcsin(n_f/n_1*sin(theta2))    # angle from dipole
    
    # for the transmission coefficient we have to start from the dipole position
    t_p, t_s = transmissionCoefficientSingleLayer(thickness, k_0, theta1, refractiveIndices)
    
    s_z = np.sqrt((n_1/n_n)**2-sin(theta)**2+0j)  # definition of Fundamentals of Nano-Optics Ch. 10
    k_n = k_0 * n_n                      # wave vector of the medium in the lower half-space

    Phi_1 = n_n/n_1 * cos(theta) / s_z * t_p * exp(1j*k_n * (z_dipole*s_z+thickness*cos(theta)))
    Phi_2 = n_n/n_1 * t_p * exp(1j*k_n * (z_dipole*s_z+thickness*cos(theta)))
    Phi_3 = cos(theta)/s_z * t_s * exp(1j*k_n * (z_dipole*s_z+thickness*cos(theta)))
    
    return Phi_1, Phi_2, Phi_3
        

if __name__ == '__main__':
    from interfaces.configuration import Media
    from materials.materials import IsotropicMaterial
    from waveguide.waveguide import SlabWaveguide
    
    #################################
    ######   plot potentials   ######
    #################################
    
    # init
    savePlots = False

    theta_1 = np.linspace(-30,30,100) # in degrees
    theta = theta_1/180.*pi
    phi = np.linspace(0,2*pi,100)
    
    wavelength = 632.8e-9
    k_0 = 2*pi/wavelength
    d_f = 145e-9
    
    theta, phi = np.meshgrid(theta,phi)
    z_dipole = np.zeros(theta.shape[0])
    theta = np.reshape(theta,[1,theta.shape[0],theta.shape[1]])
    phi = np.reshape(phi, [1,phi.shape[0],phi.shape[1]])

    glassSubstrate = IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521)
    aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33)
    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=glassSubstrate.n, n_f=2.117, 
                          n_c=aqueousMedium.n, d_f=d_f, polarization='TE', 
                            inputPower=1, wavelength = wavelength,
                            attenuationConstant=0.8)
                        
    configuration = Media([inf,0,-d_f,-inf],[aqueousMedium, waveguide, glassSubstrate])
    refractiveIndices = np.array([medium.n for medium in configuration.media])

    # calculate the field of a single protein dipole below and above the waveguide
    from dipole import Dipole
    from materials.materials import Particle


    wavelength = 632.8e-9
    analyte_Radius = ''
    n_medium = 1.33

    analyte = Particle('Streptavidin', wavelength, analyte_Radius)
    
    Protein_dipole = Dipole(analyte, wavelength, n_medium)

    dipole_position = np.zeros((3, 1))
    phase = np.zeros((1, 1))
    r = np.array([0, 0, 677e-6])

    print(dipole_position.shape[1])

    print(calc_field_single_dipole(dipole_position, Protein_dipole.staticDipoleMoment, r, refractiveIndices, d_f, k_0))
    print(calc_intensity_single_dipole(dipole_position, Protein_dipole.staticDipoleMoment, r, refractiveIndices, d_f, k_0))
    r = np.array([0, 0, -900e-6])
    print(calc_intensity_single_dipole(dipole_position, Protein_dipole.staticDipoleMoment, r, refractiveIndices, d_f, k_0))



    Phi_Upper = potentialsUpper(k_0, z_dipole, theta, refractiveIndices, d_f)
    Phi_Lower = potentialsLower(k_0, z_dipole, theta, refractiveIndices, d_f)



    fig1 = plt.figure()
    plt.subplot(131)
    plt.plot(theta_1, Phi_Upper[0][0,0,:])
    plt.title(r'$\Phi_1^{(1)}$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(132)
    plt.plot(theta_1, Phi_Upper[1][0,0,:])
    plt.title(r'$\Phi_1^{(2)}$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(133)
    plt.plot(theta_1, Phi_Upper[2][0,0,:])
    plt.title(r'$\Phi_1^{(3)}$')
    plt.xlabel('incident angle in degrees')
    plt.suptitle('Potentials in the Upper Medium (real part)', y=1.03)
    
    plt.tight_layout()
    plt.show()
        
    fig2 = plt.figure()
    plt.subplot(131)
    plt.plot(theta_1, Phi_Lower[0][0,0,:])
    plt.title(r'$\Phi_n^{(1)}$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(132)
    plt.plot(theta_1, Phi_Lower[1][0,0,:])
    plt.title(r'$\Phi_n^{(2)}$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(133)
    plt.plot(theta_1, Phi_Lower[2][0,0,:])
    plt.title(r'$\Phi_n^{(3)}$')
    plt.xlabel('incident angle in degrees')
    plt.suptitle('Potentials in the Lower Medium (real part)', y=1.03)

    plt.tight_layout()
    plt.show()
            
    fig3 = plt.figure()
    plt.subplot(131)
    plt.plot(theta_1, Phi_Lower[0][0,0,:].imag)
    plt.title(r'$\Phi_n^{(1)}$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(132)
    plt.plot(theta_1, Phi_Lower[1][0,0,:].imag)
    plt.title(r'$\Phi_n^{(2)}$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(133)
    plt.plot(theta_1, Phi_Lower[2][0,0,:].imag)
    plt.title(r'$\Phi_n^{(3)}$')
    plt.xlabel('incident angle in degrees')
    plt.suptitle('Potentials in the Lower Medium (imaginary part)', y=1.03)
   
    plt.tight_layout()
    plt.show()
                
    fig3 = plt.figure()
    plt.subplot(131)
    plt.plot(theta_1, Phi_Lower[0][0,0,:]*conj(Phi_Lower[0][0,0,:]))
    plt.title(r'$\left|\Phi_n^{(1)}\right|^2$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(132)
    plt.plot(theta_1, Phi_Lower[1][0,0,:]*conj(Phi_Lower[1][0,0,:]))
    plt.title(r'$\left|\Phi_n^{(2)}\right|^2$')
    plt.xlabel('incident angle in degrees')
    plt.subplot(133)
    plt.plot(theta_1, Phi_Lower[2][0,0,:]*conj(Phi_Lower[2][0,0,:]))
    plt.title(r'$\left|\Phi_n^{(3)}\right|^2$')
    plt.xlabel('incident angle in degrees')
    plt.suptitle('Potentials in the Lower Medium (square)', y=1.03)
     
    plt.tight_layout()
    plt.show()

    if savePlots:
        fig1.savefig('/Potentials_1.')
        fig2.savefig('/Potentials_real_n.')
        fig3.savefig('/Potentials_imag_n.')
        fig3.savefig('/Potentials_abs_n.')

