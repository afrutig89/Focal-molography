# -*- coding: utf-8 -*-
"""
This module provides the basic characteristics of a static electric 
dipole such as dipole polarizability, the static dipole moment, the 
dipole radiation as well as the scattering, absorption and extinction 
cross sections.

:author:   Silvio Bischof

"""

import numpy as np
from numpy import pi, sqrt, conj
import sys
sys.path.append("../")
import auxiliaries.constants as constants
from scipy.special import spherical_jn, spherical_yn
import logging
from materials.materials import Particle

def dipolePolarizability(a, eps_p, eps_m):
    """Calculates the (static) polarizability of a dipole
      :param a: radius of the particle (sphere)
      :param eps_p: permittivity of the particle
      :param eps_m: permittivity of the medium surrounding the particle

      .. math:: \\alpha  = \\frac{{4\\pi {a^3}\\left( {{\\epsilon _p} - {\\epsilon _m}} \\right)}}{{{\\epsilon _p} + 2{\\epsilon _m}}}
      
      :reference: Equation 5.15 Bohren and Huffman
      .. note:: radiation correction needed for strong scatterers (large dipole polarization)

      

    """

    alpha = 4 * pi * a**3 * (eps_p-eps_m)/(eps_p + 2*eps_m)
    
    return alpha
      
def staticDipoleMoment(a, eps_p, eps_m, E_0):
    """Calculates the static dipole moment of a sphere with radius a. 
      
    :param a: radius of the particle (sphere)
    :param eps_p: relative permittivity of the particle
    :param eps_m: relative permittivity of the medium surrounding the particle
    :param E_0: applied electric field (3D-vector)

    :returns: dipole moment (3D vector)
      
    :reference: Equation 5.15 Bohren and Huffman

    .. math:: \\vec p = {\\epsilon _0}{\\epsilon _p}\\alpha {{\\vec E}_0}

    """
    logging.debug('particle radii')
    logging.debug(a)
    logging.debug('particle polarizability')
    logging.debug(eps_p)
    
    alpha = dipolePolarizability(a, eps_p, eps_m)
    p = constants.eps_0 * eps_m * alpha * E_0

    logging.info('Dipole polarizabilities calculated')

    return p
      
def dipoleRadiation(p, wavelength, n=1):
    """Calculates the total dipole radiation in a certain medium with
    refractive index n.
    
    :param p: dipole moment
    :param wavelength: wavelength
    :param n: refractive index

    .. math:: P = \\frac{{{{\\left| {\\vec p} \\right|}^2}}}{{4\\pi {\\epsilon _0}{\\epsilon _p}}} \\cdot \\frac{{2^4{\\pi ^4}}}{{3{\\lambda ^4}}} \\cdot \\frac{{{\\epsilon _p}^2c}}{{\\sqrt {{\\epsilon _p}} }} = \\frac{{{{\\left| {\\vec p} \\right|}^2}}}{{4\\pi {\\epsilon _0}{\\epsilon _p}}} \\cdot \\frac{{{n^3}{\\omega ^4}}}{{3{c^3}}}

    :reference: Equation 8.71 Novotny rearranged with omega=k*c=2*pi/lambda*c

    """

    eps_p = n**2
    P = abs(p)**2/(4*pi*constants.eps_0*eps_p) * \
            (2*pi)**4/(3*wavelength**4) * \
            eps_p**2*constants.c/sqrt(eps_p)

    return P

def scatteringCrossSection(k, a, eps_p, eps_m):
    """
    Calculates the scattering cross section of a particle with particle 
    radius a and relative permittivity eps_p in a medium with relative 
    permittivity eps_m and excited by an electric field with wave 
    vector k.

    :param k: wave vector of the incident wave
    :param a: radius of the particle
    :param eps_p: relative permittivity of the particle
    :param eps_m: relative permittivity of the surrounding medium

    .. math:: {\\sigma _{sc}} = \\frac{{{k^4}}}{{6\\pi }}\\alpha {\\alpha ^*}

    :reference: Equation 5.19 Bohren & Huffman
    """
    
    alpha = dipolePolarizability(a, eps_p, eps_m)
    sigma_sca = k**4/(6*pi)*(alpha*conj(alpha))

    return sigma_sca.real
    
def absorptionCrossSection(k, a, eps_p, eps_m):
    """
    Calculates the absorption cross section of a particle with particle 
    radius a and relative permittivity eps_p in a medium with relative 
    permittivity eps_m and excited by an electric field with wave 
    vector k.

    :param k: wave vector of the incident wave
    :param a: radius of the particle
    :param eps_p: relative permittivity of the particle
    :param eps_m: relative permittivity of the surrounding medium

    .. math:: {\\sigma _a} = k{\\mathop{\\rm Im}\\nolimits} \\left( \\alpha  \\right)

    :reference: Equation 5.18 Bohren & Huffman
    """

    alpha = dipolePolarizability(a, eps_p, eps_m)
    sigma_abs = k*alpha.imag

    return sigma_abs.real
    
def extinctionCrossSection(k, a, eps_p, eps_m):
    """
    Calculates the extinction cross section of a particle with particle 
    radius a and relative permittivity eps_p in a medium with relative 
    permittivity eps_m and excited by an electric field with wave 
    vector k.

    :param k: wave vector of the incident wave
    :param a: radius of the particle
    :param eps_p: relative permittivity of the particle
    :param eps_m: relative permittivity of the surrounding medium

    .. math:: \\sigma_{ext} = \\sigma_{sc} + \\sigma_{a}

    :reference: Equation 3.25 Bohren & Huffman
    """

    sigma_sca = scatteringCrossSection(k,a,eps_p,eps_m)
    sigma_abs = absorptionCrossSection(k,a,eps_p,eps_m)

    return sigma_sca+sigma_abs


class Dipole(object):

    def __init__(self, particle, wavelength, n_medium):

        self.eps_p = complex(particle.refractiveIndex)**2 # particle permittivity
        self.eps_m = n_medium**2 # medium which surrounds particle
        self.a = particle.radius
        # polarizability
        self.polarizability = dipolePolarizability(self.a, self.eps_p, self.eps_m)
        print("Static Polarizability: " + str(self.polarizability))
        
        # static dipole moment
        E_0 = np.array([0, 1, 0])
        self.staticDipoleMoment = staticDipoleMoment(self.a, self.eps_p, self.eps_m, E_0)
        print("Static dipole moment: " + str(self.staticDipoleMoment))

        
        # dipole radiation
        self.dipoleRadation = dipoleRadiation(self.staticDipoleMoment, wavelength, n_medium)
        print("Dipole radiation in medium with refractive index " + str(n_medium) + ": " + str(self.dipoleRadation))
        
        # cross sections
        k = n_medium*2*pi/wavelength
        self.C_sca = scatteringCrossSection(k,self.a,self.eps_p,self.eps_m)
        self.C_abs = absorptionCrossSection(k,self.a,self.eps_p,self.eps_m)
        self.C_ext = extinctionCrossSection(k,self.a,self.eps_p,self.eps_m)
        print("Scattering cross section: " + str(self.C_sca))
        print("Absorption cross section: " + str(self.C_abs))
        print("Extinction cross section: " + str(self.C_ext))
        
        print("Scattering efficiency: " + str(self.C_sca/(pi*self.a**2)))
        print("Absorption efficiency: " + str(self.C_abs/(pi*self.a**2)))
        print("Extinction efficiency: " + str(self.C_ext/(pi*self.a**2)))

if __name__ == '__main__':



    ### GNP ###
    wavelength = 632.8e-9
    analyte_Radius = 10e-9
    n_medium = 1.33

    analyte = Particle('Gold Nanoparticle', wavelength, analyte_Radius)
    
    Gold_dipole = Dipole(analyte,wavelength,n_medium)

    print('----------- Protein ----------------')

    ### protein ###
    wavelength = 632.8e-9
    analyte_Radius = ''
    n_medium = 1.33

    analyte = Particle('Streptavidin', wavelength, analyte_Radius)
    
    Protein_dipole = Dipole(analyte, wavelength, n_medium)


    ### polystyrene/Latex ###
    wavelength = 632.8e-9
    analyte_Radius = 25e-9
    n_medium = 1.33

    analyte = Particle('Latex', wavelength, analyte_Radius)
    
    Latex_dipole_large = Dipole(analyte,wavelength,n_medium)

    ### polystyrene/Latex ###
    wavelength = 632.8e-9
    analyte_Radius = 10e-9
    n_medium = 1.33

    analyte = Particle('Latex', wavelength, analyte_Radius)
    
    Latex_dipole_small = Dipole(analyte,wavelength,n_medium)

    print('Scattering crosssection Latex_large_vs_latex_small particle:')
    print(str(Latex_dipole_large.C_sca/Latex_dipole_small.C_sca))

    ### compute some ratios between dipoles:

    print('Scattering crosssection Gold vs Latex particle:')
    print(str(Gold_dipole.C_sca/Latex_dipole_small.C_sca))

    print('Scattering crosssection Latex vs Sav particle:')
    print(str(Latex_dipole_small.C_sca/Protein_dipole.C_sca))




    
    