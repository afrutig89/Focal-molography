# -*- coding: utf-8 -*-
"""
This file evaluates the equations presented in 'Focal Molography: Coherent
Microscopic Detection of Biomolecular Interaction' by Christog Fattinger.

@author: Silvio
"""

#####################################
##     Paper: Focal Molography     ##
#####################################
import sys
sys.path.append('../')
from numpy import pi
import numpy as np
import auxiliaries.constants as constants
    
print("---- Calculations done by Christof Fattinger ----")

# parameters
n_0 = 1.33                          # refractive index of aqueous medium
M_R = 150e3                         # antibody mass in Dalton [g/mol]
m_R = 1e-12                         # mass of the protein layer [g]
rho_R = 1e6                         # mass density of the receptor protein in an aqueous medium [g/m3]
n_s = 1.521                         # refractive index of substrate
n_f = 2.117                         # refractive index of Ta2O5 (waveguide)
N = 1.814                           # effective refractive index

wavelength = 635e-9                 # wavelength of the incident wave

D = 400e-6                          # thickness of the mologram
f = 500e-6                          # focal point

dndc = 0.182e-6                     # refractive increment for proteins in waver [m3/g]
eta = 0.3                           # coupling efficiency of the input coupler

t_f = 145e-9                        # thickness of the waveguide
t_eff = 329e-9                      # effective thickness of the waveguide for the TE mode
deltaz_f0 = 82e-9                   # penetration depth into the cover medium

propagationLoss = 0.8               # propagation loss in [dB/cm]

# near-backward scattering intensity
r = 0.5e-3
n_R = n_0 + dndc*rho_R
V_R = dndc * M_R/(constants.N_A*(n_R-n_0)) # volume of the receptor molecule [m3]
n_bar = (n_R+n_0)**2/(n_R**2+2*n_0**2)**2 # index factor
I_scat = 9*pi**2*n_0**4/(r**2*wavelength**4) * \
            ((n_R**2-n_0**2)/(n_R**2+2*n_0**2))**2 * V_R**2
I_scat2 = 9*pi**2*n_0**4/(r**2*wavelength**4) * \
            dndc**2 * M_R**2/constants.N_A**2*n_bar
print("Equation (1a):\tIntensity of near-backward scattering: I_scat/I_R = " + str(I_scat))
print("Equation (1b):\tIntensity of near-backward scattering: I_scat/I_R = " + str(I_scat2))

print("\nEquation (2.1):\tVolume of the receptor molecule V_R = " + str(V_R) + " m3")
print("Equation (2.2):\tIndex factor n_bar = " + str(n_bar))
print("Equation (2.2):\tRefractive index of isolated receptor molecule n_R = " + str(n_R))

# coherently scattered power incident on the Airy disk
N_R = m_R/M_R*constants.N_A
P_scat = N_R**2*I_scat
print("\nEquation (3):\tNumber of Molecules N_R = " + str('%.e' % N_R))
print("Equation (3):\tScattered power incident on the Airy disk P_scat/P_R = " + str(P_scat))

# diffracted power
t_m = wavelength/20.
M = t_m * (n_R-n_0)/dndc    # surface absorbed mass density
c_R = m_R/(t_m*(pi*(D/2.)**2/2.))
n_m = n_0 + dndc*c_R
wavelengthRidges = wavelength/n_m   # wavelength in the ridges
A_molo = 2*((n_f**2-N**2)*(n_m**2-n_0**2)**2)/(N*(n_f**2-n_0**2)) * wavelengthRidges*t_m**2/(wavelength**3*t_eff)
P_diff = A_molo * pi*D/4.
print("\nEquation (5):\tConcentration of the receptor in the ridges c_R = " + str(c_R*1e-6) + " g/ml")
print("Equation (5):\tn_m = " + str(n_m))
print("Equation (5):\tLambda = " + str(wavelengthRidges))
print("Equation (5):\tA_molo = " + str(A_molo))
print("Equation (5):\tP_diff/P_wg = " + str(P_diff))

# diffracted power 2
Gamma_Delta = (t_m*c_R)/2. # surface mass-density modulation
dNdn0 = n_0/N * (n_f**2-N**2)/(n_f**2-n_0**2) * deltaz_f0/t_eff
P_diff2 = dNdn0 * (n_m**2-n_0**2)**2/n_0 * wavelengthRidges*t_m**2/(wavelength**3*deltaz_f0) * pi*D/2.
P_diff3 = dNdn0 * 8*pi*n_0*wavelengthRidges*D/(wavelength**3*deltaz_f0) * dndc**2 * Gamma_Delta**2
print("\nEquation (6):\tGamma_Delta = " + str(Gamma_Delta))
print("Equation (6a):\tP_diff/P_wg = " + str(P_diff2))
print("Equation (6b):\tP_diff/P_wg = " + str(P_diff3))
print("\nEquation (7):\tdN/d0 = " + str(dNdn0))

# density fluctuations of the molecules
equ8a = (2*n_0+dndc*c_R)*dndc*c_R
equ8b = 2*n_0*dndc*c_R
print("\nEquation (8):\tn_m^2-n_0^2 = " + str(n_m**2-n_0**2))
print("Equation (8a):\t(2*n_0+dndc*c_R)*dndc*c_R = " + str(equ8a))
print("Equation (8b):\t2*n_0*dndc*c_R = " + str(equ8b))

# P_diff4/P_m
NA = n_s*D/(2*f)
P_diff4 = eta*dNdn0 * \
            (8*pi*n_0*wavelengthRidges*D**3*NA**2)/(wavelength**5*deltaz_f0) * \
            dndc**2*Gamma_Delta**2
print("\nEquation (9):\tN.A. = " + str(NA))
print("Equation (9):\tP_diff/P_m = " + str(P_diff4))

# P_diff/P_scat
A_scat = propagationLoss*np.log(10)/20. * 100
P_diff5 = A_molo/A_scat * 2*n_s**2*D**2/wavelength**2
P_diff6 = dNdn0 * (64*n_0*n_s**2*wavelengthRidges*D**2)/ \
            (wavelength**5*deltaz_f0*A_scat) * \
            dndc**2*Gamma_Delta**2
print("\nEquation (10):\tA_scat = " + str(A_scat))
print("Equation (10a):\tP_diff/P_scat = " + str(P_diff5))
print("Equation (10b):\tP_diff/P_scat = " + str(P_diff6))


print(A_molo/A_scat)