# -*- coding: utf-8 -*-
"""
This file evaluates the equations presented in 'Focal Molography: Coherent
Microscopic Detection of Biomolecular Interaction' by Christof Fattinger.



"""

#####################################
##     Paper: Focal Molography     ##
#####################################
import sys 
sys.path.append('../')
from numpy import pi
import numpy as np
import auxiliaries.constants as constants
from binder.Adlayer import ProteinAdlayer, Protein
from collections import namedtuple




class AnalyticalFocalMolographySystem():
	"""To be implemented, needs to define an a_ani"""


	def __init__(self,focalmolographyanalytical,camera):


		self.camera = camera
		self.focalmolographyanalytical = focalmolographyanalytical



class FocalMolographyAnalyticalCMT(object):
	"""Computes the light diffracted in mologram in terms of analytical formulas derived by coupled mode theory.

	:param mologram: Mologram() object
	:param adlayer: ProteinSurfaceMassModulationAdlayer() object

	The class distinguishes the cover medium and uses two different models to compute the diffracted power of the molecular assembly. For water, the refractive index increment of proteins is used,
	whereas other covermedia such as air, calculate the thickness of the protein film from the protein density and use a fixed refractive index of the protein to calculate the diffraction efficiency.

	The conversion between the two is:

	.. math:: \\frac{{{{\\left( {{n_{\\mathrm{P}}}^2 - {n_{\\mathrm{c}}}^2} \\right)}^2}}}{{{n_{\\mathrm{c}}}}}{\\frac{{{\\Delta _{{\\Gamma _{{\\mathrm{can}}}}}}}}{{{\\rho _{\\mathrm{P}}}^2}}^2} = 16 \\cdot {n_{\\mathrm{c}}}{\\left( {\\frac{{dn}}{{dc}}} \\right)^2}{\\Delta _{{\\Gamma _{{\\mathrm{can}}}}}}^2

	or without the surface mass modulation:

	.. math:: \\frac{1}{2}\\frac{{{{\\left( {{n_{\\mathrm{P}}}^2 - {n_{\\mathrm{c}}}^2} \\right)}^2}}}{{{n_{\\mathrm{c}}}}}\\frac{1}{{{\\rho _{\\mathrm{P}}}^2}} = 8 \\cdot {n_{\\mathrm{c}}}{\\left( {\\frac{{dn}}{{dc}}} \\right)^2}
	
	"""
	def __init__(self,mologram,adlayer = None):

		super(FocalMolographyAnalyticalCMT,self).__init__()

		self.mologram = mologram

		if adlayer == None:
			self.adlayer = ProteinAdlayer()
			print "Attenation: You did not specify a protein adlayer for the focal molography analytical class, are you sure?"
		else:	
			self.adlayer = adlayer


	def massDiffPower(self,n_c):
		"""Helper function that calculates the prefractor for mass to diffraction efficiency differently depending on the covermedium. This is here in order to make the focalMolography analytical class also work with covermedia,
		different to water:
		
		returns if water:

		.. math::  2 \\cdot {n_{\\mathrm{c}}}{\\left( {\\frac{{dn}}{{dc}}} \\right)^2}

		Factor 2 instead of 8 because Fattinger used a different definition for the surface mass density modulation. 

		for other covermedia:

		.. math:: \\frac{1}{2}\\frac{{{{\\left( {{n_{\\mathrm{P}}}^2 - {n_{\\mathrm{c}}}^2} \\right)}^2}}}{{{n_{\\mathrm{c}}}}}\\frac{1}{{{\\rho _{\\mathrm{P}}}^2}}

		"""
		protein = Protein(52.8e3) # hardcoded for SAv. 

		return 2*n_c*protein.ref_index_increment_for_n_c_SI(n_c)**2

		# else:
		# 	print "Calculated diffraction in a medium different than water!!:"

		# 	# protein Properties
		# 	# 1.3 g/cm^3 (1300 kg/m^3) density of a protein, # 1.58 is assumed refractive index of proteins
		# 	n_m = self.adlayer.refIndexProtein()
		# 	rho = self.adlayer.massDensityProtein()*10**(3)

		# 	return 1/2.*(n_m**2 - n_c**2)**2/n_c*1./rho**2


	def diffractedPowerFromPowerPerUnitLengthWG(self):
		"""Equation (6) in Fattinger, this equation is for the canonical mologram
		

		.. math:: {P_{diff}}\\left[ W \\right] = \\frac{{\\left( {{n_f}^2 - {N^2}} \\right)}}{{N\\left( {{n_f}^2 - {n_c}^2} \\right)}}\\frac{1}{{{t_{eff}}}}\\frac{{8\\pi }}{{{\\lambda ^2}}}{D^2}{n_c}{\\left( {\\frac{{dn}}{{dc}}} \\right)^2}{\\Delta _\\Gamma }^2{P_{wg}}\\left[ {\\frac{W}{m}} \\right]
		
		or 
		
		.. math:: {P_{diff}}\\left[ W \\right] = \\frac{{\\left( {{n_f}^2 - {N^2}} \\right)}}{{N\\left( {{n_f}^2 - {n_c}^2} \\right)}}\\frac{1}{{{t_{eff}}}}\\frac{\\pi }{{2{\\lambda ^2}}}{D^2}\\frac{{{{\\left( {{n_m}^2 - {n_c}^2} \\right)}^2}}}{{{n_c}}}{t_m}^2{P_{wg}}\\left[ {\\frac{W}{m}} \\right]
¨		
		However, since the Adlayer class only works for water as a cover medium, we chose the first approach here. 

		:returns: P [W] diffracted power of the mologram in W
		"""

		waveguide = self.mologram.waveguide
		wl = waveguide.wavelength
		n_s,n_f,n_c,N = waveguide.getRefractiveIndexes()
		t_eff = waveguide.t_eff

		diameter = self.mologram.radius*2

		#t_m = self.adlayer.returnDeltaSI()
		#n_m = self.adlayer.refIndexProteinLayer() # this needs to be adjusted now
		delta_Gamma = self.adlayer.surfaceMassFromProteinNumber()*1e-15/(1e-6) # canonical surface mass modulation conversion to kg/m^2
		# print "t_m"
		# print t_m
		# print "t_m**2*(n_m**2-n_c**2)**2"
		# print t_m**2*(n_m**2-n_c**2)**2
		
		return np.pi*1/N*(n_f**2-N**2)/(n_f**2-n_c**2)*1/t_eff/(wl**2)*diameter**2*delta_Gamma**2*waveguide.inputPower*self.massDiffPower(n_c)


	def powerInFocalSpotFromPowerPerUnitLengthWG(self,correctBraggArea = False):
		"""Calculates the power in the focal spot. This is 83.8 % of the power that is coupled out from the waveguide. If correct bragg is
		true the total power will be less, since the total surface mass is lower."""

		A_ratio = self.mologram.Area_ratio

		if correctBraggArea:

			return 0.838*self.diffractedPowerFromPowerPerUnitLengthWG()*A_ratio**2 

		else:

			return 0.838*self.diffractedPowerFromPowerPerUnitLengthWG()


	def numberOfPhotonsInFocalSpot(self,integrationTime,correctBraggArea = False):
		""" Calculates the number of photons that impinge on the focal spot for a given ingration time of the camera.
		
		.. math:: {n_p} = 0.83 \\cdot \\frac{\\lambda }{{hc}}P\\left[ W \\right] \\cdot t

		:param integrationTime: integration time of the camera [s] 

		"""

		wavelength = self.mologram.waveguide.wavelength


		return wavelength/(constants.h*constants.c)*integrationTime*self.powerInFocalSpotFromPowerPerUnitLengthWG(correctBraggArea)


	def averageIntensityInFocalSpotFromPowerPerUnitLengthWG(self,correctBraggArea = False):
		"""

		.. math:: {I_{Air{y_{Avg}}}}\\left[ {\\frac{W}{{{m^2}}}} \\right] = 0.838 \\cdot \\frac{{N{A^2}}}{{1.16 \\cdot {\\lambda ^2}}}\\frac{{\\left( {{n_f}^2 - {N^2}} \\right)}}{{N\\left( {{n_f}^2 - {n_c}^2} \\right)}}\\frac{1}{{{t_{eff}}}}\\frac{{8\\pi }}{{{\\lambda ^2}}}{D^2}{n_c}{\\left( {\\frac{{dn}}{{dc}}} \\right)^2}{\\Delta _\\Gamma }^2{P_{wg}}\\left[ {\\frac{W}{m}} \\right]


		:param correctBraggArea: If this parameter is true then the Bragg area correction is removed.

		The area_ratio is quadratic, because the surface mass is proportional to the area and the surface mass goes quadratic into the equation. 

		"""

		A_ratio = self.mologram.Area_ratio

		if correctBraggArea:

			return 0.838*1/(self.mologram.calcAiryDiskAreaAnalytical())*self.diffractedPowerFromPowerPerUnitLengthWG()*A_ratio**2

		else:
			return 0.838*1/(self.mologram.calcAiryDiskAreaAnalytical())*self.diffractedPowerFromPowerPerUnitLengthWG()

	def maximumIntensityInFocalSpotFromPowerPerUnitLengthWG(self,correctBraggArea = False):
		"""

		.. math:: {I_{Air{y_{\\max }}}}\\left[ {\\frac{W}{{{m^2}}}} \\right] = 1.93 \\cdot \\frac{{N{A^2}}}{{1.16 \\cdot {\\lambda ^2}}}\\frac{{\\left( {{n_f}^2 - {N^2}} \\right)}}{{N\\left( {{n_f}^2 - {n_c}^2} \\right)}}\\frac{1}{{{t_{eff}}}}\\frac{{8\\pi }}{{{\\lambda ^2}}}{D^2}{n_c}{\\left( {\\frac{{dn}}{{dc}}} \\right)^2}{\\Delta _\\Gamma }^2{P_{wg}}\\left[ {\\frac{W}{m}} \\right]

		:param correctBraggArea: If this parameter is true then the Bragg area correction is removed.

		"""

		A_ratio = self.mologram.Area_ratio

		if correctBraggArea:

			return 2.31*0.838*1/(self.mologram.calcAiryDiskAreaAnalytical())*self.diffractedPowerFromPowerPerUnitLengthWG()*A_ratio**2

		else:
			return 2.31*0.838*1/(self.mologram.calcAiryDiskAreaAnalytical())*self.diffractedPowerFromPowerPerUnitLengthWG()

	def minimalDetectableCanonicalSurfaceMassModulation(self,a_ani,b_nl=1.,figureOfMeritWG = False):
		"""
			minimal canonical surface mass modulation that can be detected by a certain mologram in pg/mm^2
        the formula is for the average intensity on the Airy disk not the Airy Disk maximum. The Factor 2 is due to the uncertainty of the sign of the roughness. 
		
		.. math:: {\\Delta _\\Gamma } = 0.12\\sqrt {{{{a_{{\\rm{ani}}}}{b_{{\\rm{nl}}}}\\alpha } \\over {{{\\left( {n_{\\rm{f}}^2 - {N^2}} \\right){n_{\\rm{c}}}} \\over {N{t_{{\\rm{eff}}}}\\left( {n_{\\rm{f}}^2 - n_{\\rm{c}}^2} \\right)}}}}} {{{\\lambda ^2}} \\over {\\left( {{{dn} \\over {dc}}} \\right)D}}
		
		use this equation if figureOfMeritWG = True

		.. math:: {\Delta _{{\\Gamma _{{\\mathrm{can}}}}}} =0.12\\sqrt {\\frac{1}{{FO{M_{{\\text{WG}}}}}}} \\frac{{\\sqrt {{q_{{\\text{cl}}}}} {\\lambda ^2}}}{{\\left( {\\frac{{dn}}{{dc}}} \\right)D}}
		
		checked AF 29.11.2017
		
		"""

		waveguide = self.mologram.waveguide
		wl = waveguide.wavelength
		alpha = waveguide.attenuationConstant
		D = self.mologram.radius*2
		n_s,n_f,n_c,N = waveguide.getRefractiveIndexes()
		t_eff = waveguide.t_eff

		if figureOfMeritWG:
			# use the formula with the figure of merit.
			if isinstance(figureOfMeritWG,bool):
				raise TypeError('Please supply a nonnegative float')
			return 1/3.*np.sqrt(b_nl/(figureOfMeritWG*10**30)*n_c)/(D)*1e15/(1e6)/np.sqrt(self.massDiffPower(n_c))

		else:
			return 1/3.*np.sqrt(a_ani*b_nl*alpha/(1./N*(n_f**2-N**2)/(n_f**2-n_c**2)*1./t_eff))*wl**2/(D)*1e15/(1e6)/np.sqrt(self.massDiffPower(n_c))

	def minimalDetectableSurfaceMassDensitySinusoidalDistribution(self,a_ani,b_nl=1,figureOfMeritWG = False):


		return self.SurfaceMassDensitySinusoidalDistributionFromEquivalentcanonicalSurfaceMassModulation(self.minimalDetectableCanonicalSurfaceMassModulation(a_ani=a_ani,b_nl=b_nl,figureOfMeritWG=figureOfMeritWG))

	def canonicalSurfaceMassModulationFromAverageIntensityInFocalSpot(self,I_avg):
		"""      
		Computes the equivalent canonical surface mass modulation from the intensity in the focal spot. 

		.. math:: {\\Delta _{{\\Gamma _{{\\text{can}}}}}} = \\sqrt {\\frac{{{I_{{\\text{Air}}{{\\text{y}}_{{\\text{Avg}}}}}}}}{{18.12 \\cdot \\frac{{{n_c}\\left( {{n_f}^2 - {N^2}} \\right)}}{{N{t_{eff}}\\left( {{n_f}^2 - {n_c}^2} \\right)}}{P_{{\\text{wg}}}}}}} \\frac{{{\\lambda ^2}}}{{\\left( {\\frac{{dn}}{{dc}}} \\right) \\cdot NA \\cdot D}}
		
		:param I_avg: Average intensity in the focal spot
		
		:returns : quivalendcanonical surface mass modulation in pg/mm^2
		
		"""

		powerPerUnitlength = self.mologram.waveguide.inputPower
		waveguide = self.mologram.waveguide
		wl = waveguide.wavelength
		n_s,n_f,n_c,N = waveguide.getRefractiveIndexes()
		t_eff = waveguide.t_eff

		diameter = self.mologram.radius*2

			# squared wavelength, because I do not evaluate the Airy disk.
		
		return wl/diameter*np.sqrt(I_avg*self.mologram.calcAiryDiskAreaAnalytical()/(0.838*1/N*(n_f**2-N**2)/(n_f**2-n_c**2)*1/t_eff*self.massDiffPower(n_c)*powerPerUnitlength))*1e15/(1e6)

	def SurfaceMassDensitySinusoidalDistributionFromEquivalentcanonicalSurfaceMassModulation(self,eqvDeltaGammacan):
		"""
		
		.. math:: \\Gamma  = \\frac{4}{\\pi }{\\Delta _{{\\Gamma _{{\\text{can,equ}}}}}}\\frac{{{A_ + }}}{{{A_{{\\text{mologram}}}}}}

		"""

		A_ratio = self.mologram.Area_ratio

		return eqvDeltaGammacan*4/np.pi*A_ratio/2

	def SurfaceMassDensitySinusoidalDistributionFromAverageIntensityInFocalSpot(self,I_avg):

		return self.SurfaceMassDensitySinusoidalDistributionFromEquivalentcanonicalSurfaceMassModulation(self.canonicalSurfaceMassModulationFromAverageIntensityInFocalSpot(I_avg))

	def convertRMSnormalizedIntensityToRMSGamma(self,RMS_intensity,a_ani):

		waveguide = self.mologram.waveguide
		wl = waveguide.wavelength
		alpha = waveguide.attenuationConstant
		D = self.mologram.radius*2
		n_s,n_f,n_c,N = waveguide.getRefractiveIndexes()
		t_eff = waveguide.t_eff

		C = self.minimalDetectableSurfaceMassDensitySinusoidalDistribution(a_ani,b_nl=1.)

		return RMS_intensity/2. * C





if __name__ == '__main__':

	# a mologram with a smaller diameter.

	sys.path.append('../')

	from waveguide.waveguide import SlabWaveguide
	from mologram.mologram import Mologram

	
	from binder.Adlayer import ProteinAdlayer

	Gamma_Delta_can = 0.1*np.pi/4.
	proteinLayer = ProteinAdlayer(Gamma = Gamma_Delta_can) # surface mass density modulation of a full SAv Layer PTP modulation of the sinusoid.

	waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                                  n_c=1., d_f=145e-9, polarization='TE',
                                  inputPower=20e-6/1e-3, wavelength=632.8e-9, attenuationConstant=0)

	mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=400e-6,braggOffset = 1e-9)

	focalMoloAnalytical = FocalMolographyAnalytical(mologram,proteinLayer)
	print focalMoloAnalytical.diffractedPowerFromPowerPerUnitLengthWG()

	print focalMoloAnalytical.numberOfPhotonsInFocalSpot(0.1)


	
	print("---- Calculations done by Christof Fattinger ----")

	# parameters
	n_0 = 1.33                          # refractive index of aqueous medium
	M_R = 150e3                         # antibody mass in Dalton [g/mol]
	m_R = 1e-12                         # mass of the protein layer [g]
	rho_R = 1e6                         # mass density of the receptor protein in an aqueous medium [g/m3]
	n_s = 1.521                         # refractive index of substratex
	n_f = 2.117                         # refractive index of Ta2O5 (waveguide)
	N = 1.814                           # effective refractive index

	wavelength = 635e-9                 # wavelength of the incident wave

	D = 400e-6                          # thickness of the mologram
	f = 500e-6                          # focal point

	dndc = 0.182e-6                     # refractive increment for proteins in waver [m3/g]
	eta = 0.3                           # coupling efficiency of the input coupler

	t_f = 145e-9                        # thickness of the waveguide
	t_eff = 329e-9                      # effective thickness of the waveguide for the TE mode
	deltaz_f0 = 82e-9                   # penetration depth into the cover medium

	propagationLoss = 0.8               # propagation loss in [dB/cm]

	# near-backward scattering intensity
	r = 0.5e-3
	n_R = n_0 + dndc*rho_R
	V_R = dndc * M_R/(constants.N_A*(n_R-n_0)) # volume of the receptor molecule [m3]
	n_bar = (n_R+n_0)**2/(n_R**2+2*n_0**2)**2 # index factor
	I_scat = 9*pi**2*n_0**4/(r**2*wavelength**4) * \
	            ((n_R**2-n_0**2)/(n_R**2+2*n_0**2))**2 * V_R**2
	I_scat2 = 9*pi**2*n_0**4/(r**2*wavelength**4) * \
	            dndc**2 * M_R**2/constants.N_A**2*n_bar
	print("Equation (1a):\tIntensity of near-backward scattering: I_scat/I_R = " + str(I_scat))
	print("Equation (1b):\tIntensity of near-backward scattering: I_scat/I_R = " + str(I_scat2))

	print("\nEquation (2.1):\tVolume of the receptor molecule V_R = " + str(V_R) + " m3")
	print("Equation (2.2):\tIndex factor n_bar = " + str(n_bar))
	print("Equation (2.2):\tRefractive index of isolated receptor molecule n_R = " + str(n_R))

	# coherently scattered power incident on the Airy disk
	N_R = m_R/M_R*constants.N_A
	P_scat = N_R**2*I_scat
	print("\nEquation (3):\tNumber of Molecules N_R = " + str('%.e' % N_R))
	print("Equation (3):\tScattered power incident on the Airy disk P_scat/P_R = " + str(P_scat))

	# diffracted power
	t_m = wavelength/20.
	M = t_m * (n_R-n_0)/dndc    # surface absorbed mass density
	c_R = m_R/(t_m*(pi*(D/2.)**2/2.))
	n_m = n_0 + dndc*c_R
	wavelengthRidges = wavelength/n_m   # wavelength in the ridges
	A_molo = 2*((n_f**2-N**2)*(n_m**2-n_0**2)**2)/(N*(n_f**2-n_0**2)) * wavelengthRidges*t_m**2/(wavelength**3*t_eff)
	P_diff = A_molo * pi*D/4.
	print("\nEquation (5):\tConcentration of the receptor in the ridges c_R = " + str(c_R*1e-6) + " g/ml")
	print("Equation (5):\tn_m = " + str(n_m))
	print("Equation (5):\tLambda = " + str(wavelengthRidges))
	print("Equation (5):\tA_molo = " + str(A_molo))
	print("Equation (5):\tP_diff/P_wg = " + str(P_diff))

	# diffracted power 2
	Gamma_Delta = (t_m*c_R)/2. # surface mass-density modulation
	dNdn0 = n_0/N * (n_f**2-N**2)/(n_f**2-n_0**2) * deltaz_f0/t_eff
	P_diff2 = dNdn0 * (n_m**2-n_0**2)**2/n_0 * wavelengthRidges*t_m**2/(wavelength**3*deltaz_f0) * pi*D/2.
	P_diff3 = dNdn0 * 8*pi*n_0*wavelengthRidges*D/(wavelength**3*deltaz_f0) * dndc**2 * Gamma_Delta**2
	print("\nEquation (6):\tGamma_Delta = " + str(Gamma_Delta))
	print("Equation (6a):\tP_diff/P_wg = " + str(P_diff2))
	print("Equation (6b):\tP_diff/P_wg = " + str(P_diff3))
	print("\nEquation (7):\tdN/d0 = " + str(dNdn0))

	# density fluctuations of the molecules
	equ8a = (2*n_0+dndc*c_R)*dndc*c_R
	equ8b = 2*n_0*dndc*c_R
	print("\nEquation (8):\tn_m^2-n_0^2 = " + str(n_m**2-n_0**2))
	print("Equation (8a):\t(2*n_0+dndc*c_R)*dndc*c_R = " + str(equ8a))
	print("Equation (8b):\t2*n_0*dndc*c_R = " + str(equ8b))

	# P_diff4/P_m
	NA = n_s*D/(2*f)
	P_diff4 = eta*dNdn0 * \
	            (8*pi*n_0*wavelengthRidges*D**3*NA**2)/(wavelength**5*deltaz_f0) * \
	            dndc**2*Gamma_Delta**2
	print("\nEquation (9):\tN.A. = " + str(NA))
	print("Equation (9):\tP_diff/P_m = " + str(P_diff4))

	# P_diff/P_scat
	A_scat = propagationLoss*np.log(10)/20. * 100
	# P_diff5 = A_molo/A_scat * 2*n_s**2*D**2/wavelength**2
	# P_diff6 = dNdn0 * (64*n_0*n_s**2*wavelengthRidges*D**2)/ \
	#             (wavelength**5*deltaz_f0*A_scat) * \
	#             dndc**2*Gamma_Delta**2
	# print("\nEquation (10):\tA_scat = " + str(A_scat))
	# print("Equation (10a):\tP_diff/P_scat = " + str(P_diff5))
	# print("Equation (10b):\tP_diff/P_scat = " + str(P_diff6))


	print(A_molo/A_scat)
