# -*- coding: utf-8 -*-


import sys,os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))))
from numpy import pi
import numpy as np
import auxiliaries.constants as constants
from binder.Adlayer import MologramProteinAdlayer
from collections import namedtuple
import unittest



class AnalyticalFocalMolographySystem():
	"""To be implemented, needs to define an a_ani"""


	def __init__(self,focalmolographyanalytical, optical_system):


		self.optical_system = optical_system
		self.focalmolographyanalytical = focalmolographyanalytical

from waveguide.waveguide import SlabWaveguide
from SimulationFramework.mologram.mologram import Mologram


class SanityCheck(unittest.TestCase):

	def testSanityMassModIntensityMassMod(self):
		"""checks mass modulation --> intensity --> mass modulation"""

		Delta_Gamma = 1.
		proteinLayer = MologramProteinAdlayer(M_protein=52.8e3, Delta_Gamma=Delta_Gamma, eta=0.5) # sinusoidal mass density modulation of 1 pg/mm^2 PTP value and SAv
		waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
	                                  n_c=1., d_f=145e-9, polarization='TE',
	                                  inputPower=20e-6/1e-3, wavelength=632.8e-9, attenuationConstant=0)
		mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=400e-6,braggOffset = 25e-6)
		focalMoloAnalytical = FocalMolographyAnalytical(mologram, proteinLayer)

		intensity = focalMoloAnalytical.averageIntensityInFocalSpotRayleigh(False)

		Delta_Gamma_from_intensity = focalMoloAnalytical.mass_modulation_from_average_intensity_in_focal_spot(intensity)

		self.assertAlmostEqual(Delta_Gamma, Delta_Gamma_from_intensity, places=3)

	def testSanityMinimalDetectableAmountWaveguideBGIntensity(self):

		proteinLayer = MologramProteinAdlayer(M_protein=52.8e3, Delta_Gamma=1., eta=0.5) # sinusoidal mass density modulation of 1 pg/mm^2 PTP value and SAv
		waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
	                                  n_c=1., d_f=145e-9, polarization='TE',
	                                  inputPower=20e-6/1e-3, wavelength=632.8e-9, attenuationConstant=2.51)
		mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=400e-6,braggOffset = 25e-6)
		focalMoloAnalytical = FocalMolographyAnalytical(mologram,proteinLayer)

		a_ani = 0.054

		I_bg = mologram.NA**2*waveguide.attenuationConstant*waveguide.inputPower/4.*a_ani

		modulation_from_bg =  focalMoloAnalytical.mass_modulation_from_average_intensity_in_focal_spot(I_bg)

		modulation_from_LOD = focalMoloAnalytical.minimal_detectable_mass_modulation(a_ani)

		self.assertAlmostEqual(modulation_from_bg, modulation_from_LOD, places=3)


# need to completely rewrite this class, it does not work anymore this way...
		

class FocalMolographyAnalytical(object):
	"""Computes the light diffracted in mologram in terms of analytical formulas derived by Rayleigh scattering.

	:param mologram: Mologram() object
	:param adlayer: MologramProteinAdlayer()

	"""

	def __init__(self, mologram,adlayer = None):

		super(FocalMolographyAnalytical,self).__init__()

		self.mologram = mologram

		if adlayer == None:
			self.adlayer = MologramProteinAdlayer()
			print "Attenation: You did not specify a protein adlayer for the focal molography analytical class, are you sure?"
		else:	
			self.adlayer = adlayer


	def proteinPrefactor(self):
		""" 
		
		.. math:: {n_{\\mathrm{c}}}\\frac{{\\left( {{n_{\\mathrm{P}}}^2 - {n_{\\mathrm{c}}}^2} \\right)}}{{{\\rho _{\\mathrm{P}}}\\left( {n_{\\mathrm{P}}^2 + 2n_{\\mathrm{c}}^2} \\right)}}\
		
		"""
		_,_,n_c,_ = self.mologram.waveguide.getRefractiveIndexes()
		n_P = self.adlayer.refIndexProtein()
		rho_P = self.adlayer.massDensityProtein()*10**(3) # in kg/m^3


		return n_c*(n_P**2-n_c**2)/(rho_P*(n_P**2 + 2*n_c**2))

	def numberOfPhotonsInFocalSpot(self,integrationTime,correctBraggArea = False):
		""" Calculates the number of photons that impinge on the focal spot for a given ingration time of the camera.
		
		.. math:: {n_p} = \\cdot \\frac{\\lambda }{{hc}}P\\left[ W \\right] \\cdot t

		:param integrationTime: integration time of the camera [s] 

		"""

		wavelength = self.mologram.waveguide.wavelength

		return wavelength/(constants.h*constants.c)*integrationTime*self.powerInFocalSpotRayleigh(correctBraggArea)


	def powerInFocalSpotRayleigh(self, correctBraggArea=False):

		return self.mologram.calcAiryDiskAreaAnalytical()*self.averageIntensityInFocalSpotRayleigh(correctBraggArea)


	def averageIntensityInFocalSpotRayleigh(self, correctBraggArea=False):
		"""

		.. math:: {I_{{\\rm{sig,RS}}}} = 1.268 \\cdot {\\pi ^2}{\\rm{N}}{{\\rm{A}}^2}n_{\\rm{c}}^2\\frac{{{{\\left( {{n_{\\rm{P}}}^2 - {n_{\\rm{c}}}^2} \\right)}^2}}}{{{{\\left( {n_{\\rm{P}}^2 + 2n_{\\rm{c}}^2} \\right)}^2}}}\\frac{{{D^2}}}{{{\\lambda ^4}}}\\frac{{{\\eta _{i\\left[ {\\rm{A}} \\right]}}^2{\\Delta _\\Gamma }^2}}{{{\\rho _{\\rm{P}}}^2}}{I_0}


		"""

		I_0 = self.mologram.waveguide.intensity_on_waveguide_surface()
		print 'Intensity on waveguide surface'
		print I_0
		NA = self.mologram.NA
		n_s,n_f,n_c,N = self.mologram.waveguide.getRefractiveIndexes()
		n_m = self.adlayer.refIndexProtein()
		wavelength = self.mologram.waveguide.wavelength
		D = self.mologram.radius*2
		delta_Gamma = self.adlayer.Delta_Gamma*1e-15/(1e-6) # surface mass modulation conversion to kg/m^2
		eta = self.adlayer.eta # analyte efficiency of the surface mass modulation
		
		A_ratio = self.mologram.Area_ratio

		avg_int = 1.268*np.pi**2*self.proteinPrefactor()**2*NA**2*D**2/wavelength**4*eta**2*delta_Gamma**2*I_0

		if correctBraggArea:

			return avg_int*A_ratio**2
		
		else:

			return avg_int

	def sensitivity_of_FM(self, a_ani, correctBraggArea = False):

		# conversion factor in order to convert to SI units. 
		return 2/(self.gamma_0(a_ani)*(1e6)/1e15)
		

	def maximumIntensityInFocalSpotRayleigh(self, correctBraggArea = False):

		return 4.378*self.averageIntensityInFocalSpotRayleigh(correctBraggArea)

	def minimal_detectable_mass_modulation(self, a_ani, b_nl=1., figureOfMeritWG = False):
		"""
			minimal surface mass modulation that can be detected by a certain mologram in pg/mm^2
        	the formula is for the average intensity on the Airy disk not the Airy Disk maximum.
        	The parameter eta (in the protein adlayer class) specifices the distribution: 1 = delta distributed, 2/pi = canonical, 0.5 = sinusoidal

        	.. math:: {\\Delta _\\Gamma } = 0.1413 \\cdot \\sqrt {\\frac{{{q_{{\\rm{cl}}}}{a_{{\\rm{ani}}}}{\\alpha _{{\\rm{sca}}}}}}{{\\frac{{2{n_{\\rm{c}}}\\left( {n_{\\rm{f}}^2 - {N^2}} \\right)}}{{N{t_{{\\rm{eff}}}}\\left( {n_{\\rm{f}}^2 - n_{\\rm{c}}^2} \\right)}}}}} \\frac{{{\\lambda ^2}}}{D}\\frac{{{\\rho _{\\rm{P}}}\\left( {n_{\\rm{P}}^2 + 2n_{\\rm{c}}^2} \\right)}}{{{n_{\\rm{c}}}\\left( {{n_{\\rm{P}}}^2 - {n_{\\rm{c}}}^2} \\right)}}\\frac{1}{{{\\eta _{\\left[ A \\right]}}}}
			
			or when the figure of merit is specified:
		"""

		waveguide = self.mologram.waveguide
		wl = waveguide.wavelength
		alpha = waveguide.attenuationConstant
		D = self.mologram.radius*2
		n_s,n_f,n_c,N = waveguide.getRefractiveIndexes()
		t_eff = waveguide.t_eff
		eta = self.adlayer.eta # analyte efficiency

		if figureOfMeritWG:
			# use the formula with the figure of merit.
			if isinstance(figureOfMeritWG,bool):
				raise TypeError('Please supply a nonnegative float')
			# still for average intensity 
			return 0.1*np.sqrt(b_nl/(figureOfMeritWG))/D*1./self.proteinPrefactor()*1e15/(1e6)*1/eta

		else:

			return 0.1413*np.sqrt(a_ani*b_nl*alpha/waveguide.intensity_on_waveguide_surface_power_in_wg_ratio())*wl**2/D*1./self.proteinPrefactor()*1e15/(1e6)*1/eta


	def minimal_detectable_mass_density(self, a_ani, b_nl=1., figureOfMeritWG = False):

		A_ridges = self.mologram.A_ridges()
		A_mologram = self.mologram.Area()

		return self.minimal_detectable_mass_modulation(a_ani, b_nl, figureOfMeritWG)*A_ridges/A_mologram

	def gamma_0(self, a_ani):
		"""
		returns the mass density on the mologram that is required to give rise to the same intensity as the background intensity at the focal plane. [pg/mm^2]
		"""

		A_ridges = self.mologram.A_ridges()
		A_mologram = self.mologram.Area()

		return self.minimal_detectable_mass_modulation(a_ani)*A_ridges/A_mologram # conversion from mass density to mass modulation.

	def mass_modulation_from_average_intensity_in_focal_spot(self, I_avg, correctBraggArea=False):
		"""      
		Computes the equivalent surface mass modulation from the intensity in the focal spot (this is not Bragg Area corrected). 

		.. math:: {\\Delta _{{\\Gamma _\\delta }}} = 2.79 \\cdot \\sqrt {\\frac{{{I_{{\\rm{sig,RS}}}}}}{{{I_0}}}} \\frac{{{\\lambda ^2}}}{{{\\pi ^2}D \\cdot {\\rm{NA}}}}\\frac{{\\left( {n_{\\rm{P}}^2 + 2n_{\\rm{c}}^2} \\right)}}{{{n_{\\rm{c}}}{\\rho _{\\rm{P}}}\\left( {{n_{\\rm{P}}}^2 - {n_{\\rm{c}}}^2} \\right)}}

		:param I_avg: Average intensity in the focal spot
		:param eta: analyte efficiency of the modulation
		
		:returns : equivalend surface mass modulation in pg/mm^2
		
		"""

		I_0 = self.mologram.waveguide.intensity_on_waveguide_surface()
		wl = self.mologram.waveguide.wavelength
		D = self.mologram.radius*2
		NA = self.mologram.NA
		eta = self.adlayer.eta
		A_ridges = self.mologram.A_ridges()
		A_mologram = self.mologram.Area()

		if correctBraggArea:
		
			return 2.79*np.sqrt(I_avg)/np.sqrt(I_0)*wl**2/(np.pi**2*D*NA)*1/self.proteinPrefactor()*1/eta*1e9*A_mologram/(2*A_ridges)

		else:

			return 2.79*np.sqrt(I_avg)/np.sqrt(I_0)*wl**2/(np.pi**2*D*NA)*1/self.proteinPrefactor()*1/eta*1e9



	def mass_density_on_mologram_from_average_intensity(self, I_avg, correctBraggArea=False):

		A_ridges = self.mologram.A_ridges()
		A_mologram = self.mologram.Area()

		if correctBraggArea:

			return self.mass_modulation_from_average_intensity_in_focal_spot(I_avg)*A_mologram/(2*A_ridges)/2.

		else:

			return self.mass_modulation_from_average_intensity_in_focal_spot(I_avg)/2. # since ridges are only on half of the mologram

	def convertRMSnormalizedIntensityToRMSGamma(self, RMS_intensity, a_ani):

		print("ATTENTION THE SENSITIVITY FUNCTION STILL CONTAINS A HACK FOR THE AIRY DISK CONVOLUTION ALGORITHM!")

		gamma_0 = self.gamma_0(a_ani)

		return RMS_intensity/2.*(gamma_0/np.sqrt(2.012))


def test_mass_quantification():

	proteinLayer = MologramProteinAdlayer(M_protein=52800,eta=1.) # a sinusoidal mass density has an analyte efficiency of 0.5. 

	waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                                  n_c=1., d_f=145e-9, polarization='TE',
                                  inputPower=0.02, wavelength=632.8e-9, attenuationConstant = 7.76*np.log(10)/10*100)

	mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=400e-6,braggOffset = 25e-6)

	focalMoloAnalytical = FocalMolographyAnalytical(mologram, proteinLayer)

	print('cohernet mass density modulation')
	print focalMoloAnalytical.mass_modulation_from_average_intensity_in_focal_spot(105.08,correctBraggArea = True)

	print('coherent mass density')
	print focalMoloAnalytical.mass_density_on_mologram_from_average_intensity(105.08,correctBraggArea=True) # returns 

	proteinLayer = MologramProteinAdlayer(M_protein=52800,eta=0.5) # a sinusoidal mass density has an analyte efficiency of 0.5. 

	waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                                  n_c=1., d_f=145e-9, polarization='TE',
                                  inputPower=0.02, wavelength=632.8e-9, attenuationConstant = 7.76*np.log(10)/10*100)

	mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=400e-6,braggOffset = 25e-6)

	focalMoloAnalytical = FocalMolographyAnalytical(mologram, proteinLayer)

	print('sinusoidal mass density modulation')
	print focalMoloAnalytical.mass_modulation_from_average_intensity_in_focal_spot(105.08,correctBraggArea = True)

	print('sinusoidal mass density')
	print focalMoloAnalytical.mass_density_on_mologram_from_average_intensity(105.08,correctBraggArea=True) # returns 


def test_paper_christof_fattinger(): 


	print("---- Calculations done by Christof Fattinger ----")

	# parameters 
	n_0 = 1.33                          # refractive index of aqueous medium
	M_R = 150e3                         # antibody mass in Dalton [g/mol]
	m_R = 1e-12                         # mass of the protein layer [g]
	rho_R = 1e6                         # mass density of the receptor protein in an aqueous medium [g/m3]
	n_s = 1.521                         # refractive index of substratex
	n_f = 2.117                         # refractive index of Ta2O5 (waveguide)
	N = 1.814                           # effective refractive index

	wavelength = 635e-9                 # wavelength of the incident wave

	D = 400e-6                          # thickness of the mologram
	f = 500e-6                          # focal point

	dndc = 0.182e-6                     # refractive increment for proteins in waver [m3/g]
	eta = 0.3                           # coupling efficiency of the input coupler

	t_f = 145e-9                        # thickness of the waveguide
	t_eff = 329e-9                      # effective thickness of the waveguide for the TE mode
	deltaz_f0 = 82e-9                   # penetration depth into the cover medium

	propagationLoss = 0.8               # propagation loss in [dB/cm]

	# near-backward scattering intensity
	r = 0.5e-3
	n_R = n_0 + dndc*rho_R
	V_R = dndc * M_R/(constants.N_A*(n_R-n_0)) # volume of the receptor molecule [m3]
	n_bar = (n_R+n_0)**2/(n_R**2+2*n_0**2)**2 # index factor
	I_scat = 9*pi**2*n_0**4/(r**2*wavelength**4) * \
	            ((n_R**2-n_0**2)/(n_R**2+2*n_0**2))**2 * V_R**2
	I_scat2 = 9*pi**2*n_0**4/(r**2*wavelength**4) * \
	            dndc**2 * M_R**2/constants.N_A**2*n_bar
	print("Equation (1a):\tIntensity of near-backward scattering: I_scat/I_R = " + str(I_scat))
	print("Equation (1b):\tIntensity of near-backward scattering: I_scat/I_R = " + str(I_scat2))

	print("\nEquation (2.1):\tVolume of the receptor molecule V_R = " + str(V_R) + " m3")
	print("Equation (2.2):\tIndex factor n_bar = " + str(n_bar))
	print("Equation (2.2):\tRefractive index of isolated receptor molecule n_R = " + str(n_R))

	# coherently scattered power incident on the Airy disk
	N_R = m_R/M_R*constants.N_A
	P_scat = N_R**2*I_scat
	print("\nEquation (3):\tNumber of Molecules N_R = " + str('%.e' % N_R))
	print("Equation (3):\tScattered power incident on the Airy disk P_scat/P_R = " + str(P_scat))

	# diffracted power
	t_m = wavelength/20.
	M = t_m * (n_R-n_0)/dndc    # surface absorbed mass density
	c_R = m_R/(t_m*(pi*(D/2.)**2/2.))
	n_m = n_0 + dndc*c_R
	wavelengthRidges = wavelength/n_m   # wavelength in the ridges
	A_molo = 2*((n_f**2-N**2)*(n_m**2-n_0**2)**2)/(N*(n_f**2-n_0**2)) * wavelengthRidges*t_m**2/(wavelength**3*t_eff)
	P_diff = A_molo * pi*D/4.
	print("\nEquation (5):\tConcentration of the receptor in the ridges c_R = " + str(c_R*1e-6) + " g/ml")
	print("Equation (5):\tn_m = " + str(n_m))
	print("Equation (5):\tLambda = " + str(wavelengthRidges))
	print("Equation (5):\tA_molo = " + str(A_molo))
	print("Equation (5):\tP_diff/P_wg = " + str(P_diff))

	# diffracted power 2
	Gamma_Delta = (t_m*c_R)/2. # surface mass-density modulation
	dNdn0 = n_0/N * (n_f**2-N**2)/(n_f**2-n_0**2) * deltaz_f0/t_eff
	P_diff2 = dNdn0 * (n_m**2-n_0**2)**2/n_0 * wavelengthRidges*t_m**2/(wavelength**3*deltaz_f0) * pi*D/2.
	P_diff3 = dNdn0 * 8*pi*n_0*wavelengthRidges*D/(wavelength**3*deltaz_f0) * dndc**2 * Gamma_Delta**2
	print("\nEquation (6):\tGamma_Delta = " + str(Gamma_Delta))
	print("Equation (6a):\tP_diff/P_wg = " + str(P_diff2))
	print("Equation (6b):\tP_diff/P_wg = " + str(P_diff3))
	print("\nEquation (7):\tdN/d0 = " + str(dNdn0))

	# density fluctuations of the molecules
	equ8a = (2*n_0+dndc*c_R)*dndc*c_R
	equ8b = 2*n_0*dndc*c_R
	print("\nEquation (8):\tn_m^2-n_0^2 = " + str(n_m**2-n_0**2))
	print("Equation (8a):\t(2*n_0+dndc*c_R)*dndc*c_R = " + str(equ8a))
	print("Equation (8b):\t2*n_0*dndc*c_R = " + str(equ8b))

	# P_diff4/P_m
	NA = n_s*D/(2*f)
	P_diff4 = eta*dNdn0 * \
	            (8*pi*n_0*wavelengthRidges*D**3*NA**2)/(wavelength**5*deltaz_f0) * \
	            dndc**2*Gamma_Delta**2
	print("\nEquation (9):\tN.A. = " + str(NA))
	print("Equation (9):\tP_diff/P_m = " + str(P_diff4))

	# P_diff/P_scat
	A_scat = propagationLoss*np.log(10)/20. * 100
	# P_diff5 = A_molo/A_scat * 2*n_s**2*D**2/wavelength**2
	# P_diff6 = dNdn0 * (64*n_0*n_s**2*wavelengthRidges*D**2)/ \
	#             (wavelength**5*deltaz_f0*A_scat) * \
	#             dndc**2*Gamma_Delta**2
	# print("\nEquation (10):\tA_scat = " + str(A_scat))
	# print("Equation (10a):\tP_diff/P_scat = " + str(P_diff5))
	# print("Equation (10b):\tP_diff/P_scat = " + str(P_diff6))


	print(A_molo/A_scat)

def test_standard_calculations():
	sys.path.append('../')

	from waveguide.waveguide import SlabWaveguide
	from mologram.mologram import Mologram

	from analytical.focalMolography import FocalMolographyAnalytical
	from binder.Adlayer import ProteinAdlayer

	proteinLayer = MologramProteinAdlayer(M_protein=150e3,eta=1) # a sinusoidal mass density has an analyte efficiency of 0.5. 

	waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                                  n_c=1.33, d_f=145e-9, polarization='TE',
                                  inputPower=20e-6/1e-3, wavelength=632.8e-9, attenuationConstant = 7.76*np.log(10)/10*100)

	mologram = Mologram(waveguide,focalPoint=-900e-6, diameter=1000e-6,braggOffset = 1e-9)

	focalMoloAnalytical = FocalMolographyAnalytical(mologram,proteinLayer)

	print 'Mass noise at 10% RMS intensity:'
	print focalMoloAnalytical.convertRMSnormalizedIntensityToRMSGamma(0.1,0.054)

	print "Figure of merit waveguide:"
	print waveguide.figureOfMeritWGExperimental(0.054)

	print "Figure of merit focal molography:"
	print mologram.figure_of_merit_FM(0.054)

	# why is there a factor sqrt(2.31)
	print "Sensitivity of Focal Molography:"
	print focalMoloAnalytical.sensitivity_of_FM(0.054)*np.sqrt(2.012) # I need this factor because the mass modulation function still operates with the average intensity in the Airy disk. 

	print "Minimal detectable mass modulation:"

	print focalMoloAnalytical.minimal_detectable_mass_modulation(a_ani=0.054)

	print "Minimal detectable mass density:"

	print focalMoloAnalytical.minimal_detectable_mass_density(a_ani=0.054)
	

	print focalMoloAnalytical.numberOfPhotonsInFocalSpot(0.1)

	unittest.main()



if __name__ == '__main__':

	# a mologram with a smaller diameter.

	test_mass_quantification()

	

	