# -*- coding: utf-8 -*-
"""
This file creates a default setting database entry which 

@author: silvi
"""

import os
import numpy as np
from numpy import *


import sys
from os import sys,path
sys.path.append(path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))))
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from SharedLibraries.Database.dataBaseOperations import checkFolder, saveDB, createDB, createSQLDict

# time and date
import datetime
import time
from variables import FieldSimulationInputs,FieldSimulationResults
from materials.materials import Particle


####################################
#####      Initialization      #####
####################################

##### General Settings ######

generalSettings =  {
        # general simulation settings
        'date':datetime.datetime.now().strftime ("%Y%m%d"),
        'startTime' : 'text',
        'start':'real',
        'description':'description',
        'calculationMethod' : 'Cuda', # "parallelPool" (OSX)
        'biasSimulation':0,
        'Extinction':0, # whether to calculate the extinction of the particles or not
        'CorrSimulation':0, # if not zero, this corresponds to the experiment ID from which the correlated simulation should be loaded.
        # database settings
        'BackgroundScreen':1, # use a background screen, to calculate the intensity distribution of nonspecifically bound scatterers.
        'saveDatabase' : True,
        'databaseFile' : 'data/simulations.db',
        'saveLog' : True,
        'saveFigure' : False # not used anymore in the simulation. 
        }

##### Screen Parameters ######

screenParValues = {
    'screenWidth': 10e-6,
    'screenPlane':'xy',
    'screenRatio':1/1.,
    'screenRotation' : np.array([[0/180.*pi], [0/180.*pi]]) ,
    'center' : np.array([0,0]),        # center of the screen
    'centerBackground': np.array([-40e-6,-40e-6]), # the background screen is shifted by a certain amount.
    'screen_z_position':-890e-6, # z value of the screen, if minus 1 the screen is set up at the molographic focalpoint position, else the screen is at this position
    'npix':100      # number of pixels in each direction
    }

##### Waveguide Parameters ######

wavelength = 632.8e-9
waveguideParValues = {
            'wavelength':wavelength, # laser wavelength in meters
            'inputPower':20e-6, # input power in Watt
            'Laserfluctuations': 0 /100., # fluctuations of the laser intensity in %
            'beamWaist': 1e-3, # beamwaist of the gaussian beam
            'couplingEfficiency':1,  # amount of power which is coupled into the waveguide. This is based on a measurement. 
            'gratingPosition' : -4e-3, # grating position in meter, relative to the center of the mologram.
            'polarization':'TE', # polarization of the waveguide mode. 
            'mode':0, # order of the mode, the framework can only handle the two fundamental modes.
            'propagationLoss':0.8* log(10)/10. * 100, # propagation loss of the waveguide in Np/m
            'surfaceRoughness':0, # Standarddeviation of the roughness of the waveguide [m], if this value is specified, the propagationLoss is overwritten. Still to be implemented. 
            'Waveguidebackground':0, # boolean, if the waveguideback should be included in the total field calculation. 
            'cover' : 'Air', 
            'film' : 'Tantalum Pentoxide',
            'substrate' : 'Glass',
            'thickness' : 145e-9 # thickness of waveguide in [m]
        }

##### Mologram Parameters ######
mologramParValues = {
        'focalPoint':-900e-6, # focal point (z-component) in [m]
        'diameter':400e-6,  # maximum cell size in [m]
        'NA':-1, # numerical aperture, a value of -1 signifies that NA is not specific
        'braggOffset':25e-6, # Bragg offset for mask in [m], if it is -1 than the it is calculated with an safety angle of 2, To do: specify bragg angle and if this one is also zero, then omit the bragg completely.
        'braggTheta':-1, # -1 means that the standard theta of 2 deg is used
        'braggRadius':-1, # -1 means that the bragg radius is calculated according to the method calcBraggCircle in the molography class.
        'shape':'disk', # shape of the mologram (disk/rectangular)
        'xshift':0, # shift in x direction relative to the center point of the molographic structure.
        'aspectRatio':1, # default aspect ratio of the mologram, only relevant for rectangular molograms.
        'direction':-1
}

##### Bias Parameters ######

bias_Radius = 5e-9           # particle radius in meters

bias = Particle('Ag', wavelength, bias_Radius)
n_p = complex(bias.refractiveIndex)

bias_Radius = bias.radius

biasParValues = {
    'bias_name' : 'Ag', # what particles should be used for the bias
    'bias_Radius' : bias_Radius, # particle radius in [m]
    'bias_RefractiveIndex' : n_p,
    'bias_Ncoh':0, # placed with binding probability of 100 %
    'bias_z_min': 0,     # minimum distance of the particles above waveguide
    'bias_z_max':20e-9,      # maximum distance of the particles above waveguide          
    'bias_placement':'Ridges (Sinusoidal)', # Sinuisoidal, Rectangular, Mololines.
    'bias_surfaceMassModulation':0 # surfaceMassModulation of the bias in pg/mm² IMPORTANT: NOT IMPLEMENTED YET
}

##### Analyte Parameters ######

analyte_Radius = ''           # particle radius in meters

analyte = Particle('Gold Nanoparticle', wavelength, analyte_Radius)
n_p = complex(analyte.refractiveIndex)

analyte_Radius = 25e-9

analyteParValues = {
    'analyte_name' : 'Gold Nanoparticle',
    'analyte_Radius' : analyte_Radius, # particle radius in [m]
    'analyte_RefractiveIndex' : n_p,
    'analyte_Ncoh':200, # placed with binding probability of 100 %
    'analyte_Nincoh':0, # placed with binding probability 50 %
    'analyte_MSN':0, # boolean, whether particle exhibits molecular shot noise
    'analyte_Ncoh_wMSN':0, # placed with binding probability of 100 %, are set when placing the binders
    'analyte_Nincoh_wMSN':0, # placed with binding probability 50 %, are set when placing the binders
    'analyte_z_min': 0,     # minimum distance of the particles above waveguide
    'analyte_z_max':0,      # maximum distance of the particles above waveguide          
    'analyte_placement':'Ridges (Sinusoidal)', # sinuisoidal, rectangular, mololines.
    'analyte_seed':0, # boolean specifying whether to seed the bragg area
}

##### Analyte Interation Parameters ######

analyteInteractionParValues = {
    'analyte_RecDenRid':2*10**10*1e6, # recognition density on the ridges num/m²
    'analyte_RecDenGrov':1*10**10*1e6, # recognition density on the Grooves num/m²
    'analyte_K_D':1e-9, # analyte dissociation constant
    'analyte_c':1e-15, # analyte concentration
    'analyte_competitive':False
}



##### NSB Parameters ######

NSB_Radius = ''            # particle radius in meters

NSB = Particle('Albumin', wavelength, NSB_Radius)
n_p = complex(NSB.refractiveIndex)

NSB_Radius = NSB.radius


NSBParValues = {
    'NSB_name' : 'Albumin',
    'NSB_Radius' : NSB_Radius, # particle radius in [m]
    'NSB_RefractiveIndex' : n_p,
    'NSB_Ncoh':0, # placed with binding probability of 100 %
    'NSB_Nincoh':0, # placed with binding probability 50 %
    'NSB_MSN':0, # boolean, whether particle exhibits molecular shot noise
    'NSB_Ncoh_wMSN':0, # placed with binding probability of 100 %, are set when placing the binders
    'NSB_Nincoh_wMSN':0, # placed with binding probability 50 %, are set when placing the binders
    'NSB_z_min': NSB_Radius+20e-9,     # minimum distance of the particles above waveguide
    'NSB_z_max':NSB_Radius+20e-9,      # maximum distance of the particles above waveguide          
    'NSB_placement':'Ridges (Sinusoidal)', # sinuisoidal, rectangular, mololines.
    'NSB_seed':0, # boolean specifying whether to seed the bragg area
}

##### NSB Interaction Parameters ######

NSBInteractionParValues = {   
    'NSB_RecDenRid':2*10**10*1e6, # recognition density on the ridges num/m²
    'NSB_RecDenGrov':2*10**10*1e6, # recognition density on the Grooves num/m²
    'NSB_K_D':1e-3, # NSB dissociation constant
    'NSB_c':1e-9, # NSB concentration
    'NSB_competitive':False
}


simInput = FieldSimulationInputs()
simResults = FieldSimulationResults()


# set the simulation input values
simInput.setValues(generalSettings)
simInput.setValues(screenParValues)
simInput.setValues(waveguideParValues)
simInput.setValues(mologramParValues)
simInput.setValues(analyteParValues)
simInput.setValues(biasParValues)
simInput.setValues(NSBParValues)
#simInput.setValues(analyteInteractionParValues)
#simInput.setValues(NSBInteractionParValues)

#os.mkdir('database')
if not os.path.exists('database'):
        os.makedirs('database')

os.remove('database/default.db') if os.path.exists('database/default.db') else None



# initialize database
createDB('database/default.db', 'log',simInput.returnDBPar())
saveDB('database/default.db', 'log', simInput.getValues())