plotType = 'contour'
colorbar = True
grid = True

dpi = 100
font = 'Arial'
fontSizeTitle = 14
fontSizeAxes = 12
fontSizeTicks = 12
figSizeSingle = (6, 6)
figSizeDouble = (12, 6)
figSizeTriple = (12, 4)
figSizeQuadruple = (12, 12)
showPlots = True
savePlots = True
folder = 'plots/2017-01-03'
imageFormat = 'png'

counter = 1
