# -*- coding: utf-8 -*-
"""
The present module calculates the total reflection efficency of a 1D 
bragg grating.
(To be explained with more detail.)

:created:       20-Nov-2016
:author:        Roland Dreyfus
:revised:       02-Mai-2017 by Roland Dreyfus
:references:    Marcuse, Dietrich. Theory of Dielectric Optical Waveguides. 
                Quantum Electronics--Principles and Applications. New York: 
                Academic Press, 1974.
"""

import sys

import numpy as np
import scipy.integrate as integrate
import scipy.signal as signal
import matplotlib.pyplot as plt

# path Focal Molography framework:
from os import sys, path


sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import auxiliaries.constants as constants
from auxiliaries import RefrIndexOfLayerOfScatterers

from waveguide.waveguide import Waveguide, SlabWaveguide
from materials.materials import IsotropicMaterial



class BraggMolography(object):
    """
    Calculates the total reflection efficency of a 1D bragg grating.
    """
    def __init__(self, param_waveguide, param_bragg, sin_or_rect, two_dim=False):
        super(BraggMolography, self).__init__()
        """
        """

        self.sin_or_rect = sin_or_rect
        self.two_dim = two_dim
        self.wl = param_waveguide["wl"]
        self.a = param_bragg["a"]
        self.L = param_bragg["L"]
        self.t_m = param_bragg["t_m"]
        self.t_f = param_waveguide["t_f"]
        self.Delta_Gamma = param_bragg["Delta_Gamma"]
        self.inputPower = param_waveguide["inputPower"]
        self.polarization = param_waveguide["polarization"]
        self.attenuationConstant = param_waveguide["attenuationConstant"]

        self.theta = (90-22.5)*np.pi/180

        self.n_c = param_waveguide["n_c"]
        self.n_s = param_waveguide["n_s"]
        self.n_f = param_waveguide["n_f"]

        self.N, self.t_eff, self.beta = self.buildWaveguide()
        self.Lambda = param_bragg['Lambda']
        self.Lambda = self.getLambda()
        self.n_m = self.calc_n_m(self.t_f)
        self.x = np.linspace(0, self.L, 10*self.L/self.Lambda)
        


        # E field
        self.x = self.sampleX()
        self.omega = 2.0 *np.pi/self.wl * constants.c
        self.xPoint, self.yPoint, self.zPoint = 0.0, 0.0, 0.0
        self.c_plus_0 = 1.0



    def buildWaveguide(self):
        
        self.wg = SlabWaveguide(
                    name = 'Tantalum Pentoxide', 
                    n_s = self.n_s, 
                    n_f = self.n_f, 
                    n_c = self.n_c, 
                    d_f = self.t_f, 
                    polarization = self.polarization, 
                    inputPower = self.inputPower, 
                    wavelength = self.wl,
                    attenuationConstant = self.attenuationConstant,
                    mode = 0)

        self.wg.calcPropagationConstants(self.wl)
        N = self.wg.calcEffRefractiveIndex(1.7, self.wl)

        t_eff = self.wg.calcEffThickness(self.wl)

        beta = self.wg.beta

        if self.two_dim==True:
            N = N*np.cos(self.theta)    #rad
            beta = beta*np.cos(self.theta)

        return N, t_eff, beta


    def getLambda(self):
        """
        """
        if self.Lambda == []:
            Lambda = self.wl/(2*self.N)
        else:
            Lambda = self.Lambda    #1.74619975729e-07

        return Lambda


    def calc_n_m(self, t_f):

        protein_layer = RefrIndexOfLayerOfScatterers.RefrIndexOfLayerOfScatterers(
                                52.8e3, 
                                Delta_Gamma=self.Delta_Gamma, 
                                delta=self.t_m)

        self.n_m = protein_layer.refIndexProteinLayer()
        return self.n_m


    def sampleX(self):
        # Initialisation coordinate system
        # respecting Nyquist sampling crietrion limited by Lambda
        self.x = np.linspace(0,self.L,10.0*self.L/self.Lambda)
        return self.x


    def get_Ey(self, z):
        """ 
        gets E_y component from waveguide module
        """
        
        y = np.zeros(len(z))
        x = np.zeros(len(z))

        E_y = self.wg.E_y(
                        self.wl, 
                        x, 
                        y, 
                        z)

        return E_y


    def grating(self, x):
        """
        Defines grating function.

        :param x: x coordinate
        :type x: array(float, 1d)
        :returns: grating function grating(x)
        :rtype: array(float, 1d)

        """
        sin_or_rect = self.sin_or_rect

        # sinusoidal grating
        if sin_or_rect == 'sin':
            grating = self.t_m/2 * np.sin(2*np.pi/self.Lambda*x) + self.t_m/2

        # rectangular grating
        if sin_or_rect == 'rect':
            grating = self.t_m/2 * signal.square(2*np.pi/self.Lambda*x) + self.t_m/2

        return grating


    def integral_EyEy(self, x):
        """ 
        Computes integral of EyEy along the interval 0 to grating(x)
        """
        z = np.linspace(0, 700e-9, 100)

        # get E_y array
        E_y = self.get_Ey(z)

        # conjugate
        Ey_conj_Ey = np.conjugate(E_y)*E_y

        # seperate real and imag part for complex integration
        EyEy_real = Ey_conj_Ey.real
        EyEy_imag = Ey_conj_Ey.imag

        integral_EyEy_complex = np.zeros(len(x),dtype=np.complex_)
        integral_EyEy_real = np.zeros(len(x))
        integral_EyEy_imag = np.zeros(len(x))


        # loop through x for integration of grating_function(x)
        for i in xrange(0,len(x)):
            integral_EyEy_real[i] = integrate.quad(
                                            lambda self, i: EyEy_real[np.int(i)],
                                            0,
                                            self.grating(x[i]),
                                            args=(x[i],)
                                         )[0]

            integral_EyEy_imag[i] = integrate.quad(
                                            lambda self, i: EyEy_imag[np.int(i)],
                                            0,
                                            self.grating(x[i]),
                                            args=(x[i],)
                                         )[0]

            integral_EyEy_complex[i] = integral_EyEy_real[i] \
                                            + 1j*integral_EyEy_imag[i]

        return integral_EyEy_complex


    # Coupling coefficient
    def coupling_coefficient(self):
        """ Function needs to be described.
            
            :param x: x coordinate
            :type x: array(float, 1d)
            :returns: the coupling coefficient K(x)
            :rtype: array(complex, 1d)


            .. note:: Used to be called 'couplingCoef'
        """
        x = self.x

        return self.omega*constants.eps_0 / (4*1j*self.wg.inputPower)           \
               * (self.n_m**2-self.wg.n_c**2) * self.integral_EyEy(x)


    def coupling_coefficientDirect(self):
        self.n_m = self.calc_n_m(self.t_f)
        self.t_eff = self.wg.calcEffThickness(self.wl)
        self.K = 2 * np.pi * (self.n_f**2-self.N**2)                            \
             * (self.n_m**2-self.n_c**2)                                        \
             / (1j * self.N * self.wl * self.t_eff                              \
             * (self.n_f**2-self.n_c**2))

        return self.K


    def error_estimation_simps(self, f, x):
        """ Calculates error of numerical intergration using simpson rule.

            :param f: f coordinate
            :type f: 1d array
            :returns: grating function grating(x)
            :rtype: 1d array
            
            :reference: J. B. Scarborough, "Formulas for the Error in 
                        Simpson's Rule", URL: 
                        http://www.jstor.org/stable/2300068

            .. note:: Used to be called 'gratingFunction'
        """
        self.n = len(self.f)
        self.a = self.x[0]
        self.b = self.x[self.n-1]
        self.h = (self.b-self.a)/self.n
        self.df3_d3x = np.diff(self.f,3)
        self.error = -self.h**4/180 * (self.df3_d3x[len(self.df3_d3x)-1] \
                     - self.df3_d3x[0])
        return self.error


    def reflection_efficency(self):
        """ Function needs to be described.
            
            :param x: x coordinate
            :type x: array(float, 1d)
            ...
            :returns: the total reflection efficency (P_refl/P_in)
            :rtype: float
        """

        N, t_eff, beta = self.buildWaveguide()

        x = self.x
        c_plus_0 = 1
        integrate_c = self.coupling_coefficient() * \
                           np.exp(-1j*2*beta*x)

        # Numerical integration of samples using Simpson's rule.
        integral_c = integrate.simps(integrate_c,x)

        c_minus = -c_plus_0 * integral_c
        P_refl_over_P_in = abs(c_minus)**2
        return P_refl_over_P_in


    def reflection_efficencyDirect(self):
        """ Function needs to be described.
            
            :param x: x coordinate
            :type x: array(float, 1d)
            ...
            :returns: the total reflection efficency (P_refl/P_in)
            :rtype: float

            .. note:: Used to be called 'powerRatio'
        """
        K_hat = self.coupling_coefficientDirect()
        self.P_refl_over_P_in = abs(- K_hat * self.t_m/2 * self.L/2)**2
        return self.P_refl_over_P_in, K_hat



    def reflectionEfficencyAnalytic(self):
        """
            
        """

        N, t_eff, beta = self.buildWaveguide()

        # equ. (1.76)
        c_minus_mu_over_c_plus_nu =                                         \
            - np.pi/(1j*2*self.wl)                                          \
                * (self.n_m**2-self.n_c**2)/(self.n_f**2-self.n_c**2)       \
                * (self.n_f**2-N**2)/N                                      \
                * self.t_m/t_eff * self.L

        # equ. (1.77)
        P_refl_over_P_in = np.abs(c_minus_mu_over_c_plus_nu)**2
        return P_refl_over_P_in



    def compareWithMolo(self, eta=1.27323954474, A=40e-9):
        """
            eta : aspect ratio
        """

        L = np.sqrt(A/eta)
        D = 2*L*np.sqrt(eta/np.pi)


        N, t_eff, beta = self.buildWaveguide()

        self.P_refl_P_in = self.reflectionEfficencyAnalytic()

        # equ (2.80)
        self.P_diff_P_in = (self.n_f**2-N**2)*(self.n_m**2-self.n_c**2)**2   \
                                / (N * (self.n_f**2-self.n_c**2))            \
                            * self.wl/self.n_c * self.t_m**2                 \
                                / (self.wl**3 * t_eff)                       \
                            * np.pi * D / 2                                  \

        # equ (2.85)
        self.P_refl_P_diff = self.P_refl_P_in / self.P_diff_P_in
        
        # equ (2.87)
        self.P_refl_P_diff = np.pi**2 / 8                                     \
                             * self.n_c*(self.n_f**2-N**2)                    \
                                / (N*(self.n_f**2-self.n_c**2))               \
                             * self.L / (t_eff)


        # equ (2.99)
        P_refl_P_diff = np.pi**(1.5)/4.                                       \
                             * self.n_c*(self.n_f**2-N**2)                    \
                             / (N*(self.n_f**2-self.n_c**2))                  \
                             * np.sqrt(A) / (t_eff * eta)

        return P_refl_P_diff



    def compareWithMoloRealConfiguration(self):
        """
            eta : aspect ratio
        """

        L_eff_bragg = self.L  #    [um]
        L_eff_molo = 286e-6     #   [um]

        N, t_eff, beta = self.buildWaveguide()

        P_refl_P_in = self.reflectionEfficencyAnalytic()

        # equ (2.80)
        P_diff_P_in = (self.n_f**2-N**2)*(self.n_m**2-self.n_c**2)**2        \
                                / (N * (self.n_f**2-self.n_c**2))            \
                            * self.wl/self.n_c * self.t_m**2                 \
                                / (self.wl**3 * t_eff)                       \
                            * np.pi * L_eff_molo / 2                         \

        # equ (2.85)
        P_refl_P_diff_1 = P_refl_P_in / P_diff_P_in
        
        # equ (2.85)
        P_refl_P_diff_2 = np.pi / 2                                           \
                             * self.n_c*(self.n_f**2-N**2)                    \
                                / (N*(self.n_f**2-self.n_c**2))               \
                             * L_eff_bragg**2 / (t_eff*L_eff_molo)


        return P_refl_P_diff_1, P_refl_P_diff_2



    def twoDimReflectionEfficiency(self):
        """
        """

        theta = self.theta

        x = self.x
        c_plus_0 = 1
        integrate_c = self.coupling_coefficient() * \
                           np.exp(-1j*2*self.wg.beta*np.cos(self.theta)*x) \
                           # * np.cos(2*theta)/np.cos(theta)

        # Numerical integration of samples using Simpson's rule.
        integral_c = integrate.simps(integrate_c,x)

        c_minus = -c_plus_0 * integral_c
        P_refl_over_P_in = abs(c_minus)**2
        return P_refl_over_P_in


    def twoDimReflectionEfficiencyAnalytic(self):
        """
        """

        theta = self.theta

        P_refl_over_P_in = self.reflectionEfficencyAnalytic()*abs(np.cos(2*theta)/np.cos(theta))**2
        return P_refl_over_P_in, abs(np.cos(2*theta)/np.cos(theta))**2




if __name__ == '__main__':
    # Initialisation Waveguide
    # glassSubstrate = IsotropicMaterial(
    #                    name='Glass Substrate (D 263)', 
    #                    n_0=1.521)
    
    # aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33)


    param_waveguide = {
        "t_f": 145e-9,
        "inputPower": 1,
        "wl": 632.8e-9,
        "n_s": 1.521,
        "n_f": 2.117,
        "n_c": 1.33,
        "polarization": 'TE',
        "attenuationConstant": 0.0,
    }
    param_bragg = {
        "a": 400.0e-6,
        "L": 152e-6,         # 250.0e-6,
        "t_m": 5e-9,
        "Delta_Gamma": 1,
        "Lambda": []    #1.74619975729e-07
    }



    # Sweep parameters
    # L = np.linspace(50e-6, 600e-6, 10)
    # t_f = np.linspace(0, 250e-9, 100)
    # Delta_Gamma = np.linspace(-5000,20000, 100)

    bragg = BraggMolography(
                param_waveguide=param_waveguide,
                param_bragg=param_bragg,
                sin_or_rect='sin', 
                two_dim=False)

    L = param_bragg["L"]
    a = param_bragg["a"]
    D = L * 4/np.pi
    eta = a/L
    bragg.compareWithMolo()
    # bragg.coupling_coefficient(bragg.x)


    # Printing some results and important parameters


    print ("Total reflection efficency NUMINTEGRAL: " 
         + str(bragg.reflection_efficency()))

    print ("Total reflection efficency DIRECT: " 
           + str(bragg.reflection_efficencyDirect()))

    print ("Total reflection efficency ANALYTICAL: " 
           + str(bragg.reflectionEfficencyAnalytic()))

    print ("Total reflection efficency 2D NUMERICAL: " 
           + str(bragg.twoDimReflectionEfficiency()))

    print ("Total reflection efficency 2D ANALYTICAL: " 
           + str(bragg.twoDimReflectionEfficiencyAnalytic()))
   
    print ("Coupling Coefficient DIRECT: " 
           + str(bragg.coupling_coefficientDirect()))
    print ("Lambda: " + str(bragg.Lambda))
    print ("omega: " + str(bragg.omega))
    print ("eps_0: " + str(constants.eps_0))
    print ("mu_0: " + str(constants.mu_0))
    print ("kappa: " + str(bragg.wg.kappa))
    print ("beta: " + str(bragg.beta))
    print ("gamma: " + str(bragg.wg.gamma))
    print ("delta: " + str(bragg.wg.delta))

    print ("n_s: " + str(bragg.n_s))
    print ("n_f: " + str(bragg.n_f))
    print ("n_c: " + str(bragg.n_c))
    print ("n_m: " + str(bragg.n_m))
    print ("N_eff: " + str(bragg.N))
    print ("t_eff: " + str(bragg.t_eff))

    print("theta: " + str(bragg.theta))
    print ("Prefl_Pdiff: ")
    print bragg.compareWithMoloRealConfiguration()
    print ("Bragg reflection efficiency")
    print(bragg.reflectionEfficencyAnalytic())


    # self.txt_file = []
    # file = open("t_f_anlytic.txt","w")
    # for i in xrange(0,len(self.P)):
    #     file.write(str(self.xAxis[i])+" "+str(self.P[i][0])+"\n")
       
    # file.close()



