"""
Generates distrubances


:Author: Yves Blickenstorfer
:Revised: 4-Jan-2017

"""

import sys
sys.path.append("../")
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
from scipy import signal
import auxiliaries.constants as constants 
import matplotlib.pyplot as plt





class BraggGrating(object):
	"""generates a BraggGrating of the right period for a certain waveguide

	:param waveguide: waveguide on which the grating is generated it is adapted to the neff of the waveghide
	:type waveguide: waveguide
	:param n_groove: index of the groove
	:type n_goove: float[:]
	:param n_ridge: index of the ridge
	:type n_ridge: float[:]
	:param gratingLength: Length of the grating
	:gratingLength: float[:]
	:param form: defindes the form the grating (sin or square wave)
	:type from: string (square wave)
	:param proteinThickness: thickness of the protein layer 5nm by default
	:type proteinThickness: float



	"""
	def __init__(self, waveguide,n_groove,n_ridge,gratingLength,matchingWavelegnth,form = 'square wave',proteinThickness=5e-9):
		

		self.wg = waveguide
		self.n_groove = n_groove
		self.n_ridge = n_ridge
		self.wavelength = matchingWavelegnth

		self.gratingLength = gratingLength

		self.period = self.wavelength / (2*self.wg.n_eff)
		self.proteinThickness = proteinThickness

		self.N = gratingLength / self.period

		self.form = form


		# z = np.linspace(0,10 * self.period,100)
		# plt.plot(z,self.f(z))
		# plt.plot(z,np.exp(1j * 2 * self.wg.beta*(z- 1./4 * self.period))*5e-9)
		# plt.show()


		
		#period of a normal signal square is 2 * pi, so to change period we have to multiply z by 2*pi/period
		#normal signal.square goes from 1 to -1 to change from 0 to 1 times 1/2. and add 1/2
		#proteinThickness to make from 0 to protein thickness


	def f(self,z):
		if self.form == 'square wave':

			f = (1./ 2 * signal.square(2*np.pi / self.period * z) + 1./2) * self.proteinThickness

		if self.form == 'sin':
			f = (1./2 * np.sin(2*np.pi / self.period * z) + 1./2) * self.proteinThickness

		return f


