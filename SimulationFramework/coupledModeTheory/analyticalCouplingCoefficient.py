


import numpy as np
import scipy.special as special
import sys
sys.path.append("../")



def couplingCoefficient(wg,n_ridge):

    prefactor = wg.a * np.pi * wg.angularFrequency * constants.eps_0 * ( wg.n_medium**2 - n_ridge**2)\
                / (4 * 1j * wg.inputPower)

    fieldComponent = wg.normalized *( ( 2 * wg.a_1**2 * special.jv(wg.nu-1, wg.U)**2 \
                        + 2 * wg.a_2**2 * special.jv(wg.nu+1, wg.U)**2 ) \
                        / special.jv(wg.nu,wg.U)**2 \
                        + wg.U**2 / ( wg.a**2 *wg.beta**2 ) * wg.n_medium**2 / wg.n_wg **2)
    return prefactor * fieldComponent



def couplingCoefficientPlanar(wg,n_ridge):
    wg.calcPropagationConstants(wg.wavelength)


    couplingCoefficient = (2*np.pi / wg.wavelength* constants.c )**2 * constants.eps_0 *constants.mu_0* wg.kappa**2 \
    / (1j * abs(wg.beta) * (wg.thickness + 1/wg.gamma + 1/wg.delta)\
       * (np.square(wg.kappa) + np.square(wg.delta)))\
     *(n_ridge**2-wg.n_c**2)

    return couplingCoefficient




if __name__ == "__main__":


    from coupledModeTheory.idealModeExpansion import IdealModeExpansionCylinderBragg
    from waveguide.CylindricalWaveguide import CylindricalWaveguide
    from coupledModeTheory.disturbance import BraggGrating
    from waveguide.waveguide import SlabWaveguide
    import auxiliaries.constants as constants
    import auxiliaries.pareto as pareto
    import matplotlib.pyplot as plt





    wavelength_array= 400e-9#632.8e-9
    thickness_array = np.linspace(100,200,50)*1e-9 #2.34693877551e-07
    thickness_planar_array = 145*1e-9
   
    n_ridge_array = 1.3566
    matchingWavelength_array= wavelength_array
    gratingLength_array = 1e-4#500e-6
    n_wg_array = 2.5 #1.45372
    n_medium_array = 1.33#1.3178
    inputPower_array= 1.
    n_groove_array = n_medium_array
    thickness_protein = 5e-9
    n_wg_planar = 2.117
    n_substrate = 1.1 #1.521
    nu = 1



    parameters = pareto.paretoList(wavelength_array,n_wg_array,thickness_array,
                                thickness_planar_array, \
                                n_ridge_array,gratingLength_array,\
                                n_medium_array,matchingWavelength_array,\
                               inputPower_array, n_groove_array)


    reflection_cylinder_ana_array = np.empty(len(parameters))
    backCoupling_array = np.empty(len(parameters))
    i = 0


    for wavelength, n_wg, thickness,thickness_planar, n_ridge, gratingLength, n_medium, \
            matchingWavelength, inputPower, n_groove in parameters:

       
        wg_p = SlabWaveguide('slab', n_substrate, n_wg_planar, n_medium,thickness_planar, 'TE', 1., wavelength)
        c_planar = couplingCoefficientPlanar(wg_p,n_ridge)

        wg = CylindricalWaveguide(thickness, n_wg, n_medium, wavelength, inputPower)
        dis = BraggGrating(wg,n_ridge,n_groove,gratingLength,matchingWavelength,form = 'sin')
        CMT = IdealModeExpansionCylinderBragg(wg,dis)
        c_cylinder = couplingCoefficient(wg,n_ridge)

        reflection_cylinder_ana = c_cylinder * np.conjugate(c_cylinder)* (thickness_protein / 2 * gratingLength/ 2)**2
        reflection_cylinder_ana_array[i] = reflection_cylinder_ana
        reflection_planar_array = c_planar * np.conjugate(c_planar) * (thickness_protein / 2 * gratingLength/ 2)**2

        backCoupling = CMT.backCoupledWave()
        backCoupling_array[i] = backCoupling

        i = i + 1

    plt.plot(thickness_array,reflection_cylinder_ana_array)
    plt.plot(thickness_array,backCoupling_array)
    plt.show()
    # print backCoupling
    # print 'cylinder', reflection_cylinder_ana_array
    # print 'planar', reflection_planar_array

