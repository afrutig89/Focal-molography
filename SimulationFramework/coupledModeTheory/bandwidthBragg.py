import numpy as np
import matplotlib.pyplot as plt


wavelength = 1500
S = 0.5
n_groove = 1.32
n_ridge = 1.33
N=1000
powerInGrating = 5e-4 #educated guess
delta_n = np.linspace(2e-4,2e-2,10000)


bandwidth_pp = wavelength * S * np.sqrt((delta_n/(2*n_groove))**2 + (1./N)**2)
bandwidth_wiki = 2 * (n_ridge-n_groove) * powerInGrating / np.pi * wavelength
print(bandwidth_wiki)


plt.plot(delta_n,bandwidth_pp)
plt.show()
