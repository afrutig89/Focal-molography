# -*- coding: utf-8 -*-

from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import numpy as np
import time
import datetime

import matplotlib.pyplot as plt


import materials.getData as RefInd
import auxiliaries.pareto as pareto
from waveguide.CylindricalWaveguide import CylindricalWaveguide
from coupledModeTheory.disturbance import BraggGrating
from coupledModeTheory.idealModeExpansion import IdealModeExpansionCylinderBragg
from SharedLibraries.Database.dataBaseOperations import createDB, saveDB, loadDB


# name = 'IdealThickness'
# directory = 'C:\Users\\blyve\Documents\PHD\Data\Simulation Data\\'


# par = {
#     'ID':'integer primary key AUTOINCREMENT',
#     'wavelength':'real',
#     'n_wg':'real',
#     'n_medium':'real',
#     'idealThickness_neffInflection':'real',
#     'idealThickness_couplingMax':'real',
#     'date' : 'text',
#     'startTime' : 'text',
#     'error': 'real'
# }

# date = str(datetime.date.today())
# startTime = time.strftime("%H:%M:%S")


# nameCoef = 'IdealThicknessCoeficients'
# directoryCoef = 'C:\Users\\blyve\Documents\PHD\Data\Simulation Data\\'


# thickness_couplingMax_data = []
# thickness_neffInflection_data =  []
# wavelength_data = []
# n_wg_data = []


# simulations = loadDB(directory + name + '.db', 'Data')

# for simulation in simulations:

#     wavelength_data.append(simulation['wavelength'])
#     thickness_neffInflection_data.append(simulation['idealThickness_neffInflection'])
#     thickness_couplingMax_data.append(simulation['idealThickness_couplingMax'])
#     n_wg_data.append(simulation['n_wg'])


# xAxis = 'wavelength'
# yAxis = 'n_wg'
# zAxis = 'idealThickness_neffInflection'

# # get Axes
# xAxisList = []
# yAxisList = []


# for simulation in simulations:
#     # x-axis
#     if simulation[xAxis] not in xAxisList:
#         xAxisList.append(simulation[xAxis])
#     # y-axis
#     if simulation[yAxis] not in yAxisList:
#         yAxisList.append(simulation[yAxis])

# # sort in order to plot afterwards
# xAxisList.sort()
# yAxisList.sort()

# # make meshgrid of the two axes
# X, Y = np.meshgrid(xAxisList, yAxisList)


# #generate z-axis 
# Z = np.zeros(X.shape)

# for simulation in simulations:
#     Z[yAxisList.index(simulation[yAxis]),xAxisList.index(simulation[xAxis])] = simulation[zAxis]


# save = 'no'

# Z = Z[0:2]
# X = X[0:2]
# Y = Y[0:2]
# print Z

# for i in np.arange(np.shape(Z)[0]):
#     for j in np.arange(np.shape(Z)[1]):
#         if Z[i][j] == 0:
#             if Z[i][j+1] == 0 or Z[i][j-1] == 0:
#                 d_min = Z[i][j%4 * 4]
#                 d_max = Z[i][(j%4-1) * 4]
#                 print 'J_MAX',(j%4) * 4

#             else:
#                 d_min = Z[i][j-1]
#                 d_max = Z[i][j+1]


#             print d_min
#             print d_max
#             thickness_try = np.linspace(d_min,d_max,5)
#             wavelength = X[i][j]
#             n_wg = Y[i][j]

           
#             n_medium = RefInd.get_data('../materials/database/main/H2O/Hale.yml',wavelength*1e6)[0].real #1.3178 #
#             inputPower = 1.
#             n_ridge = 1.34
#             n_groove = n_medium
#             gratingLength = 1e-3
#             matchingWavelength = wavelength

#             neff = []
#             backCoupling_array =[]
            
#             for thickness in thickness_try:
#                 wg = CylindricalWaveguide(thickness,n_wg,n_medium,wavelength,inputPower)
#                 dis = BraggGrating(wg,n_ridge,n_groove,gratingLength,matchingWavelength)
#                 cmt = IdealModeExpansionCylinderBragg(wg,dis)

#                 neff.append(wg.n_eff)
#                 backCoupling = cmt.backCoupledWave()
#                 backCoupling_array.append(backCoupling)

#             print wavelength


         
#             neff = np.asarray(neff)

#             index = np.diff(neff).argmax()
#             index_2 = np.argmax(backCoupling_array)

#             idealThickness_neffInflection = thickness_try[index]
#             idealThickness_couplingMax = thickness_try[index_2]
#             error = idealThickness_neffInflection - idealThickness_couplingMax
#             print 'ideal thickness point of inflection', idealThickness_neffInflection
#             print 'ideal thickness max', idealThickness_couplingMax
#             # plt.plot(thickness_try,neff)
#             # plt.show()
#             plt.plot(thickness_try,backCoupling_array)
#             plt.show()
#             # # print backCoupling_array
#             # print thickness_try



#             if save == 'yes':
#                 createDB(directory + name + '.db', 'Data', par)
#                 saveDB(directory + name + '.db', 'Data', locals())







save = 'no'
# name = 'glass'
number = 1
a_min_array = np.linspace(0.7e-6,0.7e-6,number)
a_max_array = np.linspace(3.5e-6,3.5e-6,number)
wavelength_array = np.linspace(1560,1560,number)*1e-9
# wavelength_array = np.linspace(632.8,632.8,number)*1e-9
n_wg_array = np.linspace(1.45372,1.45372,number)

parList = []
for i in range(number):
    param = np.array([a_min_array[i], a_max_array[i],wavelength_array[i], n_wg_array[i]])
    parList.append(param)
print parList

for a_min, a_max, wavelength, n_wg in parList:



    thickness_try = np.linspace(a_min,a_max,50)
    # wavelength = 1560
   
    n_medium = RefInd.get_data('../materials/database/main/H2O/Hale.yml',wavelength*1e6)[0].real #1.3178 #
    inputPower = 1.
    n_ridge = 1.34
    n_groove = n_medium
    gratingLength = 1e-4
    matchingWavelength = wavelength

    neff = []
    backCoupling_array =[]
    
    for thickness in thickness_try:
        wg = CylindricalWaveguide(thickness,n_wg,n_medium,wavelength,inputPower)
        dis = BraggGrating(wg,n_ridge,n_groove,gratingLength,matchingWavelength)
        cmt = IdealModeExpansionCylinderBragg(wg,dis)

        neff.append(wg.n_eff)
        backCoupling = cmt.backCoupledWave()
        backCoupling_array.append(backCoupling)

    print wavelength


 
    neff = np.asarray(neff)

    index = np.diff(neff).argmax()
    index_2 = np.argmax(backCoupling_array)

    idealThickness_neffInflection = thickness_try[index]
    idealThickness_couplingMax = thickness_try[index_2]
    error = idealThickness_neffInflection - idealThickness_couplingMax
    print 'ideal thickness point of inflection', idealThickness_neffInflection
    print 'ideal thickness max', idealThickness_couplingMax
    fig = plt.figure()
    # plt.plot(thickness_try *1e9,neff)
    # plt.show()

    
    plt.plot(thickness_try * 1e9,backCoupling_array)
    plt.xlabel('diameter [nm]')
    plt.ylabel('reflectivity')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    name = 'wavelengt' + str(wavelength) + '_n_wg' + str(n_wg) + '_gratinglength' + str(gratingLength) +  '_deltan'+str(n_medium-n_ridge)
    print name
    directory = 'C:\Users\\blyve\Documents\PHD\Plots\Lukosz Kurven\\'
    imageFormat = 'pdf'
    fig.savefig(directory + name + '.'+imageFormat, format=imageFormat)

    plt.show()
    # # print backCoupling_array
    # print thickness_try



    if save == 'yes':
        createDB(directory + name + '.db', 'Data', par)
        saveDB(directory + name + '.db', 'Data', locals())


