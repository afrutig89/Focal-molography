"""
Caclculation of back coupling of a mode by ideal mode theory

..todo: make work for roughness

:Author: Yves Blickenstorfer
:Revised: 4-Jan-2017
:Reference: Marcuse Theory of dielectric waveguides

"""



import sys
sys.path.append("../")
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import auxiliaries.constants as constants 



class IdealModeExpansionCylinderBragg(object):
	"""
	Uses Ideal mode expansion to apply coupling mode theory. For this a waveguide (ideal) and a disturbance is used
	and the coupling to the first reflected mode is calculated in backCoupledWave

	:param waveguide: Has all the information about the waveguide and the light
	:type waveguide: waveguide
	:disturbanceFunction: objecta that contains all the information about the disturbance
	:type disturbanceFunction: distrubance
	:n_disturbance: refractive index of the disturbance
	:type n_distrubance: float
	:inputType: not clear wether really neaded
	:type inputType: string



	"""
	def __init__(self, waveguide, disturbance, inputType = 'random function', disturbanceType = 'bragg grating'):
		
		self.wg = waveguide
		self.n_wg = waveguide.n_wg
		self.n_medium = waveguide.n_medium
		self.dis = disturbance
		self.disturbanceType = disturbanceType
	

		self.inputType = inputType

		if disturbanceType == 'bragg grating':
			self.n_1 = self.dis.n_ridge
			self.n_2 = self.dis.n_groove
		if disturbanceType == 'roughness':
			self.n_1 = self.dis.n_f
			self.n_2 = self.dis.n_w



	def backCoupledWave(self):

		"""calculates the reflected power using the coupling coeficient and integration the grating
		:refernece: the formula 3.2-53 in dietrich marcuse theory of dielectric waveguides p. 105
		a phaseshift (- 1/4 * self.period) is applied to match the distortion with the sinosoidal curve
		"""


		if self.inputType == 'points':
			integral = np.sum(self.dis.fz[1:]  * np.exp(1j * 2 * self.wg.beta * (self.dis.z[1:] - 1./4 * self.dis.period)) * np.diff(self.dis.z))
			
			
		elif self.disturbanceType == 'random function':

			integral = integrate.quad(lambda z:\
				self.dis.f(z) * np.exp(1j * 2 * self.wg.beta*(z - 1./4 * self.dis.period))\
				 #integration boundaries
				, self.dis.disLength	,0,limit=self.lim)[0]

		else:
				
				
			# integral = integrate.quad(lambda z:\
			# self.dis.f(z) * np.exp(1j * 2 * self.wg.beta*(z - 1./4 * self.dis.period))\
			#  #integration boundaries
			# , self.dis.period,0)[0] * self.dis.N

			z = np.linspace(0,self.dis.gratingLength,1e5)
			# plt.plot(z,self.dis.f(z))
			# plt.xlim(0,5*self.dis.period)
			# plt.show()

			integral = np.sum(self.dis.f(z[1:])  \
					   * np.exp(1j * 2 * self.wg.beta \
					   * (z[1:] - 1./4 * self.dis.period)) * np.diff(z))
			
 

		c_minus =  self.couplingCoeficient() * integral
	
		reflectedPower = np.conjugate(c_minus) *c_minus* self.wg.inputPower
	
		return reflectedPower



	def couplingCoeficient(self):
		"""
		finds the coupling Cooeficient between of an incoming zero order wave into
		into a reflected zeroth order wave

		:refernece: The fomula is based on Dietrich marcuse Theory of dielectric optical waveguides p. 104 eq: 3.2-44
		and the derivation of formula 3.5-6 on  p. 117.

		How to get to my formula from 3.2-44:
			beta is real and in opposite direction: s_mu = 1, abs(beta)/beta = -1
			n2-n_0 can be taken out of integral
			The E field is split into radial, tangentaial and z component
			Because the E dependence on phi is exp(i*nu*phi) and nu is zero for zeroth order.
				E is constant with respect to phi (see light transmission optics p. 293-294) 
				Thus the integral over nu becomes a multiplicaion by 2 pi."""



		prefactor =   self.wg.angularFrequency * constants.eps_0 *(self.n_2**2-self.n_1**2)*self.wg.a/ (4 *self.wg.inputPower*1j)


		# print(float(integratedFunction(1))	
		
		integral = integrate.quad(lambda phi: self.integrateFunction(phi),0,2*np.pi)[0]

		couplingCoef = prefactor * integral

		return couplingCoef


		

	def integrateFunction(self,phi):
		fun = np.conjugate(self.wg.E_r(self.wg.a,phi)) * self.wg.E_r(self.wg.a,phi) \
			 + np.conjugate(self.wg.E_phi(self.wg.a,phi)) * self.wg.E_phi(self.wg.a,phi)\
		 	 + (self.n_medium**2/self.n_wg**2)*np.conjugate(self.wg.E_z(self.wg.a,phi)) * self.wg.E_z(self.wg.a,phi) 
		
		return fun.real











