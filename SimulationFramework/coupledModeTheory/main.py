"""
Main for calculation of Bragg Reflection in a cylindrical Waveguide

:Author: Yves Blickenstorfer
:Revised: 6-Jan-2017


"""


from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt
import auxiliaries.constants as constants
import auxiliaries.pareto as pareto
from SharedLibraries.Database.dataBaseOperations import createDB, saveDB, loadDB
from binder.Adlayer import ProteinSurfaceMassModulationAdlayer
from coupledModeTheory.disturbance import BraggGrating
from coupledModeTheory.idealModeExpansion import IdealModeExpansionCylinderBragg
from waveguide.CylindricalWaveguide import CylindricalWaveguide
from scipy.optimize import curve_fit
import materials.getData as RefInd



par = {
    'ID':'integer primary key AUTOINCREMENT',
    'wavelength':'real',
    'matchingWavelength': 'real',
    'gratingLength': 'real',
    'inputPower':'real',
    'nu':'integer',
    'n_wg':'real',
    'n_medium':'real', 
    'n_groove':'real',
    'thickness':'real',
    'polarization':'text',
    'backCoupling': 'complex',
    'n_ridge': 'real',
    'modulation':'real',
    'n_eff': 'real',
    'date' : 'text',
    'startTime' : 'text'

}



date = str(datetime.date.today())
startTime = time.strftime("%H:%M:%S")

name = 'BackCoupling'
directory = 'C:\Users\\blyve\Documents\PHD\Data\Simulation Data\\'



#Input Variables
save = 'no'
wavelength_array= np.linspace(400e-9,1600e-9,30) #np.linspace(400e-9,1600e-9,100)
thickness_array  = 1 #np.linspace(100,200,10)*1e-9
modulation_array = 283 #pg/mm^2
# n_ridge_array = 1.34
matchingWavelength_array = 400e-9
gratingLength_array = 100e-6
n_wg_array = 2 #1.45372
n_medium_array = 1.3178
inputPower_array = 1.
polarization = 'HE'
RI = ProteinSurfaceMassModulationAdlayer(n_solv=n_medium_array, \
    rho_solv =   1.0028, delta=5e-9, Gamma_Delta=modulation_array)
n_ridge_array = RI.refIndexProteinLayer()
n_ridge_array = 1.34
nu = 1



n_groove_array = n_medium_array

#neede so one can use array and lists as input parameters
parameters = pareto.paretoList(wavelength_array,n_wg_array,thickness_array,\
                                n_ridge_array,gratingLength_array,\
                                n_medium_array,matchingWavelength_array,\
                               modulation_array, inputPower_array, n_groove_array)


if save == 'yes':
    createDB(directory + name + '.db', 'Data', par)





#actual calculation
wg = []
wg_ana = []
dis = []
dis_ana = []
cmt = []
cmt_ana = []
n_eff_array= []
n_eff_array_ana = []
backCoupling_array = np.empty(len(parameters))
backCoupling_array_ana = np.empty(len(parameters))
i = 0

thickness_array_1=[]
value_array =[]

for wavelength, n_wg, thickness, n_ridge, gratingLength, n_medium, \
            matchingWavelength, modulation, inputPower, n_groove in parameters:

    n_medium = RefInd.get_data('../materials/database/main/H2O/Hale.yml',wavelength*1e6)[0].real #1.3178 #
    # thickness = (117.080509 /(-1.348198 + n_wg)**0.454494 +7.210039)* 1e-9
    thickness = (0.372743 * wavelength * 1e9 + 0.991620)*1e-9
    RI = ProteinSurfaceMassModulationAdlayer(n_solv=n_medium_array, \
    rho_solv =   1.0028, delta=5e-9, Gamma_Delta=modulation_array)
    n_ridge_array = RI.refIndexProteinLayer()
   
    thickness_array_1.append(thickness)
    matchingWavelength = wavelength


    wg.append(CylindricalWaveguide(thickness,n_wg,n_medium,wavelength,inputPower))
    wg_ana.append(CylindricalWaveguide(thickness,n_wg,n_medium,wavelength,inputPower,calcMethod = 'analythically'))
    dis.append(BraggGrating(wg[i],n_ridge,n_groove,gratingLength,matchingWavelength))
    dis_ana.append(BraggGrating(wg_ana[i],n_ridge,n_groove,gratingLength,matchingWavelength))
    cmt.append(IdealModeExpansionCylinderBragg(wg[i],dis[i]))
    cmt_ana.append(IdealModeExpansionCylinderBragg(wg_ana[i],dis_ana[i]))



    # value_array.append(wg[i].value * (n_medium**2-n_ridge**2)/4)
    backCoupling = cmt[i].backCoupledWave()
    backCoupling_array[i] = backCoupling

    backCoupling_ana = cmt_ana[i].backCoupledWave()
    backCoupling_array_ana[i] = backCoupling_ana

    n_eff = wg[i].n_eff
    n_eff_ana = wg_ana[i].n_eff
    beta = wg[i].beta

    n_eff_array.append(n_eff)
    n_eff_array_ana.append(n_eff_ana)


    # print wavelength
    # print matchingWavelength
    # print gratingLength
    # print inputPower
    # print nu
    # print n_wg
    # print n_medium
    # print n_groove
    # print thickness
    # print polarization
    # print backCoupling
    # print n_ridge 
    # print modulation
    # print n_eff 
    # print date 
    # print startTime 






    if save == 'yes':    
        saveDB(directory + name + '.db', 'Data', locals())

    i = i + 1




print backCoupling
# saveDB('database.db', 'results', locals())


#plotting
# plt.plot(thickness_array,backCoupling_array)
# plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))


# plt.plot(thickness_array,backCoupling_array_ana)
# plt.show()
# plt.plot(thickness_array, n_eff_array)
# plt.plot(thickness_array, n_eff_array_ana)
# print value_array

# plt.semilogy(n_wg_array,np.square(value_array)* (5e-9 * gratingLength * 1/4)**2)
# plt.show()
# plt.plot(n_wg_array, thickness_array_1)
# plt.show()
directory = 'C:\Users\\blyve\Documents\PHD\Plots\\reflectivity optimation\\'
name = 'n_wg' + str(n_wg) + 'modulation' + str(modulation) + 'gratingLength' + str(gratingLength)
fig = plt.figure()

plt.semilogy(wavelength_array,backCoupling_array)

plt.xlabel('wavelength [nm]')
plt.ylabel('reflectivity')

# plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
imageFormat = 'pdf'
fig.savefig(directory + name + '.'+imageFormat, format=imageFormat)




plt.show()








