import numpy as np
import matplotlib.pylab as plt
import scipy as sci
import pandas as pd

from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from imageProcessing.MologramProcessing import calc_IntAiryDisk
from scipy import interpolate
from scipy import constants
from auxiliaries.screen import defineScreen
from SharedLibraries.Database.dataBaseOperations import saveDB, createDB, loadDB
from auxiliaries.Journal_formats_lib import formatPRX
from auxiliaries.plotformat import scatteredIntensityPlot
from variables import CameraSimulationResults, commonKeys
from camerasimulation.CameraSensorModels import ReturnCameraSensorNames, ReturnCameraSensorParameters

# ------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------
# imports for testing purposes
from scipy import misc

from camerasimulation.CameraSensorModels import CameraSensorParameters

class CameraSensorSimulation(object):
    """Simulates the acquisition of an image (physical units must be supplied) by a certain camera model.
        :param (CameraSensorParameters) cameraSensor: if supplied then this camera sensor is used and not the one specified in the simulation
        parameters (allows for sweeps of the camera parameters)
    """

    def __init__(self, camSimParameters,fieldSimulation = '',plots='',cameraSensor=''):

        self.camSimInput = camSimParameters

        # get the simulation log and the results from the old simulation
        if fieldSimulation is not '':
            self.simInput = fieldSimulation.simInput.getValues()
            self.results = fieldSimulation.results.getValues()

            values = {}
            values['ID_results'] = loadDB(self.simInput['databaseFile'], 'log', selection = 'ID', condition = 'ORDER BY ID DESC LIMIT 1')

            self.camSimInput.setValues(values)
        else:
            self.simInput = loadDB(self.camSimInput.getValues()[
                                   'databaseFile'], 'log', condition='WHERE ID==' + str(self.camSimInput.getValues()['ID_results']))
            self.results = loadDB(self.camSimInput.getValues()['databaseFile'],'results',condition='WHERE ID==' + str(self.camSimInput.getValues()['ID_results']))

        # camera sensor to use for acquisition.
        if cameraSensor == '':
            self.camerasensor = ReturnCameraSensorParameters(self.camSimInput.getValues()['camModel'])
        else:
            self.camerasensor = cameraSensor



    def performSingleExposure(self):

        pixelsizeMoloPlane = float(self.simInput['screenWidth']/self.simInput['npix']) # pixelsize in the moloplane.
        magnification = float(self.camSimInput.getValues()['magnification'])
        wavelength = float(self.simInput['wavelength'])
        exposure_time = float(self.camSimInput.getValues()['exposureTime'])
        Airy_Disk_Radius = self.results['AiryDiskRadius']

        I_tot = self.results['I_sca']
        I_bg = self.results['I_bg_tot']

        camResults = processMoloExperiment(self.camerasensor,I_tot,I_bg,pixelsizeMoloPlane,magnification,wavelength,exposure_time,Airy_Disk_Radius)

        self.camResults = CameraSimulationResults()

        self.camResults.setValues(camResults)

        if type(plots) is dict:
            self.producePlots(plots)

        if self.simInput['saveLog']:
            self.camSimInput.createDB(self.camSimInput.getValues()[
                               'databaseFile'], 'camSimlog')
            self.camSimInput.saveDB()

        if self.simInput['saveDatabase']:
            self.camResults.createDB(self.camSimResults.getValues()[
                               'databaseFile'], 'camSimresults')
            self.camResults.saveDB()

    def optimizeAquisitionTimeInterval(self):

        """Function that aquires the maximum amount of images possible in a given time interval, the time interval is the exposure time."""

        # Input: Molographic Intensity distribution of a specified intensity and a time in seconds

        raise NotImplementedError

        pixelsizeMoloPlane = float(self.simInput['screenWidth']/self.simInput['npix']) # pixelsize in the moloplane.
        magnification = float(self.camSimInput.getValues()['magnification'])
        wavelength = float(self.simInput['wavelength'])
        exposure_time = float(self.camSimInput.getValues()['exposureTime'])
        Airy_Disk_Radius = self.results['AiryDiskRadius']

        I_tot = self.results['I_sca']
        I_bg = self.results['I_bg_tot']

        # Acquisition Time == Exposure Time, the exposure time is determined automatically.
        timeInterval = exposure_time



        # find the optimal exposure time for this camera
        I_tot_bn = SimulateImageCapture(camerasensor,I_bg, pixelsizeMoloPlane, magnification, wavelength, timeInterval)

        # check if the camera's pixelwell is near saturation,
        # if max(I_tot_bn) >= 4095*0.9:d

        #     print "hello"

        camResults = processMoloExperiment(self.camerasensor,I_tot,I_bg,pixelsizeMoloPlane,magnification,wavelength,exposure_time,Airy_Disk_Radius)


    def producePlots(self,plots):

        focalPoint = self.simInput['focalPoint']
        screen = defineScreen(self.simInput['screenWidth'],self.simInput['npix'],self.simInput['screenPlane'],self.simInput['screenRatio'],self.simInput['screenRotation'],focalPoint,self.simInput['center'])
        background_screen = defineScreen(self.simInput['screenWidth'],self.simInput['npix'],self.simInput['screenPlane'],self.simInput['screenRatio'],self.simInput['screenRotation'],focalPoint,self.simInput['centerBackground'])


        if 'scatteredIntensityCam' in plots.keys():

            formatPRX()
            fig = plt.figure()
            scatteredIntensityPlot(self.camResults.getValues()['I_tot_bn'],screen,focalPoint,fig,plots['scatteredIntensityCam'])

        if 'scatteredIntensityBackgroundCam' in plots.keys():

            formatPRX()
            fig = plt.figure()
            scatteredIntensityPlot(self.camResults.getValues()['I_bg_tot_bn'],background_screen,focalPoint,fig,plots['scatteredIntensityBackgroundCam'])


def SimulateImageCapture(camerasensor, image, pixelsizeMoloPlane, magnification, wavelength, exposure_time, poissonNoise=True, readoutNoise=True):
    """Simulates the capturing of an image and adds Poisson and ReadoutNoise to it. The returns as an array of integers with 12 bit resolution.

        Intensity to photon flux:

        ..math:: \\phi = I \\frac{\\lambda}{hc}

        Photon flux to number of photons per pixel

        ..math:: n_p = a^2*\\phi*t

        Add the poisson noise to the number of photons per pixel.

        Photon per pixel to electrons per pixel

        .. math:: n_e = q*n_p

        Add the read noise to the number of electrons per pixel.

        Electrons per pixel to Digital output units

        .. math:: M = k*n_e

        :param image: 2d numpy array of intensity values
        :param pixelsizeMoloPlane: pixelSize in the molographic Plane [m]
        :param magnification: Magnification of the imaging system
        :param wavelength: Wavelength of the mode in the waveguide [m]
        :param exposure_time: Exposure Time of the camera sensor in [s]
        :param (boolean) poissonNoise: default, true, add poissonNoise to the image (intensity noise)
        :param (boolean) readoutNoise: defautl, true, add readoutNoise to the image

    """

    # physical pixel size of the acquired image
    effPixelsize = pixelsizeMoloPlane*magnification

    # do the binning

    intensityFlux = BinnImage(
        image, effPixelsize, camerasensor.pixelsize)

    photonFlux = intensityFlux*wavelength/(constants.h*constants.c)

    photonsPerPixel = photonFlux * \
        (camerasensor.pixelsize)**2*exposure_time

    if poissonNoise:
        # add poisson noise to the captured photons, the argument of the
        # poisson distribution is the mean or the variance but not the std.
        photonsPerPixel = np.random.poisson(photonsPerPixel)

    # number of electrons via Quantum efficiency
    electronsPerPixel = camerasensor.QE*photonsPerPixel

    # add readnoise, assuming a poisson distribution with mean and standard
    # (mena readNoise in Electrons)

    rNoiseMatrix = np.ones(electronsPerPixel.shape) * \
        camerasensor.rNoise
    if readoutNoise:
        electronsPerPixel = electronsPerPixel + \
            np.random.poisson(rNoiseMatrix)

    # identify the oversaturated pixels and set them to the full well
    # capacity.
    electronsPerPixel[
        electronsPerPixel > camerasensor.saturationCap] = camerasensor.saturationCap

    # camerasensor.camGain
    # should output 2.31 and 2.31 if the function is being tested.
    # print electronsPerPixel.mean()
    # print electronsPerPixel.std()**2
    print camerasensor.saturationCap
    # round to integer values (bit)
    I_sca_bn = np.round(
        float(camerasensor.bits)/camerasensor.saturationCap*electronsPerPixel)

    # do the transformation from physical units to digital camera output

    return I_sca_bn

def processMoloExperiment(camerasensor,I_tot,I_bg, pixelsizeMoloPlane, magnification, wavelength, exposure_time, Airy_Disk_Radius, poissonNoise=True, readoutNoise=True):
    """

    performs an image capture of the intensity distribution I_dist and stores the result to a database, it requires a simulation ID from the results of a simulation.
    Should only accept a resultsID and a database connection.

    returns a dictionary with the following variables:

        'I_tot_bn':'array', # acquired digital intensity distribution in the focal spot by the camera
        'I_bg_tot_bn':'array', # acquired intensity distribution in the from the background simulation by the camera.
        'I_molo_cam':'real', # scattered intensity averaged over the Airy disk after image acquisition.
        'bkg_mean_cam': 'real', # mean background calculated by calc_Int_AiryDisk after image acquisition.
        'bkg_std_cam':'real', # std background calculated by calc_Int_AiryDisk after image acquisition.
        'I_signal_cam':'real', # scattered intensity in the focal point after camera aquisition.
        'SNR_cam':'real', # signal to noise ratio as calculated by calc_Int_AiryDisk after image acquisition.

    """

    I_tot_bn = SimulateImageCapture(
        camerasensor,I_tot, pixelsizeMoloPlane, magnification, wavelength, exposure_time, poissonNoise, readoutNoise)


    I_bg_tot_bn = SimulateImageCapture(
        camerasensor,I_bg, pixelsizeMoloPlane, magnification, wavelength, exposure_time, poissonNoise, readoutNoise)

    I_signal_cam = np.max(I_tot_bn)

    I_molo_cam, bkg_mean_cam, bkg_std_cam, SNR_cam = calc_IntAiryDisk(
        I_tot_bn,I_bg_tot_bn, camerasensor.pixelsize, Airy_Disk_Radius)

    return locals()

def BinnImage(image, pixelsize_in, pixelsize_out):
    """Interpolates an image with a given pixelsize into an image of another pixelsize"""

    x = np.arange(0, image.shape[0])*pixelsize_in
    y = np.arange(0, image.shape[1])*pixelsize_in

    # interpolation of the original image. (interpol2d) gives an overflow
    # error, since matrix is too large.
    f = interpolate.RectBivariateSpline(x, y, image)

    x = np.arange(0, image.shape[0]*pixelsize_in, pixelsize_out)
    y = np.arange(0, image.shape[1]*pixelsize_in, pixelsize_out)

    binnedImage = f(x, y)

    return binnedImage


if __name__ == '__main__':

    test = {
        'binning': 0,
        'flatIntensity': 0,
        'RealIntensityDistribution': 1
    }

    # test image binning

    if test['binning']:
        face = misc.face()

        face_binned = BinnImage(face[:, :, 0], 1.0, 20.)

        plt.imshow(face_binned)

        plt.show()
    # ------------------------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------
    # test flat intensity.

    if test['flatIntensity']:

        # roughly the intensity of the background at 900 um for a propagation
        # loss of 0.8 dB/cm.
        intensity = np.ones((300, 300))*1e-2

        camerasensor = CameraSensorParameters('SonyIMX250')

        camerasensor.rNoise = 2.31
        camerasensor.QE = 72*1e-2
        camerasensor.pixelsize = 3.45*1e-6  # in m
        camerasensor.camGain = 0.17
        camerasensor.saturationCap = 10141.
        magnification = 20

        cam_sim = CameraSensorSimulation(camerasensor)

        exposure_time = 30*1e-3  # exposure time in s

        acquiredImage = cam_sim.SimulateImageCapture(
            intensity, 0.1e-6, 20, 632.8e-9, exposure_time)

        plt.imshow(acquiredImage)

        print acquiredImage
        print acquiredImage.mean()
        print acquiredImage.std()

        plt.show()

    if test['RealIntensityDistribution']:
        # testintensity distribution acquired for 10000 SAv Molecules on a 100
        # micrometer screen with 100 nm pixelsize.

        intensity = np.load('Testintensity_distribution_10000_SAv.npy')

        camerasensor = CameraSensorParameters('SonyIMX250')

        camerasensor.rNoise = 2.31
        camerasensor.QE = 72*1e-2
        camerasensor.pixelsize = 3.45*1e-6  # in m
        camerasensor.camGain = 0.17
        camerasensor.saturationCap = 10141.
        magnification = 20

        cam_sim = CameraSensorSimulation(camerasensor)

        exposure_time = 3000*1e-3  # exposure time in s

        acquiredImage = cam_sim.SimulateImageCapture(
            intensity, 2e-6, 20, 632.8e-9, exposure_time)

        plt.imshow(acquiredImage)

        print acquiredImage
        print acquiredImage.mean()
        print acquiredImage.std()

        plt.show()
