# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pylab as plt
import scipy as sci
import pandas as pd
import os


class CameraSensorParameters(object):
	"""Struct that stores the parameters of the camera"""
	def __init__(self,model):

		self.model = model
		self.resolution = np.asarray([1,1])
		self.framerate = 0 # fps [s]
		self.pixelsize = 0 # m^2
		self.QE = 0 # quantum efficiency at 632.8 nm. is converted based on MT9P031 525nm (63 %) to 48 % at 632.8nm for all models, this is not valid . The quantum efficiency of the ORCA Flash is for such that at 633 nm it is 80 %.
		self.rNoise = 0 # Read Noise (e-)
		self.saturationCap = 0 # well saturation capacity in (e-)
		self.camGain = 0 # camera Gain in (e-)/ADU
		self.wellFillTime = 0 # saturationCap/(QE*pixelsize^2) number specifing the fill time of the well for photonflux of 1 photon /m^2
		self.height = 0
		self.width = 0
		self.area = 0
		self.bits = 0 # 4095 for a 12 bit camera. 66535 for a 16 bit camera.

def ReturnCameraSensorParameters(model='',returnALL=False):
	"""Function that stores the camera data for various models in a dict gathered from the csv file 'camerasensormodelsdatasheets.csv'"""

	camerasensors = {}
	path = os.path.dirname(__file__)
	camerasensordata = pd.read_csv(path + '/camerasensormodelsdatasheets.csv',delimiter=';')

	for i in camerasensordata.index:

		sensorname = camerasensordata.ix[i,'Sensor Name']

		camerasensors[sensorname] = CameraSensorParameters(sensorname)
		models = camerasensors[sensorname]
		models.resolution = camerasensordata.ix[i,'Resolution']
		models.framerate = camerasensordata.ix[i,'Max FPS']
		models.pixelsize = camerasensordata.ix[i,'Pixel Size (um)']*1e-6
		models.QE = camerasensordata.ix[i,'Quantum Efficiency (%)']*1e-2
		models.bits = camerasensordata.ix[i,'Bits']

		models.QE = models.QE * 0.48/0.63 # 525 to 632 nm conversion.


		models.rNoise = camerasensordata.ix[i,'Read Noise (e-)']
		models.saturationCap = camerasensordata.ix[i,'Saturation Capacity (e-)']
		models.camGain = camerasensordata.ix[i,'Gain']
		models.wellFillTime = models.saturationCap/(models.QE*models.pixelsize**2)
		models.height = int(models.resolution.split(' x ')[1])*models.pixelsize
		models.width = int(models.resolution.split(' x ')[0])*models.pixelsize
		models.area = models.height*models.width


	if returnALL:

		return camerasensors

	else:

		return camerasensors[str(model)]

def ReturnCameraSensorNames():
	"""returns a list of strings of all the cameratypes that are in the database."""

	camerasensornames = []
	path = os.path.dirname(__file__)
	camerasensordata = pd.read_csv(path +'/camerasensormodelsdatasheets.csv',delimiter=';')

	for i in camerasensordata.index:

		camerasensornames.append(camerasensordata.ix[i,'Sensor Name'])


	return camerasensornames







if __name__ == '__main__':


	print ReturnCameraSensorParameters(returnALL=True)

	print ReturnCameraSensorParameters('Sony IMX249').width
