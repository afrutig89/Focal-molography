import numpy as np
from mpmath import fresnels, fresnelc
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from waveguide.waveguide import SlabWaveguide
from materials.materials import IsotropicMaterial
from auxiliaries.pareto import paretoList


def dNdd_fCalculator(name = 'Tantalum Pentoxide', n_s =
                    IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521).n, n_f = 2.117, 
                    n_c = IsotropicMaterial(name='Aqueous Medium', n_0=1.33).n,
                    d_f = 146e-9, polarization = 'TE',inputPower = 1, wavelength = 632.8e-9,
                    attenuationConstant = .8 * np.log(10)/10. * 100, mode = 0):
    """function that calculates the change in refractive index per change in diameter.
    The input parameters are the same as for the SlabWaveguide class. However by default they are all
    set to the standard mologramm values"""
    
    dd_f = 1e-9
    d_f2 = d_f + dd_f

    #waveguide class at waveguide thickness
    waveguide1 = SlabWaveguide(name,n_s,n_f,n_c,d_f,polarization, 
                                 inputPower, wavelength,
                                attenuationConstant,mode)

    #waveguide class for a 1 nm thicker wavegiude
    waveguide2 = SlabWaveguide(name,n_s,n_f,n_c,d_f2,polarization, 
                             inputPower, wavelength,
                            attenuationConstant,mode)


    dN = waveguide2.N - waveguide1.N

    return dN/dd_f 



def PowerDephasing(dd_fdx_a,dNdd_f_a,wavlength_a,L_a,d_a):

    """calculates the reflecterd power in a mologramm with a coneshaped waveguide

    :param dd_fdx_a: change of the waveguide thickness over change in position
    :type dd_fdx_a: float, np.array or list
    :param dNdd_f_a: change of the effective index over change in waveguide thickness
    :type dd_fdx_a: float, np.array or list
    :param wavlength_a: wavlength
    :type wavlengt_a: float, np.array or list
    :param L_a: length
    :type dd_fdx_a: flh: float, np.array or list
    :param d_a: missmatch parameter
    :type dd_fdx_a: flh: float, np.array or list

    .. math:: P = \\frac{\\pi  \\left(\\left(C\\left(\\frac{d+2 L s}{\\sqrt{2 \\pi } \\sqrt{s}}\\right)-C\\left(\\frac{d}{\\sqrt{2 \\pi }
   \\sqrt{s}}\\right)\\right)^2+\\left(S\\left(\\frac{d}{\\sqrt{2 \\pi } \\sqrt{s}}\\right)-S\\left(\\frac{d+2 L s}{\\sqrt{2
   \\pi } \\sqrt{s}}\\right)\\right)^2\\right)}{2 s}

   where s is the dephasing parameter
   .. math:: s = \\frac{2 \\pi}{2\\lambda} \\frac{dN}{dt_f} \\frac{dt_f}{dx}
   d ist the missmatch parameter
   .. math:: d = \\beta_1 + \\beta_2 - \\frac{2\\pi}{\\Lambda}

   and S and C are the fresnel function fresnelS and fresnelC

    """
    


    #transfers all floats to a list because pareto cannot deal with floats
    if type(dd_fdx_a) == float: 
        dd_fdx_a = [dd_fdx_a]
    if type(dNdd_f_a) == float: 
        dNdd_f_a = [dNdd_f_a]
    if type(wavlength_a) == float: 
        wavlength_a = [wavlength_a]
    if type(L_a) == float: 
        L_a =[L_a]
    if type(d_a) == float: 
        d_a = [d_a]

    #pareto to allow for any kind of sweeping
    parameters = paretoList(dd_fdx_a,dNdd_f_a,wavlength_a,L_a,d_a)

    Pdiff=[]

    #sweepeing over everithing the loop is needed becasue fresnelc and fesnels cannot accept arrays
    for dadx,dNdd_f,wavelength,L,d in parameters:
         

        #caculate the dephasing parameter s
        s = 2 * np.pi/ (2*wavelength) *dNdd_f * dadx
       
        #calculate the diffracted power (derivation formula in mathematic analythical derivation YB000)
        Pdiff.append(float(\
            np.pi / (2. *s)\
               * ((-fresnelc( d / np.sqrt(2*np.pi*s) ) + fresnelc( (d + 2*L*s)/ np.sqrt(2*np.pi*s) ))**2\
               + (fresnels( d / np.sqrt(2*np.pi*s) ) - fresnels( (d + 2*L*s)/ np.sqrt(2*np.pi*s) ))**2\
               )))


    return np.asarray(Pdiff)


def dephasingLength(dd_fdx,dNdd_f,wavelength):
    """calculates the distance at whicht the phase shift is pi

    :param dd_fdx: change of the waveguide thickness over change in position
    :type dd_fdx: float, np.array or list
    :param dNdd_f: change of the effective index over change in waveguide thickness
    :type dd_fdx: float, np.array or list
    :param wavlength: wavlength
    :type wavlengt: float, np.array or list

    .. math:: \\sqrt{\\frac{\\lambda}{\\frac{dN}{dt_f} \\frac{dt_f}{dx}}}



    """
    return np.sqrt(wavelength/(dd_fdx*dNdd_f))


def maximalMologramSize(dd_fdx,dNdd_f,wavelength,d=0):

    """Calculates the mologram size at which the diffracted power is at the maximum
    including the dephasing

    :param dd_fdx: change of the waveguide thickness over change in position
    :type dd_fdx: float, np.array or list
    :param dNdd_f: change of the effective index over change in waveguide thickness
    :type dNdd_f: float
    :param wavlength: wavlength
    :type wavlengt: float
    :param d: missmatch parameter
    :type dd_fdx: float


    """


    L_max_array = dephasingLength(dd_fdx,dNdd_f,wavelength)
    print type(L_max_array)
    if type(L_max_array) == float or  type(L_max_array)==np.float64:
        L_max_array = np.array([L_max_array])
        dd_fdx = np.array([dd_fdx])
    else:
        L_max_array = L_max_array
    
    MaxL=[]
    i = 0
    for L_max in L_max_array:
        #if the dephasing length is so short then the input parameter are probebly worng
        if L_max < 20e-6:
            print 'unreasonrable large change in N leads to too short dephasing length'
            return

        L = np.linspace(1e-6,L_max,1000)

        
        P_dephasing = PowerDephasing(dd_fdx[i],dNdd_f,wavelength,L,d)
        MaxL.append(L[np.argmax(P_dephasing)])
        print i
        i = i+1

    return np.asarray(MaxL)


def maximalMologramSizeRatio(ratio,dd_fdx,dNdd_f,wavelength,d):

    """calculates the distance at whicht the ratio to no dphasing has a certain value
    
    :param ratio: ratio dephasing ove no dephasing
    :type ratio: float
    :param dd_fdx: change of the waveguide thickness over change in position
    :type dd_fdx: float, np.array or list
    :param dNdd_f: change of the effective index over change in waveguide thickness
    :param dNdd_f: float, np.array or list
    :param wavlength: wavlength
    :type wavlengt: float, np.array or list
    :param d: missmatch parameter
    :type dd_fdx: float, np.array or list
 
    """

    L_max = dephasingLength(dd_fdx,dNdd_f,wavelength)

    #if the dephasing length is so short then the input parameter are probebly worng
    if L_max < 20e-6:
        print 'unreasonrable large change in N leads to too short dephasing length'
        return

    L = np.linspace(1e-6,L_max,1000)

    P_noDephasing =  PowerNoDephasing(d,L)
    P_dephasing = PowerDephasing(dd_fdx,dNdd_f,wavelength,L,d)

    index = np.argmin(abs(P_dephasing / P_noDephasing - ratio))


    #check
    ratioCheck = PowerDephasing(dd_fdx,dNdd_f,wavelength,L[index],d)/PowerNoDephasing(d,L[index])
    control  =  ratioCheck - ratio
    
    print 'ratio: ', ratioCheck
    if control>0.05:
        print 'SOMETHING WENT WRONG: probebly this ratio cannot be achieved below the dephasing length'
        return

    return L[index]


def PowerNoDephasing(d,L):
    
    if d == 0:
        Pdiff = L**2
    else:
        Pdiff = np.sin(d*L/2)**2/d**2


    return Pdiff


if __name__ == '__main__':
    #initializing parameters
    wavelength = 632.8e-9
    d=0
    L= np.linspace(10e-6,500e-6)
    dadx = 10**-9 / 10**-3 #[m/m]
    


    dNdd_f = dNdd_fCalculator()
    Pdiff = PowerDephasing(dadx,dNdd_f,wavelength,L,d)
    Pdiff_noDephasing = PowerNoDephasing(d,L)

    plt.plot(L,Pdiff)
    plt.plot(L,Pdiff_noDephasing)
    plt.show()
