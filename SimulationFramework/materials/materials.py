"""
This module consist classes of different materials for particles or
layered media. The optical properties are calculated according to
the type of the material (metals, proteins, glass, ...).

.. todo:: review

:author:    Silvio Bischof
:created:   Mon Dec 5 16:23:15 2016
"""
from os import sys,path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))
from getData import get_range, get_data
import os

from SharedLibraries.Database.dataBaseOperations import saveDB, createDB, loadDB,getTableNames
import auxiliaries.constants as constants
import numpy as np
from numpy import pi, sqrt
import datetime
import logging


# short list of standard materials the refractive index database has too much unneccessary stuff in it
# this is not for the framework but rather for other modules.

materials = {
    'Au':'database/main/Au/Johnson.yml',
    'Ag':'database/main/Ag/Johnson.yml',
    'Ta2O5':'database/user-defined/Waveguide/Ta2O5 RF 30W.yml',
    'Water':'database/main/H2O/Daimon-20.0C.yml',
    'Air':'database/other/mixed gases/air/Ciddor.yml',
    'D263':'database/glass/schott/D263TECO.yml'
}

class Material(object):
    """Generic class to handle materials."""

    def __init__(self, name, refractiveIndex = ''):
        """Set material name."""
        self.name = name

        if self.name not in materials.keys():

            print('Materials in Database:')

            for i in materials.keys():

                print(i)

            raise 'This material is not in the database, please specify one of the above materials.'
        else:

            self.path = materials[self.name]

    def ReturnRefractiveIndex(self,wavelength):
        """
            :param wavelength: wavlength at which the refractive index is desired
            :return: returns the complex refractive index
        """

        refractive_index = get_data(self.path,wavelength*1e6)

        return refractive_index

    def ReturnEpsilon(self,wavelength):

        refractive_index = get_data(self.path,wavelength*1e6)

        eps_r = np.real(refractive_index)**2 - np.imag(refractive_index)**2
        eps_i = 2*np.real(refractive_index)*np.imag(refractive_index)
        
        return eps_r + 1j*eps_i



# refractiveIndexWater = 1.33
# refractiveIndexIncrement = 0.182 # dn/dc in water in ml/g

class Particle(object):
    """Use only in the simulation framework!!!!
    """

    def __init__(self, name, wavelength,radius='',refractiveIndex=''):

        self.name = name

        path = os.path.dirname(__file__)

        self.wavelength = wavelength

        # find database entry
        try:
            # if module is called from main file, path is there, if the particle class is used from outside.
            particle = loadDB(path + '/materials.db', 'Particles', condition = 'where name = \'{}\''.format(name))

        except:
            # if module is called from main file

            particle = loadDB('/materials/materials.db', 'Particles', condition = 'where name = \'{}\''.format(name))

        if radius == '':
            self.radius = particle['radius']
        else:
            self.radius = radius

        if refractiveIndex == '':
            file = particle['file']
            # if refractive index has not been calculated for the given wavelength
            self.refractiveIndex = get_data(path + "/" + file, self.wavelength*1e6)[0]
        else:
            self.refractiveIndex = refractiveIndex

        self.eps_r = self.refractiveIndex**2 # this is automatically the complex epsilon

        if particle['mass'] != '':
            self.mass = float(particle['mass'])

        self.material = particle.get('material',None)


class IsotropicMaterial(object):
    """Subclasses Material to describe isotropic materials. Frequency dispersion and thermic aware
    From Simulationframework, uses materials.db
    """
    def __init__(self, name='', wavelength = '', n_0=0):
        """Set name and refractive index


        """

        path = os.path.dirname(__file__)

        if n_0 == 0:
            # get table names, the names are stored in the materials database
            tables = getTableNames(path + '/materials.db')

            # initialize variable found
            found = False
            # go through tables and search material
            for table in tables:
                material = loadDB(path + '/materials.db', table, condition = 'where name = \'{}\''.format(name))
                if type(material) == dict:
                    # material found
                    found = True
                    break


            # if the material is found, get data, otherwise set as defined
            if found:
                file = material['file']
                if 'len' not in dir(wavelength):
                    # if not array given
                    self.wavelength = np.array([wavelength])

                # must be real (yet): todo!
                # get the refractive index from the materials datasheet.
                self.n = get_data(path +file, self.wavelength*1e6)[0].real

            else:
                self.n = n_0
        else:
            self.n = n_0


        self.name = name
        self.wavelength = wavelength
        self.eps_r = self.n**2
        self.epsilon = self.n**2 * constants.eps_0



class PlaneMaterial(IsotropicMaterial):
    """Plane material, from simulationframework use only there."""

    def __init__(self, name='', wavelength = '', n_0=0, thickness=''):

        IsotropicMaterial.__init__(self, name, wavelength,n_0=0)

        self.thickness = thickness


if __name__ == '__main__':

    gold = Material('Au')

    print(gold.ReturnRefractiveIndex(700e-9))
    print(gold.ReturnEpsilon(700e-9))

    ta2O5 = Material('Ta2O5')

    print(ta2O5.ReturnRefractiveIndex(532e-9))

    #gold = Particle('Gold Nanoparticle', 632.8e-9, 25e-9, file='database/metals/Au - Gold.yml')
