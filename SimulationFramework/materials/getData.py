"""
This file can get the data from the exported .yml files exported from
refractiveindex.info. It is based on work done by 'cinek810' found
on https://github.com/cinek810/refractiveindex.info. The functions
are slightly modified and extended (metals).

:author:        cinek810
:revised:       Silvio Bischof, 09-Dec 2016

.. note:: please check formulas
"""


from scipy.interpolate import interp1d
import numpy as np
from numpy import pi
import sys
sys.path.append('../')
import auxiliaries.constants as constants
import yaml
import numpy as np
import cmath



def get_data_N(yamlFile, wl):
    """
    This function is designed to return refractive index for specified wlda
    for file in "tabluated n " format

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="tabulated n"

    matwlda=[]
    matN=[]
    matK=[]
    #in this type of material read data line by line
    for line in materialData["data"].split('\n'):
        try:
            matwlda.append(float(line.split(' ')[0]))
            matN.append(float(line.split(' ')[1]))
        except:
            pass

    matwlda=np.array(matwlda)
    matN=np.array(matN)

    try:
        interN=interp1d(matwlda,matN)

        return [ x for x in interN(wl)]

    except:
        # constant
        return np.zeros(len(wl)) + matN


def get_range_N(yamlFile):
    """
    ..todo:: Description needed!

    :param yamlFile: .yml file exported from 'Link refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    """

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="tabulated n"
    #in this type of material read data line by line
    matwlda=[]
    for line in materialData["data"].split('\n'):
        try:
            matwlda.append(float(line.split(' ')[0]))
        except:
            pass

    return (min(matwlda),max(matwlda))


def get_data_NK(yamlFile,wl):
    """
    This function is designed to return refractive index for specified wlda
    for file in "tabluated nk " format

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]


    assert materialData["type"]=="tabulated nk"

    matwlda=[]
    matN=[]
    matK=[]
    #in this type of material read data line by line
    for line in materialData["data"].split('\n'):
        parsed = line.split(' ')
        if len(parsed) > 1:
            n = float(parsed[1])+1j*float(parsed[2])
            matwlda.append(float(parsed[0]));
            matN.append(float(parsed[1]))
            matK.append(float(parsed[2]))
        else:
            pass

    matwlda=np.array(matwlda)
    matN=np.array(matN)
    matK=np.array(matK)

    interN=interp1d(matwlda,matN)
    interK=interp1d(matwlda,matK)
    return [ x for x in [interN(wl)+1j*interK(wl)] ]

def get_range_NK(yamlFile):
    """
    ..todo:: Description needed!

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    """

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="tabulated nk"
    #in this type of material read data line by line
    matwlda=[]
    for line in materialData["data"].split('\n'):
        try:
            matwlda.append(float(line.split(' ')[0]))
        except:
            pass

    return (min(matwlda),max(matwlda))


def get_data_function1(yamlFile,wl):
    """
    this function is desined to get data from files in formula1 format

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 1"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] or max(wl)<=dataRange[1]:
        n = sellmeier_equation(wl, coeff)
    else:
        raise Exception("OutOfBands","No data for this material for this l")

    return n


def get_data_function2(yamlFile,wl):
    """
    this function is desined to get data from files in formula2 format

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 2"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] or max(wl)<=dataRange[1]:
        n = sellmeier_equation2(wl, coeff)

    else:
        raise Exception("OutOfBands","No data for this material for this l")

    return n

def get_data_function3(yamlFile,wl):
    """
    this function is desined to get data from files in formula3 format

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 3"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] and max(wl)<=dataRange[1]:
        n = cauchy_equation(wl, coeff)
    else:
        raise Exception("OutOfBands","No data for this material for wlda="+str(l))

    return n


def sellmeier_equation(wl, coefficients):
    eps_r = 0
    for i in reversed(range(1,np.size(coefficients),2)):
        eps_r = eps_r + ((coefficients[i]*wl**2)/(wl**2-coefficients[i+1]**2))

    eps_r = eps_r + coefficients[0] + 1

    n = []
    for eps_ri in eps_r:
        n.append(cmath.sqrt(eps_ri))

    return n

def sellmeier_equation2(wl, coefficients):
    eps_r = 0
    for i in reversed(range(1,np.size(coefficients),2)):
        eps_r = eps_r + ((coefficients[i]*wl**2)/(wl**2-coefficients[i+1]))

    eps_r = eps_r + coefficients[0] + 1

    n = []
    for eps_ri in eps_r:
        n.append(cmath.sqrt(eps_ri))

    return n

def cauchy_equation(wl, coefficients):
    eps_r = coefficients[0]
    for i in range(1,np.size(coefficients),2):
        eps_r = eps_r + coefficients[i]*wl**coefficients[i+1]

    n = []
    for eps_ri in eps_r:
        n.append(cmath.sqrt(eps_ri))

    return n


def get_data_function4(yamlFile,wl):
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 4"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.zeros(17)
    coeff2=map(float,materialData["coefficients"].split());

    for i in range(0,len(coeff2)):
        coeff[i]=coeff2[i]

    n = coeff[0]
    if min(wl)>=dataRange[0] and max(wl)<=dataRange[1]:
            n = n + coeff[1]*wl**coeff[2]/(wl**2-coeff[3]**coeff[4])
            n = n + coeff[5]*wl**coeff[6]/(wl**2-coeff[7]**coeff[8])
            n = n + coeff[9]*wl**coeff[10]
            n = n + coeff[11]*wl**coeff[12]
            n = n + coeff[13]*wl**coeff[14]
            n = n + coeff[15]*wl**coeff[16]
    else:
        raise Exception("OutOfBands","No data for this material("+yamlFile+" )for wlda="+str(wl))

    nres=[]
    for oneN in n:
        nres.append(cmath.sqrt(oneN))
    return nres


def get_data_function5(yamlFile,wl):
    """
    this function is desined to get data from files in formula3 format

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 5"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] and max(wl)<=dataRange[1]:
        n = cauchy_equation(wl, coeff)
    else:
        raise Exception("OutOfBands","No data for this material for wlda="+str(l))

    return n


def get_data_function6(yamlFile,wl):
    """
    this function is desined to get data from files in formula6
    format (gases)

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 6"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] and max(wl)<=dataRange[1]:
        n = 0
        for i in reversed(range(1,np.size(coeff),2)):
            n = n + (coeff[i]/(coeff[i+1]-wl**(-2)))

        n = n + coeff[0] + 1

    else:
        raise Exception("OutOfBands","No data for this material for wlda="+str(l))

    return n


def get_data_function7(yamlFile,wl):
    """
    this function is desined to get data from files in formula7
    format (Herzberger)

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 7"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] and max(wl)<=dataRange[1]:
        n = 0
        for i in range(3):
            n = n + coeff[i] * (1/(wl**2-0.028))**i \
                  + coeff[i+3] * wl ** ((i+1)*2)

    else:
        raise Exception("OutOfBands","No data for this material for wlda="+str(l))

    return n


def get_data_function8(yamlFile,wl):
    """
    this function is desined to get data from files in formula8
    format (Retro)

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 8"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] and max(wl)<=dataRange[1]:
        rightTerm = coeff[0] + coeff[1]*wl**2 / (wl**2-coeff[2]) + coeff[3] * wl**2
        eps_r = (2*rightTerm+1)/(1-rightTerm) + 0j  # such that it is complex
        n = np.sqrt(eps_r)

    else:
        raise Exception("OutOfBands","No data for this material for wlda="+str(l))

    return n


def get_data_function9(yamlFile,wl):
    """
    this function is desined to get data from files in formula9
    format (Exotic)

    :param yamlFile: .yml file exported from 'a refractiveindex.info <refractiveindex.info>'_
    :type yamlFile: string
    :param wl: wavelength
    :type wl: float ndarray
    """
    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 9"

    dataRange=np.array(map(float,materialData["range"].split()));
    coeff=np.array(map(float,materialData["coefficients"].split()));

    if min(wl)>=dataRange[0] and max(wl)<=dataRange[1]:
        eps_r = coeff[0] + coeff[1]/(wl**2-coeff[2]) \
                + coeff[3]*(wl-coeff[4])/((wl-coeff[4])**2+coeff[5]) + 0j
        n = np.sqrt(eps_r)

    else:
        raise Exception("OutOfBands","No data for this material for wlda="+str(l))

    return n
def get_data_function10(yamlFile, wl):
    wl = wl*1e-6   # wavelengths given in um

    if type(wl) == float:
        wl = np.array([wl])

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 10"

    dataRange=np.array(map(float,materialData["range"].split()));
    # coeff=np.array(map(float,materialData["coefficients"].split()));
    omega_p = float(materialData["omega_p"]) * constants.q_e/constants.h_bar
    f = np.array(map(float,materialData["f"].split()))
    Gamma = np.array(map(float,materialData["Gamma"].split())) * constants.q_e/constants.h_bar
    omega = np.array(map(float,materialData["omega"].split())) * constants.q_e/constants.h_bar
    order = len(omega)

    omega_light = 2*pi*constants.c  / wl

    epsilon_D = np.zeros(len(omega_light), dtype=complex)
    for i, w in enumerate(omega_light):
        epsilon_D[i] = 1 - (f[0] * omega_p ** 2 / \
                       (w ** 2 + 1j * (Gamma[0]) * w))
    epsilon_L = np.zeros(len(omega_light), dtype=complex)

    for i, w in enumerate(omega_light):
        for k in xrange(1, order):
            epsilon_L[i] += (f[k] * omega_p ** 2) / \
                            (omega[k] ** 2 - w ** 2 - 1j * Gamma[k] * w)

    eps_r = epsilon_D + epsilon_L


    n = []
    for eps_ri in eps_r:
        n.append(cmath.sqrt(eps_ri))

    return n

def get_data_function11(yamlFile, wl):
    wl = wl*1e-6   # wavelengths given in um

    if type(wl) == float:
        wl = np.array([wl])

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 11"

    radius = materialData["radius"]
    mass = materialData["mass"]

    dn_dc = 0.182 # dn/dc in water in m^3/g
    n_solv = 1.33

    rho_protein = 1.410 + 0.145*np.exp(-mass*1e-3/13)
    n = n_solv + dn_dc*rho_protein

    n = np.zeros(len(wl), dtype=complex) + n

    return n


def get_data_function12(yamlFile, wl):
    # constant

    wl = wl*1e-6   # wavelengths given in um

    if type(wl) == float:
        wl = np.array([wl])

    yamlStream=open(yamlFile,'r')
    allData=yaml.load(yamlStream);

    materialData=allData["DATA"][0]

    assert materialData["type"]=="formula 12"

    n_p = materialData["n_p"]
    n = np.zeros(len(wl), dtype=complex) + n_p

    return n

def error(BaseException):
    pass


def get_data(yamlFile, wl):
    """
    this is general function to check data type, and run appropriate actions
    """

    yamlStream = open(yamlFile,'r')
    allData = yaml.load(yamlStream)
    materialData = allData["DATA"][0]

    if type(wl) == float: wl = np.array([wl])

    if materialData["type"] == "tabulated nk":
        return get_data_NK(yamlFile,wl)

    elif materialData["type"] == "tabulated n":
        return get_data_N(yamlFile,wl)

    # Sellmeier equation
    elif materialData["type"] == "formula 1":
        return get_data_function1(yamlFile,wl)

    # Sellmeier equation - type 2
    elif materialData["type"] == "formula 2":
        return get_data_function2(yamlFile,wl)

    # Polynomial (Cauchy's)
    elif materialData["type"] == "formula 3":
        return get_data_function3(yamlFile,wl)

    # RefractiveIndex.INFO
    elif materialData["type"] == "formula 4":
        return get_data_function4(yamlFile,wl)

    # Cauchy (same as polynomial)
    elif materialData["type"] == "formula 5":
        return get_data_function5(yamlFile,wl)

    # gasses
    elif materialData["type"] == "formula 6":
        return get_data_function6(yamlFile,wl)

    # Herzberger
    elif materialData["type"] == "formula 7":
        return get_data_function7(yamlFile,wl)

    # retro
    elif materialData["type"] == "formula 8":
        return get_data_function8(yamlFile,wl)

    # exotic
    elif materialData["type"] == "formula 9":
        return get_data_function9(yamlFile,wl)

    # Lorentz-Drude (metals)
    # Note: not used in refractiveindex.info
    elif materialData["type"] == "formula 10":
        return get_data_function10(yamlFile, wl)

    elif materialData["type"] == "formula 11":
        return get_data_function11(yamlFile, wl)

    elif materialData["type"] == "formula 12":
        return get_data_function12(yamlFile, wl)

    else:
        return np.zeros((1,0))

def get_range(yamlFile):
    yamlStream = open(yamlFile,'r')
    allData = yaml.load(yamlStream)
    materialData = allData["DATA"][0]

    if materialData["type"] == "tabulated nk":
        return get_range_NK(yamlFile)
    elif materialData["type"] == "tabulated n":
        return get_range_N(yamlFile)
    else:
        try:
            return np.array(map(float,materialData["range"].split()))
        except:
            return np.array([0.1,2]) # arbitrary range


if __name__ == "__main__":
    rangeWavelength = get_range("database/main/Ta2O5/Bright-amorphous.yml")
    wavelength = np.linspace(rangeWavelength[0],rangeWavelength[1],200)
    print("Direct tests:")
    print("MyData="+str(get_data("database/main/Ta2O5/Bright-amorphous.yml",wavelength))+" ,test ~  0.23 + 6.41j")
