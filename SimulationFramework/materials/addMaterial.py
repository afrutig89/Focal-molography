"""
This module can be used to add a material to the database, either by
defining or loading without using the GUI.

:Author:	Silvio Bischof
:Created:	09-Jan 2017
"""

import sys
sys.path.append('../')
from variables import particleDB
from auxiliaries.writeLogFile import createDB, saveDB


def saveFileYML(formula, material, name, wavelengthRange='',
                coefficiency='', radius='', mass=''):
    """
    """
    if formula == 'Formula 11':
        # write file
        file = 'database/user-defined/' + material + '/' \
                         + name + '.yml'

        if not os.path.exists(os.path.dirname(file)):
            # create if not existing
            try:
                os.makedirs(os.path.dirname(file))
            except:
                print "wrong inputs"
                return False
        else:
            print "File already exists"
            return False

        with open(file, "w") as f:
            comment = "# This file is part of the focal molography software"
            comment += " and user-defined.\n"
            f.write("\nDATA:")
            f.write("\n  - type: formula " + formula)
            f.write("\n    radius: " + radius)
            f.write("\n    mass: " + mass)

    elif str(self.formula.currentText()) == 'Formula 10':
        # Lorentz-Drude: more coefficients required, not implemented
        pass

    else:
        # write file
        file = 'database/user-defined/' + material + '/'

        if not os.path.exists(os.path.dirname(file)):
            # create if not existing
            try:
                os.makedirs(os.path.dirname(file))
            except:
                print "wrong inputs"
                return False
        else:
            print "error"
            return False

        with open(file, "w") as f:
            f.write("DATA:")
            f.write("  - type: formula " + formula)
            f.write("    range: " + wavelengthRange)
            f.write("    coefficients: " + coefficients)


def saveDatabase(name, material, materialType, radius='', mass='', file=''):
    """
    """

    # save in database
    if materialType == 'Particles':

        variables = {
            'name' : str(self.name.text()),
            'material' : str(self.material.text()),
            'radius' : particleRadius,
            'mass' : particleMass,
            'file' : file,
        }

    # todo: other media like waveguide, cover, ... (which variables are required?)

    # save to database
    createDB('materials/materials.db', materialType, particleDB())
    # create db if not exist
    saveDB('materials/materials.db', materialType, variables)


if __name__ == '__main__':
    # use to add a material manually
