# -*- coding: utf-8 -*-
"""
In this file, different parameters are analyzed analytically and semi-
analytically. Its purpose is to find an optimal molographic mass for 
a label-free mologram as well as the influences of different noise 
sources.

@author: Silvio

.. todo::
- validate with simulations / measurements
"""


from numpy import pi, sqrt, inf, cos, sin, exp, conj
import numpy as np
from waveguide.waveguide import Waveguide
from interfaces.configuration import Media, IsotropicMaterial
import auxiliaries.constants as constants
import dipole.dipole as dipole
from auxiliaries.LorentzDrude import LD, particleRadius, refractiveIndex
from waveguidebackground.background import I_sc, returnParticleNumber

# laser
wavelength = 632.8e-9
k_0 = 2*pi/wavelength
inputPower = 2e-3
beamWaist = 2e-3

# mologram
f = 900e-6
D = 400e-6

# materials
# protein: SAv
M_SAv = 52.8e3 # kDa
radius_p = particleRadius(M_SAv)
eps_p = refractiveIndex(M_SAv)**2

# background protein: albumin
M_albumin = 66.5e3 # Da
radius_b = particleRadius(M_albumin)
eps_b = refractiveIndex(M_albumin)**2
A_0 = 1e-3  # concentration antibody (albumin)
R_0 = 2e10*1e6  # active site density
K_D = 1e-3  # antibody binding affinity
N_b = returnParticleNumber(K_D, A_0, R_0, D)

# media configuration
# cover
n_c = 1.33
aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=n_c)
# substrate
n_s = 1.521
glassSubstrate = IsotropicMaterial(name='Glass Substrate', n_0=n_s)
# waveguide
n_f = 2.117
d_f = 145e-9
propagationLoss = 0.8
couplingEfficiency = 0.3

waveguide = Waveguide(name='Waveguide', n_s=glassSubstrate.n, n_f=n_f, 
                      n_c=aqueousMedium.n, d_f=d_f, polarization='TE',
                        mode = 0, inputPower=inputPower*couplingEfficiency, 
                        attenuationConstant=propagationLoss)


# initialization
configuration = Media([-inf, -d_f, 0, inf],[glassSubstrate, waveguide, aqueousMedium])

upperMedium = configuration.returnMedium(inf)
lowerMedium = configuration.returnMedium(-inf)

n_1 = upperMedium.n
n_n = lowerMedium.n
k_1 = n_1*k_0
k_n = n_n*k_0
eps_0 = constants.eps_0
eps_1 = upperMedium.eps_r
eps_n = lowerMedium.eps_r
mu_0 = constants.mu_0
mu_n = 1
mu_1 = 1
Z_w = sqrt(mu_0/(eps_0*eps_n))
E_0 = waveguide.E_y(wavelength, np.array([-D/2.]), np.array([0]), np.array([-d_f/2.]))[0].real

########################################################

# polarizability
alpha_p = dipole.dipolePolarizability(radius_p, eps_p, eps_1)

scatteringCrossSection_p = dipole.scatteringCrossSection(k_0*n_c, radius_p, eps_p, eps_1)

absorptionCrossSection_p = dipole.absorptionCrossSection(k_0*n_c, radius_p, eps_p, eps_1)

extinctionCrossSection_p = dipole.extinctionCrossSection(k_0*n_c, radius_p, eps_p, eps_1)
extinctionCrossSection_b = dipole.extinctionCrossSection(k_0*n_c, radius_b, eps_b, eps_1)

# exciting field
z_p = 20e-9
E_p = waveguide.E_y(wavelength,np.array([0]),np.array([0]),np.array([z_p]))

# dipole moment
p_p = dipole.staticDipoleMoment(radius_p, eps_p, eps_1, E_p)
p_b = dipole.staticDipoleMoment(radius_b, eps_b, eps_1, E_p)

# dipole radiation
P_0p = dipole.dipoleRadiation(p_p, wavelength, n=n_1)
P_0b = dipole.dipoleRadiation(p_b, wavelength, n=n_1)

# minimum dipole intensity
theta_max = np.arctan((D/2.)/f)
theta = theta_max
phi = pi/2.
r = f/cos(theta_max)
I_0p = 3/(8*pi*r**2) * n_n/n_1 * P_0p * (sin(phi)**2 * cos(theta)**2 + cos(phi)**2)
I_0b = 3/(8*pi*r**2) * n_n/n_1 * P_0b * (sin(phi)**2 * cos(theta)**2 + cos(phi)**2)

# electric field contributions
f_corr = 2/pi # correction factor as dipoles are on the ridges rather than on the mololines

N_bias_eff = lambda N_bias: (np.expm1(-2*extinctionCrossSection_p/(pi*D*z_p)*N_bias)/np.expm1(-2*extinctionCrossSection_p/(pi*D*z_p)))
N_protein_eff = lambda N_protein: (np.expm1(-2*extinctionCrossSection_p/(pi*D*z_p)*N_protein)/np.expm1(-2*extinctionCrossSection_p/(pi*D*z_p)))
N_albumin_eff = lambda N_albumin:(np.expm1(-2*extinctionCrossSection_b/(pi*D*z_p)*N_albumin)/np.expm1(-2*extinctionCrossSection_b/(pi*D*z_p)))

E_bias = lambda N_bias: f_corr*k_1**2*N_bias_eff(N_bias)*p_p/(4*pi*eps_0*eps_1*r) * exp(1j*k_n*r)
E_protein = lambda N_protein: f_corr*k_1**2*N_protein_eff(N_protein)*p_p/(4*pi*eps_0*eps_1*r) * exp(1j*k_n*r)
E_albumin = lambda N_albumin: f_corr*k_1**2*N_albumin_eff(N_albumin)*p_b/(4*pi*eps_0*eps_1*r) * exp(1j*k_n*r)

# noise sources
def E_bias_noise(N_bias, laserFluctuations): return laserFluctuations/2.*E_bias(N_bias) # laser shot noise
def E_protein_noise(N_protein, laserFluctuations): return laserFluctuations/2.*E_protein(N_protein)
def E_msn(N_bias, N_protein, background=0):
    if background == 0:
        if type(N_protein) == int:
            if N_protein == 0:
                return E_bias(sqrt(N_bias))
            else:
                return E_protein(sqrt(N_protein))
        else:   # fixed bias
            return E_protein(sqrt(N_protein))
    else:
        if type(N_protein) == int:
            if N_protein == 0:
                return E_albumin(sqrt(N_b))
            else:
                return E_protein(sqrt(N_protein)) + E_albumin(sqrt(N_b))
        else:   # fixed bias
            return E_protein(sqrt(N_protein)) + E_albumin(sqrt(N_b))

# intensities
def I_noise(N_bias, N_protein, laserFluctuations):return 1/(2*Z_w)*((E_bias(N_bias)+E_protein(N_protein))* \
                                    conj(E_bias_noise(N_bias, laserFluctuations)+E_protein_noise(N_protein, laserFluctuations)+E_msn(N_bias, N_protein)) + \
                                    conj(E_bias(N_bias)+E_protein(N_protein))* \
                                    (E_bias_noise(N_bias, laserFluctuations)+\
                                    E_protein_noise(N_protein, laserFluctuations)+E_msn(N_bias, N_protein))).real
def I_noise_nomsn(N_bias, N_protein, laserFluctuations): return 1/(2*Z_w)*((E_bias(N_bias)+E_protein(N_protein))* \
                                    conj(E_bias_noise(N_bias, laserFluctuations)+E_protein_noise(N_protein, laserFluctuations)) + \
                                    conj(E_bias(N_bias)+E_protein(N_protein))* \
                                    (E_bias_noise(N_bias, laserFluctuations)+E_protein_noise(N_protein, laserFluctuations))).real
def I_bias(N_bias): return 1/(2*Z_w) * (E_bias(N_bias) * conj(E_bias(N_bias))).real
def I_enhanced(N_bias, N_protein): return 1/Z_w * (E_bias(N_bias).real*E_protein(N_protein).real + \
        E_bias(N_bias).imag*E_protein(N_protein).imag) # factor 2 cancels out
def I_protein(N_protein): return 1/(2*Z_w) * (E_protein(N_protein)*conj(E_protein(N_protein))).real
def I_tot(N_bias, N_protein, laserFluctuations): return 1/(2*Z_w)*((E_bias(N_bias)+E_protein(N_protein)+E_bias_noise(N_bias, laserFluctuations)+E_protein_noise(N_protein, laserFluctuations)+E_msn(N_bias,N_protein))*\
                                     conj(E_bias(N_bias)+E_protein(N_protein)+E_bias_noise(N_bias, laserFluctuations)+E_protein_noise(N_protein, laserFluctuations)+E_msn(N_bias,N_protein))).real
def I_tot_nomsn(N_bias, N_protein, laserFluctuations): return 1/(2*Z_w)*((E_bias(N_bias)+E_protein(N_protein)+E_bias_noise(N_bias, laserFluctuations)+E_protein_noise(N_protein, laserFluctuations))*\
                                     conj(E_bias(N_bias)+E_protein(N_protein)+E_bias_noise(N_bias, laserFluctuations)+E_protein_noise(N_protein, laserFluctuations))).real
def I_sn(N_bias, N_protein, laserFluctuations): return I_tot_nomsn(sqrt(N_bias),sqrt(N_protein), laserFluctuations) # shot noise of the detector
I_bwg = 1/(2*Z_w) * I_sc(E_0,propagationLoss,f,D)[0]
def I_background(N_bias, N_protein, laserFluctuations): return (I_noise(N_bias, N_protein, laserFluctuations) + I_bwg)
def I_NCB(N_bias, N_protein, p_bias=1, p_protein=1): return 1/(2*Z_w)*(2*(1-p_bias)*N_bias_eff(N_bias)*E_bias(1)*conj(E_bias(1))+2*(1-p_protein)*N_protein_eff(N_protein)*E_protein(1)*conj(E_protein(1)))
    
I_NCB2 = 1/(2*Z_w)*I_0b*N_b

# SNR
def SNR(N_bias, N_protein, laserFluctuations): return (I_tot(N_bias, N_protein, laserFluctuations)/(I_background(N_bias, N_protein, laserFluctuations)+I_sn(N_bias, N_protein, laserFluctuations))).real
def SNR_enhancement(N_bias, N_protein, laserFluctuations): return ((I_enhanced(N_bias, N_protein)+I_protein(N_protein))/(I_background(N_bias, N_protein, laserFluctuations)+I_sn(N_bias, N_protein, laserFluctuations))).real



# Airy disk radius in x-direction / y-direction

# Depth of field (Airy disk radius in z-direction)

# Axial resolution

# Limit of detection

###################################
####    corresponding plots    ####
###################################

def formatSurfaceMassModulation(x, labeling=0):
    'returns surface mass modulation in pg/mm2'
    if labeling==1: return '[pg/mm2]'
    else: return x/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6

def formatMass(x, labeling=0):
    'returns mass'
    if labeling==1: return '[fg]'
    else: return x/constants.N_A*M_SAv*1e15

def formatConcentration(x, labeling=0):
    if labeling==1: return '[mol]'
    else: return x/constants.N_A
    
def formatScatterers(x, labeling=0):
    if labeling==1: return ''
    else: return x


import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter  
  
formatter = FuncFormatter(formatMass) # change if other units wished


print("\n---- Plots ----")

# plot settings
font = {'family' : 'Arial',
	'weight' : 'normal',
	'size'   : 12
}
params = {'legend.fontsize':   8,
	  'xtick.labelsize' : 10,
	  'ytick.labelsize' : 10
			}
plt.rcParams.update(params)
plt.rc('font', **font)

# determine what should be plotted
plots = {"I_enhanced":0,
         "GNP_required_enhanced":1, 
         "sensitivity":0
         }

colormap = ['black','red','green','blue','cyan','magenta']
showAll = False

# background / noise intensities
array_length = 1000 # array length for bias
laserFluctuations = 1e-3

if showAll:
    for key in plots:
        plots[key] = 1
        
plotNr = 0

if plots["I_enhanced"]:
    plotNr += 1
    print("Plot " + str(plotNr) + ": Total intensity due to bias")
    colormap_copy = colormap[::-1]
    m_bias_array = np.array([100000,60000,30000,0])
    N_bias_array = m_bias_array*constants.N_A/M_SAv/1e15

    N_proteins_array = np.logspace(3,7.15,array_length)
    
    fig = plt.figure(figsize=(17/2.54,8.5/2.54))
    for N_bias in N_bias_array:
        colorPlot = colormap_copy.pop()
        plt.semilogx(formatter(N_proteins_array), I_tot(N_bias,N_proteins_array,0)-I_bias(N_bias), color=colorPlot, label="bias: " + str(('%.0e') % N_bias))
        plt.semilogx(formatter(N_proteins_array), 10*I_background(N_bias,N_proteins_array,0), color=colorPlot, linestyle='dashed', label="bias: " + str(('%.0e') % N_bias))
    plt.xlabel("Bias " + formatter(N_proteins_array,1))
    plt.ylabel(r"Intensity in $W/m^2$")
    plt.ylim([0,500*I_bwg])
    plt.xlim([0.1,1e3])
#    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.show()
    fig.savefig("analysis/label-free/I_enhanced.pdf")


if plots["GNP_required_enhanced"]:
    plotNr += 1
    print("\nPlot " + str(plotNr) + ": protein particles required if only enhanced intensity is considered")
    c_bias_array = np.logspace(0,3,array_length)
    N_bias_array = c_bias_array*constants.N_A/M_SAv*((pi*(D/2.)**2))/1e6
    
    N_protein_array = np.round(np.linspace(0,5,200)*constants.N_A/M_SAv*((pi*(D/2.)**2))/1e6)
    
    fluctuations_array = np.linspace(1e-3,0,4)
    colormap_copy = colormap[::-1]
    
    N_offset = float(inf)
    fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
    for laserFluctuations in fluctuations_array:
        print('laser fluctuations: ' + str(laserFluctuations))
        N_protein_min = np.max(N_protein_array)+1
        N_protein_needed = np.zeros(array_length)
        for i in range(array_length):
            N_bias = N_bias_array[i]
            for N_protein in N_protein_array:
                if SNR_enhancement(N_bias, N_protein, laserFluctuations) > 10.:
                    N_protein_needed[i] = N_protein
                    if N_protein < N_protein_min:
                        N_protein_min = N_protein
                        N_offset = N_bias_array[i]
                    break
                laserFluctuationsPercent = laserFluctuations*100
        plt.semilogx(formatter(N_bias_array)*1e-3, formatter(N_protein_needed), color=colormap_copy.pop(), label="Fluctuations: " + str(('%.3f') % laserFluctuationsPercent) + "%")
        print(formatter(N_protein_min),formatter(N_offset))
    plt.xlabel("Bias [pg]")
    plt.xlim(0.1,1e2)
    plt.ylabel("SAv required " + formatter(N_protein_needed,1))
#    plt.xlim([1,1000])
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.show()
    fig.savefig("analysis/label-free/GNP_required_enhanced0.pdf")


if plots["sensitivity"]:
    plotNr += 1
    print("\nPlot " + str(plotNr) + ": Sensitivity")
    laserFluctuations = 1e-3
    colormap_copy = colormap[::-1]
    c_bias_array = np.array([1e3,6e2,3e2,0])
    N_bias_array = c_bias_array*constants.N_A/M_SAv*((pi*(D/2.)**2))/1e6
    
    N_proteins_array = np.logspace(3,7.15,array_length)
    c_SAv_array = N_proteins_array/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6
    
    fig = plt.figure(figsize=(17/2.54,8.5/2.54))
    for N_bias in N_bias_array:
        colorPlot = colormap_copy.pop()
        sensitivity = np.hstack(([0],np.diff(I_tot(N_bias,N_proteins_array,0)-I_bias(N_bias))))
        plt.semilogx(c_SAv_array, sensitivity, color=colorPlot, label="bias: " + str(('%.0e') % N_bias))
    plt.xlabel("Bias in amount of scatterers")
    plt.ylabel(r"Intensity in $W/m^2$")

#    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.show()

    fig.savefig("analysis/label-free/sensitivity.pdf")