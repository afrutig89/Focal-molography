  # -*- coding: utf-8 -*-
"""
This module helps to understand the different parameters used for the 
calculations and the simulations. 

@author:    Silvio
@Created:   Tue Apr 26 16:07:58 2016

.. todo:: integrate in GUI with variable parameters
"""
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import numpy as np
import auxiliaries.constants

# dipole field calculations
from dipole.dipoleField import *
from dipole.calculationMethods import *
# polarization and dipole moments
import dipole.dipole as dipole
# dipole plots
from dipole.plots import *
# configuration setup, Fresnel formulae, Snells law
from interfaces.configuration import *
# # limit of detection calculations
# from auxiliaries.limitations import *
# coherent pattern
from mologram.generatePattern import *
# focal molography functions
from waveguide.waveguide import SlabWaveguide
from mologram.mologram import Mologram
from materials.materials import *
from auxiliaries.functions import *
from SharedLibraries.Database.dataBaseOperations import *

# time and date
import datetime

# plot modules
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

    
# unity basis vectors
e_x = np.array([[1],[0],[0]])      # unity vector to the x-direction
e_y = np.array([[0],[1],[0]])      # unity vector to the x-direction
e_z = np.array([[0],[0],[1]])      # unity vector to the x-direction

# laser
wavelength = 635e-9
k_0 = 2*pi/wavelength

# materials
d_f = 145e-9

glassSubstrate = IsotropicMaterial(name='Glass Substrate', n_0=1.521)
aqueousMedium = IsotropicMaterial(name='Aqueous Medium', n_0=1.33)
waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=glassSubstrate.n, n_f=2.117, 
                      n_c=aqueousMedium.n, d_f=d_f, polarization='TE', 
                        inputPower=1, wavelength = wavelength, 
                        attenuationConstant=0.8)



########################################################
##  three media, layer with thickness d=145nm ##
########################################################

theta_1 = np.linspace(-30,30,100) # in degrees
theta = theta_1/180.*pi
phi = np.linspace(0,2*pi,100)

theta, phi = np.meshgrid(theta,phi)
d_f = d_f

configuration = Media([inf, 0, -d_f, -inf],[aqueousMedium, waveguide, glassSubstrate])
n = np.array([medium.n for medium in configuration.media])

upperMedium = configuration.returnMedium(inf)
lowerMedium = configuration.returnMedium(-inf)

n_1, n_f, n_n = n
eps_0 = constants.eps_0
eps_1 = upperMedium.eps_r
eps_n = lowerMedium.eps_r
eps_l = 1.587**2 # protein
mu_0 = constants.mu_0
mu_n = 1
mu_1 = 1

p = dipole.staticDipoleMoment(2.5e-9,eps_l,eps_1,1)

f = 900e-6 # focal point distance
r = f/cos(theta)


s_z = sqrt((n_1/n_n)**2-sin(theta)**2)  # definition of Fundamentals of Nano-Optics Ch. 10
k_n = k_0 * n_n                      # wave vector of the medium in the lower half-space
    
# fresnel coefficients
r_p2, r_s2 = reflectionCoefficientSingleLayer(d_f, k_0, theta, n)
t_p2, t_s2 = transmissionCoefficientSingleLayer(d_f, k_0, theta, n)

t_p2_abs = t_p2 * conj(t_p2)
t_s2_abs = t_s2 * conj(t_s2)

Phi_n_1 = n_n/n_1 * cos(theta) / s_z * t_p2 * exp(1j*k_n * d_f*cos(theta))
Phi_n_2 = n_n/n_1 * t_p2 * exp(1j*k_n * d_f*cos(theta)) # note: sign corrected
Phi_n_3 = cos(theta)/s_z * t_s2 * exp(1j*k_n * d_f*cos(theta))

term_potentials_theta = sin(phi)*cos(theta)*Phi_n_2
term_potentials_phi = cos(phi)*Phi_n_3

term_potentials_x = cos(theta)*cos(phi)*term_potentials_theta - sin(phi)*term_potentials_phi
term_potentials_y = cos(theta)*sin(phi)*term_potentials_theta + cos(phi)*term_potentials_phi
term_potentials_z = sin(theta)*term_potentials_theta
    
additionalPhase_x = 1e9*arctan(term_potentials_x.imag/term_potentials_x.real)/k_0
additionalPhase_y = 1e9*arctan(term_potentials_y.imag/term_potentials_y.real)/k_0
additionalPhase_z = 1e9*arctan(term_potentials_z.imag/term_potentials_z.real)/k_0

Phi_n_1_abs = Phi_n_1*conj(Phi_n_1)
Phi_n_2_abs = Phi_n_2*conj(Phi_n_2)
Phi_n_3_abs = Phi_n_3*conj(Phi_n_3)

Phi_1_1 = 1 + r_p2
Phi_1_2 = 1 - r_p2
Phi_1_3 = 1 + r_s2

Phi_1_1_abs = Phi_1_1*conj(Phi_1_1)
Phi_1_2_abs = Phi_1_2*conj(Phi_1_2)
Phi_1_3_abs = Phi_1_3*conj(Phi_1_3)

I = 1/r**2 * cos(theta)**2 * \
    (sin(phi)**2*(t_p2*conj(t_p2)) + cos(phi)**2 * \
    aqueousMedium.n/(aqueousMedium.n-glassSubstrate.n*sin(theta)**2)*(t_s2*conj(t_s2)))
I_rel = I/np.max(I)

I_abs = 1/2. * sqrt(eps_0*eps_n/(mu_0*mu_n)) * pi**2*mu_1**2/(eps_0**2*wavelength**4*r**2) * p**2 * \
        (sin(phi)**2*(t_p2*conj(t_p2)) + cos(phi)**2 * \
            aqueousMedium.n/(aqueousMedium.n-glassSubstrate.n*sin(theta)**2)*(t_s2*conj(t_s2)))

#################################
#####        Plots        #######
#################################

x = r*sin(theta)*cos(phi)
y = r*sin(theta)*sin(phi)

# propagation loss
I_rel = I_rel*exp(-0.8*log(10)/10.*(x-np.min(x))*1e2)

save = False
axes = ["spherical","cartesian"]
dimension = 3
setup = axes[1]
showAll = False

colorbar = False
labels = False
elevationAngle = None   # elevation angle in the z plane (None or angle)
azimuthalAngle = None   # azimuthal angle in the x,y plane (None or angle)
# 3rd parameter 1 if plot is desired
params = [[t_p2,r"transmission coefficient $t^(p)$",0,"tp"],
           [t_s2,r"transmission coefficient $t^(s)$",0,"ts"],
           [t_p2_abs,r"transmission coefficient $|t^(p)|^2$",0,"tp_abs"],
           [t_s2_abs,r"transmission coefficient $|t^(s)|^2$",0,"ts_abs"],
           [r_p2,r"reflection coefficient $r^(p)$",0,"rp"],
           [r_s2,r"reflection coefficient $r^(s)$",0,"rs"],
           [Phi_1_1,r"$\Phi_1^{(1)}$",0,"potential_11"],
           [Phi_1_2,r"$\Phi_1^{(2)}$",0,"potential_12"],
           [Phi_1_3,r"$\Phi_1^{(3)}$",0,"potential_13"],
           [Phi_1_1_abs,r"$| \Phi_1^{(1)} |^2$",0,"potential_abs_11"],
           [Phi_1_2_abs,r"$| \Phi_1^{(2)} |^2$",0,"potential_abs_12"],
           [Phi_1_3_abs,r"$| \Phi_1^{(3)} |^2$",0,"potential_abs_13"],
           [Phi_n_1,r"$\Phi_n^{(1)}$",1,"potential_n1"],
           [Phi_n_2,r"$\Phi_n^{(2)}$",1,"potential_n2"],
           [Phi_n_3,r"$\Phi_n^{(3)}$",1,"potential_n3"],
           [Phi_n_1_abs,r"$| \Phi_n^{(1)} |^2$",1,"potential_abs_n1"],
           [Phi_n_2_abs,r"$| \Phi_n^{(2)} |^2$",1,"potential_abs_n2"],
           [Phi_n_3_abs,r"$| \Phi_n^{(3)} |^2$",1,"potential_abs_n3"],
           [I_rel,r'normalized intensity $I_0(\theta)$',0,"normalizedIntensity"],
           [I_abs,r'absolute intensity $I_0(\theta)$',0,"absoluteIntensity"],
           [additionalPhase_x,r'Additional Phase due to Waveguide in nm',0,"additionalPhase_x"],
           [additionalPhase_y,r'Additional Phase due to Waveguide in nm',0,"additionalPhase_y"],
           [additionalPhase_z,r'Additional Phase due to Waveguide in nm',0,"additionalPhase_z"],]

for param in params:
    if param[2] == 1 or showAll:
        print(param[1])
        fig = plt.figure(figsize=(1.5*17/(3*2.54),1.5*8.5/(2.54)))
        if dimension==3:
            ax = fig.gca(projection='3d')
            ax.view_init(elev=elevationAngle, azim=azimuthalAngle)
            if setup == "spherical":
                surf = ax.plot_surface(phi/pi*180, theta/pi*180, param[0].real, rstride=1, cstride=1, cmap=cm.rainbow,
                                       linewidth=0, antialiased=False)
                if labels:
                    ax.set_xlabel(r'azimuthal angle $\phi$')
                    ax.set_ylabel(r'polar angle $\theta$')
            if setup == "cartesian":
                surf = ax.plot_surface(x*1e6, y*1e6, param[0].real, rstride=1, cstride=1, cmap=cm.rainbow,
                                       linewidth=0, antialiased=False)
                if labels:
                    ax.set_xlabel(r'x in $\mu$m')
                    ax.set_ylabel(r'y in $\mu$m')
                    
                ax.set_xticks([-200,0,200])
                ax.set_yticks([-200,0,200])
                    
            if labels: ax.set_zlabel(param[1])
            
            if colorbar: fig.colorbar(surf, shrink=0.5, aspect=5)
        elif dimension == 2:
            plt.plot(theta_1, param[0][0,:].real)
        else:
            break
        plt.tight_layout()
        plt.show()
        
        if save:
            fig.savefig('../plots/parameters/' + param[3] + '(' + setup + ',D' + str(dimension) + ')' + '.pdf')
       