# -*- coding: utf-8 -*-
"""
Comparison between Rayleigh and Mie scattering

@author:    Silvio
@Created:   Tue Sep 13 16:56:34 2016

"""


# coding: utf-8

import matplotlib.pyplot as plt
from matplotlib.ticker import *
import numpy as np
from numpy import pi, sqrt, ceil
from scipy.special import sph_jn, sph_yn
import sys
sys.path.append('../')
from LorentzDrude import LD

figSize = (15/(3*2.54), 15/(3*2.54))
plt.rcParams.update({'font.size': 12})

radii = [15, 25, 40] # in nm

for radius in radii:
    n_medium = 1.33            #refractive index of surround medium 
    wavelength_start = 500.0   #wavelength range lower limit (nm)
    wavelength_end = 700.0    #wavelength range upper limit (nm)
    wavelength_points = 200    #total number of datapoint to be calcualted
    
    
    wavelength0_array = np.linspace(wavelength_start,wavelength_end,wavelength_points)
    array_lens = len(wavelength0_array)
    
    omega = 2*pi*3.0e3/wavelength0_array
    epson_inf = 5.9673
    omega_d = 132.0
    gama_d = 1.0
    tor_l = 6.6
    Bomega_l = 41.0
    deta_epson = 1.09
    
    epson = LD(wavelength0_array*1e-9, material = 'Au', model = 'LD').eps_r # homogenious, isotropic sphere
    n_particle = sqrt(epson)
    
    #plt.figure(figsize=(12, 6))
    #plt.rcParams.update({'font.size': 18})
    #plt.title('permittivity of gold')
    #plt.plot(wavelength0_array,epson.real,wavelength0_array,epson.imag, lw=4)
    #plt.legend(('real','imag'),loc='lower left')
    #plt.xlabel('wavelength (nm)')
    #plt.ylabel('Permittivity')
    #plt.grid(True)
    #plt.show()
    
    
    Qscat = np.zeros(array_lens)
    Qext = np.zeros(array_lens)
    Qabs = np.zeros(array_lens)
    
    
    for i in range(array_lens):
        wavelength0 = wavelength0_array[i]
        
        wavelength = wavelength0/n_medium
        m = n_particle[i]/n_medium              #relative index
        k_medium = 2*pi/wavelength            
        k_particle = m*k_medium
        r0 = k_medium * radius
        r1 = k_particle * radius
        
        Nmax = r0 + 4*r0**(1.0/3.0) + 2        #maximum order to truncate at given size parameter ka
        Nmax = ceil(Nmax)
        n = np.arange(1,Nmax+1)
        
        j0_temp = sph_jn(Nmax,r0)[0]                   #spherical Bessel function array
        j0 = np.delete(j0_temp,0)                   #delete sph_jn(0,r0) to calculate jn(n,r0)
        j0_1 = np.delete(j0_temp,-1)                #for calculating jn(n-1,r0)
    
        y0_temp = sph_yn(Nmax,r0)[0]                   
        y0 = np.delete(y0_temp,0)
        y0_1 = np.delete(y0_temp,-1) 
    
        j1_temp = sph_jn(Nmax,r1)[0]
        j1 = np.delete(j1_temp,0)
        j1_1 = np.delete(j1_temp,-1) 
    
        h0 = j0 + 1j*y0
        h0_1 = j0_1 + 1j*y0_1
    
        j0d = r0*j0_1 - n*j0
        j1d = r1*j1_1 - n*j1
        h0d = r0*h0_1 - n*h0
        
        #The coefficients of the scattered field
        a = ( m**2*j1*j0d - j0*j1d ) / ( m**2*j1*h0d - h0*j1d )
        b = ( j1*j0d - j0*j1d ) / ( j1*h0d - h0*j1d )
        
        #The coefficients incide particle
        c = ( j0*h0d - h0*j0d ) / ( j1*h0d - h0*j1d )
        d = ( m*j0*h0d - m*h0*j0d ) / ( m**2*j1*h0d - h0*j1d )
        
        #calculate cross section (nm^2))
        Qscat[i] = (2/r0**2)*sum((2*n+1)*(abs(a)**2+abs(b)**2))
        Qext[i] = (2/r0**2)*sum((2*n+1)*(a+b).real)
    
    
    Qabs = Qext - Qscat
    
    # comparison to Rayleigh / Mie
    alpha = 4 * pi * radius**3 * (n_particle**2-n_medium**2)/(n_particle**2+2*n_medium**2)
    k = 1.33*2*pi / wavelength0_array
    
    scatteringEfficiency = k**4/(6*pi)*(alpha*np.conj(alpha))/(pi*radius**2)
    absorptionEfficiency = k*alpha.imag/(pi*radius**2)
    extinctionEfficiency = scatteringEfficiency+absorptionEfficiency
    
    fig1a = plt.figure(figsize=figSize)
    #plt.title('Radius = %.0f nm' %radius)
    plt.plot(wavelength0_array,Qscat,'b',wavelength0_array,Qabs,'g',wavelength0_array,Qext,'r')
    plt.plot(wavelength0_array,scatteringEfficiency,'b--', wavelength0_array,absorptionEfficiency,'g--',wavelength0_array,extinctionEfficiency,'r--')
    #plt.legend((r'$Q_{scat}$ Mie',r'$Q_{abs}$ Mie',r'$Q_{ext}$  Mie',r'$Q_{scat}$ Rayleigh',r'$Q_{abs}$ Rayleigh',r'$Q_{ext}$  Rayleigh'),loc='upper right')
#    plt.xlabel('wavelength (nm)')
#    plt.ylabel('Scattering Efficiencies Q')
#    plt.grid(True)
#    plt.ylim([0,8])
    plt.locator_params(axis='y',nbins=4)
    plt.show()
    fig1a.savefig('scatteringEfficiency_r' + str(radius) + '.pdf')
    
    
    fig1b = plt.figure(figsize=figSize)
    #plt.title('Radius = %.0f nm' %radius)
    plt.plot(wavelength0_array,Qscat/scatteringEfficiency-1,'b',wavelength0_array,Qabs/absorptionEfficiency-1,'g',wavelength0_array,Qext/extinctionEfficiency-1,'r')
    #plt.legend((r'$Q_{scat}$ Mie',r'$Q_{abs}$ Mie',r'$Q_{ext}$  Mie',r'$Q_{scat}$ Rayleigh',r'$Q_{abs}$ Rayleigh',r'$Q_{ext}$  Rayleigh'),loc='upper right')
#    plt.xlabel('wavelength (nm)')
#    plt.ylabel(r'$Q_{Mie}/Q_{Rayleigh}$')
#    plt.grid(True)
#    plt.ylim([0,2])
    plt.locator_params(axis='y',nbins=4)
    plt.show()
    fig1b.savefig('scatteringEfficiencyError_r' + str(radius) + '.pdf')
    
    Cabs = Qabs * pi*(radius*1e-9)**2
    Cext = Qext * pi*(radius*1e-9)**2
    Cscat = Qscat * pi*(radius*1e-9)**2
    
    absorptionCrosssection = absorptionEfficiency * pi*(radius*1e-9)**2
    scatteringCrosssection = scatteringEfficiency * pi*(radius*1e-9)**2
    extinctionCrosssection = extinctionEfficiency * pi*(radius*1e-9)**2
    
    
    fig2 = plt.figure(figsize=figSize)
    #plt.title('Radius = %.0f nm' %radius)
    plt.plot(wavelength0_array,Cscat,'b',wavelength0_array,Cabs,'g',wavelength0_array,Cext,'r')
    plt.plot(wavelength0_array,scatteringCrosssection,'b--', wavelength0_array,absorptionCrosssection,'g--',wavelength0_array,extinctionCrosssection,'r--')
    #plt.legend((r'$Q_{scat}$ Mie',r'$Q_{abs}$ Mie',r'$Q_{ext}$  Mie',r'$Q_{scat}$ Rayleigh',r'$Q_{abs}$ Rayleigh',r'$Q_{ext}$  Rayleigh'),loc='upper right')
#    plt.xlabel('wavelength (nm)')
#    plt.ylabel(r'Scattering Crosssection $\sigma$')
#    plt.grid(True)
#    plt.ylim([0,4e-14])
    plt.locator_params(axis='y',nbins=4)
    plt.show()
    fig2.savefig('scatteringCrosssection_r' + str(radius) + '.pdf')
    
