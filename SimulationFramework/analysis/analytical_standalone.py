# -*- coding: utf-8 -*-
"""
In this file, different parameters are analyzed analytically and semi-
analytically. Its purpose is to find an optimal molographic mass for 
a labeled mologram (gold nanoparticles) as well as the influences of 
different noise sources.

@author: Silvio, adapted and documented by Andreas Frutiger

.. todo:: 
- validate with simulations / measurements

"""
from numpy import pi, sqrt, inf, cos, sin, exp, conj, log
import numpy as np

import sys
sys.path.append('../')
from waveguide.waveguide import SlabWaveguide           # waveguide calculations
from interfaces.configuration import Media
from materials.materials import Particle, IsotropicMaterial, PlaneMaterial
import auxiliaries.constants as constants
import dipole.dipole as dipole
from LorentzDrude import LD, particleRadius, refractiveIndex
from binder.Biosensor import *
from waveguidebackground.background import backgroundScatteringWaveguide
from auxiliaries.Journal_formats_lib import formatPRX
from scipy.optimize import minimize

# laser
wavelength = 632.8e-9
k_0 = 2*pi/wavelength
inputPower = 2e-3
beamWaist = 2e-3
Input_power_per_unit_length = inputPower/(beamWaist)

# mologram
f = 900e-6
D = 400e-6

# materials
# protein: SAv
M_SAv = 52.8e3 # kDa
radius_p = particleRadius(M_SAv)
eps_p = refractiveIndex(M_SAv)**2

# gold
radius_g = 21e-9
eps_g = LD(np.array([wavelength]), material = 'Au', model = 'LD').eps_r

# background protein: albumin
M_albumin = 66.5e3 # Da
radius_b = particleRadius(M_albumin)
eps_b = refractiveIndex(M_albumin)**2
A_0 = 1e-3  # concentration antibody (albumin) [mol/L]
R_0 = 2e10*1e6  # active site density
K_D = 1e-3  # antibody binding affinity
N_b = LangmuirIsoterm(A_0,K_D)*D**2/4*R_0

# media configuration
# cover
n_c = 1.33
aqueousMedium = IsotropicMaterial('Water', n_0=n_c)

# substrate
n_s = 1.521
glassSubstrate = IsotropicMaterial('Glass', n_0=n_s)

# waveguide
n_f = 2.117
d_f = 145e-9
propagationLoss = 0.8*np.log(10)/10. #  dB/cm conversion to Np/m
couplingEfficiency = 0.3

waveguide = SlabWaveguide(name='Ta5O2', n_s=n_s, n_f=n_f, 
                      n_c=n_c, d_f=d_f, polarization='TE', 
                        inputPower=couplingEfficiency*Input_power_per_unit_length, 
                        wavelength = wavelength,
                        attenuationConstant=propagationLoss, mode=0)


# initialization
configuration = Media([-inf, -d_f, 0, inf],[glassSubstrate, waveguide, aqueousMedium])

upperMedium = configuration.returnMedium(inf)
lowerMedium = configuration.returnMedium(-inf)

n_1 = upperMedium.n
n_n = lowerMedium.n
k_1 = n_1*k_0
k_n = n_n*k_0
eps_0 = constants.eps_0  
eps_1 = upperMedium.eps_r
eps_n = lowerMedium.eps_r
mu_0 = constants.mu_0
mu_n = 1
mu_1 = 1
Z_w = sqrt(mu_0/(eps_0*eps_n))
E_0 = waveguide.E_y(wavelength, np.array([-D/2.]), np.array([0]), np.array([-d_f/2.]))[0].real # ĥere everything is correct with the units. 

########################################################

print("---- Calculations ----")
# size
print("Protein radius: " + str(radius_p))
print("Gold radius: " + str(radius_g))

# refractive index
print("Protein refractive index: " + str(np.sqrt(eps_p)))
print("Gold refractive index: " + str(np.sqrt(eps_g)))

# polarizability
alpha_p = dipole.dipolePolarizability(radius_p, eps_p, eps_1)
alpha_g = dipole.dipolePolarizability(radius_g, eps_g, eps_1)
print("Protein polarizability: " + str(alpha_p))
print("Gold polarizability: " + str(alpha_g))

scatteringCrossSection_p = dipole.scatteringCrossSection(k_0*n_c, radius_p, eps_p, eps_1)
scatteringCrossSection_g = dipole.scatteringCrossSection(k_0*n_c, radius_g, eps_g, eps_1)
print("Protein scattering cross section: " + str(scatteringCrossSection_p))
print("Gold  scattering cross section: " + str(scatteringCrossSection_g))

absorptionCrossSection_p = dipole.absorptionCrossSection(k_0*n_c, radius_p, eps_p, eps_1)
absorptionCrossSection_g = dipole.absorptionCrossSection(k_0*n_c, radius_g, eps_g, eps_1)
print("Protein absorption cross section: " + str(absorptionCrossSection_p))
print("Gold  absorption cross section: " + str(absorptionCrossSection_g))

extinctionCrossSection_p = dipole.extinctionCrossSection(k_0*n_c, radius_p, eps_p, eps_1)
extinctionCrossSection_g = dipole.extinctionCrossSection(k_0*n_c, radius_g, eps_g, eps_1)
extinctionCrossSection_b = dipole.extinctionCrossSection(k_0*n_c, radius_b, eps_b, eps_1)
print("Protein extinction cross section: " + str(extinctionCrossSection_p))
print("Gold extinction cross section: " + str(extinctionCrossSection_g))
print("Albumin extinction cross section: " + str(extinctionCrossSection_b))

print("Protein scattering efficiency: " + str(scatteringCrossSection_p/(pi*radius_p**2)))
print("Gold  scattering efficiency: " + str(scatteringCrossSection_g/(pi*radius_g**2)))

print("Protein absorption efficiency: " + str(absorptionCrossSection_p/(pi*radius_p**2)))
print("Gold  absorption efficiency: " + str(absorptionCrossSection_g/(pi*radius_g**2)))

print("Protein extinction efficiency: " + str(extinctionCrossSection_p/(pi*radius_p**2)))
print("Gold  extinction efficiency: " + str(extinctionCrossSection_g/(pi*radius_g**2)))

# exciting field
z_p = 20e-9 # that means that the particles are distributed in a band of 20 nm, and also that their center is at 20nm above the waveguide surface. 
E_p = waveguide.E_y(wavelength,np.array([0]),np.array([0]),np.array([z_p]))
z_g = 40e-9
E_g = waveguide.E_y(wavelength,np.array([0]),np.array([0]),np.array([z_g]))
print("Electric field at proteins position: " + str(E_p))
print("Electric field at gold position: " + str(E_g))

alpha_ext = 2*extinctionCrossSection_g/(pi*D*100e-9) # particles are distributed over 100 nm. 
N_sat = -log(0.1)/alpha_ext
print(N_sat)

# dipole moment
p_p = dipole.staticDipoleMoment(radius_p, eps_p, eps_1, E_p)
p_g = dipole.staticDipoleMoment(radius_g, eps_g, eps_1, E_g)
p_b = dipole.staticDipoleMoment(radius_b, eps_b, eps_1, E_p)
print("Dipole moment for the protein: " + str(p_p))
print("Dipole moment for the gold particle: " + str(p_g))
print("Dipole moment for the albumin particle: " + str(p_b))

# dipole radiation
P_0p = dipole.dipoleRadiation(p_p, wavelength, n=n_1)
P_0g = dipole.dipoleRadiation(p_g, wavelength, n=n_1)
P_0b = dipole.dipoleRadiation(p_b, wavelength, n=n_1)
print("Dipole radiation for the protein: " + str(P_0p))
print("Dipole radiation for the gold particle: " + str(P_0g))
print("Dipole radiation for the albumin particle: " + str(P_0b))

# minimum dipole intensity
theta_max = np.arctan((D/2.)/f)
print("Maximal angle: " + str(theta_max/pi*180) + "°")
theta = theta_max
phi = pi/2.
r = f/cos(theta_max/2) # as an approximation, we take an average distance r (linear average) of the scatterer to the focal spot, this will be used for all subsequent calculations

I_0p = 3/(8*pi*r**2) * n_n/n_1 * P_0p * (sin(phi)**2 * cos(theta)**2 + cos(phi)**2)
I_0g = 3/(8*pi*r**2) * n_n/n_1 * P_0g * (sin(phi)**2 * cos(theta)**2 + cos(phi)**2)
I_0b = 3/(8*pi*r**2) * n_n/n_1 * P_0b * (sin(phi)**2 * cos(theta)**2 + cos(phi)**2)
print("Minimal intensity contribution of a protein particle: " + str(I_0p))
print("Minimal intensity contribution of a gold particle: " + str(I_0g))
print("Minimal intensity contribution of a albumin particle: " + str(I_0b))

backgroundParticles = 1

print("Incident Power on the mologram [W]: " + str(Input_power_per_unit_length*D))


# electric field contributions
f_corr = 2/pi # correction factor as dipoles are on the ridges rather than on the mololines (comes from the sinusoidal contribution)

def N_Particles_eff(N_Particles,extinctionCrossSection_P,z):
    
    """Calculates the effective number of particles on the coherent structure, taking
    into account the extinction of the individual particles. However, different particle species are not taken into
    account correctly, if there are many particles on the mologram. This is not the case for all practical cases. The equation arises from an infinite series. The equâtion for the exponent is correct, I checked it (AF 20.7.2017)
    z_p is at least the particle diameter. ATTENTION: If the particles are ordered coherently, this formula must not be used, because the extinction is proportional to the square of the particles.

    :param N_Particles: Is the number of particles on the coherent structure
    :param extinctionCrossSection_P: Is the extinction cross section of the particle.

    .. math:: N_{Particles,eff} = \\frac{{\\exp \\left( { - \\frac{{2 \\cdot {\\sigma _{ext}}}}{{\\pi D \\cdot {z_0}}} \\cdot N} \\right) - 1}}{{\\exp \\left( { - \\frac{{2 \\cdot {\\sigma _{ext}}}}{{\\pi D \\cdot {z_0}}}} \\right) - 1}}

    """

    return (np.expm1(-2*extinctionCrossSection_P/(pi*D*z)*N_Particles)/np.expm1(-2*extinctionCrossSection_P/(pi*D*z)))

# effective number of particles, this contains the approximation that only a minor part of the intensity is coupled out, otherwise all these particles must influence 
# each other.

N_bias_eff = lambda N_bias: N_Particles_eff(N_bias,extinctionCrossSection_b,z_p)
N_gold_eff = lambda N_gold: N_Particles_eff(N_gold,extinctionCrossSection_g,z_g)
N_albumin_eff = lambda N_albumin: N_Particles_eff(N_albumin,extinctionCrossSection_b,z_p)

# I should rather calculate a field correction term here. 


def E_bias(N_bias):
    """ Calculates the field of the bias
        .. math:: E_{bias} = \\frac{2}{\\pi}\\cdot\\frac{k_1^2\\cdot N_{bias,eff}\\cdot p}{4\\pi\\epsilon_0\\epsilon_1\\cdot r}\\cdot exp(j\\cdot k_n\\cdot r)
    """
    
    return f_corr*k_1**2*N_bias_eff(N_bias)*p_p/(4*pi*eps_0*eps_1*r) * exp(1j*k_n*r)

def E_gold(N_gold):
    """
        .. math:: E_{gold} = \\frac{2}{\\pi}\\cdot\\frac{k_1^2\\cdot N_{gold,eff}\\cdot p}{4\\pi\\epsilon_0\\epsilon_1\\cdot r}\\cdot exp(j\\cdot k_n\\cdot r)
    """
    
    return f_corr*k_1**2*N_gold_eff(N_gold)*p_g/(4*pi*eps_0*eps_1*r) * exp(1j*k_n*r)

def E_albumin(N_albumin):
    """
        .. math:: E_{albumin} = \\frac{2}{\\pi}\\cdot\\frac{k_1^2\\cdot N_{albumin,eff}\\cdot p}{4\\pi\\epsilon_0\\epsilon_1\\cdot r}\\cdot exp(j\\cdot k_n\\cdot r)
    """
    
    return f_corr*k_1**2*N_albumin_eff(N_albumin)*p_b/(4*pi*eps_0*eps_1*r) * exp(1j*k_n*r)


# noise sources
def E_laser_noise(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB, laserFluctuations):

    """ Calculates the field variations inherent to the laser and amplified by the bias
     
    :param: N_bias number of bias molecules, here protein scatterers
    :param laserFluctuations: standard deviation of laser intensity fluctuations :math: `\\sigma`
    
    .. math:: E_{laser,noise} = \\sqrt{\\sigma}\\cdot \\left(E_{SB} + E_{bias} + E_{msn} \\right)

    """
    
    return laserFluctuations*(E_SB(N_SB) + E_b(N_b) + E_msn(N_SB,N_NSB,E_SB,E_NSB)) # laser shot noise is amplified by the bias and the nonspecifically bound molecules. 


def E_msn(N_SB, N_NSB,E_SB,E_NSB):
    """
        Calculates the field fluctuations due to molecular shot noise of the specific and nonspecific binders.
        
        :param N_SB: Total number of specific binders on the mologram.
        :param N_NSB: Total number of nonspecific binders on the mologram.
        :param E_SB: Function to calculate the field of the specific binders.
        :param E_NSB: Function to calculate the field of the nonspecific binders. 
        
        .. math:: E_{msn} = E_{SB}\\left(\\sqrt{N_{SB}}\\right)+E_{NSB}\\left(\\sqrt{N_{NSB}}\\right)

    """

    return E_SB(sqrt(N_SB)) + E_NSB(sqrt(N_NSB))


# intensities

def I_noise_coh(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB, laserFluctuations,molecular_shot_noise = True):
    """
    Calculates the noise of the intensity for a given laser fluctuation and with molecular shot noise
        
        .. math:: I_{noise} = \\frac{1}{2 Z_w} \\left(2 E_{SB} + 2 E_{bias} + E_{noise}\\right)\\cdot E_{noise}^*

    with 

        .. math:: E_{noise} = E_{msn} + E_{laser,noise}

    """
    if molecular_shot_noise:

        E_noise = E_msn(N_SB,N_NSB,E_SB,E_NSB) + E_laser_noise(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB,laserFluctuations)
    else:
        E_noise = E_laser_noise(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB,laserFluctuations)

    return (1/(2*Z_w)*conj(E_noise)*(E_noise+2*E_SB(N_SB)+2*E_b(N_b))).real


def I_b(N_b,E_b):
    """Calculates the intensity from the bias molecules
        
        .. math:: I_b = \\frac{1}{2 Z_w} E_b(N_b)\\cdot E_b(N_b)*

    """

    return (1/(2*Z_w) * (E_b(N_b) * conj(E_b(N_b)))).real

def I_signal_enhancement(N_b,E_b,N_SB,E_SB): 
    """Calculates the intensity enhancement of the specific binders by themselves as well as by the enhancement of the specific
    binding signal by the bias molecules.
    
        .. math:: I_{enh} = \\frac{1}{2 Z_w} E_{SB}\\cdot\\left( E_{SB} + 2\\cdot E_{b}\\right)

    """

    return (1/(2*Z_w)*(E_SB(N_SB)*conj(E_SB(N_SB)+2*E_b(N_b)))).real

def I_tot_signal(N_b,E_b,N_SB,E_SB):
    """ Calculates the total intensity in the focal spot. 
        
        .. math:: I_{tot} = \\frac{1}{2 Z_w} \\left(E_{SB} + E_{bias}\\right)\\cdot \\left(E_{SB}^* + E_{bias}^* \\right)

    """
 
    return (1/(2*Z_w)*conj(E_SB(N_SB) + E_b(N_b))*(E_SB(N_SB) + E_b(N_b))).real

def I_sn(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB, laserFluctuations,molecular_shot_noise = True): 
    """Calculates the shot noise of the detector, this function is not really used here. To be checked needs to be photons."""

    if molecular_shot_noise:

        E_noise = E_msn(N_SB,N_NSB,E_SB,E_NSB) + E_laser_noise(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB,laserFluctuations)
    else:
        E_noise = E_laser_noise(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB,laserFluctuations)
    
    return sqrt(1/(2*Z_w)*conj(E_noise+2*E_SB(N_SB)+2*E_b(N_b))*(E_noise+2*E_SB(N_SB)+2*E_b(N_b)).real) # shot noise of the detector


def I_background():
    """Calculates the intensity of the waveguide background"""

    # calculation of the waveguide background.

    I_bwg = backgroundScatteringWaveguide(couplingEfficiency*Input_power_per_unit_length,propagationLoss,f,D=D,x_0=0)

    return I_bwg

# signal to background ratio and signal-to-noise ratio. 

def SBR_total(N_b,E_b,N_SB,E_SB):
    """Calculates the signal to background ratio which is defined as the total signal intensity in the focal spot caused by the coherent matter
        divided by the intensity scattered into the focal spot by the waveguide."""

    return I_tot_signal(N_b,E_b,N_SB,E_SB)/I_background()

def SNR_total(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB, laserFluctuations,molecular_shot_noise = True):
    """Calculates the signal to noise ratio, which is defined as the total signal in the focal spot, divided by the Intensity contributions of the different noise sources."""

    return I_tot_signal(N_b,E_b,N_SB,E_SB)/I_noise_coh(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB, laserFluctuations,molecular_shot_noise)


def SBR_specific_binder(N_b,E_b,N_SB,E_SB):
    """The signal to background increase for a certain amount of molecules that bind to the sensor."""
    return I_signal_enhancement(N_b,E_b,N_SB,E_SB)/I_background()

def RelSigEnhancement(N_b,E_b,N_SB,E_SB):
    """returns the relative change in signal with respect to the total signal present in the focal spot when a given amount of analyte binds to the 
    mologram. """

    return I_signal_enhancement(N_b,E_b,N_SB,E_SB)/I_tot_signal(N_b,E_b,N_SB,E_SB)

def SNR_specific_binder(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB, laserFluctuations,molecular_shot_noise = True):
    """Calculates the signal to noise ratio of the signal increase by the specific binder compared to all the noise sources"""

    return I_signal_enhancement(N_b,E_b,N_SB,E_SB)/I_noise_coh(N_b,E_b,N_SB,E_SB,N_NSB,E_NSB, laserFluctuations,molecular_shot_noise)

# ------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------
# formating functions for the x-axis of the plots.

def formatSurfaceMassModulation(x, labeling=0):
    'returns surface mass modulation in pg/mm2'
    if labeling==1: return '[pg/mm2]'
    else: return x/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6

def formatMass(x, labeling=0):
    'returns mass'
    if labeling==1: return '[fg]'
    else: return x/constants.N_A*M_SAv*1e15

def formatConcentration(x, labeling=0):
    if labeling==1: return '[mol]'
    else: return x/constants.N_A
    
def formatScatterers(x, labeling=0):
    if labeling==1: return ''
    else: return x
def formatGlycine(x,labeling=0):
    """returns the number of glycines per nm^2"""
    x = formatSurfaceMassModulation(x,labeling)

    if labeling == 1: return '[glycines/nm^2]'
    else: return x*1e-12/75*1e-12*constants.N_A
    
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

# define the formatter for the x-axis
formatter = formatMass


print("\n---- Plots Andreas Frutiger----")

plots = {"Glycines":0,  # effective number of scatterers

        }

formatPRX()

# ------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------
# Figure: number of glycines required per nm² to couple out all the light (No noise sources present)
fig = plt.figure()
array_length = 1000 # array length for bias
N_b = np.logspace(10,14,array_length)
N_ana = 0
N_NSB = 0

Maximum = np.max(I_tot_signal(N_b,E_bias,N_ana,E_bias))

plt.semilogx(formatGlycine(N_b),I_tot_signal(N_b,E_bias,N_ana,E_bias)/np.max(I_tot_signal(N_b,E_bias,N_ana,E_bias)))
plt.xlabel('glycines/nm^2')
plt.ylabel('Normalized intensity')
plt.savefig("plots_analysis_standalone/001glycines_to_couple_out.png",format='png')



fig = plt.figure()

plt.semilogx(formatGlycine(N_b),(I_tot_signal(N_b,E_bias,0,E_bias)-I_tot_signal(N_b-10**8,E_bias,0,E_bias))/np.max(I_tot_signal(N_b,E_bias,N_ana,E_bias)))
plt.xlabel('glycines/nm^2')
plt.ylabel('Normalized intensity increase per monol. glyc')
plt.savefig("plots_analysis_standalone/002maximum_no_noise_sources.png",format='png')

# ------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------
# Figure: surface mass modulation required to couple out all the light (No noise sources present)
fig = plt.figure()
array_length = 1000 # array length for bias
N_b = np.logspace(10,14,array_length)
N_ana = 0
N_NSB = 0

Maximum = np.max(I_tot_signal(N_b,E_bias,N_ana,E_bias))

plt.semilogx(formatSurfaceMassModulation(N_b),I_tot_signal(N_b,E_bias,N_ana,E_bias)/np.max(I_tot_signal(N_b,E_bias,N_ana,E_bias)))
plt.xlabel(r'pg/mm^2')
plt.ylabel('Normalized intensity')
plt.savefig("plots_analysis_standalone/003surface_mass_modulation_to_couple_out_all_light.png",format='png')



fig = plt.figure()

plt.semilogx(formatSurfaceMassModulation(N_b),(I_tot_signal(N_b,E_bias,0,E_bias)-I_tot_signal(N_b-10**8,E_bias,0,E_bias))/np.max(I_tot_signal(N_b,E_bias,N_ana,E_bias)))
plt.xlabel(r'pg/mm^2')
plt.ylabel('Normalized intensity increase per fg/mm^2')
plt.savefig("plots_analysis_standalone/004maximum_no_noise_sources_surface_mass.png",format='png')

# ------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------
# Figure: only few monolayers of glycine
fig = plt.figure()
array_length = 1000 # array length for bias
N_b = np.logspace(8,9,array_length)
N_ana = 1
N_NSB = 0

plt.plot(formatGlycine(N_b),(I_tot_signal(N_b,E_bias,N_ana,E_bias)/Maximum))
plt.xlabel('glycines/nm^2')
plt.ylabel('Normalized intensity')

plt.savefig("plots_analysis_standalone/005few_monolayers_of_glycine.png",format='png')

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
# Figure: Signal to Background and Signal-to-Noise ratio for 1000 Protein Molecules added to the sensor for different LF

fig = plt.figure()
array_length = 1000 # array length for bias
N_b = np.logspace(4,9,array_length)
N_NSB = 0
N_SB = 100000

for laser_noise in np.logspace(-6,-2,5):

    plt.loglog(formatSurfaceMassModulation(N_b),SNR_specific_binder(N_b,E_bias,N_SB,E_bias,N_NSB,E_albumin, laser_noise, True),label='SNR: Fluct {0:.4f}'.format(laser_noise*100) + '%')

plt.loglog(formatSurfaceMassModulation(N_b),SBR_total(N_b,E_bias,N_SB,E_bias),label= 'SBR')

plt.legend()


plt.xlabel('bias [pg/mm^2]')
plt.ylabel('SNR and SBR')
plt.savefig("plots_analysis_standalone/006SNR_10000P_laser_noise_with_molecular_shot_noise.png",format='png')

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
# Figure: Signal to Background and Signal-to-Noise ratio for 130 Gold particles added to the sensor for different LF

fig = plt.figure()
array_length = 1000 # array length for bias
N_b = np.logspace(4,9,array_length)
N_NSB = 0
N_SB = 200

for laser_noise in np.logspace(-6,-2,5):

    plt.loglog(formatSurfaceMassModulation(N_b),SNR_specific_binder(N_b,E_bias,N_SB,E_bias,N_NSB,E_albumin, laser_noise, True),label='SNR: Fluct {0:.4f}'.format(laser_noise*100) + '%')

plt.loglog(formatSurfaceMassModulation(N_b),SBR_total(N_b,E_bias,N_SB,E_gold),label= 'SBR')

plt.legend()


plt.xlabel('bias [pg/mm^2]')
plt.ylabel('SNR and SBR')
plt.savefig("plots_analysis_standalone/007SNR_200_GNP_laser_noise_with_molecular_shot_noise.png",format='png')

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
# Figure: Minimum amount of proteins that are detectable for an SNR of 10 and an SBR of 10
# under the assumption that the bias mass is much larger than the analyte mass and no nonspecific binders

fig = plt.figure()
array_length = 100 # array length for bias
laser_noise = np.logspace(-5,-2,array_length)
N_NSB = 0

# find the bias, which gives an SBR of 10.
N_SB = 0
# N_b = 100000
# print abs(SBR_total(N_b,E_bias,N_SB,E_bias) - 10.0
func_bias = lambda N_b: (SBR_total(N_b,E_bias,N_SB,E_bias) - 10.0)**2
print "optimal Protein Bias for SBR of 10: [pg/mm^2]"
bias_10SBR = minimize(func_bias,100000,tol=10e-16).x
print formatSurfaceMassModulation(bias_10SBR)
N_b = bias_10SBR
print N_b

minimal_det_amount = []
for ls in laser_noise:
    func_bias = lambda N_SB: (SNR_specific_binder(N_b,E_bias,N_SB,E_bias,N_NSB,E_albumin, ls, True) - 10.0)**2
    minimal_det_amount.append(minimize(func_bias,1000000,tol=10e-16,constraints={'type': 'ineq', 'fun': lambda N_SB:N_SB}).x)
    # minimal_det_amount.append((SNR_specific_binder(N_b,E_bias,10000,E_bias,N_NSB,E_albumin, ls, True)))

plt.loglog(laser_noise*100,np.asarray(minimal_det_amount))

plt.xlabel('Laser/Detector Fluctuations [%]')
plt.ylabel('Minimal detectable amount of Protein Molecules')
plt.savefig("plots_analysis_standalone/008_Minimal_detectable_amount_of_protein_molecules_no_NSB.png",format='png')

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
# Figure: relative intensity change upon protein binding as a function of bias. 
fig = plt.figure()
plt.loglog(np.asarray(minimal_det_amount),RelSigEnhancement(N_b,E_bias,np.asarray(minimal_det_amount),E_bias))

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
# Figure: Minimum amount of proteins that are detectable for an SNR of 10 and an SBR of 10
# under the assumption that the bias mass is much larger than the analyte mass, here we consider different amounts of nonspecific binders.
# I did not consider whether this is detectable by the optical system.

fig = plt.figure()
array_length = 100 # array length for bias
laser_noise = np.logspace(-5,-2,array_length)
N_NSBs = np.asarray([100,1000,5000,10000,100000,1000000])

# find the bias, which gives an SBR of 10.
N_SB = 0
# N_b = 100000
# print abs(SBR_total(N_b,E_bias,N_SB,E_bias) - 10.0
func_bias = lambda N_b: (SBR_total(N_b,E_bias,N_SB,E_bias) - 10.0)**2
print "optimal Protein Bias for SBR of 10: [pg/mm^2]"
bias_10SBR = minimize(func_bias,100000,tol=10e-16).x
print formatSurfaceMassModulation(bias_10SBR)
N_b = bias_10SBR
print N_b

for N_NSB in N_NSBs:

    minimal_det_amount = []
    for ls in laser_noise:
        func_bias = lambda N_SB: (SNR_specific_binder(N_b,E_bias,N_SB,E_bias,N_NSB,E_albumin, ls, True) - 10.0)**2
        minimal_det_amount.append(minimize(func_bias,1000000,tol=10e-16,constraints={'type': 'ineq', 'fun': lambda N_SB:N_SB}).x)
        # minimal_det_amount.append((SNR_specific_binder(N_b,E_bias,10000,E_bias,N_NSB,E_albumin, ls, True)))

    plt.loglog(laser_noise*100,np.asarray(minimal_det_amount),label='{:.1e}'.format(N_NSB) + ' NSB')
plt.legend()
plt.xlabel('Laser/Detector Fluctuations [%]')
plt.ylabel('Minimal detectable amount of Protein Molecules')
plt.savefig("plots_analysis_standalone/009_Minimal_detectable_amount_of_protein_molecules_with_NSB.png",format='png')








#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
# Figure: shot noise plot

fig = plt.figure()
array_length = 1000 # array length for bias
N_b = np.logspace(8,9,array_length)
N_SB = 100
N_NSB = 10000

plt.plot(formatGlycine(N_b),SNR_total(N_b,E_bias,N_SB,E_bias,N_NSB,E_albumin, 1e-3, False))

plt.xlabel('glycines/nm^2')
plt.ylabel('Normalized intensity')

plt.savefig("plots_analysis_standalone/SNR.png",format='png')

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------
# SNR specific binder dependence

fig = plt.figure()
array_length = 1000 # array length for bias
N_b = np.logspace(1,6,array_length)
N_NSB = 1000000

for N_SB in np.linspace(1,10000,5):

    plt.plot(formatSurfaceMassModulation(N_b),SNR_total(N_b,E_bias,N_SB,E_bias,N_NSB,E_albumin, 1e-3, True))


plt.xlabel('[pg/mm^2]')
plt.ylabel('Normalized intensity')
plt.savefig("plots_analysis_standalone/SNR_specific_binders.png",format='png')



# print("\n---- Plots ----")

# # plot settings
# font = {'family' : 'Arial',
# 	'weight' : 'normal',
# 	'size'   : 12
# }
# params = {'legend.fontsize':   8,
# 	  'xtick.labelsize' : 10,
# 	  'ytick.labelsize' : 10
# 			}
# plt.rcParams.update(params)
# plt.rc('font', **font)

# # determine what should be plotted
# plots = {"N_eff":0,  # effective number of scatterers
#          "N_eff_gold":0, #effective number of gold scatterers
#          "I_tot":0, # total intensity scattered into the focal spot
#          "sensitivity":0, # sensitivity if no noise source is present
#          "amount_NGP":0,
#          "I_enhanced":0,
#          "optimal_bias":0,
#          "backgrounds":0,
#          "SNR10":0,
#          "GNP_required":0,
#          "SNR_gold":0, 
#          "GNP_required_enhanced":0, 
#          "SNR_saturation":0, 
#          "1GNP":0,
#          "SNR_enhanced_maxima":0,
#          "different_fluctuations":0,
#          "different_fluctuations_gold":0,
#          "different_fluctuations_enhanced":0,
#          "no_fluctuations":0}

# colormap = ['black','red','green','blue','cyan','magenta']
# showAll = False

# # background / noise intensities
# array_length = 1000 # array length for bias
# laserFluctuations = 1e-3

# if showAll:
#     for key in plots:
#         plots[key] = 1
        
# plotNr = 0

# if plots["N_eff"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": effective number of particles")
#     N_bias_array = np.logspace(11,14,array_length)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.semilogx(N_bias_array,N_bias_eff(N_bias_array))#I_tot(0,N_gold_array,0))
#     varsigma = 2*extinctionCrossSection_p/(pi*D*z_p)
#     plt.axvline(-np.log(0.05)/varsigma, color='k')
#     plt.axvline(np.log(2*(np.exp(varsigma)+1))/varsigma, color='k')
#     plt.xlabel("protein scatterers")
#     plt.ylabel(r"effective number of scatterers")
#     plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/N_eff.pdf")
    
# if plots["N_eff_gold"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": effective number of particles")
#     N_bias_array = np.logspace(3,6,array_length)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.semilogx(N_bias_array,N_gold_eff(N_bias_array))#I_tot(0,N_gold_array,0))
#     varsigma = 2*extinctionCrossSection_g/(pi*D*z_g)
#     plt.axvline(-np.log(0.05)/varsigma, color='k')
#     plt.axvline(np.log(2*(np.exp(varsigma)+1))/varsigma, color='k')
#     plt.xlabel("protein scatterers")
#     plt.ylabel(r"effective number of scatterers")
#     plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/N_eff_gold.pdf")
    
    
# if plots["I_tot"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": Total intensity due to bias")
#     N_bias_array = np.logspace(12,15,array_length)    
#     c_SAv_array = N_bias_array/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6
    
#     fig, ax = plt.subplots(1,1, figsize=(17/2.54,8.5/2.54))
#     plt.semilogx(formatMass(N_bias_array)*1e-9,I_tot(N_bias_array,0,0))
#     plt.xlabel("Bias [ug]")
#     plt.ylabel(r"Intensity in $W/m^2$")
# #    plt.xlim([1e5,1e8])
    
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/I_tot.pdf")
    
    
# if plots["sensitivity"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": Sensitivity")
#     N_bias_array = np.logspace(12,15,array_length)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.semilogx(formatMass(N_bias_array)*1e-9,np.hstack(([0],np.diff(I_tot(N_bias_array,0,0))*1e-6)))
#     plt.xlabel("Bias [ug]")
#     plt.ylabel(r"Sensitivity")
# #    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
# #    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/sensitivity.pdf")

# # how many gold particles are required for the same molographic signal?
# if plots["amount_NGP"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": Gold particles needed to achieve same signal")
#     N_bias_array = np.logspace(5,7,array_length)
#     NGP = np.arange(10000)
#     NGP_required = []
#     for N_bias in N_bias_array:
#         NGP_required.append(np.min(np.where(I_tot(0,NGP,0)>I_tot(N_bias,0,0))))

#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.plot(formatter(N_bias_array), np.array(NGP_required))
#     plt.xlabel("Bias (SAv) " + formatter(N_bias_array, labeling=1))
#     plt.ylabel("Number of Gold Nanoparticles (Au)")
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/amount_NGP.pdf")


# if plots["I_enhanced"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": Total intensity due to bias")
#     N_bias_array = np.logspace(9,15,array_length)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.semilogx(formatter(N_bias_array),I_tot(N_bias_array,50,0)-I_bias(N_bias_array),label="50 GNP")
#     plt.semilogx(formatter(N_bias_array),I_tot(N_bias_array,100,0)-I_bias(N_bias_array),label="100 GNP")
# #    plt.loglog(N_bias_array,I_tot(N_bias_array,200,0)-I_bias(N_bias_array),label="200 GNP")
# #    plt.loglog(N_bias_array,I_tot(N_bias_array,200,1e-10)-I_bias(N_bias_array),label="1e-8% fluctuations")
# #    plt.semilogx(N_bias_array,I_tot(N_bias_array,200,1e-2)-I_bias(N_bias_array),label="1% fluctuations")
#     plt.xlabel("Bias " + formatter(N_bias_array, labeling=1))
#     plt.ylabel(r"Intensity in $W/m^2$")
# #    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/I_enhanced.pdf")
    
# if plots["optimal_bias"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": Highest increase of intensity for additional gold particles")
#     N_bias_array = np.logspace(9,15,array_length)
    
#     N_gold_array = np.linspace(1,30000,200)
#     optimal_bias_array = []
#     difference = []
#     for N_gold in N_gold_array:
#         optimal_bias_array.append(N_bias_array[np.diff(I_tot(N_bias_array,N_gold,0)-I_tot(N_bias_array,N_gold-1,0)).argmax()])
#         difference.append(I_tot(optimal_bias_array[-1],N_gold,0)-I_tot(optimal_bias_array[-1],N_gold-1,0))
        
#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
#     plt.plot(N_gold_array, formatter(np.asarray(optimal_bias_array)))
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/optimal_bias.pdf")
#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
#     plt.plot(N_gold_array, difference)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/optimal_bias_difference.pdf")
    
# if plots["backgrounds"]:
#     plotNr += 1
#     print("Plot " + str(plotNr) + ": Different background contributions")
#     N_bias_array = np.logspace(5,8,array_length)
#     c_SAv_array = N_bias_array/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6
    
#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
#     plt.semilogx(N_bias_array,I_noise(N_bias_array, 0, laserFluctuations)-I_noise_nomsn(N_bias_array, 0, laserFluctuations), color="black", label="Molecular Shot Noise")
#     plt.semilogx(N_bias_array,I_sn(N_bias_array, 0, laserFluctuations), color="red", label="Detector Shot Noise")
#     plt.semilogx(N_bias_array,I_NCB(N_bias_array,0,0.8,0.8), color="green", label="Non-specific binding (p=0.8)")
#     plt.semilogx(N_bias_array,I_noise_nomsn(N_bias_array, 0, laserFluctuations), color="blue", label="Laser Fluctuations of " + str(laserFluctuations*1e2) + '%')
#     plt.semilogx(N_bias_array,np.zeros(len(N_bias_array))+I_bwg, color="cyan", label="Waveguide")
#     plt.semilogx(N_bias_array,np.zeros(len(c_SAv_array))+I_NCB2, color="green", label="Non-specific binding (Albumin)")
# #    plt.xlabel("Bias in amount of scatterers")
# #    plt.ylabel(r"Intensity in $W/m^2$")
#     plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
# #    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/backgrounds_1.pdf")
    

#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
#     N_bias_array = np.linspace(0,2000,array_length)
#     plt.plot(N_bias_array,I_noise(N_bias_array, 0, laserFluctuations)-I_noise_nomsn(N_bias_array, 0, laserFluctuations), color="black", label="Molecular Shot Noise")
#     plt.plot(N_bias_array,I_sn(N_bias_array, 0, laserFluctuations), color="red", label="Detector Shot Noise")
#     plt.plot(N_bias_array,I_NCB(N_bias_array,0,0.8,0.8), color="green", label="Non-specific binding (p=0.8)")
#     plt.plot(N_bias_array,I_noise_nomsn(N_bias_array, 0, laserFluctuations), color="blue", label="Laser Fluctuations of " + str(laserFluctuations*1e2) + '%')
#     plt.plot(N_bias_array,np.zeros(len(N_bias_array))+I_bwg, color="cyan", label="Waveguide")
# #    plt.semilogx(N_bias_array,np.zeros(len(c_SAv_array))+I_NCB2, color="green", label="Non-specific binding (Albumin)")
# #    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     plt.ylim([0,2e-13])
# #    plt.xlim([0,2000])
# #    plt.yticks(np.linspace(0,2e-13,6))
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/backgrounds_2.pdf")
    
# #    N_bias_array = np.logspace(1,7,array_length)
# #    plt.loglog(N_bias_array,I_noise(N_bias_array, 0, laserFluctuations)-I_noise_nomsn(N_bias_array, 0, laserFluctuations), color="black", label="Molecular Shot Noise")
# #    plt.loglog(N_bias_array,I_noise_nomsn(N_bias_array, 0, laserFluctuations), color="blue", label="Laser Fluctuations of " + str(laserFluctuations*1e2) + '%')

# if plots["SNR10"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Bias required to reach a SNR of 10")
#     N_bias_array = np.logspace(5,7,array_length)
    
#     N_offset = N_bias_array[I_tot(N_bias_array,0, laserFluctuations) > I_background(N_bias_array,0, laserFluctuations)*10][0]
#     laserFluctuations = 1e-3
    
#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
#     plt.plot(formatter(N_bias_array), I_tot(N_bias_array,0, laserFluctuations), color=colormap[0], label="Signal")
#     plt.plot(formatter(N_bias_array), I_background(N_bias_array,0, laserFluctuations)*10, color=colormap[1], label="background")
#     plt.plot(formatter(N_bias_array), np.zeros(len(c_SAv_array))+I_bwg*10, color=colormap[2], linestyle='dashed', label="waveguide background")
# #    plt.axvline(N_offset, color="black", linestyle='--', label="Offset required: " + str(('%.2e') % N_offset))
#     plt.xlabel("Bias" + formatter(N_bias_array,1))
#     plt.ylabel(r"Intensity in $W/m^2$")
#     plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/SNR10.pdf")

# if plots["GNP_required"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Gold particles required to reach SNR of 10 with bias")
#     N_bias_array = np.linspace(1e6,1e7,array_length)
#     c_SAv_array = N_bias_array/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6
#     N_gold_array = np.round(np.linspace(1,600,200))
#     N_offset = N_bias_array[I_tot(N_bias_array,0, laserFluctuations) > I_background(N_bias_array,0, laserFluctuations)*10][0]
    
#     N_gold_needed = np.zeros(array_length)
#     for i in range(array_length):
#         N_bias = N_bias_array[i]
#         for N_gold in N_gold_array:
#             if SNR(N_bias, N_gold, laserFluctuations) > 10.:
#                 N_gold_needed[i] = N_gold
#                 if N_gold == 1 and N_offset > N_bias_array[i]:
#                     N_offset = N_bias_array[i]
#                 break
           
#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))         
#     plt.plot(c_SAv_array, N_gold_needed)
#     plt.xlabel("Bias in amount of scatterers")
#     plt.ylabel("Number of gold particles required")
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/GNP_required.pdf")

# if plots["SNR_gold"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": (total) SNR for a fixed bias of " + str(int(N_offset)) + " proteins")
#     N_gold_array = np.linspace(0,1000,200)
#     N_offset = 0
#     laserFluctuations = 0#1e-3
    
#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
#     plt.plot(N_gold_array,SNR(N_offset, N_gold_array, laserFluctuations))
#     plt.plot(N_gold_array,np.zeros(len(N_gold_array))+SNR(N_offset, 1, laserFluctuations))
# #    plt.grid(True)
#     plt.xlabel("Number of gold particles")
#     plt.ylabel("Signal-to-noise ratio")
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/SNR_gold.pdf")

# if plots["GNP_required_enhanced"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Gold particles required if only enhanced intensity is considered")
#     N_bias_array = np.logspace(6,9.06,array_length)
    
#     N_gold_array = np.round(np.linspace(1,1000,300))
#     fluctuations_array = np.linspace(1e-3,0,4)
#     colormap_copy = colormap[::-1]
    
#     fig = plt.figure(figsize=(8.5/2.54,8.5/2.54))
#     for laserFluctuations in fluctuations_array:
#         N_gold_min = np.max(N_gold_array)+1
#         N_gold_needed = np.zeros(array_length)
#         for i in range(array_length):
#             N_bias = N_bias_array[i]
#             for N_gold in N_gold_array:
#                 if SNR_enhancement(N_bias, N_gold, laserFluctuations) > 10.:
#                     N_gold_needed[i] = N_gold
#                     if N_gold < N_gold_min:
#                         N_gold_min = N_gold
#                         N_offset = N_bias_array[i]
#                     break
#                 laserFluctuationsPercent = laserFluctuations*100
#         plt.semilogx(formatter(N_bias_array)*1e-3, N_gold_needed, color=colormap_copy.pop(), label="Fluctuations: " + str(('%.3f') % laserFluctuationsPercent) + "%")
#         print(N_gold_min, formatter(N_offset))
        
#     plt.xlabel("Bias [pg]")
# #    plt.xlim([0.1,1e2])
#     plt.ylabel("Number of gold particles required")
# #    plt.xlim([1,1000])
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/GNP_required_enhanced_" + str(backgroundParticles) + ".pdf")


# if plots["SNR_saturation"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Saturation of the SNR")
#     laserFluctuations = 1e-3
#     N_bias_array = np.logspace(5,10,1e4)
    
#     SNR_array = SNR(N_bias_array,1, laserFluctuations)
#     SNR_array_enhancement = SNR_enhancement(N_bias_array,1, laserFluctuations)

#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.subplot(121)
#     plt.semilogx(formatMass(N_bias_array)*1e-3,SNR_array)
#     plt.xlim([1e-2,1e3])
#     plt.xlabel("Bias [pg]")
# #    plt.title(r"SNR vs. $N_{bias}$")
# #    plt.grid(True)
#     plt.subplot(122)
#     plt.semilogx(formatMass(N_bias_array)*1e-3,SNR_array_enhancement)
#     plt.xlim([1e-2,1e3])
#     plt.xlabel("Bias [pg]")
# #    plt.title(r"$SNR_{enhanced}$ (1GNP) vs. $N_{bias}$")
# #    plt.grid(True)
#     plt.tight_layout()
#     #plt.show()
#     print("Optimal bias: " + str((N_bias_array[SNR_array_enhancement == np.max(SNR_array_enhancement)]/constants.N_A)[0]) + " mol")

#     fig.savefig("plots_analysis_standalone/SNR_saturation.pdf")
    
# if plots["1GNP"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Enhanced intensity due to one NGP")
#     N_bias_array = np.logspace(10,14,1e4)
#     c_SAv_array = N_bias_array/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6
#     I_enhanced_array = I_tot(N_bias_array,1,0)-I_bias(N_bias_array)
#     I_enhanced_array2 = I_tot(N_bias_array,1,1e-3)-I_bias(N_bias_array)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.loglog(c_SAv_array,I_enhanced_array,label="No fluctuations")
#     plt.loglog(c_SAv_array,I_enhanced_array2,label="0.1% fluctuations")
#     plt.loglog(c_SAv_array,I_noise(N_bias_array,1,1e-3), label="Noise due to 0.1% fluctuations")
#     plt.xlabel("Bias in amount of scatterers")
#     plt.title("Enhanced intensity due to 1 gold nano-particle")
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     plt.grid(True)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/1GNP.pdf")
    
# if plots["SNR_enhanced_maxima"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Maxima for different amounts of gold nano particles")
#     N_bias_array = np.logspace(6,10,1e4)
    
#     SNR_array = SNR(N_bias_array,0, laserFluctuations)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.semilogx(formatter(N_bias_array)*1e-3,SNR_enhancement(N_bias_array,200, laserFluctuations), color='black', label="200 GNP")
#     plt.semilogx(formatter(N_bias_array)*1e-3,SNR_enhancement(N_bias_array,100, laserFluctuations), color='red', label="100 GNP")
#     plt.semilogx(formatter(N_bias_array)*1e-3,SNR_enhancement(N_bias_array,10, laserFluctuations), color='green', label="10 GNP")
#     plt.semilogx(formatter(N_bias_array)*1e-3,SNR_enhancement(N_bias_array,1, laserFluctuations), color='blue', label="1 GNP")
# #    plt.semilogx(c_SAv_array,SNR_enhancement(N_bias_array,2, laserFluctuations), color='cyan', label="2 GNP")
# #    plt.semilogx(c_SAv_array,SNR_enhancement(N_bias_array,1, laserFluctuations), color='magenta', label="1 GNP")
#     plt.xlabel("Bias [pg]")
#     plt.ylabel(r"$SNR_{enhanced}$")
#     plt.xlim([1e-1,1e2])
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
# #    plt.title("$SNR_{enhanced}$ vs. $N_{bias}$")
# #    plt.grid(True)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/SNR_enhanced_maxima.pdf")
    
# if plots["different_fluctuations"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Maxima for different fluctuations")
#     N_bias_array = np.logspace(6,11,1e4)
    
#     colormap_copy = colormap[::-1]
#     fluctuations_array = np.linspace(1e-3,1e-2,4)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     for laserFluctuations in fluctuations_array:
#         SNR_array = SNR(N_bias_array,0,laserFluctuations)
#         laserFluctuationsPercent = laserFluctuations*100
#         plt.semilogx(formatter(N_bias_array),SNR_array, color=colormap_copy.pop(), label="Fluctuations: " + str(('%.3f') % laserFluctuationsPercent) + "%")
#     plt.xlabel("Bias " + formatter(N_bias_array, 1))
#     plt.ylabel(r"$SNR_{enhanced}$")
# #    plt.xlim([1,1e3])
#     plt.ylim([0,1000])
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
# #    plt.title("$SNR_{enhanced}$ vs. $N_{bias}$ for 200 NGP")
# #    plt.grid(True)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/different_fluctuations.pdf")
    
    
# if plots["different_fluctuations_gold"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Maxima for different fluctuations")
#     N_gold_array = np.logspace(3,6,1e4)
#     colormap_copy = colormap[::-1]
#     fluctuations_array = np.linspace(1e-3,1e-2,4)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     for laserFluctuations in fluctuations_array:
#         SNR_array = SNR(0,N_gold_array,laserFluctuations)
#         laserFluctuationsPercent = laserFluctuations*100
#         plt.semilogx(N_gold_array, SNR_array, color=colormap_copy.pop(), label="Fluctuations: " + str(('%.3f') % laserFluctuationsPercent) + "%")
#     plt.xlabel("Bias in amount of scatterers")
#     plt.ylabel(r"$SNR_{enhanced}$")
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#     plt.ylim([10,60])
# #    plt.title("$SNR_{enhanced}$ vs. $N_{bias}$ for 200 NGP")
# #    plt.grid(True)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/different_fluctuations_gold.pdf")
    
# if plots["different_fluctuations_enhanced"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Maxima for different fluctuations")
#     N_bias_array = np.logspace(6,11,1e4)
    
#     colormap_copy = colormap[::-1]
#     fluctuations_array = np.logspace(-8,-2,6)[2:]
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     for laserFluctuations in fluctuations_array:
#         SNR_array = SNR(N_bias_array,0,laserFluctuations)
#         laserFluctuationsPercent = laserFluctuations*100
#         plt.semilogx(formatMass(N_bias_array)*1e-3,SNR_enhancement(N_bias_array,200,laserFluctuations), color=colormap_copy.pop(), label="Fluctuations: " + str(('%.3f') % laserFluctuationsPercent) + "%")
#     plt.xlabel("Bias [pg]")
#     plt.ylabel(r"$SNR_{enhanced}$")
#     plt.xlim([1e-1,1e4])
#     plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
# #    plt.title("$SNR_{enhanced}$ vs. $N_{bias}$ for 200 NGP")
# #    plt.grid(True)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/different_fluctuations_enhanced0.pdf")
    
    
# if plots["no_fluctuations"]:
#     plotNr += 1
#     print("\nPlot " + str(plotNr) + ": Maxima for no fluctuations")
#     N_bias_array = np.logspace(6,11,1e4)
#     c_SAv_array = N_bias_array/constants.N_A*M_SAv/((pi*(D/2.)**2))*1e6
#     laserFluctuations = 0
#     SNR_array = SNR(N_bias_array,0,laserFluctuations)
    
#     fig = plt.figure(figsize=(17/2.54,8.5/2.54))
#     plt.semilogx(c_SAv_array,SNR_enhancement(N_bias_array,200,laserFluctuations))
#     plt.xlabel("Bias in amount of scatterers")
#     plt.ylabel(r"$SNR_{enhanced}$")
#     plt.title("$SNR_{enhanced}$ vs. $N_{bias}$ for 200 NGP")
# #    plt.grid(True)
#     #plt.show()
#     fig.savefig("plots_analysis_standalone/no_fluctuations.pdf")
