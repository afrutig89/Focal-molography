# -*- coding: utf-8 -*-
"""
In this file, different parameters are analyzed analytically and semi-
analytically. Its purpose is to find an optimal molographic mass for 
a labeled mologram (gold nanoparticles) as well as the influences of 
different noise sources.

@author: Silvio

.. todo::
- validate with simulations / measurements
"""

from numpy import pi, sqrt, inf, cos, sin, exp, conj, log
import numpy as np

import sys
sys.path.append('../')
from waveguide.waveguide import SlabWaveguide           # waveguide calculations
from materials.materials import Particle
import auxiliaries.constants as constants
import dipole.dipole as dipole
from LorentzDrude import particleRadius, refractiveIndex


def analytical(parameters):
    """Calculates the analytical expression for the field in the focal point. This class only works for the analyte so far. (No Multiple binders)"""
    # input wave field
    globals().update(parameters.waveguide)
    k_0 = 2*pi/wavelength

    # mologram parameters
    globals().update(parameters.mologram)

    # screen
    globals().update(parameters.screen)

    # materials
    globals().update(parameters.binders)

    # calculate properties for the particle
    materialParticle = Particle(name = analyte_Name,
                                wavelength = np.array([wavelength]), 
                                radius = analyte_Radius,
                                refractiveIndex = analyte_RefractiveIndex)

    coherentParticles = int((2*analyte_coherentBinding-1)*analyte_N)
    noncoherentParticles = analyte_N-coherentParticles

    # to do still need to change this
    powerperUnitLength = inputPower/(beamWaist) # calculate the power per unit length in the waveguide. 
    effectivePowerperUnitLength = couplingEfficiency * powerperUnitLength

    waveguide = SlabWaveguide(name=film, n_s=n_s, n_f=n_f, 
                          n_c=n_c, d_f=thickness, polarization=polarization, 
                            inputPower=effectivePowerperUnitLength, 
                            wavelength = wavelength,
                            attenuationConstant=propagationLoss, mode=mode)

    refractiveIndex = analyte_RefractiveIndex

    # polarizability
    alpha = dipole.dipolePolarizability(materialParticle.radius, materialParticle.eps_r, n_c**2)
    scatteringCrossSection = dipole.scatteringCrossSection(k_0*n_s, materialParticle.radius, materialParticle.eps_r, n_c**2)
    absorptionCrossSection = dipole.absorptionCrossSection(k_0*n_s, materialParticle.radius, materialParticle.eps_r, n_c**2)
    extinctionCrossSection = dipole.extinctionCrossSection(k_0*n_s, materialParticle.radius, materialParticle.eps_r, n_c**2)

    # excitation field
    z_avg = -np.log(1/(analyte_z_max-analyte_z_min)*(-np.sinh(analyte_z_min)+np.cosh(analyte_z_min)+np.sinh(analyte_z_max)-np.cosh(analyte_z_max)))
    E_exc_mean = waveguide.E_y(wavelength,np.array([0]),np.array([0]),np.array([z_avg])).real

    # dipole moment
    p = dipole.staticDipoleMoment(materialParticle.radius, materialParticle.eps_r, n_c**2, E_exc_mean)

    # dipole radiation
    P_0 = dipole.dipoleRadiation(p, wavelength, n_c)
    V = 4/3.*pi*materialParticle.radius**3

    # minimum dipole intensity
    f = focalPoint
    D = diameter
    theta_max = np.abs(np.arctan((D/2.)/f))
    theta = theta_max
    phi = pi/2.
    r_avg = np.sqrt(f**2 + (D/2.)**3/D)
    I_0 = 3/(8*pi*r_avg**2) * n_s/n_c * P_0 * (sin(phi)**2 * cos(theta)**2 + cos(phi)**2)

    I_NSB = analyte_N*I_0
    I_SB = (2/pi)**2 * coherentParticles**2*I_0
    I_signal = I_NSB + I_SB
    I_molo = I_signal

    SNR = I_signal/(4*I_NSB)


    # parameters from Fattinger's paper
    try: # protein
        dndc = 0.182e-6 # refractive increment for proteins in waver [m3/g]
        t_m = z_max
        n_R = refractiveIndex
        n_0 = n_c
        # waveguide params
        N = waveguide.N
        t_eff = waveguide.calcEffThickness(wavelength)

        # surface absorbed mass density
        M = t_m * (n_R-n_0)/dndc
        m_R = (coherentParticles+noncoherentParticles/2) / constants.N_A * particleMass
        m_G = (noncoherentParticles/2) / constants.N_A * particleMass

        c_R = m_R/(t_m*(pi*(D/2.)**2/2.))
        c_G = m_G/(t_m*(pi*(D/2.)**2/2.))

        n_R = n_0 + dndc*c_R
        n_G = n_0 + dndc*c_G

        # wavelength in the ridges
        Lambda_R = wavelength/n_R 
        Lambda_G = wavelength/n_G 

        A_molo = 2*((n_f**2-N**2)*(n_R**2-n_0**2)**2)/(N*(n_f**2-n_0**2)) * Lambda_R*t_m**2/(wavelength**3*t_eff)
        A_scat = propagationLoss*np.log(10)/20. * 100

        Gamma_Delta = (t_m*c_R)/2. # surface mass-density modulation

    except:
        pass


    return locals()

