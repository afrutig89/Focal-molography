import numpy as np

from PIL import Image
import matplotlib.pylab as plt
import sys
sys.path.append("../")

from scipy.signal import detrend
from scipy import misc
from scipy import signal
from scipy import constants
from pylab import rcParams
from matplotlib.path import Path
import matplotlib.patches as patches
from materials.materials import Material
from auxiliaries.Journal_formats_lib import formatPRX

class SPRWaveguide(object):
	"""This class calculates everything related to surface plasmon waveguides.

	:param wavelength: wavelength of the light [m]
	:param n_c: cover index at this wavelength
	:param w_p: plasma frequency (default is gold)
	:param n_f: complex refractive index of the plasmonic film, if None the plasma frequency and the Drude model will be used to calculate the refractive index of the film. 

	"""

	def __init__(self,wavelength,n_c,w_p = 1.3 * 10**(16),n_f= None):
		
		self.wavelength = wavelength;
		self.w = 2*np.pi*constants.c/self.wavelength
		self.n_c = n_c

		if n_f != None:

			self.n_f = n_f
			self.eps_r = np.real(self.n_f)**2 - np.imag(self.n_f)**2 # real part of the permittivity.
			self.eps_i = 2*np.real(self.n_f)*np.imag(self.n_f) # imaginary part of the permittivity.

		else:
			self.w_p = w_p
			self.calcDrudeMetalEps_r()
			self.n_f = self.eps_r**2
		
		self.calc_N_SPR()
		self.calc_pen_depth_cover()
		self.calc_dN_dnc()


	def calcDrudeMetalEps_r(self):
		"""Calculates the permittivity of the metal according to the Drude Model. 
		
		.. math:: {\\varepsilon _r} = 1 - \\frac{{{\\omega ^2}}}{{{\\omega _p}^2}}
		
		"""
		
		w = self.w
		w_p = self.w_p

		self.eps_r = (1.0 - ((w_p)/(w))**2)
		print "Permittivity Drude metal:"
		print self.eps_r
		return self.eps_r

	def calc_N_SPR(self):
		"""Calculates the effective refractive index of the surface plasmon according to:

		.. math:: N = {\\left[ {{{\\left( {{n_C}} \\right)}^{ - 2}} + {{\\left( {{\\varepsilon _r}} \\right)}^{ - 1}}} \\right]^{ - \\frac{1}{2}}}

		"""

		eps_r = self.eps_r
		n_c = self.n_c

		self.N = ((n_c**(-2)+eps_r**(-1))**(-(0.5)))
		print "effective refractive index waveguide:"
		print self.N

		return self.N

	def calc_pen_depth_cover(self):
		"""Calculates the penetration depth into the cover medium according to:

		.. math:: \\Delta {z_c} = \\left[ {\\frac{\\lambda }{{2 \\cdot \\pi  \\cdot {n_c}N}}} \\right]{\\left( { - {\\varepsilon _r}} \\right)^{\\frac{1}{2}}}
		

		"""

		self.delta_zc = self.wavelength/(2*np.pi*self.n_c*self.N)*(-self.eps_r)**(0.5)
		print "Penetration depth (nm):"
		print self.delta_zc*10**9

		return self.delta_zc

	def calc_dN_dnc(self):
		"""
		Sensitivity of the effective refractive index to cover index changes.

		.. math:: \\frac{{\\partial N}}{{\\partial {n_C}}} = {\\left( {\\frac{N}{{{n_c}}}} \\right)^3}

		"""

		self.dN_dnc = (self.N/self.n_c)**3
		print "dN/dnc:"
		print self.dN_dnc
		return self.dN_dnc

	def returnWaveNumber(self):

		self.beta = self.N*self.w/constants.c

		return self.beta
		
	def plot_disp_relation(self):

		assert len(self.w) > 1, 'Only works for Drude metal and if w is an array'

		plt.plot(self.N*self.w/constants.c,self.w,label="SPP dispersion")
		plt.xlim([0,1*10**8])
	
	def plot_light_line(self,n):

		assert len(self.w) > 1, 'Only works for Drude metal and if w is an array'

		plt.plot(n*self.w/constants.c,self.w, label="light line n = " +str(n))
		plt.ylim([0,max(self.w)])

	def plot_light_cone(self,n):

		assert len(self.w) > 1, 'Only works for Drude metal and if w is an array'

		ax = plt.gca()

		verts = [
			(0., 0.), # left, bottom
			(max(n*self.w/constants.c), max(self.w)), # left, top
			(0, max(self.w)), # right, top
			(0., 0.), # left, bottom
		]

		codes = [Path.MOVETO,
			Path.LINETO,
			Path.LINETO,

			Path.CLOSEPOLY,
		]

		path = Path(verts, codes)

		patch = patches.PathPatch(path, facecolor='green',alpha = 0.3, lw=2)
		ax.add_patch(patch)



if __name__ == '__main__':

	wavelength = 632.8 # wavelength of the readout laser (nm)
	n_c = 1.33 # cover refractive index


	test = SPRWaveguide(wavelength,n_c)


	wl = np.linspace(144,20000,10000)*1e-9

	# #-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	# plot for the dispersion relation in water

	test = SPRWaveguide(wl,n_c)

	test.returnWaveNumber()
	formatPRX()
	plt.figure()

	test.plot_disp_relation()
	test.plot_light_line(1.33)
	# test.plot_light_cone(1.33)

	plt.legend(loc='best')

	ax = plt.gca()
	ax.text(0.5*10**8,1*10**16,r'$\omega = \frac{k\cdot c}{n}  $')
	ax.text(0.5*10**8,4*10**15,r'$\omega  = k \cdot {c }\cdot \sqrt{\frac{{\varepsilon _m} + {\varepsilon _d}}{{\varepsilon _m}{\varepsilon _d}}}$')

	plt.xlabel(r"$k\quad [1/m]$")
	plt.ylabel(r"$\omega \quad[Hz]$")



	plt.savefig("SPP_water_inc_water.png",format='png')

	#-----------------------------------------------------------------------------------------------
	#-----------------------------------------------------------------------------------------------
	# Figure: Plot for the dispersion relation from  the glass 

	test = SPRWaveguide(wl,n_c)
	plt.figure()

	test.plot_disp_relation()
	test.plot_light_line(1.33)
	test.plot_light_line(1.5)

	plt.legend(loc='best')

	ax = plt.gca()
	ax.text(0.1*10**8,0.8*10**16,r'$\omega = \frac{k\cdot c}{1.33}  $')
	ax.text(0.5*10**8,4 *10**15,r'$\omega  = k \cdot {c }\cdot \sqrt{\frac{{\varepsilon _m} + {\varepsilon _d}}{{\varepsilon _m}{\varepsilon _d}}}$')
	ax.text(0.6*10**8,1*10**16,r'$\omega = \frac{k\cdot c}{1.5}  $')


	plt.xlabel(r"$k\quad [1/m]$")
	plt.ylabel(r"$\omega \quad[Hz]$")
	plt.savefig("SPP_water_inc_water_prism.png",format='png')


	# plot of the light line
	plt.figure()
	test.plot_light_line(1)

	name = "Light_line_air"
	plt.legend(loc='best')
	ax = plt.gca()
	ax.text(0.1*10**8,0.8*10**16,r'$\omega = k\cdot c  $')


	plt.xlabel(r"$k\quad [1/m]$")
	plt.ylabel(r"$\omega \quad[Hz]$")


	plt.savefig(name + ".png",format='png')


