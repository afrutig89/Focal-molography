import numpy as np
import pandas
import astropy
from PIL import Image
import matplotlib.pylab as plt
import sys
sys.path.append("../")

import itertools
from scipy.signal import detrend
from scipy import misc
from scipy import signal
from scipy import constants
from pylab import rcParams
from matplotlib.path import Path
import matplotlib.patches as patches
from materials.materials import Material
from auxiliaries.Journal_formats_lib import formatPRX
#from waveguideSPR import SPRWaveguide
from SPR.waveguideSPR import SPRWaveguide
from auxiliaries.constants import dn_dcSI

class SPRAnalytical(SPRWaveguide):
	"""Class for the calculation of the Sensor response of an SPR sensor by means of analytical equations."""
	
	def __init__(self,wavelength,n_c,proteinAdlayer,w_p = 1.3 * 10**(16),n_f= None):
		
		SPRWaveguide.__init__(self,wavelength,n_c,w_p=w_p,n_f=n_f)


		self.Gamma = proteinAdlayer.surfaceMassFromProteinNumber()*1e-15/(1e-6)

		print 
		print self.Gamma

		self.h = self.calc_h()

	def calc_h(self,n_m = 0):
		"""Proportionality factor, is close to 2 for most cases, if the exact calculation is used the refractive index of the adlayer needs to be provided.
		
		.. math:: h = \\left( {\\frac{{{n_c}}}{{{n_m}}}} \\right)\\left( {1 + \\frac{{{n_c}}}{{{n_m}}}} \\right)
		"""
		if n_m:
			self.h = self.n_c/n_m*(1+self.n_c/n_m)
		else:
			self.h = 2.
		print self.h
		return self.h

	def calc_deltaN(self):
		"""
		.. math:: \\Delta N = \\frac{{4 \\cdot \\pi  \\cdot {n_c}N}}{{\\lambda {{\\left( { - {\\varepsilon _r}} \\right)}^{\\frac{1}{2}}}}}\\frac{{\\partial N}}{{\\partial {n_c}}}\\frac{{dn}}{{dc}}\\Delta \\Gamma 
		
		.. math:: \\Delta N = {\\left( {\\Delta {z_c}} \\right)^{ - 1}}\\frac{{\\partial N}}{{\\partial {n_c}}}\\frac{{dn}}{{dc}}h\\Delta \\Gamma

		"""
		deltaN = self.delta_zc**(-1)*self.dN_dnc*dn_dcSI*self.h*self.Gamma
		print "deltaN:"
		print deltaN
		return deltaN

	def penetraction_depth(self):

		return self.delta_zc

	def calc_deltaTheta(self):
		# calculates the change in SPR angle, when a certain surface mass density of protein is present on the chip. 
		deltaTheta = np.arcsin(calc_deltaN()/self.n)*360/(2*np.pi)
		
		print "Change in Resonance angle of SPR (deg)"
		print deltaTheta
		return deltaTheta

	def ReflectivityChangePerEffIndexChange(self):
		"""
		Calculates the sensitivity with respect to refractive index changes S_N
		.. math:: \\frac{{dR}}{{dN}} = \\frac{{4\\sqrt 3 {\\varepsilon _r}^2}}{{9{n_c}^3{\\varepsilon _i}}}
		
		"""
		print self.eps_r
		print self.eps_i

		return 4*np.sqrt(3.)*self.eps_r**2/(9*self.n_c**3*self.eps_i)

	def S_Gamma(self):
		"""Sensitivity with respect to surface mass density changes."""

		print self.N

		return 16*np.sqrt(3)*self.eps_r**2/(9*self.eps_i*np.sqrt(-self.eps_r))*np.pi*self.N/(self.wavelength*self.n_c**2)*dn_dcSI


if __name__ == '__main__':

	from binder.Adlayer import ProteinAdlayer
	
	
	protein_layer = ProteinAdlayer(52.8e3,Gamma=0.1,delta=82*1e-9)

	eps = -16. + 1j*1.1
	eps_r = -16.
	eps_i = 1.1

	n = 1/np.sqrt(2.)*np.sqrt(eps_r + np.sqrt(eps_r**2+eps_i**2))
	kappa = 1/np.sqrt(2.)*np.sqrt(-eps_r + np.sqrt(eps_r**2+eps_i**2))

	wavelength = 700e-9 # wavelength of the readout laser [m]
	n_c = 1.33



	spr = SPRAnalytical(wavelength,n_c,protein_layer,n_f = (n + 1j*kappa))
	print(spr.penetraction_depth())
	print spr.calc_deltaN()
	print spr.ReflectivityChangePerEffIndexChange()
	print "Reflectivity Change in [%]"
	print spr.ReflectivityChangePerEffIndexChange()*spr.calc_deltaN()*100.
	print "Sensitivity of Surface Plasmon resonance S_Gamma:"
	print spr.S_Gamma()



