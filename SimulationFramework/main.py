
# path = 'path to Simulation Framework'
import sys
from os import sys,path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))



import datetime, time
import numpy as np
from auxiliaries.pareto import paretoList
from simulations import FieldSimulation
from SharedLibraries.Database.dataBaseOperations import loadDB
from auxiliaries.screen import defineScreen
from variables import FieldSimulationInputs,FieldSimulationResults, CameraSimulationInputs
from camerasimulation.CameraSensorSimulation import CameraSensorSimulation
from camerasimulation.CameraSensorModels import CameraSensorParameters
import logging



logging.basicConfig(filename='example.log',level=logging.DEBUG)

# load a simulation results file.   
# self.results = loadDB(self.camSimInput.getValues()['databaseFile'],'results',condition='WHERE ID==' + str(self.camSimInput.getValues()['ID_results']))


diameters = [117.3e-6,131.5e-6,148.5e-6,168.7e-6,192.8e-6,221.4e-6,255.4e-6,295.70e-6,343.4e-6,400.0e-6]

for i in diameters:

    simInputs = FieldSimulationInputs()

    simInputs.loadDB('database/default.db')

    mologramParValues = {
        'diameter':i,  # maximum cell size in [m]
    }

    simInputs.setValues(mologramParValues)

    # the dictionary of the plots that are to be produced.
    plots = {
        'Excitation_intensity':'Excitation_intensity_NRT_5000_1',  # plot of the excitation field at the scatterer position.
        'Scattered_intensity':'Scattered_intensity_NRT_5000_1',  # plot of the scattered field at the focal plane.
        'Scattered_intensityBackground':'Scattered_intensity_Background_NRT_5000_1',  # plot of the scattered field at the focal plane.
        'scattererPlot':'scatterPlot_NRT_5000_1',  # plot of scatterer distribution on the mologram.
        'scattererPlotLarge':'scattererPlotLarge', # plot with larger scatterers to visualize the lines better
        }

    # the dictionary of the results that are to be stored in the database. If the key is specified this value is stored the value in the dict does not matter.

    saveDict = {
        'ID':0,
        'I_sca':0,
        'I_bg':0,
        'I_molo':0,
        'I_signal':0,
        'screen':0,
        'runningTime':0
    }


    fieldsim = FieldSimulation(simInputs,plots=plots,saveDict=saveDict)

    fieldsim.startSimulation()

# for i in range(0,50):

#     generalSettings =  {
#             'CorrSimulation':i+1, # if not zero, this corresponds to the experiment ID from which the correlated simulation should be loaded.
#     }

#     plots = {
#         'Excitation_intensity':'Excitation_intensity_NRT_5000_2',  # plot of the excitation field at the scatterer position.
#         'Scattered_intensity':'Scattered_intensity_NRT_5000_2',  # plot of the scattered field at the focal plane.
#         'Scattered_intensityBackground':'Scattered_intensity_Background_NRT_5000_2',  # plot of the scattered field at the focal plane.
#         'scattererPlot':'scatterPlot_NRT_5000_2',  # plot of scatterer distribution on the mologram.
#         }

#     simInputs.setValues(generalSettings)

#     fieldsim = FieldSimulation(simInputs)

#     #fieldsim = FieldSimulation(simInputs,plots=plots,saveDict=saveDict)


#     fieldsim.startSimulation()

# #simulation inputs for a camerasensorsimulation object.


# exp_time = np.linspace(0.1,1.5,10)

# for i in exp_time:
#     name = 'Cam_scattered_intensity' + str(i) +"_Exp_time"
#     name_BG = 'CamScatteredIntensityBG' + str(i) +"_Exp_time"

#     plots = {
#         'scatteredIntensityCam':name,  # plot of the excitation field at the scatterer position.
#         'scatteredIntensityBackgroundCam':name_BG
#         }

#     # plots = ''
#     camSimInputValues = {
#         'camModel':'MT9P031', # the camera that was used to acquire the image
#         'exposureTime':i, # exposure time of the camera for a single aquisition in sec [s]
#         'magnification':20.0, # magnification of the imaging system used
#         'databaseFile':'simulations.db', # database file
#         'ID_results':1
#     }

#     camSimInputs = CameraSimulationInputs()

#     camSimInputs.setValues(camSimInputValues)

#     CameraSensorSimulation(camSimInputs,plots=plots)


# plots = {
#         'scatteredIntensityCam':'Signal_1596ms',  # plot of the excitation field at the scatterer position.
#         'scatteredIntensityBackgroundCam':'Background_1596ms'
#         }

# camSimInputValues = {
#     'camModel':'MT9P031', # the camera that was used to acquire the image
#     'exposureTime':1.596, # exposure time of the camera for a single aquisition in sec [s]
#     'magnification':20.0, # magnification of the imaging system used
#     'databaseFile':'simulations.db', # database file
#     'ID_results':1
# }

# cameraSensor = CameraSensorParameters()

# cameraSensor.model = "custom"
# cameraSensor.sensorsize = ''
# cameraSensor.framerate = ''
# cameraSensor.pixelsize = ''
# cameraSensor.QE = ''
# cameraSensor.rNoise = ''
# cameraSensor.saturationCap = ''
# cameraSensor.camGain = ''

# camSimInputs = CameraSimulationInputs()

# camSimInputs.setValues(camSimInputValues)

# CameraSensorSimulation(camSimInputs,plots=plots,cameraSensor = cameraSensor)










# for N_particles, coherentBinding in paretoList(N_particles_array, coherentBinding_array):

#     parameters = SimulationInputValues()
#     parameters.waveguide = waveguidePar
#     parameters.mologram = mologramPar
#     parameters.mologram['N_particles'] = N_particles
#     parameters.mologram['coherentBinding'] = coherentBinding
#     parameters.particle = particlePar
#     parameters.screen = screenPar
#     parameters.settings = generalSettingsPar

# #    ID = loadDB(generalSettingsPar['databaseFile'], 'log', selection = 'ID', condition = 'ORDER BY ID DESC LIMIT 1')
# #    parameters.settings['ID'] = ID
# #    createDB(generalSettingsPar['databaseFile'], 'results', simulation_dict)

# #    parameters.settingsSave = calParameters
# #    parameters.settings['start'] = time.time()

# #    saveDB(generalSettingsPar['databaseFile'], 'log', parameters.return_all_parameters())

#     run_simulation(parameters)


#     if True: # saveFigure:
#         fig, ax = plt.subplots(1, 2)

#         screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(screen, focalPoint)

#         extent = (np.min(screen_1),np.max(screen_1),np.min(screen_2),np.max(screen_2))
#         ax[0] = plt.imshow(I_sca,
#                             cmap = plt.cm.inferno,
#                             origin = 'upper',
#                             extent = [border*1e6 for border in extent],
#                             norm = colors.PowerNorm(gamma=1/2.))

#         # print(dir(ax[0]))
#         # ax[0].set_xlabel(axis1)
#         # ax[0].set_ylabel(axis2)
#         # ax[0].set_axes().set_aspect('equal')

#         # ax = plt.gca()
#         # divider = make_axes_locatable(ax)
#         # cax1 = divider.append_axes("right", size="5%", pad=0.1)
#         # cbar = plt.colorbar(ax[0], cax = cax1, ticks=np.linspace(0,np.max(parameter),4), format='%.0e')
#         # cbar.ax.tick_params(labelsize=10)

#         # ridges
#         bound_particles = bind_no_bind < coherentBinding
#         ax[1].scatter(x_sca[bound_particles]*1e6, y_sca[bound_particles]*1e6, color='green')
#         # grooves
#         ax[1].scatter(x_sca[bound_particles == False]*1e6, y_sca[bound_particles == False]*1e6, color='red')

#         ax[1].axis('equal')
#         ax[1].set_xlim(-mologram.radius*1e6, mologram.radius*1e6)
#         ax[1].set_ylim(-mologram.radius*1e6, mologram.radius*1e6)

#         fig.savefig('{}.png'.format(i))
