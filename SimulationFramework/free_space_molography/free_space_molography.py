import sys
sys.path.append('../')
sys.path.append('../../')
from phasemask.Diffraction_grating import DiffractionGrating
from binder.RefrIndexOfLayerOfScatterers import RefrIndexOfLayerOfScatterers
import matplotlib.pyplot as plt
import numpy as np

class FreeSpaceDiffractionSensor(DiffractionGrating):
	"""This class allows for the diffraction efficiency calculations of a free space diffractometric sensor.

	:param M_protein [Da]: Molecular mass of the protein in Da, default is 52.8e3 Da for SAv. 
	:param n_c: Refractive index of the solvent.

	:param Delta_Gamma [pg/mm^2]: mass density modulation between grooves and ridges.

	:param n_s: refractive index of the substrate material of the diffractometric sensor the sensor is illuminated from the backside.
	:param Ill_wl: illumination wavelength [m]
	:param Pol: Polarization of the incident light (TE,TM)
	:param Lambda: period of the diffraction grating [m]
	:param t_m [m]: thickness of the layer, where the proteins are distributed, 
		default is 5 nm (penetration depth of the field).
	:param rho_c [g/ml]: Mass density of the solvent, default 1.0028 for water
	:param Inc_Ang = 0: Incidence angle of the incident plane wave, default is 0 [deg]
	
	"""
	
	def __init__(self,n_c,Delta_Gamma,n_s,Lambda,Ill_wl = 632.8e-9,Pol='TE',M_protein=52.8,t_m=1e-9,Inc_Ang = 0,rho_c = 1.0028):

		protein_layer = RefrIndexOfLayerOfScatterers(
		M_protein, 
		Delta_Gamma=Delta_Gamma,
		delta=t_m)

		self.n_m = protein_layer.refIndexProteinLayer()

		DiffractionGrating.__init__(self,n_c,self.n_m,n_s,Ill_wl,t_m,Lambda,Dutycycle=0.5,Inc_Ang=Inc_Ang,NN=10)

		self.n_c =n_c
		self.t_m =t_m


	def PDiffPInFreeSpaceAnalytical(self):
		"""Analytical equation for the free space coupling between two modes 
		through periodically arranged biological matter in space.
		
		Still needs to be updated.
		.. math:: \\eta = \\frac{P_{diff}}{P_{in}}=\\frac{(n_m^2-n_c^2)^2}{n_c^2}\\frac{t_m^2}{\\cos(\\theta)^2-cos(\\theta)\\frac{\\lambda}{\\Lambda n_c}}\\frac{\\pi^2}{4\\lambda^2}
		
		"""
		Inc_Ang = self.Inc_Ang/360.*2*np.pi

		phi = self.Ill_wl/(self.Lambda*self.n_c)

		kappa = 1/4.*2*np.pi/self.Ill_wl*(self.n_m**2-self.n_c**2)/self.n_c

		c_s_c_r = np.cos(Inc_Ang)*(np.cos(Inc_Ang)**2 \
				+ 2*phi*np.sin(Inc_Ang)
				- phi**2)**(0.5)

		c_s_c_r = np.cos(Inc_Ang)**2 # Kogelnik 

		return kappa**2*self.t_m**2/c_s_c_r


	def PDiffPInFreeSpaceNumerical(self,reflection=False):
		"""
		Does the conversion from the canonical to the sinusoidal form by multiplying with the Factor: 

		.. math:: \\frac{\\pi^2}{6}\\cdot
		"""

		return self.performIndividualSimulation(reflection=reflection)[1]/np.pi**2*6


if __name__=="__main__":

	n_c = 1.33
	N = 1.814
	n_f = 2.114
	Lambda = 500
	wl = 632.8

	phi = wl/(Lambda*n_c)

	C_FF = 1/(n_c*np.sqrt(1.- phi**2))
	C_FG = 1/(N*n_c)\
	*np.pi*(n_f**2-N**2)/(n_f**2-n_c**2)\
	*1/(1-N**2/n_c**2+2*N/n_c*phi-phi**2)**(1/2.)
	C_GG =1/N**2*((n_f**2-N**2)/(n_f**2-n_c**2))**2
	print "C_FF:"
	print C_FF
	print "C_FG:"
	print C_FG
	print "C_GG:"
	print C_GG

	print "hello"

	efficiencies_numerical = []
	efficiencies_analytical = []
	efficiencies_tirf = []

	for i in np.linspace(10,2000,100):
		FSDS = FreeSpaceDiffractionSensor(1.33,i,1.5,600e-9,Inc_Ang=0)
		efficiencies_numerical.append(FSDS.PDiffPInFreeSpaceNumerical())
		efficiencies_analytical.append(FSDS.PDiffPInFreeSpaceAnalytical())
		efficiencies_tirf.append(FSDS.PDiffPInFreeSpaceNumerical(reflection=True))

	plt.plot(np.linspace(10,2000,100),np.asarray(efficiencies_numerical),label='test')
	plt.plot(np.linspace(10,2000,100),np.asarray(efficiencies_analytical),label='test1')
	plt.plot(np.linspace(10,2000,100),np.asarray(efficiencies_tirf),label='test1')
	plt.legend()
	print np.asarray(efficiencies_analytical)/np.asarray(efficiencies_numerical)
	plt.show()