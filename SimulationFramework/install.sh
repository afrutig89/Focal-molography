#!/bin/bash


###############################
## Python 2.7 for simulation ##
###############################

# go to home
cd

# install git and clone folder
sudo apt install git
git clone https://git.ee.ethz.ch/afrutig/Focal-molography


# install pip
sudo apt install python-pip

# install modules
pip install numpy
pip install scipy
pip install matplotlib
sudo apt-get install python-yaml
sudo apt-get install python-tk
sudo apt-get install python-skimage
pip install -U scikit-image

apt-cache search pyqt
sudo apt-get install python-qt4

cd Downloads/
wget http://www.pyqtgraph.org/downloads/0.10.0/pyqtgraph-0.10.0-deb/python-pyqtgraph_0.10.0-1_all.deb
sudo dpkg -i python-pyqtgraph_0.10.0-1_all.deb 

pip install numba

python Focal-molography/Simulation\ Framework/mainGUI.py
