
# -*- coding: utf-8 -*-
"""

This script allows to calculate the number of molecules on various types of sensor geometries as a function of time and concentration


:Author: Andreas Frutiger, Yves Blickenstorfer
:Revised: 18-Jan-2017

"""


import numpy as np
import matplotlib.pylab as plt
import sys
import numpy.matlib
from scipy.special import jv,yn
from scipy.integrate import quad
import time
import datetime

from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

import auxiliaries.constants as constants
import auxiliaries.pareto as pareto
from Adlayer import ProteinAdlayer

def numberOfMoleculesAdsorbed(concentration_0, t, a, geometry, L = 0, diffusionConstant = 130*10**(-12)):
	"""
	Returns the number of adsorbed molecules after a certain time in a solution with a certain analythe concetration for a sensor with a certain geometry

	:param concentration_0: initial solution concentration [M]
	:type concentration_0: float
	:param t: response time [s]
	:type t: float
	:param a: radius of the stucture [m]
	:type a: float
	:param geometry: geometry of the sensor: hemisphere, disk, hemicylinder, cylinder
	:type geometry: string
	:param L: length of the cylinder (only needed for cylinder or hemixylinder)
	:type L: float
	:param diffusionConstant: diffusion constant by default for SAv
	:type diffusionConstant: float = 130*10**(-12)

	:Reference: Diffusion Constant:  Diffusion Constant of SAv from: Zhang, Liangfang, Ligand-receptor Binding on Nanoparticle-Stabilized Liposome Surfaces


	:Hemisphere:

	.. math:: N(t) = 2\\pi {N_A}{c_0}D\\left( {at + {a^2}\\sqrt {{t \\over {\\pi D}}} } \\right)
	:Reference: Paul E.Detection Limits for Nanoscale Biosensors, eqn 2


	:Disk:
	.. math:: N(t) = 4D{N_A}{c_0}at
	:Reference: Paul E.Detection Limits for Nanoscale Biosensors, eqn 4


	:Hemicylinder:
	.. math:: N(t) = {{4l{N_A}{c_0}} \\over \\pi }\\int\\limits_0^\\infty  {{{1 - {e^{ - D{u^2}t}}} \\over {{{\\rm{J}}_0}{{(au)}^2} + {{\\rm{Y}}_0}{{(au)}^2}}}{{du} \\over {{u^3}}}}
	:Reference: Paul E.Detection Limits for Nanoscale Biosensors, eqn 5


	:Cylinder:
	.. math:: N(t) = {{8l{N_A}{c_0}} \\over \\pi }\\int\\limits_0^\\infty  {{{1 - {e^{ - D{u^2}t}}} \\over {{{\\rm{J}}_0}{{(au)}^2} + {{\\rm{Y}}_0}{{(au)}^2}}}{{du} \\over {{u^3}}}}

	"""

	concentration_0 = 1000*concentration_0 #to ge from mol/L to mol / m^3




	if geometry == 'hemisphere':

		return 2*np.pi*constants.N_A*concentration_0*diffusionConstant*(a*t+2*a**2*np.sqrt(t/(np.pi*diffusionConstant)))




	elif geometry == 'disk':

		return 4*diffusionConstant*constants.N_A*concentration_0*a*t


	elif geometry == 'hemicylinder':

		if L == 0:
			print 'no length defined'

		integral = lambda u: (1-np.exp(-diffusionConstant*u**2*t))\
								/((jv(0,a*u)**2+yn(0,a*u)**2)*u**3)

		result = quad(integral,0,np.inf)

		return 4*L*constants.N_A*concentration_0/np.pi*result[0]




	elif geometry == 'cylinder':

		if L == 0:
			print 'no length defined'

		integral = lambda u: (1-np.exp(-diffusionConstant*u**2*t))\
							/((jv(0,a*u)**2+yn(0,a*u)**2)*u**3)

		result = quad(integral,0,np.inf)

		return 8*L*constants.N_A*concentration_0/np.pi*result[0]




	else:
		print 'no valid geometry'

		return nan

def calcMassDensity(N,M_w,a,geometry,L=0):
	"""Converts a number of molecules for a certain geometry to a
	surface Mass density in pg/mm^2.

	:param N: Number of Proteins on the sensor
	:type N: float
	:param M_w: Molecular Weight of Protein [g/mol]
	:type M_w: float
	:param a: radius of the sensor
	:type a: float
	:param geometry: geometry of the sensor: hemisphere, disk, hemicylinder, cylinder
	:type geometry: string
	:param L: length of the cylinder (only needed for cylinder or hemixylinder)
	:type L: float

	"""



	if geometry == 'hemisphere':
		area = 4 * np.pi * a**2 / 2

	elif geometry == 'disk':
		area = np.pi * a**2

	elif geometry == 'hemicylinder':
		if L == 0:
			print 'no length entered'
		area = 2*np.pi * L * a

	elif geometry == 'cylinder':
		if L ==0:
			print 'no length entered'
		area = 2 * np.pi * L * a

	else:
		print 'no valid geometry'
		area = 0

	surfaceDensity = N / area
	surfaceMassDensity = surfaceDensity * M_w / constants.N_A * 1e6 #1e6 is used to get from g/m^2 to pg/mm^2

	return surfaceMassDensity

def diffusionCoefficientStokesEinstein(d, eta= 8.90*1e-4, T=293.15):
    """
    Calculates the diffusion coefficient of small spherical
    particles suspended in liquids. (Stokes-Einstein equation)

    :param d: particle diameter
    :param eta: dynamic viscosity [Pa s] the default value is the viscosity of water at 25°C. 
    :param T: absolute temperature (Kelvin)
    :returns: diffusion coefficient

    .. math::

        D = \\frac{kT}{3\\pi\\eta d}
    """
    k = constants.k     # Boltzmann constant
    D = k*T/(3*np.pi*eta*d)

    return D


if __name__ == "__main__":

    concentration = 1e-9
    radiusDisk = 200e-6
    incubationTime = 300 # [s]
    diffusionConstant = diffusionCoefficientStokesEinstein(42e-9, 1e-3) # diffusion coefficient of a particle with a certain size.

    diffusionConstant = 130*10**(-12) # diffusion coefficient of SAv in water

    N = numberOfMoleculesAdsorbed(concentration,
                                    incubationTime,
                                    radiusDisk,
                                    'disk',diffusionConstant=diffusionConstant)*0.27 # 27% modulation (not accurate yet)

    protein_adlayer = ProteinAdlayer(N_scat = N)

    print "fg/mm^2 adsorbed on the chip after " + str(incubationTime/60.) + " mins:"
    print protein_adlayer.Gamma*1000.




	# font = {'family' : 'Arial',
	# 'weight' : 'normal',
	# 'size'   : 12
	# }
	# params = {'legend.fontsize':   8,
	# 	  'xtick.labelsize' : 10,
	# 	  'ytick.labelsize' : 10
	# 			}
	# plt.rcParams.update(params)
	# plt.rc('font', **font)

	# imageFormat = 'pdf'
	# dpi = 600
	# size = 7.5
	# inch = 2.54
	# size_inch = size / inch



	# par = {
	#     'ID':'integer primary key AUTOINCREMENT',
	#     'vectorlength': 'real',
	# 	'radiusDisk': 'real',
	# 	'incubationTime': 'real',
	# 	'proteinConcentration': 'real',
	# 	'radiusFiber': 'real',
	# 	'gratingLength': 'real',
	# 	'diffusionConstant': 'real',
	# 	'surfaceMassDensity_disk': 'real',
	# 	'surfaceMassDensity_fiber': 'real',
	# 	'date' : 'text',
	# 	'startTime' : 'text'

	# }

	# date = str(datetime.date.today())
	# startTime = time.strftime("%H:%M:%S")

	# name = 'Diffusion'
	# directory = '.'




	# save = 'yes'
	# if save == 'yes':
	# 	createDB(directory + name + '.db', 'Data', par)



	# radiusDisk_array = 200*10**(-6) # meters
	# incubationTime_array =  np.linspace(1,3600,100)  #np.linspace(0,3600,vectorlength) # seconds
	# proteinConcentration_array = np.linspace(0.1e-9,100*1e-9,100) #50*10**(-9)*10**3 # protein concentration in the solution 10 nmol/dm^3 -->*1000 10 umol/m^3
	# molarMassProtein_array = 52800 # g/mol
	# radiusFiber_array = np.array([0.07e-6])
	# gratingLength_array = np.array([100e-6])
	# diffusionConstant_array = 130* 1e-12 #130*10**(-12)





	# parameters = pareto.paretoList(radiusDisk_array ,incubationTime_array,\
	# 							proteinConcentration_array,molarMassProtein_array,\
	# 							radiusFiber_array,gratingLength_array,\
	# 							diffusionConstant_array)



	# surfaceMassDensity_disk = []
	# surfaceMassDensity_fiber = []

	# for radiusDisk,incubationTime,proteinConcentration, \
	# 	molarMassProtein,radiusFiber,gratingLength,diffusionConstant\
	# 		 in parameters:


	# 	N_disk = numberOfMoleculesAdsorbed(proteinConcentration,incubationTime,\
	# 											radiusDisk,'disk',\
	# 											diffusionConstant = diffusionConstant)

	# 	surfaceMassDensity_disk = calcMassDensity(N_disk,molarMassProtein\
	# 												,radiusDisk,'disk',L=0)


	# 	N_fiber = numberOfMoleculesAdsorbed(proteinConcentration,incubationTime,\
	# 											radiusFiber,'cylinder',\
	# 											L=gratingLength, \
	# 											diffusionConstant = diffusionConstant)


	# 	surfaceMassDensity_fiber = calcMassDensity(N_fiber,molarMassProtein\
	# 											,radiusFiber,'cylinder',L = gratingLength)


	# 	if save == 'yes':
	# 		saveDB(directory + name + '.db', 'Data', locals())
