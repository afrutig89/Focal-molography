"""
This modules converts and scales the units from the number of particles 
to suface mass modulation, mass or concentration. The functions return 
the quantity as well as the label, such that they are useful when 
plotting (no case-study needed).

@ author:   Silvio
@ created:  July 16
"""


import numpy as np
from numpy import pi
import auxiliaries.constants as constants

M_SAv = 52.8e3    # antibody mass in Dalton [g/mol]

def formatSurfaceMassModulation(N, D=400e-6, M = M_SAv, labeling=0):
    """
    returns the surface mass modulation in pg/mm2

    :param N: number of particles
    :param D: diameter of the (circular) mologram (default is 400 um)
    :param labeling: True if one is interested in the label (string)
    :returns: the surface mass modulation in pg/mm2 or the label "pg/mm2"
    
    """

    if labeling==1: return '[pg/mm2]'
    else: return N/constants.N_A * M/((pi*(D/2.)**2)) *1e6

def formatMass(N, M = M_SAv, labeling=0):
    """
    returns the mass in fg as a function of the number of particles

    :param N: number of particles
    :param labeling: True if one is interested in the label (string)
    :returns: the mass in fg or the label "pg/mm2"
    """
    if labeling==1: return '[fg]'
    else: return N/constants.N_A*M*1e15

def formatConcentration(N, labeling=0):
    """
    returns the concentration in mol as a function of the number of particles

    :param N: number of particles
    :param labeling: True if one is interested in the label (string)
    :returns: the concentration in mol or the label "pg/mm2"
    """
    if labeling==1: return '[mol]'
    else: return N/constants.N_A
    
def formatScatterers(N, labeling=0):
    """
    returns the number of scatterers

    :param N: number of particles
    :param labeling: True if one is interested in the label (string)
    :returns: the number of scatterers or the label "pg/mm2"
    """
    if labeling==1: return ''
    else: return N


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FuncFormatter

    import sys
    sys.path.append('../')
    from binder.statisticalLOD import *

    # definition of a sigmoidal function with a certain deviation (see performance/statisticalLOD)
    n_measurements = 7
    n_repetitions = 20
    n_scatterers = np.logspace(1,5,n_measurements)
    A,B,C,D = -0.0002, 6.805, 3.776, 0.6989
    y_true = logistic4(np.log10(n_scatterers+2), A, B, C, D)
    y_meas = np.matlib.repmat(y_true,n_repetitions,1) + 0.01*npr.randn(n_repetitions,n_measurements)
    negCtrl = np.array(0.02*npr.randn(n_repetitions,1)).T

    plt.semilogx(formatMass(n_scatterers),y_true)
    plt.xlabel(formatMass(n_scatterers, labeling=1))
    plt.show()
