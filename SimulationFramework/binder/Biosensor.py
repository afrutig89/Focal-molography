"""
This is a collection of binding models that allow to predict the number of analyte molecules, depending on affinity constants and concentrations.

:Created:		11-Jul-2017
:Author:		Andreas Frutiger
:Revised:		11-Jul-2017 by Andreas Frutiger
:References:
"""

import numpy as np
import sys
sys.path.append('../')
from Adlayer import ProteinAdlayer
from mologram.mologram import Mologram
from auxiliaries.constants import N_A
from phasemask.phase_mask_illumination_system import calc_max_activation_modulation


# calls for the strategy pattern...
# Fancy trial to make this super general, which I do not need currently.
# class BioSensorSurface(object):

# 	def __init__(self,area,receptordensity):
#
# 		self.area = area

# class Binder(object):

# 	def __init__(self,concentration):
# 		self.concentration = concentration

# class BinderBioSensorSurfaceInteraction(object):

# 	def __init__(self,binder,bioSensorSurface):

# 		self.binder = binder
# 		self.bioSensorSurface = bioSensorSurface
#		self.K_D = K_D
#		self.receptordensity = receptordensity

# 	def NumBoundToSurface(self):


class BinderSurfaceInteraction(object):
	"""
		Calculates the number of specific and nonspecifically bound molecules on a sensor surface for given receptor densities, concentrations and
		dissociation constants. The class is not general and only works for a binary system!
		:cite:`Squires2008-pt`

		:param SurfaceArea: Sensor Surface area [m^2]
		:param [A]: Analyte A concentration
		:param [NSB]: Analyte NSB concentration
		:param [K_DA]: Dissociation constant of the analyte A
		:param [K_DNSB]: Dissociation constant of the nonspecific binders
		:param receptorDensityAnalyte: receptor density analyte [sites/m^2], default is 2*10**16 sites per m^2.
		:param receptorDensityNSB: receptor density for NSB [sites/m^2], default is 2*10**16 sites per m^2.

	"""

	def __init__(self,surfacearea,A=1,K_DA=1,NSB=1,K_DNSB=1,receptorDensityAnalyte=2*10**10*1e6,receptorDensityNSB=2*10**10*1e6):

		self.surfacearea = surfacearea
		self.receptorDensityAnalyte = receptorDensityAnalyte
		self.receptorDensityNSB = receptorDensityNSB

		self.numberOfReceptorSitesAnalyte = self.surfacearea*self.receptorDensityAnalyte
		self.numberOfReceptorSitesNSB = self.surfacearea*self.receptorDensityNSB
		self.A = A # concentration of analyte
		self.K_DA = K_DA # Dissociation constant of analyte
		self.NSB = NSB # concentration of NSB
		self.K_DNSB = K_DNSB # Dissociation of NSB

	def NumAnaBoundToSurface(self,competitive = False):
		"""
		Calculates the number of analyte molecules on the sensor surface for a given affinity and a given concentration

		:param boolean competitive: default false, if True the competitive Langmuir model will be used to compute the surface coverage.

		.. math:: N_{Ana}^B = \\theta_A\\cdot N_{Ana}

		"""
		if competitive:
			return CompetitiveLangmuir(self.A,self.K_DA,self.NSB,self.K_DNSB)[0]*self.numberOfReceptorSitesAnalyte
		else:
			# print "NumAnaBoundToSurface"
			# print self.A
			# print self.K_DA
			# print self.numberOfReceptorSitesAnalyte
			return LangmuirIsoterm(self.A,self.K_DA)*self.numberOfReceptorSitesAnalyte

	def NumNSBBoundToSurface(self,competitive = False):
		"""
		Number of nonspecific binders adsorbed to the biosensor surface for a given K_D of the nonspecific binders and a concentration

		:param [NSB]: Analyte concentration
		:param [K_D]: Dissociation constant of the analyte receptor system
		:param boolean competitive: default false, if True the competitive Langmuir model will be used to compute the surface coverage.

		.. math:: N_{NSB}^B = \\theta_{NSB}\\cdot N_{NSB}

		"""

		if competitive:
			return CompetitiveLangmuir(self.A,self.K_A,self.NSB,self.K_DNSB)[1]*self.numberOfReceptorSitesNSB
		else:
			# print "NumNSBBoundToSurface"
			# print self.NSB
			# print self.K_DNSB
			# print self.numberOfReceptorSitesNSB
			return LangmuirIsoterm(self.NSB,self.K_DNSB)*self.numberOfReceptorSitesNSB

	def AnaMassDensityOnSurface(self,competitive = False):
		"""
		Calculates the mass density of the analyte on the sensor surface based on the number of molecules that has adsorbed.
		:param boolean competitive: default false, if True the competitive Langmuir model will be used to compute the surface coverage.
		"""

		adlayer = ProteinAdlayer(N_scat = NumAnaBoundToSurface(competitive)/self.surfacearea)

		# assuming that the adlayer is made from SAv, M= 52.8 kDa.

		return adlayer.surfaceMassFromProteinNumber()

	def NSBMassDensityOnSurface(self,competitive = False):
		"""
		Calculates the mass density of the nonspecific binder on the sensor surface based on the number of molecules that has adsorbed.
		:param boolean competitive: default false, if True the competitive Langmuir model will be used to compute the surface coverage.
		"""

		adlayer = ProteinAdlayer(N_scat = NumNSBBoundToSurface(competitive)/self.surfacearea)

		# assuming that the adlayer is made from SAv, M= 52.8 kDa.

		return adlayer.surfaceMassFromProteinNumber()

	def calcRatioNSB_SB(self,competitive = False):
		"""
		Calculates the ratio of nonspecific to specific binding with the Langmuir isotherm.
		:param [A]: Analyte A concentration [M]
		:param [NSB]: Analyte NSB concentration [M]
		:param [K_DA]: Dissociation constant of the analyte A
		:param [K_DNSB]: Dissociation constant of the nonspecific binders
		:param boolean competitive: default false, if True the competitive Langmuir model will be used to compute the surface coverage.

		:returns: :math:`\\frac{\\theta_{NSB}}{\\theta_{SB}}`

		Noncompetitive model:

		.. math:: \\frac{\\theta_{NSB}}{\\theta_{SB}} = \\frac{[NSB](K_A+[A])}{(K_{NSB}+[NSB])[A]}

		Competitive Model:

		.. math:: \\frac{\\theta_{NSB}}{\\theta_{SB}} = \\frac{[NSB]K_A}{K_{NSB}[A]}

		"""

		theta_NSB = Langmuir_Isoterm(self.NSB,self.K_DNSB)
		theta_SB = Langmuir_Isoterm(self.A,self.K_DA)

		if competitive:
			theta_SB, theta_NSB = Competitive_Langmuir(self.A,self.K_DA,self.NSB,self.K_DNSB)
		else:
			theta_SB = Langmuir_Isoterm(self.A,self.K_DA)
			theta_NSB = Langmuir_Isoterm(self.NSB,self.K_DNSB)

		return theta_NSB/theta_SB


def LangmuirIsoterm(A,K_DA):
	"""
	Calculates the Langmuir isoterm, according to:

	:param [A]: Analyte concentration
	:param [K_D]: Dissociation constant of the analyte receptor system

	:return: :math:`\\theta` Surface coverage

	.. math:: \\theta = \\frac{[A]}{K_D + [A]}

	"""

	return A/(K_DA + A)

def CompetitiveLangmuir(A,B,K_DA,K_DB):
	"""Calculates the competitive Langmuir Isoterm, according to:

	:param [A]: Analyte A concentration [M]
	:param [B]: Analyte B concentration [M]
	:param [K_DA]: Dissociation constant of the analyte A
	:param [K_DB]: Dissociation constant of the analyte B

	:return: :math:`\\theta_{Ana}`, :math:`\\theta_{NSB}`

	.. math:: \\theta = \\frac{\\frac{[A]}{K_D^A}}{1+\\frac{[B]}{K_D^B} + \\frac{[A]}{K_D^A}}

	"""

	return 1./K_DA*A/(1.+1./K_DA*A+1./K_DB*B), 1./K_DB*B/(1.+1./K_DA*A+1./K_DB*B)

class SPRBiosensor(BinderSurfaceInteraction):
	"""
		:param SurfaceArea: Sensor Surface area [m^2]
		:param [A]: Analyte A concentration
		:param [NSB]: Analyte NSB concentration
		:param [K_DA]: Dissociation constant of the analyte A
		:param [K_DNSB]: Dissociation constant of the nonspecific binders
		:param receptorDensityAnalyte: receptor density analyte [sites/m^2], default is 2*10**16 sites per m^2.
		:param receptorDensityNSB: receptor density for NSB [sites/m^2], default is 2*10**16 sites per m^2.
	"""

	def __init__(self,surfacearea,A=1,K_DA=1,NSB=1,K_DNSB=1,receptorDensityAnalyte=2*10**10*1e6,receptorDensityNSB=2*10**10*1e6):

		BinderSurfaceInteraction.__init__(self,surfacearea,A,K_DA,NSB,K_DNSB,receptorDensityAnalyte,receptorDensityNSB)




class MologramBiosensor(object):
	"""The affinity of the Braggregion for the analyte is assumed to be the same as for the nonspecific binder to the mologram, the recognition
	site density is the average of the recognition site density of ridges and grooves for the nonspecific binder. We assume a canonical mologram. (Other types would need to be
	implemented separately in the future.)

	:param mologram: instance of class Mologram
	:param InteractionParameter: list of two instances of class MologramBiosensorInteractionParameter one named analyte the other NSB
	:param (boolean) competitive: specifies whether the competitive Langmuir isoterm is to be used to calculate the surface coverage.

	This class should have a phase mask, or that it was fabricated with a certain intensity ratio.
	"""

	def __init__(self,mologram,InteractionParameter):

		MoloArea = mologram.AreaWithoutBragg()
		EntireArea = mologram.Area()

		globals().update(InteractionParameter.getValues()) # unpack the analyte parameter dictionary

		self.competitive = analyte_competitive
		print "_______________________________________________"

		# convert PTP to RMS value, if the binders are sinusoidally distributed.
		if analyte_placement == 'Ridges (Sinusoidal)':
			self.Grooves = BinderSurfaceInteraction(MoloArea/2.0,analyte_c,analyte_K_D,NSB_c,NSB_K_D,analyte_RecDenGrov*np.sqrt(2.)/2,NSB_RecDenGrov*np.sqrt(2.)/2)
			self.Ridges = BinderSurfaceInteraction(MoloArea/2.0,analyte_c,analyte_K_D,NSB_c,NSB_K_D,analyte_RecDenRid*np.sqrt(2.)/2,NSB_RecDenRid*np.sqrt(2.)/2)
		else:
			self.Grooves = BinderSurfaceInteraction(MoloArea/2.0,analyte_c,analyte_K_D,NSB_c,NSB_K_D,analyte_RecDenGrov,NSB_RecDenGrov)
			self.Ridges = BinderSurfaceInteraction(MoloArea/2.0,analyte_c,analyte_K_D,NSB_c,NSB_K_D,analyte_RecDenRid,NSB_RecDenRid)

		self.BraggRegion = BinderSurfaceInteraction(EntireArea - MoloArea,analyte_c,NSB_K_D,NSB_c,NSB_K_D,(NSB_RecDenRid + NSB_RecDenGrov)/2.,(NSB_RecDenRid + NSB_RecDenGrov)/2.)

		print self.Ridges.NumAnaBoundToSurface(self.competitive)
		print self.Grooves.NumAnaBoundToSurface(self.competitive)
		print self.Ridges.NumNSBBoundToSurface(self.competitive)
		print self.Grooves.NumNSBBoundToSurface(self.competitive)


	def cohAnalyteN(self):
		"""returns the number of coherent analytes on the mologrpahic structure"""

		return self.Ridges.NumAnaBoundToSurface(self.competitive)-self.Grooves.NumAnaBoundToSurface(self.competitive)

	def inCohAnalyteN(self):
		"""returns the number of incoherent analytes on the molographic structure"""

		return self.Grooves.NumAnaBoundToSurface(self.competitive)*2 + self.BraggRegion.NumAnaBoundToSurface(self.competitive)

	def cohNSBN(self):
		"""returns the number of coherent nonspecific binders on the molographic structure"""

		return self.Ridges.NumNSBBoundToSurface(self.competitive)-self.Grooves.NumNSBBoundToSurface(self.competitive)

	def inCohNSBN(self):
		"""returns the number of incoherent nonspecific binders on the molographic structure"""

		return self.Grooves.NumNSBBoundToSurface(self.competitive)*2 + self.BraggRegion.NumNSBBoundToSurface(self.competitive)


def minimalDetectableConcentrationForGivenSurfaceMassDensityModulation(Delta_Gamma,I_R,M_protein,K_D,R_0=2*10**8):
	"""
	Calculates the minimal detectable analyte concentration as a function of the surface mass density modulation at the detection limit, the mass of the protein, the dissociation constant of the interaction, the intensity ratio of the phase mask and the surface receptor concentration.

	.. math:: \\left[ {{A_0}} \\right] = \\frac{{{N_A}{\\Delta _\\Gamma }}}{{{M_{ana}}}}\\frac{{{K_d}}}{{{R_0}}}\\frac{1}{{\\left( {{{\\left( {\\frac{1}{{{r_I}}}} \\right)}^{\\frac{{2{r_I}}}{{{r_I}^2 - 1}}}} - {{\\left( {\\frac{1}{{{r_I}}}} \\right)}^{\\frac{{2{r_I}^2}}{{{r_I}^2 - 1}}}}} \\right)}}

	:param Delta_Gamma: [pg/mm^2]
	:param I_R: []
	:param M_protein: [g/mol]
	:param K_D: [M]
	:param R_0: [number/mm^2]

	"""

	return N_A*Delta_Gamma*1e-12/M_protein*K_D/R_0*100./calc_max_activation_modulation(I_R)




# ------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------
# Testing functions for this module

def test_CompetitiveLangmuir():
	assert CompetitiveLangmuir(1e-3,1e-6,1e-10,1e-3) == 0,0

def test_Binder_Surface_Interaction():

	binder_obj = BinderSurfaceInteraction(np.pi*(50e-6/2.)**2, K_DA=10**(-9),A=10**(-13))
	binder_obj = BinderSurfaceInteraction(100000e-12, K_DA=10**(-9),A=10**(-13))

	print(binder_obj.NumAnaBoundToSurface())
	print('Number of binding sites on the sensor:')
	print(binder_obj.numberOfReceptorSitesAnalyte)

if __name__ == "__main__":

	test_Binder_Surface_Interaction()
	Concentration = np.logspace(-16,0,100)


	
	# attention this is Delta_Gamma and not the mass density on the ridges.
	print minimalDetectableConcentrationForGivenSurfaceMassDensityModulation(Delta_Gamma=1.,I_R=1.7,M_protein = 53000.,K_D=1e-9)

	# --> roughly 300 pM can be detected in equilibrium.


	# NSB = np.logspace(-16,0,100)
	# A = np.logspace(-16,0,100)



	# theta_NSB = Langmuir_Isoterm(NSB,K_NSB)
	# theta_A = Langmuir_Isoterm(A,K_DA)

	#plt.plot(np.logspace(-16,0,100),theta_NSB)
	#plt.plot(np.logspace(-16,0,100),theta_A)
