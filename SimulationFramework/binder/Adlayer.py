"""
:created:		20-Dec-2016
:author:		Andreas Frutiger
:revised:		31-Jan-2017 by Roland Dreyfus
:references:
"""

import sys
sys.path.append('../')
import numpy as np
from auxiliaries.constants import dn_dc,N_A
from dipole.dipole import scatteringCrossSection

# to do: A class needs to be added that does accepts a mass distribution over ridge and groove

class MologramProteinAdlayer():
	"""Class should have different constructors that allow to call with mass density, mass modulation etc.
	
	:param Delta_Gamma: is the surface mass modulation
	:param eta: is the analyte efficiency of the modulation"""

	def __init__(self, M_protein, eta, Delta_Gamma=0):

		self.protein = Protein(M_protein)
		self.Delta_Gamma = Delta_Gamma
		self.eta = eta

	def refIndexProtein(self):

		return self.protein.refIndexProtein()

	def massDensityProtein(self):

		return self.protein.massDensityProtein()


class Protein():

	def __init__(self, M_protein):
		"""class that calculates the refractive index, volume, radius and the density of a protein sphere in water according to Fischer. """

		self.M_protein = M_protein*1e-3	# Da to kDa
		self.n_solv = 1.33 # refractive index of water
		self.rho_solv = 1.0028 # density of water in g/ml

	def massDensityProtein(self):
	    """ Returns the average density of proteins in g/ml according to Fischer et
	    al. :cite:`Fischer2004` According to this reference the average protein
	    density is a molecular weight independent function.

	    .. math:: {\\rho _{protein}} = 1.410 + 0.145 \\cdot {e^{ - {{{M_
	    	{protein}}\\left[{kDa} \\right]} \\over {13}}}}
	    """

	    rho_protein = 1.410 + 0.145*np.exp(-self.M_protein/13.)

	    return rho_protein

	def volumeProtein(self):
		""" According to Fattinger :cite:`fattinger_focal_2014`

		.. math:: {V_{protein}}\\left[ {ml} \\right] = {{dn} \\over {dc}}{{{M_{protein}}\\left[
			{{g \\over {mol}}} \\right]} \\over {{N_A}\\left( {{n_{protein}} - {n_
			{solv}}} \\right)}}
		Formula is depreciated, this is the correct formula:

		.. math:: {V_{\\rm{P}}} = {{{M_{\\rm{P}}}} \\over {{N_A}{\\rho _{\\rm{P}}}}} = {{{M_{\\rm{P}}}} \\over {{N_A}}}{2 \\over 3}{{dn} \\over {dc}}{{n_{\\rm{P}}^2 + 2n_{\\rm{c}}^2} \\over {{n_{\\rm{c}}}\\left( {n_{\\rm{P}}^2 - n_{\\rm{c}}^2} \\right)}}
		:return: Volume of the protein [ml] or cm^3

		"""

		M_protein = self.M_protein*1000 # convert to Dalton
		#V_protein = dn_dc*M_protein/(N_A*(self.refIndexProtein() - self.n_solv))
		n_p = self.refIndexProtein()
		n_c = self.n_solv
		V_protein = M_protein/N_A*2/3.*dn_dc*(n_p**2+2*n_c**2)/(n_c*(n_p**2-n_c**2))


		return V_protein # in cm^3

	def radiusProtein(self):

		V_protein = self.volumeProtein() # (1 ml = 1 cm^3)
		r_protein = (3*V_protein/(4*np.pi))**(1/3.)

		return r_protein	# in cm

	def refIndexProtein(self):
		"""Calculates the refractive index of a protein sphere according to Zhao et al. (On the distribution of protein refractive index increments)
		
		The formula stated in Fattinger is not accurate!

		:param dn_dc: refractive index increment (0.182 in water [ml/g])

		.. math:: $$n_{\\rm{P}}^2 = {{n_{\\rm{c}}^2\\left( {3{n_{\\rm{c}}} + 4{\\rho _{\\rm{P}}}{{dn} \\over {dc}}} \\right)} \\over {3{n_{\\rm{c}}} - 2{\\rho _{\\rm{P}}}{{dn} \\over {dc}}}}$$

		"""

		rho_p = self.massDensityProtein()
		n_c = self.n_solv
		
		return np.sqrt(n_c**2*(3*n_c+4*rho_p*dn_dc)/(3*n_c-2*rho_p*dn_dc))

	def ref_index_increment_for_n_c(self, n_c):
		"""computes the refractive index increment for a given cover medium"""

		rho_p = self.massDensityProtein()
		n_p = self.refIndexProtein()
		return 3/2.*1/rho_p*n_c*(n_p**2-n_c**2)/(n_p**2+2*n_c**2)

	def ref_index_increment_for_n_c_SI(self,n_c):

		return self.ref_index_increment_for_n_c(n_c)*1e-6/(1e-3) 


# all class internal units are in ml/cm^3, cm and g!!

class ProteinAdlayer():
	"""This class contains all the necessary equations to relate refractive index
	changes of adlayers to protein numbers/mm^2 or surface mass density [pg/mm^2], important this class is only fully correct for water, however, it also works sufficiently well in air. (< 10 % error)

	:param M_protein [Da]: Molecular mass of the protein in Da, default is 52.8e3 Da for SAv.
	:param delta [m]: thickness of the layer, where the proteins are distributed,
		default is 82 nm (penetration depth of the field).
	:param Gamma [pg/mm^2]: average mass density on the surface
	:param N_scat: Number of protein scatteres on the surface per [mm2]
	"""

	def __init__(self, M_protein=52.8e3, delta=82*1e-9, Gamma=0, N_scat = 0):

		self.protein = Protein(M_protein)

		self.n_solv = 1.33 # refractive index of water 
		self.rho_solv = 1.0028  #[g/ml]: Mass density of the water
		self._delta = delta*1e2 # conversion from m to cm

		if N_scat is not 0:

			self.N_scat = N_scat
			self.Gamma = self.surfaceMassFromProteinNumber()

		if Gamma is not 0:
			self.Gamma = Gamma*1e-12 # convert pg/mm2 to g/mm2
			self.N_scat = self.proteinNumberFromSurfaceMass()

	def returnDeltaSI(self):

		return self._delta*1e-2

	def proteinNumberFromSurfaceMass(self):
		"""
		Calculates the number of protein scatterers per mm^2 from the surface_mass
		density in

		.. math:: {N_{scat}} = {\\Gamma \\over M} \\cdot {N_A}

		:return: N_scat, number of protein scatteres/mm^2
		"""
		M_protein = self.protein.M_protein*1e3 # kDa to Da conversion [g/mol]

		N_scat = self.Gamma/(M_protein)*N_A

		return N_scat

	def surfaceMassFromProteinNumber(self):
		"""Calculates the surface mass density from the number of protein
		scatterer difference between grooves and ridges per pg/mm^2

		.. math:: {\\Gamma } = {{{N_{scat}} \\cdot M} \\over {{N_A}}}

		:return: Gamma [pg/mm^2]
		"""
		M_protein = self.protein.M_protein*1e3 # kDa to Da conversion

		Gamma=self.N_scat*M_protein/N_A*1e12

		return Gamma

	def refIndexProteinLayer(self):
		"""Calculates the refractive index of a protein adlayer as a function of the
		mass density of the protein layer. (According to Voeroes :cite:`Voros2002`)

		.. math:: {n_{layer}} = {n_{solv}} + {{{\\rho _{layer}} - {\\rho _{solv}}} \\over {1 - {{{\\rho _{solv}}}
			\\over {{\\rho _{protein}}}}}}{{dn} \\over {dc}}

		:return: n_layer, refractive index of the protein adlayer.

		"""
		rho_solv = self.rho_solv
		n_solv = self.n_solv
		rho_layer = self.massDensityLayer()
		rho_protein = self.protein.massDensityProtein()

		n_layer = n_solv + (rho_layer-rho_solv)/(1-rho_solv/rho_protein)*dn_dc

		return n_layer

	def massDensityProtein(self):
		"""Adapter function for compatibility reasons"""

		return self.protein.massDensityProtein()

	def volumeProtein(self):
		"""Adapter function for compatibility reasons"""

		return self.protein.volumeProtein()

	def radiusProtein(self):
		"""Adapter function for compatibility reasons"""

		return self.protein.radiusProtein()

	def refIndexProtein(self):
		"""Adapter function for compatibility reasons"""

		return self.protein.refIndexProtein()


	def volumeFractionProtein(self):
		"""
		This function calculates the volume fraction of a protein molecule in a layer
		with height  :math:`\\delta`. per default :math:`\delta` is the penetration
		depth of the evanescent field.


		.. math:: {\\Phi _{protein}} = {{{\\Gamma}} \\over \\delta } \\cdot {{{N_A}} \\over
			{{M_{protein}}\\left[ {{g \\over {mol}}} \\right]}} \\cdot {V_{protein}}

		:return: :math:`\\Phi_{protein}` the volume fraction of the protein molecule
		in the layer."""

		Gamma = self.Gamma
		delta = self._delta
		M_protein = self.protein.M_protein
		Phi_protein = Gamma/delta*N_A/(M_protein)*self.protein.volumeProtein()


		return Phi_protein

	def massDensityLayer(self):
		"""
		Calculates the mass density of the layer depending on the volume fraction of
		protein in it.

		.. math:: {\\rho _{layer}} = \\left( {1 - {\\Phi _{protein}}} \\right)
			{\\rho _{solv}} + {\\Phi _{protein}}{\\rho _{protein}}

		:return: :math:`\\rho_{layer}`

		"""

		Phi_Protein = self.volumeFractionProtein()
		rho_protein = self.protein.massDensityProtein()
		rho_solv = self.rho_solv

		rho_layer = (1-Phi_Protein)*rho_solv+Phi_Protein*rho_protein
		return rho_layer


#Diffraction efficiency conversion for a canonical to a sinusoidal grating is 16/pi^2

################################################################################################
################################################################################################
# needs to be reimplemented with the current knowledge

# class ProteinSurfaceMassModulationAdlayer(ProteinAdlayer):
# 	"""converts surface mass density to surface mass density modulations for various mologram types.

# 	:param Gamma_Delta: Surface mass density modulation (Peak to Peak value of the sinusoid)
# 	:param A_ridges: Surface area of the ridges [mm]
# 	:param A_molo: Surface area of the entire molographic structure (ridges + grooves + bragg area) [mm]
# 	:param mologram_type: 'Ridges (Sinusoidal), Ridges (Rectangular)'
# 	:param N_coh: number of coherent scatterers on the mologram
# 	:param Gamma_coh: coherent surface mass density

# 	"""

# 	def __init__(self, M_protein=52.8e3, n_solv=1.33, rho_solv =
# 		1.0028, delta=82*1e-9, Gamma_Delta=0, Gamma_coh = 0,N_coh = 0,A_ridges = 0,A_molo = 0,mologram_type = 'Ridges (Sinusoidal)'):

# 		# if the coherent surface mass density is supplied.
# 		if Gamma_coh is not 0:
# 			self.m_coh = Gamma_coh*A_molo

# 			if mologram_type == 'Ridges (Sinusoidal)':
# 				"""Delta Gamma is the Peak-to-Peak Value"""

# 				self.Gamma_Delta = self.m_coh/A_ridges*np.sqrt(2.)
# 				Gamma_Delta = self.Gamma_Delta

# 			else:
# 				"""Delta Gamma is the RMS value any shape"""

# 				self.Gamma_Delta = self.m_coh/A_ridges
# 				Gamma_Delta = self.Gamma_Delta


# 		# if the surface mass density modulation is supplied.
# 		if Gamma_Delta is not 0:

# 			self.Gamma_Delta = Gamma_Delta

# 			if mologram_type == 'Ridges (Sinusoidal)':
# 				"""Delta Gamma is the Peak-to-Peak Value"""

# 				self.m_coh = A_ridges*self.Gamma_Delta*np.sqrt(2.)/2.

# 			else:
# 				"""Delta Gamma is the RMS value any shape"""

# 				self.m_coh = A_ridges*self.Gamma_Delta

# 			Gamma = self.m_coh/A_molo

# 			ProteinAdlayer.__init__(self,M_protein=M_protein, n_solv=n_solv, rho_solv =rho_solv, delta=delta, Gamma=Gamma)

# 			self.N_coh = self.N_scat*A_molo # number of coherent scatterers

# 		# if the number of coherent scatterers is supplied.
# 		if N_coh is not 0:

# 			self.N_coh = N_coh

# 			self.m_coh = self.M_protein*self.N_coh/N_A

# 			if mologram_type == 'Ridges (Sinusoidal)':
# 				"""Delta Gamma is the Peak-to-Peak Value"""

# 				self.Gamma_Delta = self.m_coh/A_ridges*np.sqrt(2.)

# 			elif mologram_type == 'Ridges (Rectangular)':
# 				"""Delta Gamma is the RMS value"""

# 				self.Gamma_Delta = self.m_coh/A_ridges

# 			Gamma = self.m_coh/A_molo

# 			ProteinAdlayer.__init__(self,M_protein=M_protein, n_solv=n_solv, rho_solv =rho_solv, delta=delta, Gamma=Gamma, N_scat = N_scat)

	# def volumeFractionProtein(self):

	# 	"""

	# 	This function calculates the volume fraction of a protein molecule in a layer
	# 	with height  :math:`\\delta`. per default :math:`\delta` is the penetration
	# 	depth of the evanescent field.


	# 	.. math:: {\\Phi _{protein}} = {{{\\Gamma_Delta}} \\over \\delta } \\cdot {{{N_A}} \\over
	# 		{{M_{protein}}\\left[ {{g \\over {mol}}} \\right]}} \\cdot {V_{protein}}

	# 	:return: :math:`\\Phi_{protein}` the volume fraction of the protein molecule
	# 	in the layer."""

	# 	Gamma_Delta = self.Gamma_Delta
	# 	delta = self._delta
	# 	M_protein = self.M_protein
	# 	Phi_protein = Gamma_Delta/delta*N_A/(M_protein)*self.volumeProtein()

	# 	return Phi_protein

def differenceWaterModelInAirSimulation():
	"""This function is to show that the refractive index and the volume of the protein can be estimated in air in the same way as they are done in water
	and the dipole polarizability is not considerably affected. 

	"""

	protein_layer = ProteinAdlayer(52.8e3,Gamma=0.275,delta=82*1e-9)

	print 'Water properties:'
	print'\n volumeProtein:', protein_layer.volumeProtein()
	print'\n radiusProtein:', protein_layer.radiusProtein()
	print'\n refractiveIndexProtein:', protein_layer.refIndexProtein()

	volumeProtein = protein_layer.volumeProtein()
	radiusProtein = protein_layer.radiusProtein()
	nProtein = protein_layer.refIndexProtein()

	print 'Scattering crosssection of protein particle in air calculated from water properties:'
	n_medium = 1.
	wavelength = 632.8e-9
	k = n_medium*2*np.pi/wavelength
	C_scat_waterProp = scatteringCrossSection(k, radiusProtein*10**-2, np.sqrt(nProtein), np.sqrt(1.))

	n_P = 1.615 # refractive index of lysine
	V_P = 6.64*10**(-20) # volume of SAv calculated from the dry density of BSA 1.32 g/cm^2
	r_P = (V_P/(4/3.*np.pi))**(1/3.)
	
	print 'Scattering crosssection of protein particle in air calculated from air properties:'
	C_scat_airProp = scatteringCrossSection(k, r_P*10**-2, np.sqrt(n_P), np.sqrt(1.33))

	print 'relative Difference of the scattering crosssections calculated in air and water:'
	print C_scat_airProp/C_scat_waterProp
	


	def FattingerEquation1LastPart(n_r,n_0,V_P):

		return abs((n_r**2 - n_0**2)/(n_r**2+2*n_0**2))**2*V_P**2

	n_P = 1.587 # SAv particle
	V_P = 6.2*10**(-20) # volume of protein in cm^3
	water_and_refractive_index_incr = FattingerEquation1LastPart(n_P,1.,V_P)


	n_P = 1.615 # refractive index of lysine
	V_P = 6.64*10**(-20) # volume of SAv calculated from the dry density of BSA 1.32 g/cm^2
	protein_particle_in_air = FattingerEquation1LastPart(n_P,1.,V_P)

	print "Difference of using the dn_dc model in air for the simulation formula [%]:"
	print (np.sqrt(protein_particle_in_air)/np.sqrt(water_and_refractive_index_incr)-1.)*100







if __name__ == "__main__":


	# testing of functions, delta should be the same size as the layer thickness
	# used to place the proteins (default: 52.8e3,Gamma=3,delta=82*1e-9)
	protein_layer = ProteinAdlayer(
				52.8e3,Gamma=3.,delta=82*1e-9
			)

	print'\n refIndexProteinLayer:', protein_layer.refIndexProteinLayer()
	# Result: 1.33006658537

	print'\n proteinNumberFromSurfaceMass:', protein_layer.proteinNumberFromSurfaceMass()
	#Result: 34216709

	print'\n refIndexProtein:',  protein_layer.refIndexProtein()
	#Result: 1.598

	print'\n volumeProtein:', protein_layer.volumeProtein()
	print'\n radiusProtein:', protein_layer.radiusProtein()
	print'\n massDensityProtein:', protein_layer.massDensityProtein()

	protein_layer = ProteinAdlayer(52.8e3, N_scat= 34216709.419)

	print'\n surfaceMassFromProteinNumber:',  protein_layer.surfaceMassFromProteinNumber()
	# Result: 3 pg/mm^2

	print'\n massDensityProtein:', protein_layer.massDensityProtein()
	print'\n refIndexProtein:', protein_layer.refIndexProtein()
	print'\n volumeProtein:', protein_layer.volumeProtein()
	print'\n radiusProtein:', protein_layer.radiusProtein()


	protein = Protein(52.8e3)
	print'\n refractive index increment in Air:', protein.ref_index_increment_for_n_c(1.)
	print'\n refractive index increment in Water:', protein.ref_index_increment_for_n_c(1.33)


################################################################################################
################################################################################################
# difference to Fattinger's publication: (n_m^2-n_c^2=4n_cdn/dc\Delta_Gamma/t_m)
	protein_layer = ProteinAdlayer(
				52.8e3,Gamma=2000.,delta=0.0001*1e-9
			)
	n_m = protein_layer.refIndexProteinLayer()
	n_c = 1.33
	print((n_m**2-n_c**2)/(4*n_c*dn_dc*2000*10**(-6)/(0.0001*1e-7)))


	protein_layer = ProteinAdlayer(52.8e3,Gamma=540,delta=82*1e-9)

	print 'proteinNumberFromSurfaceMass:', protein_layer.proteinNumberFromSurfaceMass()

	differenceWaterModelInAirSimulation()

	print'angle', 180 / np.pi * np.arcsin(290. / (1.5*2* 145))

