
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
using namespace std;
// includes, project
#include <cuda_runtime.h>
#include <book.h>




__global__ void compute(float *dThetaknk1, float *E_x, float *E_y,float *E_z,float *E_xi,float *E_yi,float *E_zi,float *rP0,float *rP1,float *rP2,float *iP0,float *iP1,float *iP2,float *r_px,float *r_py,float *r_pz,float *i_px,float *i_py,float *i_pz,float *sc_x,float *sc_y,float *sc_z,float *n,float *phase,float *x,float *y,float *z,int offs,int npixx,int npixy){
 
    int i = blockIdx.x+offs;
    int xx = threadIdx.x + blockIdx.y * blockDim.x;
    int yy = threadIdx.y + blockIdx.z * blockDim.y;
    
    if (xx<npixx)
    {
        if (yy<npixy)
        {

            int offset = xx + yy * npixx;
            float dTheta = dThetaknk1[0];
            float k_1 = dThetaknk1[2];
            float k_n = dThetaknk1[1];

                float x_dip=x[i];
                float y_dip=y[i];
                float z_dip=z[i];
                float rpx=r_px[i];
                float rpy=r_py[i];
                float rpz=r_pz[i];
                float ipx=i_px[i];
                float ipy=i_py[i];
                float ipz=i_pz[i];


                    float s_x=sc_x[offset];
                    float s_y=sc_y[offset];
                    float s_z=sc_z[offset];

                    float dz=s_z-z_dip;
                    float dx=s_x-x_dip;
                    float dy=s_y-y_dip;

                    float r = sqrtf( dx*dx + dy*dy + dz*dz );
                    float rho = sqrtf(dx*dx + dy*dy );
                    float phi = atan2(dy,dx);
                    float theta = atan2(rho,abs(dz));
                    
                    float st=sinf(theta);
                    float ct=cosf(theta);
                    
                    float stst=st*st;

                    
                    float sp=sinf(phi);
                    float cp=cosf(phi);
                    
                    float cpct=cp*ct;
                    float spct=sp*ct;
                    
                    
                    int first= theta/ dTheta;
                    int second=first+1;
                    float interpolator=(theta-first*dTheta)/dTheta;
                    float rPhi_0_t=rP0[first]+(rP0[second]-rP0[first])*interpolator;
                    float iPhi_0_t=iP0[first]+(iP0[second]-iP0[first])*interpolator;
                    float rPhi_1_t=rP1[first]+(rP1[second]-rP1[first])*interpolator;
                    float iPhi_1_t=iP1[first]+(iP1[second]-iP1[first])*interpolator;
                    float rPhi_2_t=rP2[first]+(rP2[second]-rP2[first])*interpolator;
                    float iPhi_2_t=iP2[first]+(iP2[second]-iP2[first])*interpolator;

                    float rexp=cosf(k_n*z_dip*sqrtf( (n[0]/n[2])*(n[0]/n[2])-stst));
                    float iexp=sinf(k_n*z_dip*sqrtf( (n[0]/n[2])*(n[0]/n[2])-stst));

                    float rPhi1= rexp*rPhi_0_t-iexp*iPhi_0_t;
                    float iPhi1= iexp*rPhi_0_t+rexp*iPhi_0_t;
                    float rPhi2= rexp*rPhi_1_t-iexp*iPhi_1_t;
                    float iPhi2= iexp*rPhi_1_t+rexp*iPhi_1_t;
                    float rPhi3= rexp*rPhi_2_t-iexp*iPhi_2_t;
                    float iPhi3= iexp*rPhi_2_t+rexp*iPhi_2_t;

                    float c=cosf(k_n*r+phase[i]);
                    float s=sinf(k_n*r+phase[i]);

                    float theta_part_r=(-(c*iPhi2*ipx) + c*rPhi2*rpx - ipx*rPhi2*s - iPhi2*rpx*s)*cpct;
                    theta_part_r+=(-(c*iPhi2*ipy) + c*rPhi2*rpy - ipy*rPhi2*s - iPhi2*rpy*s)*spct;
                    theta_part_r+=(-(c*iPhi1*ipz) - c*rPhi1*rpz - ipz*rPhi1*s + iPhi1*rpz*s)*st;
                    theta_part_r=theta_part_r/r;

                    float theta_part_i=(c*ipx*rPhi2 + c*iPhi2*rpx - iPhi2*ipx*s + rPhi2*rpx*s)*cpct;
                    theta_part_i+=(c*ipy*rPhi2 + c*iPhi2*rpy - iPhi2*ipy*s + rPhi2*rpy*s)*spct;
                    theta_part_i+=(c*ipz*rPhi1 - c*iPhi1*rpz - iPhi1*ipz*s - rPhi1*rpz*s)*st;
                    theta_part_i=theta_part_i/r;

                    float phi_part_r=(c*iPhi3*ipx - c*rPhi3*rpx + ipx*rPhi3*s + iPhi3*rpx*s)*sp;
                    phi_part_r+=(-(c*iPhi3*ipy) + c*rPhi3*rpy - ipy*rPhi3*s - iPhi3*rpy*s)*cp;
                    phi_part_r=phi_part_r/r;

                    float phi_part_i=(-(c*ipx*rPhi3) - c*iPhi3*rpx + iPhi3*ipx*s - rPhi3*rpx*s)*sp;
                    phi_part_i+=(c*ipy*rPhi3 + c*iPhi3*rpy - iPhi3*ipy*s + rPhi3*rpy*s)*cp;
                    phi_part_i=phi_part_i/r;

                    atomicAdd(&E_x[offset],cpct*theta_part_r - sp*phi_part_r);
                    atomicAdd(&E_y[offset],spct*theta_part_r + cp*phi_part_r);
                    atomicAdd(&E_z[offset],-st*theta_part_r);

                    atomicAdd(&E_xi[offset],cpct*theta_part_i - sp*phi_part_i);
                    atomicAdd(&E_yi[offset],spct*theta_part_i + cp*phi_part_i);
                    atomicAdd(&E_zi[offset],-st*theta_part_i);
                     __syncthreads();   
                    }  //end of if yy<npixy
                    }   //end of if xx<npixx
                   
};



extern "C"
void moloGPU(float dTheta, float k_n, float k_1, float *E_x, float *E_y,float *E_z,float *E_xi,float *E_yi,float *E_zi,float *rP0,float *rP1,float *rP2,float *iP0,float *iP1,float *iP2,float *r_px,float *r_py,float *r_pz,float *i_px,float *i_py,float *i_pz,float *sc_x,float *sc_y,float *sc_z,float *n,float *phase,float *x,float *y,float *z,int ndip,int npixx,int npixy){

    int DIM=16;  //This is how many workers (threads) are launched, in a DIM by DIM square
    dim3    threads(DIM,DIM); //Here. Imagine these workers as the ones who calculate at once the contribution of 1 scatter on a DIMxDIM subgrid of the screen.

   int max_block=62000; //The dimension of 1 grid axis cannot exceed 62k~. So for N>62000 particles, we'll need to launch the cuda kernel N/62000 times. It's okay
   
   int n_launches=ndip/max_block;  //How many times we need to relaunch the kernel
   int last_launch= ndip%max_block; //How many scatterers are left to calculate in the last launch
   
   
   int numeric_size = sizeof(float); //Since we're handling floats, the correct memory occupied by 1 float is this. Important for mem allocation on the card.

    // Here h denotes host (cpu) and d denotes device (gpu)
   float h_dthetaknk1[3];
   h_dthetaknk1[0]= dTheta;
   h_dthetaknk1[1]= k_n;
   h_dthetaknk1[2]= k_1;
   
   //Allocate pointers, that will later point to device memory. They are the 'boats' on which one pushes CPU data onto the GPU
   float *d_dthetaknk1;
   float *d_E_x, *d_E_y, *d_E_z,*d_E_xi, *d_E_yi, *d_E_zi;
   float *d_rP0, *d_rP1, *d_rP2, *d_iP0, *d_iP1, *d_iP2;
   float *d_r_px, *d_r_py,*d_r_pz, *d_i_px,*d_i_py, *d_i_pz;
   float *d_sc_x, *d_sc_y, *d_sc_z;
   float *d_n;
   float *d_phase;
   float *d_x;
   float *d_y;
   float *d_z;

    //This is how the 'boats' are shipped to and from CPU<->GPU
    
   HANDLE_ERROR( cudaMalloc( (void**)&d_dthetaknk1, 3*numeric_size));  //1. Allocate space on the GPU, of size 3*floats, pointed to by the pointer d_dthetaknk1
   HANDLE_ERROR( cudaMemcpy( d_dthetaknk1, h_dthetaknk1, 3*numeric_size, cudaMemcpyHostToDevice)); //2. Ship the CPU data h_dthetaknk1 into the now correctly allocated GPU memory


   HANDLE_ERROR( cudaMalloc( (void**)&d_E_x, npixx*npixy*numeric_size )); //1. etc
   HANDLE_ERROR( cudaMemcpy( d_E_x,E_x, npixx*npixy*numeric_size, cudaMemcpyHostToDevice )); //2. etc
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_E_y, npixx*npixy*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_E_y,E_y, npixx*npixy*numeric_size, cudaMemcpyHostToDevice ));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_E_z, npixx*npixy*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_E_z,E_z, npixx*npixy*numeric_size, cudaMemcpyHostToDevice ));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_E_xi, npixx*npixy*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_E_xi,E_xi, npixx*npixy*numeric_size, cudaMemcpyHostToDevice ));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_E_yi, npixx*npixy*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_E_yi,E_yi, npixx*npixy*numeric_size, cudaMemcpyHostToDevice ));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_E_zi, npixx*npixy*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_E_zi,E_zi, npixx*npixy*numeric_size, cudaMemcpyHostToDevice ));      
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_rP0, 100*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_rP0,rP0, 100*numeric_size, cudaMemcpyHostToDevice));      
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_rP1, 100*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_rP1,rP1,100*numeric_size, cudaMemcpyHostToDevice ));   
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_rP2, 100*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_rP2,rP2, 100*numeric_size, cudaMemcpyHostToDevice ));   

   HANDLE_ERROR( cudaMalloc( (void**)&d_iP0, 100*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_iP0,iP0, 100*numeric_size, cudaMemcpyHostToDevice ));      
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_iP1, 100*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_iP1,iP1, 100*numeric_size, cudaMemcpyHostToDevice ));   
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_iP2, 100*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_iP2,iP2, 100*numeric_size, cudaMemcpyHostToDevice ));   
   

   HANDLE_ERROR( cudaMalloc( (void**)&d_r_px, ndip*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_r_px,r_px,ndip*numeric_size, cudaMemcpyHostToDevice )); 
        
   HANDLE_ERROR( cudaMalloc( (void**)&d_r_py, ndip*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_r_py,r_py, ndip*numeric_size, cudaMemcpyHostToDevice ));      
       
   HANDLE_ERROR( cudaMalloc( (void**)&d_r_pz, ndip*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_r_pz,r_pz, ndip*numeric_size, cudaMemcpyHostToDevice ));      
 
   HANDLE_ERROR( cudaMalloc( (void**)&d_i_px, ndip*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_i_px,i_px, ndip*numeric_size, cudaMemcpyHostToDevice ));      
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_i_py, ndip*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_i_py,i_py, ndip*numeric_size, cudaMemcpyHostToDevice ));      
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_i_pz, ndip*numeric_size ));
   HANDLE_ERROR( cudaMemcpy( d_i_pz,i_pz, ndip*numeric_size, cudaMemcpyHostToDevice ));   
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_sc_x, npixx*npixy*numeric_size)) ;   
   HANDLE_ERROR( cudaMemcpy( d_sc_x,sc_x, npixx*npixy*numeric_size, cudaMemcpyHostToDevice));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_sc_y, npixx*npixy*numeric_size)) ;   
   HANDLE_ERROR( cudaMemcpy( d_sc_y,sc_y, npixx*npixy*numeric_size, cudaMemcpyHostToDevice));

   HANDLE_ERROR( cudaMalloc( (void**)&d_sc_z, npixx*npixy*numeric_size)) ;   
   HANDLE_ERROR( cudaMemcpy( d_sc_z,sc_z, npixx*npixy*numeric_size, cudaMemcpyHostToDevice));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_n, 3*numeric_size));
   HANDLE_ERROR( cudaMemcpy(d_n,n, 3*numeric_size, cudaMemcpyHostToDevice));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_phase, ndip*numeric_size));
   HANDLE_ERROR( cudaMemcpy(d_phase,phase, ndip*numeric_size, cudaMemcpyHostToDevice));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_x, ndip*numeric_size));
   HANDLE_ERROR( cudaMemcpy(d_x,x, ndip*numeric_size, cudaMemcpyHostToDevice));

   HANDLE_ERROR( cudaMalloc( (void**)&d_y, ndip*numeric_size));
   HANDLE_ERROR( cudaMemcpy(d_y,y, ndip*numeric_size, cudaMemcpyHostToDevice));
   
   HANDLE_ERROR( cudaMalloc( (void**)&d_z, ndip*numeric_size));
   HANDLE_ERROR( cudaMemcpy(d_z,z, ndip*numeric_size, cudaMemcpyHostToDevice));
   
   
   if(n_launches>0){
        for(int i=0;i<n_launches;++i){
             dim3 grids(max_block,(npixx+DIM-1)/DIM,(npixy+DIM-1)/DIM); //Workers are launched in a 3D grid of execution (N_scatters, N_steps_to_cover_screen_in_x, N_steps_to_cover_screen_in_y)
             //Meaning that with DIM=16, and a 100x200 screen, one will need 100/16+1 = 7 steps in x and 200/16+1 steps in y to cover the screen with the 16x16 grid of workers, and this will be repeated
             //N_scatterer times for the scatterers.
             //Therefore we have : BlockDim.x=16, BlockDim.y=16 (how many threads)
             //And we have : GridDim.x=7,GridDim.y=13 (how many blocks)
             //N_particles is capped at 62k, because the block cannot be larger. So we loop over the kernel the necessary amount of times.
             int offs=i*max_block; //For every loop, the next 62k particles need to be taken.
             compute<<<grids,threads>>>(d_dthetaknk1, d_E_x,d_E_y,d_E_z,d_E_xi,d_E_yi,d_E_zi,d_rP0,d_rP1,d_rP2,d_iP0,d_iP1,d_iP2,d_r_px,d_r_py,d_r_pz,d_i_px,d_i_py,d_i_pz,d_sc_x,d_sc_y,d_sc_z,d_n,d_phase,d_x,d_y,d_z,offs,npixx,npixy);
        }
             dim3 grids(last_launch,(npixx+DIM-1)/DIM,(npixy+DIM-1)/DIM); //For the last loop, the correct ammount of particles needs to be taken.
            compute<<<grids,threads>>>(d_dthetaknk1, d_E_x,d_E_y,d_E_z,d_E_xi,d_E_yi,d_E_zi,d_rP0,d_rP1,d_rP2,d_iP0,d_iP1,d_iP2,d_r_px,d_r_py,d_r_pz,d_i_px,d_i_py,d_i_pz,d_sc_x,d_sc_y,d_sc_z,d_n,d_phase,d_x,d_y,d_z,n_launches*max_block,npixx,npixy);
       }
   else{
            dim3 grids(ndip,(npixx+DIM-1)/DIM,(npixy+DIM-1)/DIM);
            compute<<<grids,threads>>>(d_dthetaknk1, d_E_x,d_E_y,d_E_z,d_E_xi,d_E_yi,d_E_zi,d_rP0,d_rP1,d_rP2,d_iP0,d_iP1,d_iP2,d_r_px,d_r_py,d_r_pz,d_i_px,d_i_py,d_i_pz,d_sc_x,d_sc_y,d_sc_z,d_n,d_phase,d_x,d_y,d_z,0,npixx,npixy);
       }
       
   //At every kernel call the x,y,z components of the real and imaginary fields are updated by reference on Card.
   //Now we need the 'boats' again to ship back the data from the GPU to the CPU    
   cudaMemcpy(E_x,d_E_x,npixx*npixy*numeric_size, cudaMemcpyDeviceToHost ); 

   cudaMemcpy(E_y,d_E_y,npixx*npixy*numeric_size, cudaMemcpyDeviceToHost ); 
   cudaMemcpy(E_z,d_E_z,npixx*npixy*numeric_size, cudaMemcpyDeviceToHost ); 
   cudaMemcpy(E_xi,d_E_xi,npixx*npixy*numeric_size, cudaMemcpyDeviceToHost ); 
   cudaMemcpy(E_yi,d_E_yi,npixx*npixy*numeric_size, cudaMemcpyDeviceToHost ); 
   cudaMemcpy(E_zi,d_E_zi,npixx*npixy*numeric_size, cudaMemcpyDeviceToHost );    
   
   //make sure all the allocated memory on card is destroyed. Otherwise there could be problems at sweep stages, the card isn't always reinitialzied.
   cudaFree(d_dthetaknk1);
   cudaFree(d_E_x);
   cudaFree(d_E_y);
   cudaFree(d_E_z);
   cudaFree(d_E_xi);
   cudaFree(d_E_yi);
   cudaFree(d_E_zi);
   cudaFree(d_rP0);
   cudaFree(d_rP1);
   cudaFree(d_rP2);
   cudaFree(d_iP0);
   cudaFree(d_iP1);
   cudaFree(d_iP2);
   cudaFree(d_r_px);
   cudaFree(d_r_py);
   cudaFree(d_r_pz);
   cudaFree(d_i_px);
   cudaFree(d_i_py);
   cudaFree(d_i_pz);
   cudaFree(d_sc_x);
   cudaFree(d_sc_y);
   cudaFree(d_sc_z);
   cudaFree(d_n);
   cudaFree(d_phase);
   cudaFree(d_x);
   cudaFree(d_y);
   cudaFree(d_z);
   
};

int main(void){return 0;}

