import os
import numpy as np
from numpy import *

from os import sys,path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
sys.path.append(path.dirname(path.abspath(__file__)))

from SharedLibraries.Database.dataBaseOperations import loadDB
from auxiliaries.writeLogFile import loadDB

import datetime
import time
from variables import FieldSimulationInputs,FieldSimulationResults
import pandas as pd
pd.set_option('display.max_rows', None)
"""Loads and displays the database content in a quick fashion"""

number = 0

path = 'data/simulations.db'


simulations = loadDB(path,'results',returnDataFrame=True)
log = loadDB(path,'log',returnDataFrame=True)

print simulations.sort_index().ix[number,:]
print log.ix[number,:]

