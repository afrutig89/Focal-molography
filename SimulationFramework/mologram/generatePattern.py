# -*- coding: utf-8 -*-
"""
Mologram patterns on xy-plane

:Author:    Silvio
:Created:   Fri Mar 4 15:06:42 2016
"""

import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from generateMask import *

# from shapely.geometry import Point
import matplotlib.patches as patches

try:
    from numba import jit
    numba_installed = True
except:
    print "Numba is not properly installed on your system!"
    numba_installed = False


def placeIncScatterer(N, z_min,z_max, w=None, h=None, center=(0, 0), masks=None,seedBragg = 1):
    """generates a pattern with randomly placed scatterer on a disk (default with radius 1) or an a rectangle.

    :param N: number of scatterers
    :param z_min: minimal z distance of the scatterers
    :param z_max: maximal z distance of the scatterers
    :param w: width of the rectangle where the scatterers are placed (define only if a rectangle is required)
    :param h: height of the rectangle where the scatterers are placed (define only if a rectangle is required)
    :param (boolean) seedBragg: also seed the Bragg area with incoherent scatterers
    """
    if(N == 0):
        return [], [],[]

    x_rand = np.random.uniform(-w/2 +
                               center[0], w/2+center[0], int(max(1e3, N*3)))
    y_rand = np.random.uniform(-h/2 +
                               center[1], h/2+center[1], int(max(1e3, N*3)))
    z_rand = np.random.uniform(z_min,z_max,int(N))

    masked = np.zeros(len(x_rand), dtype=bool)
    if (masks != None):

        for mask in masks:


            if seedBragg and ('annulus' in mask.shape):
                continue

            masked = (masked | mask.applyMask(x_rand,y_rand))

    x_rand = x_rand[masked == False]
    y_rand = y_rand[masked == False]

    a = np.arange(0, N)

    return x_rand[a], y_rand[a],z_rand


def generateMologramExact(wavelength, focalPoint, diameter, N_particles,
                          n_m, N_eff, p=1, masks=None):
    """
    Returns scatterer positions randomly distributed over the mololines
    according to Eq. 4 in Fattinger.

    :param wavelength: wavelength of the incident light
    :param focalPoint: focal point of the mologram
    :param diameter: diameter of the mologram
    :param N_particles: number of particles
    :param n_m: refractive index of the medium where the focal point is located
    :param N_eff: effective refractive index of the waveguide
    :param p: binding probability to the mololines
    :param masks: list of masks of the object Mask
    DEPRECIATED

    """

    f = abs(focalPoint)
    D = diameter
    j_0 = f*n_m/wavelength
    j_min = int((-1/2. * N_eff * D - j_0*wavelength +
                 np.sqrt(f**2*n_m**2 + n_m**2*(-D/2.)**2)) / wavelength)
    j_max = int((1/2. * N_eff * D - j_0*wavelength +
                 np.sqrt(f**2*n_m**2 + n_m**2*(D/2.)**2)) / wavelength)
    N_lines = j_max-j_min
    j, y = np.meshgrid(
        np.array(range(j_min, j_max)),
        np.random.uniform(-D/2., D/2., N_lines*10)
    )
#    # just one line
#    j,y = np.meshgrid(np.random.uniform(j_min,j_max),np.random.uniform(-D/2.,D/2.,N_lines*10))
#    # just on the x-axis
#    j,y = np.meshgrid(np.array(range(j_min,j_max)),0)

    x = (wavelength*N_eff*(j_0+j) -
         sqrt(n_m**2*(N_eff**2-n_m**2)*(y**2+f**2) +
              (n_m*wavelength)**2*(j_0+j)**2)) / \
        (N_eff**2-n_m**2)

    x_mologram = x
    y_mologram = y

    masked = False
    if (masks != None):
        for mask in masks:
            if ("disk" in mask.shape):
                masked = (masked | diskMask(x_mologram, y_mologram,
                                            mask.radius, mask.center, mask.inverted))
            elif ("rectangle" in mask.shape or "square" in mask.shape):
                masked = (masked | rectangleMask(x_mologram, y_mologram,
                                                 mask.width, mask.height, mask.center, mask.inverted))
            elif ("annulus" in mask.shape):
                masked = (masked | annulusMask(x_mologram, y_mologram,
                                               mask.r_inner, mask.r_outer, mask.center, mask.inverted))
            else:
                continue

    x_constr = x_mologram[masked == False]
    y_constr = y_mologram[masked == False]

    a = np.arange(0, len(x_constr))
    np.random.shuffle(a)
    n = a[0:int((2*p-1)*N_particles)]

    x_c = x_constr[n]
    y_c = y_constr[n]

    x_back, y_back = generateRandom(
        N=(2*(1-p) * N_particles), w=D, h=D, masks=masks)

    x = np.hstack((x_c, x_back))
    y = np.hstack((y_c, y_back))

    return x, y


def generateMologram(wavelength, focalPoint, D, N_particles,
                     n_m, N_eff, p=1, masks=None):
    """
    Returns scatterer positions randomly distributed over ridges
    according to masterthesis, Silvio Bischof (Eq. 17 above).

    :param wavelength: wavelength of the incident light
    :param focalPoint: focal point of the mologram
    :param diameter: diameter of the mologram
    :param N_particles: number of particles
    :param n_m: refractive index of the medium where the focal point is located
    :param N_eff: effective refractive index of the waveguide
    :param p: binding probability to the mololines
    :param masks: list of masks of the object Mask

    :returns x,y: x,y coordinates of as horizontal stacks of coherent and incoherent particles.
    """

    x_mologram = np.random.uniform(-D/2, D/2, int(max(1e5, N_particles*3)))
    y_mologram = np.random.uniform(-D/2, D/2, int(max(1e5, N_particles*3)))

    masked = mologramMask(x_mologram, y_mologram,
                          wavelength, focalPoint, D, n_m, N_eff)

    if (masks != None):
        for mask in masks:
            if ("disk" in mask.shape):
                masked = (masked | diskMask(x_mologram, y_mologram,
                                            mask.radius, mask.center, mask.inverted))
            elif ("rectangle" in mask.shape or "square" in mask.shape):
                masked = (masked | rectangleMask(x_mologram, y_mologram,
                                                 mask.width, mask.height, mask.center, mask.inverted))
            elif ("annulus" in mask.shape):
                masked = (masked | annulusMask(x_mologram, y_mologram,
                                               mask.r_inner, mask.r_outer, mask.center, mask.inverted))
            else:
                continue

    x_constr = x_mologram[masked == False]  # array of size (1,N) needed
    y_constr = y_mologram[masked == False]

    if p < 0.5:
        p = 1-p
    # place all the coherent scatterers
    a = np.arange(0, len(x_constr))
    np.random.shuffle(a)
    n = a[0:int((2*p-1)*N_particles)]

    x_constr = x_constr[n]
    y_constr = y_constr[n]

    # place all the incoherent scatterers
    x_back, y_back = generateRandom(
        N=int((2-2*p)*N_particles), w=D, h=D, masks=masks)

    x = np.hstack((x_constr, x_back))
    y = np.hstack((y_constr, y_back))

    return x, y

def generateMologram_cf(mologram,resolution_of_line=1000):
    """generates the mologram according to Csabas implementation, this is the function that would need to be generalized. 

    The centre of the molographic lines (centre ridges) are given by formula 4 in Fattinger (Fattinger 2014) and implemented in this way in this function. 

    :param mologram: instance of class mologram

    Fattingers formula 4 is used.

    .. math:: b = n_m^2(N^2-n_m^2)

    .. math:: a = (N^2-n_m^2)

    .. math:: c = (n_m\\lambda)^2\\cdot(j_0+j)^2

    .. math:: d = b\\cdot f**2

    .. math:: r = \\lambdaN(j_0 + j)

    n_m is either n_c or n_s depending on which side the focal point is.

    """
    waveguide = mologram.waveguide
    N = waveguide.N
    n_m = mologram.n_m

    f = mologram.focalPoint
    j_0 = mologram.j_0  # shift of the mologram

    x0 = mologram.xshift

    b = n_m**2*(N**2-n_m**2)
    a = N**2-n_m**2

    # initialize the x and y corrdinate containers, all mololines that have a non-zero length will be stored in here.
    # the j's of the lines of non-zero length are stored in j_linesNonZeroLength.
    y_coordinates_mololines = np.zeros((1,resolution_of_line))
    x_coordinates_mololines = np.zeros((1,resolution_of_line))
    j_linesNonZeroLength = []

    for j in range(mologram.j_min, mologram.j_max):
        # some shorthand notation for hyperbola definitions below.
        c = (n_m*mologram.wavelength)**2*(j_0+j)**2

        d = b*f**2 + c

        r = mologram.wavelength*waveguide.N*(mologram.j_0+j)

        s = r/a

        a_n = d/(a*a)  # ok, I do not get this
        b_n = d/b  # ok I also do not get this

        R = mologram.radius
        Rb = mologram.braggRadius
        o = Rb + mologram.braggOffset

        # Only the upper half of the mologram is generated, every second
        # scatterer is subsequently mirrored, that is why shifts in y do not
        # work.

        # calculate intersections of the mologram inclusion circle, and the 2 1-st order exclusion circles of the bragg
        # intersection with first bragg circle

        x_br1 = (-a_n*o+b_n*s)/(a_n+b_n)-np.sqrt(a_n*a_n*b_n+a_n*b_n*b_n - a_n*b_n *
                                                 o*o + a_n*a_n*Rb*Rb + a_n*b_n*Rb*Rb-a_n*b_n*s*s-2*a_n*b_n*o*s)/(a_n+b_n)
        y_br1 = np.sqrt(-a_n*b_n+b_n*s*s-2*b_n*s*x_br1 +
                        b_n*x_br1*x_br1)/np.sqrt(a_n)

        # intersection with second bragg circle
        o = Rb - mologram.braggOffset

        x_br2 = (-a_n*o+b_n*s)/(a_n+b_n)-np.sqrt(a_n*a_n*b_n+a_n*b_n*b_n - a_n*b_n *
                                                 o*o + a_n*a_n*Rb*Rb + a_n*b_n*Rb*Rb-a_n*b_n*s*s-2*a_n*b_n*o*s)/(a_n+b_n)
        y_br2 = np.sqrt(-a_n*b_n+b_n*s*s-2*b_n*s*x_br2 +
                        b_n*x_br2*x_br2)/np.sqrt(a_n)

        if mologram.shape == 'disk':

            # intersection of the line with the molocircle (this needs to be
            # adapted to allow for rectangular)

            x_s = (2*a*r + 2*b*x0 - np.sqrt((-2*a*r - 2*b*x0)**2 - 4 *
                                            (a**2+b)*(-c-b*f**2+r**2-b*R**2+b*x0**2)))/(2*(a**2+b))
            y_s = np.sqrt(R**2 - (x_s-x0)**2)

        if mologram.shape == 'rectangular':

            y_s = mologram.height/2.
            x_s = (r-np.sqrt(b*f**2+b*y_s**2+c))/a

            # only the left half of it is correctly implemented.
            if x_s < (-R + x0):
                # calculate y_s

                x_s = (-R+x0)
                y_s = np.sqrt(-c-b*f**2+r**2-2*a*r*x_s+a**2*x_s**2)/np.sqrt(b)

        which_line = np.nanargmin((y_s, y_br1, y_br2))

        # if nan then there is no intersection. Count the nan.
        how_many_miss = np.sum(np.isnan((y_s, y_br1, y_br2)))

        # these are the mololines that don't touch the exclusion circles
        if which_line == 0 and (how_many_miss == 0 or how_many_miss == 2):
            y = np.linspace(0, y_s, resolution_of_line)
            j_linesNonZeroLength.append(j)

        if which_line == 0 and how_many_miss == 1:  # these are the mololines who are completely in between the two exclusion circles, and never touch them before the inclusion circle
            continue

        # these are the lines that lie partly between the exclusion circles,
        # but a part is still inside the mologram (left-side)
        if which_line == 1:
            y = np.linspace(0, y_br1, resolution_of_line)
            j_linesNonZeroLength.append(j)

        if which_line == 2:  # same as previous line, but right-side.
            y = np.linspace(y_br2, y_s, resolution_of_line)
            j_linesNonZeroLength.append(j)

        y_coordinates_mololines = np.vstack((y_coordinates_mololines, y))
        x_coordinates_mololines = np.vstack((x_coordinates_mololines, (r - np.sqrt(b*y**2+d))/a))
    
    # discard the initial dummy line. 
    x_coordinates_mololines = x_coordinates_mololines[1:, :]
    y_coordinates_mololines = y_coordinates_mololines[1:, :]

    # compute the arc lengths of the different segments of all different mololines
    # matrix of size (length(j_linesNonZeroLength),resolution_of_line-1)
    segment_lengths = np.sqrt(1+(np.diff(y_coordinates_mololines, 1)/np.diff(x_coordinates_mololines, 1))**2)*(-np.diff(x_coordinates_mololines, 1))


    # compute the matrix of the normalized arc lengths up to different segments, this goes from 0 to 1.
    normalizedArcLengths = np.cumsum(np.hstack((np.zeros((len(segment_lengths), 1)), segment_lengths)
                               )/np.sum(segment_lengths, 1).reshape(len(segment_lengths), 1), 1)

    # sum up the individual segments of every molographic line to a (1,length(j_linesNonZeroLength)) vector of all the lengths of the molographic lines.
    # attention this is only the upper part of the line.
    lengthsMololines = np.sum(segment_lengths, 1)
    
    return x_coordinates_mololines, y_coordinates_mololines, normalizedArcLengths, lengthsMololines, np.asarray(j_linesNonZeroLength)

def seed(N_scatterers, normalizedArcLengths, random_cdf_inverse_indices, lines_to_deposit, x_coordinates_mololines, y_coordinates_mololines, x, y, z, distancesBetweenTwoRidges, ridge_shift, bindingProbabilityRidge, zmin, zmax, groove_vs_ridge, z_shifter):
    
    """This function places N scatterers on the mologram. It is the sub-function used in place_scatterers_fast.
    Given the line lengths, it weightedly selects one, places a scatterer on its exact middle, and depending on the binding probability,
    will sinusoidally shift their x-position on the line, or into the groove on the left of the line.
    
    :param N_scatterers: vector of length of the number of scatterers
    :param cdf: vector of vectors of the normalized arc lengths for the mololines (goes from 0 to 1)
    :param random_cdf_inverse_indices: vector of uniformly sampled indices [0,1] for computation of the location of the particles, has length Number of particles. 
    :returns x,y,z: returns three 1-dimensional arrays of the x,y,z location of the scatterers.
    
    """

    for i in range(N_scatterers):
        # A N_scatterer length vector of lines to seed was created in
        # place_scatterers already.
        line_idx = lines_to_deposit[i]
        distanceBetweenTwoRidges = distancesBetweenTwoRidges[line_idx]  # Find the width of this line
        # If we're on the left-edge of the mologram, there isn't a
        # more-left line.
        if line_idx == 0:
            line_idx2 = line_idx
        else:
            line_idx2 = line_idx-1

        # Inverse sampling : find the closest entry to the uniform random
        # sampling (random_cdf..)
        sampled_cdf_index = np.argmin(
            np.abs(normalizedArcLengths[line_idx] - random_cdf_inverse_indices[i]))

        # get an x coordinate from one of the ridges. 
        x[i] = x_coordinates_mololines[line_idx, :][sampled_cdf_index]
        y[i] = y_coordinates_mololines[line_idx, :][sampled_cdf_index]

        # this particle binds to the grooves!
        if groove_vs_ridge[i] > bindingProbabilityRidge:

            raise NotImplementedError, 'Grooves Placement is not implemented'
            # # calculate the size of the grove to the left of the line
            # left_bdry = x_coordinates_mololines[line_idx2, :][
            #     sampled_cdf_index] + distancesBetweenTwoRidges[line_idx2]/4.
            # right_bdry = x_coordinates_mololines[line_idx, :][sampled_cdf_index]-distancesBetweenTwoRidges/4.
            # size_bdry = right_bdry - left_bdry
            # # the shift is a quarter of the line-distancesBetweenTwoRidges (from center to
            # # left-edge) + a sinusoidal seeding the size of the grove
            # shift = distancesBetweenTwoRidges/4. + size_bdry*ridge_shift[i]
        
        else:
            # sinusoidal seeding on [-distancesBetweenTwoRidges/4,distancesBetweenTwoRidges/4]
            shift = distanceBetweenTwoRidges/2 * ridge_shift[i]
        
        # -sign, since the left-shift was positive, and doesn't change anything on the mid-shift, since it's centered.
        x[i] -= shift

        z[i] = z_shifter[i]*(zmax - zmin) + zmin

    return x, y, z

def seed_mid_line(N_scatterers, mologram, zmin, zmax):
 # seeds roughly uniformly the mid-cutout section.

    R = mologram.braggRadius
    dist_min = mologram.braggRadius - mologram.braggOffset
    dist_max = mologram.braggRadius + mologram.braggOffset
    r = mologram.radius
    # choose a starting x position on the bragg sectors, there are 10000
    # different starting positions on the sector.
    x_start = dist_min + np.random.rand(10000)*(dist_max-dist_min)

    if mologram.shape == 'disk':
        # calculate the intersection between the seed-circle and the
        # inscribing molo-circle
        y_seg = (1/x_start*np.sqrt(4*x_start*x_start *
                                   R*R-(x_start*x_start-r*r+R*R)**2))/2
        # calculate the intersection between the seed-circle and the
        # inscribing molo-circle
        x_seg = (x_start*x_start-r*r+R*R)/(2*x_start)
        # for each scatterer choose one of the 10000 bragg lines to seed
        # on.
        chose_line = np.random.randint(
            low=0, high=10000, size=N_scatterers)
        x_pos = -(x_start-R)[chose_line]  # place the 'radius' (uniform)
        # calculate the angle sectors
        angle = np.arctan(y_seg/x_seg)[chose_line]

        # sample the angles uniformly on their sectors.
        rand_angle = angle*np.random.rand(N_scatterers)
        y_pos = R*np.sin(rand_angle)
        x_pos = x_pos-(R*(1-np.cos(rand_angle)))


    if mologram.shape == 'rectangular':
        y_seg = mologram.height/2.*np.ones(len(x_start))
        # for each scatterer choose one of the 10000 bragg lines to seed
        # on.
        chose_line = np.random.randint(
            low=0, high=10000, size=N_scatterers)

        # angle of the circle segment is the same for all.
        angle = np.arcsin(y_seg/R)[chose_line]

        # sample the angles uniformly on their sectors.
        rand_angle = angle*np.random.rand(N_scatterers)

        y_pos = R*np.sin(rand_angle)
        x_pos = -(x_start-R)[chose_line] - (R*(1-np.cos(rand_angle))) - R
        # place them randomly on z.

    z_pos = zmin + (zmax - zmin)*np.random.rand(N_scatterers)

    return x_pos, y_pos, z_pos


def place_scatterers_cf_fast(N_scatterers, generatedMologram, bindingProbabilityRidge, z_min, z_max,ridge_shift,mologram):
        
    """
    This function prepares the relevant quantities and random arrays to pass the "seed" function to seed the scatterers. Such a convoluted structure is needed
    because the seed function is jit compiled, and as such cannot create arrays (so we cannot create the random arrays inside the jit-ted function)

    """

    x_coordinates_mololines, y_coordinates_mololines, normalizedArcLengths, lengths,j_linesNonZeroLength = generatedMologram

    # distance between two adjacent mololines.
    distancesBetweenTwoRidges = mologram.calcLineDistances()[(j_linesNonZeroLength-j_linesNonZeroLength[0]).astype(int)]

    x = np.zeros(N_scatterers)
    y = np.zeros(N_scatterers)
    z = np.zeros(N_scatterers)
    num_lines = len(x_coordinates_mololines)

    # normalize lenghts as 1-probability vector
    probabilities = lengths/np.sum(lengths)
    line_indices = np.arange(num_lines)

    # choses N_scatterer lines from the available line indices given the
    # prob weight vector.
    lines_to_deposit = np.random.choice(
        line_indices, N_scatterers, True, probabilities)

    # generate a N_scatterer-length  uniform seed on [0,1].
    random_cdf_inverse_indices = np.random.rand(N_scatterers)

    # entries above bindingProbabilityRidge 
    groove_vs_ridge = np.random.rand(N_scatterers)

    # uniform random array to decide the z_shift.
    z_shifter = np.random.rand(N_scatterers)

    # use the seed_function described above.
    # if numba_installed == True:
    #     seed = jit(seed,nopython=True)

    x, y, z = seed(N_scatterers, normalizedArcLengths, random_cdf_inverse_indices, lines_to_deposit, x_coordinates_mololines, y_coordinates_mololines,
                   x, y, z, distancesBetweenTwoRidges, ridge_shift, bindingProbabilityRidge, z_min, z_max, groove_vs_ridge, z_shifter)

    return x, y, z, groove_vs_ridge

def seed_mologram_cf(N_scatterers, generatedMologram, bindingProbabilityRidge, z_min, z_max, mologram, seed_mid=True, placement='Ridges (Sinusoidal)'):
    """This function uses the output of generateMologram_cf
       - mologram, seed_mid: to be able to seed on the missing arc (bragg)
        -z_min,z_max: to know what height to place the scatterers."""

    # generate the distribution functions for the particle placement on the ridges, all have a centerpoint of 0.5.
    if placement == 'Ridges (Sinusoidal)':
        # inverse CDF of a sinusoidal seeding on the ridges one can sample directly from.
        # gives a sinusoidal distribution between -0.5 and 0.5.
        ridge_shift = np.arccos(1-2*np.random.rand(N_scatterers))/np.pi - 0.5
        # ATTENTION: Wrong assumption, real CDF is in AF0025
        # ATTENTION: Need to define a groove shift, if I was to implement this properly. 
        # groove_shift = 

    elif placement == 'Ridges (Rectangular)':
        # inverse CDF one can sample directly from.
        ridge_shift = np.random.rand(N_scatterers) - 0.5

    elif placement == 'Mololines':
        ridge_shift = np.zeros(N_scatterers) # no shift because directly on the moloLine. 

    if(seed_mid == True):
        N_molo = int(mologram.Area_ratio*N_scatterers)
        N_mid = N_scatterers - N_molo

        x_mid, y_mid, z_mid = seed_mid_line(
            N_mid, mologram, z_min, z_max)
        x_molo, y_molo, z_molo, bind_no_bind = place_scatterers_cf_fast(
            N_molo, generatedMologram, bindingProbabilityRidge, z_min, z_max, ridge_shift,mologram)

        bind_no_bind = np.hstack((np.zeros(x_mid.shape), bind_no_bind))
        x_ret = np.hstack((x_mid, x_molo))
        y_ret = np.hstack((y_mid, y_molo))
        z_ret = np.hstack((z_mid, z_molo))
    else:
        x_ret, y_ret, z_ret, bind_no_bind = place_scatterers_cf_fast(
            N_scatterers, generatedMologram, bindingProbabilityRidge, z_min, z_max, ridge_shift,mologram)
    
    return x_ret, y_ret, z_ret, bind_no_bind



def linewidthFromDataUnits(linewidth, axis, reference='y'):
    """
    Convert a linewidth in data units to linewidth in points.

    :param linewidth: Linewidth in data units of the respective reference-axis
    :type linewidth: float
    :param axis: The axis which is used to extract the relevant transformation data (data limits and size must not change afterwards)
    :type axis: matplotlib axis
    :param reference: The axis that is taken as a reference for the data width. Possible values: 'x' and 'y'. Defaults to 'y'.
    :type reference: string
    :returns: Linewidth in points
    :rtype: float

    """
    fig = axis.get_figure()
    if reference == 'x':
        length = fig.bbox_inches.width * axis.get_position().width
        value_range = np.diff(axis.get_xlim())
    elif reference == 'y':
        length = fig.bbox_inches.height * axis.get_position().height
        value_range = np.diff(axis.get_ylim())

    # Convert length to points
    length *= 72
    # Scale linewidth to value range
    return linewidth * (length / value_range)


def plotPattern(x, y, markersize=1, title='', xlim=None, ylim=None,fontSizeTicks=1):
    """ plots the pattern given by x, y (in meter)

    :param x: x components of the pattern (will be plotted in um)
    :param y: y components of the pattern (will be plotted in um)
    :param markersize: size of the scatterers (1 by default)
    :param title: title of the plot
    """
    plt.plot(x*1e6, y*1e6, 'o', color='black', markersize=markersize)
    if xlim != None:
        plt.xlim(xlim*1e6)
    if ylim != None:
        plt.ylim(ylim*1e6)
    if xlim == None and ylim == None:
        plt.axis('equal')

    plt.xticks(size=fontSizeTicks)
    plt.yticks(size=fontSizeTicks)
    plt.xlabel('x in um')
    plt.ylabel('y in um')
    if title != '':
        plt.title(title)


def savePattern(x_sca, y_sca, title='Scatterer Pattern', xlim=[-inf, inf], ylim=[-inf, inf], mologram=None):
    """ plots the pattern given by x, y (in meter)

    :param x: x components of the pattern (will be plotted in um)
    :param y: y components of the pattern (will be plotted in um)
    :param markersize: size of the scatterers (1 by default)
    :param title: title of the plot
    """

    if xlim == [-inf, inf] and ylim == [-inf, inf]:
        zoomed = False
        fig = plt.figure(figsize=figSizeSingle)
        plotPattern(x_sca, y_sca, markersize=3/np.log10(size(x_sca)))
    else:
        zoomed = True
        fig = plt.figure(figsize=figSizeDouble)
        ax = fig.add_subplot(121, aspect='equal')
        plotPattern(x_sca, y_sca, markersize=3/np.log10(size(x_sca)))
        ax.add_patch(
            patches.Rectangle(
                (xlim[0]*1e6, ylim[0]*1e6),   # (x,y)
                xlim[1]*1e6-xlim[0]*1e6,          # width
                ylim[1]*1e6-ylim[0]*1e6,          # height
                fill=False,
                edgecolor='red'
            )
        )
        ax2 = fig.add_subplot(122, axisbg=(
            253/255., 235/255., 0), xlim=(xlim[0], xlim[1]), ylim=(ylim[0], ylim[1]))

    # plot mologram
    if mologram != None:
        f = abs(mologram.focalPoint)
        n_m = mologram.n_m
        N_eff = mologram.waveguide.N
        wavelength = mologram.wavelength

        j_0 = mologram.j_0

        ylim[0] = np.max(ylim[0], -mologram.radius)
        ylim[1] = np.min(ylim[1], mologram.radius)

        j_min = int((N_eff * xlim[0] - j_0*wavelength +
                     np.sqrt(f**2*n_m**2 + n_m**2*(xlim[0])**2)) / wavelength)
        j_max = int((N_eff * xlim[1] - j_0*wavelength + np.sqrt(f **
                                                                2*n_m**2 + n_m**2*(xlim[1])**2)) / wavelength)+20

        N_lines = j_max-j_min

        j, y = np.meshgrid(np.array(range(j_min, j_max)),
                           np.linspace(ylim[0], ylim[1], N_lines*1000))

        x = (wavelength*N_eff*(j_0+j) -
             sqrt(n_m**2*(N_eff**2-n_m**2)*(y**2+f**2)+(n_m*wavelength)**2*(j_0+j)**2)) / \
            (N_eff**2-n_m**2)

        for i in range(y.shape[1]-1):
            pointlinewid = linewidthFromDataUnits((x[0, i+1]-x[0, i])/2., ax2)
            plt.plot(x[:, i]*1e6, y[:, i]*1e6, linewidth=pointlinewid,
                     color=(247/255., 177/255., 50/255.))

    if zoomed:
        x_s = x_sca[((x_sca > xlim[0]) & (x_sca < xlim[1]) &
                     (y_sca > ylim[0]) & (y_sca < ylim[1]))]
        y_s = y_sca[((x_sca > xlim[0]) & (x_sca < xlim[1]) &
                     (y_sca > ylim[0]) & (y_sca < ylim[1]))]
        markersize = linewidthFromDataUnits(50e-9, ax2)
        plt.plot(x_s*1e6, y_s*1e6, 'o', color='black', markersize=markersize)
        title = title + " (zoomed)"

        plt.xlim(xlim[0]*1e6, xlim[1]*1e6)
        plt.ylim(ylim[0]*1e6, ylim[1]*1e6)
        plt.xticks(size=fontSizeTicks)
        plt.yticks(size=fontSizeTicks)
        plt.xlabel('x in um')
        plt.ylabel('y in um')
        if title != '':
            plt.title(title)

        fig.tight_layout()

    if showPlots:
        plt.show()
    if savePlots:
        if zoomed:
            fig.savefig(folder + '/pattern_zoomed_' + str(counter) +
                        '.' + imageFormat, format=imageFormat, dpi=dpi)
        else:
            fig.savefig(folder + '/pattern_' + str(counter) +
                        '.' + imageFormat, format=imageFormat, dpi=dpi)

################################################################################################
################################################################################################
# functions for testing purposes:

def TestGenerateMologramCF():
    """plots the mologram generates by Csabas function"""

    from mologram import Mologram
    mologram = Mologram.standardMologramWithBragg()

    mologram_seed_parameters = generateMologram_cf(mologram)
    x_coordinate_mololines, y_coordinates_mololines, _, _, j_linesNonZeroLength = mologram_seed_parameters

    # convert to um
    x_coordinate_mololines *= 1e6
    y_coordinates_mololines *= 1e6
    distancesBetweenTwoRidges = mologram.calcLineDistances()[(j_linesNonZeroLength-j_linesNonZeroLength[0]).astype(int)]*1e6
    print distancesBetweenTwoRidges
    fig = plt.figure()
    ax = fig.add_subplot(111)

    for i in range(y_coordinates_mololines.shape[1]-1):
        try:
            # lines outside the mologram range are nan, therefore we need a try
            ax.fill_betweenx(y_coordinates_mololines[i], x_coordinate_mololines[i]-distancesBetweenTwoRidges[i]/4.,x_coordinate_mololines[i]+distancesBetweenTwoRidges[i]/4.,color='r',alpha=0.1)
            ax.fill_betweenx(-y_coordinates_mololines[i], x_coordinate_mololines[i]-distancesBetweenTwoRidges[i]/4.,x_coordinate_mololines[i]+distancesBetweenTwoRidges[i]/4.,color='r',alpha=0.1)
        except:
            continue

    plt.show()

def TestSeedMologram():

    from mologram import Mologram
    mologram = Mologram.standardMologramWithBragg()

    generatedMologram = generateMologram_cf(mologram)
    N_scatterers = 100
    bindingProbabilityRidge = 1.
    z_min = 0.
    z_max = 0.

    x_ret, y_ret, z_ret, bind_no_bind = seed_mologram_cf(N_scatterers, generatedMologram, bindingProbabilityRidge, z_min, z_max, mologram, seed_mid=False, placement='Ridges (Sinusoidal)')

    print len(x_ret)

    


if __name__ == '__main__':


    test = np.arccos(1-2*np.random.rand(10000))/np.pi

    plt.hist(test)
    plt.show()
    #TestGenerateMologramCF()
    TestSeedMologram()

    # x,y,z = generateMologram(632.8e-6, -900e-6, 400e-6, 100000,
    #                  1.521, 1.81, p=0.504, masks=None)

    import sys
    sys.path.append('../')
    from waveguide.waveguide import SlabWaveguide           # waveguide calculations

    from mologram import Mologram

    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
        n_c=1.33, d_f=145e-9, polarization='TE',
        inputPower=1, wavelength=632.8e-9, attenuationConstant=0)
    mologram = Mologram(waveguide, focalPoint=-900e-6, diameter=400e-6,braggOffset=25e-6)

    # x,y,z = generateRandom(1000000, 0,10e-9, w=mologram.width, h=mologram.height, center=mologram.center, masks=mologram.mask,seedBragg = 0)

    plt.show()

    N = 1000000
    print(np.sqrt(N*0.504*(1-0.504)))
