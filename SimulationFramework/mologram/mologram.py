import numpy as np
from numpy import sqrt
import matplotlib.pyplot as plt
from generateMask import *
import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

from generatePattern import *              # coherent pattern
import datetime
from gdsCAD_local import core, shapes, utils
from gdsCAD_local.boolean_AF import substraction
from generateMask import Mask
from scipy.optimize import fsolve
import time
from checkMologram import maximumNumericalApertureIfmOrderNotPresent,maximumNumericalApertureIfmBraggOrderNotPresent
from variables import MologramParameter
from waveguide.waveguide import SlabWaveguide

def diameterGivenNA_F_n(f,NA,n):
    """

    .. math:: D = 2 \\cdot f \\cdot \\tan \\left[ {\\arcsin \\left[ {\\frac{{NA}}{{{n_s}}}} \\right]} \\right]
    """

    return 2*f*np.tan(np.arcsin(NA/n))

def numericalApertureGivenF_D_n(f,D,n):
    """

    .. math:: NA = \\sin \\left[ {\\arctan \\left[ {\\frac{D}{{2 \\cdot f}}} \\right]} \\right] \\cdot {n_s}

    """

    return np.sin(np.arctan(D/(2.*abs(f))))*n

def focalDistanceGivenNA_D_n(NA,D,n):
    """

    .. math:: f = \\frac{D}{{2tan\\left[ {\\arcsin \\left[ {\\frac{{NA}}{{{n_s}}}} \\right]} \\right]}}

    """

    return D/(2*np.tan(np.arcsin(NA/n)))

def calcBraggOffset(f, braggTheta):

    return abs(f)*tan(braggTheta)

class Mologram(MologramParameter):
    """
    This is a class that calculates a mologram for a given waveguide,
    wavelength, focus and radius.

    :param waveguide: waveguide object
    :param focalPoint: focal point distance of the mologram, default None, at least two of the parameters f,d,NA need to be specified.
    :param diameter: radius of the mologram, default None, at least two of the parameters f,d,NA need to be specified.
    :param NA: numerical aperture of the mologram, default None, at least two of the parameters f,d,NA need to be specified.
    :param xshift: offset of the molographic structure in x direction.
    :param braggRadius: outer radius of the Bragg circle
    :param braggOffset: offset of the Bragg circle (half thickness of the annulus)
    :param mask: list of mask objects that are applied to the mologram (only important for phasemask framework)
    :param shape: shape of the molograms, implemented are 'disk' and 'rectangular'
    :param aspectRatio: additional parameter for rectangular molograams, only the height is changed the width is equal to the diameter.
    :param fieldSimulation: If true then the molographic lines are created for the seed mologram function that is used in the fieldSimulation framework
    :param direction: on which side of the waveguide the mologram is -1 is below the waveguide, +1 is above the waveguide, default is -1

    """

    def __init__(self, waveguide, focalPoint=0,
                 diameter=0,NA=-1,xshift=0, braggRadius=-1, braggOffset=-1, shape='disk', aspectRatio=1,fieldSimulation= False,braggTheta=-1,direction = -1):

        self.waveguide = waveguide
        self.wavelength = waveguide.wavelength

        self.n_m = waveguide.n_s if np.sign(direction) < 0 else waveguide.n_c

        if (focalPoint != 0) & (diameter != 0):
            print NA
            assert NA == -1, "Overdefined mologram, please only specify two of the variables, focalPoint,NA and diameter. The third one must be 0!"

            self.focalPoint = abs(focalPoint)
            self.radius = diameter/2.
            self.NA = numericalApertureGivenF_D_n(self.focalPoint,diameter,self.n_m)

        if (focalPoint != 0) & (NA != -1):

            assert diameter == 0., "Overdefined mologram, please only specify two of the variables, focalPoint,NA and diameter. The third one must be 0!"

            self.NA = NA
            self.focalPoint = abs(focalPoint)
            self.radius = diameterGivenNA_F_n(self.NA,self.focalPoint,self.n_m)/2.

        if (diameter != 0) & (NA != -1):

            assert focalPoint == 0., "Overdefined mologram, please only specify two of the variables, focalPoint,NA and diameter. The third one must be 0!"

            self.radius = diameter/2.
            self.NA = NA
            self.focalPoint = focalDistanceGivenNA_D_n(NA,diameter,self.n_m)

        self.j_0 = self.focalPoint*self.n_m/self.wavelength

        self.xshift = xshift
        self.center = (xshift,0)
        self.direction = direction


        if braggRadius == -1:
            self.braggRadius = self.calcBraggCircle()
        else:
            self.braggRadius = braggRadius

        if braggTheta == -1:
            self.braggTheta = 2*np.pi/180 # Bragg theta not specified => set to 2 degrees (2pi/180 radians)
        else:
            self.braggTheta = braggTheta*np.pi/180 # Bragg theta specified => use this as theta

        if braggOffset == -1:
            self.braggOffset = calcBraggOffset(self.focalPoint, self.braggTheta) # Bragg offset not specified => calculate based on theta
        else:
            self.braggOffset = braggOffset # Bragg offset specified => use this offset
            self.braggTheta = np.arctan(abs(self.braggOffset/self.focalPoint)) # require Bragg theta to be consistent with the Bragg offset

        self.shape = shape
        self.width = self.radius*2
        self.height = self.radius*2*aspectRatio  # the width is equal to the diameter
        self.aspectRatio = aspectRatio

        self.generateMaskList() # generate the mask list for the Phase mask and the placement of the incoherent scatterers.

        # Here I would need to generate the appropriate masks to be consistent.
        f = self.focalPoint

        n_m = self.n_m
        N = self.waveguide.N

        j_0 = self.j_0
        self.j_min = int((N * (-self.radius + self.xshift) - j_0*self.wavelength +
                          np.sqrt(f**2*n_m**2 + n_m**2*(self.radius)**2)) / self.wavelength)
        self.j_max = int((N * (self.radius + self.xshift) - j_0 * self.wavelength +
                          np.sqrt(f**2*n_m**2 + n_m**2*(self.radius)**2)) / self.wavelength)

        self.Area_ratio = self.area_ratio()

        # needs quite some time, hence this is not executed, if the mologram does not need to be seeded.
        if fieldSimulation:
            self.mologram_seed_parameters = generateMologram_cf(self)

        # object for storing the mologram parameters to the database.
        super(Mologram,self).__init__()

        self.setValues() # create the database object for storage of the mologram parameters.


    @classmethod
    def standardMologramWithoutBragg(cls):

        waveguide = SlabWaveguide.standardWaveguide()

        return cls(waveguide,focalPoint=-900e-6, diameter=400e-6,braggOffset = 1e-9)


    @classmethod
    def standardMologramWithBragg(cls):

        waveguide = SlabWaveguide.standardWaveguide()

        return cls(waveguide,focalPoint=-900e-6, diameter=400e-6,braggOffset = 25e-6)


    def setValues(self):
        """Overrides the base class function setValues of the VariableContainer class
        here the values are collected from the object"""

        values = {
            'focalPoint':self.focalPoint*self.direction, # focal point (z-component) in [m]
            'diameter':self.radius*2.,  # maximum cell size in [m]
            'NA':self.NA,
            'braggOffset':self.braggOffset, # Bragg offset for mask in [m], if it is None than the it is calculated with an safety angle of 2, To do: specify bragg angle and if this one is also zero, then omit the bragg completely.
            'braggTheta':self.braggTheta,
            'braggRadius':self.braggRadius,
            'shape':self.shape, # shape of the mologram (disk/rectangular)
            'xshift':self.xshift, # shift in x direction relative to the center point of the molographic structure.
            'aspectRatio':self.aspectRatio, # default aspect ratio of the mologram, only relevant for rectangular molograms.
            'direction':self.direction
        }

        super(Mologram,self).setValues(values)


    def generateMaskList(self):

        self.mask = []

        if self.shape == 'rectangular':

            self.mask.append(Mask().rectangle(self.width, self.height,center=(self.xshift,0)),inverted=True)

        if self.shape == 'disk':

            self.mask.append(Mask().disk(self.radius,center=(self.xshift,0),inverted=True))

        self.mask.append(Mask().annulus(self.braggRadius - self.braggOffset, self.braggRadius +
                                            self.braggOffset, center=(-self.braggRadius + self.xshift, 0), inverted=False))

    def calcAiryDiskDiameterAnalytical(self):
        """calculates the expected Airy disk diameter of the mologram according to the following equation:

        .. math:: {d_A} = \\frac{{1.22\\lambda }}{{NA}}
        
        """

        return 1.22*self.wavelength/self.NA

    def calcAiryDiskAreaAnalytical(self):
        """calculates the expected Airy disk diameter of the mologram according to the following equation:

        .. math:: {A_{Airy}} = \\frac{{1.48{\\lambda ^2}\\pi }}{{N{A^2} \\cdot 4}} = 1.16\\frac{{{\\lambda ^2}}}{{N{A^2}}}
        
        """

        return 1.1618*self.wavelength**2/self.NA**2

    def figure_of_merit_FM(self,a_ani):

        D = self.radius*2

        return D**2*self.waveguide.figureOfMeritWGExperimental(a_ani)

    def calcBraggCircle(self):
        """
        Calculates the radius of the circle to avoid second order Bragg
        reflections. Modified to use not the first but the outermost Bragg point
        inside the Mologram radius. Returns the radius of this Bragg circle in meters.

        """
        braggcoords = self.calcCoordinatesFirstBraggInsideMologram()
        xcoords = braggcoords[0]
        ycoords = braggcoords[1]

        lastindex = len(ycoords)-1

        y = ycoords[lastindex]
        x = xcoords[lastindex]

        braggRadius = (0.5*abs((x**2 + y**2)/x))

        return braggRadius

    def calcCoordinatesFirstBraggInsideMologram(self):
        """
        This function calculates the coordinates of the all the Bragg points inside the Mologram radius
        defined by focal point and numerical aperture, and returns a numpy array with the x- and the y-
        coordinates (in meters), respectively.

        """

        f = self.focalPoint
        NA = self.NA

        ycoords = []
        xcoords = []
        ycoords.append(0) # origin
        xcoords.append(0) # origin

        Braggpoint = self.calcCoordinatesFirstBraggGeneral(10e-6)
        y = Braggpoint[1]
        x = Braggpoint[0]

        ycoords.append(y)
        xcoords.append(x)

        i = 2 # set counter

        while sqrt(x**2 + y**2) <= self.radius: # proceed as long as the last calculated point is inside the mologram radius
            Braggpoint = self.calcCoordinatesFirstBraggGeneral(10e-6, i)
            y = Braggpoint[1]
            x = Braggpoint[0]
            ycoords.append(y)
            xcoords.append(x)
            i = i + 1

        xcoords = np.array(xcoords)
        ycoords = np.array(ycoords)

        return xcoords, ycoords

    def calcCoordinatesFirstBraggGeneral(self, y_init, index = 1):
        """
        Calculates the x- and y-coordinates (in meters) of the Bragg point with index j.
        If no index is specified, the returned coordinates correspond to Bragg point (x_1, y_1).
        Note that index = 1 corresponds to grating line j = -1, and so on.
        Note: A Bragg point is defined as the point on the grating line with index j where the
        tangent to the grating line at that point goes through the origin.

        """

        waveguide = self.waveguide
        n_s = waveguide.n_s
        N = waveguide.N
        f = self.focalPoint
        wavelength = self.wavelength

        j_0 = f*n_s/wavelength

        func1 = lambda y: n_s**2*(N**2-n_s**2)*(f**2+y**2)+n_s**2*wavelength**2*(j_0-index)**2
        func2 = lambda y: ((j_0-index)*wavelength*N-sqrt(func1(y)))/(N**2-n_s**2)+y**2*n_s**2/sqrt(func1(y))

        y = fsolve(func2,y_init)

        j_0 = f*n_s/wavelength

        a = n_s**2*(N**2-n_s**2)*(f**2+y**2)+n_s**2*wavelength**2*(j_0-index)**2

        x = ((j_0-index)*wavelength*N-sqrt(a))/(N**2-n_s**2)

        return x, y

    def calcBraggHyperbolaparams(self, A_init):
        """
        Calculates the hyperbola to avoid second order Bragg reflections. The function fits a
        hyperbola through the origin, the outermost Bragg point inside the mologram, and
        the Bragg point whose index lies in the middle of the index of the outermost point and the origin.
        The function returns the parameters A, B, of the general hyperbola equation (x^2)/A + (y^2)/B = 1,
        as well as the x- and y-coordinates of the Bragg points (in meters).

        """
        braggcoords = mologram.calcCoordinatesFirstBraggInsideMologram()
        xcoords = braggcoords[0]
        ycoords = braggcoords[1]

        lastindex = len(ycoords)-1

        middleindex = int(round(lastindex/2))

        y1 = ycoords[middleindex]
        x1 = xcoords[middleindex]
        y2 = ycoords[lastindex]
        x2 = xcoords[lastindex]

        # Calculate parameters for hyperbola through origin, (x_jout/2, y_jout/2), and (x_jout, y_jout)
        func = lambda A: (((x2-sqrt(A))**2)/A) - (y2**2)/((y1**2)/((((x1-sqrt(A))**2)/A)-1)) - 1

        A = fsolve(func,A_init)
        B = (y1**2)/((((x1-sqrt(A))**2)/A)-1)

        return A, B, xcoords, ycoords

    def calcBraggOrderDistances(self,order = 0):
        """Calculates the Periods of first, second and third order Bragg reflection, if order is specified the corresponding order is calculated.

        .. math:: \\Lambda  = \\frac{\\lambda }{{2 \\cdot N}} \\cdot m

        :return (list): always a list object for consistency.
        """

        wl = self.wavelength
        N = self.waveguide.N

        if order != 0:

            return [wl/(N*2)*order]

        else:

            a = wl/(2*N)

            return [a,a*2,a*3]

    def calcMololineX(self, j, y):
        """
        Calculates the x coordinate for a point on the jth mololine,
        given its j and y components. Formula implemented according to Fattinger it defines the middle of the ridges and the grooves.

        
        :param j: index of mololine, the mololine with index zero goes through the origin
        :param y: y coordinate of mololine j
        :returns: coordinate of the x-axis
        """

        waveguide = self.waveguide
        N = waveguide.N
        n_m = self.n_m
        f = self.focalPoint

        j_0 = self.j_0

        x = (self.wavelength*N*(j_0+j)-np.sqrt(n_m**2*(N**2-n_m**2) *
                                               (y**2+f**2)+(n_m*self.wavelength)**2*(j_0+j)**2))/(N**2-n_m**2)

        return x

    def calcMololineY(self, j, x):
        """
        Calculates the y coordinate for a point on the jth mololine,
        given its j and x components.

        :param j: index of mololine, the mololine with index zero goes through the origin
        :param x: x coordinate of mololine j
        :returns: coordinate of the y-axis
        """
        waveguide = self.waveguide
        N = waveguide.N
        n_m = self.n_m
        f = self.focalPoint
        wavelength = self.wavelength

        y = sqrt(N**2*x**2 - 2*N*f*x*n_m - 2*N*j*wavelength*x + 2*f *
                 j*wavelength*n_m + j**2*wavelength**2 - x**2*n_m**2)/n_m
        return y

    def calcMololineJ(self, x, y):
        """
        Calculates the line number j, that goes through a point (x,y).

        :param x: x-coordinate of the point of interest
        :param y: y-coordinate of the point of interest
        :returns: line number j
        :rtype: int
        """
        waveguide = self.waveguide
        N = waveguide.N
        n_m = self.n_m
        f = self.focalPoint
        j_0 = self.j_0
        wavelength = self.wavelength

        j = (N*x-wavelength*j_0 - sqrt(f**2*n_m**2 +
                                       x**2*n_m**2 + y**2*n_m**2))/wavelength
        return int(j)

    def calcLinedistance(self, j, N=1):
        """
        Calculates the line distance at the location j, the difference
        is calculated with the next higher j --> j + N, N=1 per default, the distance
        is in meters

        :param j: line number
        :param N: default N=1, distance between N higher line number and the current line.
        :returns: distance to the next higher line on the x-axis
        :rtype: float
        """

        distance = np.abs(self.calcMololineX(
            j, 0) - self.calcMololineX(j+N, 0))

        return distance

    def maximumLinePeriod(self):
        """returns the maximum line period for a mologram with given focal length and diameter.

        .. math:: \\Lambda  = \\frac{\\lambda }{{N - {n_c}\\sin \\left( {\\arctan \\left[ {\\frac{D}{{2 \\cdot f}}} \\right]} \\right)}}

        """

        return self.wavelength/(self.waveguide.N - self.n_m*np.sin(np.arctan(self.radius/self.focalPoint)))

    def calcLineDistances(self, N=1):
        """
        Calculates the line distances (Periods) over the entire mologram (between j_min and j_max)
        and returns them as a numpy array, the distance is in meters

        :params N: default is 1. return every Nth line, important, for phase mask N=2.
        :returns: distances of all the lines in the mologram
        :rtype: ndarray
        """
        distances = []

        for j in range(self.j_min, self.j_max, N):
            distances.append(self.calcLinedistance(j, N))

        return np.asarray(distances)

    def calcLineLengthAndAverageAngle(self, j):
        """
        to calculate the weighting of the phase mask optimization it works with the general mask utility mologram.mask, mologram.mask
        can be a single mask object or a list of masks.

        :param j: line index of the line the length in [m] that should be calculated.
        :return: line length of line j in [m], average absolute angle orientation in radians. (this is important
            to calculate the contributions of TE and TM mode in the phase mask optimization process)
            .. to do:: Average Angle has not been tested yet and is therefore not yet returned.
        :rtype: float

        """
        y = np.linspace(-self.radius, self.radius, 1000)

        x = self.calcMololineX(j, y)

        # check whether there is a mask applied to the mologram.
        if (self.braggRadius != None and self.mask != None):

            unmasked = np.ones(x.shape, dtype=bool)

            if type(self.mask) is list:

                for mask in self.mask:

                    masked = mask.applyMask(x, y)
                    unmasked = unmasked & (masked != True)

            else:

                masked = self.mask.applyMask(x, y)
                unmasked = (masked != True)

        lineLength = 0
        averageAngle = 0
        # unit vector pointing in y-direction.
        relative_unity_vector = np.array([0, 1])

        # check whether the first point is already on a line segment.
        accumulate = unmasked[0]

        for i, element in enumerate(unmasked[:-1]):

            # add the euclidian distance of the next datapoint and add it to
            # the line distance.
            if accumulate == True:

                norm = np.sqrt((x[i]-x[i+1])**2+(y[i]-y[i+1])**2)
                lineLength += norm
                linevector_norm = np.array(x[i]-x[i+1], y[i]-y[i+1])/norm
                # norm is here to account for the relative contribution of this line segment
                # to the average angle.

                averageAngle += np.arccos(
                    np.sum(relative_unity_vector*linevector_norm))*norm

            # new line segment starts
            if element == False and unmasked[i+1] == True:

                accumulate = True

            # line segment ends
            if element == True and unmasked[i+1] == False:

                accumulate = False

        # since some of the lines have length 0
        if lineLength == 0:
            averageAngle = 0
        else:
            averageAngle = averageAngle/lineLength
        # for testing whether the masking is correct or not.
        # plt.plot(x[unmasked]*1e6,y[unmasked]*1e6,'o',color='black', markersize=1)

        # plt.xlabel('x in um')
        # plt.ylabel('y in um')
        # plt.title('Mologram')
        # xlim = None
        # ylim = None
        # if xlim != None:
        #     plt.xlim(xlim*np.array(1e6))
        # if ylim != None:
        #     plt.ylim(ylim*np.array(1e6))
        # if xlim == None and ylim == None:
        #     plt.axis('equal')

        # integrate the lines, need to detect missing line segments, the
        # easiest thing would be to somehow split the boolean array.

        return lineLength

    def calcLineLengths(self, N=2):
        """
        Calculates the line lengths  over the entire mologram (between j_min and j_max)
        and returns them as a numpy array, the length is in meters

        :returns: lineLenths of all the lines in the mologram
        :rtype: numpy.ndarray
        """
        lineLengths = []

        for j in range(self.j_min, self.j_max, N):
            lineLength = self.calcLineLengthAndAverageAngle(j)
            lineLengths.append(lineLength)

        return np.asarray(lineLengths)

    def placeScatterer(self, N_particles, placement, bindingProbabilityRidge,
                       z_min, z_max, seedBragg=True):
        """
        Returns points for scatterer positions as a tuple (x,y,z) given
        the amount of scatterers, the desired placement, the binding
        probability and the different masks such as the minimum and
        maximum vertical distance of the particles.

        ATTENTION: There is a mistake in the function for incoherent scatterer placement
        use generateRandom in generatePattern for this purpose.

        :param N_particles: number of particles
        :type N_particles: int
        :param placement: 'Mololines', 'Ridges (Rectangular)' or 'Ridges (Sinusoidal)'.
        :type placement: string
        :param bindingProbabilityRidge: probability that the particles bind on ridge
        :type bindingProbabilityRidge: float
        :param z_min: minimal vertical distance of the particles
        :param z_max: maximal vertical distance of the particles

        """

        # Need to calculate the ratio of striped vs non-striped area (to know
        # how many to seed in the middle)

        x_sca, y_sca, z_sca, groove_vs_ridge = seed_mologram_cf(
            N_particles,
            self.mologram_seed_parameters, # precomputed because of Speed
            bindingProbabilityRidge,
            z_min,
            z_max,
            self,
            seed_mid=seedBragg,
            placement=placement
        )

        y_sca = y_sca*(2*np.random.randint(low=0, high=2,
                                           size=np.max(N_particles))-1)
        shuffled_indexing = np.arange(len(x_sca))
        np.random.shuffle(shuffled_indexing)

        x_sca = x_sca[shuffled_indexing]
        y_sca = y_sca[shuffled_indexing]
        z_sca = z_sca[shuffled_indexing]
        groove_vs_ridge = groove_vs_ridge[shuffled_indexing]

        # AF: 3.8.2017 I am not sure,whether this function is still needed.
        # this uses the old and slow function of Silvio
        # if placement == 'Ridges (Rectangular)':    # place scatterer on the grooves
        #     x_sca, y_sca = generateMologram(self.wavelength, self.focalPoint, 2*self.radius, N_particles, self.n_m, self.waveguide.N, p=bindingProbabilityRidge)

        #     if z_max == 0:  # scatterer directly on the waveguide
        #         z_sca = np.zeros(x_sca.shape)
        #     else:   # scatterer randomly placed at a certain distance to the waveguide
        #         z_sca = np.random.uniform(z_min, z_max, x_sca.shape)

        return x_sca, y_sca, z_sca, groove_vs_ridge

    def AreaWithoutBragg(self):
        """returns the area of the mologram (grooves + ridges) without the central bragg area in m^2"""

        if self.shape == 'disk':
            R = self.braggRadius
            d = self.braggRadius - self.braggOffset
            d2 = self.braggRadius + self.braggOffset
            r = self.radius

            # part of the right half moon. (Looks like an implementation of
            # Csaba)
            A_right = np.pi*r*r-(r*r*np.arccos((d*d+r*r-R*R)/(2*d*r)) + R*R*np.arccos(
                (d*d+R*R-r*r)/(2*d*R)) - 1/2.*np.sqrt((-d+r+R)*(d+r-R)*(d-r+R)*(d+r+R)))
            A_left = (r*r*np.arccos((d2*d2+r*r-R*R)/(2*d2*r)) + R*R*np.arccos(
                (d2*d2+R*R-r*r)/(2*d2*R)) - 1/2.*np.sqrt((-d2+r+R)*(d2+r-R)*(d2-r+R)*(d2+r+R)))
            A = A_right + A_left

        if self.shape == 'rectangular':

            R = self.braggRadius
            d = self.braggRadius - self.braggOffset
            d2 = self.braggRadius + self.braggOffset

            bragg_circle = shapes.Disk((-R, 0), d2, d)

            molo = shapes.Rectangle(
                (-self.radius, -self.height/2.), (self.radius, self.height/2.))
            # do the substraction
            A = utils.boolean(2, [molo, bragg_circle],
                              lambda cir, tri: cir and not tri).area()

        return A

    def Area(self):
        """return the area of the footprint of the molographic structure"""

        if self.shape == "disk":

            A = np.pi*self.radius*self.radius
        if self.shape == "rectangular":

            A = self.radius*2*self.height

        return A

    def A_ridges(self):
        """return the area of the ridges"""

        return self.AreaWithoutBragg()/2.

    def area_ratio(self):
        """
        returns the ratio of the area the mologram is spanning (groves+ridges)
        versus the area of the whole mologram circle. This is useful to
        find out how many scatterers to place in the 1st-order cut-out. Only works for disk shaped molograms.
        """

        AWithoutBragg = self.AreaWithoutBragg()
        ATotal = self.Area()

        return AWithoutBragg/(ATotal)

    def generatePhasemaskUnderetchCorrection(self, Underetch=30e-9,save=True):
        """
        :param Underetch: is the underetch in [m] on each side of the ridge to generate the dutycycle vector. Hence 30 nm means that a Ridge of size 300 nm will become
        a ridge of 240 nm (two sides)
        generates the mask with the generatePhasemask function
        """

        ridges_width = []
        for j in xrange(self.j_min, self.j_max,2):

            ridges_width.append(self.calcLinedistance(j, 2)/2.)

        ridges_width = np.asarray(ridges_width)

        ridges_width_etch_cor = ridges_width + 2*Underetch

        dutycycles = ridges_width_etch_cor/(ridges_width*2)

        print dutycycles

        return self.generatePhasemask(D=dutycycles,save = save)


    def generatePhasemask(self, D=0.5, index=2,save = True):
        """generates a gds-file of the molographic design. It is assumed that the first mask is a square or a disk mask and the second one an anulus mask containing the Braggcircle.


        :param D: D is a numpy array of Dutycycle vectors, starting with the largest molographic period to smaller ones (left to right of the molographic structure)
        :param index: index is an int. If index = 2 the phase mask is created, if index = 1 the mologram is created.

        returns the phasemask pattern as a core.Elements object. 

        """

        cell = core.Cell('TOP')
        elist = core.Elements()

        r = self.mask[0].radius

        if type(D) == float:
            D = np.ones(len(xrange(self.j_min, self.j_max, index)))*D

        for jindex, j in enumerate(xrange(self.j_min, self.j_max, index)):

            points = []

            for i in np.linspace(-r, r, 1000):

                points.append(
                    tuple((self.calcMololineX(j-D[jindex]*index/2.0, i)*1e6, i*1e6)))

            for i in np.linspace(r, -r, 1000):

                points.append(
                    tuple((self.calcMololineX(j+D[jindex]*index/2.0, i)*1e6, i*1e6)))

            elist.add(core.Boundary(points))

        hilf_rect = shapes.Rectangle(
            (-1.5*r*1e6, -1.5*r*1e6), (1.5*r*1e6, 1.5*r*1e6))

        for i in self.mask:
            if "disk" in i.shape:
                disk1 = shapes.Disk((0, 0), i.radius*1e6)
                schablone = substraction(hilf_rect, disk1)

                # cuts out a circular area.
                mologram_shape = substraction(elist, schablone) # this line throws an error for too large molograms...


            if "rectangle" in i.shape:
                rect = shapes.Rectangle(
                    (-i.width/2.*1e6, -i.height/2.*1e6), (i.width/2.*1e6, i.height/2.*1e6))
                schablone = substraction(rect, rect)
                # cuts out a rectangular area
                mologram_shape = substraction(elist, schablone)

            if "annulus" in i.shape: # this function would need to be adapted for hyperbolas and the higher order braggs.
                # cuts out the bragg circle
                disk1 = shapes.Disk(
                    (i.center[0]*1e6, i.center[1]*1e6), i.r_inner*1e6)
                disk2 = shapes.Disk(
                    (i.center[0]*1e6, i.center[1]*1e6), i.r_outer*1e6)
                Braggcircle = substraction(disk2, disk1)
                mologram_shape = substraction(mologram_shape, Braggcircle)

        
        # cell.add(elist)

        # cell.add(elist)

        if save:
            cell.add(mologram_shape)
            layout = core.Layout('LIBRARY')
            layout.add(cell)

            layout.save('Phasemask_.gds')

        return mologram_shape

    def calculateAndGenerateOptimalPhaseMask(self):
        """calcuates the optimal Phase Mask with the phase mask optimization framework and saves the optimal design to a gds-II file.

        .. todo:: link to materialsdatabase, account for the process variations of the IMT process.

        """

        Lambda_space = self.calcLineDistances(N=2)
        lineLengths = self.calcLineLengths()
        weight_Lambda = lineLengths/np.sum(lineLengths)

        # 405 nm Phase mask parameters.
        n_i = 1.497
        n_f = 2.231
        n_s = 1.540
        IWL = 405e-9

        PM = PhaseMaskCalculator(
            self, n_i, n_f, n_s, IWL, 246e-9, Lambda_space, weight_Lambda, 'TE', Etch_Angle=0)

        PM.optimizePhaseMask(robust=True, saveplots=False, optimal=False)
        PM.applyGaussianFilterToDutyCycles(10)
        D = PM.D_robust
        print "Robust Height Phase Mask:"
        print PM.h_robust
        self.generatePhasemask(D)

    def display(self, fig = None,N_omit=50, markersize=0.01, xlim=None, ylim=None, save=False,Nolabels=False,color='black',alpha=1):
        """
        Displays the mologram for the given parameters. The lines can
        only be distinguished if some are omitted (N_omit) since they
        are too close together.

        :param N_omit: number of lines that are omitted
        :param markersize: size of the scatterers
        :param xlim: limits in x-direction [left, right]
        :param ylim: limits in y-direction [bottom, top]
        :param save: image is stored if true
        :type save: bool
        """

        f = self.focalPoint

        n_m = self.n_m
        N = self.waveguide.N

        j_0 = self.j_0
        j_min = self.j_min
        j_max = self.j_max

        N_lines = j_max-j_min
        j, y = np.meshgrid(np.array(range(int(j_min/N_omit), int(j_max/N_omit)))
                           * N_omit, np.linspace(-self.radius, self.radius, N_lines*10))
        x = (self.wavelength*N*(j_0+j) -
             sqrt(n_m**2*(N**2-n_m**2)*(y**2+f**2)+(n_m*self.wavelength)**2*(j_0+j)**2)) / \
            (N**2-n_m**2)
        if fig == None:
            fig = plt.figure()

        if (self.braggRadius != None and self.mask != None):
            # cut out the circular shape of the mologram
            x_mologram = x
            y_mologram = y

            for i in self.mask:

                masked = i.applyMask(x_mologram,y_mologram)

                x_mologram = x_mologram[masked == False]
                y_mologram = y_mologram[masked == False]

            # create the mask for the Bragg circle.
            plt.plot(x_mologram*1e6, y_mologram*1e6, 'o', color=color, markersize=markersize,alpha=alpha)

        if Nolabels == False:
            plt.xlabel('x in um')
            plt.ylabel('y in um')
            plt.title('Mologram')

        if xlim != None:
            plt.xlim(xlim*np.array(1e6))
        if ylim != None:
            plt.ylim(ylim*np.array(1e6))
        if xlim == None and ylim == None:
            plt.axis('equal')



        if save:
            fig.savefig('Mologram_' + 'Nomit_' + str(N_omit) + '.png')
            plt.show()
        return fig


def testBragg(waveguide,mologram):
    """displays the Bragg Hyperbola and the Bragg Circle for the given waveguide and mologram"""

    hyperbolaparams = mologram.calcBraggHyperbolaparams(10e-6)

    A = hyperbolaparams[0]
    B = hyperbolaparams[1]

    xcoords = hyperbolaparams[2]
    ycoords = hyperbolaparams[3]

    lastx = xcoords[len(xcoords)-1]

    # Note: the explicit hyperbola plot is smoother if plotted for not too many points (probably numerical issue)
    xlinspace = np.linspace(0, lastx, 100)

    yhyperbola = sqrt(B*((((xlinspace-sqrt(A))**2)/A)-1))
    # Note: yhyperbola[0] should from the formula be zero for x = 0, but this is not always the case;
    # for some parameter values, the deviation from zero can be substantial (probably numerical issue).
    # However, it does not further matter because the circle is used for the Bragg area anyways, since
    # the circle (as visible on the plots) approximates the hyperbola very well in the region of interest.
    print yhyperbola[0]

    # formatPRX()
    mologram.display()

    # Note: So far, all the coordinates are in meters. For plotting, need to multiply everything by 10**6 (m => um)

    # Setting up color for plotting (use viridis/plasma/inferno/magma perceptually uniform sequential colormap)
    color_idx = np.linspace(0.1, 0.9, 3) # cutoff at 0.8 or 0.9 for better visibility in black'n'white

    # Hyperbola as line through all the Bragg points
    plt.plot(xcoords*10**6, ycoords*10**6, color = plt.cm.viridis(color_idx[0]), linewidth = 2, label = "Bragg Hyperbola (connected points)")
    plt.plot(xcoords*10**6, ycoords*-1*10**6, color = plt.cm.viridis(color_idx[0]), linewidth = 2)

    # Hyperbola (calculated explicitly) fitted through (0, 0), (x_-jout, y_-jout), (x_-jout/2, y_-jout/2), where jout is the index of the outermost Bragg point inside the mologram
    # Note: I have manually added the origin here for the plot (because yhyperbola[0] is not always zero although it should be form the formula)
    plt.plot(np.concatenate([[0], xlinspace])*10**6, np.concatenate([[0], yhyperbola])*10**6, color = plt.cm.viridis(color_idx[1]), linewidth = 2, label = "Bragg Hyperbola (explicit)")
    plt.plot(np.concatenate([[0], xlinspace])*10**6, np.concatenate([[0], yhyperbola])*-1*10**6, color = plt.cm.viridis(color_idx[1]), linewidth = 2)

    # Bragg circle with radius calculated from (0, 0) and (x_-jout, y_-jout)
    r = mologram.braggRadius*10**6
    ycircle = sqrt(r**2 - (xlinspace*10**6 + r)**2) # Note: I used (x+r) instead of (x-r) as written in the analytical derivation document AF0007
    plt.plot(xlinspace*10**6, ycircle, color = plt.cm.viridis(color_idx[2]), linewidth = 2,  label = "Bragg Circle")
    plt.plot(xlinspace*10**6, ycircle*-1, color = plt.cm.viridis(color_idx[2]), linewidth = 2)

    # Add legend and parameter values (NA, f, radius, offset) to the plot
    # Note: if using formatPRX(), positioning and size of legend and text need to be adjusted
    legend = plt.legend(loc = 4, prop = {'size':8}, frameon=1)
    frame = legend.get_frame()
    frame.set_facecolor("white")
    frame.set_edgecolor("black")
    frame.set_linewidth(0.5)
    frame.set_alpha(1)
    plt.annotate("NA: %.2f\nfocal point: %ium\nradius: %.2fum\nBragg offset: %ium\nBragg theta: %.2fdeg"
    %(mologram.NA, mologram.focalPoint*10**6, mologram.radius*10**6, mologram.braggOffset*10**6, mologram.braggTheta*180/np.pi), xy=(0.805, 0.89),
    xycoords = "axes fraction", fontsize = 7)

    #plt.savefig("Mologram_900_06.png") => change name according to parameters (Mologram_focalpoint_NA.png)
    plt.show()



if __name__ == '__main__':

    from waveguide.waveguide import SlabWaveguide           # waveguide calculations
    from generateMask import *

    # waveguide = Waveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
    #                       n_c=1.33, d_f=145e-9, polarization='TE',
    #                         inputPower=1, wavelength=632.8e-9, attenuationConstant=0)
    # mologram = Mologram(waveguide, wavelength=632.8e-9, focalPoint=-900e-6, radius=200e-6)
    # mologram.braggRadius = calcBraggCircle(mologram)    # add mask to avoid the resonant case (second-order Bragg reflection) in the propagation path of the waveguide mode
    # mologram.braggOffset = 25e-6
    # mologram.mask = Mask().annulus(mologram.braggRadius-mologram.braggOffset,mologram.braggRadius+mologram.braggOffset, center=(-mologram.braggRadius,0),inverted=False)
    # print mologram.mask.shape
    # print mologram.calcLineDistances()
    # mologram.display()

    # test of calcLineLength
    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117,
                              n_c=1.33, d_f=145e-9, polarization='TE',
                              inputPower=1, wavelength=632.8e-9, attenuationConstant=2.51*np.log(10)/10*100)

    #f = -900e-6
    #mologram = Mologram(waveguide,focalPoint=f, radius=0.5*diameterGivenNA_F_ns(f=abs(f), NA=0.2))
    mologram = Mologram(waveguide,focalPoint = -900e-6, diameter=300*1e-6,braggOffset = -1)

    print mologram.area_ratio()
    print 'Figure of merit FM:'
    print mologram.figure_of_merit_FM(0.054)
    testBragg(waveguide,mologram)

    print mologram.maximumLinePeriod()
    print mologram.braggOffset

    print focalDistanceGivenNA_D_n(0.1,400e-6,1.)
    print focalDistanceGivenNA_D_n(0.6,400e-6,1.521)

    mologram.generatePhasemask()

    print "Numerical Aperture:"
    print mologram.NA
    # add mask to avoid the resonant case (second-order Bragg reflection) in
    # the propagation path of the waveguide mode
    print maximumNumericalApertureIfmOrderNotPresent(waveguide.N,1.521,2)
    print maximumNumericalApertureIfmOrderNotPresent(waveguide.N,1.521,3)
    print maximumNumericalApertureIfmBraggOrderNotPresent(waveguide.N,3)


    print mologram.braggRadius
    print mologram.j_min
    print mologram.j_max
    print mologram.calcMololineX(mologram.j_max, 0)
    print mologram.AreaWithoutBragg()




    # important,

    mologram.display()
    # for testing, plots the lineLength distribution over the mologram.
    plt.figure()
    lineLengths = mologram.calcLineLengths()

    plt.plot(lineLengths)

    print mologram.calcBraggOrderDistances()

    # mologram.calculateAndGenerateOptimalPhaseMask()
    #mologram.generatePhasemask(index=1)
