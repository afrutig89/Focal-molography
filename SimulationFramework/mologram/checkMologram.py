import numpy as np

"""This is a module that tests the molographic structure entered by the user against several physical constraints."""



def checkBraggOrderConstraint(mologram,orders=5):
    """Check for which of the Bragg conditions is fullfilled in the mologram (checks N = 1 - 5)

    Important note: Roughly, as soon as the focal distance gets smaller than the diameter of the mologram, the third order bragg starts to appear 
    in air this is equivalent to a numerical aperture of the mologram of 0.5. The exact value is 0.6.$

    :return (bool) braggFullfilled: is a list, if entry true, the concerned bragg condition is within the mologram.
    
    """

    # calculate the minimal and maximal line distance in the mologram

    d_min = mologram.calcLinedistance(mologram.j_max,1)

    d_max = mologram.calcLinedistance(mologram.j_min,1)

    # check which bragg periods are within this interval.

    braggFullfilled = []

    for i in range(1,orders+1):

        braggDistance = mologram.calcBraggOrderDistances(i)

        if (braggDistance[0] > d_min and braggDistance[0] < d_max):

            braggFullfilled.append(True)
        else:
            braggFullfilled.append(False)


    return braggFullfilled


def checkFirstOrderOutcouplingConstraint(mologram):
    """
    Based on Tamir, Analysis and Design of Grating Couplers 1977, Figure 2

    In order to have only one beam below and above the grating coupler, the following conditions need to be fulfilled:

    .. math:: \\left| N - \\frac{\\lambda}{\\Lambda} \\right| <= n_c

    .. math:: 2\\cdot \\frac{\\lambda}{\\Lambda} - N >= n_s

    :return: 2 entry list, with a Boolean and a string, Boolean is false if more than one beam is present, the string
    indicates the type of constraint violation as a Message that can be displayed in a terminal. 

    The free spectral range of a mologram is limited by: 

    .. math:: \\frac{\\lambda}{N+n_c}<\\Lambda<\\frac{2\\cdot\\lambda}{N+n_s}

    For a mologram with focus distance -900um and diameter 400 um this range is 201.2 nm and 377 nm (Schott Glas) the values that I calculated in the Nature Nanotechnology
    Paper were for Air: 223 nm and 450 nm. Hence our standard molograms already have a second order beam in the substrate!!

    """
    d_min = mologram.calcLinedistance(mologram.j_max,1)
    print d_min

    d_max = mologram.calcLinedistance(mologram.j_min,1)
    print d_max
    
    # Only first order beam allowed to couple out.
    
    N = mologram.waveguide.N
    wl = mologram.waveguide.wavelength
    n_c = mologram.waveguide.n_c
    n_s = mologram.waveguide.n_s

    if (abs(N - wl/(d_max)) >= n_c):
        # d_min does not need to be considered, since the maximal d is limiting.
        # at most one diffraction order in the cover. 

        return False, 'More than one forward or backward radiating beam in cover!'

    elif ((2*wl/d_max)-N) <= n_s:
        # d_min does not need to be considered. 

        return False, 'Two beams in substrate!'
    elif d_min < wl/(N+n_c):


        return False, 'No outcoupling of the light possible, Grating period is too small.'

    else:
        return True, 'Allowed Mologram'


def checkForbiddenAngles(mologram):
    """Checks whether the beams are totally internally reflected at the substrate air interface. Function assumes that
    the molographic focal spot is detected in air."""

    if mologram.NA > 1:

        return False

    else:
        return True

def maximumNumericalApertureIfmOrderNotPresent(N,n_c,order = 2):
    """Calculates the maximum numerical aperture such that only orders smaller than the specified one can couple out of the waveguide.

    .. math:: NA = {n_c}\sin \\left( \\theta  \\right) = N - \\frac{{N + {n_c}}}{m}
    
    """

    return N - (N+n_c)/order

def maximumNumericalApertureIfmBraggOrderNotPresent(N,order = 3):
    """Calculates the maximum numerical aperture such that only orders smaller than the specified one can couple out of the waveguide.

    .. math:: NA = {n_c}\sin \\left( \\theta  \\right) = N - \\frac{{N + {n_c}}}{m}
    
    """

    return N*(1.-2./order)

def criticalAngle(n_s,n_c=1):
    """returns the critical angle of an interface in degrees, the maximum NA is given by the medium refractive index n_c
    in which the mologram is detected (typically air)"""


    return np.arcsin(n_c/n_s)


if __name__ == '__main__':
    import sys
    sys.path.append('../')
    from mologram import Mologram
    from waveguide.waveguide import SlabWaveguide           # waveguide calculations
    from generateMask import *
    #from braggCircle import calcBraggCircle
    
    # waveguide = Waveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117, 
    #                       n_c=1.33, d_f=145e-9, polarization='TE', 
    #                         inputPower=1, wavelength=632.8e-9, attenuationConstant=0)
    # mologram = Mologram(waveguide, wavelength=632.8e-9, focalPoint=-900e-6, radius=200e-6)
    # mologram.braggRadius = calcBraggCircle(mologram)    # add mask to avoid the resonant case (second-order Bragg reflection) in the propagation path of the waveguide mode
    # mologram.braggOffset = 25e-6
     # print mologram.mask.shape
    # print mologram.calcLineDistances()
    # mologram.display()

    # test of calcLineLength
    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117, 
                          n_c=1.33, d_f=145e-9, polarization='TE', 
                            inputPower=1, wavelength=632.8e-9, attenuationConstant=0)
    mologram = Mologram(waveguide, focalPoint=-900e-6, radius=800e-6,xshift=1000e-6)
    mologram.braggOffset = 25e-6
    

    mologram.display()

    print checkBraggOrderConstraint(mologram)

    print checkFirstOrderOutcouplingConstraint(mologram)