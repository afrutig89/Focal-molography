# -*- coding: utf-8 -*-
"""
Class mask and functions for masking. Available masks: circular,
rectangular and annular.

:author:    Silvio
:created:   Mon Mar 14 16:32:47 2016
"""

# plot tools
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import numpy as np
from numpy import sqrt, mod
import os
import datetime
# from shapely.geometry import Point, box


class Mask:
    """
    Points in the xy-plane can be masked by different shapes (disk, 
    rectangle, annulus). The X and Y coordinates belong together, e.g.
    point P = (X[1],Y[1]).

    ATTENTION: This is no longer used in the simulation of the mologram, but only in the phase mask simulation class.

    :param X: x coordinates in a xy-plane
    :param Y: y coordinates in a xy-plane
    """
    def __init__(self,X=0,Y=0):
        self.X = X
        self.Y = Y
    
    def disk(self,r,center=(0,0),inverted=False):
        """returns a disk shaped mask object"""
        self.shape = "disk%.e.{}".format(center) %(r)
        self.center = center
        self.inverted = inverted
        maskedArea = (self.X-center[0])**2+(self.Y-center[1])**2 < r**2
        self.maskedArea = (maskedArea != inverted)
        self.radius = r
        # self.geometry = Point(center[0],center[1]).buffer(r)
        return self
        
    def annulus(self, r_inner, r_outer, center=(0,0), inverted=False):
        """returns a annular shaped mask object (annulus = circle with hole)"""

        self.shape = "annulus%.e,%e.{}".format(center) %(r_inner,r_outer)
        self.center = center
        self.inverted = inverted
        maskedArea = ((self.X-center[0])**2+(self.Y-center[1])**2 > r_inner**2) and \
                        ((self.X-center[0])**2+(self.Y-center[1])**2 < r_outer**2)
        self.masekdArea = (maskedArea != inverted)
        self.r_inner = r_inner
        self.r_outer = r_outer
        # self.geometry = Point(center[0],center[1]).buffer(r_outer)- \
                            # Point(center[0],center[1]).buffer(r_inner)
        return self
        
    def rectangle(self, w, h, center=(0,0), inverted=False):
        """returns a rectangluar mask object"""
        if(w==h):
            self.shape = "square%.ex%.e" %(w,h)
        else:
            self.shape = "rectangle%.ex%.e" %(w,h)
        self.inverted = inverted
        maskedArea = (abs(self.X-center[0])<w/2) & (abs(self.Y-center[1])<h/2)
        self.maskedArea = (maskedArea != inverted)
        self.width = w
        self.height = h
        self.center = center
        # self.geometry = box(center[0]-w/2,center[1]-h/2,center[0]+w/2,center[1]+h/2)
        return self

    def display():
        plotMask([self])

    def applyMask(self,X,Y):
        """This function applies the mask to the numpy arrays, X and Y and returns
        a boolean array of the same shape.
        :param X: X coordinates of points to be masked
        :type X: numpy array
        :param Y: Y coordinates of points to be masked
        :type Y: numpy array
        :return: boolean array of the same size as X and Y, masked areas are True, whereas unmasked areas are False
        :rtype: boolean
        """

        if "disk" in self.shape:

            return (((X-self.center[0])**2+(Y-self.center[1])**2 < self.radius**2) != self.inverted)

        if "annulus" in self.shape:

            maskedArea = ((X-self.center[0])**2+(Y-self.center[1])**2 > self.r_inner**2) & \
                        ((X-self.center[0])**2+(Y-self.center[1])**2 < self.r_outer**2)
            return (maskedArea != self.inverted)

        if "rectangle" in self.shape or "square" in self.shape:

            return (((abs(X-self.center[0])<self.width/2) & (abs(Y-self.center[1])<self.height/2)) != self.inverted)


# depreciated functions.
    
def diskMask(X,Y,r,center=(0,0),inverted=False):
    """returns a booelan mask of a disk

    :param X: X coordinates
    :param Y: Y coordinates
    :param r: radius of the disk
    :param center: center of the disk (optional)
    """
    return (((X-center[0])**2+(Y-center[1])**2 < r**2) != inverted)

def annulusMask(X, Y, r_inner, r_outer, center=(0,0), inverted=False):
    """returns a boolean mask of an annulus

    :param X: X coordinates
    :param Y: Y coordinates
    :param r_inner: inner radius of the annulus
    :param r_outer: outer radius of the annulus
    :param center: center point of the annulus

    """
    maskedArea = ((X-center[0])**2+(Y-center[1])**2 > r_inner**2) & \
                    ((X-center[0])**2+(Y-center[1])**2 < r_outer**2)
    return (maskedArea != inverted)

def rectangleMask(X, Y, w, h, center=(0,0), inverted=False):
    """returns a boolean mask of a rectangle

    :param X: X coordinates
    :param Y: Y coordinates
    :param w: width of the rectangle
    :param h: height of the rectangle
    :param center: center of the rectangle (optional)
    """
    return (((abs(X-center[0])<w/2) & (abs(Y-center[1])<h/2)) != inverted)

def mologramMask(X, Y, wavelength, focalPoint, D, n_m, N):
    """ generates a boolean mask for the mologram of focal molography
    
    :params X: x-components
    :params Y: y-components
    :params wavelength: wavelength of the incident wave
    :params focalPoint: position of the focal point (3D-vector)
    :params D: diameter of the mologram
    :params n_m: refractive index of the medium where the focal point is placed at
    :params N: effective refractive index of the waveguide
    """
    f = focalPoint
    j_0 = np.ceil(f*n_m/wavelength)
    j_min = int((2*(N*-D/2 - j_0*wavelength) - 2*sqrt(f**2*n_m**2 + n_m**2*(-D/2)**2)) / wavelength)
    j_max = int((2*(N*D/2 - j_0*wavelength) - 2*sqrt(f**2*n_m**2 + n_m**2*(D/2)**2)) / wavelength)
    j = np.array(range(j_min,j_max+1))
    N_lines = j_max-j_min
    
    a = 1/2. * sqrt(n_m**2*(4*N**2*f**2 + 4*f*j*wavelength*n_m + j**2*wavelength**2))/(N**2-n_m**2)
    b = 1/2. * sqrt((4*N**2*f**2 + 4*f*j*wavelength*n_m + j**2*wavelength**2)/(N**2-n_m**2))
    x_0 = 1/2. * (2*N*f*n_m + N*j*wavelength)/(n_m**2-N**2)

    maskedArea = np.zeros(X.shape)
    for n in range(N_lines):
        maskedArea = maskedArea + ((((X-x_0[n])/a[n])**2-(Y/b[n])**2) <= 1)
        
    return (mod(maskedArea,2) == 0)

    
def plotMask(masks):
    """
    plots the masks given as a list.

    .. todo:: annulus

    :param masks: list of masks (object Mask)
    """
    
    colors = ["black", "white"]
    currentAxis = plt.gca()        
    currentAxis.set_axis_bgcolor(colors[not(masks[0].inverted)])
    
    for mask in masks:
        # plot disk
        if ("disk" in mask.shape):
            currentAxis.add_patch(Circle(mask.center,mask.radius,color=colors[mask.inverted]))
        
        # plot rectangle
        elif ("rectangle" in mask.shape or "square" in mask.shape):
            currentAxis.add_patch(Rectangle((-mask.width/2+mask.center[0],-mask.height/2+mask.center[1]), mask.width, mask.height, facecolor=colors[mask.inverted]))
            
        elif ("fresnel" in mask.shape):
            i = len(mask.radius)+(1+mask.inverted)
            for radius in reversed(mask.radius):
                currentAxis.add_patch(Circle((0,0),radius,color=colors[(i/2.).is_integer()]))
                i = i-1

    plt.axis([mask.X.min(), mask.X.max(), mask.Y.min(), mask.Y.max()])
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.xlabel('x in m')
    plt.ylabel('y in m')

if __name__ == '__main__':


    import sys
    sys.path.append('../')
    from waveguide.waveguide import SlabWaveguide           # waveguide calculations

    from braggCircle import calcBraggCircle
    from mologram import Mologram
    
    waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117, 
        n_c=1.33, d_f=145e-9, polarization='TE', 
        inputPower=1, wavelength=632.8e-9, attenuationConstant=0)
    mologram = Mologram(waveguide, focalPoint=-900e-6, radius=200e-6,braggOffset=25e-6)
    mologram.braggRadius = calcBraggCircle(mologram)    # add mask to avoid the resonant case (second-order Bragg reflection) in the propagation path of the waveguide mode
    mologram.braggOffset = 25e-6

    mologram.mask.append(Mask().annulus(mologram.braggRadius-mologram.braggOffset,mologram.braggRadius+mologram.braggOffset, center=(-mologram.braggRadius,0),inverted=False))
    mologram.mask.append(Mask().disk(200e-6,inverted=True))