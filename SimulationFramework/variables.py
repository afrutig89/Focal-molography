from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from SharedLibraries.Database.variableContainers import VariableContainer,VariableContainerList

# parameters that will be stored in the parameters database table

class GeneralSettings(VariableContainer):

    _DB =  {
        # general simulation settings
        'ID':'integer primary key AUTOINCREMENT',
        'date':'text',
        'startTime' : 'text',
        'start':'real',
        'description':'text',
        'calculationMethod' : 'text',
        'biasSimulation':'integer',
        'Extinction':'integer',
        'CorrSimulation':'integer',
        'BackgroundScreen':'integer', # boolean, whether to calculate a background screen or not.


        # database settings
        'saveDatabase' : 'integer',
        'databaseFile' : 'text',
        'saveLog' : 'integer',
        'saveFigure' : 'integer'
    }

    _table_name = 'GeneralSettings'

class ScreenParameter(VariableContainer):

    _DB = {
        'ID':'integer primary key AUTOINCREMENT',
        'screenWidth':'real',
        'screenPlane':'text',
        'screenRatio':'real',
        'screenRotation' : 'array',
        'center' : 'array',        # center of the screen
        'centerBackground': 'array', # center of the screen to calculate the background intensity
        'screen_z_position':'real', # z value of the screen
        'npix':'integer'        # number of pixels in each direction
    }

    _table_name = 'ScreenParameter'


class WaveguideParameter(VariableContainer):

    _DB = {
        'ID':'integer primary key AUTOINCREMENT',
        'wavelength':'real',
        'inputPower':'real', # mean of the laser intensity that impinges onto the grating coupler.
        'Laserfluctuations':'real',
        'beamWaist':'real',
        'couplingEfficiency':'real',
        'gratingPosition' : 'real',
        'polarization':'text',
        'mode':'integer',
        'propagationLoss':'real',
        'surfaceRoughness':'real',
        'Waveguidebackground':'integer', # ID specifying which waveguide background simulation to use
        'cover' : 'text',
        'film' : 'text',
        'substrate' : 'text',
        'thickness' : 'real'
    }

    _table_name = 'WaveguideParameter'



class ParticleParameter(VariableContainer):
    """A binder class that allows to specify the properties of a molecule or particle that binds to the sensor surface. It acts like a struct for
    the parameter values. The database storage must not be organized by this level. """
    def __init__(self,name):

        self.name = name

        # the name flag is necessary, because all the parameters will be stored in the same 'log' table of the simulation.
        self._DB  = {

            'ID':'integer primary key AUTOINCREMENT',
            self.name + '_name' : 'text',
            self.name + '_Radius' : 'real',
            self.name + '_dispersity' : 'real', # 0 => particle radius is fixed; >0 => sd of Gaussian size distribution around the mean 
            self.name + '_RefractiveIndex' : 'complex',
            self.name + '_Ncoh':'integer', # placed with binding probability of 100 %
            self.name + '_Nincoh':'integer', # placed with binding probability 50 %
            self.name + '_MSN':'integer', # boolean, whether particle exhibits molecular shot noise
            self.name + '_z_min':'real',
            self.name + '_z_max':'real',
            self.name + '_placement':'text', # sinuisoidal, rectangular, mololines.
            self.name + '_seed':'integer', # boolean specifying whether to seed the bragg area
            self.name + '_surfaceMassModulation' : 'real', # surfacemass modulation of the current binder.
            self.name + '_commonMassDensity':'real', # the common mass density of the grooves and the ridges, the modulation is on top of this.
            self.name + '_coherentSurfaceMassDensity':'real',
            self.name + '_totalCoherentProteinMass':'real', # the total coherent mass on the structure in pg
            self.name + '_totalInCoherentProteinMass':'real' # the total incoherent mass on the structure in pg

        }

        self._table_name = self.name + '_Particle'

        super(ParticleParameter,self).__init__()


class MologramBiosensorInteractionParameter(VariableContainer):

    def __init__(self,name):

        self.name = name

        self._DB = {
            'ID':'integer primary key AUTOINCREMENT',
            self.name + '_RecDenRid':'real', # these four parameters maybe should go into a separate class.
            self.name + '_RecDenGrov':'real',
            self.name + '_K_D':'real',
            self.name + '_c':'real', # analyte concentration
            self.name + '_competitive':'integer' # boolean, specifies whether an interaction is competitive.
        }

        self._table_name = self.name + '_Interaction'

        super(MologramBiosensorInteractionParameter,self).__init__()

class MologramParameter(VariableContainer):
    
    _DB = {
        'ID':'integer primary key AUTOINCREMENT',
        'focalPoint':'real',
        'diameter':'real',
        'NA':'real',
        'braggOffset':'real',
        'braggTheta':'real',
        'braggRadius':'real',
        'shape':'text', # shape of the mologram
        'xshift':'real', # shift in x direction relative to the center point of the molographic structure.
        'aspectRatio':'real',
        'direction':'real'
            
    }

    _table_name = 'MologramParameter'

# generate dictionary with all parameters that will be stored in the log database.
class FieldSimulationInputs(VariableContainerList):

    def __init__(self):

        self.generalSettings = GeneralSettings()
        self.screenPar = ScreenParameter()
        self.waveguidePar = WaveguideParameter()

        self.analyte = ParticleParameter('analyte')
        self.NSB = ParticleParameter('NSB')
        self.bias = ParticleParameter('bias')

        self.particlePar = VariableContainerList([self.analyte,self.NSB,self.bias])

        self.analyteInteraction = MologramBiosensorInteractionParameter('analyte')
        self.NSBInteraction = MologramBiosensorInteractionParameter('NSB')

        self.interactionPar = VariableContainerList([self.analyteInteraction,self.NSBInteraction])

        self.mologramPar = MologramParameter()

        self._containerlist = [self.generalSettings,self.screenPar,self.waveguidePar,self.mologramPar,self.particlePar,self.interactionPar]

        super(FieldSimulationInputs,self).__init__()


# parameters that will be stored in the simulations database table, to add a Variable, this also needs to be changed in the default dataset.

class FieldSimulationResults(VariableContainer):

    _DB =  {
        # general settings
        'ID':'integer primary key AUTOINCREMENT',
        'runningTime':'real', # running time of the simulation in sec.
        
        # binder properties
        'dipolePositions':'array', # array of the positions of all the different binders (contains three arrays (x,y,z))
        'bindernames':'array', # array of the names of all the different binders
        'coh_inc':'array', # boolean 1 Particle is placed coherently, 0 particle is placed incoherently.
        'dipoleMoments':'array', # array of the dipole moments of all the different binders
        'eps_r':'array', # array of the permittivity of all the different binders
        'Radius':'array', # array of the radii of all the different binders
        # calculated fields

        'inputPower':'real', # actual intensity that impinges onto the grating coupler (accounts for laser fluctuations)
        'I_0' : 'real', # intensity per unit length impinging onto the mologram at the surface of the waveguide.
        'E_sca':'array', # scattered field in the focal plane (without waveguide background)
        'I_sca':'array', # scattered intensity in the focal plane (without waveguide background)
        'I_tot':'array', # waveguide background + scattered field.
        'E_exc':'array', # excitation field at the scatterer positions.
        'E_bg':'array', # field on the background screen
        'I_bg':'array', # intensity on the background screen without the waveguide background
        'I_bg_tot':'array', # intensity on the background screen with the waveguide background
        'I_bwg':'real', # background intensity due to the waveguide impurities.
        'I_signal' : 'real', # scattered intensity in the focal point.
        'I_molo' : 'real', # scattered intensity averaged over the Airy disk (Maybe this should rather be a power...)
        'AiryDiskRadius':'real', # Radius of the Airy disk, which is calculated from by the analytical formula in fattinger (The numerical algorith is too unstable.)  and fed to calc_Int_AiryDisk
        'screen':'array', # numpy array of the screen locations for plotting.
        'SNR' : 'real', # SNR calculated by calc_Int_AiryDisk calculated by calc_Int_AiryDisk
        'bkg_mean':'real', # mean background intensity calculated by calc_Int_AiryDisk
        'bkg_std':'real', # std background intensity calculated by calc_Int_AiryDisk

    }
    
    _table_name = 'results'


# parameters for the image aquisition with the camera and the resulting intensity distributions.

class CameraSimulationInputs(VariableContainer):

    _DB = {
        'ID':'integer primary key AUTOINCREMENT', # ID of the captured image (allows to take multiple images from the same simulation)
        'ID_results': 'real', # ID of the simulation from which the results are to be taken.
        'camModel':'text', # the camera that was used to acquire the image
        'exposureTime':'real', # exposure time of the camera for a single aquisition.
        'magnification':'real', # magnification of the imaging system used
        'databaseFile':'text' # database file
    }


class CameraSimulationResults(VariableContainer):

    _DB = {
        'ID':'integer primary key AUTOINCREMENT', # ID of the captured image (allows to take multiple images from the same simulation)
        'I_tot_bn':'array', # acquired digital intensity distribution in the focal spot by the camera
        'I_bg_tot_bn':'array', # acquired intensity distribution in the from the background simulation by the camera.
        'I_molo_cam':'real', # scattered intensity averaged over the Airy disk after image acquisition.
        'bkg_mean_cam': 'real', # mean background calculated by calc_Int_AiryDisk after image acquisition.
        'bkg_std_cam':'real', # std background calculated by calc_Int_AiryDisk after image acquisition.
        'I_signal_cam':'real', # scattered intensity in the focal point after camera aquisition.
        'SNR_cam':'real', # signal to noise ratio as calculated by calc_Int_AiryDisk after image acquisition.
    }


def commonKeys(dict1, dict2):
    """
    returns a dictionary which has the keys which are in both dictionaries is used to save variables in the results dict into the database.
    """
    newDict = dict()
    for key in dict2.keys():
        if key in dict1.keys(): newDict[key] = dict1[key]

    return newDict

# I did not convert these to classes yet.
# todo when material database (object material), the particle parameters that are stored in the materials database.

particleParDB = {
    'ID' : 'integer primary key AUTOINCREMENT',
    'name' : 'text',
    'material' : 'text',
    'radius' : 'real',
    'mass' : 'real',
    'file' : 'text',
}

planeMaterialPar = {
    'name' : 'text',
    'refractiveIndex' : 'complex',
    'thickness' : 'real'
}

mediumDBDict = {
    'ID' : 'integer primary key AUTOINCREMENT',
    'name' : 'text',
    'material' : 'text',
    'file' : 'text',
}

def particleDB():
    return particleParDB

def mediumDB():
    return mediumDBDict


if __name__ == '__main__' and __package__ is None:

    print(GeneralSettings().getValues())
    print(ParticleParameter('Analyte').getValues())