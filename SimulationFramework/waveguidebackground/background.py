# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 11:11:40 2016

@author: Silvio
"""

import numpy as np
from numpy import log, sin, cos, exp, pi
from scipy.integrate import dblquad, quad
import matplotlib.pyplot as plt
import time
import sys
sys.path.append('../')
from mologram.mologram import numericalApertureGivenF_D_n, focalDistanceGivenNA_D_n
import auxiliaries.constants as constants
from binder.Adlayer import ProteinAdlayer
from materials.materials import IsotropicMaterial
from waveguide.waveguide import SlabWaveguide
from materials.materials import IsotropicMaterial

# To do: This needs to be programmed into a class with which inherits from Slabwaveguide. 

def calculateAlpha(wg,rmsRoughness, L_c = 'max'):
    """calcutes the attenuation constante (in [neper/m]) coming from the surface roughhess
    of a waveguide
     
    :param wg: waveguide
    :type wg: SlabWaveguide
    :param rmsRoughenss: the rms roughness of the waveguide [m]
    :type rmsRoughness: float
    :param L_c: corelation length of the roughenss [m]
    :type L_c: float or string='max'

    .. math:: {\\alpha _{{\\text{bg}},{\\text{r}}}} = 2{\\pi ^2}\\frac{{\\left( {n_{\\text{f}}^2 - {N^2}} \\right)}}{{{t_{{\\text{eff}}}}\\left( {n_{\\text{f}}^2 - n_{\\text{c}}^2} \\right)}}\\left( {{{\\left( {n_{\\text{f}}^2 - n_{\\text{c}}^2} \\right)}^2}{G_{\\text{c}}}\\left( {{L_{\\text{c}}},\\beta } \\right) + {J^2}{{\\left( {n_{\\text{f}}^2 - n_{\\text{s}}^2} \\right)}^2}{G_{\\text{s}}}\\left( {{L_{\\text{c}}},\\beta } \\right)} \\right)\\frac{{N{\\sigma ^2}}}{{{n_{\\text{f}}}{\\lambda ^2}}}

    """
    if L_c == 'max':
        L_c_array=np.linspace(1/(4*wg.beta),10/wg.beta,1000)
        G =  (L_c_array*wg.beta)\
                * np.sqrt(np.sqrt(4 * wg.beta**2 * L_c_array**2 + (1-L_c_array**2 * (wg.beta**2 -wg.n_s**2 * wg.k**2))**2)\
                + 1 - L_c_array**2 * (wg.beta**2-wg.n_s**2*wg.k**2))\
                / np.sqrt(4 * wg.beta**2 * L_c_array**2 + (1-L_c_array**2 * (wg.beta**2 - wg.n_s**2 *  wg.k**2))**2)



        L_c = L_c_array[np.argmax(G)]
        print L_c
        # plt.plot(L_c_array,G)
        # plt.show()
        if np.argmax(G) == 0 or  np.argmax(G) == 1000:
            print 'ERROR: something went wrong. there is no maximum for L_c'

    Gs = (L_c*wg.beta)\
    * np.sqrt(np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 -wg.n_s**2 * wg.k**2))**2)\
    + 1 - L_c**2 * (wg.beta**2-wg.n_s**2*wg.k**2))\
    / np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 - wg.n_s**2 *  wg.k**2))**2)
    

    Gc = (L_c*wg.beta)\
    * np.sqrt(np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 -wg.n_c**2 * wg.k**2))**2)\
    + 1 - L_c**2 * (wg.beta**2-wg.n_c**2*wg.k**2))\
    / np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 - wg.n_c**2 *  wg.k**2))**2)


    # Gs = getGs(wg,L_c)
    # Gc = getGc(wg,L_c)
    n_s,n_f,n_c,N = wg.getRefractiveIndexes()

    # #assymetry term
    # J = (np.cos(wg.d_f*wg.beta*(wg.n_f**2-wg.N**2)**(1/2.)/wg.N)\
    #     +(wg.N**2-wg.n_c**2)**(1/2.) / (wg.n_f**2-wg.N**2)**(1/2.) \
    #     *np.sin(wg.d_f*wg.beta*(wg.n_f**2-wg.N**2)**(1/2.)/wg.N))**2


    J = wg.asymmetryParameterJ()
    print(J)

    #there might be an 1/N to much in this equation
    alpha = np.sqrt(2)/2.*np.pi**2 * (wg.n_f**2 - wg.N**2) / (wg.t_eff * (wg.n_f**2-wg.n_c**2)*wg.n_f*wg.N)\
            *((wg.n_f**2-wg.n_c**2)**2+ J**2 * (wg.n_f**2 - wg.n_s**2)**2)\
            *(Gc + Gs)\
            *rmsRoughness**2 /(wg.wavelength**2)

    # theta = np.arcsin(wg.beta/(wg.n_f*wg.k))
    # K = 4. * np.pi / wg.wavelength * np.sqrt(2)*rmsRoughness
    # alpha = K**2 * 1./2 * np.cos(theta)**3/np.sin(theta) * 1./wg.t_eff
    

    # alpha2 = 1/4.*wg.coefA(0)**2*wg.N/(constants.Z_w*wg.inputPower) *(wg.n_f**2-wg.n_c**2)**2\
    #         * wg.beta**2/(wg.N**3*wg.n_f)\
    #         *rmsRoughness**2 * G




    return alpha


def getGc(wg,L_c):


    Gc = (L_c*wg.beta)\
    * np.sqrt(np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 -wg.n_c**2 * wg.k**2))**2)\
    + 1 - L_c**2 * (wg.beta**2-wg.n_c**2*wg.k**2))\
    / np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 - wg.n_c**2 *  wg.k**2))**2)

    return Gc



def getGs(wg,L_c):

    Gs = (L_c*wg.beta)\
    * np.sqrt(np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 -wg.n_s**2 * wg.k**2))**2)\
    + 1 - L_c**2 * (wg.beta**2-wg.n_s**2*wg.k**2))\
    / np.sqrt(4 * wg.beta**2 * L_c**2 + (1-L_c**2 * (wg.beta**2 - wg.n_s**2 *  wg.k**2))**2)

    return Gc





def calculateAaniAlpha(wg,NA,rmsRoughness,L_c):

    J = wg.asymmetryParameterJ()

    lowerBoundary = np.pi/2  - np.arcsin(NA/wg.n_s)
    upperBoundary = np.pi/2  + np.arcsin(NA/wg.n_s)
    function = lambda theta: 1/(1 + L_c**2 *(wg.beta - wg.k*wg.n_s*np.cos(theta))**2)

    integral = quad(function,lowerBoundary,upperBoundary)[0]

    ################################################################################################
    ################################################################################################
    # should be the right equations
    definitionFactorPlane = 2*np.pi/(2*(NA/wg.n_s))
    a_anialpha = np.pi \
            *(wg.n_f**2 - wg.N**2) / (wg.t_eff * (wg.n_f**2-wg.n_c**2)*wg.n_f*wg.N)\
            *((wg.n_f**2-wg.n_c**2)**2+ J**2 * (wg.n_f**2 - wg.n_s**2)**2)\
            *rmsRoughness**2 /(wg.wavelength**2)\
            *L_c * wg.beta * integral * definitionFactorPlane #2*np.pi/(upperBoundary-lowerBoundary)

    return a_anialpha

def alphaFormAttenuation(attenuation):
    """dB to neper conversion


    :param attenuation: attnuation [neper/m]
    :type alpha: float
    """
    attenuation = attenuation * 10**2
    alpha = np.log(10)/10. * attenuation
    return alpha

def attenuation(alpha):
    """neper/m to dB/cm connversion
    definition as power same as from payne lacey
    :param alpha: attnuation constant [neper/m]
    :type alpha: float

    """
    
    attenuation = 10./np.log(10) * alpha #[dB/m]
    attenuation = attenuation * 10**-2 #dB/cm
    return attenuation

################################################################################################
################################################################################################


def Iscat(alpha,NA,power,a_ani = 1.):
    """Calculates the intensity scattered into the focal spot from the surface rougheness
    fro a given attenuation approximatively.

    :param alpha: attenuation constants [neper/m]
    :type alpha: float
    :param NA: numerical aperture
    :type NA: float
    :parama power: power per length inside the waveguide [W/m]
    :type power: float
    """

    #mno multiplation by D although power is in W/m because this is already accounted for in the equation
    Iscat = NA**2 * alpha* power/4

    return Iscat

def leakageParameterBulkWG(waveguide,rho,C_scat):
    """Calculates the bulk leakage coefficient in the waveguide due to refractive index inhomogenities.

    ATTENTION: This function has not been tested yet!

    :param waveguide: instance of class waveguide
    :type waveguide: SlabWaveguide
    :param power: power per length inside the waveguide [W/m]
    :type power: float
    :param rho: density of the rayleigh scatter in the waveguide material [#/m^2]
    :type rho: float
    :param C_scat: scattering cross section of the rayleigh scatterers [m^2]
    :type C_scat: float
    
    .. math:: {\\alpha _{\\text{b}}} = {\\rho _{\\text{b}}}{C_{{\\text{sca}}}}\\frac{1}{{{t_{{\\text{eff}}}}}}\\left( {{t_{\\text{f}}} + \\frac{\\lambda }{{2\\pi }}\\left( {\\frac{{\\sqrt {{N^2} - {n_{\\text{c}}}^2} }}{{{n_{\\text{f}}}^2 - {n_{\\text{c}}}^2}} + \\frac{{\\sqrt {{N^2} - {n_{\\text{s}}}^2} }}{{{n_{\\text{f}}}^2 - {n_{\\text{s}}}^2}}} \\right)} \\right)

    """

    raise NotImplementedError, "This function has neither been tested nor verfied!! Do not use!"

    wl = waveguide.wavelength
    n_s,n_f,n_c,N = waveguide.getRefractiveIndexes()
    t_eff = waveguide.t_eff
    t_f = waveguide.thickness

    return rho*C_sca*1./t_eff * \
        (t_f + \
        wl/(2*np.pi) * \
        (np.sqrt(N**2 - n_c**2) / (n_f**2 - n_c**2) +  \
         np.sqrt(N**2 - n_s**2) / (n_f**2 - n_s**2)))


def Iscat_solution(concentration,f,D,waveguide,M_P=52.8e3):
    """Calculates the intensity scattered by t2115601.88579he scatterers in a solution
    on a diffraction limited spot after interacting with the 
    evanescent field sneglecting attenuation
    
    :param concentration: concentration of the particles in solution [mol/L]
    :type concentration: float
    :param f: focal distance [m]
    :type f: float
    :param D: mologram diameter
    :type D: float
    :param waveguide: instant of a waveguide class
    :type waveguide: SlabWaveguide
    :param M_P: molecular mass Portein [g/mol]
    :type M_P: float = 52.8e3





    """

    #makes a instance for the protein and caculates the refractive index
    Protein = ProteinAdlayer(M_protein = M_P)
    M_P = M_P * 10**-3 #tranfor from g/mol to kg/mol
    n_P = Protein.refIndexProtein()

    concentration = concentration * 10**3 #conversion from mol/L to mol/m^3
    dndc = constants.dn_dc * 10**-3 #conversion from ml/g to m^3/kg



    I_scat = concentration*constants.N_A * 9* np.pi**3* waveguide.n_c**4 \
            / waveguide.wavelength**4\
            * dndc**2 * M_P**2/constants.N_A**2 * (n_P + waveguide.n_c)**2/(n_P**2 + 2*waveguide.n_c**2)**2\
            * waveguide.n_c/(2*constants.Z_w) * waveguide.coefA(0)**2/(2*waveguide.delta)\
            *(D**2/(4*f**2))


    #difference between formula of silvio and adams. One uses N the other uses N_c

    return I_scat


def alpha_NSB(NSBsurfaceMassDensity,waveguide,M_NSB=52.8e3):
    """Calculates the intensity scattered by the scatterers in a solution
    on a diffraction limited spot after interacting with the 
    evanescent field sneglecting attenuation
    
    :param NSBsurfaceMassDensity: Surface Mass density of the NSB [pg/mm^2]
    :type concentration: float
    :param f: focal distance [m]
    :type f: float
    :param D: mologram diameter
    :type D: float
    :param waveguide: instant of a waveguide class
    :type waveguide: SlabWaveguide
    :param M_P: molecular mass Portein [g/mol]
    :type M_P: float = 52.8e3




    """

    #makes a instance for the protein and caculates the refractive index
    Protein = ProteinAdlayer(M_protein = M_NSB)

    M_NSB = M_NSB * 10**-3 #tranfor from g/mol to kg/mol
    n_NSB = Protein.refIndexProtein()

    NSBsurfaceMassDensity = NSBsurfaceMassDensity* 10**-9 #conversion from pg/mm^2 to kg/m^2
    dndc = constants.dn_dc * 10**-3 #conversion from ml/g to m^3/kg


    alpha = 4*18 * np.pi**3 * waveguide.n_c**5/ waveguide.n_s \
            *(n_NSB + waveguide.n_c)**2/(n_NSB**2 + 2*waveguide.n_c**2)**2\
            *(waveguide.n_f**2 - waveguide.N**2)**2 / (waveguide.N * waveguide.t_eff * (waveguide.n_f**2 - waveguide.n_c**2))**2\
            *dndc**2* M_NSB/constants.N_A * NSBsurfaceMassDensity/waveguide.wavelength**4
            


    #difference between formula of silvio and adams. One uses N the other uses N_c

    return alpha



def Iscat_NSB(NSBsurfaceMassDensity,f,D,waveguide,M_NSB=52.8e3):
    """Calculates the intensity scattered by the scatterers in a solution
    on a diffraction limited spot after interacting with the 
    evanescent field sneglecting attenuation
    
    :param NSBsurfaceMassDensity: Surface Mass density of the NSB [pg/mm^2]
    :type concentration: float
    :param f: focal distance [m]
    :type f: float
    :param D: mologram diameter
    :type D: float
    :param waveguide: instant of a waveguide class
    :type waveguide: SlabWaveguide
    :param M_P: molecular mass Portein [g/mol]
    :type M_P: float = 52.8e3

    """

    #makes a instance for the protein and caculates the refractive index
    Protein = ProteinAdlayer(M_protein = M_NSB)

    M_NSB = M_NSB * 10**-3 #tranfor from g/mol to kg/mol
    n_NSB = Protein.refIndexProtein()

    NSBsurfaceMassDensity = NSBsurfaceMassDensity* 10**-9 #conversion from pg/mm^2 to kg/m^2
    dndc = constants.dn_dc * 10**-3 #conversion from ml/g to m^3/kg



    I_scat = 18 * np.pi * NSBsurfaceMassDensity * waveguide.n_c**4 \
            / waveguide.wavelength**4\
            * dndc**2 * M_NSB/constants.N_A * (n_NSB + waveguide.n_c)**2/(n_NSB**2 + 2*waveguide.n_c**2)**2\
            * waveguide.n_c/(constants.Z_w) * waveguide.coefA(0)**2\
            *(D**2/(4*f**2))


    #difference between formula of silvio and adams. One uses N the other uses N_c

    return I_scat



def Iscat_Dust(N_dust,f,wg,D_scat=2e-6,scat_perLine = 0.02):
    """Calculates the intensity in the focal spot coming from dust as scattering sites.
    The amount of scatering for single particles has been determined with FDTD calculations


    :param N_dust: number of Dust particle per mologramm
    :type N_dust: float
    :param f: focal distance [m]
    :type f: flaot
    :param wg: waveguide
    :type wg: waveguide instance
    :param D_scat: size of the dust particles [m]
    :type D_scat: float = 2.e-6 (reasonable value)
    :param scat_perLine: fraction of light scatered for 1 particle per meter
    :type scat_perLine: float = 0.02 (value determined by FDTD for Ta2O5 waveguide)
    

    """
    
    

    Iscat_surfacParticle = wg.n_c/(2*constants.Z_w)*D_scat*wg.coefA(0)**2*scat_perLine
    Iscat_focal =   N_dust *np.pi * D_scat**2/(4* np.pi * f**2)
    return Iscat_focal


def backgroundScatteringWaveguide(P_0, alpha, f, D = 400e-6, x_0 = -4e-3,a = 0.054,n = 1.):
    """ Returns the background intensity that is scattered by the waveguide. The factor 1/4pi r^2 is necessary because the power is radiated into the entire solid angle.

    :param P_0: initial power in the waveguide per unit length [W/m]
    :param alpha: propagation loss of the waveguide in Np/m (this is a power quantity), calculation from [dB/cm] according to np.log(10)/10. * 100
    :param f: focal length
    :param D: (mologram) diameter (actually numerical aperture)
    :param x0: xshift, coupling grating relative to the waveguide.
    :param a: experimentally (AF027 012_isotropic scattering correction) determined for the nonuniform scattering of the radiated power.
    :param n: refractive index of the medium for which the mologram was designed for

    .. math:: I_bwg = \\alpha P_0 \\int\\limits_{0}^{2\\pi} \\int\\limits_{0}^{D/2} \\frac{e^{-\\alpha (\\rho \\cos \\phi - x_0)}}{4\\pi (f^2+\\rho^2)} \\rho d\\rho d\\phi

    """

    if n != 1:
        NA = numericalApertureGivenF_D_n(f,D,n)

        f = focalDistanceGivenNA_D_n(NA,D,1)   # We detect in air hence the focal length needs to be adjusted.

    # # cartesian coordinates
    # I_f = 1/(*w) * dblquad(lambda x, y: (P_0 * alpha * np.exp(-alpha*(x+np.abs(x_0))-2*y**2/w**2))/(4*np.pi*(f**2 + x**2 + y**2)), np.max((x_0,-D/2.)), D/2., lambda y: -D/2.,lambda y: D/2.)[0]

    # polar coordinates, this formula is correct A.F. I checked it multiple times, otherwise an assumption is not correct.



    I_bwg = a*P_0 * alpha/(4*np.pi) * dblquad(lambda phi, rho: np.exp(-alpha*(rho*cos(phi)-x_0))/(f**2 + rho**2) * rho, 0, D/2., lambda phi: 0,lambda phi: 2*np.pi)[0]

    return I_bwg

# To do: add sanity test functions for these three expressions

def backgroundScatteringWaveguideApproximative(P_wg, alpha, f, D, ns, a = 0.054):
	"""
	Calculates an approximative formula for the background, it agrees quite well with the exact expression. Parameters are the same as the exact expression above.

	.. math:: {I_{bg}}\\left[ {\\frac{W}{{{m^2}}}} \\right] = \\frac{{a \\cdot N{A^2} \\cdot \\alpha  \\cdot {P_{wg}}\\left[ {\\frac{W}{m}} \\right]}}{4}

	"""


	NA = numericalApertureGivenF_D_n(f,D,ns)
	# total power scattered


	return NA**2*alpha*P_wg/4.* a

def a_ani_from_power_intensity_damping_constant(P_wg, I_bg, alpha, NA):

    return 4*I_bg/(P_wg*NA**2*alpha)

def powerInWaveguidePerUnitLengthFromIntensityAtFocalPlace(I_bwg,alpha,f,D,ns,a = 0.054):
    """
    :param I_bwg: intensity in the focal plane in [W/m^2]
    .. math:: {P_{wg}}\\left[ {\\frac{W}{m}} \\right] = \\frac{{4 \\cdot {I_{bg}}\\left[ {\\frac{W}{{{m^2}}}} \\right]}}{{a \\cdot N{A^2} \\cdot \\alpha }}

    """
    NA = numericalApertureGivenF_D_n(f,D,ns)

    return 4*I_bwg/(alpha*a*NA**2)

def meanCircles(screen, intensity, circles, axis=0):
    """
    Averages the intensity in circles
    """
    radii = np.linspace(0, np.max(screen[axis]), circles)
    i = 0
    for i in np.arange(circles-1)+1:
        maskedArea = (screen[0]**2+screen[1]**2 < radii[i]**2) & (screen[0]**2+screen[1]**2 > radii[i-1]**2)
        intensity_mean = np.mean(intensity[maskedArea])
        intensity[maskedArea] = intensity_mean
        print intensity_mean

    return intensity

# Johnson-Nyquist noise

# instrumental background and noise
#poissonNoise = numpy.random.poisson(img)



def equivalentAmount(a_1, a_2, n_1, n_2, n_m, N_1):
    return (a_1/a_2)**3 * np.abs(((n_1**2-n_m**2)*(n_2**2+2*n_m**2))/((n_2**2-n_m**2)*(n_1**2+2*n_m**2)))




if __name__ == '__main__':
   
    
    # d_f_array = np.linspace(60,500,100)*1e-9
    d_f = 145e-9  #145e-9
    inputPower = 2e-3
    beamWaist = 1e-3
    
    effectivePowerperUnitLength = 2e-2#inputPower * couplingEfficiency/(beamWaist)
    wavelength_array = np.linspace(60,600,100) * 1e-9
    attenuationConstant = 0.8 * np.log(10)/10. * 100
    glassSubstrate = IsotropicMaterial(name='Glass Substrate (D 263)', n_0=1.521)
    filmMedium = IsotropicMaterial(name='Tantalum Pentoxide', n_0=2.117)
    wavelength=632.8e-9
    # TE
    atten_array = []
    beta_k_array = []
    NSBsurfaceMassDensity = 1000
    INSB = []
    N = []
    beta_array=[]


    waveguide = SlabWaveguide(
                    name = 'Tantalum Pentoxide', n_s = glassSubstrate.n, n_f = filmMedium.n, 
                    n_c = 1., d_f = d_f, polarization = 'TE', 
                    inputPower = effectivePowerperUnitLength, wavelength = wavelength,
                    attenuationConstant = attenuationConstant,
                    mode = 0)


    # L_c = np.linspace(0.1,1000,1000)*1e-9
    rmsRoughness = 0.45e-9
    L_c = 23e-9
    alphaR = calculateAlpha(waveguide,rmsRoughness,L_c=L_c)
    attenR = attenuation(alphaR)
    # attenTot = 2.51
    # alphaTot = alphaFormAttenuation(attenTot)
    # attenB = attenTot/10.
    # alphaB = alphaFormAttenuation(attenB)
    NA = 0.4
    aanialpha = calculateAaniAlpha(waveguide,NA,rmsRoughness,L_c)
    

    intensity = Iscat(aanialpha,NA,0.02)

    aani = aanialpha/alphaR
    wg = waveguide
    # print 'attenR', attenR
    # print 'attenB', attenB
    # print 'alphaR', alphaR
    # print 'alphaR2', alphaFormAttenuation(attenR)
    # print 'alphaB', alphaB
    # print 'ration R', alphaR / (alphaB + alphaR)
    # print 'ration B', alphaB / (alphaB + alphaR)
    print alphaR
    print 'atten', attenuation(alphaR)
    print 'aanialpha', aanialpha
    print 'aani', aani
    print 'alpha', aanialpha/aani
    print 'Ib', intensity
    print alphaFormAttenuation(2.51)*0.054
    print 'alpha mes',2.7e-3/NA**2 * 4 / 0.054 / 0.02
    print 'aanialpha mes', 2.7e-3/NA**2 * 4/ 0.02

    print 1.47/1.2
    print 62.5 * 0.054
    
    aaa = alphaFormAttenuation(2.51)*0.054
    print aaa
    print wg.n_c * (wg.n_f**2 -wg.N**2)/(wg.N * wg.t_eff *(wg.n_f**2-wg.n_c**2)* (wg.wavelength**4*1e30) *aaa)

    rmsRoughness = 0.08e-9
    L_c = 8.76e-9
    alphaR = calculateAlpha(waveguide,rmsRoughness,L_c=L_c)
    print alphaR
    attenR = attenuation(alphaR)
    print attenR


    # alpha = calculateAlpha(waveguide,0.59140402564e-9,L_c=L_c)
    
    
    # plt.plot(L_c,alpha)
    # plt.show()
    #plt.style.use('presentation')

    ################################################################################################
    ################################################################################################



    #-----------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------


    # w = 1e-3
    # D = 400e-6
    # npix = 500
    # x_0 = -4e-3
    # x = np.linspace(x_0, -x_0, npix)
    # y = np.linspace(-2*w, 2*w, npix)
    # f = 900e-6
    # alpha = 0.01
    # P_0 = 1e-3*0.05/(w*)

    # alpha_array = np.linspace(0,1,100)
    # I_bg1a = []
    # I_bg1b = []
    # for alpha in alpha_array:
    #     print (alpha)
    #     I_bg1 = backgroundScatteringWaveguide(P_0, alpha * log(10)/10. * 100, f, D=D,x_0=x_0)
    #     I_bg1a.append(I_bg1)
    #     # I_bg1b.append(I_bg1[1])

    # alpha = 0.8
    # D_array = np.linspace(0,400e-6,100)
    # I_bg2a = []
    # I_bg2b = []
    # for D in D_array:
    #     print(D)
    #     I_bg2 = backgroundScatteringWaveguide(P_0, alpha * log(10)/10. * 100, f, D,x_0=-D_array[-1])
    #     I_bg2a.append(I_bg2)
    #     # I_bg2b.append(I_bg2[1])

    # fig = plt.figure(figsize=(12,6))
    # ax = fig.add_subplot(1, 2, 1)
    # ax.plot(D_array*1e6, I_bg2a)
    # # plt.plot(D_array*1e3, I_bg2b)
    # ax.set_xlabel('Diameter [um]')
    # ax.set_ylabel('Background intensity [W/m^2]')

    # ax = fig.add_subplot(1, 2, 2)
    # ax.plot(alpha_array, I_bg1a)
    # # plt.plot(alpha_array, I_bg1b)
    # ax.set_xlabel('Propagation Loss [dB/cm]')
    # ax.set_ylabel('Background intensity [W/m^2]')


    # fig.savefig('../documentation/figures/background_2.png', bbox_inches='tight')
    # plt.show()


    # # 3D plots
    # from mpl_toolkits.mplot3d import axes3d
    # import matplotlib.ticker as mtick


    # alpha = 0.8 * np.log(10)/10. * 100
    # D = 400e-6

    # X, Y = np.meshgrid(x,y)
    # P = P_0 * np.exp(-alpha*(X+np.abs(x_0))-2*Y**2/w**2)
    # dP = P_0 * alpha /(*w) * np.exp(-alpha*(X+np.abs(x_0))-2*Y**2/w**2)*(x[1]-x[0])*(y[1]-y[0])
    # I = dP/(4*np.pi*(f**2 + X**2 + Y**2))

    # print(np.sum(I), backgroundScatteringWaveguide(P_0, alpha, f, D=D))

    # fig = plt.figure(figsize=(24,8))
    # ax = fig.add_subplot(1, 3, 1, projection='3d')
    # ax.plot_surface(X*1e3, Y*1e3, P*1e3)
    # ax.set_xlabel('X axis [mm]')
    # ax.set_ylabel('Y axis [mm]')
    # ax.set_zlabel('Power [mW]')

    # ax = fig.add_subplot(1, 3, 2, projection='3d')
    # ax.plot_surface(X*1e3, Y*1e3, dP*1e9)
    # ax.set_xlabel('X axis [mm]')
    # ax.set_ylabel('Y axis [mm]')
    # ax.set_zlabel('dP [nW]')

    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # ax.plot_surface(X*1e3, Y*1e3, I*1e6)
    # ax.set_xlabel('X axis [mm]')
    # ax.set_ylabel('Y axis [mm]')
    # ax.set_zlabel('Intensity [uW/m2]')

    # # plt.tight_layout()

    # fig.savefig('../documentation/figures/background_power.png', bbox_inches='tight')
    # plt.show()

