# -*- coding: utf-8 -*-
"""
This file creates a default setting database entry which 

@author: silvi
"""

import os
import numpy as np
from numpy import *

path = '../'
import sys
sys.path.append('../')
from auxiliaries.writeLogFile import checkFolder, saveDB, createDB, createSQLDict

# time and date
import datetime
import time
from variables import FieldSimulationInputs,FieldSimulationResults
from materials.materials import Particle
from simulations import FieldSimulation
from auxiliaries.writeLogFile import loadDB
import logging
logging.basicConfig(filename='example.log',level=logging.DEBUG)

####################################
# Description
####################################

# Calculates the waveguide background as a distribution of a fixed number of particles that are randomly
# distributed on the mologram

####################################
#####      Initialization      #####
####################################

##### Load default values ######

simInput = FieldSimulationInputs()

simInput.setValues(loadDB(path + 'database/default.db', 'log'))

globals().update(simInput.getValues())


##### General Settings ######

generalSettings =  {
        # general simulation settings
        'Extinction':0, # whether to calculate the extinction of the particles or not
        # database settings
        'databaseFile' : 'data/waveguideBG.db',
        'description':'100000 incoherent scatterers incorrectly produce a focal spot'
        }


##### Analyte Parameters ######

analyte_Radius = 0.3e-9           # particle radius in meters

analyte = Particle('Ta2O5', wavelength, analyte_Radius)
n_p = complex(analyte.refractiveIndex)


analyteParValues = {
    'analyte_name' : 'Ta2O5',
    'analyte_Radius' : analyte_Radius, # particle radius in [m]
    'analyte_RefractiveIndex' : n_p,
    'analyte_Ncoh':0, # placed with binding probability of 100 %
    'analyte_Nincoh':1000000, # placed with binding probability 50 %
    'analyte_MSN':0, # boolean, whether particle exhibits molecular shot noise
    'analyte_Ncoh_wMSN':100, # placed with binding probability of 100 %, are set when placing the binders
    'analyte_Nincoh_wMSN':500, # placed with binding probability 50 %, are set when placing the binders
    'analyte_z_min': 0,     # minimum distance of the particles above waveguide
    'analyte_z_max':2*analyte_Radius,      # maximum distance of the particles above waveguide          
    'analyte_placement':'Mololines', # Ridges (Sinusoidal), Ridges (Rectangular), Mololines.
    'analyte_seed':0, # boolean specifying whether to seed the bragg area
}

# set the simulation input values
simInput.setValues(generalSettings)

simInput.setValues(analyteParValues)

plots = {
    'Excitation_intensity':'Excitation_intensity_NRT_5000',  # plot of the excitation field at the scatterer position. 
    'Scattered_intensity':'Scattered_intensity_NRT_5000',  # plot of the scattered field at the focal plane.
    'Scattered_intensityBackground':'Scattered_intensity_Background_NRT_5000',  # plot of the scattered field at the focal plane.
    'scattererPlot':'scatterPlot_NRT_5000',  # plot of scatterer distribution on the mologram.
    }

print simInput.particlePar.getValues()['analyte_seed']

saveDict = {
    'ID':0,
    'I_sca':0,
    'I_bg':0
}

waveguideBackgroundSim = FieldSimulation(simInput,plots=plots,saveDict=saveDict)

waveguideBackgroundSim.startSimulation()

