

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import colors
import numpy as np
import matplotlib.pylab as plt


import matplotlib as mpl


def plotTwoParameterOptimization(x,y,Z, color_bar=True,ax=None):

	"""Plots a 2D parameter space.
   
	"""
	
	# returns the two screens (one axis is equal to 0)

	# parameter
	if ax is None:
		plt.figure()
		ax = plt.gca()

	X,Y = np.meshgrid(x,y)

	CS = ax.contourf(X, Y, Z, 
		levels = np.linspace(np.min(Z.real),np.max(Z.real),1000),
		cmap=plt.cm.viridis,
		origin = 'upper')

	# plt.xlabel(axis1)
	# plt.ylabel(axis2)
	#plt.axes().set_aspect('equal')
	# if name != '':
	# 	plt.title(name + " " + title)
	
	if color_bar:
		
		divider = make_axes_locatable(ax)
		cax1 = divider.append_axes("right", size="5%", pad=0.1)
		cbar = plt.colorbar(CS, cax = cax1, ticks=np.linspace(np.min(Z.real),np.max(Z.real),10), format='%.0e')
		cbar.ax.tick_params(labelsize=10) 
	
	return CS,ax

def plotFancy(data,params,paramlist,x_positions,colors,shapes,fills,selection):

		print colors

		for i,x_pos in enumerate(x_positions):
			#RWL
			for j,color in enumerate(colors):
				# IWL
				for k,shape in enumerate(shapes):
					#IMM
					for m,fill in enumerate(fills):
						#Pol

						# print i
						# print j
						# print k
						# print m
						y = data.loc[(data[params[0]]==paramlist[0][i]) & (data[params[1]]==paramlist[1][j]) & (data[params[2]]==paramlist[2][k]) & (data[params[3]]==paramlist[3][m]),selection]
						x = x_pos*np.ones(len(y))
						if fill == 'none':
							plt.plot(x*1e9,y,alpha=1,marker=shape,mec=color,mfc='none',linestyle='none',markersize=3,mew=1)
							# print paramlist[0][i]
							# print x_pos
							print y
							print paramlist[1][j]
							print color
							# print paramlist[2][k]
							print shape
							# print paramlist[3][m]
							# print fill
						else:
							#plt.plot(x*1e9,y,alpha=1,marker=shape,mec=color,mfc=color,linestyle='none',markersize=3)
							print "hello"


if __name__ =="__main__":

	print "hello"

	x = np.linspace(0,10,100)
	y = np.sin(x)
	
	formatOpticsLetters()
	plt.figure()
	plt.xlabel(r'$\Lambda$')
	plt.ylabel(r'$C_I$')
	plt.plot(x,y)

	plt.savefig('test.png',format='png',dpi=600)
	plt.show()
