"""
This is a collection of functions that allow to determine the different contrast of a phase mask process from a molographic measurement.

@ Created:		17-Dec-2016
@ Author:		Andreas Frutiger
@ Revised:		14-Mar-2018 by Cla Duri Tschannen
@ References:

CHANGES OVERVIEW:

Variable re-naming:
I_inc => I_0
R_I => r_I

Function naming: adopted lower_case_with_underscores style for new / replaced functions

Functions replaced:
CalculateOptimalIlluminationDoseForGivenRatio => calc_I0_for_max_activation_modulation
CalculateMaximumActivityModulationGivenRatio => calc_max_activation_modulation

Functions new:
calc_activation_function_for_plotting
calc_amount_of_binding_sites
calc_analyte_efficiency_normal
activation_distribution_sampling
activation_distribution_sampling_sinusoidal_approx

Notes: The replaced functions have been changed both inside and outside of the class (the old ones are commented out but not deleted yet)
The new functions have not yet been included into the class => need to decide which ones to inlcude and adopt the class accordingly (description, __init__)
I have not checked the other existing functions for correctness and not changed their naming.

"""


import numpy as np
import matplotlib.pylab as plt
import sys

import numpy as np
from numpy import *
import numpy.matlib
from scipy.special import jv, yn
from scipy.integrate import quad
import pandas as pd
import matplotlib as mpl
import sys
sys.path.append('../')
from auxiliaries.Journal_formats_lib import formatPRX


def molo_intensity_as_function_of_illumination_dose(I_0, b, C_I=0, A=4700000, r_I=0):
    # 4700000 corresponds to 2005 pg/mm^2 mass modulation. This parameter A needs to be determined extremely accurately, since the contrast predition depends very sensitively on it. The deprotection rate is very robust though.
    """
    This function returns the diffracted power of the mologram if the parameters A,b, C_I have been determined experimentally. The function can also be used in order to fit experimental datapoints and deduce the parameters from there.

    .. math::	P_{diff} = A \\cdot \\left({\\exp \\left( { - 2 \\cdot b \\cdot {I_{inc}} \\cdot \\left( {1 + {C_I}} \\right)} \\right) + \\exp \\left( { - 2 \\cdot b \\cdot {I_{inc}} \\cdot \\left( {1 - {C_I}} \\right)} \\right) - 2 \\cdot \\exp \\left( { - b \\cdot \\left( {2 \\cdot {I_{inc}}} \\right)} \\right)} \\right)

    :param I_0: incident intensity of the illumination setup
    :type I_0: float
    :param A: prefactor of the integral, corresponds to the maximum achievable mologram intensity for perfect contrast. default for Zeptoreader data: 1544017
    :type A: float
    :param b: deprotection rate of the photocleavable group of the polymer
    :type b: float
    :param C_I: Intensity contrast of the phase mask based on the assumption that mass and activation contrast are linearly related, if Zero the intensity ratio will be used.
    :param r_I: Intensity ratio of the phase mask based on the assumption that mass and activation contrast are linearly related, if Zero the intensity contrast will be used.

    :type C_I: float"""

    if C_I != 0:
        return A * (np.exp(-2 * b * I_0 * (1 + C_I)) + np.exp(-2 * b * I_0 * (1 - C_I)) - 2 * exp(-b * (2 * I_0)))
    else:
        I_Grov = 2 * I_0 / (1. + r_I)
        I_Rid = 2 * I_0 / (1. + 1. / r_I)

        return A * ((1 - np.exp(-b * I_Rid)) - (1 - np.exp(-b * I_Grov)))**2


def ZeptoreaderIntensityFunctionOfIlluminationDoseContrast(I_0, b, C_I):

    return molo_intensity_as_function_of_illumination_dose(I_0, b, C_I=C_I, A=4700000)


def ZeptoreaderIntensityFunctionOfIlluminationDoseRatio(I_0, b, r_I):
    """It seems that the Ratio fit is much less robust than the Contrast fit, you should use the other function."""

    return molo_intensity_as_function_of_illumination_dose(I_0, b, A=4700000, r_I=r_I)


def CalculateRatioFromContrast(C):
    """
    .. math:: R = \\frac{1+C}{1-C}

    """

    return (1. + C) / (1. - C)


def CalculateContrastFromRatio(R):
    """
    .. math:: C = \\frac{{R - 1}}{{R + 1}}

    """
    return (R - 1.) / (R + 1.)

# def CalculateOptimalIlluminationDoseForGivenRatio(R,b):
# 	"""
#
# 	:param R: Intensity Ratio of the phase mask
# 	:param b: deprotection rate of the photoprotective group
#
# 	.. math:: {I_{in{c_{opt}}}} = \\frac{{{r_I} \\cdot \\log \\left[ {\\frac{1}{{{r_I}}}} \\right]}}{{b \\cdot \\left( {1 - {r_I}} \\right)}}
#
# 	"""
#
# 	return R*np.log(1/R)/(b*(1-R))

#### CDT ####
def calc_I0_for_max_activation_modulation(b=0.0004, r_I=1.3):
    """
    Calculates the incident intensity I_0 that maximizes the activation modulation.

    .. math:: \\frac{\\log({r_I})\\cdot(1+{r_I})}{2\\cdotb\\cdot({r_I-1})}

    :param b: Deprotection rate of the photosensitive group
    :param r_I: Ratio of peak intensity of ridges vs. grooves (phase mask quality)
        """

    I_0 = np.log(r_I) * (1. + r_I) / (2. * b * (r_I - 1))
    return I_0

# def CalculateMaximumActivityModulationGivenRatio(R):
# 	"""Calculates the maximum activation modulation for a given intensity ratio of the phase mask
#
# 	:param R: Intensity ratio of the phase mask
# 	:return: maximum activity modulation [%]
#
# 	.. math:: {\\Delta _{{\\phi _{\\max }}}} = 100\\left( {{{\\left( {\\frac{1}{{{r_I}}}} \\right)}^{\\frac{{2{r_I}}}{{{r_I}^2 - 1}}}} - {{\\left( {\\frac{1}{{{r_I}}}} \\right)}^{\\frac{{2{r_I}^2}}{{{r_I}^2 - 1}}}}} \\right)
#
# 	"""
#
# 	return 100.*((1/R)**(2*R/(R**2-1))-(1/R)**(2*R**2/(R**2-1)))

#### CDT ####
def calc_max_activation_modulation(r_I=1.3):
    """
    Calculates the maximum activation modulation of the normal process for a given intensity ratio.

    .. math:: \\Delta\\phi_{max} = (r_I - 1)\\cdot(r_I ^ {-\\frac{r_I}{r_I - 1}})

    :param r_I: Intensity ratio (phase mask quality)

    """
    return (r_I - 1) * (r_I**(-r_I / (r_I - 1.)))


# personal note, I should make a class of this (Phase mask illumination system) and inhereit a standard class with standard parameters the current values determined in the experiments!.

def ActivationRidges(I_0, b, C_I=0, r_I=0):
    """ This function calculates the activation of the grooves for a given mask () and activation function of the brushed polymer layer.

    .. math:: {\\phi _ + } = 100 \\cdot \\left( {1 - \\exp \\left( { - b \\cdot {I_ + }} \\right)} \\right)
    .. math:: {I_ + } = {I_{inc}} \\cdot \\left( {1 + {C_I}} \\right)
    .. math:: {I_ + } = \\frac{2\\cdot I_{inc}}{\\left( \\frac{1}{r_{I}} + 1 \\right)}

    :param I_0: incident intensity of the illumination setup
    :type I_0: float
    :param b: deprotection rate of the photocleavable group of the polymer
    :type b: float
    :param C_I: Intensity contrast of the phase mask based on the assumption that mass and activation contrast are linearly related.
    :type C_I: float
    :param r_I: Intensity ratio of the phase mask based on the assumption that mass and activation ratio are linearly related.
    :type C_I: float

    :return: Activation of ridges in %.
    """
    if C_I != 0:

        I_Rid = I_0 * (1. + C_I)
    else:
        I_Rid = 2 * I_0 / (1. + 1. / r_I)

    phi_Rid = 100. * (1. - np.exp(-b * I_Rid))
    return phi_Rid


def ActivationGrooves(I_0, b, C_I=0, r_I=0):
    """ This function calculates the activation of the grooves for a given mask and activation function of the brushed polymer layer.

    .. math:: {\\phi _ - } = 100 \\cdot \\left( {1 - \\exp \\left( { - b \\cdot {I_ - }} \\right)} \\right)
    .. math:: {I_ - } = {I_{inc}} \\cdot \\left( {1 - {C_I}} \\right)
    .. math:: {I_ - } = \\frac{2\\cdot I_{inc}}{\\left(r_{I} + 1 \\right)}

    :param I_0: incident intensity of the illumination setup
    :type I_0: float
    :param b: deprotection rate of the photocleavable group of the polymer
    :type b: float
    :param C_I: Intensity contrast of the phase mask based on the assumption that mass and activation contrast are linearly related.
    :type C_I: float
    :return: Activation of grooves in %.
    """

    if C_I != 0:

        I_Grov = I_0 * (1. - C_I)
    else:
        I_Grov = 2 * I_0 / (1. + r_I)

    phi_Grov = 100. * (1. - np.exp(-b * I_Grov))

    return phi_Grov


def CalcActivationContrast(I_0, b, C_I, inverse=False):
    """Calculates the Activation contrast, given an Intensity contrast, an activation function of the brushed polymer as well as for a given intensity.

    :param I_0: Incident Dose
    :type I_0: float
    :param b: deprotection rate
    :type b: float
    :param C_I: Intensity contrast
    :param inverse: boolean, if true the Activation contrast of the inverse mologram is calculated.
    """

    C_phi = (ActivationRidges(I_0, b, C_I) - ActivationGrooves(I_0, b, C_I)) / \
        (ActivationGrooves(I_0, b, C_I) + ActivationRidges(I_0, b, C_I))

    # if the inverse
    if inverse:
        C_phi = (ActivationGrooves(I_0, b, C_I) - ActivationRidges(I_0, b, C_I)) / \
            (100 - ActivationGrooves(I_0, b, C_I) +
             100 - ActivationRidges(I_0, b, C_I))

    return C_phi

    # Something is still wrong with these functions.


def IntensityRidgesFromActivation(phi_Rid, b):
    """Calculates the intensity in the ridges as a function of the activation of the ridges and the deprotection rate of the photopolymer.

    :param phi_{Rid}: activation of the ridges
    :type phi_{Rid}: float
    :param b: deprotection rate of the photocleavable group of the polymer
    :type b: float
    .. math:: {I_ - } =  - {{\\ln \\left( {1 - {{{\\phi _ - }} \\over {100}}} \\right)} \\over b}
    """

    I_Rid = - np.log(1. - phi_Rid / 100.0) / b

    return I_Rid


def IntensityGroovesFromActivation(phi_Grov, b):
    """Calculates the intensity in the grooves as a function of the activation of the ridges and the deprotection rate of the photopolymer.

    :param phi_{Rid}: activation of the ridges
    :type phi_{Rid}: float
    :param b: deprotection rate of the photocleavable group of the polymer
    :type b: float

    .. math:: {I_ + } =  - {{\\ln \\left( {1 - {{{\\phi _ + }} \\over {100}}} \\right)} \\over b}

    """

    I_Grov = -np.log(1. - phi_Grov / 100.0) / b

    return I_Grov


def IntensityContrastFromActivationContrast(C_phi, b, I_0):
    """
    This function calculates the Intensity Contrast from a given activation contrast,
    a given deprotection rate and the incident intensity at which the activation contrast was determined.

    :param C_phi: Activation contrast
    :param b: deprotection rate
    :param I_0: Dose at which the activation contrast was measured.
    .. math:: {\\phi _ + } =  - {\\phi _ - }{{{C_\\phi } + 1} \\over {{C_\\phi } - 1}}
    .. math:: a: =  - {{{C_\\phi } + 1} \\over {{C_\\phi } - 1}}
    .. math:: {I_ + } = {{\\left( {\\ln \\left( {{{\\left( {1 - a} \\right) \\cdot \\left( { - {e^{2 \\cdot b \\cdot {I_{inc}}}}} \\right) + \\sqrt {{{\\left( {1 - a} \\right)}^2} \\cdot {e^{4 \\cdot b \\cdot {I_{inc}}}} + 4 \\cdot a \\cdot {e^{2b \\cdot {I_{inc}}}}} } \\over {2a}}} \\right)} \\right)} \\over b}
    .. math:: {C_I} = {{{I_ + } - \\left( {2 \\cdot {I_{inc}} - {I_ + }} \\right)} \\over {2 \\cdot {I_{inc}}}}
    """

    a = (C_phi + 1) / (C_phi - 1) * (-1)

    I_Rid = (np.log(((1 - a) * (-np.exp(2 * b * I_0)) + np.sqrt((1 - a) **
                                                                2 * np.exp(4 * b * I_0) + 4 * a * np.exp(2 * b * I_0))) / (2 * a))) / b

    print I_Rid

    C_I = (I_Rid - (2. * I_0 - I_Rid)) / (2. * I_0)

    return C_I

### CDT ####
def calc_activation_function_for_plotting(b=0.0004, r_I=1.3, I_0=0, maximize=True, inverse=False, approx=False):
    """
    Calculates the activation function or the inverse activation function, respectively, over the range [0, 2*pi].
    Main purpose of this function is use for plotting.

    :param b: Deprotection rate of the photosensitive group
    :param r_I: Ratio of peak intensity of ridges vs. grooves (phase mask quality)
    :param I_0: Incident intensity
    :param maximize: True => I_0 is calculated based on r_I such that the activation modulation is maximized (only makes sense for the normal process)
    :param inverse: True => inverse activation function
    :param approx: True => the sinusoidal approximation to the activation function (normal process) is calculated

    """
    if inverse == True:
        assert I_0 != 0, "For the inverse process you need to specify an incident intensity I_0!"

    if approx == True:
        assert inverse == False, "There is no approximation for the inverse process!"

    x = np.linspace(0, 2 * np.pi, 100)

    if inverse == False:
        if maximize == True:
            I_0 = calc_I0_for_max_activation_modulation(b=b, r_I=r_I)
        if approx == False:
            phi = 1 - np.exp(-b * I_0 * ((r_I - 1.) /
                                         (r_I + 1.) * (np.sin(x)) + 1))
        else:
            A = I_0 * (r_I - 1.) / (r_I + 1.)
            C = I_0
            D = 1 - 0.5 * np.exp(-b * C) * (np.exp(-b * A) + np.exp(b * A))
            B = 0.5 * np.exp(-b * C) * (np.exp(b * A) - np.exp(-b * A))
            phi = B * np.sin(x) + D
    else:
        phi = 1 - \
            (1 - np.exp(-b * I_0 * ((r_I - 1.) / (r_I + 1.) * (np.sin(x)) + 1)))

    return x, phi

#### CDT ####
def calc_amount_of_binding_sites(b=0.0004, r_I=1.3, I_0=10000):
    """
    Calculates the area under the inverse activation function for a specified intensity ratio r_I relative to 2pi.

    :param b: Deprotection rate of the photosensitive group
    :param r_I: Ratio of peak intensity of ridges vs. grooves (phase mask quality)
    :param I_0: Incident intensity

    """
    x = np.linspace(0, 2 * np.pi, 1000)

    return (integrate.quad(lambda x: 1 - (1 - np.exp(-b * I_0 * (((r_I - 1.) / (r_I + 1.)) * (np.sin(x)) + 1))), 0, 2 * np.pi)[0]) / (2 * np.pi)

#### CDT ####
def calc_analyte_efficiency_normal(b=0.0004, r_I=1.3):
    """
    Calculates the analyte efficiency of molograms fabricated with the normal process. The incident intensity I_0 is determined
    based on the intensity ratio (phase mask quality) r_I such that it maximizes the activity modulation. This analytical model
    approximates the activation function with a sinusoid; the approximation gets worse as r_I increases.

    .. math:: \\frac{B}{2D}

    with

    .. math:: D = 1 - 0.5\\cdot\\exp(-bC)\\cdot(\\exp(-bA) + \\exp(bA))
    .. math:: B = 0.5\\cdot\\exp(-bC)\\cdot(\\exp(bA) - \\exp(-bA))

    where

    .. math:: A = I_{0,\\Delta\\phi_{max}}\\cdot\\frac{r_I - 1}{r_I + 1}
    .. math:: C = I_{0,\\Delta\\phi_{max}}

    :param b: Deprotection rate of the photosensitive group
    :param r_I: Ratio of peak intensity of ridges vs. grooves (phase mask quality)

    """

    I_0 = calc_I0_for_max_activation_modulation(b=b, r_I=r_I)

    A = I_0 * (r_I - 1.) / (r_I + 1.)
    C = I_0

    D = 1 - 0.5 * np.exp(-b * C) * (np.exp(-b * A) + np.exp(b * A))
    B = 0.5 * np.exp(-b * C) * (np.exp(b * A) - np.exp(-b * A))

    analyteEfficiency = B / (2. * D)

    return analyteEfficiency

#### CDT ####
def activation_distribution_sampling(N=0, I_0=0, r_I=0, b=0.0004, inverse=False, maximize=True, log=True):
    """
    Implementation of a Monte Carlo rejection sampling algorithm. Returns a list with N samples (x-coordinates
    between 0 and 2pi) generated from the activity modulation distribution

    .. math:: 1 - exp(-b\\cdot(A\\cdotsin(x)+C))

    with

    .. math:: A = \\frac{C\\cdot{r_I} - 1)}{{r_I} + 1}

    and

    .. math:: {I_0} = C

    The inverse process flips the activation distribution, thus resulting in

    .. math:: 1 - (1 - exp(-b\\cdot(A\\cdotsin(x)+C)))

    :param I_0: Incident intensity; corresponds to offset C in intensity distribution A\\sin(x)+C
    :param r_I: Ratio of peak intensity of ridges vs. grooves (phase mask quality)
    :param N: Sample size
    :param b: Deprotection rate
    :param inverse: True => inverse fabrication process
    :param maximize: True => I_0 is calculated based on r_I such that the activation modulation is maximized (only makes sense for the normal process)
    :param log: True => results are stated with a print command

    """
    if (maximize == True) & (inverse == True):
        print "It makes no sense to maximize the activation modulation for the inverse molograms!"

    if maximize:
        I_0 = calc_I0_for_max_activation_modulation(b=b, r_I=r_I)

    C = I_0

    A = (C * (r_I - 1.)) / (r_I + 1.)

    if log == True:
        if inverse:
            print "inverse:"
            print "r_I:", r_I, ", I_0:", I_0
        else:
            if maximize:
                print "normal (maximized):"
                print "r_I:", r_I, ", I_0:", I_0
            else:
                print "normal:"
                print "r_I:", r_I, ", I_0:", I_0

    sample = []

    if inverse:
        while len(sample) < N:
            X = np.random.uniform(0, 2 * np.pi)
            Y = 1 - (1 - np.exp(-b * I_0 *
                                ((r_I - 1.) / (r_I + 1.) * (np.sin(X)) + 1)))
            U = np.random.uniform(
                0, np.exp(-b * (I_0 * (1 - ((r_I - 1.) / (r_I + 1.))))))
            if U < Y:
                sample.append(X)

    else:
        while len(sample) < N:
            X = np.random.uniform(0, 2 * np.pi)
            Y = 1 - np.exp(-b * I_0 * ((r_I - 1.) /
                                       (r_I + 1.) * (np.sin(X)) + 1))
            U = np.random.uniform(0, 1)
            if U < Y:
                sample.append(X)

    if inverse:
        sampleforinfluence = [x + np.pi for x in sample]
        influence = np.sin(sampleforinfluence)
        mean = abs(np.mean(influence))

    else:
        influence = np.sin(sample)
        mean = abs(np.mean(influence))

    if log == True:
        print "mean influence (analyte efficiency): %.4f\n" % (mean)

    return sample, influence

#### CDT ####
def activation_distribution_sampling_sinusoidal_approx(b=0.0004, r_I=1.3, N=100):
    """
    Samples x-positions from the analytical approximation to the actual activation function in the normal process, such
    that the analytical formula can be verified.

    """

    sample = []

    I_0 = calc_I0_for_max_activation_modulation(b=b, r_I=r_I)

    A = I_0 * (r_I - 1.) / (r_I + 1.)
    C = I_0

    D = 1 - 0.5 * np.exp(-b * C) * (np.exp(-b * A) + np.exp(b * A))
    B = 0.5 * np.exp(-b * C) * (np.exp(b * A) - np.exp(-b * A))

    while len(sample) < N:
        X = np.random.uniform(0, 2 * np.pi)
        Y = B * np.sin(X) + D
        U = np.random.uniform(0, 1)
        if U < Y:
            sample.append(X)

    influence = np.sin(sample)
    mean = abs(np.mean(influence))

    print "mean influence (analyte efficiency): %.4f\n" % (mean)
    print "analytically expected: ", B / (2 * D)

    return sample, influence


class PhasemaskIllumiationSystem(object):
    """PhasemaskIllumiationSystem consisting of a mask with a intensity ratio/contrast ratio"""

    def __init__(self, r_I=0, b=0):

        if r_I is not 0:
            self.C_I = CalculateContrastFromRatio(r_I)

        self.r_I = r_I
        self.b = b

    @classmethod
    def NatureNanotech(cls):
        """Phase mask illumination system with the parameters determined in the Nature Nanotech paper"""

        self = cls()

        self.C_I = 0.180125444809
        self.r_I = 1.43939757288
        self.b = 0.00323522636204

        return self

    @classmethod
    def Standard405(cls):
        """Phase mask illumination system with the parameters determined from experiment: AF015"""

        self = cls()

        # Contrast was adapted such that I obtain 27 % modulation at 2000 mJ/cm^2.
        self.C_I = 0.37
        self.r_I = CalculateRatioFromContrast(self.C_I)
        self.b = 0.000404402906699

        return self

    def calculate_optimal_parameters(self):

        self.I_opt = self.calc_I0_for_max_activation_modulation()
        self.MaxActMod = self.calc_max_activation_modulation()

    # def CalculateOptimalIlluminationDose(self):
    # 	"""
    # 	.. math:: {I_{in{c_{opt}}}} = \\frac{{{r_I} \\cdot \\log \\left[ {\\frac{1}{{{r_I}}}} \\right]}}{{b \\cdot \\left( {1 - {r_I}} \\right)}}
    #
    # 	"""
    #
    # 	return self.r_I*np.log(1/self.r_I)/(self.b*(1-self.r_I))

    #### CDT ####
    def calc_I0_for_max_activation_modulation(self):
        """
        Calculates the incident intensity I_0 that maximizes the activation modulation.

        .. math:: \\frac{\\log({r_I})\\cdot(1+{r_I})}{2\\cdotb\\cdot({r_I-1})}

        :param b: Deprotection rate of the photosensitive group
        :param r_I: Ratio of peak intensity of ridges vs. grooves (phase mask quality)

        """

        I_0 = np.log(self.r_I) * (1. + self.r_I) / \
            (2. * self.b * (self.r_I - 1))
        return I_0

    # def CalculateMaximumActivityModulation(self):
    # 	"""Calculates the maximum activation modulation for a given intensity ratio of the phase mask
    #
    # 	:param R: Intensity ratio of the phase mask
    # 	:return: maximum activity modulation [%]
    #
    # 	.. math:: {\\Delta _{{\\phi _{\\max }}}} = 100\\left( {{{\\left( {\\frac{1}{{{r_I}}}} \\right)}^{\\frac{{2{r_I}}}{{{r_I}^2 - 1}}}} - {{\\left( {\\frac{1}{{{r_I}}}} \\right)}^{\\frac{{2{r_I}^2}}{{{r_I}^2 - 1}}}}} \\right)
    #
    # 	"""
    #
    # 	return 100.*((1/self.r_I)**(2*self.r_I/(self.r_I**2-1))-(1/self.r_I)**(2*self.r_I**2/(self.r_I**2-1)))

    #### CDT ####
    def calc_max_activation_modulation(self):
        """
        Calculates the maximum activation modulation of the normal process for a given intensity ratio.

        .. math:: \\Delta\\phi_{max} = (r_I - 1)\\cdot(r_I ^ {-\\frac{r_I}{r_I - 1}})

        :param r_I: Intensity ratio (phase mask quality)

        """
        return (self.r_I - 1) * (self.r_I**(-self.r_I / (self.r_I - 1.)))

    def ActivationRidges(self, I_0):
        """ This function calculates the activation of the grooves for a given mask () and activation function of the brushed polymer layer.

        .. math:: {\\phi _ + } = 100 \\cdot \\left( {1 - \\exp \\left( { - b \\cdot {I_ + }} \\right)} \\right)
        .. math:: {I_ + } = {I_{inc}} \\cdot \\left( {1 + {C_I}} \\right)
        .. math:: {I_ + } = \\frac{2\\cdot I_{inc}}{\\left( \\frac{1}{r_{I}} + 1 \\right)}

        :param I_0: incident intensity of the illumination setup
        :type I_0: float

        :return: Activation of ridges in %.
        """

        self.I_Rid = 2 * I_0 / (1. + 1. / self.r_I)

        phi_Rid = 100. * (1. - np.exp(-self.b * self.I_Rid))

        return phi_Rid

    def ActivationGrooves(self, I_0):
        """ This function calculates the activation of the grooves for a given mask and activation function of the brushed polymer layer.

        .. math:: {\\phi _ - } = 100 \\cdot \\left( {1 - \\exp \\left( { - b \\cdot {I_ - }} \\right)} \\right)
        .. math:: {I_ - } = {I_{inc}} \\cdot \\left( {1 - {C_I}} \\right)
        .. math:: {I_ - } = \\frac{2\\cdot I_{inc}}{\\left(r_{I} + 1 \\right)}

        :param I_0: incident intensity of the illumination setup
        :type I_0: float
        :return: Activation of grooves in %.
        """

        self.I_Grov = 2 * I_0 / (1. + self.r_I)

        phi_Grov = 100. * (1. - np.exp(-self.b * self.I_Grov))

        return phi_Grov

    def CalcActivationContrast(self, I_0, inverse=False):
        """Calculates the Activation contrast, given an Intensity contrast, an activation function of the brushed polymer as well as for a given intensity.

        :param I_0: Incident Dose
        :type I_0: float
        """

        C_phi = (self.ActivationRidges(I_0) - self.ActivationGrooves(I_0)) / \
            (self.ActivationGrooves(I_0) + self.ActivationRidges(I_0))

        # if the inverse
        if inverse:
            C_phi = (self.ActivationGrooves(I_0) - self.ActivationRidges(I_0)) / \
                (100 - self.ActivationGrooves(I_0) +
                 100 - self.ActivationRidges(I_0))

        return C_phi

    def CalcActivationRatio(self, I_0, inverse=False):

        C_phi = self.CalcActivationContrast(I_0, inverse)
        R_phi = CalculateRatioFromContrast(C)

        return R_phi

    def CalcActModulation(self, I_0, inverse=False):

        return abs(self.ActivationRidges(I_0) - self.ActivationGrooves(I_0))


def test_IntensityContrastFromActivationContrast():

    assert 0.23 == IntensityContrastFromActivationContrast(
        CalcActivationContrast(184., 3.45 * 10**(-3), 0.23), 3.45 * 10**(-3), 184.)


if __name__ == "__main__":

    natureNanotech = PhasemaskIllumiationSystem.NatureNanotech()
    standard = PhasemaskIllumiationSystem.Standard405()

    print standard.r_I

    print standard.calc_I0_for_max_activation_modulation()

    print standard.ActivationRidges(2000)
    print standard.ActivationGrooves(2000)
    print standard.CalcActModulation(2000)

    I_0 = np.linspace(50, 20000, 500)

    # plt.plot(I_0,standard.CalcActModulation(I_0))
    # plt.show()

    ################################################################################################
    ################################################################################################
    # Penzberg Batch

    Gamma = 1650  # pg/mm^2

    expectedMassModulationForIlluminationDose = standard.CalcActModulation(
        I_0) * Gamma / 100
    formatPRX()
    plt.figure()
    plt.plot(I_0, expectedMassModulationForIlluminationDose)
    plt.xlabel(r'Exposure Dose $\mathrm{[mJ/cm^2]}$')
    plt.ylabel(r'$\Delta_{\Gamma_{\mathrm{SAv,PTP}}}$')
    plt.savefig(
        'Expected_mass_modulation as a function of illumination dose for Penzberg Batch.png', dpi=600, format='png')

    output = pd.DataFrame(list(zip(I_0, expectedMassModulationForIlluminationDose)),
                          columns=['I_0', 'expectedMassModulationForIlluminationDose'])  # these are the normalized intensities
    output.to_csv(
        'Expected_mass_modulation as a function of illumination dose for Penzberg Batch.csv', sep=';')
