import numpy as np
import matplotlib.pylab as plt
import pickle

class PhaseMaskSimulationEvaluation(object):
    """
    The functions contained in this class can be used to calculate analyte efficiency and fractional receptor density for normal
    and inverse mologram fabrication process as a function of distance from the phase mask and illumination dose used in the
    phase mask illumination step.

    Remarks:
    -the simulated intensity distributions need to be already available in the same folder
    -if multiple distances are specified, the output quantity is always an average over these distances
    -all quantities represent adequatly weighted averages
    -the focal spot intensity is in arbitrary units, i.e., it needs to be linearly scaled to fit experimental data
    -the focal spot intensity is the same for normal and inverse process, but the formula is not generally applicable
    (only when the fractional receptor density is proportional to the total mass density on the mologram)

    :param b: Deprotection rate of the photosensitive group
    :type b: float
    :param distance_from_PM: Distance behind phase mask [nm]
    :type distance_from_PM: list
    :param Dose: Illumination dose [mJ/cm2]
    :type Dose: list

    """

    def __init__(self, b=0.000583, distance_from_PM = [2000], Dose = [2000]):

        self.b = b
        self.distance_from_PM = distance_from_PM
        self.Dose = Dose

    def analyte_efficiency_normal(self):

        eta = []

        for i in self.distance_from_PM:

            with open('TM_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TM, weight_Lambda, Lambda = pickle.load(f)

            with open('TE_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TE, weight_Lambda, Lambda = pickle.load(f)

            norm_intensities = (np.asarray(norm_intensities_TE) + np.asarray(norm_intensities_TM))/2. # random polarization of input beam.

            ana_eff = []
            R_tilde = []

            for j, Dose in enumerate(self.Dose):

                ana_eff.append(0)
                R_tilde.append(0)

                for i, intensity in enumerate(norm_intensities):

                    R_tilde[j] += np.sum((1-np.exp(-self.b*norm_intensities[i]*Dose)))/len(norm_intensities[i])*weight_Lambda[i]  

                for i, intensity in enumerate(norm_intensities):

                    ana_eff[j] += np.sum(np.cos(4*np.pi/Lambda[i]*distance[i])*(1-np.exp(-self.b*norm_intensities[i]*Dose)))/np.sum((1-np.exp(-self.b*norm_intensities[i]*Dose))) \
                                    *np.sum((1-np.exp(-self.b*norm_intensities[i]*Dose)))/len(norm_intensities[i])*weight_Lambda[i]/(R_tilde[j])

            eta.append(ana_eff)

        eta_to_be_averaged = np.zeros(len(self.distance_from_PM))
        eta_distance_averaged = []

        for i in range(0, len(self.Dose)):
            for j in range(0, len(self.distance_from_PM)):
                eta_to_be_averaged[j] = eta[j][i]
            eta_distance_averaged.append(np.mean(eta_to_be_averaged))
            eta_to_be_averaged = np.zeros(len(self.distance_from_PM))

        return eta_distance_averaged

    def analyte_efficiency_inverse(self):

        eta = []

        for i in self.distance_from_PM:

            with open('TM_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TM, weight_Lambda, Lambda = pickle.load(f)

            with open('TE_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TE, weight_Lambda, Lambda = pickle.load(f)

            norm_intensities = (np.asarray(norm_intensities_TE) + np.asarray(norm_intensities_TM))/2. # random polarization of input beam.

            ana_eff = []
            R_tilde = []

            for j, Dose in enumerate(self.Dose):

                ana_eff.append(0)
                R_tilde.append(0)

                for i, intensity in enumerate(norm_intensities):

                    R_tilde[j] += np.sum((np.exp(-self.b*norm_intensities[i]*Dose)))/len(norm_intensities[i])*weight_Lambda[i] # weighted-average!

                for i, intensity in enumerate(norm_intensities):

                    ana_eff[j] += np.sum(np.cos(4*np.pi/Lambda[i]*distance[i] + np.pi)*(np.exp(-self.b*norm_intensities[i]*Dose)))/np.sum((np.exp(-self.b*norm_intensities[i]*Dose))) \
                            *np.sum((np.exp(-self.b*norm_intensities[i]*Dose)))/len(norm_intensities[i])*weight_Lambda[i]/(R_tilde[j])

            eta.append(ana_eff)

        eta_to_be_averaged = np.zeros(len(self.distance_from_PM))
        eta_distance_averaged = []

        for i in range(0, len(self.Dose)):
            for j in range(0, len(self.distance_from_PM)):
                eta_to_be_averaged[j] = eta[j][i]
            eta_distance_averaged.append(np.mean(eta_to_be_averaged))
            eta_to_be_averaged = np.zeros(len(self.distance_from_PM))

        return eta_distance_averaged

    def Rtilde_normal(self):

        Rtilde = []

        for i in self.distance_from_PM:

            with open('TM_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TM, weight_Lambda, Lambda = pickle.load(f)

            with open('TE_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TE, weight_Lambda, Lambda = pickle.load(f)

            norm_intensities = (np.asarray(norm_intensities_TE) + np.asarray(norm_intensities_TM))/2. # random polarization of input beam.

            R_tilde = []

            for j, Dose in enumerate(self.Dose):

                R_tilde.append(0)

                for i, intensity in enumerate(norm_intensities):

                    R_tilde[j] += np.sum((1-np.exp(-self.b*norm_intensities[i]*Dose)))/len(norm_intensities[i])*weight_Lambda[i] # average, NOT weighted-average!

            Rtilde.append(R_tilde)

        Rtilde_to_be_averaged = np.zeros(len(self.distance_from_PM))
        Rtilde_distance_averaged = []

        for i in range(0, len(self.Dose)):
            for j in range(0, len(self.distance_from_PM)):
                Rtilde_to_be_averaged[j] = Rtilde[j][i]
            Rtilde_distance_averaged.append(np.mean(Rtilde_to_be_averaged))
            Rtilde_to_be_averaged = np.zeros(len(self.distance_from_PM))

        return Rtilde_distance_averaged

    def Rtilde_inverse(self):

        Rtilde = []

        for i in self.distance_from_PM:

            with open('TM_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TM, weight_Lambda, Lambda = pickle.load(f)

            with open('TE_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TE, weight_Lambda, Lambda = pickle.load(f)

            norm_intensities = (np.asarray(norm_intensities_TE) + np.asarray(norm_intensities_TM))/2. # random polarization of input beam.

            R_tilde = []

            for j, Dose in enumerate(self.Dose):

                R_tilde.append(0)

                for i, intensity in enumerate(norm_intensities):

                    R_tilde[j] += np.sum((np.exp(-self.b*norm_intensities[i]*Dose)))/len(norm_intensities[i])*weight_Lambda[i] # average, NOT weighted-average!

            Rtilde.append(R_tilde)

        Rtilde_to_be_averaged = np.zeros(len(self.distance_from_PM))
        Rtilde_distance_averaged = []

        for i in range(0, len(self.Dose)):
            for j in range(0, len(self.distance_from_PM)):
                Rtilde_to_be_averaged[j] = Rtilde[j][i]
            Rtilde_distance_averaged.append(np.mean(Rtilde_to_be_averaged))
            Rtilde_to_be_averaged = np.zeros(len(self.distance_from_PM))

        return Rtilde_distance_averaged


    def focal_spot_intensity(self):

        focal_spot_intensity = []

        for i in self.distance_from_PM:

            with open('TM_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TM, weight_Lambda, Lambda = pickle.load(f)

            with open('TE_' + str(i) + 'nm_after_mask.pkl') as f:

                distance,norm_intensities_TE, weight_Lambda, Lambda = pickle.load(f)

            norm_intensities = (np.asarray(norm_intensities_TE) + np.asarray(norm_intensities_TM))/2. # random polarization of input beam.

            diffraction_efficiency = []

            for j, Dose in enumerate(self.Dose):

                diffraction_efficiency.append(0)

                for i, intensity in enumerate(norm_intensities):

                    diffraction_efficiency[j] += (np.sum(np.cos(4*np.pi/Lambda[i]*distance[i])*(1-np.exp(-self.b*norm_intensities[i]*Dose)))/len(norm_intensities[i]))*weight_Lambda[i]

            focal_spot_intensity.append(np.asarray(diffraction_efficiency)**2)

        intensity_to_be_averaged = np.zeros(len(self.distance_from_PM))
        intensity_distance_averaged = []

        for i in range(0, len(self.Dose)):
            for j in range(0, len(self.distance_from_PM)):
                intensity_to_be_averaged[j] = focal_spot_intensity[j][i]
            intensity_distance_averaged.append(np.mean(intensity_to_be_averaged))
            intensity_to_be_averaged = np.zeros(len(self.distance_from_PM))

        return intensity_distance_averaged



if __name__ == "__main__":

## How to use the PhaseMaskSimulationEvaluation class

    obj = PhaseMaskSimulationEvaluation()
    obj.Dose = np.linspace(1, 20000, 20)
    obj.Dose = [2000]
    obj.distance_from_PM = [1000, 1250, 1500, 1750, 2000, 2500, 3000, 4000, 5000, 6000]

    Rtilde = obj.Rtilde_normal()
    print "Rtilde: "
    print(Rtilde)
    Rtildeinverse = obj.Rtilde_inverse()

    eta = obj.analyte_efficiency_normal()
    print "eta: "
    print(eta)
    etainverse = obj.analyte_efficiency_inverse()

    intensities = obj.focal_spot_intensity()


    fig, ax = plt.subplots()
    ax.plot(obj.Dose, Rtilde, label="normal")
    ax.plot(obj.Dose, Rtildeinverse, label="inverse")
    ax.legend(loc=0)
    ax.set_ylim(0)
    ax.set_xlim(min(obj.Dose), max(obj.Dose))
    ax.set_ylabel("$\widetilde{R}$ [-]")
    ax.set_xlabel("$D_{\mathrm{I}}$ [mJ/cm$^2$]")
    plt.show()

    fig, ax = plt.subplots()
    ax.plot(obj.Dose, eta, label="normal")
    ax.plot(obj.Dose, etainverse, label="inverse")
    ax.legend(loc=0)
    ax.set_ylim(0)
    ax.set_xlim(min(obj.Dose), max(obj.Dose))
    ax.set_ylabel("$\eta_{\mathrm{[A]}}$ [-]")
    ax.set_xlabel("$D_{\mathrm{I}}$ [mJ/cm$^2$]")
    plt.show()

    fig, ax = plt.subplots()
    ax.plot(obj.Dose, intensities)
    ax.set_ylim(0)
    ax.set_xlim(min(obj.Dose), max(obj.Dose))
    ax.set_ylabel("$\\left( \widetilde{R} \cdot \eta_{\mathrm{[A]}} \\right)^2$ [a.u.]")
    ax.set_xlabel("$D_{\mathrm{I}}$ [mJ/cm$^2$]")
    plt.show()
