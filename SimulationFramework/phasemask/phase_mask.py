"""
:author: Andreas Frutiger
:created: 26.12.2016

"""

from mrcwa import *
import numpy as np
import sys
sys.path.append("../")
import auxiliaries.writeLogFile as WL
from auxiliaries.Journal_formats_lib import formatOpticsLetters

# Database support functions
import sqlite3
from pandas.io.sql import read_sql

from scipy.signal import convolve2d
from phasemask.plotsPhaseMask import *
import matplotlib.ticker as ticker
from phasemask.grating import Grating
from phasemask.Diffraction_grating import DiffractionGrating

from scipy.ndimage.filters import gaussian_filter
from joblib import Parallel
import multiprocessing
import os



def RoundToN(x, n):

	if type(x) == type(float()) or isinstance(x, np.float64):

		rounded = round(x, -int(np.floor(np.sign(x) * np.log10(abs(x)))) + n)
		
	else: 
		
		rounded = np.ones(len(x))

		for i,j in enumerate(x):
			
			rounded[i] = round(x[i], -int(np.floor(np.sign(x[i]) * np.log10(abs(x[i])))) + n)
	
	return rounded


class PhaseMaskCalculator(DiffractionGrating):
	"""This is a class that allows the optimization of the phase mask dutycycle and thickness weighted with the pattern of the molographic structure (line length). 
	It is linked to a database with already performed simulations in order not to repeat them again.
	
	:param mologram: Mologram object, will be used to determine the Lambda and the weight_Lambda
	:param n_i: refractive index of the immersion medium at the illumination wavelength
	:param n_p: refractive index of the phase shift layer at the illumination wavelength
	:param n_s: refractive index of the substrate material of the phase mask at the illumination wavelength.
	:param Ill_wl: illumination wavelength [m]
	:param Pol: Polarization of the incident light (TE,TM)
	:param height: thickness of the phase shift layer. [m]
	:param Lambda: numpy array of the periods of the mologram [m]
	:param Dutycycles: numpy array of all the dutycycles of the periods of the mologram. (In case an Etch Angle is specified, this is the average dutycycle.)
	:param Etch_Angle: The etch angle of the ridges [deg]
	:param weight_Lambda: numpy array of the weights of each line in the mologram (length), this is the relative contribution of the optimization of this particular linewidth
		the vector needs to be normalized (np.sum(weight_Lambda) = 1)
	:param Inc_Ang = 0: Incidence angle of the incident plane wave, default is 0
	:param NN = 10: Number of Fourier Components used in the simulation.
	:param eBeamOffset: [nm] overdevelopment of eBeam lines in PMMA, the written line will be larger by this amount, param was determined in AF030
	
	"""

	def __init__(self, mologram,n_i,n_p,n_s,Ill_wl,height,Lambdas,weight_Lambdas=1,Pol='TE',Dutycycles=[0.5],Etch_Angle=6.82,Inc_Ang = 0,NN = 10,eBeamOffset= 75e-9):

		DiffractionGrating.__init__(self,n_i,n_p,n_s,Ill_wl,height,Lambdas[0],Dutycycles[0],Pol=Pol,Etch_Angle=6.82,Inc_Ang = 0,NN = 10)

		if not os.path.exists('data'):
			os.makedirs('data')

		self.mologram = mologram
		# round all values to four significant digits

		self.Dutycycles = RoundToN(Dutycycles,1) # for some reason this means two significant digits
		self.weight_Lambdas = weight_Lambdas
		self.Lambdas = Lambdas
		self.eBeamOffset = eBeamOffset # offset of the eBeam patterning due to the finite resolution of the PMMA
		self.Etch_Angle = Etch_Angle

		# table name for the SQlite database
		self.table_name = 'PM{:4.0f}NI{:4.0f}NP{:4.0f}NS{}P{:3.0f}WL'.format(n_i*1e3,n_p*1e3,n_s*1e3,Pol,Ill_wl*1e9)
		self.mologram_params = 'MG{:3.0f}N{:3.0f}WL{:3.0f}radius{:3.0f}f'.format(
			mologram.waveguide.N*1e2,mologram.wavelength*1e9,mologram.radius*1e6,abs(mologram.focalPoint*1e6))
		self.waveguide_params = 'WG{:3.0f}D{:4.0f}NC{:4.0f}NF{:4.0f}NS{}P'.format(
			mologram.waveguide.d_f*1e9,mologram.waveguide.n_c*1e3,mologram.waveguide.n_f*1e3,mologram.waveguide.n_s*1e3,mologram.waveguide.polarization)
		
		self.simulation_name = self.table_name + self.mologram_params + self.waveguide_params
		
		self.createPMDatabase()
		# here I should check if the entry is already in the database Optimized Masks

	def optimizePhaseMask(self,robust = True,optimal = True, saveplots = False,epsilon_coarse = [1,1,10],epsilon_fine=[2,2,20]):
		"""Optimizes the phase mask via Brute Force optimization, first the state space is sampled coarsly to find the
		
		starting values for the subsequent fine grained optimization.
		
		:param robust: boolean, if true robust regularization is used to find the robust mask.
		:param optimal: boolean, if true the optimal mask is calculated in addition to the robust mask.
		:param epsilon_coarse: 3 valued list, contains the values for the robust regularization neighboorhood of the 
			coarse grained optimization, [D,h,Lambda] D,h are indexes, Lambda is in [m] these three 
			are the main determinants of the robustness of the resulting phase mask.
		:param epsilon_fine: 3 valued list, contains the values for the robust regularization neighboorhood of the 
			fine grained optimization, [D,h,Lambda] D,h are indexes, Lambda is in [m] these three 
			are the main determinants of the robustness of the resulting phase mask. 
		
		"""
		if robust == False and optimal == False:
			print "Not allowed, error should be thrown. Either robust or optimal has to be True. "

		D_start,h_start = self.coarseGrainedOptimization(robust = robust,optimal = optimal,saveplots=saveplots,epsilon=epsilon_coarse)

		if robust == True and optimal == True:
			D_start_optimal = D_start[0]
			D_start_robust = D_start[1]
			h_start_optimal = h_start[0]
			h_start_robust = h_start[1]

		if robust == False and optimal == True:
			D_start_optimal = D_start
			h_start_optimal = h_start

		if robust == True and optimal == False:
			D_start_robust = D_start
			h_start_robust = h_start
			
		if optimal == True:
			D_optimal,h_optimal,Intensity_Ratio_optimal = self.fineGrainedOptimization(D_start_optimal,h_start_optimal,
				robust = False,saveplots=saveplots,epsilon=epsilon_fine)
			weighted_Ratio_optimal = np.sum(Intensity_Ratio_optimal*self.weight_Lambdas)
		if robust == True:
			self.epsilon_name = 'Coarse_Deps_{:d}_heps{:d}_Leps_{:3.0f}_Fine_Deps_{:d}_heps{:d}_Leps_{:3.0f}'.format(
				epsilon_coarse[0],epsilon_coarse[1],epsilon_coarse[2]*1e9,epsilon_fine[0],epsilon_fine[1],epsilon_fine[2]*1e9)
		
			D_robust,h_robust,Intensity_Ratio_robust = self.fineGrainedOptimization(D_start_robust,h_start_robust,
				robust = True,saveplots=saveplots,epsilon=epsilon_fine)
			weighted_Ratio_robust = np.sum(Intensity_Ratio_robust*self.weight_Lambdas)

		if robust == True and optimal == True:
			self.D_robust = D_robust
			self.h_robust = h_robust
			self.D_optimal = D_optimal
			self.h_optimal = h_optimal
			
			if saveplots:
				self.plotDutycycles(robust)
			return [D_optimal,D_robust],[h_optimal,h_robust],[Intensity_Ratio_optimal,Intensity_Ratio_robust], [weighted_Ratio_optimal,weighted_Ratio_robust]
		
		if robust == False and optimal == True:
			self.D_optimal = D_optimal
			self.h_optimal = h_optimal
			if saveplots:
				self.plotDutycycles(robust)
			return D_optimal,h_optimal,Intensity_Ratio_optimal,weighted_Ratio_optimal
			
		if robust == True and optimal == False:
			self.D_robust = D_robust
			self.h_robust = h_robust
			if saveplots:
				self.plotDutycycles(robust)
			return D_robust,h_robust,Intensity_Ratio_robust,weighted_Ratio_robust

	def coarseGrainedOptimization(self,saveplots = False,optimal = True,robust = True,epsilon = [1,1,10]):
		"""Performs a coarse grained optimization of the search space by sampling Dutycycle, phase shift layer thickness and period space
		
		of the phase mask, the contribution of the individual periods is accounted for by the numpy array weight_Lambda

		:param saveplots: boolean, if the overview plots of the coarse grained optimization are to be saved, saveplots = True
		:param optimal: computes the optimal solution
		:param robust: computes the robust solution
		:param epsilon: 3 valued list, contains the values for the robust regularization neighboorhood, [D,h,Lambda] D,h, Lambda are indexes, these three 
			are the main determinants of the robustness of the resulting phase mask. 
		
		:return: Depends on the optimal and robust variable, if optimal == True, return: D_optimal,h_optimal; if robust = True, D_robust, h_robust; if optimal = True
		robust = True, [D_optimal, D_robust], [h_optimal, h_robust]
		D_optimal/D_robust is a numpy array with a starting value of the dutycycle for the fineGrainedOptimization,
		h_optimal/h_robust is the starting value for the thickness optimization. 
		
		"""

		self.D_space_coarse = RoundToN(np.arange(0.3,0.6+0.01,0.05),2) # smaller and larger dutycycles are simply not fabricatable. # 0.3,0.6,0.025
		start_height = self.estimatePMThickness()
		self.h_space_coarse = RoundToN(np.arange(start_height*0.8,start_height+start_height*0.3,5e-9),2) # steps of 5 nm. upper limit should be 0.3 instead of 0.2. 
		self.Lambdas_space_coarse = self.Lambdass # important, currently this needs to be calculated for every grating period, since there
		# is no interpolation for the starting Dutycycle value.

		# Values for the robust regularization neighborhood.
		if robust == True:
			self.epsilon_D_coarse = epsilon[0]							# 2/1
			self.epsilon_h_coarse = epsilon[1]
			self.epsilon_Lambda_coarse = epsilon[2] 						# 1/5

		
		# check whether the values for this simulation are already in the database
		selection = 'Intensity_Ratio_coarse'

		simulation_input = {
			
			'MologramType':self.simulation_name,
			'Etch_Angle':self.Etch_Angle
		
		}
		condition = self.formatSimulationInputAsSQL(simulation_input)

		# since it is the same for optimal and robust masks

		data_entry = WL.loadDB('data/phase_mask.db','CoarseIntensities',selection=selection,condition=condition)


		# check if the object is a list, if yes it is emtpy and the entry in the database does not exist.
		if type(data_entry) is not list:

			self.D_space_coarse = RoundToN(np.arange(0.3,0.3 + 0.05*(data_entry.shape[0]-1)+0.01,0.05),2) # since I changed this a few times. 
			self.intensity_ratio_coarse_optimization = data_entry

			#self.intensity_contrast_coarse_optimization = intensity_contrast_coarse_optimization['Intensity_Contrast_coarse']
		
		else:
			self.intensity_ratio_coarse_optimization = np.ones((len(self.D_space_coarse),len(self.h_space_coarse),len(self.Lambdas_space_coarse)))

			# calculate all the values of the coarse optimization.
			for i,D in enumerate(self.D_space_coarse):
				print D
				for j,h in enumerate(self.h_space_coarse):
					
					for k,Lambda in enumerate(self.Lambdas_space_coarse):
						#self.data[i,j,k] = self.performIndividualSimulation(D,h,Lambda)[0]
						self.intensity_ratio_coarse_optimization[i,j,k] = self.performIndividualSimulation(D,h,Lambda,Etch_Angle=self.Etch_Angle)[4]

			params = {
					'MologramType':self.simulation_name,
					'Intensity_Ratio_coarse':self.intensity_ratio_coarse_optimization,
					'D_space_coarse':self.D_space_coarse,
					'h_space_coarse':self.h_space_coarse,
					'Lambda_space_coarse':self.Lambdas_space_coarse,
					'Etch_Angle':self.Etch_Angle
			}

			WL.saveDB('data/phase_mask.db','CoarseIntensities',params)

		# robust optimization
		if robust == True:
			h_robust, h_index_robust,intensity_ratio_robust = self.findOptimalHeightRobustRegularization(
				self.Lambdas_space_coarse,self.D_space_coarse,self.h_space_coarse,self.intensity_ratio_coarse_optimization,
				self.epsilon_D_coarse,self.epsilon_h_coarse,self.epsilon_Lambda_coarse)

			D_index_robust = np.ones(len(self.Lambdas_space_coarse))
			D_robust = np.ones(len(self.Lambdas_space_coarse))

			for k,Lambda in enumerate(self.Lambdas_space_coarse):
				D_index_robust[k] = np.argmax(intensity_ratio_robust[:,h_index_robust,k])
				D_robust[k] = self.D_space_coarse[D_index_robust[k]]

		# optimal optimization
		if optimal == True:
			h_optimal, h_index_optimal = self.findOptimalHeight(
				self.Lambdas_space_coarse,self.D_space_coarse,self.h_space_coarse,self.intensity_ratio_coarse_optimization)
			intensity_ratio = self.intensity_ratio_coarse_optimization

			D_index_optimal = np.ones(len(self.Lambdas_space_coarse))
			D_optimal = np.ones(len(self.Lambdas_space_coarse))

			for k,Lambda in enumerate(self.Lambdas_space_coarse):
				D_index_optimal[k] = np.argmax(intensity_ratio[:,h_index_optimal,k])
				D_optimal[k] = self.D_space_coarse[D_index_optimal[k]]

		# plot is independent of robust or optimal execution. 
		if saveplots == True:
			formatOpticsLetters()
			plt.figure()
			plotTwoParameterOptimization(self.D_space_coarse,self.h_space_coarse*1e9,np.transpose(np.sum(self.intensity_ratio_coarse_optimization*self.weight_Lambdas,axis=2)))
			plt.xlabel(r'$D$')
			plt.ylabel(r'$h$ [nm]')
			plt.savefig('2D_weighted_coarse_grained_ratio_' + self.simulation_name + '.png',format='png',dpi=600)

		if optimal == True and robust == False:

			return D_optimal,h_optimal
		if optimal == True and robust == True:

			return [D_optimal,D_robust],[h_optimal,h_robust]
		if optimal == False and robust == True:

			return D_robust,h_robust

	def fineGrainedOptimization(self,D_start,h_start,saveplots = False,robust = True,epsilon = [2,2,20]):
		"""Performs the fine grained optimization of the phase mask. It requires the optimal starting values
		from the coarse grained optimization. 
		:params D_start: is a numpy array with all the starting dutycycle values for each grating period
		:params h_start: the start value of the phase mask thickness
		"""

		h_space = RoundToN(np.arange(h_start-10e-9,h_start+10e-9,1e-9),2) # steps of 1 nm.
		Lambda_space = self.Lambdass 
		
		self.intensity_ratio_fine_optimization = np.ones((11,len(h_space),len(Lambda_space)))
		
		D_spaces = np.ones((len(Lambda_space),11))

		# Values for the robust regularization neighborhood. (DO NOT CHANGE), it is +-0.04 for the Dutycycle, +- 2nm for the height and +- 5 nm for the grating period. 
		if robust == True:
			self.epsilon_D_fine = epsilon[0]
			self.epsilon_h_fine = epsilon[1]
			self.epsilon_Lambda_fine = epsilon[2]

		if robust == True:
			selection = 'Intensity_ratio_fine'
			# VERY IMPORTANT THE epsilons of the fine grained optimization do not matter which Intensity_ratio_fine is to be loaded
			# this only depends on the parameters of the coarse grained optimization, thus the values of the fine grained optimization can be very
			# cheaply changed. 

			simulation_input = {
			'MologramType':self.simulation_name,
			'Epsilon_D_coarse':self.epsilon_D_coarse,
			'Epsilon_h_coarse':self.epsilon_h_coarse,
			'Epsilon_Lambda_coarse':self.epsilon_Lambda_coarse,
			'Etch_Angle':self.Etch_Angle
			}

			condition = self.formatSimulationInputAsSQL(simulation_input)
			data_entry = WL.loadDB('data/phase_mask.db','RobustMasks',selection=selection,condition=condition)
		else:

			selection = 'Intensity_Ratio_fine'
			condition = 'WHERE MologramType="' + self.simulation_name + '"'
			data_entry = WL.loadDB('data/phase_mask.db','OptimalMasks',selection=selection,condition=condition)

		if type(data_entry) is not list:	
			self.intensity_ratio_fine_optimization = data_entry
			loaddata = True
		else:
			loaddata = False

		for k,Lambda in enumerate(Lambda_space):
			
			#print Lambda

			D = D_start[k]
			D_space = RoundToN(np.arange(D - 0.1,D + 0.11,0.02),2)     # - 0.1 +0.11, 02
			# since the D space is different for every grating period, one needs to store it for
			# later access, once the optimal Dutycycle for every phase mask period has been determined.

			# if the dutycycles are not contained in the coarse optimization domain, then replace all occurences outside with the edge values.
			#-----------------------------------------------------------------------------------------------
			#-----------------------------------------------------------------------------------------------
			# to do: actually for the robustness calculation they should both evaluated around the boundaries. I currently have serious edge effects in the robust calculation.
			D_space[self.D_space_coarse[0] >= D_space] = self.D_space_coarse[0]
			D_space[self.D_space_coarse[-1] <= D_space] = self.D_space_coarse[-1]
			

			D_spaces[k,:] = D_space

			for i,D in enumerate(D_space):
				
				for j,h in enumerate(h_space):
					# necessary since  D is different for every lambda
					if loaddata == False:
						self.intensity_ratio_fine_optimization[i,j,k] = self.performIndividualSimulation(D,h,Lambda,Etch_Angle=self.Etch_Angle)[4]
		
		# save the parameters of this optimization to a database, such if the same optimization is to be run again, they can easily be fetched. 
		if loaddata == False:
			if robust == True:
				params = {
					'MologramType':self.simulation_name,
					'Intensity_Ratio_fine':self.intensity_ratio_fine_optimization,
					'Epsilon_D_coarse':self.epsilon_D_coarse,
					'Epsilon_h_coarse':self.epsilon_h_coarse,
					'Epsilon_Lambda_coarse':self.epsilon_Lambda_coarse,
					'D_space_coarse':self.D_space_coarse,
					'h_space_coarse':self.h_space_coarse,
					'Lambda_space_coarse':self.Lambdas_space_coarse,
					'Etch_Angle':self.Etch_Angle
				}

				WL.saveDB('data/phase_mask.db','RobustMasks',params)
			else:
				params = {
					'MologramType':self.simulation_name,
					'Intensity_ratio_fine':self.intensity_ratio_fine_optimization,
					'D_space_coarse':self.D_space_coarse,
					'h_space_coarse':self.h_space_coarse,
					'Lambda_space_coarse':self.Lambdas_space_coarse,
					'Etch_Angle':self.Etch_Angle
				}

				WL.saveDB('data/phase_mask.db','OptimalMasks',params)
	
		if robust == True:
			h_optimal, h_index_optimal,intensity_ratio = self.findOptimalHeightRobustRegularization(
				Lambda_space,D_space,h_space,self.intensity_ratio_fine_optimization,self.epsilon_D_fine,self.epsilon_h_fine,self.epsilon_Lambda_fine)
		else:
			h_optimal, h_index_optimal = self.findOptimalHeight(
				Lambda_space,D_space,h_space,self.intensity_ratio_fine_optimization)
			intensity_ratio = self.intensity_ratio_fine_optimization
		# get the value of the dutycycles at the optimal height

		D_index_optimal = np.ones(len(Lambda_space))
		D_optimal = np.ones(len(Lambda_space))
		Intensity_Ratio_optimal = np.ones(len(Lambda_space))

		for k,Lambda in enumerate(Lambda_space):
			D_index_optimal[k] = np.argmax(intensity_ratio[:,h_index_optimal,k])
			D_optimal[k] = D_spaces[k,D_index_optimal[k]]
			Intensity_Ratio_optimal[k] = self.intensity_ratio_fine_optimization[D_index_optimal[k],h_index_optimal,k]

		# determine the optimal parameters and plot the area around the minimum.

		if saveplots == True:

			plt.figure()
			plt.plot(Lambda_space*1e9,Intensity_Ratio_optimal)
			
			for i,j in enumerate(D_space):

				plt.plot(Lambda_space*1e9,self.intensity_ratio_fine_optimization[i,h_index_optimal,:],'o',mew=0,color='b',alpha=0.01)
			
			plt.xlim([Lambda_space[-1]*1e9,Lambda_space[0]*1e9])
			plt.xlabel(r'$\Lambda [nm]$')
			plt.ylabel(r'$C_I$')
			if robust == True:
				plt.savefig('Robust_vs_all_other_designs' + self.simulation_name + self.epsilon_name + '.png',format='png',dpi=600)
			else:
				plt.savefig('Optimal_vs_all_other_designs' + self.simulation_name + '.png',format='png',dpi=600)


		#print D_optimal

		print h_optimal

		return D_optimal,h_optimal,Intensity_Ratio_optimal

	def findOptimalHeight(self,Lambda_space,D_space,h_space,intensity_ratio):
		"""Finds the optimal height (phase shift layer thickness of the phase mask for a given intensity_ratio matrix.
		Each Period is weighted with its linewidth in the molographic structure. 
		"""

		Optimal_ratio_for_given_h = np.ones(len(h_space))

		for j,h in enumerate(h_space):
			Optimal_ratio_for_given_h[j] = 0
			# for each period optimize the dutycycle for the given height and take this ratio, here it needs
			# to be weighted with the weighting function.  
			for k,Lambda in enumerate(Lambda_space):
				Optimal_ratio_for_given_h[j] += np.amax(intensity_ratio[:,j,k])*self.weight_Lambdas[k]
		
		# calculate the optimal height to do the fine grained optimization.
		h_index = np.argmax(Optimal_ratio_for_given_h)

		h_optimal = h_space[h_index]


		return h_optimal,h_index

	def findOptimalHeightRobustRegularization(self,Lambda_space,D_space,h_space,intensity_ratio,epsilon_D_index,epsilon_h_index,epsilon_Lambda_index,saveplots=False):
		"""
		Finds the optimal height of the phase mask with Robust Regularization. 
		:param epsilon_D_index: is the size of the regularization neighborhood of the Dutycycle. It is in index
		:param epsilon_h_index: is the size of the regularization neighborhood of the height, it is an index
		:param epsilon_Lambda_index: is the size of the regularization neighborhood of the grating period, it is an index. 
		:cite: `Beyer2007-xd`
		
		"""
		print "Perform Robust Regularization:"

		# the variation in h is +- 2 nm
		# the variation in D is assumed to be +- 0.05
		# the variation in Grating constant is assumed to be 5 nm.

		intensity_ratio_robust = np.ones((len(D_space),len(h_space),len(Lambda_space)))

		for k,Lambda in enumerate(Lambda_space):

			# since the Lambda spacing is not equidistant, we need to calculate the regularization radius for every grating period individually. This is not wise to
			# do since the averaging will be performed over different numbers of grating lines. 
			# epsilon_Lambda_lower_index = np.where(Lambda_space > (Lambda + epsilon_Lambda))[0]

			# if epsilon_Lambda_lower_index.size == 0:
			# 	kk = 0
			# else:
			# 	kk = epsilon_Lambda_lower_index[-1]
				
			
			# epsilon_Lambda_upper_index = np.where(Lambda_space < (Lambda - epsilon_Lambda))[0]

			# if epsilon_Lambda_upper_index.size == 0:
			# 	kkk = len(Lambda_space)-1
			# else:
			# 	kkk = epsilon_Lambda_upper_index[0]

			if (k - epsilon_Lambda_index) < 0:
						kk = 0
			else:
				kk = k - epsilon_Lambda_index
			if (k + epsilon_Lambda_index) >= len(Lambda_space):
				kkk = len(Lambda_space)-1
			else:
				kkk = k + epsilon_Lambda_index
			
			for i,D in enumerate(D_space):

				if (i - epsilon_D_index) < 0:
						ii = 0
				else:
					ii = i - epsilon_D_index
				if (i + epsilon_D_index) >= len(D_space):
					iii = len(D_space)-1
				else:
					iii = i + epsilon_D_index
				
				for j,h in enumerate(h_space):
					
					# account for the edge effects of the D,h matrix. 

					if (j - epsilon_h_index) < 0:
						jj = 0
					else:
						jj = j - epsilon_h_index
					if (j + epsilon_h_index) >= len(h_space):
						jjj = len(h_space)-1
					else:
						jjj = j + epsilon_h_index
					
					intensity_ratio_robust[i,j,k] = np.amin(np.amin(np.amin(intensity_ratio[ii:iii,jj:jjj,kk:kkk])))
		
		#-----------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------
		# to do, make this plot useful or delete.
		if saveplots == True:
			plt.figure()
			plotTwoParameterOptimization(D_space,h_space,np.transpose(np.sum(intensity_ratio_robust,axis=2))/len(Lambda_space))
			plt.savefig('Optimal_robust.png',format='png')

		h_robust, h_index_robust = self.findOptimalHeight(Lambda_space,D_space,h_space,intensity_ratio_robust)
		print h_robust

		return h_robust,h_index_robust,intensity_ratio_robust

	def plotDutycycles(self,robust = False,figsizex = 2,figsizey = 2):
		"""This function plots the dutycycle vector."""

		# this should be the robustness plot around the minimum. 
		formatOpticsLetters(figsizex,figsizey)
		plt.figure()
		#plotTwoParameterOptimization(self.D_space,self.h_space,np.transpose(np.sum(self.intensity_ratio_coarse_optimization,axis=2))/len(self.Lambdas_space))
		if robust == True:
			plt.plot(self.Lambdas*1e9,self.D_robust)
		else:
			plt.plot(self.Lambdas*1e9,self.D_optimal)

		plt.plot(self.Lambdas*1e9,self.weight_Lambdas*100+0.2)
		plt.xlabel(r'$\Lambda [nm]$')
		plt.ylabel(r'D')
		plt.xlim([self.Lambdas[-1]*1e9,self.Lambdas[0]*1e9])
		if robust == True:
			plt.savefig('Robust_D_' + self.simulation_name + self.epsilon_name + '.png',format='png',dpi=600)
		else:
			plt.savefig('Optimal_D_' + self.simulation_name  + '.png',format='png',dpi=600)

	def applyGaussianFilterToDutyCycles(self,stdKernel = 10,robust = True, optimal = True):
		"""Applies a Gaussian filter to the Dutycycle vector, depending on the 
		settings onto the robust, optimal or initial dutycycle vector. This is in order to smooth the result from 
		the optimization. """

		if robust == True:
			self.D_robust = gaussian_filter(self.D_robust,stdKernel)
		if optimal == True:
			self.D_optimal = gaussian_filter(self.D_optimal,stdKernel)
		if optimal == False and robust == False:
			self.Dutycycles = gaussian_filter(self.Dutycycles,stdKernel)

	def simulateMaskVariations(self, D_variation=0.05,h_variation=2e-9,Etch_Angle_Variations = 0, saveplots = True,filename='',robust = True,optimal=False):
		"""simulates variations in the dutycycle and height parameters around the design point.

		:params  D_variation: Absolute value of the variations in the dutycycle
		:params h_variation: Absolute value of the variations in the phase shift layer thickness [m]
		:params Etch Angle variation: variations in the etch angle. (Etch Angle variations of +- 2 deg)

		:return : returns either a matrix intensities and weighted intensities for Dutycycle variations vs height variations if no Etch_Angle Variations are specified or a matrix of 
			dutycycles vs Etch angle variations. 
		"""
		if robust == True:
			h = self.h_robust
			Dutycycles = self.D_robust
		elif optimal == True:
			h = self.h_optimal
			Dutycycles = self.D_optimal
		else:
			h = self.h
			Dutycycles = self.Dutycycles
		Etch_Angle = self.Etch_Angle

		perm_D = [-1,-1,1,1]
		perm_h = [-1,1,-1,1]

		D_span = np.arange(-D_variation,+D_variation+0.005,0.01)
		h_span = np.arange(h-h_variation,h+h_variation+1e-10,2e-9)
		
		
		if Etch_Angle_Variations != 0:
			Etch_span = np.arange(-Etch_Angle_Variations+Etch_Angle,Etch_Angle+Etch_Angle_Variations+0.1,1)
			
		selection = 'wICs'

		simulation_input = {
			'D':np.array_str(Dutycycles)[1:-1],
			'h':'{:3.0f}'.format(h*1e9),
			'MologramType':self.simulation_name,
			'EtchAngle':Etch_Angle,
			'EtchAngleVariations':Etch_Angle_Variations
			}

		condition = self.formatSimulationInputAsSQL(simulation_input)
		data_entry = WL.loadDB('data/phase_mask.db','MaskRobustNessEvaluation',selection=selection,condition=condition)



		if type(data_entry) is not list:
			wICs = data_entry
			selection = 'ICs'
			
			simulation_input = {
				'D':np.array_str(Dutycycles)[1:-1],
				'h':'{:3.0f}'.format(h*1e9),
				'MologramType':self.simulation_name,
				'EtchAngleVariations':Etch_Angle_Variations,
				'EtchAngle':Etch_Angle
				}

			condition = self.formatSimulationInputAsSQL(simulation_input)

			data_entry = WL.loadDB('data/phase_mask.db','MaskRobustNessEvaluation',selection=selection,condition=condition)

			ICs = data_entry
			
			loaddata = True
		else:
			loaddata = False
			
			if Etch_Angle_Variations == 0:
				wICs = np.zeros((len(D_span),len(h_span)))
				ICs = np.zeros((len(D_span),len(h_span),len(self.Lambdas)))
				
				for i,D_Variations in enumerate(D_span):
					print D_Variations
					for j,height in enumerate(h_span):
						IC,wIC = self.simulateEntirePhaseMask(Dutycycles+D_Variations,height,Etch_Angle=Etch_Angle)
						wICs[i,j] = wIC
						ICs[i,j,:] = IC

			else:
				Etch_span = np.arange(-Etch_Angle_Variations+Etch_Angle,Etch_Angle+Etch_Angle_Variations+0.1,1)
				D_span = np.arange(-D_variation,+D_variation+0.005,0.05)
				wICs = np.zeros((len(D_span),len(Etch_span)))
				ICs = np.zeros((len(D_span),len(self.Lambdas),len(Etch_span)))
				

				for i,D_Variations in enumerate(D_span):
					print D_Variations

					for k,EtchAngle in enumerate(Etch_span):
						#D_variations_random = np.random.rand((len(Dutycycles)))*2*D_variation-D_variation
						# print Dutycycles + D_variations_random
						# print j
						IC,wIC = self.simulateEntirePhaseMask(Dutycycles+D_Variations,h,Etch_Angle=EtchAngle)
						wICs[i,k] = wIC
						ICs[i,:,k] = IC

		
		if loaddata == False:
			
			params = {
				'h':'{:3.0f}'.format(h*1e9),
				'D':np.array_str(Dutycycles)[1:-1],
				'EtchAngle':Etch_Angle,
				'EtchAngleVariations':Etch_Angle_Variations,
				'MologramType':self.simulation_name,
				'ICs':ICs,
				'wICs':wICs
			}

			WL.saveDB('data/phase_mask.db','MaskRobustNessEvaluation',params)

		#-----------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------
		# This still has to be adapted for the edge angle variations. 
		if saveplots:


			if Etch_Angle_Variations == 0:
				plt.figure()
				
				for i in range(len(h_span)):
					plt.plot(D_span,wICs[:,i],color='k',alpha=0.2+0.07*i)
					plt.xlabel(r'$\Delta D$')
					plt.xlim(-0.05,0.05)
				
				plt.ylabel(r'$C_I(molo)$')
				
				ax = plt.gca()
				ax.yaxis.set_major_locator(ticker.MultipleLocator(0.05))
				#plt.ylim(np.min(wICs)-0.02,np.max(wICs)+0.02)
				plt.ylim(0.7,1)

				if robust:
					plt.savefig('Sensitivity D_h' + self.simulation_name + self.epsilon_name + filename +'.png',dpi=600)
				else:
					plt.savefig('Sensitivity D_h' + self.simulation_name + filename + '.png',dpi=600)
				plt.figure()

				for i in range(len(D_span)):

					plt.plot(self.Lambdas*1e9,np.transpose(ICs[i,:,:]),color='#332288',alpha=0.1)
				ax = plt.gca()
				ax.yaxis.set_major_locator(ticker.MultipleLocator(0.2))

				plt.xlabel(r'$\Lambda$ [nm]')
				plt.ylabel(r'$C_I$')
				plt.xlim(self.Lambdas[-1]*1e9,self.Lambdas[0]*1e9)  

				if robust:
					plt.savefig('Total_C_I' + self.simulation_name + self.epsilon_name + filename + '.png',dpi=600)
				else:
					plt.savefig('Total_C_I' + self.simulation_name + filename + '.png',dpi=600)
			else:
				plt.figure()
				
				for k in range(len(D_span)):
					plt.plot(Etch_span,wICs[k,:],color='k',alpha=0.2+0.07*k)
					print wICs[k,:]
					plt.xlabel(r'$\Delta D$')
					plt.xlim(Etch_span[0],Etch_span[-1])
				
				plt.ylabel(r'$C_I(molo)$')
				
				ax = plt.gca()
				ax.yaxis.set_major_locator(ticker.MultipleLocator(0.05))
				#plt.ylim(np.min(wICs)-0.02,np.max(wICs)+0.02)
				plt.ylim(0.7,1)

				if robust:
					plt.savefig('Sensitivity D_Etch' + self.simulation_name + self.epsilon_name + filename +'.png',dpi=600)
				else:
					plt.savefig('Sensitivity D_Etch' + self.simulation_name + filename + '.png',dpi=600)
				plt.figure()

				for i in range(len(D_span)):
					for k in range(len(Etch_span)):
						print ICs.shape
						print self.Lambdas.shape
						plt.plot(self.Lambdas*1e9,np.transpose(ICs[i,:,k]),color='#332288',alpha=0.1)
				
				ax = plt.gca()
				ax.yaxis.set_major_locator(ticker.MultipleLocator(0.2))

				plt.xlabel(r'$\Lambda$ [nm]')
				plt.ylabel(r'$C_I$')
				plt.xlim(self.Lambdas[-1]*1e9,self.Lambdas[0]*1e9)  

				if robust:
					plt.savefig('Total_C_I_Etch' + self.simulation_name + self.epsilon_name + filename + '.png',dpi=600)
				else:
					plt.savefig('Total_C_I_Etch' + self.simulation_name + filename + '.png',dpi=600)


		return ICs,wICs


	def simulateEntirePhaseMask(self,Dutycycles = 0, h = 0,robust = False, optimal = False, Etch_Angle=0,save=False):
			"""This function calculates the ratio of a given phase mask (defined Dutycycles, defined Periods, defined height of the
			phase shift layer and weights for the different grating periods. 
			
			:param Dutycycles: numpy array of dutycycles, if not specified then default value that was used to initialize the PhaseMaskCalculator object is used
			:param h: float specifing the phase shift layer thickness [m], if empty the default value that was used to initialize the PhaseMaskCalculator object is used
			:param robust: if True the robust dutycycles and height are used for the simulation. (optimal and robust cannot be True at the same time)
			:param optimal: if True the optimal dutycycles and height are used for the simulation.
			"""

			if type(Dutycycles) == int:
				Dutycycles = self.Dutycycles
			if h == 0:
				h = self.h
			if optimal:
				Dutycycles = self.D_optimal
				h = self.h_optimal
			if robust: 
				Dutycycles = self.D_robust
				h = self.h_robust

			self.Etch_Angle = Etch_Angle

			selection = 'IC'

			simulation_input = {
				'MologramType':self.simulation_name,
				'Etch_Angle':Etch_Angle,
				'D':np.array_str(Dutycycles)[1:-1],
				'h':'{:3.0f}'.format(h*1e9)
			}

			condition = self.formatSimulationInputAsSQL(simulation_input)
			data_entry = WL.loadDB('data/phase_mask.db','EntirephasemaskSimulated',selection=selection,condition=condition)

			if type(data_entry) is not list:	
				PM_efficiency = data_entry
				
			selection = 'wIC'
			data_entry = WL.loadDB('data/phase_mask.db','EntirephasemaskSimulated',selection=selection,condition=condition)
			
			if type(data_entry) is not list:	
				wIC = data_entry
			else:

				PM_efficiency = np.ones(len(self.Lambdas))
				Diff_order0 = np.ones(len(self.Lambdas))
				Diff_order1 = np.ones(len(self.Lambdas))
				Diff_order2 = np.ones(len(self.Lambdas))
				Diff_order3 = np.ones(len(self.Lambdas))

				weighted_PM_efficiency = np.ones(len(self.Lambdas))

				for i,j in enumerate(self.Lambdas):
					if i == 0:
						Diff_order0[i], Diff_order1[i], Diff_order2[i], Diff_order3[i], PM_efficiency[i] = self.performIndividualSimulation(Dutycycles[i],h,self.Lambdas[i],Etch_Angle=Etch_Angle,plotStructure=True)
					else:
						Diff_order0[i], Diff_order1[i], Diff_order2[i], Diff_order3[i], PM_efficiency[i] = self.performIndividualSimulation(Dutycycles[i],h,self.Lambdas[i],Etch_Angle=Etch_Angle,plotStructure=False)
					weighted_PM_efficiency[i] = PM_efficiency[i]*self.weight_Lambdas[i]
					wIC = np.sum(weighted_PM_efficiency)

				if save:
					params = {
						'MologramType':self.simulation_name,
						'IC':PM_efficiency,
						'wIC':wIC,
						'Etch_Angle':Etch_Angle,
						'D':np.array_str(Dutycycles)[1:-1],
						'h':'{:3.0f}'.format(h*1e9),
						'DiffractionOrder0':Diff_order0,
						'DiffractionOrder1':Diff_order1,
						'DiffractionOrder2':Diff_order2,
						'DiffractionOrder3':Diff_order3,
					}
					
					WL.saveDB('data/phase_mask.db','EntirephasemaskSimulated',params)
			
			sum_diff_orders_other_than_first = Diff_order0 + Diff_order2 + Diff_order3
			first_order = Diff_order1
			return PM_efficiency, wIC, sum_diff_orders_other_than_first, first_order

	def estimatePMThickness(self):
		"""Estimates the starting value of the phase mask thickness optimization, according to:

		.. math:: h = {\\lambda  \\over {2\\left( {{n_p} - {n_i}} \\right)}}"""

		return self.Ill_wl/(2.*(self.n_p-self.n_i))

	def createPMDatabase(self):
		"""creates a sqlite Database object if it does not already exists, where all the simulation results can be stored. 
		Input parameter for the simulation are: polarization, height, incidence angle, Period, Dutycycle, n_i, n_p and Illumination wavelength
		Stored output parameters is the relative intensity in the 0,1,2 and 3 diffraction order """

		params = {
			'Polarization':'text',
			'IncidenceAngle':'real',
			'height':'real',
			'Period':'real',
			'Dutycycle':'real',
			'n_i':'real',
			'n_p':'real',
			'n_s':'real',
			'IlluminationWavelength':'real',
			'DiffractionOrder0':'real',
			'DiffractionOrder1':'real',
			'DiffractionOrder2':'real',
			'DiffractionOrder3':'real',
			'FourierComponents':'real',
			'IntensityRatio':'real'
		}
		
		WL.createDB('data/phase_mask.db',self.table_name,params)


		params = {
			'MologramType':'text',
			'IC':'array',
			'wIC':'real',
			'Etch_Angle':'real',
			'D':'text',
			'h':'text'
		}
		# stores the values for the simulatedMasks Variations.
		WL.createDB('data/phase_mask.db','EntirephasemaskSimulated',params)


		params = {
			'MologramType':'text',
			'Intensity_Ratio_coarse':'array',
			'D_space_coarse':'array',
			'h_space_coarse':'array',
			'Lambda_space_coarse':'array',
			'Etch_Angle':'real'
		}
		# stores the values of the optimal PhaseMasks
		WL.createDB('data/phase_mask.db','CoarseIntensities',params)

		params = {
			'MologramType':'text',
			'Intensity_Ratio_fine':'array',
			'Epsilon_D_coarse':'real',
			'Epsilon_h_coarse':'real',
			'Epsilon_Lambda_coarse':'real',
			'D_space_coarse':'array',
			'h_space_coarse':'array',
			'Lambda_space_coarse':'array',
			'Etch_Angle':'real'
		}
		#stores the values of the robust PhaseMasks
		WL.createDB('data/phase_mask.db','RobustMasks',params)

		params = {
			'MologramType':'text',
			'Intensity_Ratio_fine':'array',
			'D_space_coarse':'array',
			'h_space_coarse':'array',
			'Lambda_space_coarse':'array',
			'Etch_Angle':'real'
		}
		# stores the values of the optimal PhaseMasks
		WL.createDB('data/phase_mask.db','OptimalMasks',params)

		params = {
			'MologramType':'text',
			'D':'text',
			'h':'text',
			'EtchAngle':'real',
			'ICs':'array',
			'wICs':'array',
			'EtchAngleVariations':'real'
		}
		# stores the values for the simulateMaskVariation
		WL.createDB('data/phase_mask.db','MaskRobustNessEvaluation',params)

		# create indices for faster DB access and retrival this is essential for speedup. 
		conn = sqlite3.connect('data/phase_mask.db', detect_types=sqlite3.PARSE_DECLTYPES)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'Period' + ' ON "' + self.table_name + '" (Period)'
		c = conn.cursor()
		c.execute(sqlx)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'Dutycycle' + ' ON "' + self.table_name + '" (Dutycycle)'
		c = conn.cursor()
		c.execute(sqlx)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'height' + ' ON "' + self.table_name + '" (height)'
		c = conn.cursor()
		c.execute(sqlx)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'FourierComponents' + ' ON "' + self.table_name + '" (FourierComponents)'
		c = conn.cursor()
		c.execute(sqlx)

	def saveSimulationResultToDataBase(self,simulation_input, results):
		"""Saves the result of the simulation to the database."""

		params = simulation_input.copy()
		params.update(results)
		
		WL.saveDB('data/phase_mask.db',self.table_name,params)

	def loadSimulationResultFromDataBase(self,simulation_input):
		"""Loads the diffraction efficiencies"""

		# 
		condition = self.formatSimulationInputAsSQL(simulation_input)
		return WL.loadDB('data/phase_mask.db',self.table_name,selection = 'DiffractionOrder0,DiffractionOrder1,DiffractionOrder2,DiffractionOrder3,IntensityContrast',condition = condition)

		# condition = self.formatSimulationInputAsPDQuery(simulation_input)


		# return self.data.query(condition)

	def formatSimulationInputAsSQL(self,simulation_input):
		"""Formats the dictonary simulation_input in the SQL format"""

		condition = 'WHERE '

		for key,value in simulation_input.iteritems():
			# string have to have the string delimiters for the SQL command. 
			if type(value) == type(str()):
				condition += key + '=\"' + str(value) + '\" AND '
			else:
				condition += key + '=' + str(value) + ' AND '
		condition = condition[:-5]

		return condition



if __name__=="__main__":

	from waveguide.waveguide import SlabWaveguide           # waveguide calculations
	from mologram.generateMask import * 
	from mologram.braggCircle import calcBraggCircle
	from mologram.mologram import Mologram
	
	waveguide = SlabWaveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117, 
		n_c=1.33, d_f=145e-9, polarization='TE', 
		inputPower=1, wavelength=632.8e-9, attenuationConstant=0)
	mologram = Mologram(waveguide, focalPoint=-900e-6, radius=200e-6)
	mologram.braggRadius = calcBraggCircle(mologram)	# add mask to avoid the resonant case (second-order Bragg reflection) in the propagation path of the waveguide mode
	mologram.braggOffset = 25e-6
	mologram.mask = []
	mologram.mask.append(Mask().annulus(mologram.braggRadius-mologram.braggOffset,mologram.braggRadius+mologram.braggOffset, center=(-mologram.braggRadius,0),inverted=False))
	mologram.mask.append(Mask().disk(200e-6,inverted=True))
	
	Lambda_space = mologram.calcLineDistances(N=2)
	lineLengths = mologram.calcLineLengths()
	epsilon_Lambda = 5*1e-9


	weight_Lambda = lineLengths/np.sum(lineLengths)

	n_i = 1.5
	n_p = 2.246
	n_s = 1.5422
	Ill_wl = 390e-9
	height = 246e-9

	
	#weight_Lambda = np.ones(len(Lambda))
	PM = PhaseMaskCalculator(mologram,n_i,n_p,n_s,Ill_wl,height,Lambda_space,weight_Lambda,'TE',Dutycycles=np.ones(len(Lambda_space))*0.5)

	#D_optimal,h_optimal,Intensity_Ratio_optimal,weighted_Ratio = PM.optimizePhaseMask()


	# I need to save the optimal phase mask parameters to a database, after optimization.

	# PM.saveSimulationResultToDataBase()
	#PM.performIndividualSimulation(0.5,246e-9,590e-9)

	#IC, wIC = PM.simulateEntirePhaseMask()
	
	#print plt.plot(IC)

	#PM = PhaseMaskCalculator(mologram,1.5,2.246,1.5422,390e-9,Lambda,weight_Lambda,'TE',height,Dutycycles,NN=30)

	height = 246e-9

	# PM = PhaseMaskCalculator(mologram,1.5,2.246,1.5422,390e-9,Lambda,weight_Lambda,'TM',height,Dutycycles)
		
	# IC_TM, wIC_TM = PM.simulateEntirePhaseMask()
	
	# print plt.plot(IC_TM)

	# PM = PhaseMaskCalculator(mologram,1.5,2.246,1.5422,390e-9,Lambda,weight_Lambda,'TE' ,height,Dutycycles)

	print PM.calcCutoffGratingPeriod(1)
	
	
	# PM.saveSimulationResultToDataBase()
	#PM.performIndividualSimulation(0.5,246e-9,590e-9)

	#IC, wIC = PM.simulateEntirePhaseMask()
	
	#print plt.plot(IC)

	#PM = PhaseMaskCalculator(mologram,1.5,2.246,1.5422,390e-9,Lambda,weight_Lambda,'TE',height,Dutycycles,NN=30)
	
	IC_TE, wIC_TE = PM.simulateEntirePhaseMask(Etch_Angle=6.82)
	
	plt.plot(IC_TE)
	#plt.plot((IC_TE+IC_TM)/2)
	plt.show()


		# refractive indices for Ta2O5
	# 390 nm = 2.246
	# 405 nm = 2.231
	# 450 nm = 2.190
	# 457 nm = 2.186
	# 488 nm = 2.167
	# 514 nm = 2.155
	# 532 nm = 2.147
	# 632.8 nm = 2.118

	# refractive indices for D263
	# 390 nm = 1.542
	# 405 nm = 1.540
	# 450 nm = 1.534
	# 457 nm = 1.533
	# 488 nm = 1.530
	# 514 nm = 1.528
	# 532 nm = 1.527
	# 632.8 nm = 1.521

	# refractive indices for DMSO
	# 390 nm = 1.500
	# 405 nm = 1.497

	# refractive indices for water
	# 390 nm = 1.340
	# 405 nm = 1.339
	# 450 nm = 1.337
	# 457 nm = 1.337
	# 488 nm = 1.336
	# 514 nm = 1.335
	# 532 nm = 1.334
	# 632.8 nm = 1.332




