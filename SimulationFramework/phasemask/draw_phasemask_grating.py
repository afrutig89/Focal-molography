"""
:author: Pierre-Louis Ronat
:created: 13.11.2017

"""
import sys
sys.path.append("../")
import os
from gdsCAD_local import *
from gdsCAD_local.boolean_AF import *
import os
import numpy as np

class Grating():
    """This is a class to draw the gratings in gdsCAD. All units are in um.
        :params sizeG: (w,h) the width and height of the grating
        :params posG: (x,y) the x y position of the first element of the grating at the bottom left
        :params period:
    """
    def __init__(self, sizeG, posG, period):
        self.sizeG = sizeG
        self.posG = posG
        self.period = period

    def drawGrating(self,obj):
        """This is a class to draw the gratings in gdsCAD. All units are in um.
                :params obj: obj must be an core.elements object
         """

        grating = core.Elements()
        rect_sizeG = (self.period/2,self.sizeG[1])
        rect = shapes.Rectangle((0,0), rect_sizeG, layer=2)

        N = int(self.sizeG[0]/self.period)
        for i in range(N):
            d = (i*self.period,0)
            grating.add(utils.translate(rect, d))

        obj.add(utils.translate(grating,self.posG))


class Cross(Grating):
    """This is a class to draw the alignment crosses with inner grating in gdsCAD. All units are in um.
            :params sizeC: (w,h) the width and height of the cross
            :params posC: (x,y) the x y position of the center of the cross
            :params t: thickness of the cross
            :params period: period of the grating within the cross
    """
    def __init__(self, sizeC, posC, t, period):
        # Grating.__init__(self,sizeG = sizeG,posG = posG,period)
        # self.grating = grating

        self.sizeC = sizeC
        self.posC = posC
        self.t = t
        self.period = period

    def drawCross(self,obj):
        #Draw high block
        gratingh = Grating(sizeG = (self.t,self.sizeC[1]), posG = (self.posC[0]-self.t/2,self.posC[1]-self.sizeC[1]/2),period = self.period)
        gratingh.drawGrating(obj)

        #Draw left block
        gratingl = Grating(sizeG = (self.sizeC[0]/2-self.t/2,self.t), posG = (self.posC[0]-self.sizeC[0]/2+self.period/2,self.posC[1]-self.t/2),period = self.period)
        gratingl.drawGrating(obj)
        #Draw right block
        gratingr = Grating(sizeG = (self.sizeC[0]/2-self.t/2,self.t), posG = (self.posC[0]+self.t/2,self.posC[1]-self.t/2),period = self.period)
        gratingr.drawGrating(obj)



if __name__ == '__main__':
    mask = core.Elements()

    # Base (first layer)
    base = core.Elements()

    base_sizeG = (7000,6000)
    ground = shapes.Rectangle((0,0), base_sizeG, layer=1)
    base.add(ground)
    mask.add(base)

    # Coupling Grating (second layer)
    grating = Grating(sizeG = (500,2800), posG = (2250,1600),period = 0.4)
    grating.drawGrating(mask)

    # Cross (second layer)
    cross1 = Cross(sizeC = (600,400), posC = (5000,1400), t = 20, period = 0.8)
    cross1.drawCross(mask)
    cross2 = Cross(sizeC = (600,400), posC = (5000,4600), t = 20, period = 0.8)
    cross2.drawCross(mask)

    # Printing + saving
    layout = core.Layout('LIBRARY',unit=1e-06, precision=1e-09)
    cell = core.Cell('TOP')
    cell.add(mask)
    layout.add(cell)
    layout.save('GratingMask.GDS')
    OpenInKlayout('GratingMask.GDS')

