"""
:author: Andreas Frutiger
:created: 03.01.2017

"""
from mrcwa import *
import numpy as np


class Grating(object):
	"""This is a class for the generic buildup of the unit cell of the grating function. Please use the syntax Grating().rectangular
		to create the corresponding shapes."""
	def __init__(self):

		a = 1

	def rectangle(self,D,h,Lambda,n_i,n_p,n_s):
		"""Creates a rectangular grating profile

			:param n_i: refractive index of the immersion medium
			:param n_s: refractive index of the substrate
			:param n_f: refractive index of the phase shift layer

		"""

		# The incident and transmission halfspaces are represented as slices with
		# only one step that extends over the full period. The thickness of these slices
		# may be set to an arbitrary number

		# Material instances are created in this way.
		self.n_i  = material(n_i,'#86CEB7')
		self.n_p = material(n_p,'#DDCC77')
		self.n_s = material(n_s,'#95CCED')

		incident	 = slice(1.0,[[1,self.n_s]])
		binary       = slice(h/Lambda,[[(1-D)/2.0,self.n_i],[D+(1-D)/2.0,self.n_p],[1,self.n_i]])
		transmission = slice(1.0,[[1,self.n_i]])

		self.grating = stack( [incident, binary, transmission] )
		# The grating slices are passed in a list, starting from the incident slice
		# and ending at the transmission slice

		return self.grating

	def trapezoidal(self,D,h,Lambda,n_i,n_p,n_s,Etch_Angle):
		"""Creates a trapezoidal grating profile (Approximated through 10 rectangular multilayers, since the method was only shown to be robust up to 16 layers in the
		publication)

		.. figure:: figures//trapeziodal_definitions.png
			:width: 100 %
			
		:param D: Average Dutycycle
		:param D_lower: Dutycycle at the top of the ridge
		:param h: thickness of the phase shift layer [m]
		:param Lambda: Grating Period [m]
		:param n_i: refractive index of the immersion medium
		:param n_s: refractive index of the substrate
		:param n_f: refractive index of the phase shift layer

		"""
		# Material instances are created in this way.
		self.n_i  = material(n_i,'#86CEB7')
		self.n_p = material(n_p,'#DDCC77')
		self.n_s = material(n_s,'#95CCED')

		layers = []
		incident	 = slice(1.0,[[1,self.n_s]])
		layers.append(incident)

		N = 10

		# here the Etch angle is recalculated into a D_lower and a D_upper. 
		addiditional_ridge_width = np.tan(Etch_Angle/180.0*np.pi)*h
		#print addiditional_ridge_width

		D_lower = (D*Lambda - addiditional_ridge_width)/Lambda
		#print D_lower
		D_upper = (D*Lambda + addiditional_ridge_width)/Lambda
		#print D_upper

		for i in range(N):
			D_eff = D_upper - float(i)/N*(D_upper-D_lower)
			layers.append(slice(h/Lambda/float(N),[[(1-D_eff)/2.,self.n_i],[(1-D_eff)/2. + D_eff,self.n_p],[1,self.n_i]]))

		transmission = slice(1.0,[[1,self.n_i]])
		layers.append(transmission)

		self.grating = stack(layers)


		return self.grating

if __name__=="__main__":

	grating = Grating().trapezoidal(0.45,246e-9,840e-9,1.5,2.246,1.5422,6.82)
	grating = Grating().rectangle(0.45,246e-9,840e-9,1.5,2.246,1.5422)

	Ill_wl = 390e-9
	Lambda = 590e-9
	Pol = 'TE'
	Inc_Ang = 0.0
	NN = 10

	grating.l  = Ill_wl/float(Lambda)
	grating.pol    = Pol
	grating.theta = Inc_Ang 

	# The polarization can be either 'TE' or 'TM'

	# The number of positive and negative diffraction orders retained
	# in the computation is determined like this
	# DO NOT CHANGE, THIS IS IMPORTANT FOR CONVERGENCE AND ALSO FOR THE EXTRACTION OF THE RIGHT NUMERICAL VALUES AFTERWARDS, default: NN =100

	grating.order = (-NN, NN)


	# An fake color image of the grating is saved for visalization
	#grating.paint().save('basic-example.png')

	# The calculation is executed by calling the compute method
	T = grating.computeTransmittedOnly()


