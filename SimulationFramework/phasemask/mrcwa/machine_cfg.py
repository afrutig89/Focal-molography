# intel ifort 32 bit and intel mkl accordingly
# f2py_options  = ['--fcompiler=intel',
#                  '--f90exec=/opt/intel/fc/10.0.026/bin/ifort',
#                  '--opt="-O3 -xW -ipo"',
#                  '--noarch']

# libs          = ['mkl_lapack', 'mkl_ia32']
# libpaths      = ['/opt/intel/mkl/9.1.023/lib/32']

#intel ifort x86_64 and intel mkl em64t accordingly
# f2py_options  = ['--fcompiler=intelem',
#                 '--f90exec=/opt/intel/fce/9.1.032/bin/ifort']

# libs          = ['mkl_lapack','mkl','guide','pthread']
# libpaths      = ['/opt/intel/mkl/9.1.023/lib/em64t']

f2py_options  = ['--fcompiler=gfortran',
                '--f90exec=/usr/local/bin/gfortran']

libs          = ['openblas']  
libpaths	= ['/usr/local/opt/openblas/lib']


dll_suffix='.so'

# How to correctly setup
# install the current gfortran compiler from the website: https://www.webmo.net/support/fortran_osx.html
# locate the path of your gfortran compiler
# install openblas (laplack) on your system with homebrew 'brew install openblas'
# link to openblas

