SUBROUTINE wavevec(theta, epsr, epst, qq, nm, np, q, kr, kt)
IMPLICIT NONE

INTEGER, INTENT(IN) :: nm, np
REAL(4), INTENT(IN) :: theta, epsr, qq
COMPLEX(4), INTENT(IN) :: epst

REAL(4), INTENT(OUT) :: q(nm:np)
COMPLEX(4), INTENT(OUT) :: kr(nm:np), kt(nm:np)

INTEGER :: i
REAL(4) :: r


r = SQRT(epsr)*SIN(theta)

DO i = nm, np
   q(i) = r + i * qq
   kr(i) = SQRT(CMPLX(epsr - q(i)**2,0))
   IF ( AIMAG(kr(i)) .lt. 0.0 ) THEN
      kr(i) = - kr(i)
   END IF
   kt(i) = SQRT(epst - CMPLX(q(i)**2,0))
   IF ( AIMAG(kt(i)) .lt. 0.0 ) THEN
      kt(i) = - kt(i)
   END IF
END DO

END SUBROUTINE wavevec
