SUBROUTINE waveamp(JOB, n, q, kr, kt, l, d, nz, eps, b)
IMPLICIT NONE

CHARACTER(2), INTENT(IN) :: JOB
INTEGER, INTENT(IN) :: n, l, nz
REAL(4), INTENT(IN) :: q(n), d(0:l+1)
COMPLEX(4), INTENT(IN) :: eps(-(n-1):n-1,0:l+1), kr(n), kt(n)
COMPLEX(4), INTENT(OUT) :: b(2*n*(l+1))

INTERFACE
   SUBROUTINE secular(JOB, q, n, eps, d, v, lambda, vl, vx, vlx)
     IMPLICIT NONE
     CHARACTER(2), INTENT(IN) :: JOB
     INTEGER, INTENT(IN) :: n
     REAL(4), INTENT(IN) :: q(n), d
     COMPLEX(4), INTENT(IN) :: eps(-(n-1):(n-1))
     COMPLEX(4), INTENT(OUT) :: v(n,n), lambda(n), vl(n,n), vx(n,n), vlx(n,n)
   END SUBROUTINE secular
END INTERFACE

INTEGER :: i,j,nn
INTEGER :: INFO
INTEGER, ALLOCATABLE, SAVE :: IPIV(:) 
COMPLEX(4), ALLOCATABLE, SAVE :: a(:,:), v(:,:), lambda(:)    &
                                 , vl(:,:), vx(:,:), vlx(:,:)

nn = 2*n*(l+1)

IF ( .NOT. ALLOCATED(a) ) ALLOCATE(a(nn,nn),v(n,n),lambda(n)    &
                                  ,vl(n,n),vx(n,n),vlx(n,n),IPIV(nn))

a = 0

! TE JOB
IF (JOB .eq. 'TE') THEN

   ! rhs
   b = 0
   b(nz) = 1
   b(nz+n) = (0,1) * kr(nz)

   ! incident and transmission region
   DO i = 1, n
      a(i,i) = -1
      a(n+i,i) = (0,1) * kr(i)
      a(2*n*l+i,2*n*l+n+i) = 1
      a(2*n*l+n+i,2*n*l+n+i) = (0,1) * kt(i)
   END DO

! TM JOB
ELSE IF (JOB .eq. 'TM') THEN

   ! rhs
   b = 0
   b(nz) = 1
   b(nz+n) = (0,1) / eps(0,0) * kr(nz)

   ! incident and transmission region
   DO i = 1, n
      a(i,i) = -1
      a(n+i,i) = (0,1) / eps(0,0) * kr(i)
      a(2*n*l+i,2*n*l+n+i) = 1
      a(2*n*l+n+i,2*n*l+n+i) = (0,1) / eps(0,l+1) * kt(i)
   END DO

ELSE
   WRITE(*,*) 'waveamp: ERROR: input JOB invalid'
   STOP
END IF


! layers
DO j = 1, l
   CALL secular(JOB, q, n, eps(:,j), d(j), v, lambda, vl, vx, vlx)
   a( (j-1)*2*n+1:(j-1)*2*n+n , (j-1)*2*n+n+1:j*2*n ) =  v
   a( (j-1)*2*n+n+1:j*2*n     , (j-1)*2*n+n+1:j*2*n ) = -vl
   a( j*2*n+1:j*2*n+n         , (j-1)*2*n+n+1:j*2*n ) = -vx
   a( j*2*n+n+1:(j+1)*2*n     , (j-1)*2*n+n+1:j*2*n ) =  vlx
   a( (j-1)*2*n+1:(j-1)*2*n+n , j*2*n+1:j*2*n+n ) =  vx
   a( (j-1)*2*n+n+1:j*2*n     , j*2*n+1:j*2*n+n ) =  vlx
   a( j*2*n+1:j*2*n+n         , j*2*n+1:j*2*n+n ) = -v
   a( j*2*n+n+1:(j+1)*2*n     , j*2*n+1:j*2*n+n ) = -vl
END DO


! solve 
CALL CGESV(nn,1,a,nn,IPIV,b,nn,INFO)

IF (INFO .ne. 0) THEN
   WRITE(*,*) 'waveamp: ERROR: sgesv failed, info = ', INFO
   STOP
END IF

END SUBROUTINE waveamp
