SUBROUTINE inveps(eps,n,epsinv)
IMPLICIT NONE

INTEGER, INTENT(IN) :: n
COMPLEX(4), INTENT(IN) :: eps(-(n-1):n-1)
COMPLEX(4), INTENT(OUT) :: epsinv(n,n)

INTEGER :: INFO
INTEGER, ALLOCATABLE, SAVE :: IPIV(:)
COMPLEX(4), ALLOCATABLE, SAVE :: a(:,:)

INTEGER :: i, j

IF ( .NOT. ALLOCATED(a) ) ALLOCATE(a(n,n),IPIV(n))

DO i = 1, n
   DO j = 1, n
      a(i,j) = eps(i-j)
   END DO
END DO

epsinv = (0,0)

DO i = 1, n
   epsinv(i,i) = (1,0)
END DO

CALL CGESV(n,n,a,n,IPIV,epsinv,n,INFO)

END SUBROUTINE inveps
