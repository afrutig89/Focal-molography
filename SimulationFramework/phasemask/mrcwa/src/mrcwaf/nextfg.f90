SUBROUTINE nextfg(n, v, vl, vx, vlx, f, g)
IMPLICIT NONE

INTEGER, INTENT(IN) :: n
COMPLEX(4), INTENT(IN) :: v(n,n), vl(n,n), vx(n,n), vlx(n,n)
COMPLEX(4), INTENT(INOUT) :: f(n,n), g(n,n)


INTEGER :: lwork, info
INTEGER, ALLOCATABLE, SAVE :: IPIV(:)
COMPLEX(4), ALLOCATABLE, SAVE :: a(:,:), p(:,:), work(:)

lwork = 64*2*n

IF ( .NOT. ALLOCATED(a) ) ALLOCATE(a(n,n),p(2*n,2*n),IPIV(2*n),work(lwork))

p(1:n,1:n) = -v
p(n+1:2*n,1:n) = -vl
p(1:n,n+1:2*n) = f
p(n+1:2*n,n+1:2*n) = g

! invert p
CALL cgetrf( 2*n, 2*n, p, 2*n, IPIV, info )
CALL cgetri( 2*n, p, 2*n, IPIV, work, lwork, info )

a = MATMUL(p(1:n,1:n),vx)-MATMUL(p(1:n,n+1:2*n),vlx)

f = MATMUL(vx,a)+v
g = MATMUL(vlx,a)-vl

END SUBROUTINE nextfg
