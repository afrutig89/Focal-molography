SUBROUTINE partialt(JOBP, n, q, kr, kt, l, d, nz, eps, c)
IMPLICIT NONE

CHARACTER(2), INTENT(IN) :: JOBP
INTEGER, INTENT(IN) :: n, l, nz
REAL(4), INTENT(IN) :: q(n), d(0:l+1)
COMPLEX(4), INTENT(IN) :: eps(-(n-1):n-1,0:l+1), kr(n), kt(n)
COMPLEX(4), INTENT(OUT) :: c(2*n)

INTERFACE
   SUBROUTINE secular(JOB, q, n, eps, d, v, lambda, vl, vx, vlx)
     IMPLICIT NONE
     CHARACTER(2), INTENT(IN) :: JOB
     INTEGER, INTENT(IN) :: n
     REAL(4), INTENT(IN) :: q(n), d
     COMPLEX(4), INTENT(IN) :: eps(-(n-1):(n-1))
     COMPLEX(4), INTENT(OUT) :: v(n,n), lambda(n), vl(n,n), vx(n,n), vlx(n,n)
   END SUBROUTINE secular
   SUBROUTINE nextfgab(n, v, vl, vx, vlx, f, g, a, b)
     IMPLICIT NONE
     INTEGER, INTENT(IN) :: n
     COMPLEX(4), INTENT(IN) :: v(n,n), vl(n,n), vx(n,n), vlx(n,n)
     COMPLEX(4), INTENT(INOUT) :: f(n,n), g(n,n), a(n), b(n)
   END SUBROUTINE nextfgab
END INTERFACE

INTEGER :: i,j
INTEGER :: INFO
INTEGER, ALLOCATABLE, SAVE :: IPIV(:) 
COMPLEX(4), ALLOCATABLE, SAVE :: M(:,:), v(:,:), lambda(:)    &
                                 , vl(:,:), vx(:,:), vlx(:,:),f(:,:),g(:,:)

IF ( .NOT. ALLOCATED(M) ) ALLOCATE(v(n,n),lambda(n),vl(n,n),vx(n,n),vlx(n,n) &
                                  ,f(n,n),g(n,n),M(2*n,2*n),IPIV(2*n))

f = 0
g = 0
M = 0
c = 0

! TE JOBP
IF (JOBP .eq. 'TE') THEN

   ! a0, b0
   c(nz) = 1
   c(nz+n) = (0,1) * kr(nz)

   ! incident and transmission region
   DO i = 1, n
      f(i,i) = 1
      g(i,i) = -(0,1) * kr(i)
      M(i,i) = 1
      M(n+i,i) = (0,1) * kt(i)
   END DO

! TM JOBP
ELSE IF (JOBP .eq. 'TM') THEN

   ! a0, b0
   c(nz) = 1
   c(nz+n) = (0,1) / eps(0,0) * kr(nz)

   ! incident and transmission region
   DO i = 1, n
      f(i,i) = 1
      g(i,i) = -(0,1) / eps(0,0) * kr(i)
      M(i,i) = 1
      M(n+i,i) = (0,1) / eps(0,l+1) * kt(i)
   END DO

ELSE
   WRITE(*,*) 'partialt: ERROR: input JOBP invalid'
   STOP
END IF


! layers
DO j = 1, l
   CALL secular(JOBP, q, n, eps(:,j), d(j), v, lambda, vl, vx, vlx)
   CALL nextfgab(n, v, vl, vx, vlx, f, g, c(1:n), c(n+1:2*n))
END DO

! init final solve

M(1:n,n+1:2*n) = -f
M(n+1:2*n,n+1:2*n) = -g

! solve 
CALL CGESV(2*n,1,M,2*n,IPIV,c,2*n,INFO)

IF (INFO .ne. 0) THEN
   WRITE(*,*) 'partialt: ERROR: sgesv failed, info = ', INFO
   STOP
END IF

END SUBROUTINE partialt
