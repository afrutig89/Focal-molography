SUBROUTINE fouriercoeff(n,x,f,m,c)
IMPLICIT NONE

INTEGER, INTENT(IN) :: n, m
REAL(4), INTENT(IN) :: x(n)
COMPLEX(4), INTENT(IN) :: f(n)

COMPLEX(4), INTENT(OUT) :: c(-m:m)

REAL(4), PARAMETER :: twopi = 2*3.141592653589793238462643383279502884197

INTEGER :: k
REAL(4) :: T

T = x(n)

DO k = -m, -1
   c(k) = (0,1)/(twopi*k) * (   f(n) - f(1)      &
     + SUM( ( f(1:n-1) - f(2:n) ) * EXP( -(0,1)*twopi*k/T*x(1:n-1) ) )   )
END DO

c(0) = 1/T * (   f(1)*x(1) + SUM( f(2:n) * ( x(2:n) - x(1:n-1) ) )   )

DO k = 1, m
   c(k) = (0,1)/(twopi*k) * (   f(n) - f(1)      &
     + SUM( ( f(1:n-1) - f(2:n) ) * EXP( -(0,1)*twopi*k/T*x(1:n-1) ) )   )
END DO

END SUBROUTINE fouriercoeff
