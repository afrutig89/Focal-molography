SUBROUTINE secular(JOB, q, n, eps, d, v, lambda, vl, vx, vlx)
IMPLICIT NONE

CHARACTER(2), INTENT(IN) :: JOB
INTEGER, INTENT(IN) :: n
REAL(4), INTENT(IN) :: q(n), d
COMPLEX(4), INTENT(IN) :: eps(-(n-1):(n-1))

COMPLEX(4), INTENT(OUT) :: v(n,n), lambda(n), vl(n,n), vx(n,n), vlx(n,n)

INTERFACE
   SUBROUTINE inveps(eps,n,epsinv)
     IMPLICIT NONE
     INTEGER, INTENT(IN) :: n
     COMPLEX(4), INTENT(IN) :: eps(-(n-1):n-1)
     COMPLEX(4), INTENT(OUT) :: epsinv(n,n)
   END SUBROUTINE inveps
END INTERFACE

INTEGER :: i, j
INTEGER :: LWORK, INFO
COMPLEX(4) :: z
COMPLEX(4), ALLOCATABLE, SAVE :: epsmat(:,:), epsinv(:,:), A(:,:)  &
                                 , VLEFT(:,:), WORK(:), RWORK(:)

IF ( .NOT. ALLOCATED(A) ) ALLOCATE(epsmat(n,n), epsinv(n,n), A(n,n)  &
                                 , VLEFT(1,n), WORK(4*n), RWORK(2*n))

LWORK = 4*n


! TE JOB
IF (JOB .eq. 'TE') THEN

   ! eigenvalue problem matrix A
   DO i = 1, n
      DO j = 1, n
         A(i,j) = -eps(i-j) 
      END DO
   END DO

   DO j = 1, n
      A(j,j) = A(j,j) + q(j)**2
   END DO

   ! solve eigenvalue problem. Eigen vectors are in v, eigen values are in lambda.
   CALL CGEEV('N','V',n,A,n,lambda,VLEFT,1,v,n,WORK,LWORK,RWORK,INFO)
   IF (INFO .ne. 0) THEN
      WRITE(*,*) 'secular: ERROR: sgeev failed, info = ', INFO
      STOP
   END IF

   ! sqrt(lambda) and remaining matrix blocks vl, vx, vlx
   lambda = SQRT(lambda)
   
   DO i = 1, n
      vl(:,i)=lambda(i)*v(:,i)
   END DO

   DO i = 1, n
      z = EXP(-lambda(i)*d)
      vx(:,i) = v(:,i) * z
      vlx(:,i) = vl(:,i) * z
   END DO


! TM JOB
ELSE IF (JOB .eq. 'TM') THEN

   ! eps matrix
   DO i = 1, n
      DO j = 1, n
         epsmat(i,j) = eps(i-j)
      END DO
   END DO

   ! inv eps matrix
   CALL inveps(eps,n,epsinv)

   ! eigenvalue problem matrix A
   A = epsinv

   DO i = 1, n
      DO j = 1, n
         A(i,j) = A(i,j) * q(i) * q(j)
      END DO
   END DO

   DO i = 1, n
      A(i,i) = A(i,i) - (1,0)
   END DO

   A = MATMUL(epsmat,A)

   ! solve eigenvalue problem. Eigen vectors are in v, eigen values are in lambda.
   CALL CGEEV('N','V',n,A,n,lambda,VLEFT,1,v,n,WORK,LWORK,RWORK,INFO)
   IF (INFO .ne. 0) THEN
      WRITE(*,*) 'secular: ERROR: sgeev failed, info = ', INFO
      STOP
   END IF

   ! sqrt(lambda) and remaining matrix blocks vl, vx, vlx

   lambda = SQRT(lambda)

   DO i = 1, n
      vl(:,i)=lambda(i)*v(:,i)
   END DO

   DO i = 1, n
      z = EXP(-lambda(i)*d)
      vx(:,i) = v(:,i) * z
      vlx(:,i) = vl(:,i) * z
   END DO

   vl  = MATMUL(epsinv, vl )
   vlx = MATMUL(epsinv, vlx)

ELSE
   WRITE(*,*) 'secular: ERROR: input JOB invalid'
   STOP
END IF

END SUBROUTINE secular
