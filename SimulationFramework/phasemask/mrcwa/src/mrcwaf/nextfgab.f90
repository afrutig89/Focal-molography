SUBROUTINE nextfgab(n, v, vl, vx, vlx, f, g, a, b)
IMPLICIT NONE

INTEGER, INTENT(IN) :: n
COMPLEX(4), INTENT(IN) :: v(n,n), vl(n,n), vx(n,n), vlx(n,n)
COMPLEX(4), INTENT(INOUT) :: f(n,n), g(n,n), a(n), b(n)


INTEGER :: lwork, info
INTEGER, ALLOCATABLE, SAVE :: IPIV(:)
COMPLEX(4), ALLOCATABLE, SAVE :: p(:,:), work(:), M(:,:), s(:)

lwork = 64*2*n

IF ( .NOT. ALLOCATED(M) ) ALLOCATE(p(2*n,2*n),IPIV(2*n),work(lwork),M(n,n),s(n))

p(1:n,1:n) = f
p(n+1:2*n,1:n) = g
p(1:n,n+1:2*n) = -v
p(n+1:2*n,n+1:2*n) = vl

! invert p
CALL cgetrf( 2*n, 2*n, p, 2*n, IPIV, info )
CALL cgetri( 2*n, p, 2*n, IPIV, work, lwork, info )

M = MATMUL(p(n+1:2*n,1:n),vx)+MATMUL(p(n+1:2*n,n+1:2*n),vlx)
s = MATMUL(p(n+1:2*n,1:n),a)+MATMUL(p(n+1:2*n,n+1:2*n),b)

f = v+MATMUL(vx,M)
g = vl-MATMUL(vlx,M)
a = -MATMUL(vx,s)
b = MATMUL(vlx,s)

END SUBROUTINE nextfgab
