from numpy import *
from stack import *
from domain import domain, background
from PIL import Image
from PIL import ImageColor

class cell:

    def __init__(self, T, H, mat, anchor=None ):
        self.T = T
        self.H = H
        if anchor != None:
            domain.anchor = anchor
        self.background = background(mat)
        self.pile = [ self.background ]

    def __add__(self,dom):
        self.pile.append(dom)

    def __iadd__(self,dom):
        self.pile.append(dom)
        return self

    def mat(self,r):
        "Return material at given position."
        # OPTIONAL: check if r is a numpy array([0.0, 0.0])
        for dom in reversed(self.pile):
            if dom.inside(r-dom.anchor-dom.pos):
                return dom.mat

    def n(self,r,l):
        "Return refractive index at given position and wavelength."
        # OPTIONAL: check if r is a numpy array([0.0, 0.0])
        for dom in reversed(self.pile):
            if dom.inside(r-dom.anchor-dom.pos):
                return dom.mat(l)

    def paint(self, d=None):
        "Return a PIL RGB image of the cell."

        if d == None:
            d = self.T/100.0

        pile=self.pile

        nx = int(self.T/d)
        ny = int(self.H/d)

        data = zeros([nx,ny,3], dtype = uint8)

        y = d
        for i in range(ny):
            x = d
            for k in range(nx):
                for dom in reversed(pile):
                    if dom.inside(array([x,y])-dom.anchor-dom.pos):
                        data[k,i,:] = ImageColor.getrgb(dom.mat.color)
                        break
                x=x+d
            y=y+d

        return Image.frombuffer('RGB',(ny,nx),data,'raw','RGB',0,1).transpose(2)


    def slice(self, dx=None, dy=None, xmesh=None, ymesh=None):
        "Return a stack from the cell."
        T = float(self.T)
        H = float(self.H)

#         if keywords.has_key('xmesh'):
#             xmesh = keywords['xmesh']
#         else:
#             if keywords.has_key('dx'):
#                 dx = keywords['dx']
#             else:
#                 dx = T/100.0
#             xmesh = linspace(0,T,round(T/float(dx))+1)

#         if keywords.has_key('ymesh'):
#             ymesh = keywords['ymesh']
#         else:
#             if keywords.has_key('dy'):
#                 dy = keywords['dy']
#             else:
#                 dy = T/100.0

#             ymesh = linspace(0,H,round(H/float(dy))+1)

        if xmesh == None:
            if dx == None:
                dx = T/100.0
            xmesh = linspace(0,T,round(T/float(dx))+1)

        if ymesh == None:
            if dy == None:
                dy = T/100.0
            ymesh = linspace(0,H,round(H/float(dy))+1)

        list = []
        for i,y in enumerate(ymesh[1:]):
            list.append(slice(y-ymesh[i],self.cut(y,xmesh)))
        # merge identical layers:
        i = 0
        while i < len(list)-1:
            if list[i+1].steps == list[i].steps:
                list[i].d += list.pop(i+1).d
            else:
                i+=1
        list.reverse()
        return list

    # OPTIONAL: implement automatic refining at boundaries,
    # e.g. by adapting the mesh from inside the loop
    def cut(self,y,xmesh):
        "Return a cut at y through the cell."
        pile = self.pile
        list = []
        for x in xmesh[1:]:
            for dom in reversed(pile):
                if dom.inside(array([x,y])-dom.anchor-dom.pos):
                    list.append([x,dom.mat])
                    break
        # merge in x-direction:
        i = 0
        while i < len(list)-1:
            if list[i+1][1] == list[i][1]:
                list[i][0] = list.pop(i+1)[0]
            else: i+=1
        return list

