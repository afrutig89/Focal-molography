from numpy import *
import mrcwaf
from PIL import Image
from PIL import ImageColor

class stack:
    
    def __init__(self,slices=[], order=(-12,12), l=1.0, theta=0, pol='TE', method='partial'):

        # slices
        self.slices = slices

        # default prameters
        self.order  = order
        self.l      = l
        self.theta  = theta
        self.pol    = pol
        self.method = method

        # data:
        # slices = []
        # q
        # kr
        # kt
        # b
        # refl
        # trans

    def __add__(self,slice):
        self.slices.append(slice)

    def __iadd__(self,slice):
        self.slices.append(slice)
        return self

    def fourier(self):
        "Calc and assign Fourier coefficients to all slices in the stack."
        o = self.order[1]-self.order[0]
        l = self.l
        for slice in self.slices:
            slice.fourier(l,o)

    def wavevec(self):
        "Calculate wave vectors in incident and transmission region."
        M=self.order[0]; P=self.order[1]; l=self.l
        theta = self.theta*pi/180.0
        epsr=self.slices[0].steps[0][1](l)**2
        epst=self.slices[-1].steps[0][1](l)**2
        Q = l/float(self.T())
        self.q,self.kr,self.kt = mrcwaf.wavevec(theta,epsr,epst,Q,M,P)

#     def angles(self):
#         "Return diffraction angles in reflection and transmission region relative z-axis."

#         if not hasattr(self,'q'):
#             self.wavevec()
        
#         return angle( self.kr + 1j*self.q )*180/pi, angle( self.kt + 1j*self.q )*180/pi


    def angle(self, side='R', order=0, deg=True):
        """Return angle of diffraction order 'order' on side 'side',
        where 'side' is one of 'R' and 'T'. If the order is complex, +-pi/2 respectively +-90 is returned."""

        o = order - self.order[0]

        if not hasattr(self,'kr'):

            self.wavevec()

        kdict = { 'R':self.kr, 'T':self.kt }
    
        k = kdict[side][o]
        q = self.q[o]

        if not k.imag == 0:

            theta = sign(q) * 0.5 * pi

        else:

            theta = arctan(q/k.real)

        if deg:

            theta = theta * 180/pi

        return theta


    def dispersion(self, side='R', order=0, deg=True):
        """Return dispersion of diffraction order 'order' on side 'side',
        where 'side' is one of 'R' and 'T'. If the order is complex, None is returned."""

        o = order - self.order[0]

        if not hasattr(self,'kr'):

            self.wavevec()

        kdict = { 'R':self.kr, 'T':self.kt }
    
        k = kdict[side][o]

        if k.imag != 0:

            return None

        D = abs(order) / ( self.T() * k.real )

        if deg:

            D = D * 180/pi

        return D


    def full(self):
        "Calculate waves with full solution approach."
        M=self.order[0]; P=self.order[1]; l=self.l; job=self.pol
        n = P-M+1
        o = P-M
        nz = -M
        q = self.q; kr=self.kr; kt=self.kt
        slices=self.slices
        S=len(slices)
        L=S-2
        d=empty(S,dtype=float)
        eps=empty((2*(P-M)+1,S),dtype=complex)
        for i,slice in enumerate(slices):
            d[i]=slice.d
            eps[:,i]=slice.fcoeff
        d = 2*pi/l*d
        self.b = b = mrcwaf.waveamp(job,q,kr,kt,d,nz+1,eps)
        if job == 'TE':
            self.refl = real( conj(b[:n])*b[:n] ) * real( kr / kr[nz] )
            self.trans = real( conj(b[2*n*L+n:2*n*(L+1)])*b[2*n*L+n:2*n*(L+1)] ) * real( kt / kr[nz] )
        elif job == 'TM':
            self.refl = real( conj(b[:n])*b[:n] ) * real( kr / kr[nz] )
            self.trans = real( conj(b[2*n*L+n:2*n*(L+1)])*b[2*n*L+n:2*n*(L+1)] ) * real( kt / kr[nz] * eps[o,0]/eps[o,-1] )
        return self.refl, self.trans

    def partialr(self):
        "Calculate reflected waves with (fast) partial solution method."
        M=self.order[0]; P=self.order[1]; l=self.l; job=self.pol
        n = P-M+1
        o = P-M
        nz = -M
        q = self.q; kr=self.kr; kt=self.kt
        slices=self.slices
        S=len(slices)
        d=empty(S,dtype=float)
        eps=empty((2*(P-M)+1,S),dtype=complex)
        for i,slice in enumerate(slices):
            d[i]=slice.d
            eps[:,i]=slice.fcoeff
        d = 2*pi/l*d
        self.b = b = mrcwaf.partialr(job,q,kr,kt,d,nz+1,eps)
        if job == 'TE':
            self.refl = real( conj(b[:n]) * b[:n] ) * real(kr / kr[nz])
        elif job == 'TM':
            self.refl = real( conj(b[:n]) * b[:n] ) * real(kr / kr[nz])
        return self.refl

    def partialt(self):
        "Calculate transmitted waves with (fast) partial solution method."
        M=self.order[0]; P=self.order[1]; l=self.l; job=self.pol
        n = P-M+1
        o = P-M
        nz = -M
        q = self.q; kr=self.kr; kt=self.kt
        slices=self.slices
        S=len(slices)
        d=empty(S,dtype=float)
        eps=empty((2*(P-M)+1,S),dtype=complex)
        for i,slice in enumerate(slices):
            d[i]=slice.d
            eps[:,i]=slice.fcoeff
        d = 2*pi/l*d
        self.b = b = mrcwaf.partialt(job,q,kr,kt,d,nz+1,eps)
        if job == 'TE':
            self.trans = real( conj(b[:n]) * b[:n] ) * real( kt/kr[nz] )
        elif job == 'TM':
            self.trans = real( conj(b[:n]) * b[:n] ) * real( kt/kr[nz] * eps[o,0]/eps[o,-1] )
        return self.trans

    def compute(self):
        """Calculate the forward and backward scattered waves with the default solution method."""
        self.fourier()
        self.wavevec()
        if self.method == 'partial':
            self.partialr()
            self.partialt()
        elif self.method == 'full':
            self.full()
        return self.refl, self.trans

    def computeTransmittedOnly(self):
        """Calculates only the transmitted (forward scattered waves) diffraction orders."""
        self.fourier()
        self.wavevec()
        self.partialt()
        return self.trans

    def computeReflectedOnly(self):
        """Calculates only the transmitted (forward scattered waves) diffraction orders."""
        self.fourier()
        self.wavevec()
        self.partialr()
        return self.refl

    def paint(self,d=None):
        """Return a PIL RGB image of the stack."""
        T=self.T()
        H=self.H()
        # default resolution
        if d == None:
            d = self.l/100.0
        slices = self.slices

        ymesh = []

        y = 0
        for slice in slices:
            y += slice.d
            ymesh += [y]

        ymesh = array(ymesh)

        ny = int(ymesh[-1] / float(d)) - 1

        nx = int(T/float(d)) -1 

        data = zeros([nx,ny,3], dtype = uint8)

        y = d
        for i in range(ny):

            steps = slices[ymesh.searchsorted(y)].steps

            xmesh = array( [ step[0] for step in steps] )
            f = array( [ step[1] for step in steps] )

            x = d
            for k in range(nx):
                data[k,i,:] = ImageColor.getrgb(f[xmesh.searchsorted(x)].color)
                x=x+d
            y=y+d

        return Image.frombuffer('RGB',(ny,nx),data,'raw','RGB',0,1).transpose(1).transpose(4)


# auxilliary:
    def T(self):
        T = self.slices[0].steps[-1][0]
        for slice in self.slices:
            if slice.steps[-1][0] != T:
                raise ValueError, 'Failed to determine T: Slices of unequal length.'
        return T

    def H(self):
        H = 0
        for slice in self.slices:
            H += slice.d
        return H

class slice:

    def __init__(self,d,steps=[]):
        self.d = d
        self.steps = steps

    def __add__(self,step):
        self.steps.append(step)

    def __iadd__(self,step):
        self.steps.append(step)
        return self

    def fourier(self,l,o):
        "Given lambda and number of orders, calculate and assign Fourier coefficients to the slice."
        # Note: While the realspace slice is described in terms of its refractive index,
        # The fouriercoefficients are the expanded eps=n**2!"
        x = array([step[0] for step in self.steps],dtype=float64)
        f = array([step[1](l)**2 for step in self.steps],dtype=complex64)
        self.fcoeff = mrcwaf.fouriercoeff(x,f,o)
        return self.fcoeff
