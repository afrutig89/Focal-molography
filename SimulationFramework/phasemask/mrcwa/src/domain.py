from numpy import *
from material import *

class domain:
    anchor = zeros(2)

class background(domain):

    def __init__(self,mat,pos=(.0,.0)):
        self.pos = array(pos)
        self.mat = mat

    def inside(self,r):
        return 1

class halfspace(domain):

    def __init__(self,y0,y1,mat,pos=(.0,.0)):
        self.pos = array(pos)
        self.mat = mat
        if y1 < y0:
            t = y0
            y0 = y1
            y1 = t
        self.y0=y0
        self.y1=y1

    def inside(self,r):
        y=r[1]
        if self.y0 < y < self.y1:
            return 1
        else:
            return 0

class bar(domain):

    def __init__(self,y0,y1,mat,pos):
        self.pos = array(pos)
        self.mat = mat
        if y1 < y0:
            t = y0
            y0 = y1
            y1 = t
        self.y0=y0
        self.y1=y1

    def inside(self,r):
        y=r[1]
        if self.y0 < y < self.y1:
            return 1
        else:
            return 0

class rectangle(domain):

    def __init__(self,width,height,mat,pos):
        self.pos = array(pos)
        self.mat = mat
        self.width = width
        self.height = height

    def inside(self,r):
        if abs(r[0]) < 0.5*self.width and abs(r[1]) < 0.5*self.height:
            return 1
        else:
            return 0

class point(domain):

     def __init__(self,mat,pos):
          self.pos = array(pos)
          self.mat = mat

     def inside(self,r):
         if r[0] == 0.0 and r[1] == 0.0:
             return 1
         else:
             return 0

class circle(domain):

     def __init__(self,radius,mat,pos):
          self.pos = array(pos)
          self.mat = mat
          self.radius = radius

     def inside(self,r):
         if sum(multiply(r,r)) < self.radius**2:
             return 1
         else:
             return 0


# CONTINUE: add more basic shapes
