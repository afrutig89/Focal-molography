#! /usr/bin/env python

from distutils.core import setup
from distutils.command.build import build

from version import *
from machine_cfg import *

class mrcwa_build(build):
   def run(self):

      f90='fouriercoeff.f90 wavevec.f90 waveamp.f90 partialr.f90 partialt.f90 nextfg.f90 nextfgab.f90 secular.f90 inveps.f90'
      pyf='mrcwaf.pyf'

      libstrings = ['-l'+lib for lib in libs]
      Lstrings   = ['-L'+path for path in libpaths]
      
      f2py_command='cd src; cd mrcwaf;'
      f2py_command+='f2py -c '
      f2py_command+=' '.join(f2py_options)+' '
      f2py_command+=' '.join(Lstrings)+' '
      f2py_command+=' '.join(libstrings)+' '
      f2py_command+=f90+' '+pyf+';'
      f2py_command+='cp mrcwaf'+dll_suffix+' ..'

      import os
      os.system(f2py_command)

      return build.run(self)


setup(name         = "mrcwa",
      version      = version,
      description  = "Multilayer Rigorous Coupled Wave Analysis",
      author       = "Helmut Rathgen",
      author_email = "helmut.rathgen@web.de",
      url          = "http://mrcwa.sourceforge.net",
      packages     = ['mrcwa'],
      package_dir  = {'mrcwa':'src'},
      package_data = {'mrcwa': ['mrcwaf'+dll_suffix]},
      cmdclass     = {'build': mrcwa_build},
      )
