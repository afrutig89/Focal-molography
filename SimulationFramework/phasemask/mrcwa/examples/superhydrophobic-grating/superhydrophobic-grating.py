# This example computes the diffraction from a superhydrophobic optical grating
# immersed in water. A curved liquid-gas interface spans each groove.
# The data is stored in hdf5 format ( http://en.wikipedia.org/wiki/Hierarchical_Data_Format )
# using the tables module


from mrcwa import *
from tables import *

# dimensionless curvature of the interface
kr=-0.5
basename = 'kr%.2f' % (kr)

# groove width
g = 4
# wall width
w = 4
# depth
h = 16

# period
T = g + w
r = 0.5 * g
R=r/kr

l = R * sqrt( 1 - r**2 / R**2 )
zeta = R * ( 1 - sqrt( 1 - r**2 / R**2 ) )

air = material()
water = material(1.33,'blue')
Si = material((4.367+0.079j),'black')

box = cell(T, h+g, water)

domain.anchor = array([r,h+r])

d = 0.006

if kr < 0:

    box + halfspace(-inf, 0.0, Si)
    box + rectangle(g, h, air, (0, -0.5*h) )
    box + circle(R, water, (.0,-l))

    ymesh = hstack( ( array([0.0,r]), arange(r+h+zeta-d, h+r+d, d), array([h+g]) ) )

elif kr > 0:

    box + circle(R, air, (.0,-l))
    box + halfspace(-inf, 0.0, Si)
    box + rectangle(g, h, air, (0, -0.5*h) )

    ymesh = hstack( ( array([0.0,r]), arange(r+h-d, h+g+d, d) ) )

xmesh = hstack( (arange(0,g+0.01,0.001), array([T]) ) )

# generate a stack of the cell.
# To speed up the slicing process, the slicing mesh is redefined (optional)
grating=stack( box.slice(xmesh=xmesh, ymesh=ymesh) )
#grating=stack( box.slice() )

# save an image of the stack
grating.paint(0.01).save(basename+'.jpg')

M = -128
P = 128

grating.order=(M,P)

grating.l = 0.488

grating.fourier()

angles = arange(0,88,0.4)
orders = arange(M,P+1)

rte=empty((len(orders),len(angles)))
for i,theta in enumerate(angles):
    grating.theta = theta
    grating.wavevec()
    rte[:,i]=array(grating.partialr())

# save the data in hdf5 format

h5file = openFile(basename+'.h5', mode = "w")
h5file.createArray(h5file.root, 'rte', rte)
h5file.root.rte.attrs.theta=angles
h5file.root.rte.attrs.order=orders
h5file.close()

