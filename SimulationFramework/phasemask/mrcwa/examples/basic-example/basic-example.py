# This code snippet represents minimal examples that
# explain the code logic

# load all functions from the library
from mrcwa import *

# Material instances are created in this way.
air  = material(1.0)
SiO2 = material(1.45,'gray')
# The first argument is the refractive index.
# As an optional the second argument a color may be specified,
# that will be used when painting a fake color
# representation of the grating for visualization.

# A grating slice is created through the slice class in this way.
binary       = slice( 1.0,  [ [0.5,air], [1,SiO2]  ] )
# The first argument is the thickness of the slice. the second argument
# is a list of steps. Each step comprises an x-position and a material.
# A step with an x-position 'x' and material 'mat' is interpreted as:
# 'starting from the previous step, until x, use material mat'.
# Thus, in above example the slice represents an air groove with width 0.5,
# followed by a SiO2 wall that extends until 1, i.e that also has a width 0.5.

# The incident and transmission halfspaces are represented as slices with
# only one step that extends over the full period. The thickness of these slices
# may be set to an arbitrary number
incident     = slice( 1.0,  [              [1,SiO2]  ] )
transmission = slice( 1.0,  [              [1,air]   ] )

# The grating is created in this way
grating = stack( [incident, binary, transmission] )
# The grating slices are passed in a list, starting from the incident slice
# and ending at the transmission slice

# The wavelength, polarization and incident angle are specified in this way
grating.l     = 0.064
grating.po    = 'TE'
grating.theta = 0
# The polarization can be either 'TE' or 'TM'

# The number of positive and negative diffraction orders retained
# in the computation is determined like this
grating.order = (-100, 100)

# An fake color image of the grating is saved for visalization
grating.paint().save('basic-example.png')

# The calculation is executed by calling the compute method
R, T = grating.compute()

print T
# The method returns the intensities of the reflected and transmitted
# diffraction orders.

