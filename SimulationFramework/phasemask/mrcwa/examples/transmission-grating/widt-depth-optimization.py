# This example computes the optimization of the groove width and depth of
# a binary fused silica transmission grating for 1st order Littrow configuration
# with transmission angle 60 deg

from numpy import *
from mrcwa import *
import cPickle

# diffraction orders
order = (-12, 12)

# 1st order Littrow mount with transmission angle 60 deg
m = -1
n = 1.45
l = 1.064
theta = arcsin(1.0/n * sin(60.0*pi/180)) * 180/pi
T = -m * l / ( 2 * n *  sin(pi/180*theta) )

# create material instances
SiO2 = material(1.45,'gray')
air  = material(1.0,'white')

# grating slices including 'halfspaces'
incident     = slice( 1.0,  [              [T,SiO2]  ] )
binary       = slice( 1.0,  [ [0.5*T,air], [T,SiO2]  ] )
transmission = slice( 1.0,  [              [T,air]   ] )

# stack
grating=stack([incident, binary, transmission])

# array for results
widths = arange(0.01, 1, 0.01)
depths = arange(0.01, 6, 0.01)
orders = array([-1,0,1])

intensity = { 'TE':empty( (len(widths), len(depths), len(orders)) ),
              'TM': empty( (len(widths), len(depths), len(orders)) )  }

# computation parameters
grating.order = order
grating.l     = l
grating.theta = theta

grating.wavevec()

# widths
for i, w in enumerate(widths):

    print w

    binary.steps[0][0] = T*w

    grating.fourier()

    # depths
    for j, d in enumerate(depths):

        binary.d = T*d

        # polarizations
        for pol in [ 'TE', 'TM' ]:

            grating.pol = pol

            intensity[pol][i,j,:] = grating.partialt()[11:14]

file=open('width-depth-optimization.pyd','w')
cPickle.dump([intensity, widths, depths, orders], file, 1)
file.close()
