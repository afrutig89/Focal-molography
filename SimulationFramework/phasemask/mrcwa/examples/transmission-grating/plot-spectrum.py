from numpy import *
from pylab import *
import cPickle


file=open('spectral-characteristics.pyd','r')
intensity, angle, dispersion, wavelengths, orders = cPickle.load(file)
file.close()

plot( 1e3 * wavelengths, intensity['TE'][:,orders==-1], 'b-')
plot( 1e3 * wavelengths, intensity['TM'][:,orders==-1], 'b--')

xlabel('$\lambda$ [nm]')
ylabel('intensity')

savefig('plot-spectrum.png')
