from scipy import *
from pylab import *
import cPickle


interpolation='bicubic'
vmin = 0
vmax = 1


file=open('width-depth-optimization.pyd','r')
intensity, widths, depths, orders = cPickle.load(file)
file.close()

n = orders.searchsorted(-1)

xlims = ( widths[0], widths[-1] )
ylims = ( depths[0], depths[-1] )

extent = ( widths[0], widths[-1], depths[0], depths[-1] )
aspect = ( widths[-1] - widths[0] ) / float( depths[-1] - depths[0] )


rc('figure.subplot', right=0.9)
rc('axes', titlesize=10)
rc('axes', labelsize=8)
rc('xtick', labelsize=8)
rc('ytick', labelsize=8)

figure( )


I = intensity['TE'][:,:,n]

subplot(1,3,1)
imshow(I.transpose(), origin='lower', extent=extent, interpolation=interpolation, aspect=aspect, vmin=vmin, vmax=vmax)
xlim(xlims)
ylim(ylims)
xticks( (0, 0.2, 0.4, 0.6, 0.8, 1), ('0', '0.2', '0.4', '0.6', '0.8', '1'))
xlabel('width [T]')
ylabel('depth [T]')
title(r'$s$-polarized')

I = intensity['TM'][:,:,n]

subplot(1,3,2)
imshow(I.transpose(), origin='lower', extent=extent, interpolation=interpolation, aspect=aspect, vmin=vmin, vmax=vmax)
xlim(xlims)
ylim(ylims)
xticks( (0, 0.2, 0.4, 0.6, 0.8, 1), ('0', '0.2', '0.4', '0.6', '0.8', '1'))
xlabel('width [T]')
ylabel('depth [T]')
title(r'$p$-polarized')


I = 0.5 * ( intensity['TE'][:,:,n] + intensity['TM'][:,:,n] )

ax = subplot(1,3,3)
imshow(I[:,:].transpose(), origin='lower', extent=extent, interpolation=interpolation, aspect=aspect, vmin=vmin, vmax=vmax)
xlim(xlims)
ylim(ylims)
xticks( (0, 0.2, 0.4, 0.6, 0.8, 1), ('0', '0.2', '0.4', '0.6', '0.8', '1'))
xlabel('width [T]')
ylabel('depth [T]')
title(r'$0.5(s+p)$')

colorbar 
ax = axes( [0.92, 0.3, 0.01, 0.4] )

cb = colorbar(cax=ax)
cb.ax.set_ylabel('intensity')

savefig('plot-width-depth.png')

close()
