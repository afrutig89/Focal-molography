# This example computes the spectral characteristics of
# a binary fused silica transmission grating with groove width 0.57 T  and
# groove depth 2.07 T  for 1st order Littrow configuration
# with transmission angle 60 deg.
# besides the diffraction intensity, aslo the angles and dispersion of
# the transmitted orders are saved

from mrcwa import *
import cPickle

# groove width and depth
width = 0.57
depth = 2.07


# diffraction orders
order = (-12, 12)

# 1st order Littrow mount
m = -1
n = 1.45
l = 1.064
theta = arcsin(1.0/n * sin(60.0*pi/180)) * 180/pi
T = -m * l / ( 2 * n *  sin(pi/180*theta) )

# create material instances
SiO2 = material(1.45,'gray')
air  = material(1.0,'white')

# grating slices including 'halfspaces'
incident     = slice( 1.0,     [                [T,SiO2]  ] )
binary       = slice( depth*T, [ [width*T,air], [T,SiO2]  ] )
transmission = slice( 1.0,     [                [T,air]   ] )

# stack
grating=stack([incident, binary, transmission])

# arrays for results
wavelengths = arange(0.5,1.2,0.005)
orders  = arange(order[0], order[1]+1)

intensity={'TE':empty( (len(wavelengths),len(orders)) ),
           'TM':empty( (len(wavelengths),len(orders)) )  }

angle = empty( (len(wavelengths),len(orders)) )

dispersion = empty( (len(wavelengths),len(orders)) )

# computation parameters
grating.order = order
grating.theta = theta

for i, l in enumerate(wavelengths):
        
    print l

    grating.l = l

    grating.wavevec()

    for j, o in enumerate(orders):
        angle[i,j]      = grating.angle('T',o)
        dispersion[i,j] = grating.dispersion('T',o)

    grating.fourier()

    for pol in [ 'TE', 'TM' ]:

        grating.pol = pol

        intensity[pol][i,:]=grating.partialt()


file=open('spectral-characteristics.pyd','w')
cPickle.dump([intensity, angle, dispersion, wavelengths, orders], file, 1)
file.close()
