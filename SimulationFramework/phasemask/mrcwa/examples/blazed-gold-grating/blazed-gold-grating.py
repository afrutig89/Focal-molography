# This example calculates the spectral characteristics
# of a blazed gold grating, taking into account the
# intrinsic dispersion of the gold.
# Slices are created 'manually' rather than through the cell() class

from numpy import *
from mrcwa import *
import cPickle

# blaze angle
alpha = 33 * pi / 180

# diffraction orders
order = (-24,24)

# 1st order Littrow mount
m = -1
n = 1.0
l = 1.064
theta = 60
T = -m * l / ( 2 * n *  sin(pi/180*theta) )

# create material instances
air   = material(1.0, 'white')
gold  = material( [ [ 0.2883, (1.74  + 1.900j) ],
                    [ 0.2952, (1.776 + 1.918j) ],
                    [ 0.3024, (1.812 + 1.920j) ],
                    [ 0.3100, (1.830 + 1.916j) ],
                    [ 0.3179, (1.840 + 1.904j) ],
                    [ 0.3263, (1.824 + 1.878j) ],
                    [ 0.3351, (1.798 + 1.960j) ],
                    [ 0.3444, (1.766 + 1.846j) ],
                    [ 0.3542, (1.740 + 1.848j) ],
                    [ 0.3647, (1.716 + 1.862j) ],
                    [ 0.3757, (1.696 + 1.906j) ],
                    [ 0.3875, (1.674 + 1.936j) ],
                    [ 0.4000, (1.658 + 1.956j) ],
                    [ 0.4133, (1.636 + 1.958j) ],
                    [ 0.4275, (1.616 + 1.940j) ],
                    [ 0.4428, (1.562 + 1.904j) ],
                    [ 0.4592, (1.426 + 1.846j) ],
                    [ 0.4769, (1.242 + 1.796j) ],
                    [ 0.4959, (0.916 +  1.84j) ],
                    [ 0.5166, (0.608 +  2.12j) ],
                    [ 0.5391, (0.402 +  2.54j) ],
                    [ 0.5636, (0.306 +  2.88j) ],
                    [ 0.5904, (0.180 +  2.84j) ],
                    [ 0.6199, (0.130 +  3.16j) ],
                    [ 0.6526, (0.166 +  3.15j) ],
                    [ 0.6888, (0.160 +   3.8j) ],
                    [ 0.7293, (0.164 +  4.35j) ],
                    [ 0.7749, (0.174 +  4.86j) ],
                    [ 0.8266, (0.188 +  5.39j) ],
                    [ 0.8856, (0.210 +  5.88j) ],
                    [ 0.9537, (0.236 +  6.47j) ],
                    [ 1.033 , (0.272 +  7.07j) ],
                    [ 1.127 , (0.312 +  7.93j) ],
                    [ 1.240 , (0.372 +  8.77j) ],
                    [ 1.265 , (0.389 +  8.09j) ],
                    [ 1.291 , (0.403 +  8.25j) ],
                    [ 1.319 , (0.419 +  8.42j) ],
                    [ 1.384 , (0.436 +  8.59j) ],
                    [ 1.378 , (0.454 +  8.77j) ],
                    [ 1.409 , (0.473 +  8.96j) ],
                    [ 1.442 , (0.493 +  9.15j) ],
                    [ 1.476 , (0.515 +  9.36j) ],
                    [ 1.512 , (0.537 +  9.58j) ],
                    [ 1.550 , (0.559 +  9.81j) ],
                    [ 1.590 , (0.583 +  10.1j) ],
                    [ 1.631 , (0.609 +  10.3j) ],
                    [ 1.675 , (0.636 +  10.6j) ],
                    [ 1.722 , (0.665 +  10.9j) ],
                    [ 1.771 , (0.696 +  11.2j) ],
                    [ 1.823 , (0.730 +  11.5j) ],
                    [ 1.879 , (0.767 +  11.9j) ],
                    [ 1.937 , (0.807 +  12.2j) ],
                    [ 2.000 , (0.850 +  12.6j) ],
                    [ 2.066 , (0.896 +  13.0j) ]  ], 'yellow')

# halfspaces
incident     = slice( 0.02,  [  [T,air]  ] )
transmission = slice( 0.02,  [  [T,gold] ] )

# array for results
wavelengths = arange(0.5, 1.2, 0.01)
orders      = arange(order[0], order[1]+1)

intensity = { 'TE':empty( (len(wavelengths),len(orders)) ),
              'TM':empty( (len(wavelengths),len(orders)) ) }

angle = empty( (len(wavelengths),len(orders)) )

dispersion = empty( (len(wavelengths),len(orders)) )

# slice thickness
dh = 0.01

# the tip of the triangle divides the period in two parts
A = T * cos(alpha)**2
B = T - A

# height of the triangle
H = T * cos(alpha) * sin(alpha)

slices = []

h = dh

while h < H :

    a = h/H * A
    b = h/H * B

    slices += [ slice( dh,  [ [A-a,air], [A+b,gold], [T,air] ] ) ]

    h += dh

slices = [ incident ] + slices + [ transmission ]

# grating
grating = stack(slices)

grating.paint(0.002).save('blazed-gold-grating.png' )

# computation parameters
grating.order = order
grating.theta = theta

for i,l in enumerate(wavelengths):

    print l

    grating.l = l

    grating.wavevec()

    for j,o in enumerate(orders):
        angle[i,j] = grating.angle('R',o)
        dispersion[i,j] = grating.dispersion('R',o)

    grating.fourier()

    for pol in [ 'TE', 'TM' ]:

        grating.pol = pol

        intensity[pol][i,:] = grating.partialr()

file=open('blazed-gold-grating.pyd','w')
cPickle.dump([intensity, angle, dispersion, wavelengths, orders], file, 1)
file.close()
