from numpy import *

class linterpol:

    def __init__(self, x, y):

        x = array(x)
        y = array(y)

        i = x.argsort()
        x = x[i]
        y = y[i]

        self.x = x
        self.y = y


    def __call__(self,x0):

        x = self.x
        y = self.y

        i = x.searchsorted(x0)

        xp = x[i]
        yp = y[i]
        xm = x[i-1]
        ym = y[i-1]

        return ym + (yp-ym) / (xp-xm) * (x0-xm)


class material:

    """Base class for optical materials.

When called, returns the (complex) refractive index at a given wavelength.

Creation of a new instance accepts two arguments: The first argument is a specification of the refractive index. The second argument is a color that is to be used for the generation of fake color images. If the first argument is a complex number, this number is used as the refractive index at any wavelength. If the first argument is a list of tuples (or an Nx2 array or anything that can be converted into one), the first element in each tuple is interpreted as a wavelength and the second argument as a refractive index and the refractive index at a given wavelength is found by linear interpolation. If the first argument is callable, it is interpreted as a function that returns the refractive index at a given wavelength.

The default arguments are n=1.0 and color='white'

Usage examples:

   air = material()

   Si = material(19+0.7j, 'darkgray')

   aluminum = material([ [ 563.56, (1.018 + 6.846j) ], [ 516.60, (0.826 + 6.283j) ] ], 'gray')

   def f(l):
      return 19+0.7j + l*(0.01+0.02j)

   Si = material(f)
"""

    def __init__(self, n=1.0, color='white'):

        if callable(n):

            self.__call__ = n

        elif hasattr(n,'__iter__'):

            data = array(n)

            self.__call__ = linterpol(data[:,0], data[:,1])

        else:

            self.n = n

        self.color=color

    def __call__(self,l):
        return self.n



# aluminum = material([ [ 563.56, (1.018 + 6.846j) ], [ 516.60, (0.826 + 6.283j) ]],'gray')

# print aluminum(540)

# import cPickle

# file= open('test.pyd','w')

# cPickle.dump(aluminum,file,1)

# file.close()

# Usage examples:
#
# Creation of a new material with default properties:
# >>> air = material()
# 
# Invoking the material returns the refractive index.
# The call requires the wavelength as an argument:
# >>> air(0.488)
# 1.0
# The material also has a fake color that is to appear
# when an image of the cell is created:
# >>> air.color
# 'white'
#
# Creation of a new material with custom
# refractive index and color:
# >>> water = material(1.33,'blue')
# >>> water(1.0)
# 1.33
# >>> water.color
# 'blue'
#
# Complex refractive index:
# >>> Si = material(19+0.7j)
#
# Wavelength dependent refractive index:
# >>> def f(l):
# ...    return 19+0.7j + l*(0.01+0.02j)
# ...
# >>> Si = material(f)
#
# Equivalent way using python's lambda form:
# >>> Si = material( lambda l: 19+0.7j + l*(0.01+0.02j) )
#
# Note the difference that in this way the function f
# does not occupy namespace in the main program
# 
# 
# Interpolated refractive index:
# >>> import scipy
# >>> x = [ 1, 2, 3]
# >>> y = [ 1, 2, 3]
# >>> f = interpolate.interp1d(x,v)
# >>> Si = material(f)
#
# Again, without defining f in the main scope:
# >>> import scipy
# >>> x = [ 1, 2, 3]
# >>> y = [ 1, 2, 3]
# >>> Si = material(interpolate.interp1d(x,y))


# old code (abandoned because unpickleable)
# class material:
#     def __call__(self,l):
#         return l
#     def __init__(self,n=lambda l:1.0,color='white'):
#         if type(n)==type(lambda l:1.0):
#             self.__call__=n
#         else:
#             self.__call__=lambda l:n
#         self.color=color
