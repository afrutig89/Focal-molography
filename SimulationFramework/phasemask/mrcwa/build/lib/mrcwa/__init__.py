# Imports.

from numpy import *

from version import *
from material import *
from domain import *
from cell import *
from stack import *

# Splash screen.

print
print "    Multilayer Rigorous Coupled Wave Analysis ver.", version
print "- Copyright (C) 2007-2008 Helmut Rathgen, University of Twente -"
print


