from mrcwa import *
import numpy as np
import sys
sys.path.append("/Users/andreasfrutiger/Daten/ds_Admin_Andreas/Studium/ETH/PhD/Projects/Focal Molography/Simulation tool Focal Molography/Focal-Molography")
import auxiliaries.writeLogFile as WL
from phasemask.plotsPhaseMask import *
from phasemask.phase_mask import *

from waveguide.waveguide import Waveguide           # waveguide calculations
from mologram.generateMask import *
from mologram.braggCircle import calcBraggCircle
from mologram.mologram import Mologram

if __name__=="__main__":

	Pol = ['TE']

	for i in Pol:
		waveguide = Waveguide(name='Tantalum Pentoxide', n_s=1.521, n_f=2.117, 
			n_c=1.33, d_f=145e-9, polarization='TE', 
			inputPower=1, wavelength=632.8e-9, attenuationConstant=0)
		mologram = Mologram(waveguide, wavelength=632.8e-9, focalPoint=-900e-6, radius=10e-6)
		mologram.braggRadius = calcBraggCircle(mologram)	# add mask to avoid the resonant case (second-order Bragg reflection) in the propagation path of the waveguide mode
		mologram.braggOffset = 2.5e-6
		mologram.mask = []
		mologram.mask.append(Mask().annulus(mologram.braggRadius-mologram.braggOffset,mologram.braggRadius+mologram.braggOffset, center=(-mologram.braggRadius,0),inverted=False))
		mologram.mask.append(Mask().disk(5e-6,inverted=True))
		
		Gamma = mologram.calcLineDistances(N=2)
		lineLengths = mologram.calcLineLengths()

		weight_Gamma = lineLengths/np.sum(lineLengths)

		PM = PhaseMaskCalculator(mologram,1.5,2.246,1.542,390e-9,Gamma,weight_Gamma,i)

		D_optimal,h_optimal,Intensity_Contrast_optimal,weighted_Contrast = PM.optimizePhaseMask()