"""
:author: Andreas Frutiger
:created: 20.5.2017

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))))
from mrcwa import *
import numpy as np

import auxiliaries.writeLogFile as WL
from auxiliaries.Journal_formats_lib import *

# Database support functions
import sqlite3
from pandas.io.sql import read_sql

from scipy.signal import convolve2d
from phasemask.plotsPhaseMask import *
import matplotlib.ticker as ticker
from phasemask.grating import Grating

from scipy.ndimage.filters import gaussian_filter
from joblib import Parallel
import multiprocessing

from auxiliaries.constants import Z_w

__all__ = ['DiffractionGrating']

def RoundToN(x, n):

	if type(x) == type(float()) or isinstance(x, np.float64):

		rounded = round(x, -int(np.floor(np.sign(x) * np.log10(abs(x)))) + n)
		
	else: 
		
		rounded = np.ones(len(x))

		for i,j in enumerate(x):
			
			rounded[i] = round(x[i], -int(np.floor(np.sign(x[i]) * np.log10(abs(x[i])))) + n)
	
	return rounded


class DiffractionGrating(object):
	"""This is a class that allows the calculation of individual diffraction gratings and should become the base class of the phase mask simulator class. The diffraction grating
	is illuminated through a substrate with refractive index n_s and immersed in a medium with index n_i, the grating itself is made
	from the refractive index n_p

	TO DO: Etch Angle should be added to the self.tablename table, since all the other grating parameters are stored there. 
	.
	:param n_i: refractive index of the immersion medium at the illumination wavelength
	:param n_p: refractive index of the phase shift layer at the illumination wavelength
	:param n_s: refractive index of the substrate material of the phase mask at the illumination wavelength.
	:param Ill_wl: illumination wavelength [m]
	:param Pol: Polarization of the incident light (TE,TM)
	:param height: thickness of the phase shift layer. [m]
	:param Lambda: period of the diffraction grating [m]
	:param Dutycycle: float (In case an Etch Angle is specified, this is the average dutycycle.)
	:param Etch_Angle: The etch angle of the ridges [deg]
	:param Inc_Ang = 0: Incidence angle of the incident plane wave, default is 0
	:param NN = 10: Number of Fourier Components used in the simulation. 

	Height and dutycycle should be optional parameters
	
	"""

	def __init__(self,n_i,n_p,n_s,Ill_wl,height,Lambda,Dutycycle,Pol='TE',Etch_Angle=6.82,Inc_Ang = 0,NN = 10):

		if not os.path.exists('data'):
			os.makedirs('data')

		# round all values to four significant digits
		n_i = RoundToN(n_i,3)
		n_p = RoundToN(n_p,3)
		n_s = RoundToN(n_s,3)
		Ill_wl = RoundToN(Ill_wl,3)
		height = RoundToN(height,2)
		print Dutycycle
		Dutycycle = RoundToN(Dutycycle,1) # for some reason this means two significant digits


		Lambda = RoundToN(Lambda,3)

		# Material instances are created in this way.
		self.n_i  = n_i
		self.n_p  = n_p
		self.n_s = n_s

		# The first argument is the refractive index.
		# As an optional the second argument a color may be specified,
		# that will be used when painting a fake color
		# representation of the grating for visualization.

		# A grating slice is created through the slice class in this way.
		#binary	   = slice( 1.0,  [ [0.5,air], [1,SiO2]  ] )
		# The first argument is the thickness of the slice. the second argument
		# is a list of steps. Each step comprises an x-position and a material.
		# A step with an x-position 'x' and material 'mat' is interpreted as:
		# 'starting from the previous step, until x, use material mat'.
		# Thus, in above example the slice represents an air groove with width 0.5,
		# followed by a SiO2 wall that extends until 1, i.e that also has a width 0.5.
		
		self.Ill_wl = Ill_wl
		self.Pol = Pol

		self.Lambda = Lambda
		self.Dutycycle = Dutycycle
		self.Etch_Angle = Etch_Angle # the etch angles in degrees.
		self.h = height
		
		self.Inc_Ang = Inc_Ang

		self.NN = NN
		# table name for the SQlite database for the different
		self.table_name = 'PM{:4.0f}NI{:4.0f}NP{:4.0f}NS{}P{:3.0f}WL'.format(n_i*1e3,n_p*1e3,n_s*1e3,Pol,Ill_wl*1e9)
		
		self.simulation_name = self.table_name
		
		self.createPMDatabase()
		# here I should check if the entry is already in the database Optimized Masks

	def calcCutoffGratingPeriod(self,m):
		"""calculates the CutOff grating period for the mth diffraction order, according to the following formula:
		
		.. math:: \\Lambda  = {{m \\cdot \\lambda } \\over {{n_i}}}
		
		where :math: `n_i` is the refractive index of the immersion medium and :math: `\lambda` is the illumination wavelength.

		:param m: is the diffraction order.
		"""

		return m*self.Ill_wl/self.n_i

	def calc_talbot_length(self):
		"""Calculates the Talbot length according to:
		
		.. math:: {z_T} = {\\lambda  \\over {n\\left( {1 - \\sqrt {1 - {{{\\lambda ^2}} \\over {{n^2}{\\Lambda ^2}}}} } \\right)}}
		"""

		return self.Ill_wl/(self.n_i*(1-np.sqrt(1-self.Ill_wl**2/(self.n_i**2*self.Lambda**2))))

	def DiffractionAngle(self,m):

		"""An analitical formula which calculates the Diffraction angle for a given order and grating the grating sits on a medium with refractive index `n_s`
		and is immersed into a medium with refractive index `n_i`

		.. math:: \theta_{out,i} = \\arcsin \\left(\\frac{m\\lambda}{\\Lambda\\cdotn_{out}} - \\frac{n_{s}}{n_{i}}\\sin\\left( \\theta_{s}\\right)\\right)
		
		:param m: Diffraction order
		:return: Diffraction angle
		"""
		theta_out = np.arcsin(-self.n_s/self.n_i*np.sin(self.Inc_Ang)-m*self.Ill_wl/(self.Lambda*self.n_i))

		return theta_out

	def numberOfDiffractionOrders(self):
		"""returns the number of the highest diffraction order that can still propagate."""

		m = 0
		while True:

			if self.Lambda > self.calcCutoffGratingPeriod(m):
				m += 1
			else:
				return m-1 # because otherwise it is one too much

	def calcIntensityRatio(self, DiffractionOrders):
		"""
		
		:params DiffractionOrders: is a numpy array with length 7, starting at the 
		-3 diffraction order to the 3 diffraction order
		
  		
  		:return: Intensity Ratio for the particular grating period. 
  		:rtype: float
		
		"""
		# zero = DiffractionOrders[3]
		# first = DiffractionOrders[2]
		# second = DiffractionOrders[1]
		# third = DiffractionOrders[0]

		#ModulationAmplitudeIntensity = 2*first**2 + 4*zero*second + 4*first*third

		eta_I = self.mask_efficiency()

		intensity_ratio = self.ratio_from_mask_efficiency(eta_I)

		# eta_I = (r_I - 1)/(2*(r_I + 1))


		# I_ridge = 0.5 + ModulationAmplitude
		# I_groove = 0.5 - ModulationAmplitude

		# return intensity_ratio

		return eta_I 

	def performIndividualSimulation(self,D=0,h=0,Lambda=0,saveDB = False,loadDB = False,Etch_Angle= 6.82,plotStructure = False,reflection = False):
		"""Performs an indiviual simulation of a grating with a 
		fixed polarization, height, incidence angle, Period, Dutycycle, 
		n_i, n_p and Illumination wavelength.
		
		:param Etch_angle: the angle of the slope of the ridge profile.
		:return: returns a numpy array of the 0,1,2,3 Diffraction order + Intensity ratio
		"""
		

		# check whether the simulation with the values is already in the database, then this simulation does not need to be performed again.
		# ATTENTION: THE DATABASE OPERATIONS ARE EXTREMELY SLOW, ONLY ENABLE IF REALLY NEEDED!

		if D != 0:
			self.Dutycycle = D
			print D
		if h != 0:
			self.h = h
			print h
		if Lambda != 0:
			self.Lambda = Lambda
			print Lambda


		if loadDB == True:

			simulation_input = {
			'height':self.h,
			'Period':self.Lambda, 
			'Dutycycle':self.Dutycycle,
			'FourierComponents':self.NN
			}

			data_entry = self.loadSimulationResultFromDataBase(simulation_input)

			if data_entry:

				DiffractionOrder0 = data_entry['DiffractionOrder0']
				DiffractionOrder1 = data_entry['DiffractionOrder1']
				DiffractionOrder2 = data_entry['DiffractionOrder2']
				DiffractionOrder3 = data_entry['DiffractionOrder3']
				Intensity_Ratio = data_entry['IntensityRatio']
			
				return [DiffractionOrder0,DiffractionOrder1,DiffractionOrder2,DiffractionOrder3,Intensity_Ratio]

		# otherwise, perform the simulation and save the values of the diffraction orders in the database

		if Etch_Angle == 0:

			grating = Grating().rectangle(self.Dutycycle,self.h,self.Lambda,self.n_i,self.n_p,self.n_s)
			if plotStructure:
				grating.paint().save('rectangle.png')


		else:
			
			grating = Grating().trapezoidal(self.Dutycycle,self.h,self.Lambda,self.n_i,self.n_p,self.n_s,Etch_Angle)
			if plotStructure:
				grating.paint().save('trapezoidal_{:.2f}.png'.format(Etch_Angle))

		# The wavelength, polarization and incident angle are specified in this way
		grating.l     = self.Ill_wl/float(self.Lambda)
		grating.pol    = self.Pol
		grating.theta = self.Inc_Ang 
		# The polarization can be either 'TE' or 'TM'

		# The number of positive and negative diffraction orders retained
		# in the computation is determined like this
		# DO NOT CHANGE, THIS IS IMPORTANT FOR CONVERGENCE AND ALSO FOR THE EXTRACTION OF THE RIGHT NUMERICAL VALUES AFTERWARDS, default: NN =100

		grating.order = (-self.NN, self.NN)


		# An fake color image of the grating is saved for visalization
		#grating.paint().save('basic-example.png')

		# The calculation is executed by calling the compute method
		if reflection == False:
			T = grating.computeTransmittedOnly()
		else:
			T = grating.computeReflectedOnly()
		# The method returns the intensities of the reflected and transmitted
		# diffraction orders.
		
		
		first_three_diffraction_orders = T[self.NN-3:self.NN+4] 
		DiffractionOrder0 = first_three_diffraction_orders[3]
		DiffractionOrder1 = first_three_diffraction_orders[2]*2
		DiffractionOrder2 = first_three_diffraction_orders[1]*2
		DiffractionOrder3 = first_three_diffraction_orders[0]*2

		self.difforders = [DiffractionOrder0,DiffractionOrder1,DiffractionOrder2,DiffractionOrder3] 
		Intensity_Ratio = self.calcIntensityRatio(first_three_diffraction_orders)
		
		if saveDB == True:
			results = {
				'DiffractionOrder0':float(DiffractionOrder0),
				'DiffractionOrder1':float(DiffractionOrder1),
				'DiffractionOrder2':float(DiffractionOrder2),
				'DiffractionOrder3':float(DiffractionOrder3),
				'IntensityRatio':float(Intensity_Ratio)
			}

			simulation_input = {
				'height':self.h,
				'Period':self.Lambda,
				'Dutycycle':self.Dutycycles,
				'FourierComponents':self.NN,
				'Polarization':self.Pol,
				'IncidenceAngle':self.Inc_Ang,
				'n_i':self.n_i.n,
				'n_p':self.n_p.n,
				'n_s':self.n_s.n,
				'IlluminationWavelength':self.Ill_wl,
				}

			self.saveSimulationResultToDataBase(simulation_input,results)

		
		
		return [DiffractionOrder0,DiffractionOrder1,DiffractionOrder2,DiffractionOrder3,Intensity_Ratio]

	def calc_field_intensity_from_diffraction_orders(self, x, y):
		"""
		
		:param difforders: Is a vector of the diffraction orders amplitudes (relative intensities) [0,1,2,3, ...]
		:param x,y: x,y numpy arrays of the positions where the field should be evaluated.
		
		"""
		if not hasattr(self, 'difforders'):
			self.performIndividualSimulation()
		difforders = np.asarray(self.difforders)

		num_diff_orders = self.numberOfDiffractionOrders()
		for i in range(num_diff_orders+1):

			# zeroth diffraction order is treated differently than the others.
			if i == 0:
				k_x, k_y = self.calc_k_vector_diffraction_order(i)
				E_field = np.sqrt(2.*Z_w/self.n_i*difforders[i])*np.exp(1j*(k_x*x+k_y*y))
			else:
				k_x, k_y = self.calc_k_vector_diffraction_order(i)
				E_field += np.sqrt(2.*Z_w/self.n_i*difforders[i]/2.)*np.exp(1j*(k_x*x+k_y*y)) # - direction
				E_field += np.sqrt(2.*Z_w/self.n_i*difforders[i]/2.)*np.exp(1j*(-k_x*x+k_y*y)) # + direction

		return 1/2.*self.n_i/Z_w*E_field*np.conj(E_field)

	def get_field_slice(self, y, N=100):
		"""gets a field slice at the location y"""

		x = np.linspace(0 , self.Lambda, N)

		return x, np.real(self.calc_field_intensity_from_diffraction_orders(x,y))

	def get_talbot_field_matrix(self, Nx=100, Ny=200):

		x = np.linspace(0 , self.Lambda, Nx)
		y = np.linspace(0 , self.calc_talbot_length(), Ny)

		X,Y = np.meshgrid(x,y)

		return x, y, self.calc_field_intensity_from_diffraction_orders(X,Y)

	def talbot_y_average(self):
		"""Averages the field distribution behind the mask over one Talbot length."""

		x,y,Z = self.get_talbot_field_matrix()


		return x, np.mean(Z,axis=0)

	def contour_plot_field_intensity(self,x,y):

		X,Y = np.meshgrid(x,y)

		Z = self.calc_field_intensity_from_diffraction_orders(X,Y)

		return plotTwoParameterOptimization(x,y,Z)

	def mask_efficiency(self):

		x, intensity = self.talbot_y_average()

		return np.real(np.sum(np.cos(4*np.pi/self.Lambda*x)*intensity)/np.sum(intensity))

	def ratio_from_mask_efficiency(self, eta_I):

		return (2*eta_I+1)/(1-2*eta_I)

	def calc_k_vector_diffraction_order(self, m):
		"""
		:param m: Diffraction order
			
		.. math:: {k_x} = {{2\\pi m} \\over \\Lambda }

		.. math:: {k_y} = 2\\pi \\sqrt {{{{n^2}} \\over {{\\lambda ^2}}} - {{{m^2}} \\over {{\\Lambda ^2}}}} 
		"""

		k = 2*pi*self.n_i/self.Ill_wl

		k_x = 2*np.pi/self.Lambda*m

		k_y = 2*np.pi*np.sqrt(self.n_i**2/self.Ill_wl**2 - (m/self.Lambda)**2)

		return k_x, k_y


	def createPMDatabase(self):
		"""creates a sqlite Database object if it does not already exists, where all the simulation results can be stored. 
		Input parameter for the simulation are: polarization, height, incidence angle, Period, Dutycycle, n_i, n_p and Illumination wavelength
		Stored output parameters is the relative intensity in the 0,1,2 and 3 diffraction order """

		params = {
			'Polarization':'text',
			'IncidenceAngle':'real',
			'height':'real',
			'Period':'real',
			'Dutycycle':'real',
			'n_i':'real',
			'n_p':'real',
			'n_s':'real',
			'IlluminationWavelength':'real',
			'DiffractionOrder0':'real',
			'DiffractionOrder1':'real',
			'DiffractionOrder2':'real',
			'DiffractionOrder3':'real',
			'FourierComponents':'real',
			'IntensityRatio':'real'
		}
		
		WL.createDB('data/phase_mask.db',self.table_name,params)


		# create indices for faster DB access and retrival this is essential for speedup. 
		conn = sqlite3.connect('data/phase_mask.db', detect_types=sqlite3.PARSE_DECLTYPES)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'Period' + ' ON "' + self.table_name + '" (Period)'
		c = conn.cursor()
		c.execute(sqlx)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'Dutycycle' + ' ON "' + self.table_name + '" (Dutycycle)'
		c = conn.cursor()
		c.execute(sqlx)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'height' + ' ON "' + self.table_name + '" (height)'
		c = conn.cursor()
		c.execute(sqlx)
		sqlx = 'CREATE INDEX IF NOT EXISTS ' + self.table_name + 'FourierComponents' + ' ON "' + self.table_name + '" (FourierComponents)'
		c = conn.cursor()
		c.execute(sqlx)

	def saveSimulationResultToDataBase(self,simulation_input, results):
		"""Saves the result of the simulation to the database."""

		params = simulation_input.copy()
		params.update(results)
		
		WL.saveDB('data/phase_mask.db',self.table_name,params)


if __name__ == '__main__':


	grating = DiffractionGrating(n_i=1.497,n_p=2.231,n_s=1.540,Ill_wl=405e-9,height=246e-9,Lambda=777e-9,Dutycycle=0.5,Etch_Angle=0.)
	grating = DiffractionGrating(n_i=1.500,n_p=2.246,n_s=1.542,Ill_wl=390e-9,height=246e-9,Lambda=840e-9,Dutycycle=0.5,Etch_Angle=0.)

	print grating.numberOfDiffractionOrders()

	print grating.performIndividualSimulation()

	print grating.calc_talbot_length()

	print grating.mask_efficiency()

	print grating.ratio_from_mask_efficiency(grating.mask_efficiency())

	x,y,Z = grating.get_talbot_field_matrix()

	# plotTwoParameterOptimization(x,y,Z)

	# plt.show()

	# plt.plot(x,np.mean(Z,axis=0))
	# plt.show()

	print grating.difforders
	formatPRX()
	plt.figure()
	x = np.linspace(0,840e-9,100)
	y = np.linspace(0,-grating.calc_talbot_length()*2,100)

	grating.contour_plot_field_intensity(x,y)
	plt.show()

	plt.figure()
	x, intensity = grating.get_field_slice(-1000e-9)
	plt.plot(x, intensity)
	plt.ylim(0,1000)
	plt.show()