import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from phase_mask_illumination_system import *
import sys
sys.path.append('../')
from auxiliaries.Journal_formats_lib import *


# Activation function for different I_0 and given r_I (normal process)
r_I = 2
b = 0.0004
I_0 = [100, 1000, 5000, 10000]
formatPRX(2, 2)
fig, ax = plt.subplots()
for i in range(0, len(I_0)):
    x, phi = calc_activation_function_for_plotting(
        r_I=r_I, b=b, I_0=I_0[i], inverse=False, approx=False, maximize=False)
    ax.plot(x, phi, label="$I_0 = $%.f" % (I_0[i]))
ax.set_xlabel("x-position")
ax.set_ylabel("Activation")
ax.set_xlim(0, 2 * np.pi)
ax.set_ylim(0, 1)
ax.legend()
ax.set_title("Activation distribution after normal fabrication process")
# plt.savefig(
#     "/Users/ClaDuri/Desktop/ActivationDistributionNormalIntensityRatio2.png", dpi=600)
plt.show()

# Activation sampling and influence
sample, influence = activation_distribution_sampling(
    N=100, r_I=1.3, b=0.0004, maximize=True, inverse=False)
y0 = np.zeros(len(sample))
b = 0.0004
r_I = 1.3
I_0 = calc_I0_for_max_activation_modulation(b=b, r_I=r_I)
formatPRX(2, 2)
plt.plot(sample, y0, marker="o", markersize=3,
         alpha=0.3, linestyle="", label="samples")
plt.plot(sample, influence, marker="o", markersize=3,
         alpha=0.3, linestyle="", label="influence")
x, phi = calc_activation_function_for_plotting(b=b, r_I=r_I, maximize=True)
plt.plot(x, phi, label="activation")
plt.xlim(0, 2 * np.pi)
plt.annotate("mean influence: %.2f\n(analyte efficiency)" %
             (np.mean(influence)), xy=(0.08, 0.3), xycoords="axes fraction")
plt.legend()
plt.xlabel("x position")
# plt.savefig(
#     "/Users/ClaDuri/Desktop/ActivationSamplingAndInfluenceForwardI.png", dpi=600)
plt.show()

# Activity modulation maximization in terms of incident intensity (normal process)
intensities = np.linspace(0, 20000, 10000)
intensityRatios = [1.3, 2]
b = 0.0004
formatPRX(2, 2)
for r_I in intensityRatios:
    I_plus = (2. * intensities) / ((1. / r_I) + 1)
    I_minus = (2. * intensities) / (r_I + 1)
    activationmodulation = (1 - np.exp(-b * I_plus)) - \
        (1 - np.exp(-b * I_minus))
    print "I_0 such that affinity modulation is max:"
    print intensities[np.argmax(activationmodulation)]
    print "predicted was", np.log(r_I) * (1. + r_I) / (2. * b * (r_I - 1))
    print "for r_I =", r_I
    plt.plot(intensities, activationmodulation, label="$r_I=%.1f$" % (r_I))

plt.annotate("$I_{0\Delta\phi max}=\\frac{log(r_I)(1+r_I)}{2b(r_I-1)}$",
             xy=(0.55, 0.7), xycoords="axes fraction")
plt.annotate("$I_{0\Delta\phi max}(r_I=1.3)=%.1f$" % (np.log(
    1.3) * (1. + 1.3) / (2. * b * (1.3 - 1))), xy=(0.55, 0.625), xycoords="axes fraction")
plt.annotate("$I_{0\Delta\phi max}(r_I=2)=%.1f$" % (np.log(
    2) * (1. + 2) / (2. * b * (2 - 1))), xy=(0.55, 0.55), xycoords="axes fraction")
plt.xlabel("$I_0$")
plt.ylabel("$\Delta\phi$")
plt.xlim(0, 20000)
plt.legend()
# plt.savefig(
#     "/Users/ClaDuri/Desktop/ActivationModulationMaximizationIntensity.png", dpi=600)
plt.show()

# Activation sampling and influence (inverse)
sample, influence = activation_distribution_sampling(
    N=250, r_I=2, b=0.0004, maximize=False, inverse=True, I_0=10000)
x0 = np.linspace(0, 2 * np.pi, 100)
y0 = np.zeros(len(sample))
b = 0.0004
r_I = 2
I_0 = 10000
formatPRX(2, 2)
plt.plot(sample, y0, marker="o", markersize=3,
         alpha=0.3, linestyle="", label="samples")
plt.plot(sample, influence, marker="o", markersize=3,
         alpha=0.3, linestyle="", label="influence")
x, phi = calc_activation_function_for_plotting(I_0=I_0, r_I=r_I, inverse=True)
plt.plot(x, phi, label="activation")
plt.xlim(0, 2 * np.pi)
plt.xlabel("x position")
plt.annotate("mean influence: %.2f\n(analyte efficiency)" %
             (np.mean(influence)), xy=(0.5, 0.3), xycoords="axes fraction")
plt.legend()
# plt.savefig(
#     "/Users/ClaDuri/Desktop/ActivationSamplingAndInfluenceInverse.png", dpi=600)
plt.show()


# Activation function for different I_0 (inverse)
I_0 = [100, 1000, 5000, 10000]
r_I = 2
b = 0.0004
formatPRX(2, 2)
fig, ax = plt.subplots()
for i in range(0, len(I_0)):
    x, phi = calc_activation_function_for_plotting(
        r_I=r_I, I_0=I_0[i], inverse=True)
    ax.plot(x, phi, label="$I_0=%i$" % (I_0[i]))
ax.set_xlim(0, 2 * np.pi)
ax.legend()
ax.set_title("Activation distribution after inverse fabrication process")
ax.set_xlabel("x position")
ax.set_ylabel("Activation")
# plt.savefig(
#     "/Users/ClaDuri/Desktop/ActivationAndInfluenceInverseForDifferentIntensities.png", dpi=600)
plt.show()

# Optics Letters Fig. 3a) intensity ratio vs. analyte efficiency (for max. activation modulation) for the normal process
r_I = np.linspace(1.5, 20, 76)
analyteEfficiencyAnalytical = []
analyteEfficiencySimulated = []

for r_I in r_I:
    _, influence = activation_distribution_sampling(
        N=100000, b=0.0004, maximize=True, inverse=False, r_I=r_I)
    analyteEfficiencySimulated.append(np.mean(influence))
    analyteEfficiencyAnalytical.append(
        calc_analyte_efficiency_normal(b=0.0004, r_I=r_I))

formatOpticsLetters(2, 2)
fig, ax = plt.subplots()
r_I = np.linspace(1.5, 20, 76)
ax.plot(r_I, analyteEfficiencyAnalytical,
        label="$\eta_{[A]_{max}}$ (analytical)")
ax.plot(r_I, analyteEfficiencySimulated,
        label="$\eta_{[A]_{max}}$ (simulated)")
ax.set_ylabel("$\eta_{[A]_{max}}$")
ax.set_xlabel("$r_I$")
ax.set_xlim(1.3, 20)
ax.set_ylim(min(analyteEfficiencyAnalytical))
ax.legend()
ax.set_title("Analyte Efficiency (Normal Process)")
# plt.savefig(
#     "/Users/ClaDuri/Desktop/AnalyteEfficiencyNormal.png", dpi=600)
plt.show()

# Optics Letters Fig. 3b) Analyte efficiency and amount of binding sites  vs. illumination dose for different intensity ratios

r_I = [1.3, 5, 10]
I_0 = np.linspace(100, 10000, 100)
b = 0.0004
analyteEfficiency = []
bindingsites = []

for r_I in r_I:
    for i in range(0, len(I_0)):
        _, influence = activation_distribution_sampling(
            N=10000, r_I=r_I, b=0.0004, maximize=False, inverse=True, I_0=I_0[i])
        analyteEfficiency.append(np.mean(influence))
        bindingsites.append(calc_amount_of_binding_sites(
            b=0.0004, r_I=r_I, I_0=I_0[i]))

analyteEfficiency = np.asarray(analyteEfficiency)
bindingsites = np.asarray(bindingsites)

formatOpticsLetters(2, 2)
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
r_I = [1.3, 5, 10]
I_0 = np.linspace(100, 10000, 100)
for i in range(0, len(r_I)):
    ax2.plot(I_0 * b, analyteEfficiency[i * len(I_0):(i + 1)
                                        * len(I_0)], label="$r_I = $%.1f" % (r_I[i]))
    ax1.plot(I_0 * b, bindingsites[i * len(I_0):(i + 1) * len(I_0)],
             label="$r_I = $%.1f" % (r_I[i]), linestyle="--")

ax1.set_xlim(min(I_0) * b, max(I_0) * b)
ax2.set_ylim(analyteEfficiency[0])
ax1.set_xlabel("$I_0 \cdot b$")
ax2.set_ylabel("$\eta_{[A]}$")
ax1.set_ylabel("relative amount of binding sites")
ax1.set_title("Analyte Efficiency (Inverse Process)")
ax2.legend()
# plt.savefig(
#     "/Users/ClaDuri/Desktop/AnalyteEfficiencyInverse.png", dpi=600)
plt.show()

# Product of the two quantities plotted in figure 3b)
r_I = [1.3, 5, 10]
I_0 = np.linspace(100, 10000, 100)
b = 0.0004
analyteEfficiency = []
bindingsites = []

for r_I in r_I:
    for i in range(0, len(I_0)):
        _, influence = activation_distribution_sampling(
            N=100000, r_I=r_I, b=0.0004, maximize=False, inverse=True, I_0=I_0[i])
        analyteEfficiency.append(np.mean(influence))
        bindingsites.append(calc_amount_of_binding_sites(
            b=0.0004, r_I=r_I, I_0=I_0[i]))

analyteEfficiency = np.asarray(analyteEfficiency)
bindingsites = np.asarray(bindingsites)
product = analyteEfficiency * bindingsites

formatOpticsLetters(2, 2)
fig, ax = plt.subplots()

r_I = [1.3, 5, 10]
I_0 = np.linspace(100, 10000, 100)

for i in range(0, len(r_I)):
    ax.plot(I_0 * b, product[i * len(I_0):(i + 1) *
                             len(I_0)], label="$r_I = $%.1f" % (r_I[i]))

ax.set_ylabel("product")
ax.set_xlabel("$I_0 \\cdot b$")
ax.legend()
# plt.savefig(
#     "/Users/ClaDuri/Desktop/AnalyteEfficiencyBindingSitesInverseProductWithoutComments.png", dpi=600)
plt.show()
