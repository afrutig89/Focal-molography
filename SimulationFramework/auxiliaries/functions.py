# -*- coding: utf-8 -*-
"""
This module contains different useful functions.

@ author:   Silvio
@ created:  Wed Apr 20 15:14:03 2016

.. todo:: split into files
"""

from PyQt4 import QtCore, QtGui
from numpy import *
import numpy as np
import sys
sys.path.append('../')
from dipole.plots import findCoordinates
from auxiliaries.Journal_formats_lib import formatPRX, removeWhiteSpace
import matplotlib.pylab as plt
from scipy import special
from scipy.optimize import curve_fit

    
def airyDiskDiameter(wavelength, n, D, f):
    """calcultes the airy disk radius given by Abbe's formula (The width of the point spread function is usually defined as the radial distance
    for which the value of the paraxial point spread function becomes zero)
    
    :param wavelength: wavelength of the incoming wave
    :param n: refractive index of the substrate
    :param D: diameter of the mologram
    :param f: focal length
    """
    
    return wavelength*f/(n*D)


# def AiryDiskDiameter(x,I):
#     """searches the Airy Disk diameter for a given function I(x)
#     """
#     # get the maximum
#     index_max = I.argmax()
#     x_neg = x[index_max]
#     i = index_max
#     while i > 0:
#         if I[i-1] < I[i]:
#             x_neg = x[i]
#             i = i-1
#             if (i == 0):
#                 return 0
#         else:
#             break
        
#     x_pos = x[index_max]
#     i = index_max
#     while i > 0:
#         if (i+1 == len(x)):
#             return 0
#         if I[i+1] < I[i]:
#             x_pos = x[i]
#             i = i+1

#         else:
#             break
    
#     return x_pos-x_neg



def AiryFunction(x,radiusAiryPixels,plot = False):
    """returns the Airy function in pixels if x is also in pixels"""

    values = []
    for i in x:
        if i ==0:
            values.append(0.25)
            
        else:
            values.append(((special.jn(1,(i*3.83/radiusAiryPixels))/(i*3.83/radiusAiryPixels))**2))

    return np.asarray(values)

def plotAiry(radiusAiryPixels,save = False):
    """Plots a cartoon of the Airy function"""

    # plot the Airy function
    formatPRX(8,8)
    fig = plt.figure()
    x = np.linspace(-7.5,7.5,100)
    plt.plot(x,AiryFunction(x,radiusAiryPixels))
    plt.gca().set_axis_off()
    plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, 
      hspace = 0, wspace = 0)
    plt.margins(0,0)
    plt.xlim([-7.5,7.5])
    plt.ylim([-0.01,0.255])
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    fig = removeWhiteSpace(fig)

    if save == True:
        plt.savefig('02plots/Filter_Kernel.png',format='png',dpi=600)
    else:
        plt.show()

def AiryDiskDiameter(x,I):
    """searches the Airy Disk diameter for a given function I(x)

    :param x: distance vector maping to 1 pixel of the intensity vector. 
    :param I: intensity vector

    """
    x = np.asarray(x.real).astype(float)
    I = np.asarray(I.real).astype(float)


    # get the maximum in order to fit the Airy function and the rough Airy function in order to fix the curve.
    pixelsize = abs(x[1] - x[0])
    index_max = I.argmax()
    index_neg = index_max
    i = index_max
    while i > 0:
        if I[i-1] < I[i]:
            index_neg = i
            i = i-1
            if (i == 0):
                return 0
        else:
            break
        
    
    i = index_max
    index_pos = index_max

    while i > 0:
        if (i+1 == len(I)):
            return 0
        if I[i+1] < I[i]:
            index_pos = i
            i = i+1

        else:
            break

    # perform a least squares fit
    # shift the Airy function to the center

    index = np.arange(len(I)) - index_max
    #
    popt, pcov = curve_fit(AiryFunction,index,I/I.max()*0.25,p0=[abs(index_pos - index_neg)/2])

    radius_Airy = popt[0]*pixelsize # pixelsize


    return radius_Airy*2
    
def findAiryDiskDiameter(screen, screenPlane, I, focalPoint):
    """returns the Airy disk diameter in two directions.    
    
    """
    screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(screen,focalPoint)

    AiryDisk = np.zeros([2,1])
    if 'xy' in screenPlane:
        AiryDisk[0] = AiryDiskDiameter(screen_1[idx1],I[idx1,:])
        AiryDisk[1] = AiryDiskDiameter(screen_2[:,idx2],I[:,idx2])
    if 'xz' in screenPlane:
        AiryDisk[0] = AiryDiskDiameter(screen_1[:,idx1],I[:,idx1])
        AiryDisk[1] = AiryDiskDiameter(screen_2[idx2],I[idx2])
    if 'yz' in screenPlane:
        AiryDisk[0] = AiryDiskDiameter(screen_1[:,idx1],I[:,idx1])
        AiryDisk[1] = AiryDiskDiameter(screen_2[idx2,:],I[idx2,:])
        
    return AiryDisk


if __name__ == '__main__':

    ################################################################################################
    ################################################################################################
    # testing for the fitting algorithm. 
    pixelsize = 0.11e-6
    x = np.arange(-100e-6,60e-6,pixelsize)
    #print x[1] - x[0]
    radiusAiryPixels = 20.
    radiusAiry = radiusAiryPixels*pixelsize
    print radiusAiry
    print AiryFunction([0.000000000001],radiusAiry)
    intensity = AiryFunction(x,radiusAiry)

    plt.plot(x,intensity)
    radiusAiry = AiryDiskDiameter(x,intensity)/2.
    print radiusAiry
    plt.plot(x,AiryFunction(x,radiusAiry))
    plt.show()
    #plotAiry(4.0)