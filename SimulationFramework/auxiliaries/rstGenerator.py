import numpy as np
from random import randint

class rstFile(object):

    def __init__(self, filename, author=''):
        self.filename = filename
        self.author = author

        self.content = []
        self.footnotes = []
        self.references = []

    def addTitle(self, title):
        self.content.append('{}\n{}\n\n'.format(title,'='*len(title)))

    def addSubtitle(self, subtitle):
        self.content.append('{}\n{}\n\n'.format(subtitle,'-'*len(subtitle)))

    def addSubsubtitle(self, subsubtitle):
        self.content.append('{}\n{}\n\n'.format(subsubtitle,'^'*len(subsubtitle)))

    def addParagraph(self, text):
        self.content.append('{}\n\n'.format(text))

    def addBulletList(self, bulletList):
        for item in bulletList:
            self.content.append('- {}\n\n'.format(item))

    def addEnumeratedList(self, enumeratedList):
        for item in enumeratedList:
            self.content.append('{} {}\n\n'.format(enumeratedList.index(item), item))

    def addFieldList(self, fieldDict):
        for key, value in fieldDict.items():
            self.content.append(':{}: {}\n\n'.format(key, value))

    def addPicture(self, image, caption, width=100, reference=''):
        """
        :param width: width in percent
        """
        text = '|\n'
        if reference != '':
            text += '.. _{}\n\n'.format(reference)
        text += '.. figure:: {}\n'.format(image)
        text += '\t:width: {} %\n\n'.format(width)
        text += '{}\n\n'.format(caption)
        text += '|\n\n'

        self.content.append(text)


    def addMath(self, formula, label=''):
        text = '.. math::\n'
        text += '\t{}\n'.format(formula)
        if label != '':
            text += ':label: {}'.format(label)
        text += '\n\n'

        self.content.append(text)


    def addTable(self, table, header=False):
        """
        table given as rows (type list) in a list
        """
        maxLength = 0
        for row in table:
            # find longest string
            for item in row:
                if len(item) > maxLength: maxLength = len(item)

        text = ''
        for row in table:
            for column in row:
                if table.index(row) != 1 or header == False: text += '+{}'.format('-'*(maxLength+2))
                else: text += '+{}'.format('='*(maxLength+2))

            text += '+\n'
            for column in row:
                text += '| {}{}'.format(column,' '*((maxLength+1)-len(column)))
            text += '|\n'

        for column in row:
            text += '+{}'.format('-'*(maxLength+2))

        text += '+\n\n\n'

        self.content.append(text)


    def addFootnote(self, footnote):
        self.footnotes.append(footnote)
        self.content.append('[{}]_'.format(len(footnotes)))


    def addText(self, text):
        self.content.append(text)


    def generateFile(self):
        file = open(self.filename,'w')
        for line in self.content:
            file.write(line)

        for footnote in self.footnotes:
            file.write('.. [{}] {}'.format(self.footnotes.index(footnote), footnote))


if __name__ == '__main__':
    tableTest = []
    for i in range(4):
        tableTest.append([])
        for j in range(6):
            tableTest[i].append('a'*randint(0,9))

    rstfile = rstFile('test.rst')
    rstfile.addTitle('This is a test')
    rstfile.addSubtitle('This is a subtest')
    rstfile.addTable(tableTest, header=True)
    rstfile.generateFile()