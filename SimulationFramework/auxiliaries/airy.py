# -*- coding: utf-8 -*-
"""
This module contains different functions to calculate the Airy disk 
diameter.

@ author:   Silvio
@ created:  Wed Apr 20 15:14:03 2016

"""

from numpy import *
import numpy as np
from dipole.plots import findCoordinates

    
def airyDiskDiameter(wavelength, n, D, f):
    """calcultes the airy disk radius given by Abbe's formula (The width of the point spread function is usually defined as the radial distance
for which the value of the paraxial point spread function becomes zero)
    
    :param wavelength: wavelength of the incoming wave
    :param n: refractive index of the substrate
    :param D: diameter of the mologram
    :param f: focal length
    """
    
    return wavelength*f/(n*D)
def AiryDiskDiameter(x,I):
    """searches the Airy Disk diameter for a given function I(x)
    """
    index_max = I.argmax()
    x_neg = x[index_max]
    i = index_max
    while i > 0:
        if I[i-1] < I[i]:
            x_neg = x[i]
            i = i-1
            if (i == 0):
                return 0
        else:
            break
        
    x_pos = x[index_max]
    i = index_max
    while i > 0:
        if (i+1 == len(x)):
            return 0
        if I[i+1] < I[i]:
            x_pos = x[i]
            i = i+1

        else:
            break
    
    return x_pos-x_neg
    
def findAiryDiskDiameter(screen, screenPlane, I, focalPoint):
    """returns the Airy disk diameter in two directions.    
    
    """
    screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(screen,focalPoint)

    AiryDisk = np.zeros([2,1])
    if 'xy' in screenPlane:
        AiryDisk[0] = AiryDiskDiameter(screen_1[idx1],I[idx1,:])
        AiryDisk[1] = AiryDiskDiameter(screen_2[:,idx2],I[:,idx2])
    if 'xz' in screenPlane:
        AiryDisk[0] = AiryDiskDiameter(screen_1[:,idx1],I[:,idx1])
        AiryDisk[1] = AiryDiskDiameter(screen_2[idx2],I[idx2])
    if 'yz' in screenPlane:
        AiryDisk[0] = AiryDiskDiameter(screen_1[:,idx1],I[:,idx1])
        AiryDisk[1] = AiryDiskDiameter(screen_2[idx2,:],I[idx2,:])
        
    return AiryDisk
    

def fitAiryFunction(data):
    pass