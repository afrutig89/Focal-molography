import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
from dipole.plots import plotParameter, findCoordinates
from matplotlib import colors
from mpl_toolkits.axes_grid1 import make_axes_locatable


def firePlotMologram(x,y,z,figure,filename):
    """Plots the field intensity at the scatterer location as a fire plot in the shape of the mologram."""

    x = x*1e6
    y = y*1e6
    ax = figure.add_subplot(111)
    ax.set_facecolor('black')
    rgb = np.asarray([z, np.zeros(len(z)), np.zeros(len(z))])
    rgb = rgb/np.max(rgb)
    ax.scatter(x,y,facecolors=np.transpose(rgb),lw=0,s=0.2)
    #ax.scatter(x,y,facecolors='k',lw=0,s=0.1,alpha=0.1)
    ax.axis('equal')
    ax.set_xlim([x.min(), x.max()])
    ax.set_ylim([y.min(), y.max()])
    ax.set_xlabel(r'x [$\mu$m]')
    ax.set_ylabel(r'y [$\mu$m]')

    plt.savefig(filename + '.png',dpi=600,format='png')

def scattererPlotMologram(x,y,figure,filename,size = 0.2,alpha = 0.1):
    """Plots the field intensity at the scatterer location as a fire plot in the shape of the mologram."""

    x = x*1e6
    y = y*1e6
    ax = figure.add_subplot(111)
    ax.set_facecolor('white')

    ax.scatter(x,y,facecolors='k',lw=0,s=size,alpha=alpha)
    ax.axis('equal')
    ax.set_xlim([x.min(), x.max()])
    ax.set_ylim([y.min(), y.max()])
    ax.set_xlabel(r'x [$\mu$m]')
    ax.set_ylabel(r'y [$\mu$m]')

    plt.savefig(filename + '.png',dpi=600,format='png')

# need a plot that plots scatterers of different types.

def scatteredIntensityPlot(I_sca,screen,focalPoint,figure,filename,save=True, colorbar=True):

    screen_1, axis1, idx1, screen_2, axis2, idx2, title, z = findCoordinates(screen, focalPoint)
            

        
    extent = (np.min(screen_1),np.max(screen_1),np.min(screen_2),np.max(screen_2))

    axes = plt.imshow(np.flip(I_sca,0), 
                    cmap = plt.cm.inferno,
                    origin = 'upper',
                    extent = [border*1e6 for border in extent],
                    norm = colors.PowerNorm(gamma=0.25))
    
    plt.xlabel(axis1)
    plt.ylabel(axis2)
    plt.axes().set_aspect('equal')
    
    if colorbar:
        ax = plt.gca()
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="5%", pad=0.1)
        print I_sca
        cbar = plt.colorbar(axes, cax = cax1, ticks=np.linspace(0,np.max(I_sca),4), format='%.0e')
        cbar.ax.tick_params(labelsize=10)
    
    if save:
        plt.savefig(filename + '.png',dpi=600,format='png')

