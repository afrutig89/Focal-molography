"""
ParetoList makes a list of all combination of input arrays or lists
The order is:
first change the last parameter and keep the rest constant.
than the second last and so on

:Author: Silvio Bischof
:Revised: 5-Jan-2017
"""

import itertools
import numpy as np


def paretoList(* args):
    """
    Returns all the combinations of arbitrary arguments of any type.
    """
    # pareto: all variations of the input parameters
    params = list(itertools.product(* instancesToLists(* args)))
    return params


def instancesToLists(* args):
    """
    Converts the arguments to lists if they are of another type
    """
    arguments = []

    if type(args) != list:
        args = list(args)

    for i in range(len(args)):
        if not hasattr(args[i], '__len__'):
            args[i] = tuple([args[i]])

    return args

if __name__ == '__main__':
    # parameter sweep
    a = np.linspace(1,5,5)
    b = 10.
    c = [1, 2, 3]

    params = paretoList(a,b,c)

    for a, b, c in params:
        print a+b-c

