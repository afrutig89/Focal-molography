# -*- coding: utf-8 -*-
"""
Class of electric fields. Methods that calculate the field and at 
phase for specific positions.

@ author:   Silvio
@ created:  Mar 2 14:44:28 2016

.. todo:: this module should be revised
"""

import numpy as np
from numpy import *
from numpy.linalg import norm

class electricPlaneWave:

    def __init__(self, E_0, k, r=0, f=0, t=0, alpha=0):
        """ plane wave
        
        :param E_0: amplitude of the electric field
        :param k: wave vector (direction of k is the direction of the phase velocity)
        :param r: position vector (cartesian coordinates)
        :param f: frequency
        :param t: time
        :param alpha: phase lead measured in radians
        
        """
        
        self.E_0 = E_0
        self.k = k
        self.r = r
        self.f = f
        self.t = t
        self.alpha = alpha
        self.E = E_0 * exp(1j*((norm(k)*norm(r))-2*pi*f*t-alpha))
        
def calcFieldPlaneXY(X, Y, r_0, ElField, alpha):
    """calculates the electric field on the xy-plane when the incident wave has an arbitrary angle

    :param X: x coordinates where the electric field has to be calculated at
    :param Y: y coordinates where the electric field has to be calculated at
    :param r_0: 2d vector (x,y) where full amplitude (ElField.E_0) is given
    :param ElField: object of the class electricPlaneWave with the incident field direction vec(k)/norm(k)
    :param alpha: propagation loss in db/cm
    :returns: A field vector in cartesian coordinates
    :rtype: float[3 x n]
    """
    
    r = np.vstack([np.reshape(X,(1,-1)),np.reshape(Y,(1,-1))])-r_0
    
    alpha_Np = alpha*log(10)/10.

    k = ElField.k[0:2]
    phi = arccos(sum(r*k,axis=0)/(norm(k)*sqrt(sum(r**2,axis=0))))
    
    r_k = sqrt(sum(r**2,axis=0)) * cos(phi)
    
    E_x = ElField.E_0[0] * exp(-(alpha_Np*r_k*1e2))
    E_y = ElField.E_0[1] * exp(-(alpha_Np*r_k*1e2))
    E_z = ElField.E_0[2] * exp(-(alpha_Np*r_k*1e2))
    
    E = np.array([E_x, E_y, E_z])

    return E

def calcPhasePlaneXY(X,Y,k):
    """calculates the electric field on the xy-plane when the incident 
    wave has an arbitrary angle (only relevant for molographic contexts 
    that do not use a waveguide).

    :param X: x coordinates where the phase has to be calculated at
    :param Y: y coordinates where the phase has to be calculated at
    :param k: the incident field direction vec(k)/norm(k)
    :returns: the phase at specific positions
    """
    
    X = np.reshape(X,(1,-1))
    Y = np.reshape(Y,(1,-1))

    # polar angle (k to the xy-plane)
    if not hasattr(k, '__len__'):
        theta = pi/2
        phi = 0
    else:
        theta = arccos(k[2]/norm(k))
        phi = arctan2(k[1],k[0])
    
    # angles to each point of the grid
    alpha = arctan2(Y,X)
    r_xy = sqrt(X**2 + Y**2) * cos(alpha-phi)

    # 3d distance
    r_phi = r_xy * sin(theta)

    phase = norm(k)*r_phi

    return phase

def test_electric_plane_wave():

    PW = electricPlaneWave(1.,np.asarray([1,0,0])*2*np.pi/632.8e-9)

def test_Phase_Plane():

    pass




if __name__ == '__main__':
    test_electric_plane_wave()