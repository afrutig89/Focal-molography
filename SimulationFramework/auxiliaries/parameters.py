# -*- coding: utf-8 -*-
"""
Collection of experimentally determined parameters

:author: 	Andreas Frutiger
:created:	Thu Nov 29 14:34:37 2017
"""

from numpy import pi,sqrt

#Not yet in use. 
		
a_ani = 0.186 # anisotropy of the waveguide scattering determined in experiment AF027 for a 145 nm Ta2O5 waveguide for 632.8 nm readoutwavelength and a 0.4 NA objective. 
b_nl = 33.2 # mean to maximum conversion. 		
