# -*- coding: utf-8 -*-
"""
This module contains functions that define the screen.

@ author:   Silvio
@ created:  Wed Apr 20 15:14:03 2016
"""

from numpy import *
import numpy as np

def rotateScreen(screen,theta_x,theta_y,offset_z):
    """
    rotates the screen in x- and y- direction.
    
    :param screen: screen (3d-array)
    :param theta_x: angle in x-direction
    :param theta_y: angle in y-direction
    :param offset_z: offset in z-direction
    """
    x,y,z = screen
    x_screen = cos(theta_y)*x + sin(theta_y)*(z-offset_z)
    y_screen = sin(theta_x)*sin(theta_y)*x + cos(theta_x)*y - sin(theta_x)*cos(theta_y)*(z-offset_z)
    z_screen = -cos(theta_x)*sin(theta_y)*x + sin(theta_x)*y + cos(theta_x)*cos(theta_y)*(z-offset_z) + offset_z
    
    screen = vstack([[x_screen],[y_screen],[z_screen]])
    return screen
    
def defineScreen(screenWidth, npix, screenPlane, screenRatio, screenRotation, focalPoint, center=(0,0)):
    """sets up a 2D-screen at the focal point depending on the axes defined.
    
    :param screenWidth: width of the screen (quadratic)
    :param npix: number of pixels for the screen
    :param screenPlane: definition of the screen ('xy','xz','yz')
    :param screenRatio: width ratio between axis 1 and axis 2
    :param screenRotation: rotation in x- and y-direction (2d-vector)
    :param focalPoint: distance from the source to the screen
    :param center: translation of the center of the screen
    """
    x_screen = center[0]
    y_screen = center[1]
    z_screen = focalPoint
    
    lim1 = screenWidth/2.
    lim2 = screenWidth/2./screenRatio
    
    if 'x' in screenPlane:
        x_screen = np.linspace(-lim1,lim1,npix) + center[0]
    if screenPlane[0] == 'y':
        y_screen = np.linspace(-lim1,lim1,npix) + center[1]
    if screenPlane[1] == 'y':
        y_screen = np.linspace(-lim2,lim2,npix) + center[1]
    if 'z' in screenPlane:
        z_screen = np.linspace(-lim2,lim2,npix) + focalPoint
                
    screen = np.vstack(np.meshgrid(x_screen,y_screen,z_screen)).reshape(3,npix,npix)
    screen = rotateScreen(screen,screenRotation[0],screenRotation[1],focalPoint)
    
    return screen
