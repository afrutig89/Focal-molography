# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 10:32:15 2016

This module is based on the paper 'A Statistical Method for
Determining and Comparing Limits of Detection of Bioassays'.
It is used to perform the limit of detection (LOD) analysis by 
means of statistical methods.

@author: Silvio
"""
    
import numpy as np
from numpy import exp, log
import numpy.random as npr
import numpy.matlib
import matplotlib.pyplot as plt
from scipy.optimize import leastsq
from scipy.stats import t
import math
    
def logistic4(x, a, b, c, d):
    """4PL lgoistic equation."""
    return ((a-d)/(1.0+((x/c)**b))) + d

def residuals(p, y, x):
    """Deviations of data from fitted 4PL curve"""
    a, b, c, d = p
    err = y-logistic4(x, a, b, c, d)
    return err

def peval(x, p):
    """Evaluated value at x with current parameters."""
    a, b, c, d = p
    return logistic4(x, a, b, c, d)
    
def gradfun(pars, x):
    c, d, b, e = pars
    dc = 1 - 1./(1 + exp(b*(log(x) - log(e))))
    dd = 1./(1 + exp(b*(log(x) - log(e))))
    db = -1*(d - c)*(log(x) - log(e))*exp(b*(log(x) - log(e)))/(1 + exp(b*(log(x) - log(e))))**2
    de = ((d - c)*b*exp(b*(log(x) - log(e)))/e)/(1 + exp(b*(log(x) - log(e))))**2
    y = np.vstack((dc,dd,db,de))
    return y.T

def gradfuninv(pars, x):
    c, d, b, e = pars
    dc = -1*(e*((d - x)/(x - c))**(1/b))/(b*(c - x))
    dd = e*((x - d)/(c - x))**(1/b)/(b*(d - x))
    db = -1*e*((x - d)/(c - x))**(1/b)*log((x - d)/(c - x))/(b**2)
    de = ((d - x)/(x - c))**(1/b)
    y = np.vstack((dc,dd,db,de))
    return y

def dfitfuninv(x, pars):
    c, d, b, e = pars
    y = (e*(c - d)*((x - d)/(c - x))**(1/b - 1))/(b*(c - x)**2)
    return y

def LODanalysis(test, negCtrl, testConc, alpha = 0.05, beta = 0.05):
    """
    :param test: m x n array of pixel intensities for the test data where n is the number of test concentrations and m the number of replicates per concentration
    :param negCtrl: m x 1 array of pixel intensities for the negative controls where m is the number of replicates of the negative control
    :param testConc: 1 x n array of the concentrations of analyte tested, corresponding to the data in the 'test' field
    :param alpha: desired probability of false positive
    :param beta: desired probability of false negative
    """
    # initialization
    numRepsTest, numConcentrations = test.shape

    testVar = np.var(test, axis=0)
    testConc_logplus2 = np.log10(testConc+2) # n x 1
    allTestData_pixInt = np.reshape(test,numConcentrations*numRepsTest,1) # 1 x n*m
    allTestData_testConc = np.reshape(np.matlib.repmat(testConc,numRepsTest,1),numConcentrations*numRepsTest,1) # 1 x n*m
    allTestData_testConc_logplus2 = np.reshape(np.matlib.repmat(testConc_logplus2,numRepsTest,1),numConcentrations*numRepsTest,1) # 1 x n*m
    allTestData_pixIntVar = np.reshape(np.matlib.repmat(testVar,1,numRepsTest),numConcentrations*numRepsTest,1) # 1 x n*m

    numReps_negCtrl = negCtrl.shape[1] # m
    negConc = np.zeros(numReps_negCtrl)
    negConc_logplus2 = np.log10(negConc+2)
    negCtrlVar = np.var(negCtrl)
    negConc_pixIntVar = negCtrlVar + np.zeros(numReps_negCtrl)
    
    allData_testConc = np.concatenate((negConc,allTestData_testConc), axis=0) # 1 x (n*m+1)
    allData_testConc_logplus2 = np.concatenate((negConc_logplus2,allTestData_testConc_logplus2), axis=0) # 1 x (n*m+1)
    allData_pixInt = np.concatenate((negCtrl[0],allTestData_pixInt)) # 1 x (n*m+1)
    allData_pixIntVar = np.concatenate((negConc_pixIntVar,allTestData_pixIntVar))

    numReps_Total = (numRepsTest*numConcentrations) + numReps_negCtrl

    # Calculate Upper Limit of the Negative Controls, L_c
    mu_c = np.mean(negCtrl)     # mean signal of the negative controls
    SD_c = np.std(negCtrl)      # standard deviation of the negative controls
    df_c = numReps_negCtrl-1       # degrees of freedom for negative controls = n-1
    t_c = t.ppf(1-alpha, df_c)  # t-multiplier for given alpha and df
    Lc = mu_c + t_c*SD_c        # Limit of the negative controls based on SD
    
    # Calculate Pooled SD of Test Concentrations, Determine Ld in Signal Space
    var_test_pooled = np.mean(testVar) # pooled variance for all test concentrations (assumes equal reps per concentration)
    SD_test = np.sqrt(var_test_pooled) # standard deviation of the pooled test data (assumes homoscedasticity)
    df_test = numConcentrations*(numRepsTest-1) # degrees of freedom for test data = nCon*(nReps-1) (per stats consulting)
    t_test = t.ppf(1-beta, df_test) # t-multiplier for given alpha and df
    t_test = np.nan_to_num(t_test)
    Ld = Lc + t_test*SD_test # Limit of detection in signal space based on SD

    # perform 4PL fit
    p0 = [1, 1, 1, 1] # Initial guess for parameters
    plsq = leastsq(residuals, p0, args=(allData_pixInt, allData_testConc_logplus2)) # Fit equation using least squares optimization
 
    # calculate LOD from fit
    a, b, c, d = plsq[0]
    logplus2_LOD = c*(((a-d)/(Ld-d)-1)**(1./b))
    LOD = 10**logplus2_LOD - 2;

    # Plot results
    plt.semilogx(allData_testConc,peval(allData_testConc_logplus2,plsq[0]),allData_testConc,allData_pixInt,'o')
    plt.semilogx(allData_testConc,np.zeros(len(allData_testConc_logplus2))+Lc,allData_testConc,np.zeros(len(allData_testConc_logplus2))+Ld)
    plt.axvline(LOD, linewidth=2, color='r')
    plt.xlabel('Concentration')
    plt.title('Least-squares 4PL fit to noisy data')
    plt.legend(['Fit', 'Noisy', 'Lc', 'Ld', 'LOD = ' + str(int(LOD))], loc='upper left')
    plt.savefig('LOD_analysis.png')
    
    return Lc, Ld, LOD
    
if __name__ == '__main__':
    # Make up some data for fitting and add noise
    # In practice, y_meas would be read in from a file
    n_concentrations = 7
    n_repetitions = 20
    concentrations = np.array([5,25,125,625,3125,15625,78125])
    A,B,C,D = -0.0002, 6.805, 3.776, 0.6989
    y_true = logistic4(np.log10(concentrations+2), A, B, C, D)
    y_meas = np.matlib.repmat(y_true,n_repetitions,1) + 0.01*npr.randn(n_repetitions,n_concentrations)
    negCtrl = np.array(0.02*npr.randn(n_repetitions,1)).T
    # plt.semilogx(concentrations,y_true)
    # plt.show()

    Lc, Ld, LOD = LODanalysis(y_meas, negCtrl, concentrations)
    print(Lc,Ld,LOD)