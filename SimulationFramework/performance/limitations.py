# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 19:22:48 2016

@author: silvi
"""
import numpy as np

def standardDeviationNumber(n,p):
    return np.sqrt(n*p*(1-p))
    
def standardDeviationIntensity(n,p):
    return standardDeviationNumber(n,p)**2
    
def limitOfDetectionI(n,p,E_ampl=1):
    I_BGP = meanIntensity(n,p,E_ampl)
    sigmaI_BGP = standardDeviationIntensity(n,p)
    return I_BGP + 3*sigmaI_BGP
    
def limitOfDetectionN(n,p,E_ampl=1):
    return np.floor(np.sqrt(limitOfDetectionI(n,p,E_ampl)))
    
def meanIntensity(n,p,E_ampl):
    return (2*p-1)*n*E_ampl**2
    
def criticalAngle(n_1,n_2):
    return np.arcsin(n_1/n_2)
