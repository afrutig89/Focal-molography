import numpy as np 
from math import factorial
from numpy import exp, sqrt
import sys
sys.path.append("../")
import auxiliaries.constants as constants

def statisticalTrial(p):
	x = np.random.rand()
	return (x<=p)

def relativeFrequencies(p, M, K):
	q = np.zeros(K+1)
	m = np.zeros(K+1)
	for i in range(int(M)):
		N = 0
		for k in range(K):
			N = N + statisticalTrial(p)
		q[N] = q[N]+1

	for k in range(K):
		m[k] = q[k]/M

	return m

def avgNoPoints(m):
	Nbar = sum(range(len(m))*m)
	return Nbar


def dispersion(m):
	Nbar = avgNoPoints(m)
	sigma = sum((Nbar-range(len(m)))**2 * m)
	return sigma

def powerPoisson(Nbar,T):
	return Nbar/T

def PoissonDistribution(N,Nbar):
	P = Nbar**N/(factorial(N))*exp(-Nbar)
	return P

def photocurrentDensity(I,q,wavelength):
	"""Calculates the average photocurrent density (central law of photo-effect)

	:params I: intensity
	:params q: quantum efficiency
	:params wavelength: incident wavelength
	"""
	f = constants.c/wavelength
	j = q*constants.q_e*I/(constants.h*f)
	return j

def numberOfPhotons(phi, pixelSize, exposureTime):
    """ number of photons arriving in a pixel depending on the flux of photons
    and the exposure time
    """
    return phi*pixelSize**2*exposureTime
    
def photoElectrons(phi, quantumEfficiency, pixelSize, exposureTime):
    n_p = numberOfPhotons(phi, pixelSize, exposureTime)
    n_e = quantumEfficiency * n_p
    return n_e
    
def meanPhotocurrent(I, wavelength, pixelSize, quantumEfficiency, exposureTime):
    phi = fluxOfPhotons(I,wavelength)
    n_e = photoElectrons(phi, quantumEfficiency, pixelSize, exposureTime)
    i_mean = constants.q_e * n_e / exposureTime
    return i_mean
    
def shotNoiseCurrent(I,wavelength,pixelSize,quantumEfficiency,exposureTime):
    """shot noise current (Schottky formula) of the detector
    """
    i_mean = meanPhotocurrent(I,wavelength,pixelSize,quantumEfficiency,exposureTime)
    i_sh = sqrt(2*constants.q_e*i_mean/exposureTime)
    return i_sh

def averageDensityOfQuanta(I,wavelength):
	f = constants.c/wavelength
	nbar = I/(constants.c*constants.h*f)
	return nbar

def fluxOfPhotons(I, wavelength):
    """calculates the flux of photons (photons per second and unit area)
    """
    f = constants.c/wavelength
    phi = I/(constants.h*f)
    return phi

def photonNoise(I, wavelength, exposureTime):
    phi = fluxOfPhotons(I,wavelength)
    return sqrt(phi*exposureTime)

if __name__ == '__main__':
	p_e = [0.0025, 0.025, 0.125] 		# probability
	M = 1e6 						# total number of repetitions
	K = 20 							# statistical trials

#	for p in p_e:
#		m = relativeFrequencies(p,M,K)
#		Nbar = avgNoPoints(m)
#		sigma = dispersion(m)
