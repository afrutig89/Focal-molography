"""
:created:       01-Feb-2017
:author:        Roland Dreyfus
:revised:       22-Feb-2017 by Roland Dreyfus
:references:    
"""

import sys
sys.path.append('../')
from auxiliaries.writeLogFile import loadDB
import matplotlib.pyplot as plt
from imageProcessing.figureFormat import *

import os
from PIL import Image, ImageDraw
import numpy as np
from skimage import filters

from matplotlib import colors
from matplotlib.colors import LogNorm
from matplotlib import gridspec
from matplotlib.colors import LightSource, Normalize
from scipy import optimize, interpolate

import time

class ImageAnalysis():
    """
    This is a class allows to perform image processing and analysis on 
    the image raw data generated by the moloreader.
        
    :param folder: Name of folder where images are saved.
    :param name_postfix: Name of postfix of the processed image. The image
                         is automatically saved.
    :param options_plot: Struct holding all the settings for generating the plots.
    """

    def __init__(self, folder, name_postfix, options_plot, parameters_plot):
    
        self.folder = folder
        self.name_postfix = name_postfix
        self.options_plot = options_plot
        self.parameters_plot = parameters_plot
        
        self.pixelSize = 2.2e-6
        self.magnification = 20
        self.n_s = 1.521     # refractive index substrate
        self.n_0 = 1.
        self.scalingFactor = self.n_0/self.n_s
        self.pixelWidth = self.pixelSize/self.magnification
        self.imageFormat = 'png'
        self.dpi = 600

    def loadImages(self,folder):
        """
        Loads images from a specified folder and saves them in 
        a list of arrays.
        """
        # load images and save in list
        self.filenames = os.listdir(self.folder)

        # (dirty bugfix)
        if self.filenames[0] == ".DS_Store":
            self.filenames = self.filenames[1:]
        if self.filenames[-1] == "Icon\r":
            self.filenames = self.filenames[0:-1]

        self.bragg_number = np.zeros(len(self.filenames))
        self.images = []
        i = 0
        maximum = 0

        for self.filename in self.filenames:
            self.imarray = np.array(Image.open(self.folder + self.filename))
            # rotate image in order to have coordinate system
            self.imarray = np.rot90(self.imarray,3)
            self.images.append(self.imarray)
            self.bragg_number[i] = float(str.split(self.filename,'_')[-1])
            if np.max(self.imarray) > maximum:
                # image with highest intensity
                maximum = np.max(self.imarray)
                self.strongest_image = i # focal plane
            i += 1

        # sort arrays
        self.sort = self.bragg_number.argsort()
        if len(self.sort) > 1:
            # if there are multiple images
            self.strongest_image = int(np.where(self.sort == self.strongest_image)[0])
            self.bragg_number = self.bragg_number[self.sort]
            self.images = np.array(self.images)
            self.images = self.images[self.sort]


    def createZStack(self, images, z_screen, focalImage):

        self.z_screen = np.zeros(len(self.filenames))
        self.images = []
        i = 0
        maximum = 0
        for self.filename in self.filenames:
            if self.filename.endswith(".avi"):
                # no image: reduce z-axis by one
                self.z_screen = self.z_screen[0:len(self.z_screen)-1]
                pass



            else:
                self.imarray = np.array(Image.open(self.folder + self.filename))
                # rotate image in order to have coordinate system
                self.imarray = np.rot90(self.imarray,3)
                self.images.append(self.imarray)
                # self.z_screen[i] = float(str.split(self.filename,'_')[-2])*1e-6
                if np.max(self.imarray) > maximum:
                    # image with highest intensity
                    maximum = np.max(self.imarray)
                    self.focalImage = i # focal plane
            i += 1



        # sort arrays
        self.sort = self.z_screen.argsort()
        if len(self.sort) > 1:
            # if there are multiple images
            self.focalImage = int(np.where(self.sort == self.focalImage)[0])
            self.z_screen = self.z_screen[self.sort]
            self.images = np.array(self.images)
            self.images = self.images[self.sort]
            # self.images_self.sorted = self.images
            # for i in xrange(0,len(self.sort)):
            #     self.images_self.sorted[i] = self.images[self.sort[i]]
            # self.images = self.images_sorted

            # adjust z-axis
            self.z_screen = self.z_screen - self.z_screen[self.focalImage]

        self.x_max = int(self.imarray.argmax()/self.imarray.shape[1])+1 # y-axis with the highest values
        self.y_max = self.imarray[self.x_max].argmax() # x-axis with the highest value

        # create z-stack
        self.I_meas_xz = np.zeros((len(self.images),self.images[0].shape[1]))
        
        row = 0
        for im in self.images:
            self.I_meas_xz[row] = im[self.x_max]
            row += 1
        
        # take account for multiple maxima (saturation)
        self.focalImage = self.focalImage + int(sum(self.I_meas_xz[:,self.y_max] == np.max(self.I_meas_xz[:,self.y_max]))/2)


    # @profile
    def otsuThresholding(self, Image_array):
        """
        Function performs Otsu's thresholding to an image. All pixels
        smaller than the threshold are set to zero.

        :param Image_aray: image to which thrsholding is applied

        :type Image_aray: array(float, 2d)

        :returns: thresholded image and threshold

        :rtype: array(float,2d), float
        """
        self.threshold = filters.threshold_otsu(self.I_meas_xy)

        self.mask_otsu = self.I_meas_xy > self.threshold
        self.Image_otsu = self.I_meas_xy*self.mask_otsu

        return self.Image_otsu, self.threshold

    # @profile
    def imageProcessing(self, image_to_process):
        """
        Performs the image processing: 
        - the image to be processed is chosen (e.g. strongest image)
        - the xy of the measured image is created
        - max x and y coordinates are detected based on which the axis are 
        defined (such that the maximum/focus is set at zero)
        - Otsu's thresholding is performed on the image
        - thresholding is applied 
        - arrays that contain the thresholded coordinates are created (x_coord,
        y_coord)
        """

        # self.image_to_process = self.strongest_image
        # create xy-plots
        print int(self.image_to_process)
        self.I_meas_xy = self.images[int(self.image_to_process)]

        print self.I_meas_xy.argmax(), self.I_meas_xy.shape
       
        self.x_max, self.y_max = np.unravel_index(self.I_meas_xy.argmax(), 
                                                  self.I_meas_xy.shape)

        # define screen
        self.x_screen = self.pixelWidth*(
                                np.arange(self.I_meas_xy.shape[0])
                                -self.y_max)
        self.y_screen = self.pixelWidth*(
                                np.arange(self.I_meas_xy.shape[1])
                                -self.x_max)
        # self.z_screen = self.pixelWidth*self.z_screen


        # Threshold image (Otsu's thresholding)
        self.I_meas_xy_threshold, self.threshold = self.otsuThresholding(
                                                        self.I_meas_xy)


        # index of pixels with intesity over threshold
        self.y_indx, self.x_indx = np.where(self.I_meas_xy_threshold > 0)


        # dots where intensity over threshold
        self.x_coord = self.x_screen[self.x_indx]
        self.y_coord = self.y_screen[self.y_indx]

        # dots where intensity over threshold
        self.x_coord = self.x_indx
        self.y_coord = self.y_indx

        

    def calc_R(self, c):
        """
        .. :note:: Use circleFit() for fitting a circle on a set of data points.
        This function is an auxiliary function to circleFit().
        
        Circle function for leastsquare fit.

        :param c: center

        :type c: array(float,1d)

        :returns: radius

        :rtype: float

        :reference:

        Adapted from: http://scipy-cookbook.readthedocs.io/items/Least_Squares_Circle.html, Method 2b, [20-Jan-17]
        """
        return np.sqrt((x_data-c[0])**2 + (y_data-c[1])**2)


    def f(self, c):
        """ 
        .. :note:: Use circleFit() for fitting a circle on a set of data points.
        This function is an auxiliary function to circleFit().

        Circle function for leastsquare fit.
        Calculates the algebraic distance between the 2D points and the mean 
        circle centered at c=(xc, yc).
        :param c: center

        :type c: array(float,1d)

        :returns: residus

        :rtype: float

        :reference:

        Adapted from: http://scipy-cookbook.readthedocs.io/items/Least_Squares_Circle.html, Method 2b, [20-Jan-17]
       """
        self.Ri = self.calc_R(c)
        return self.Ri - self.Ri.mean()


    def circleFit(self, x, y, xc_estimate=-500, yc_estimate=-500):
        """ 
        Least square fit of a circle. Starting point of center needs first to be 
        guessed (xc_estimate, yc_estimate). In case of bad estimated one might 
        get fall into a local minimum.
        :param x_data: x coordinates of data to be fitted
        :param y_data: y coordinates of data to be fitted

        :type x_data: array(float,1d)
        :type y_data: array(float,1d)

        :returns xc, yc, R: coordinates of center, radius
        :returns x_fit, y_fit: sampling of fitted circle with 180 points

        :rtype xc, yc, R: floats
        :rtype x_fit, y_fit: array(float,1d)

        :reference: Adapted from: http://scipy-cookbook.readthedocs.io/items/Least_Squares_Circle.html, Method 2b, [20-Jan-17]
        """
        global x_data
        global y_data
        x_data = x
        y_data = y

        self.center_estimate = xc_estimate, yc_estimate
        self.center, self.ier = optimize.leastsq(self.f, self.center_estimate)

        self.xc, self.yc = self.center
        self.Ri       = self.calc_R(self.center)
        self.R        = self.Ri.mean()

        self.theta_fit = np.linspace(-np.pi, np.pi, 180)
        self.x_fit = self.xc + self.R*np.cos(self.theta_fit)
        self.y_fit = self.yc + self.R*np.sin(self.theta_fit)



    # @profile
    def signalROI(self):
        """
        Defines signal region of interest (ROI). Creates a mask that cutts out the all the
        pixels that are not considered as signal.
        """

        self.circleFit(self.x_coord, self.y_coord)
        self.r_sig = 0.5*1e-6   # m

        self.n = len(self.x_screen)

        self.x = np.array([self.x_screen])
        self.y = np.transpose(np.array([self.y_screen]))

        self.mask_sig = (self.x-0)*(self.x-0) + (self.y-0)*(self.y-0)        \
                        >= self.r_sig*self.r_sig

        self.mask_array = np.zeros((self.n, self.n), dtype=bool)
        self.mask_array[self.mask_sig] = True
        self.I_sig_xy = np.ma.masked_array(
                                self.I_meas_xy, 
                                mask=self.mask_array)

    def backgroundROI(self):
        """
        Defines background region of interest (ROI). Creates a mask that cutts out the the
        pixels that are not considered as background.
        """
        self.circleFit(self.x_coord, self.y_coord)
        self.Delta_r = 1.5/self.pixelWidth    # um

        xc, yc = self.xc, self.yc # m
        self.r1 = (self.R+self.Delta_r)   # m
        self.r2 = (self.R-self.Delta_r)   # m
        self.r3 = 10/self.pixelWidth                     # m
        self.r4 = 60/self.pixelWidth                      # m


        self.mask1 = (self.x-xc)*(self.x-xc)                                \
                        + (self.y-yc)*(self.y-yc)                           \
                        >= self.r1*self.r1
        self.mask2 = (self.x-xc)*(self.x-xc)                                \
                        + (self.y-yc)*(self.y-yc)                           \
                        <= self.r2*self.r2
        self.mask3 = (self.x-0)*(self.x-0) + (self.y-0)*(self.y-0)          \
                        <= self.r3*self.r3
        self.mask4 = (self.x-0)*(self.x-0) + (self.y-0)*(self.y-0)          \
                        >= self.r4*self.r4

        self.n = len(self.x_screen)
        self.mask_array = np.zeros((self.n, self.n),  dtype=bool)
        # "Mask. Must be convertible to an array of booleans with the same 
        # shape as data. True indicates a masked (i.e. invalid) data."
        self.mask_array[self.mask1] = True
        self.mask_array[self.mask2] = True
        self.mask_array[self.mask3] = True
        self.mask_array[self.mask4] = True

        self.I_bckgrnd_xy= np.ma.masked_array(
                                self.I_meas_xy, 
                                mask=self.mask_array)

    def calcSNR(self):
        """
        Calculates signal to noise ration (SNR) and all the parameters needed
        to do so.
        """

        self.max_sig = np.ma.max(self.I_sig_xy)
        # np.ma.mean() in order to mean only the True values and ignore masked ones.
        self.mean_sig = np.ma.mean(self.I_sig_xy)
        
        self.mean_bckgrnd = np.ma.mean(self.I_bckgrnd_xy)
        self.std_bckgrnd = np.ma.std(self.I_bckgrnd_xy)
        self.var_bckgrnd = np.ma.var(self.I_bckgrnd_xy)

        self.sig_amp = self.mean_sig - (self.mean_bckgrnd+3*self.std_bckgrnd)

        self.SNR = self.sig_amp/self.std_bckgrnd

    def braggCondition(self):
        # load images
        self.loadImages(self.folder)

        self.bragg_foci = np.array([])
        for self.image_to_process in self.bragg_number-1:
            self.imageProcessing(self.image_to_process)
            self.signalROI()
            self.backgroundROI()
            self.calcSNR()

            # safe focus intensities of braggs
            self.bragg_foci = np.append(
                                self.bragg_foci,
                                np.array([self.mean_sig]), 
                                axis=0)
        plt.plot(self.bragg_number,self.bragg_foci)
        plt.axvline(x=8.5, color='k', linestyle='--')
        plt.axvline(x=16.5, color='k', linestyle='--')
        # plt.show()


    # @profile
    def createPlots(self):
        """
        Creates plots according to the settings defined in options_plot.
        """
        # self.loadImages(self.folder)
        # self.imageProcessing(self.strongest_image)
        # self.signalROI()
        # self.backgroundROI()
        # self.calcSNR()

        self.braggCondition()

        plt.close('all')
        if self.parameters_plot["formatOpticsLetters"]:
            formatOpticsLetters()
        if self.parameters_plot["style"]:
            style()

        self.colbNorm = self.parameters_plot["colbNorm"]
        self.alpha = 1

        if self.options_plot["measured"]:
            fig1 = plt.figure()
            CS = plt.imshow(self.I_meas_xy/(self.I_meas_xy.max()*1.),
                            cmap=plt.cm.inferno, 
                            extent=[np.min(self.x_screen)*1e6,np.max(self.x_screen)*1e6,
                                    np.min(self.y_screen)*1e6,np.max(self.y_screen)*1e6],
                            origin = 'lower',
                            norm = colors.PowerNorm(gamma=self.colbNorm)
                            )
                            
            cbar = plt.colorbar(CS, format='%.e')
            cbar.set_label(''.join(map(str,[u'Intensity normailzed to I_max = ',
                            self.I_meas_xy.max(),',\n color scaling ^',
                            self.colbNorm])), rotation=90)
            plt.xlabel(u'x [\u03bcm]')
            plt.ylabel(u'y [\u03bcm]')
            plt.autoscale(False)    # needs to be turned off because fitted circle is huge

        
        if self.options_plot["sig"]:
            fig1 = plt.figure()
            CS = plt.imshow(self.I_sig_xy/(self.I_sig_xy.max()*1.),
                            cmap=plt.cm.inferno, 
                            extent=[np.min(self.x_screen)*1e6,np.max(self.x_screen)*1e6,
                                    np.min(self.y_screen)*1e6,np.max(self.y_screen)*1e6],
                            origin = 'lower',
                            norm = colors.PowerNorm(gamma=self.colbNorm)
                            )
                            
            cbar = plt.colorbar(CS, format='%.e')
            cbar.set_label(''.join(map(str,[u'Intensity normailzed to I_max = ',
                            self.I_sig_xy.max(),',\n color scaling ^',
                            self.colbNorm])), rotation=90)
            plt.xlabel(u'x [\u03bcm]')
            plt.ylabel(u'y [\u03bcm]')
            plt.autoscale(False)    # needs to be turned off because fitted circle is huge
            

        if self.options_plot["bckgrnd"]:
            
            fig1 = plt.figure()
            CS = plt.imshow(self.I_bckgrnd_xy/(self.I_bckgrnd_xy.max()*1.),
                            cmap=plt.cm.inferno, 
                            extent=[np.min(self.x_screen)*1e6,np.max(self.x_screen)*1e6,
                                    np.min(self.y_screen)*1e6,np.max(self.y_screen)*1e6],
                            origin = 'lower',
                            norm = colors.PowerNorm(gamma=self.colbNorm)
                            )
                            
            cbar = plt.colorbar(CS, format='%.e')
            cbar.set_label(''.join(map(str,[u'Intensity normailzed to I_max = ',
                            self.I_bckgrnd_xy.max(),',\n color scaling ^',
                            self.colbNorm])), rotation=90)
            plt.xlabel(u'x [\u03bcm]')
            plt.ylabel(u'y [\u03bcm]')
            plt.autoscale(False)    # needs to be turned off because fitted circle is huge
            

        if self.options_plot["thresholded"]:
            fig1 = plt.figure()
            CS = plt.imshow(self.I_meas_xy_threshold,
                            cmap=plt.cm.Reds, 
                            extent=[np.min(self.x_screen)*1e6,np.max(self.x_screen)*1e6,
                                    np.min(self.y_screen)*1e6,np.max(self.y_screen)*1e6],
                            # norm = colors.PowerNorm(gamma=1./2.),
                            origin = 'lower')
            cbar = plt.colorbar(CS, format='%.e')

        if self.options_plot["dots"]:
            fig1 = plt.figure()
            # plt.plot(self.x_coord, self.y_coord,'.')
            plt.imshow((self.I_meas_xy_threshold > 0),
                         extent=[np.min(self.x_screen)*1e6,np.max(self.x_screen)*1e6,
                                 np.min(self.y_screen)*1e6,np.max(self.y_screen)*1e6],
                         origin = 'lower'
                        )

        if self.options_plot["circleFit"]:
            self.circleFit(self.x_coord, self.y_coord)
            plt.plot(
                # self.circle_fit_data["x_fit data points"], 
                # self.circle_fit_data["y_fit data points"],
                self.x_fit,
                self.y_fit,
                '--', 
                color ='blue',
                lw=2)
            #plt.plot([xc], [yc], 'o', label='fitted centre')
            

        if self.options_plot["histogram"]:
            fig2 = plt.figure(figsize=(5,4))
            CS = plt.hist(self.I_meas_xy)
            plt.axvline(x=threshold, color='b', linestyle='dashed', linewidth=2)
            plt.yscale('log')

            fig2.savefig(
            'RD_xy_experiment_measured_' + self.name_postfix + "." 
                + self.imageFormat, 
                format=self.imageFormat, 
                dpi=self.dpi)


        fig1.savefig(
            'RD_xy_experiment_measured_' + self.name_postfix + "." 
                + self.imageFormat, 
            format=self.imageFormat, 
            dpi=self.dpi)

        plt.show()
        
    


if __name__ == '__main__':

    # image settings
    # folder = '/Users/rolanddreyfus/Google Drive/Diffractometric Biosensing Subgroup/Experiments/RD003 (BG02 New mask alignment frame)/RD003_BG2_01_moloreader/without SAv/'
    folder = '/Users/rolanddreyfus/Google Drive/Diffractometric Biosensing Subgroup/Experiments/RD003 (BG02 New mask alignment frame)/RD003_BG2_01_moloreader/Images/'
    # folder = '/Users/rolanddreyfus/Google Drive/Diffractometric Biosensing Subgroup/Experiments/RD004 (BG02 quantifify bragg condition bandwidth)/RD_first_exp_bragg_cond/BG2_E_blaNH-Bi_NH-PEG_exp-1.5ms/'
    name_postfix = "B-L2-M2_exposure-15-msz_0.0"


    options_plot = {
        "measured":     1,
        "sig":          0,
        "bckgrnd":      1,
        "histogram":    0,
        "thresholded":  0,
        "circleFit":    0,
        "dots":         0,
    }

    parameters_plot = {
        "colbNorm":     0.25,
        "formatOpticsLetters": 0,
        "style": 1
    }



    iA = ImageAnalysis(
            folder=folder, 
            name_postfix=name_postfix, 
            options_plot=options_plot,
            parameters_plot=parameters_plot)

    iA.createPlots()

    
    

