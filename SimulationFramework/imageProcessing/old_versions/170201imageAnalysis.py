import sys
sys.path.append('../')
from auxiliaries.writeLogFile import loadDB
from auxiliaries.functions import findAiryDiskDiameter, AiryDiskDiameter
from dipole.plots import plotParameter
import matplotlib.pyplot as plt

import os
from PIL import Image
import numpy as np
from skimage import filters

from matplotlib import colors
from matplotlib.colors import LogNorm
from matplotlib import gridspec
from matplotlib.colors import LightSource, Normalize
from scipy import optimize

import time

def loadImages(folder):
    """
    Loads images from a specific folder and saves them in 
    a list of arrays (coordinate system: |_)
    """
    # load images and save in list
    filenames = os.listdir(folder)

    # (dirty bugfix)
    if filenames[-1] == "Icon\r":
        filenames = filenames[0:-1]

    z_screen = np.zeros(len(filenames))
    images = []
    i = 0
    maximum = 0
    for filename in filenames:
        if filename.endswith(".avi"):
            # no image: reduce z-axis by one
            z_screen = z_screen[0:len(z_screen)-1]
            pass
        else:
            imarray = np.array(Image.open(folder + filename))
            # rotate image in order to have coordinate system
            imarray = np.rot90(imarray,3)
            images.append(imarray)
            z_screen[i] = float(str.split(filename,'_')[-2])*1e-6
            if np.max(imarray) > maximum:
                # image with highest intensity
                maximum = np.max(imarray)
                focalImage = i # focal plane
        i += 1
    

    # sort arrays
    sort = z_screen.argsort()
    if len(sort) > 1:
        # if there are multiple images
        focalImage = int(np.where(sort == focalImage)[0])
        z_screen = z_screen[sort]
        images = np.array(images)
        images = images[sort]
        # images_sorted = images
        # for i in xrange(0,len(sort)):
        #     images_sorted[i] = images[sort[i]]
        # images = images_sorted

        # adjust z-axis
        z_screen = z_screen - z_screen[focalImage]

    return images, z_screen, focalImage

def createZStack(images, z_screen, focalImage):
    x_max = int(imarray.argmax()/imarray.shape[1])+1 # y-axis with the highest values
    y_max = imarray[x_max].argmax() # x-axis with the highest value

    # create z-stack
    I_meas_xz = np.zeros((len(images),images[0].shape[1]))
    
    row = 0
    for im in images:
        I_meas_xz[row] = im[x_max]
        row += 1
    
    print focalImage
    # take account for multiple maxima (saturation)
    focalImage = focalImage + int(sum(I_meas_xz[:,y_max] == np.max(I_meas_xz[:,y_max]))/2)

def otsuThresholding(Image_array):
    """
    Function performs Otsu's thresholding to an image. All pixels
    smaller than the threshold are set to zero.

    :param Image_aray: image to which thrsholding is applied
    :type: array(float, 2d)
    ...
    :returns: thresholded image and threshold
    :rtype: array(float,2d), float
    """
    threshold = filters.threshold_otsu(I_meas_xy)
    Image_otsu = I_meas_xy < threshold
    val_array = np.ones(3)*threshold

    Image_otsu = np.zeros((len(I_meas_xy[0:]),len(I_meas_xy[0:])))

    for i in xrange(0,Image_otsu.shape[0]):
        for j in xrange(0,Image_otsu.shape[1]):
            if I_meas_xy[i,j] < threshold:
                Image_otsu[i,j] = 0
            if I_meas_xy[i,j] >= threshold:
                Image_otsu[i,j] = I_meas_xy[i,j]

    return Image_otsu, threshold


# Circle fit
def calc_R(c):
    """
    Circle function for leastsquare fit.
    :param c: center
    :type c: array(float,1d)
    ...
    :returns: radius
    :rtype: float
    :reference:
    ...
    Adapted from: http://www.scipy.org/Cookbook/Least_Squares_Circle 
    [20-Jan-17], Method 2b
    """
    return np.sqrt((x_data-c[0])**2 + (y_data-c[1])**2)


def f(c):
    """ 
    Circle function for leastsquare fit.
    Calculates the algebraic distance between the 2D points and the mean 
    circle centered at c=(xc, yc).
    :param c: center
    :type c: array(float,1d)
    ...
    :returns: residus
    :rtype: float
    :reference:
    ...
    Adapted from: http://www.scipy.org/Cookbook/Least_Squares_Circle 
    [20-Jan-17], Method 2b
   """
    Ri = calc_R(c)
    return Ri - Ri.mean()

def circleFit(x, y, xc_estimate, yc_estimate):
    """ 
    Least square fit of a circle. Starting point of center needs first to be 
    guessed (xc_estimate, yc_estimate). In case of bad estimated one might 
    get fall into a local minimum.
    :param x_data: x coordinates of data to be fitted
    :type x_data: array(float,1d)
    :param y_data: y coordinates of data to be fitted
    :type y_data: array(float,1d)
    ...
    :returns xc, yc: coordinates of center
    :rtype: float
    :returns R: radius
    :rtype: float
    :returns x_fit, y_fit: sampling of fitted circle with 180 points
    :rtype: array(float,1d)
    ...
    :reference:
    Adapted from: http://www.scipy.org/Cookbook/Least_Squares_Circle 
    [20-Jan-17], Method 2b
    """
    global x_data
    global y_data
    x_data = x
    y_data = y

    center_estimate = x_mean-xc_estimate, y_mean-yc_estimate
    center, ier = optimize.leastsq(f, center_estimate)

    xc, yc = center
    Ri       = calc_R(center)
    R        = Ri.mean()

    theta_fit = np.linspace(-np.pi, np.pi, 180)
    x_fit = xc + R*np.cos(theta_fit)
    y_fit = yc + R*np.sin(theta_fit)

    return xc, yc, R, x_fit, y_fit
    


if __name__ == '__main__':

    # image settings
    folder = '/Users/rolanddreyfus/Google Drive/Diffractometric Biosensing Subgroup/Experiments/RD002 (BG01)/RD002_moloreader/images/'
    pixelSize = 2.2e-6
    magnification = 20
    n_s = 1.521     # refractive index substrate
    n_0 = 1.
    scalingFactor = n_0/n_s
    name_postfix = "B-L2-M2_exposure-15-msz_0.0"

    pixelWidth = pixelSize/magnification

    # plot settings
    font = {
        'family' : 'sans-serif',
        'sans-serif' : 'Arial',
        'weight' : 'normal',
        'size'   : 12
    }
    params = {
        'legend.fontsize':   8,
        'xtick.labelsize' : 10,
        'ytick.labelsize' : 10
    }

        plt.rcParams.update(params)
        plt.rc('font', **font)

    imageFormat = 'png'
    dpi = 100

    plots = {
        "experiments": 1,
        "measured": 1,
        "histogram":0,
        "thresholded": 0,
        "circleFit": 0,
        "mean of dots": 0,
        "dots": 0,
        "half between max and min of dots": 0,
        "focal spot": 0,
    }


    # load images
    images, z_screen, focalImage = loadImages(folder)

    # create xy-plots
    I_meas_xy = images[focalImage]
    I_max = np.max(I_meas_xy)
    
    npix_x, npix_y = I_meas_xy.shape
    x_max, y_max = np.unravel_index(I_meas_xy.argmax(), I_meas_xy.shape)

    # define screen
    x_screen = pixelWidth*(np.arange(I_meas_xy.shape[0])-y_max)
    y_screen = pixelWidth*(np.arange(I_meas_xy.shape[1])-x_max)
    z_screen = pixelWidth*z_screen


    # Threshold image (Otsu's thresholding)
    I_meas_xy_threshold, threshold = otsuThresholding(I_meas_xy)


    # index of pixels with intesity over threshold
    y_indx, x_indx = np.where(I_meas_xy_threshold > 0)

    # dots where intensity over threshold
    x_dots = x_screen[x_indx]
    y_dots = y_screen[y_indx]
    


    if plots["experiments"]:

        # figures
        plt.close('all')
        plt.style.use('seaborn-colorblind')
        colbNorm = 2.
        if plots["measured"]:
            fig1 = plt.figure(figsize=(5,4))
            CS = plt.imshow(I_meas_xy/(I_meas_xy.max()*1.),
                            cmap=plt.cm.inferno, 
                            extent=[np.min(x_screen)*1e6,np.max(x_screen)*1e6,
                                    np.min(y_screen)*1e6,np.max(y_screen)*1e6],
                            origin = 'lower',
                            norm = colors.PowerNorm(gamma=1/colbNorm)
                            )
                            # norm = colors.SymLogNorm(linthresh=1e-30, linscale=1e-30,
                            #         vmin=0.0, vmax=1.0)
            cbar = plt.colorbar(CS, format='%.e')
            cbar.set_label(''.join(map(str,[u'Intensity normailzed to I_max = ',
                            I_meas_xy.max(),',\n color scaling ^1/',
                            int(colbNorm)])), rotation=90)
            plt.xlabel(u'x [\u03bcm]')
            plt.ylabel(u'y [\u03bcm]')
            plt.autoscale(False)

        alpha = 1

        if plots["thresholded"]:
            CS = plt.imshow(I_meas_xy_threshold,
                            cmap=plt.cm.Reds, 
                            extent=[np.min(x_screen)*1e6,np.max(x_screen)*1e6,
                                    np.min(y_screen)*1e6,np.max(y_screen)*1e6],
                            # norm = colors.PowerNorm(gamma=1./2.),
                            origin = 'lower')
            cbar = plt.colorbar(CS, format='%.e')


        
        if plots["mean of dots"]:
            plt.plot(x_mean*1e6,y_mean*1e6, 'o', label='mean', alpha=alpha)
        
        if plots["half between max and min of dots"]:
            plt.plot(x_half*1e6,y_half*1e6, 'o', label='half', alpha=alpha)
        
        if plots["focal spot"]:
            plt.plot(0, 0, 'o', label='focal spot', alpha=alpha)

        if plots["dots"]:
            plt.plot(x_dots*1e6, y_dots*1e6,'.')

        if plots["circleFit"]:
            xc, yc, r, x_fit, y_fit = circleFit(x_dots, y_dots, 5000, 5000)
            plt.plot(x_fit, y_fit, '--', color ='grey',lw=2)
            # plt.plot([xc], [yc], 'o', label='fitted centre')
            plt.legend()

        if plots["histogram"]:
            fig3 = plt.figure(figsize=(5,4))
            CS = plt.hist(I_meas_xy)
            plt.axvline(x=threshold, color='b', linestyle='dashed', linewidth=2)
            plt.yscale('log')


        plt.legend()

        plt.show()
        fig1.savefig('RD_xy_experiment_measured_' + name_postfix + "." + imageFormat, format=imageFormat, dpi=dpi)
