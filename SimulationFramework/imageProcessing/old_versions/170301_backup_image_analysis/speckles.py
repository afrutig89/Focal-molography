import numpy as np
import scipy
import scipy.ndimage as ndimage
import scipy.ndimage.filters as filters
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import time
import sys
sys.path.append('../')
from auxiliaries.writeLogFile import loadDB, saveDB, createDB
from scipy.signal import fftconvolve


def averageSpeckleDimension(data, specklePositions):
    minimum = np.inf
    for i in range(len(specklePositions[0])):
        if minimum > data[specklePositions[1][i],specklePositions[0][i]]:
            minimum = data[specklePositions[1][i],specklePositions[0][i]]
    
    average = np.sum(data > minimum)/len(specklePositions[0])
    return average

def averageSpeckleSize(data, specklePositions):
    minimum = np.inf
    for i in range(len(specklePositions[0])):
        if minimum > data[specklePositions[1][i],specklePositions[0][i]]:
            minimum = data[specklePositions[1][i],specklePositions[0][i]]
    
    average = (np.sum(data[data > minimum])-minimum)/len(specklePositions[0])
    return average


def findSpeckles(data, neighborhoodSize, threshold=0):
    neighborhoodSize = np.max([neighborhoodSize, 2])
    data_max = filters.maximum_filter(data, neighborhoodSize)
    maxima = (data == data_max)
    data_min = filters.minimum_filter(data, neighborhoodSize)
    diff = ((data_max - data_min) > threshold)
    maxima[diff == 0] = 0

    labeled, num_objects = ndimage.label(maxima)
    specklePositions = np.array(ndimage.center_of_mass(data, labeled, range(1, num_objects+1)))

    return np.array(
                np.vstack(
                    [specklePositions[:, 1],
                    specklePositions[:, 0]]
                ), dtype=int)


if __name__ == '__main__':
    neighborhoodSize = 16
    threshold = 0

    speckleDict = {
        'N_particles' : 'integer',
        'coherentParticles' : 'integer',
        'coherentBinding' : 'real',
        'numberOfSpeckles' : 'integer',
        'speckleDensity' : 'real',
        'averageSize' : 'real',
        'averageHeight' : 'real'
    }

    databaseFile = '../database/backgroundAnalysisSmall.db'

    createDB(databaseFile, 'speckles', speckleDict)
    logs = loadDB('../database/backgroundAnalysis.db', 'log')
    results = loadDB('../database/backgroundAnalysis.db', 'results')

    speckleAnalysis = dict()
    i = 0
    for i in range(len(logs)):
        # statisticsDB(results[i], logs[i], 'statisticsSmall.db')
        try:
            if results[i]['I_sca'].all() == 0:
                continue
        except:
            break

        if str(logs[i]['N_particles']) not in speckleAnalysis.keys():
            speckleAnalysis[str(logs[i]['N_particles'])] = []

        N_particles = logs[i]['N_particles']
        coherentParticles = logs[i]['coherentParticles']
        coherentBinding = logs[i]['coherentBinding']

        specklePositions = findSpeckles(results[i]['I_sca'], neighborhoodSize, threshold)
        speckleAnalysis[str(logs[i]['N_particles'])].append(len(specklePositions))

        numberOfSpeckles = len(specklePositions[0])
        averageSize = averageSpeckleDimension(results[i]['I_sca'], specklePositions)
        averageHeight = averageSpeckleSize(results[i]['I_sca'], specklePositions)
        speckleDensity = numberOfSpeckles/logs[i]['screenWidth']**2

        saveDB(databaseFile, 'speckles', locals())

        # fig = plt.figure(figsize=(12,4))
        # plt.subplot(131)
        # plt.imshow(results[i]['I_sca'], origin='lower')

        # plt.subplot(132)
        # plt.imshow(results[i]['I_sca'], origin='lower')
        # plt.autoscale(False)
        # plt.plot(specklePositions[0], specklePositions[1], 'ro')

        # plt.subplot(133)
        # plt.plot(specklePositions[0], specklePositions[1], 'ro')
        # plt.xlim([0,results[i]['I_sca'].shape[0]])
        # plt.ylim([0,results[i]['I_sca'].shape[1]])

        # plt.tight_layout()
        # plt.show()

    # x_values = []
    # meanValues = []
    # minValues = []
    # maxValues = []

    # for key, value in speckleAnalysis.items():
    #     x_values.append(int(key))
    #     meanValues.append(np.mean(np.array(value)))
    #     maxValues.append(np.max(np.array(value)))
    #     minValues.append(np.min(np.array(value)))

    # sort = np.argsort(np.array(x_values))
    # x_values = np.array(x_values)[sort]
    # meanValues = np.array(meanValues)[sort]
    # minValues = np.array(minValues)[sort]
    # maxValues = np.array(maxValues)[sort]
    # yerr_top = maxValues-meanValues
    # yerr_bottom = meanValues-minValues

    # plt.errorbar(x_values,meanValues,yerr=[yerr_bottom, yerr_top])
    # plt.xscale("log", nonposx='clip')
    # plt.show()