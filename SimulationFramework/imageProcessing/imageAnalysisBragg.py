"""
:created:       01-Feb-2017
:author:        Roland Dreyfus
:revised:       28-Feb-2017 by Roland Dreyfus
:references:    
"""

import sys
sys.path.append('../')
import matplotlib.pyplot as plt

import os
from PIL import Image, ImageDraw
import numpy as np
from skimage import filters

from matplotlib import colors
from matplotlib.colors import LogNorm
from matplotlib import gridspec
from matplotlib.colors import LightSource, Normalize
from scipy import optimize, interpolate
from scipy.ndimage.filters import gaussian_filter


import time

class ImageAnalysisBragg():
    """
    This is a class allows to perform image processing and analysis on 
    the image raw data generated by the moloreader.
        
    :param folder: Name of folder where images are saved.
    :param name_postfix: Name of postfix of the processed image. The image
                         is automatically saved.
    :param options_plot: Struct holding all the settings for generating the plots.
    """

    def __init__(self,
                 pixelsize,
                 Airy_disk_radius,
                 image=[]
                ):
            
        self.image = image
        self.Airy_disk_radius = Airy_disk_radius
        
        # defines m-line properties
        self.radius_mline = 1200.0*1e-6        # m
        self.angle_mline = 180.0+35.0          # deg
        self.xc_mline = self.radius_mline*np.cos(self.angle_mline*np.pi/180)
        self.yc_mline = self.radius_mline*np.sin(self.angle_mline*np.pi/180)


        self.pixelSize = 2.2e-6
        self.magnification = 20
        self.pixelWidth = self.pixelSize/self.magnification
        self.imageFormat = 'png'
        self.dpi = 600

    
        

    def defineXYimageAndAxis(self, image_to_process):
        """
        Performs the image processing: 
        - the image to be processed is chosen (e.g. strongest image)
        - the xy of the measured image is created
        - max x and y coordinates are detected based on which the axis are 
        defined (such that the maximum/focus is set at zero)
        - Otsu's thresholding is performed on the image
        - thresholding is applied 
        - arrays that contain the thresholded coordinates are created (x_coord_thresh,
        y_coord_thresh)
        """

        # create xy-plots
        image_xy = image_to_process#[8]

        # reverse y axis to have origin in lower left and not upper right
        image_xy = image_xy[::-1]

        # rotate image
        # image_xy = np.rot90(image_xy,3)

        y_max, x_max = np.unravel_index(image_xy.argmax(), 
                                                  image_xy.shape)


        # define screen
        x_axis = self.pixelWidth*(np.arange(image_xy.shape[1])-x_max)
        y_axis = self.pixelWidth*(np.arange(image_xy.shape[0])-y_max)


        return image_xy, x_axis, y_axis


    def thresholdedImageToCoordinates(self, image_to_coord, x_axis, y_axis):
        """
        Takes a thresholded image as argument and returns two arrays. The arrays
        have the x and y coordinates of all non zero values.
        """
        # index of pixels with intesity over threshold
        y_indx, x_indx = np.where(image_to_coord > 0)

        # coordinates of points where intensity over threshold
        x_coord_thresh = self.x_axis[x_indx]
        y_coord_thresh = self.y_axis[y_indx]

        return x_coord_thresh, y_coord_thresh



    def otsuThresholding(self, image_to_be_otsu):
        """
        Function performs Otsu's thresholding to an image. All pixels
        smaller than the threshold are set to zero.

        :param Image_aray: image to which thrsholding is applied

        :type Image_aray: array(float, 2d)

        :returns: thresholded image and threshold

        :rtype: array(float,2d), float
        """

        # np.ma.MaskedArray.harden_mask(image_to_be_otsu)
        # gaussian filter
        # image_to_be_otsu = gaussian_filter(image_to_be_otsu, sigma=2)

        threshold = filters.threshold_otsu(image_to_be_otsu)
        mask_otsu = image_to_be_otsu > threshold

        mask_array = np.zeros(np.shape(mask_otsu), dtype=bool)
        mask_array[mask_otsu] = False

        image_otsu = np.ma.masked_array(
                                image_to_be_otsu, 
                                mask=mask_array)

        return image_otsu, threshold


    def maskSignalROI(
            self,
            x_axis, 
            y_axis,
            image_to_mask,
            r_sig_inner=5e-6,
            r_sig_outer=50e-6
            ):

        """
        Defines signal region of interest (ROI). Creates a mask that cutts out all 
        pixels inside the signal ROI. This function is used to mask the high
        intensity pixels in order to fit the circle on the m-line only.
        """

        x = np.array([self.x_axis])
        y = np.transpose(np.array([self.y_axis]))

        mask_sig_in = (x-0)*(x-0) + (y-0)*(y-0) <= r_sig_inner*r_sig_inner
        mask_sig_out = (x-0)*(x-0) + (y-0)*(y-0) >= r_sig_outer*r_sig_outer

        mask_array = np.ones(np.shape(image_to_mask), dtype=bool)
        mask_array[mask_sig_in] = None
        mask_array[mask_sig_out] = None

        image_sig_xy = image_to_mask*mask_array

        return image_sig_xy


    def maskPeak(
            self,
            x_axis, 
            y_axis,
            image_to_mask, 
            Airy_disk_radius
            ):
        """
        Defines peak region of interest (ROI). Creates a mask that cutts out the all the
        pixels that are not considered as peak.
        """
        r_peak = self.Airy_disk_radius*1e-6   # m

        x = np.array([self.x_axis])
        y = np.transpose(np.array([self.y_axis]))

        mask_peak = (x-0)*(x-0) + (y-0)*(y-0) >= r_peak*r_peak

        mask_array = np.zeros(np.shape(image_to_mask), dtype=bool)
        mask_array[mask_peak] = True

        image_peak_xy = np.ma.masked_array(
                                image_to_mask, 
                                mask=mask_array)
        return image_peak_xy

    def maskBackgroundROI(
            self, 
            x_axis, 
            y_axis,
            image_to_mask,
            xc_mline,
            yc_mline,
            radius_mline,
            Delta_r=2.5*1e-6, 
            r_bkg_inner=10.0*1e-6,
            r_bkg_outer=100.0*1e-6):
        """
        Defines background region of interest (ROI). Creates a mask that cutts out the the
        pixels that are not considered as background.
        Radius are in (um).
        """

        r1 = (radius_mline+Delta_r)             # m
        r2 = (radius_mline-Delta_r)             # m
        x = np.array([self.x_axis])
        y = np.transpose(np.array([self.y_axis]))


        mask1 = (x-xc_mline)*(x-xc_mline) + (y-yc_mline)*(y-yc_mline) >= r1*r1
        mask2 = (x-xc_mline)*(x-xc_mline) + (y-yc_mline)*(y-yc_mline) <= r2*r2
        mask3 = (x-0)*(x-0) + (y-0)*(y-0) <= r_bkg_inner*r_bkg_inner
        mask4 = (x-0)*(x-0) + (y-0)*(y-0) >= r_bkg_outer*r_bkg_outer

        mask_array = np.zeros(np.shape(image_to_mask),  dtype=bool)

        mask_array[mask1] = True
        mask_array[mask2] = True
        mask_array[mask3] = True
        mask_array[mask4] = True

        image_bckgrnd_xy = np.ma.masked_array(
                            image_to_mask, 
                            mask = mask_array
                            )
        return image_bckgrnd_xy

    def calcSNR(self, image_calc_SNR, x_axis, y_axis):
        """
        Calculates signal to noise ration (SNR) and all the necessary parameters.
        """

        # mask pixels outside of ROI of signal -> keep signal only
        image_sig_cutout = self.maskSignalROI(
                                self.x_axis, 
                                self.y_axis,
                                image_calc_SNR
                                )

        # # Threshold image (Otsu's thresholding)
        # image_xy_threshold, threshold = self.otsuThresholding(image_sig_cutout)


        # # Get coordinate arrays of non zero pixels of image. Circle fit 
        # # function requires coordinate arrays.
        # x_coord, y_coord = self.thresholdedImageToCoordinates(
        #                                     image_xy_threshold,
        #                                     self.x_axis, 
        #                                     self.y_axis
        #                                     )


        # mask pixels outside of ROI of peak -> keep peak only
        image_sig_xy = self.maskPeak(
                            self.x_axis, 
                            self.y_axis,
                            image_calc_SNR, 
                            self.Airy_disk_radius
                            )

        # mask pixels outside of ROI of background -> keep background
        image_bckgrnd_xy = self.maskBackgroundROI(
                                    self.x_axis, 
                                    self.y_axis,
                                    image_calc_SNR,
                                    self.xc_mline,
                                    self.yc_mline,
                                    self.radius_mline
                                    )


        # value of max pixel (focus)
        max_sig = np.ma.max(image_sig_xy)

        # np.ma.mean() in order to mean only the True values and ignore masked ones.
        mean_sig = np.ma.mean(image_sig_xy)
        
        # mean of background
        bkg_mean = np.ma.mean(image_bckgrnd_xy)

        # standard deviation of background
        bkg_std = np.ma.std(image_bckgrnd_xy)

        # variance of background
        var_bckgrnd = np.ma.var(image_bckgrnd_xy)

        # signal amplitude
        sig_amp = mean_sig - (bkg_mean+3*bkg_std)

        # signal to noise ratio
        SNR = sig_amp/bkg_std

        # plot background (masked areas are cut out)
        self.plotImage(image_bckgrnd_xy, self.x_axis, self.y_axis)
        self.drawCircle(self.xc_mline, self.yc_mline, self.radius_mline)



        return max_sig, mean_sig, bkg_mean, bkg_std, var_bckgrnd, SNR



    def calcAiryDisk(self, image_raw_data, lowSignalSearch=False):

        Airy_disk_radius = self.Airy_disk_radius

        # option in moloreader framework for low signals
        # not implemented yet
        if lowSignalSearch:
            pass
            
        # convert raw data to image array and define axis
        image_xy, self.x_axis, self.y_axis = self.defineXYimageAndAxis(image_raw_data)


        # calculate SNR and other realted parameters
        max_sig, mean_sig, bkg_mean, bkg_std, var_bckgrnd, SNR = self.calcSNR(
                                                                    image_xy, 
                                                                    self.x_axis, 
                                                                    self.y_axis,
                                                                    )
        
        # plot full image
        self.plotImage(image_xy, self.x_axis, self.y_axis)

        # plot m-line circle
        self.drawCircle(self.xc_mline, self.yc_mline, self.radius_mline)

        return mean_sig, bkg_mean, bkg_std 


    def braggCondition(self,images):
        bragg_foci = np.array([])
        for image in images:
            mean_sig, bkg_mean, bkg_std = self.calcAiryDisk(image)

            # safe focus intensities of braggs
            bragg_foci = np.append(
                                bragg_foci,
                                np.array([bkg_mean]), 
                                axis=0)

        fig2 = plt.figure(50)
        plt.plot(np.linspace(1,24,24),bragg_foci)

    def drawCircle(self, xc, yc, R):
        theta_fit = np.linspace(-np.pi, np.pi, 180)
        x_circle = xc + R*np.cos(theta_fit)
        y_circle = yc + R*np.sin(theta_fit)

        plt.plot(
            x_circle*1e6,
            y_circle*1e6,
            '--', 
            color ='blue',
            lw=2)


    def plotImage(self, image_to_plot, x_axis, y_axis):
        fig = plt.figure()
        colbNorm = 0.25

        CS = plt.imshow(
                image_to_plot/(image_to_plot.max()*1.),
                cmap=plt.cm.inferno, 
                extent=[np.min(self.x_axis)*1e6,np.max(self.x_axis)*1e6,
                        np.min(self.y_axis)*1e6,np.max(self.y_axis)*1e6],
                origin = 'lower',
                norm = colors.PowerNorm(gamma=colbNorm)
                )
                        
        cbar = plt.colorbar(CS, format='%.e')
        cbar.set_label(
                ''.join(map(str,[u'Intensity normailzed to I_max = ',
                image_to_plot.max(),',\n color scaling ^',
                colbNorm])), rotation=90)
        plt.xlabel(u'x [\u03bcm]')
        plt.ylabel(u'y [\u03bcm]')
        plt.autoscale(False)

        plt.scatter(x_axis[1319],y_axis[924])

        # # histogram
        # fig = plt.figure()
        # CS = plt.hist(image_to_be_otsu)
        # plt.axvline(x=threshold, color='b', linestyle='dashed', linewidth=2)
        # plt.yscale('log')
        
    


if __name__ == '__main__':

    def loadImagesFromFolder(folder):
        """
        Loads images from a specified folder and saves them in 
        a list of arrays.
        """
        # load images and save in list
        filenames = os.listdir(folder)

        # # (dirty bugfix)
        if filenames[0] == ".DS_Store":
            filenames = filenames[1:]
        if filenames[-1] == "Icon\r":
            filenames = filenames[0:-1]

        # filename = 'blaNH-Bi_NH-PEG_exp-10.0ms_1'

        bragg_number = np.zeros(len(filenames))

        images = []
        i = 0

        for filename in filenames:
            imarray = np.array(Image.open(folder + filename))
            images.append(imarray)
            bragg_number[i] = float(str.split(filename,'-')[-1])
            i += 1

        return images


    folder = '/Users/rolanddreyfus/Google Drive/Diffractometric Biosensing Subgroup/Experiments/RD004 (BG02 quantifify bragg condition bandwidth)/035_RD_automated_bragg_cond/'
    images = loadImagesFromFolder(folder)
    pixelsize = 1.1
    Airy_disk_radius = 0.5

    iA = ImageAnalysisBragg(
            pixelsize = pixelsize,
            Airy_disk_radius = Airy_disk_radius,
            image = images
            )

    lowSignalSearch = False

    # iA.calcAiryDisk(images, lowSignalSearch)
    iA.braggCondition(images)
    plt.show()

