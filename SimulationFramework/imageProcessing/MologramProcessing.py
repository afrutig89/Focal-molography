import numpy as np
from scipy import ndimage
from scipy.ndimage.filters import gaussian_filter, median_filter
# from Data_Processing.imageAnalysisBragg import ImageAnalysisBragg

def calc_IntAiryDisk(I_sig, I_bg,pixelsize = 0.110,  Airy_disk_radius = 0.5, lowSignalSearch=False):
	"""

	:param I_sig: I_sig is a numpy array of the total intensity distribution in the focal spot
	:param I_bg: I_bg is a numpy array of the total intensity distribution -40/-40 microns beside the focal spot. 
	:param pixelsize: size of one pixel in the I_sig in um (camera pixel size / magnification)
	:param Airy_disk_radius: radius of the signal spot (will be averaged) 

	"""
	I_sig = np.copy(I_sig)
	
	# calculate the maximum of the I_sig, which must be the molographic center I_sig.
	I_sig = I_sig.astype('float')

	try:
		if lowSignalSearch == True:

			img = I_sig.copy()
			img = median_filter(img, size=(int(2*Airy_disk_radius/pixelsize),int(2*Airy_disk_radius/pixelsize)))

			i, j = np.unravel_index(img.argmax(),img.shape)
			center = j, i # I_sig processing: different coordinate system

			print('low signal detection: mologram at (x,y) = ' + str(center))


		else:
			i, j = np.unravel_index(I_sig.argmax(),I_sig.shape)
			center = j, i # I_sig processing: different coordinate system

	except:
		# empty I_sig
		return 0, 0, 0, 0

	# cut an I_sig area with a radius of 40 microns. 
	
	# calculate mean and standard deviation
	bkg_mean = np.mean(I_bg)
	bkg_std = np.std(I_bg)

	if Airy_disk_radius == 0:
		mologram_intensity = float(np.max(I_sig)) - bkg_mean

	else:
		# substract the background + 3 times the standard deviation of the molographic focal spot. 
		signal_image = I_sig - bkg_mean # - 3*bkg_std
		
		# sum up the signal in a disk with the given Airy disk radius.
		mask_signal = c_mask(signal_image,center,0,int(Airy_disk_radius/pixelsize))

		mologram_intensity = np.sum(signal_image[mask_signal])/np.sum(mask_signal)

	if mologram_intensity < 0:
		mologram_intensity = 0
	
	SNR = (mologram_intensity- 3*bkg_std ) / bkg_std

	return mologram_intensity, bkg_mean, bkg_std, SNR


def c_mask(image, centre, radius1, radius2):
	"""
	Return a boolean mask for a donat with two radii specified by radius1 (inner radius) and radius2 (outer radius) 
	"""

	x, y = np.ogrid[:image.shape[0],:image.shape[1]]
	cy, cx = centre

	# circular mask
	circmask1 = (x-cx)**2 + (y-cy)**2 >= radius1**2
	circmask2 = (x-cx)**2 + (y-cy)**2 <= radius2**2

	return np.logical_and(circmask1,circmask2)


def calc_datapoint_entry(img, algorithm,  pixelsize = 0.110, Airy_disk_radius=0.5, lowSignalSearch=False):
	
	if algorithm == 'sum over background plus 3 sigma (filter)':
		img = gaussian_filter(img, sigma=5)
		mologram_intensity, bkg_mean, bkg_std, SNR = calc_IntAiryDisk(img, pixelsize = pixelsize, lowSignalSearch=lowSignalSearch)

	elif algorithm == 'maximum over background plus 3 sigma':
		mologram_intensity, bkg_mean, bkg_std, SNR = calc_IntAiryDisk(img, pixelsize = pixelsize, Airy_disk_radius=0, lowSignalSearch=lowSignalSearch)

	elif algorithm == 'Bragg':
		# pixelsizeCamera = self.camera.info['pixel_size']*1e-6
		# magnification = self.main_widget.setup.camera.specifications.magnification
		imagAnalBragg = ImageAnalysisBragg(
			image = img,
			# pixelsizeCamera = pixelsizeCamera,
			# magnification =magnification,
			# airy_disk_radius = Airy_disk_radius
			)
		mologram_intensity, bkg_mean, bkg_std, SNR = imagAnalBragg.calcAiryDisk(img)
		pass


	else: # algorithm == 'sum over background plus 3 sigma':
		mologram_intensity, bkg_mean, bkg_std , SNR = calc_IntAiryDisk(img, pixelsize = pixelsize, lowSignalSearch=lowSignalSearch)

	if SNR < 0:
		SNR = 0
	return mologram_intensity, bkg_mean, bkg_std, SNR