import sys

import numpy as np
import matplotlib.pyplot as plt

sys.path.append("../../")

def formatOpticsLetters(figsizex = 2,figsizey = 2, spines_top_left=False):
        plt.rcParams['font.sans-serif']='Arial'
        plt.rcParams['font.size']= 8
        plt.rcParams['figure.figsize']=5.25/figsizex,4.75/figsizey
        plt.rcParams['axes.labelpad']= 0
        plt.rcParams['figure.dpi']= 600
        plt.rcParams['figure.autolayout']= True
        plt.rcParams['legend.fontsize'] = 8
        plt.rcParams['axes.color_cycle'] = ['#332288','#88CCEE','#44AA99','#117733',
                                            '#999933','#DDCC77','#CC6677','#882255',
                                            '#AA4499']

def style():
    plt.style.use('seaborn-colorblind')

def pltRemoveSpineTopRight():
    # remove spine top right
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    plt.gca().get_xaxis().tick_bottom()
    plt.gca().get_yaxis().tick_left()

def pltMaxTicks(xMax=5,yMax=5):
    # control tick number
    max_yticks = yMax
    yloc = plt.MaxNLocator(max_yticks)
    plt.gca().yaxis.set_major_locator(yloc)
    max_xticks = xMax
    xloc = plt.MaxNLocator(max_xticks)
    plt.gca().xaxis.set_major_locator(xloc)

def pltYlabelToTop():
    plt.gca().yaxis.set_label_coords(-0.025,1.05)
    plt.gca().xaxis.set_label_coords(0.5,-0.2)

def pltAdjustAll(xMaxTicks=None,yMaxTicks=None):
    pltRemoveSpineTopRight()
    if xMaxTicks!=None or yMaxTicks!=None:
        pltMaxTicks(xMaxTicks,yMaxTicks)
    pltYlabelToTop()



if __name__ == '__main__':
    
    # Example plot
    formatOpticsLetters()
    x = np.linspace(0, 10, 500)
    dashes = [10, 5, 100, 5]  # 10 points on, 5 off, 100 on, 5 off

    fig, ax = plt.subplots()
    line1, = ax.plot(x, np.sin(x), '--', linewidth=2)
    line1.set_dashes(dashes)

    line2, = ax.plot(x, -1 * np.sin(x), dashes=[30, 5, 10, 5])

    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)

    plt.savefig('test.png',dpi=600,format='png')
    plt.show()