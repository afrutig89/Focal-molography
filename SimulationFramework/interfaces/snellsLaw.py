# -*- coding: utf-8 -*-
"""
This module contains functions that returns the angle for refraction and for reflection for a given incident angle and different refractive indices.

@author: Silvio
"""
from numpy import sin, arcsin

def refractionAngle(theta_in,n_1,n_2):
    """returns the refraction angle according to Snells law
    
    :param theta_in: incident wave angle
    :param n_1: refractive index of medium 1
    :param n_2: refractive index of medium 2
    """
    # equation 4.16 Marchenko
    
    theta_t = arcsin(n_1/n_2 * sin(theta_in))
    
    return theta_t
    

def reflectionAngle(theta_in):
    """returns the reflection angle according to Snells law
    
    :param theta_in: incident angle
    """
    # equation 4.15 Marchenko

    return float(theta_in)