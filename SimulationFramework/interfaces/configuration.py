# -*- coding: utf-8 -*-
"""
With this module, a configuration of different media can be defined. 
It also containes a function to calculate the effective focal point 
that will be measured in praxis.

@ author: Silvio Bischof

.. todo:: class isotropic material should be in materials.py (delete completely!)
"""

from numpy import *
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import datetime
import sys
from auxiliaries.constants import *
import numpy as np

class Configuration():
    """Struct holding the waveguide and the media (thickness and refractive indices)."""

    def __init__(self,media,waveguide):

        self.Media = media
        self.waveguide = waveguide


class Media(object):
    def __init__(self,z,media):
        """Class for handling layered dielectric materials of different refractive indices and thickness.
        
        :param z: z-coordinates of the optical interfaces (array)
        :param media: array of the corresponding media (e.g. media1 is in z<z1, media2 is in z1<z<z2 etc.)
        """
        if (len(z) != len(media)+1):
            return False

        for i in range(len(media)):
            media[i].boundaries = [z[i],z[i+1]]
            
        self.media = media
        
    def returnMedium(self,z):
        """returns the medium at z
        Note: will only return one medium exactly at the interface
        """

        for medium in self.media:
            if(z >= min(medium.boundaries[0],medium.boundaries[1]) and z <= max(medium.boundaries[0],medium.boundaries[1])):
                return medium
                
        return None

    def returnRefractiveIndices(self):
        """returns a numpy array of the refractive indices of the different media."""

        return np.array([medium.n for medium in self.media])
        
    def returnLayerThickness(self,i=2):
        """returns the thickness of the i-th medium-layer
        """
        return abs(self.media[i-1].boundaries[1]-self.media[i-1].boundaries[0])
        
       
    def display(self, dpi=None):
        """
        Displays the configuration defined. The color of the different 
        media corresponds to the refractive index of such.
        """

        n_max = 1.0001
        d_min = inf
        for medium in self.media:
            n_max = max(n_max,medium.n)
            d_min = min(d_min,abs(medium.boundaries[1]-medium.boundaries[0]))
            
        fig = plt.figure()
        ax = fig.add_subplot(111)
        if self.media[0].boundaries[0] == -inf:
            for medium in self.media:
                d = min(abs(medium.boundaries[1]-medium.boundaries[0]),5*d_min)
                p = patches.Rectangle(
                    (0,max(medium.boundaries[0],-5*d_min)), 1, d,
                    color = 'green',
                    alpha = (medium.n-1)/(n_max-1)
                )
                ax.add_patch(p)
                ax.text(0.5,max(medium.boundaries[0],-5*d_min)+d_min/5,medium.name + ' (n = ' + str(medium.n) + ')', horizontalalignment='center')
        else:
            for medium in self.media:
                d = min(abs(medium.boundaries[1]-medium.boundaries[0]),5*d_min)
                p = patches.Rectangle(
                    (0,max(medium.boundaries[1],-5*d_min)), 1, d,
                    color = 'green',
                    alpha = (medium.n-1)/(n_max-1)
                )
                ax.add_patch(p)
                ax.text(0.5,max(medium.boundaries[1],-5*d_min)+d_min/5,medium.name + ' (n = ' + str(medium.n) + ')', horizontalalignment='center')

        
        ax.set_ylim(-5*d_min,medium.boundaries[0]+d)
        ax.set_ylabel('z')
        ax.get_xaxis().set_ticks([])
        fig.show()

        if dpi != None:
            fig.savefig('/configuration.png', format='png', dpi=dpi)
        
    def save(self, dpi=100):
        self.display(dpi)
        

          
def calculateFocalPoint(D_molo, delta_s, n_s, n_0, focalPoint_0):
    """ calculates the effective focal point considering an optical
    interface (glass substrate finite size)

    :param D_molo: diameter of the mologram
    :delta_s: thickness of the glass substrate
    :param n_s: refractive index of the substrate
    :param n_0: refractive index of the medium below the substrate
    :param focalPoint_0: initially calculated focal point
    """

    # incident angle from substrate
    theta_1 = np.arctan((D_molo/2.)/focalPoint_0)

    # snells law
    theta_2 = np.arcsin(n_s/n_0*sin(theta_1))

    focalPoint_med1 = delta_s-(D_molo/2.-delta_s*np.tan(theta_1))/np.arctan(theta_1)
    focalPoint_med2 = delta_s+(D_molo/2.-delta_s*np.tan(theta_1))/np.arctan(theta_2)

    return (focalPoint_med1, focalPoint_med2)

if __name__ == "__main__":
    print 'Hello'