# -*- coding: utf-8 -*-
"""
Calculation of the different Fresnel coefficients.

:author:    Silvio
:created:  Mon Mar 28 20:09:12 2016

:reference: Diffraction of Light. In Demonstrational Optics, 
Springer US, 2007. DOI: 10.1007/978-0-387-68327-0 2.
"""
import numpy as np
from numpy import *
from numpy.linalg import norm
import cmath
import matplotlib.pyplot as plt
from snellsLaw import *


def reflectionCoefficient(k_0, theta, refractiveIndices):
    """Fresnel reflection coefficient
    
    :param k_0: wave vector in free space
    :param eps_1: permittivity of medium 1
    :param eps_2: permittivity of medium 2
    :param mu_1: permeability of medium 1
    :param mu_2: permeability of medium 2
    """
    # equation 2.29 Novotny

    eps_r = refractiveIndices**2
    mu_r = np.ones(len(refractiveIndices))
    n = refractiveIndices
    
    k_1 = n[0] * k_0
    k_2 = n[1] * k_0
    n_tilde = n[0]/n[1]

    k_z1 = k_1 * np.sqrt(1-sin(theta)**2)
    k_z2 = k_2 * np.sqrt(1-n_tilde**2*sin(theta)**2+0j)

    r_p = (eps_r[1]*k_z1-eps_r[0]*k_z2)/(eps_r[1]*k_z1+eps_r[0]*k_z2)
    r_s = (mu_r[1]*k_z1-mu_r[0]*k_z2)/(mu_r[1]*k_z1+mu_r[0]*k_z2)
    
    return r_p, r_s
    
def transmissionCoefficient(k_0, theta, refractiveIndices):
    """Fresnel transmission coefficient
    
    """
    # equation 2.30 Novotny

    eps_r = refractiveIndices**2
    mu_r = np.ones(len(refractiveIndices))
    n = refractiveIndices
    
    k_1 = n[0] * k_0
    k_2 = n[1] * k_0
    n_tilde = n[0]/n[1]
    k_z1 = k_1 * np.sqrt(1-sin(theta)**2)
    k_z2 = k_2 * np.sqrt(1-n_tilde**2*sin(theta)**2+0j)
 
    t_p = (2*eps_r[1]*k_z1)/(eps_r[1]*k_z1+eps_r[0]*k_z2) * sqrt((mu_r[1]*eps_r[0])/(mu_r[0]*eps_r[1]))
    t_s = (2*mu_r[1]*k_z1)/(mu_r[1]*k_z1+mu_r[0]*k_z2)
    
    # total internal reflection
    t_p = np.nan_to_num(t_p)
    t_s = np.nan_to_num(t_s)
    
    return t_p, t_s
     
def reflectionCoefficientSingleLayer(d, k_0, theta, refractiveIndices):
    """returns the parallel and perpendicular reflection coefficient of a single layer of thickness d (r_p, r_s)
    configuration setup:
    
    medium1 | medium2 | medium3
        inf | <- d -> | -inf
       
    :param d: thickness of the layer
    :mu eps_x: permittivity of the corresponding medium x
    :param mu_x: permeability of the corresponding medium x
    """

    # equation 10.20 Novotny

    n = refractiveIndices

    theta_2 = refractionAngle(theta, n[0], n[1])
    
    r_p12, r_s12 = reflectionCoefficient(k_0, theta, refractiveIndices[0:2])
    r_p23, r_s23 = reflectionCoefficient(k_0, theta_2, refractiveIndices[1:3])

    k_2 = n[1] * k_0
    n_tilde = n[0]/n[1]
    k_z2 = k_2 * np.sqrt(1-n_tilde**2*sin(theta)**2+0j)
    
    r_p = (r_p12+r_p23*exp(2j*k_z2*d))/(1+r_p12*r_p23*exp(2j*k_z2*d))
    r_s = (r_s12+r_s23*exp(2j*k_z2*d))/(1+r_s12*r_s23*exp(2j*k_z2*d))

    return r_p, r_s
    
def transmissionCoefficientSingleLayer(d, k_0, theta, refractiveIndices):
    """transmission coefficient of a single layer of thickness d
    
    configuration setup:
    
    medium1 | medium2 | medium3
       inf  | <- d -> | -inf
       
    :param d: thickness of the layer
    :mu eps_x: permittivity of the corresponding medium x
    :param mu_x: permeability of the corresponding medium x
    """    
    # equation 10.21 Novotny
    
    n = refractiveIndices

    theta_2 = refractionAngle(theta, n[0], n[1])
    
    t_p12, t_s12 = transmissionCoefficient(k_0, theta, refractiveIndices[0:2])
    t_p23, t_s23 = transmissionCoefficient(k_0, theta_2, refractiveIndices[1:3])
    
    r_p12, r_s12 = reflectionCoefficient(k_0, theta, refractiveIndices[0:2])
    r_p23, r_s23 = reflectionCoefficient(k_0, theta_2, refractiveIndices[1:3])
    
    k_2 = n[1] * k_0
    n_tilde = n[0]/n[1]
    k_z2 = k_2 * np.sqrt(1-n_tilde**2*sin(theta)**2+0j)
    
    t_p = (t_p12*t_p23*exp(1j*k_z2*d))/(1+r_p12*r_p23*exp(2j*k_z2*d))
    t_s = (t_s12*t_s23*exp(1j*k_z2*d))/(1+r_s12*r_s23*exp(2j*k_z2*d))

    return t_p, t_s
    
def brewsterAngleTE(n1, n2):
    """calculates the Brewster angle (field is fully transmitted). Valid for parallel polarization (TE)
    
    :param n1: refractive index of the first medium
    :param n2: refractive index of the second medium
    """
    # equation 4.34 Marchenko
    
    return arctan(n2/n1)
    
def criticalAngle(n1,n2):
    """calculates the critical angle, i.e. the angle of incidence above which total internal reflection occurs.
    
    :param n1: refractive index of the first medium
    :param n2: refractive index of the second medium
    """
    # equation 4.20 Marchenko

    return arcsin(n2/n1)
    
def returnWaveVectors(k_0, theta, n_1, n_2):
    theta1 = theta
    k_10x = -norm(k_0)*sin(theta1)
    k_10z = norm(k_0)*cos(theta1)
    k_10 = [k_10x,np.zeros(theta.shape),k_10z]
    
    theta2 = refractionAngle(theta,n_1,n_2)
    k_20x = -norm(k_0)*sin(theta2)
    k_20z = norm(k_0)*cos(theta2)
    k_20 = [k_20x,np.zeros(theta.shape),k_20z]
    
    return k_10, k_20, theta1, theta2
    
def plotCoefficients(theta, **kwargs):
    if kwargs is not None:
        for key, value in kwargs.iteritems():
            plt.figure()
            plt.plot(theta/pi*180,value)
            plt.title(key)
            plt.xlabel('incident angle in degrees')
            
if __name__ == '__main__':
    print 900e-6*np.arctan(criticalAngle(2.117,1.54)/np.pi*180)

    folder = 'plots/parameters'
    figSizeDouble = (17/2.54,8.5/2.54)
    # plot settings
    font = {'family' : 'Arial',
    	'weight' : 'normal',
    	'size'   : 12
    }
    params = {'legend.fontsize':   8,
    	  'xtick.labelsize' : 10,
    	  'ytick.labelsize' : 10
    			}
    plt.rcParams.update(params)
    plt.rc('font', **font)
    legend = False
    title = False
    savePlots = False
    imageFormat = 'pdf'
    savePlots = False
    
    k_0 = 2*pi/635e-9
    refractiveIndices = np.array([1.33, 2.117, 1.521])
    theta = np.linspace(0,90,100)/180.*pi
    delta = 145e-9
    
    t_p, t_s = transmissionCoefficientSingleLayer(delta, k_0, theta, refractiveIndices)
    r_p, r_s = reflectionCoefficientSingleLayer(delta, k_0, theta, refractiveIndices)
    
    fig1 = plt.figure(figsize = figSizeDouble)
    plt.subplot(121)
    plt.plot(theta/pi*180, t_p.real, label="parallel polarization")
    plt.plot(theta/pi*180, t_s.real, label="perpendicular polarization")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Real Part')
    if legend: plt.legend()
    plt.subplot(122)
    plt.plot(theta/pi*180, t_p.imag, label="parallel polarization")
    plt.plot(theta/pi*180, t_s.imag, label="perpendicular polarization")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Imaginary Part')
    if legend: plt.legend()
    if title: plt.suptitle('Transmission Coefficients for a Single Layer', y=1.03, fontsize=fontSizeTitle)
    plt.tight_layout()

    plt.show()
    if savePlots:
        fig1.savefig('../' + folder + '/transmissionCoef.' + imageFormat, format=imageFormat, dpi=dpi)
    
    fig2 = plt.figure(figsize = figSizeDouble)
    plt.subplot(121)
    plt.plot(theta/pi*180,r_p.real, label="parallel polarization")
    plt.plot(theta/pi*180,r_s.real, label="perpendicular polarization")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Real Part')
    if legend: plt.legend()
    plt.subplot(122)
    plt.plot(theta/pi*180,r_p.imag, label="parallel polarization")
    plt.plot(theta/pi*180,r_s.imag, label="perpendicular polarization")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Imaginary Part')
    if legend: plt.legend()
    if title: plt.suptitle('Reflection Coefficients for a Single Layer', y=1.03, fontsize=fontSizeTitle)
    plt.tight_layout()
    
    plt.show()
    if savePlots:
        fig2.savefig('../' + folder + '/reflectionCoef.' + imageFormat, format=imageFormat, dpi=dpi)
        
        
    fig2 = plt.figure(figsize = figSizeDouble)
    plt.subplot(121)
    plt.plot(theta/pi*180,abs(t_p), label="parallel polarization")
    plt.plot(theta/pi*180,abs(t_s), label="perpendicular polarization")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Transmission')
    if legend: plt.legend()
    plt.subplot(122)
    plt.plot(theta/pi*180,abs(r_p), label="parallel polarization")
    plt.plot(theta/pi*180,abs(r_s), label="perpendicular polarization")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Reflection')
    if legend: plt.legend()
    if title: plt.suptitle('Absolute Values of Fresnel Coefficients for a Single Layer', y=1.03, fontsize=fontSizeTitle)
    plt.tight_layout()
   
    if savePlots:
        fig2.savefig('../' + folder + '/absoluteCoefficients.' + imageFormat, format=imageFormat, dpi=dpi)
     
    # comparison with and w/o layer
    t_p13, t_s13 = transmissionCoefficient(k_0, theta, refractiveIndices[[0,2]])    
    r_p13, r_s13 = reflectionCoefficient(k_0, theta, refractiveIndices[[0,2]])
   
    fig2 = plt.figure(figsize = figSizeDouble)
    plt.subplot(121)
    plt.plot(theta/pi*180,abs(t_p), 'b', label="parallel polarization layer")
    plt.plot(theta/pi*180,abs(t_s), 'g', label="perpendicular polarization layer")
    plt.plot(theta/pi*180,abs(t_p13), 'b--', label="parallel polarization no layer")
    plt.plot(theta/pi*180,abs(t_s13), 'g--', label="perpendicular polarization no layer")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Transmission')
    
    plt.subplot(122)
    plt.plot(theta/pi*180,abs(r_p), 'b', label="parallel polarization layer")
    plt.plot(theta/pi*180,abs(r_s), 'g', label="perpendicular polarization layer")
    plt.plot(theta/pi*180,abs(r_p13), 'b--', label="parallel polarization no layer")
    plt.plot(theta/pi*180,abs(r_s13), 'g--', label="perpendicular polarization no layer")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Reflection')
    if legend: plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    if title: plt.suptitle('Absolute Values Comparison with and without layer', y=1.03, fontsize=fontSizeTitle)
    plt.tight_layout()
   
    plt.show()
    if savePlots:
        fig2.savefig('../' + folder + '/absoluteCoefficients_comparison.' + imageFormat, format=imageFormat, dpi=dpi)

    fig2 = plt.figure(figsize = figSizeDouble)
    plt.subplot(121)
    plt.plot(theta/pi*180,t_p, 'b', label="parallel polarization layer")
    plt.plot(theta/pi*180,t_s, 'g', label="perpendicular polarization layer")
    plt.plot(theta/pi*180,t_p13, 'b--', label="parallel polarization no layer")
    plt.plot(theta/pi*180,t_s13, 'g--', label="perpendicular polarization no layer")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Transmission')
#    plt.legend()
    plt.subplot(122)
    plt.plot(theta/pi*180,r_p, 'b', label="parallel polarization layer")
    plt.plot(theta/pi*180,r_s, 'g', label="perpendicular polarization layer")
    plt.plot(theta/pi*180,r_p13, 'b--', label="parallel polarization no layer")
    plt.plot(theta/pi*180,r_s13, 'g--', label="perpendicular polarization no layer")
    plt.xlabel('Angle in degrees')
    plt.ylabel('Reflection')
    if legend: plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    if title: plt.suptitle('Comparison with and without layer', y=1.03, fontsize=fontSizeTitle)
    plt.tight_layout()
   
    plt.show()
    if savePlots:
        fig2.savefig('../' + folder + '/CoefficientsComparison.' + imageFormat, format=imageFormat, dpi=dpi)
     
     
    refractiveIndices = np.array([1.33, 2.117])
    # comparison with and w/o layer
    t_p34, t_s34 = transmissionCoefficient(k_0, theta, refractiveIndices)    
    r_p34, r_s34 = reflectionCoefficient(k_0, theta, refractiveIndices)
   
    fig2 = plt.figure(figsize = figSizeDouble)
    plt.subplot(121)
    plt.plot(theta/pi*180,abs(t_p34)**2, 'b', label="parallel polarization layer")
    plt.plot(theta/pi*180,abs(t_s34)**2, 'g', label="perpendicular polarization layer")
    plt.xlabel('Angle in degrees')
    plt.ylabel(r'Transmission $|t_{s,p}|^2$')

    plt.subplot(122)
    plt.plot(theta/pi*180,abs(r_p34)**2, 'b', label="parallel polarization layer")
    plt.plot(theta/pi*180,abs(r_s34)**2, 'g', label="perpendicular polarization layer")
    plt.xlabel('Angle in degrees')
    plt.ylabel(r'Reflection $|r_{s,p}|^2$')
    if legend: plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    if title: plt.suptitle('Squared magnitudes of the Fresnel coefficients', y=1.03, fontsize=fontSizeTitle)
    plt.tight_layout()

    plt.show()
