.. _braggmolography:

Bragg-molography
====================

This part of the module allows to simualate the bragg reflection of guided modes at a molecule grating.


.. automodule:: bragg_molography.simulations.bragg_molography
	:members:

» `top`_

.. _top: #