Miscellaneous
=============

Using Euler
-----------
For simulations with a large number of scatterers or pixels, they can take several hours. Thus, it is convenient to use ETHs supercomputer Euler. For this purpose, the module *mainSim.py* was implemented: in this file a set of parameters can be defined for which the field distribution or other variables are calculated. With the following commands, one can connect to the computer and start a job.

Connect:

.. code-block:: bash
	
	$ ssh <nethz-name>@euler.ethz.ch

Load python module:

.. code-block:: bash
	
	$ module load python/2.7

Download everything from the gitlab repository:

.. code-block:: bash

	$ git fetch origin

Submit job (8 cores, 16 GB per core, max. 24 hours, file *main.py*):

.. code-block:: bash

	$ bsub -n 8 -R "rusage[mem=16384]" -W 24:00 "python mainSim.py"

Kill job (replace 0 by jobID(s) if only a specific job(s) should be killed)

.. code-block:: bash
    
    $ bkill 0

Read file on euler

.. code-block:: bash

    $ more <file>

Copy file from euler

.. code-block:: bash

	$ scp <nethz-name>@euler.ethz.ch:/cluster/home/<nethz-name>/focal_molography/<file> <target directory>

You can check the status of the job via bjobs

.. code-block:: bash

    $ bjobs

    
See also `ETH Supercomputing`_

.. _ETH Supercomputing: https://people.ee.ethz.ch/~muejonat/eth-supercomputing/

Gitlab
------

Clone directory:

.. code-block:: bash

    $ git clone https://<username>@git.ee.ethz.ch/afrutig/Focal-molography.git

Upload all files (replace --all by the filenames if only some files are to be uploaded):

.. code-block:: bash

    $ git add --all
    $ git commit -am '<Comment>'
    $ git push

The login data are required to make some changes.

Download files

.. code-block:: bash
    
    $ git pull

Download everything from another repository marked as "origin", discard changes and revert to the mentioned branch ("master" in repository "origin").

.. code-block:: bash

    $ git fetch origin
    $ git reset --hard origin/master
    $ git pull

Pull will just get everything from a remote repository and integrate.


Sphinx
------
Open console (anaconda for windows)
In console go to Folder focal molography documentation using cd
Delete all html files using

.. code-block:: bash
    $ make clean

Generate html files
.. code-block:: bash
    $ make html




