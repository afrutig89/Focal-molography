.. _particle:

Particle Scattering
====================

Theory
------

Most generally speaking, scattering and absorption describe the behavior of a particle illuminated by a light beam. These phenomena are dependent on particle characteristics, i.e. shape, size and the material. The study of different shaped particles requires cumbersome mathematics. However, in the case of biosensors the particles of interest are most often proteins, which are small compared to the wavelength. Small particles can be described by Rayleigh scatterers which means that they are not anymore dependent on the shape but rather are assumed to behave like small spheres. :cite:`bohren_particles_1998`

According to Bohren and Huffman :cite:`bohren_particles_1998`, a spherical particle in an electrostatic field radiates as an electric dipole. Consequently, closed formulae for radiation in free-space as well as near a planar interface are available. On that basis, single particle contributions of a biosensor-signal can be analyzed. But this model of scattering is only valid up to a certain particle size. Larger spheres (ratio between radius and wavelength greater than 0.05 :cite:`kerker_chapter_1969`) have to be described by Mie theory which is also briefly covered in this section.

.. _Rayleigh:

Rayleigh scattering
^^^^^^^^^^^^^^^^^^^

The scattered field from a small spherical particle illuminated by a monochromatic plane wave is found by imposing conditions for continuity of the potentials and the electric field at the boundary between sphere and medium. :cite:`bohren_particles_1998` By doing so, the scattered field can be expressed as the radiated field of a dipole: :cite:`stratton_radiation_2015`

.. math:: \vec{E}_{\mathrm{sca}}=\frac{e^{\mathrm{i}kr}}{-\mathrm{i}kr}\frac{\mathrm{i}k^3}{4\pi \varepsilon_m} \vec{e}_r \times (\vec{e}_r \times \vec{p})
	:label: dipole_radiation

In Eq. :eq:`dipole_radiation` (Note that Rayleigh scatterers induce a :math:`\pi/2` phase shift upon free space propagation in the far field. However, since a wave has a periodicity of :math:`2\pi`, the same formulae are valid to build a diffraction grating with Rayleigh scatterers.), only the far-field is considered. :math:`k` is the wave number, :math:`r` the distance from the dipole to the point of observation and :math:`\vec{e}_r` the corresponding unit vector. The dipole moment :math:`\vec p = (p_x,p_y,p_z)` is dependent on the excitation field :math:`\vec{E}_0` and the static polarizability :math:`\alpha`:

.. math:: \vec p = {\varepsilon _0}{\varepsilon _m}\alpha {{\vec E}_0}
	:label: dipoleMoment

.. math:: \alpha  = 4\pi {a^3}\frac{{{\varepsilon _p} - {\varepsilon _m}}}{{{\varepsilon _p} + 2{\varepsilon _m}}}
	:label: polarizability_const

where :math:`a` is the particle radius and :math:`\varepsilon_p` and :math:`\varepsilon_m` the relative permittivity of the particle and the surrounding medium, respectively.

:num:`Fig. #figrayleigh` illustrates a cross-section of the dipole field in the :math:`xy` plane for a :math:`y` polarized dipole (:math:`\vec{E}_0=(0,E_0,0)`). This field distribution is cylindrically symmetrical, such that a three-dimensional representation of the field would have the shape of a torus.

|
.. _figRayleigh:

.. figure:: figures/Rayleigh_scattering.png
	:width: 100 %

	The scattered field of an illuminated protein molecule is equivalent to the radiation field of a y-polarized dipole in xy-plane. The field is cylindrically symmetrical, such that in three dimensions it looks like a torus. For better visualization, the color intensity is normalized with a square root scaling.

|

The scattering of a particle can be quantified by the scattering cross-section :math:`\sigma_\mathrm{sca}`. It is defined as the ratio between the scattered energy :math:`P_\mathrm{sca}` to the incident intensity :math:`I_i`:

.. math::
	\sigma_\mathrm{sca} = \frac{P_\mathrm{sca}}{I_i}
	:label: scattering_cross-section_definition


Eq. :eq:`scattering_cross-section_definition` can be expressed as

.. math::
	\begin{eqnarray}
		\sigma_{sca}	&=&	\frac{{k^{4}}}{6\pi}\left|\alpha\right|^{2}\\
		{}				&=&	\frac{24\pi^3 V^2}{\lambda^4}\left|\frac{n^2-1}{n^2+2}\right|^{2}
	\end{eqnarray}
	:label: scatter_cross-section_Rayleigh


The scattering cross-section represents an effective area that expresses the probability of a scattering event. :cite:`bohren_particles_1998` Some particles such as gold nanoparticles do not only scatter but also absorb a portion of the incident energy. For such particles the polarizability :math:`\alpha` or rather the refractive index is complex (see section :ref:`complex_n`). Accordingly, the absorption cross-section :math:`\sigma_\mathrm{abs}` is defined as

.. math::
	\sigma_{\mathrm{abs}}=k\Im\left\{ \alpha\right\}
	:label: absorption_cross-section

The sum of the scattering and the absorption is called extinction and is the total power :math:`P_0` dissipated by the particle divided by the incident intensity :math:`I_i`:

.. math::
	\begin{eqnarray}
		\sigma_{\mathrm{ext}}	&=	\sigma_{\mathrm{sca}}+\sigma_{\mathrm{abs}}\\
		{}						&=	\frac{P_0}{I_{i}}
	\end{eqnarray}
	:label: extinction_cross-section

The scattering, absorption, and extinction efficiencies (:math:`Q_\mathrm{sca}`, :math:`Q_\mathrm{abs}`, and :math:`Q_\mathrm{ext}`) are defined as the corresponding cross-section divided by the cross-sectional area :math:`G` of the particle:

.. math::
	\begin{eqnarray}
	Q_{sca} &= \frac{\sigma_{\mathrm{sca}}}{G}\\
	Q_{abs} &= \frac{\sigma_{\mathrm{abs}}}{G}\\
	Q_{ext} &= \frac{\sigma_{\mathrm{ext}}}{G}
	\end{eqnarray}
	:label: efficiencies

For a spherical particle :math:`G` is :math:`\pi a^2`. These quantities are useful if the influence of different particles on a signal is compared.

.. _complex_n:

Complex refractive index and dielectric constant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The scattered field of a particle can be enhanced by labeling a larger particle. Apart from the particle size, a high refractive index of the secondary label is beneficial for a higher signal. The refractive index :math:`n` thus represents an important optical parameter. It describes the refraction and the absorption of a medium and is related to the relative dielectric constant :math:`\varepsilon_r` as follows :cite:`fox_optical_2010`

.. math::
	n=\sqrt{\varepsilon_{r}}=\sqrt{\varepsilon'+i\varepsilon''}
	:label: refractive-index

The relative dielectric constant :math:`\varepsilon_{r}` is heavily dependent on the frequency as soon there are free conduction electrons. For metals which contain a large density of free electrons, the Lorentz-Drude oscillator model can be used to describe a frequency dependent dielectric function: :cite:`rakic_optical_1998`

.. math::
	\varepsilon_{r}(\omega)=\varepsilon_{r}^{(f)}(\omega)+\varepsilon_{r}^{(b)}(\omega)
	:label: Lorentz_Drude

The first term :math:`\varepsilon_{r}^{(f)}` that accounts for the free-electron effects is

.. math::
	\varepsilon_{r}^{^{(f)}}(\omega)=1-\frac{{\omega_{p}^{2}}}{\omega^{2}+i\gamma\omega}
	:label: Drude_model


with the material dependent plasma frequency :math:`\omega_p`

According to Novotny :cite:`novotny_principles_2012`, the dielectric function follows Eq. :eq:`Drude_model` for wavelengths above 650 nm. Below, the energy of a photon is sufficient to cause interband transition accounted for by the second term :math:`\varepsilon_{r}^{(b)}`. It is expressed as a Lorenzian by

.. math::
	\varepsilon_{r}^{^{(b)}}(\omega)=\sum_{j=1}^{k}\frac{f_{j}\omega_{p}^{2}}{(\omega_{j}^{2}-\omega^{2})+i\gamma_j\omega}
	:label: Lorentz_model


where :math:`k` is the number of oscillators. Both the oscillator strength :math:`f_j` and damping :math:`\gamma_j` are dependent on the material. Rakic et. al. :cite:`rakic_optical_1998` provide these parameters for different materials.

Compared to the dielectric constant of a biomolecule (approximately 1.6 at 635 nm), the signal can be significantly enhanced by gold particles. This is observed from the real and the imaginary part of the dielectric function of gold (:num:`Fig. #epsilonau`). For a wavelength of 635 nm, the enhancement corresponds approximately to a factor 10 (absolute values).

|
.. _epsilonAu:

.. figure:: figures/epsilon_Au.png
	:width: 100 %

	The dielectric function of gold according to the Lorentz-Drude model.

|

In conclusion, a remarkably lower amount of molecules that are labeled with gold particles than unlabeled particles is required to obtain the same signal. But this relation is only valid for Rayleigh scattering, in other words for scattering of small spherical particles with low refractive index. If the particle size increases, the theory of Mie scattering has to be applied.

.. note:: The refractive index of other materials than metals can be approximated by different dispersion formulas (see :ref:`materials`). A material database is implemented and will be integrated in the GUI.


Mie scattering
^^^^^^^^^^^^^^

The dipole approximation as discussed in the previous sections is only valid for small particles compared to the wavelength. Bohren and Huffman :cite:`bohren_particles_1998` quantify this condition with the following inequality:

.. math::
	\frac{2\pi}{\lambda} \left|n\right| a \ll 1
	:label: Rayleigh_limit

with the wavelength :math:`\lambda`, the refractive index :math:`n` and the sphere particle radius :math:`a`. If Eq. :eq:`Rayleigh_limit` is not fulfilled, the shape of the particle becomes more relevant. An exact solution can be obtained by solving the Maxwell equations with the given boundary conditions. This was done by Gustav Mie in 1908 :cite:`bohren_particles_1998` and thoroughly explained by Kerker :cite:`kerker_chapter_1969` for a homogeneous sphere. The general solution for this configuration corresponds to the linear superposition of spherical multipole partial waves.

For the far-field solution, the expression for the electric field is simplified. However, in this work, the field for Mie scatterer will not be investigated but rather compared to the Rayleigh-scattering. Accordingly, it suffices to compare the cross-sections calculated by Rayleigh theory (Eq. :eq:`scatter_cross-section_Rayleigh`) and by Mie theory:

.. math::
	\sigma_{\mathrm{sca,Mie}} = \frac{\lambda^2}{2\pi} \sum_{n=1}^{\infty} \left(2n+1\right) \left(\left|a_n\right|^2+\left|b_n\right|^2\right)
	:label: scattering_cross-section_Mie

with the Mie coefficients

.. math::
	{a_n} = \frac{{m{\psi _n}(mx)\psi {'_n}(x) - {\psi _n}(x)\psi {'_n}(mx)}}{{m{\psi _n}(mx)\xi {'_n}(x) - {\xi _n}(x)\psi {'_n}(mx)}}
	:label: MieCoefficient-a_n

.. math::
	{b_n} = \frac{{{\psi _n}(mx)\psi {'_n}(x) - m{\psi _n}(x)\psi {'_n}(mx)}}{{{\psi _n}(mx)\xi {'_n}(x) - m{\xi _n}(x)\psi {'_n}(mx)}}
	:label: MieCoefficient-b_n


where :math:`m=n/n_m` is the relative refractive index and :math:`x` the size parameter which is defined as :math:`x=\frac{2\pi a}{\lambda_m}` with the wavelength :math:`\lambda_m` in the surrounding medium. The functions :math:`{\psi _n}(\rho )` and :math:`{\xi _n}(\rho )` are defined as

.. math::
	{\psi _n}(\rho ) = \rho {j_n}(\rho )
	:label: Mie_psi

.. math::
	{\xi _n}(\rho ) = \rho h_n^{(1)}(\rho)
	:label: Mie_xi

with the spherical Hankel functions :math:`h_n^{(1)}` as a combination of the spherical Bessel functions :math:`j_n` and :math:`y_n`

.. math::
	h_n^{(1)}(\rho ) = {j_n}(\rho ) + \mathrm{i} {y_n}(\rho )
	:label: hankel


.. math::
	{j_n}(\rho ) = \sqrt {\frac{\pi }{{2\rho }}} {J_{n + 1/2}}(\rho )
	:label: j_n

.. math::
	{y_n}(\rho ) = \sqrt {\frac{\pi }{{2\rho }}} {Y_{n + 1/2}}(\rho )
	:label: y_n

:math:`J_{n+1/2}` and :math:`Y_{n+1/2}` denote the half integral order Bessel of the first and second kind. The corresponding scattering efficiency can be calculated by Eq. :eq:`efficiencies`. 

For an enhancement of the signal, gold nanoparticles as secondary labels were used. For 25 nm gold nanoparticles, the inequality :eq:`Rayleigh_limit` is poorly satisfied for the given particles (:math:`\frac{2\pi}{\lambda} \left|\tilde{n}\right|a=0.79`). However, the difference between Rayleigh and Mie scattering is minor. Fig. \ref{fig:scattering} depicts the wavelength dependent extinction (red), absorption (blue) and scattering (green) efficiencies calculated with Mie (solid) and Rayleigh (dashed) scattering theory (a,b,c) and the relative difference (d,e,f) for gold nanoparticle radii of 15 nm, 25 nm and 40 nm, respectively. It can be observed that the theories differ more for increasing radius. Also, the absorption (blue curve) becomes more pronounced, which can gain relevance for an optimization process.

|
.. _scattering:

.. figure:: figures/scattering.png
	:width: 100 %

	Comparison of Mie and Rayleigh theory. (a-c) Spectra of efficiency of absorption :math:`Q_{abs}` (blue), scattering :math:`Q_{sca}` (green) and extinction :math:`Q_{ext}` (red) for Mie (solid) and Rayleigh (dashed) theory and for different gold nanoparticle radii ((a,d): :math:`r=15` nm, (b,e): :math:`r=25` nm, (c,f): :math:`r=40` nm). (d-f) illustrate the corresponding relative difference of the two theories. For increasing radius, the error of the Rayleigh theory becomes relevant. According to Kerker :cite:`kerker_chapter_1969`, the Rayleigh limit is defined as :math:`\left| \tilde n \right| r/\lambda \leq 0.05` which correspond to a radius of approximately 10 nm.

|


Although Mie theory provides the exact values, the dipole approximation is used in the simulations. In this case, the influence of the nearby interface can be considered with the theory derived in :ref:`interface`. The exact results would require the analysis of the physical effects of multipoles close to a planar interface which does not lie in the scope of this thesis.

.. note:: This issue was investigated by Christof Fattinger during his dissertation and is available at ETHZ. However, its implementation is referred to future projects.


Model of the scattering particles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The scattered light considerably depends on the optical properties (i.e. refractive index), the shape and the size of the particle. These parameters can differ throughout the solution. Nevertheless, spherical particles with a specific size were assumed, since the number of target proteins is large and thus single particle contributions can be averaged.

The average refractive index of a receptor molecule (dielectric particle) in water (:math:`n_0 = 1.33`) was calculated as

.. math::
	n_R = n_0 + \left(\frac{dn}{dc}\right) \rho_R
	:label: refractiveIndexReceptor

where the mass density :math:`\rho_R` of the receptor molecule was calculated as reported in Fischer et al. :cite:`fischer_average_2009` 
For the refractive-index increment :math:`dn/dc` 0.182 ml/g was used. :cite:`de_feijter_ellipsometry_1978` The resultant refractive index for SAv was 1.587.

With help of de Feijter’s formula :cite:`de_feijter_ellipsometry_1978`, the volume of the protein was calculated as a function of the molecular mass :math:`M` and the refractive index difference between the protein :math:`n_R` and the surrounding medium :math:`n_0`:

.. math::
	V_R = \frac{dn}{dc}\frac{M}{N_A (n_R-n_0)}
	:label: protein_volume


with the Avogadro constant :math:`N_A`. Equalization of Eq. :eq:`protein_volume` with the volume of a sphere resulted in a particle radius :math:`a` of approximately 2.5 nm.



» `top`_


Implementation
--------------

.. automodule:: dipole.dipole
	:members:

LorentzDrude.py
^^^^^^^^^^^^^^^

.. automodule:: LorentzDrude

.. autofunction:: LorentzDrude.massDensityProtein
.. autofunction:: LorentzDrude.refractiveIndex
.. autofunction:: LorentzDrude.particleRadius

.. autoclass:: LorentzDrude.LD 
	:members:

.. note:: this module is deprecated and will be replaced by materials.py, where it is possible to choose between different materials (also organic or others).

dipole.py
^^^^^^^^^

.. automodule:: dipole

.. autofunction:: dipole.dipolePolarizability
.. autofunction:: dipole.staticDipoleMoment
.. autofunction:: dipole.dipoleRadiation

.. autofunction:: dipole.scatteringCrossSection
.. autofunction:: dipole.absorptionCrossSection
.. autofunction:: dipole.extinctionCrossSection



» `top`_

.. _top: #