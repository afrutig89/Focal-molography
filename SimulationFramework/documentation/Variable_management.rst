Variables and Datamanagement
============================

A new variable that is to be stored needs to be added to one of the dicts managed by parameters. The parameters in the different tabs are stored in dictionaries and these dictionaries can be access by a dedicated function, which returns the dictionary. Example: The Waveguide Parameters stored in the dict waveguidePar are returned by the function returnWaveguidePar as a dict. A new variable is to be added in the file variables.py and also in the corresponding widget.

There is a Tab called *Parameter* on the right side, which displays the parameters that are used in the simulation.
	* parameterWidget.py 

The MainWidget stores a dictionary of the results of the last simulation in a dictionary called, results. This is added to the dataase in the function updateResults() and therein by finding the common keys of the simulation_dict and the results dict. This intersection dictionary is then saved with *saveDB()* from writeLogFile.py to the database. Tablename is specified by this function. 

.. automodule:: variables
   :members:




Database Access
----------------

.. automodule:: auxiliaries.writeLogFile
   :members: