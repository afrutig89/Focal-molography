.. _initialization:

Initialization
==============

For the calculations, different parameters are required which are declared in the following. These parameters are arranged in a list, whereas the nomenclature used in the script are denoted as italic text. Note that all the parameters are given in SI units.

.. _init_parameters:

Waveguide Parameters
--------------------

Input wave field
^^^^^^^^^^^^^^^^

Wavelength :math:`\lambda` (m): *wavelength* / float
	The wavelength of the incoming beam. The calculations made in :cite:`fattinger_focal_2014` are based on a wavelength of 635nm.

Input Power :math:`P` (W): *inputPower* / float
	The power of the laser in Watts.

Beam Waist :math:`w` (:math:`m^2`): beamWaist* / float
	Beam diameter in meter

	.. todo:: This parameter is not relevant for the current simulation tool since this factor is considered by the coupling efficiency. In the future, the influence of different sources could be investigated, such that the coupling efficiency does not have to be estimated.

Media
^^^^^
In the program, the media are stored as an object consisting of a name and a refractive index. Depending on the type of the material, different equations are used to calculate the refractive index (see :ref:`materials`).

Cover Medium:

	.. note:: The refractive index of the protein particles is calculated for the surrounding medium water. I.e. the refractive index increment dn/dc is 0.182 ml/g as reported in :cite:`fattinger_focal_2014`.

Film:
	Name and refractive index of the film medium (waveguide).


Substrate:
	Name and refractive index of the substrate.


Coupling
^^^^^^^^

Efficiency (%): *couplingEfficiency* / float
	The amount of power that is coupled into the waveguide.

Polarization: *polarization*
	polarization of the mode of the waveguide ('TE' or 'TM')

mode
	Mode of the incident wave.

	.. todo:: Only the fundamental mode (0) is implemented, yet. Depending on the waveguide thickness, more or no modes propagate.


Waveguide
^^^^^^^^^

Thickness :math:`d_f`: *thickness* / float
	Thickness of the waveguide in meters.

Propagation loss: *propagationLoss* / float
	Propagation loss of the waveguide given in Np/m.


Mologram
--------

Mologram parameters
^^^^^^^^^^^^^^^^^^^

Focal point :math:`f`: *focalPoint* / float
	Distance of the focal point in meters. The focal point is assumed to be in z-direction, namely (0, 0, focalPoint). Use  negative value if the focus is supposed to be in the substrate (below the mologram).

	.. note:: The yet available chips consist of molograms that generate a focal point at -900 um which is larger than the chip thickness (700 um). Consequently, the measured focal distance does not correspond to the calculated distance. This can be taken account for by multiplying the screen below (700 um) with n_c/n_s.

Diameter of the mologram: *diameter* / float

Bragg Offset: *braggOffset* / float
	A certain range in the mologram is masked due to second-order Bragg reflections which are to be avoided. This region is defined as an annulus. The difference between the outer and inner radii is equal to this value.

.. todo:: in the script, this mask is defined as an annulus. In reality, this should be a hyperbola. A more thorough investigation in the Bragg offset is needed. Also, the consequences of a mologram which is not masked have not been investigated, yet.


Particles
^^^^^^^^^

The particle is as an object consisting the material and the size of the particle. Depending on the type of the material, different equations are used to calculate the refractive index and thus the extinction coefficient of the particle (see :ref:`materials`).

Material of the particle: *particleMaterial*
	The material of the particle. Possible inputs are 'Ag', 'Au', 'Cu', 'Cr', 'Ni', 'W', 'Ti', 'Be', 'Pd', 'Pt', and 'SAv'. See chapter :ref:`particle` for a more thorough explanation.

.. todo:: in the GUI, this object will be extended by an editable material database (refractiveIndex.info), such that any particle material can be analyzed.

Particle Radius: *particleRadius* / float
	The radius of the particle. The particle is assumed to be spherical. This parameter is stored in the object *Particle*.

Particle mass: *particleMass* / float
	Molecular mass of the particle in Daltons. This parameter is only required for dielectric materials in order to calculate its size. This parameter is stored in the object *Particle*.

Coherent / Noncoherent Particles: *coherentParticles*, *noncoherentParticles* / int
	Number of coherently / noncoherently placed particles (particles on the ridges / grooves). The total number of particles is *N_particles* = *coherentParticles*+*noncoherentParticles*. Note that the noncoherent particles are evenly distributed over the mologram. That means that e.g. for 400 coherent and 400 noncoherent particles, 600 are placed on the ridges and 200 on the grooves.


Number of particles and Coherent Binding: *N_particles*, *coherentBinding* / int, float
	These two parameters are a different way to define the distribution of the particles on the mologram. Here, the particles bind to the ridges with a certain probability. E.g. 400 particles bind with 75% Coherent Binding means that 300 particles are located on the ridges and 100 on the grooves.


Particle placement: *placement* / string
	This parameter defines where the scatterers will be placed, either *Mololines* on mololines or on the *Ridges (Rectangular)*. See section :ref:`mologram`.

.. todo:: in reality, the distribution is not exactly rectangular but has a sinusoidal shape. Therefore the expected result will be better than the results given be a rectangular distribution.


Vertical distance: *z_min*, *z_max* / float
	These values define the range of the vertical distance of the dipoles on the waveguide.


Screen
------

Width and resolution: *screenWidth*, *npix* / float, int
	Defines the width and the number of pixels of the screen. If the number of pixels is set 1, the field is only calculated in the focal point.


Plane and ratio: *screenPlane*, *screenRatio* / string, float
	The screen plane can be either 'xy', 'xz', or 'yz'. The ratio defines the ratio between the first and the second axis. This is useful since the focal depth is much larger than the with of the focal spot.

Center and rotation: *center*, *screenRotation* / float tuple, float
	These parameters can be used, if one is interested in the field distribution in a tilted plane or/and at a different center point. It gives a possibility to interpolate a 3D field distribution.


.. _init_settings:

Settings
--------

General settings
^^^^^^^^^^^^^^^^

Calculation method (programming issue): *calculationMethod* / string
	Defines the method with which the field is calculated. If many scatterers or a many screen pixels are used, the simulation should be split, since otherwise a memory error could rise. Use *splitProcess* when using a windows OS and *parallelPool* (uses multiple cores) when using a unix OS.



Plot settings
^^^^^^^^^^^^^

The following settings are not of relevance when using the :ref:`GUI`.

showPlots, savePlots
	Determines whether the plots are shown and saved after the simulation.


intensityPlot, summaryPlot, patternPlot
	Different kinds of plots that will be shown if set True. 


plotType, colorbar, grid
	General settings for the intensity- and summary plot. The plot type can either be defined as '*contour*' or as a cross-section: '*x-axis*', '*y-axis*', or '*z-axis*'.


plotZoomedPattern, xlim, ylim, plotMologram
	These parameters define the appearance of the pattern-plot. It can be helpful to zoom in the pattern such that the distance between the particles is seen more clearly. If the parameter *plotMologram* is set true, the mologram will appear behind the zoomed pattern plot.


dpi, fontSizeAxes, fontSizeTicks, figSizeSingle, figSizeDouble, figSizeTriple, figSizeQuadruple, imageFormat
	General settings for the plots.

	.. note:: A much more elegant way would be to define it with *plt.rc* and *plt.rcParams*


Log settings
^^^^^^^^^^^^

saveLog, saveDatabase
	Saves a log file or/and a database.


» `top`_


.. _init_implementation:

Implementation
--------------

writeLogFile.py
^^^^^^^^^^^^^^^

.. automodule:: writeLogFile

.. autofunction:: writeLogFile.writeVarsToFile
.. autofunction:: writeLogFile.checkFolder
.. autofunction:: writeLogFile.createDB
.. autofunction:: writeLogFile.saveDB
.. autofunction:: writeLogFile.loadDB
.. autofunction:: writeLogFile.addColumns
.. autofunction:: writeLogFile.deleteColumn
.. autofunction:: writeLogFile.adapt_array
.. autofunction:: writeLogFile.convert_array
.. autofunction:: writeLogFile.returnType


screen.py
^^^^^^^^^

.. automodule:: screen

.. autofunction:: screen.rotateScreen
.. autofunction:: screen.defineScreen

constants.py
^^^^^^^^^^^^

.. automodule:: constants


» `top`_

.. _top: #