Mologram Widget
===============

Widget for the visualization of the scatterer distribution on the mologram. 

.. figure:: figures/plotMologram_Widget.png
	:width: 100 %

	Distribution of the scatterers on the mologram.


.. automodule:: GUI.mologramWidget.plotMologram_widget
   :members:



