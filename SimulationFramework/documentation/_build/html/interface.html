<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Interface &mdash; Focal Molography 1.0 documentation</title>
    
    <link rel="stylesheet" href="_static/agogo.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="top" title="Focal Molography 1.0 documentation" href="index.html" />
    <link rel="next" title="Mologram" href="mologram.html" />
    <link rel="prev" title="Dielectric slab waveguide" href="waveguide.html" /> 
  </head>
  <body role="document">
    <div class="header-wrapper" role="banner">
      <div class="header">
        <div class="headertitle"><a
          href="index.html">Focal Molography 1.0 documentation</a></div>
        <div class="rel" role="navigation" aria-label="related navigation">
          <a href="waveguide.html" title="Dielectric slab waveguide"
             accesskey="P">previous</a> |
          <a href="mologram.html" title="Mologram"
             accesskey="N">next</a> |
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a>
        </div>
       </div>
    </div>

    <div class="content-wrapper">
      <div class="content">
        <div class="document">
            
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="interface">
<span id="id1"></span><h1>Interface<a class="headerlink" href="#interface" title="Permalink to this headline">¶</a></h1>
<div class="section" id="theory">
<h2>Theory<a class="headerlink" href="#theory" title="Permalink to this headline">¶</a></h2>
<div class="section" id="dipole-emission-near-planar-interfaces">
<h3>Dipole emission near planar interfaces<a class="headerlink" href="#dipole-emission-near-planar-interfaces" title="Permalink to this headline">¶</a></h3>
<p>Molecules that are arranged in a diffraction pattern are directly attached to the surface. It is therefore important to know how the particles interact with an interface in close proximity (few tens of nm). This issue was extensively investigated by Novotny <a class="reference internal" href="references.html#novotny-allowed-1997" id="id2">[Nov97b]</a>, <a class="reference internal" href="references.html#novotny-allowed-1997-1" id="id3">[Nov97a]</a> and is briefly summarized in this section.</p>
<p><a href="#id4"><span class="problematic" id="id5">:num:`Fig. #dipoleinterface`</span></a> illustrates a dipole placed near a planar interface, e.g. a waveguide. The total energy dissipation of an illuminated dipole will be split into different radiated parts, namely, into the upper and lower half-space (<span class="math">\(P^{\uparrow}, P^{\downarrow}\)</span>) and into the waveguide (<span class="math">\(P_m\)</span>). The radiated power in the lower half-space can be further subdivided into allowed and forbidden zones <span class="math">\(P_a^{\downarrow.}\)</span> and <span class="math">\(P_f^{\downarrow}\)</span>. In the forbidden zone, the waves are evanescent and decay exponentially from the waveguide. <a class="reference internal" href="references.html#lukosz-light-1977" id="id6">[LK77]</a> In the following, the terminology for planar waveguides is used: the upper and the lower medium correspond to the cover and the substrate with refractive index <span class="math">\(n_c\)</span> and <span class="math">\(n_s\)</span>, respectively, and the interface to the film with refractive index <span class="math">\(n_f\)</span> (<a href="#id7"><span class="problematic" id="id8">:num:`Fig. #dipoleinterface`</span></a>b).</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<div class="figure" id="id23">
<span id="dipoleinterface"></span><a class="reference internal image-reference" href="_images/Dipole_interface.png"><img alt="_images/Dipole_interface.png" src="_images/Dipole_interface.png" style="width: 100%;" /></a>
<p class="caption"><span class="caption-text">Dipole radiation near a planar interface. a) A dipole placed on a waveguide (position <span class="math">\((x_0,y_0,z_0)\)</span>) radiates differently into the different media (figure adapted from <a class="reference internal" href="references.html#novotny-dipole-2006" id="id9">[NH06]</a>). A part of the radiated power couples into the waveguide (<span class="math">\(P_m\)</span>) and the rest is emitted into the two half-spaces (<span class="math">\(P^{\uparrow}, P^{\downarrow}\)</span>), whereas the power in the lower medium can be divided into forbidden <span class="math">\((P_f^{\downarrow})\)</span> and allowed zones <span class="math">\((P_a^{\downarrow.})\)</span>. b) The dipole is located in a medium with refractive index <span class="math">\(n_c\)</span> at a height <span class="math">\(z_0\)</span> on a waveguide (<span class="math">\(n_f\)</span>) with thickness <span class="math">\(d_f\)</span>. Depending on the radiation angle (only one angle <span class="math">\(\theta\)</span> shown illustratively) and the refractive index difference, the fraction of transmitted and reflected power varies.</span></p>
</div>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>Important for this work is the power radiated into the allowed zone since the signal is measured in this region. Moreover, the detection system is placed in the far-field, i.e. at a distance much greater than the wavelength. For this case the electric field in spherical coordinates (<span class="math">\(r\)</span>, <span class="math">\(\theta\)</span>, <span class="math">\(\phi\)</span>) originating from a dipole can be expressed as: <a class="reference internal" href="references.html#novotny-dipole-2006" id="id10">[NH06]</a></p>
<div class="math" id="equation-elField">
<span class="eqno">(1)</span>\[\begin{split}\vec E = \left[ {\begin{array}{*{20}{c}}
{{E_\theta }}\\
{{E_\phi }}
\end{array}} \right] = \frac{{k_c^2}}{{4\pi {\varepsilon _0}{\varepsilon _c}}}\frac{{\exp (i{k_s}r)}}{r}\left[ {\begin{array}{*{20}{c}}
{\left( {{p_x}\cos \phi  + {p_y}\sin \phi } \right)\cos \theta \,\Phi _s^{(2)} - {p_z}\sin \theta \,\Phi _s^{(1)}}\\
{ - \left( {{p_x}\sin \phi  - {p_y}\cos \phi } \right)\,\Phi _s^{(3)}}
\end{array}} \right]\end{split}\]</div>
<p>with the dipole moment <span class="math">\(\vec{p}=(p_x,p_y,p_z)\)</span></p>
<div class="math" id="equation-dipoleMoment">
<span class="eqno">(2)</span>\[\vec p = {\varepsilon _0}{\varepsilon _m}\alpha {{\vec E}_0}\]</div>
<p><span class="math">\(\varepsilon_c\)</span> denotes the relative permittivity of the medium that surrounds the dipole, i.e. the cover medium. The electric far-field has no radial component, i.e. it is only transverse since the longitudinal part decays rapidly (<span class="math">\(1/r^3\)</span>). <a class="reference internal" href="references.html#stratton-radiation-2015" id="id11">[SS15]</a></p>
<p>The potentials <span class="math">\(\Phi_s^{(1)-(3)}\)</span> are defined asfootnote{Note: The second potential (Eq. (10.37)) reported in <a class="reference internal" href="references.html#novotny-dipole-2006" id="id12">[NH06]</a> has the wrong sign. The potentials from Eqs. <a href="#equation-potentials1">(3)</a> to <a href="#equation-potentials3">(5)</a> are corrected}</p>
<div class="math" id="equation-potentials1">
<span class="eqno">(3)</span>\[\Phi _s^{(1)} = \frac{{{n_s}}}{{{n_c}}}\frac{{\cos \theta }}{{{{\tilde s}_z}(\theta )}}{t^{(p)}}(\theta ){e^{i{k_s}\left( {{z_0}{{\tilde s}_z}(\theta ) + d \cos \theta } \right)}}\]</div>
<div class="math" id="equation-potentials2">
<span class="eqno">(4)</span>\[\Phi _s^{(2)} = \frac{{{n_s}}}{{{n_c}}}{t^{(p)}}(\theta ){e^{i{k_s}\left( {{z_0}{{\tilde s}_z}(\theta ) + d \cos \theta } \right)}}\]</div>
<div class="math" id="equation-potentials3">
<span class="eqno">(5)</span>\[\Phi _s^{(3)} = \frac{{\cos \theta }}{{{{\tilde s}_z}(\theta )}}{t^{(s)}}(\theta ){e^{i{k_s}\left( {{z_0}{{\tilde s}_z}(\theta ) + d \cos \theta } \right)}}\]</div>
<p>with</p>
<div class="math" id="equation-tilde_s">
<span class="eqno">(6)</span>\[{{\tilde s}_z}(\theta ) = \sqrt {{{\left( {\frac{{{n_c}}}{{{n_s}}}} \right)}^2} - {{\sin }^2}\theta }\]</div>
<p>The potential <span class="math">\(\Phi_s^{(1)}\)</span> describes a vertically oriented dipole, whereas potentials <span class="math">\(\Phi_s^{(2)}\)</span> and <span class="math">\(\Phi_s^{(3)}\)</span> characterize a p-polarized and s-polarized horizontal dipole. The amount of transmitted and reflected power is dependent on the radiation angle <span class="math">\(\theta\)</span> and the refractive indices of the media (<a href="#id13"><span class="problematic" id="id14">:num:`Fig. #dipoleinterface`</span></a>b). Furthermore, an additional phase shift is introduced due to the dipole&#8217;s vertical position <span class="math">\(z_0\)</span> and the waveguide thickness <span class="math">\(d_f\)</span>.</p>
<p>If the potentials are set to unity, the electric field from Eq. <a href="#equation-elField">(1)</a> corresponds to the dipole radiation in free space. Therefore, the optical interface introduces an amplification of the radiated power into the lower half-space as can be observed in <a href="#id15"><span class="problematic" id="id16">:num:`Fig. #potentials`</span></a>.</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<div class="figure" id="id24">
<span id="potentials"></span><a class="reference internal image-reference" href="_images/potentials.png"><img alt="_images/potentials.png" src="_images/potentials.png" style="width: 100%;" /></a>
<p class="caption"><span class="caption-text">Potentials for a dipole near a planar interface. The intensity in the lower half space of a vertically oriented dipole is described by the square of the potential <span class="math">\(\left|\Phi_s^{(1)}\right|\)</span> (a), whereas the intensity of a horizontal dipole can be calculated with help of <span class="math">\(\left|\Phi_s^{(2)}\right|^2\)</span> and <span class="math">\(\left|\Phi_s^{(3)}\right|^2\)</span> with respect to the polarization. In free-space, these potentials equal unity, therefore the radiation into the lower half-space is higher if radiating into a denser medium. The refractive indices used for this plot are <span class="math">\(n_c=1.33\)</span>, <span class="math">\(n_f=2.117\)</span>, and <span class="math">\(n_s=1.521\)</span>, respectively.</span></p>
</div>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>The potentials <span class="math">\(\Phi_s^{(1)-(3)}\)</span> are functions of the Fresnel transmission coefficients <span class="math">\(t^{(p)}\)</span> and <span class="math">\(t^{(s)}\)</span>, which define the amount of power that is transmitted for parallel and perpendicularly polarized plane waves. The Fresnel coefficients of a two-interface structure are derived by Lukosz <a class="reference internal" href="references.html#lukosz-light-1981" id="id17">[Luk81]</a> as</p>
<div class="math" id="equation-transmissionSingleLayer">
<span class="eqno">(7)</span>\[{t^{(p,s)}} = \frac{{t_{1,2}^{(p,s)}t_{2,3}^{(p,s)}\exp \left( {i{k_{z2}}d} \right)}}{{1 + r_{1,2}^{(p,s)}r_{2,3}^{(p,s)}\exp \left( {2i{k_{z2}}d} \right)}}\]</div>
<p>where the Fresnel transmission coefficients for a single interface are dependent on the z-component of the wave vector <span class="math">\(\vec k = (k_x, k_y, k_z)\)</span> in the different media (1,2) such as on the corresponding permeability <span class="math">\(\mu_{1,2}\)</span> and permittivity <span class="math">\(\varepsilon_{1,2}\)</span>:</p>
<div class="math" id="equation-Fresnel_transmission_p">
<span class="eqno">(8)</span>\[t_{1,2}^{(s)}(\theta ) = \frac{{2{\mu _2}{k_{z1}}}}{{{\mu _2}{k_{z1}} + {\mu _1}{k_{z2}}}}\]</div>
<div class="math" id="equation-Fresnel_transmission_s">
<span class="eqno">(9)</span>\[t_{1,2}^{(p)}(\theta ) = \frac{{2{\varepsilon _2}{k_{z1}}}}{{{\varepsilon _2}{k_{z1}} + {\varepsilon _1}{k_{z2}}}}\sqrt {\frac{{{\mu _2}{\varepsilon _1}}}{{{\mu _1}{\varepsilon _2}}}}\]</div>
<div class="math" id="equation-Fresnel_reflection_p">
<span class="eqno">(10)</span>\[r_{1,2}^{(s)}(\theta ) = \frac{{{\mu _2}{k_{{z_1}}} - {\mu _1}{k_{z2}}}}{{{\mu _2}{k_{z1}} + {\mu _1}{k_{z2}}}}\]</div>
<div class="math" id="equation-Fresnel_reflection_s">
<span class="eqno">(11)</span>\[r_{1,2}^{(p)}(\theta ) = \frac{{{\varepsilon _2}{k_{z1}} - {\varepsilon _1}{k_{z2}}}}{{{\varepsilon _2}{k_{z1}} + {\varepsilon _1}{k_{z2}}}}\]</div>
<p><a href="#id18"><span class="problematic" id="id19">:num:`Fig. #fresnelcoefficients`</span></a> shows that for similar refractive indices of cover and substrate (<span class="math">\(n_c=1.33\)</span>, <span class="math">\(n_s=1.521\)</span>), the Fresnel coefficients for a thin single layer (solid curves) do not differ significantly compared to the case of a single interface between cover and substrate (dashed curves). Also, both the transmission and the reflection are only slightly dependent on the polarization for small angles (parallel: blue, perpendicular: green).</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<div class="figure" id="id25">
<span id="fresnelcoefficients"></span><a class="reference internal image-reference" href="_images/Fresnel_Coefficients.png"><img alt="_images/Fresnel_Coefficients.png" src="_images/Fresnel_Coefficients.png" style="width: 100%;" /></a>
<p class="caption"><span class="caption-text">Absolute values of the Fresnel coefficients for a planar layered medium (solid) and single interface (dashed) according to Eqs. <a href="#equation-transmissionSingleLayer">(7)</a> to <a href="#equation-Fresnel_reflection_s">(11)</a> for parallel polarization (blue) and perpendicular polarization (green) as a function of the angle <span class="math">\(\theta\)</span>. The values used for these curves are <span class="math">\(n_c = 1.33\)</span>, <span class="math">\(n_f=2.117\)</span>, <span class="math">\(n_s=1.521\)</span> and <span class="math">\(d_f=145\)</span> nm.</span></p>
</div>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>While the electric field has to be summed up for a coherent signal, what is effectively measured by a detection system is the radiated power passing through an area (pointing flux). This is represented by the intensity <span class="math">\(I\)</span>:</p>
<div class="math" id="equation-intensity_general">
<span class="eqno">(12)</span>\[I = \frac{1}{2}\sqrt {\frac{{{\varepsilon _0}{\varepsilon _s}}}{{{\mu _0}{\mu _s}}}} E{E^*}\]</div>
<p>whereas the asterisk denotes the complex conjugate. To keep it simple, only a y-directed excitation field <span class="math">\(E_y\)</span> (plane polarized incident wave) is analyzed (complete analysis can be found in <a class="reference internal" href="references.html#novotny-dipole-2006" id="id20">[NH06]</a>). With the given simplification, inserting Eq. <a href="#equation-elField">(1)</a> into Eq. <a href="#equation-intensity_general">(12)</a> gives an expression for the intensity of a perpendicularly polarized horizontal dipole in the lower medium</p>
<div class="math" id="equation-dipole_intensity_TE">
<span class="eqno">(13)</span>\[{I_0} = \frac{3}{{8\pi {r^2}}}\frac{n_s}{n_1}\frac{{{P_0}}}{{{{\left| {\vec p} \right|}^2}}}\left[ {p_y^2\left( {{{\sin }^2}\phi \, {{\cos }^2}\theta \,{{\left| {\Phi _s^{(2)}} \right|}^2} + {{\cos }^2}\phi \,{{\left| {\Phi _s^{(3)}} \right|}^2}} \right)} \right]\]</div>
<p>with the average radiated power of a dipole</p>
<div class="math" id="equation-average_radiated_power">
<span class="eqno">(14)</span>\[{P_0} = \frac{{{{\left| {\vec p} \right|}^2}}}{{4\pi {\varepsilon _0}{\varepsilon _c}}}\frac{n_s^3 \omega^3}{3c^3}\]</div>
<p><span class="math">\(\omega\)</span> is the angular frequency and <span class="math">\(c\)</span> the speed of light in free-space.</p>
<p>In free space where <span class="math">\(n_c=n_s=1\)</span>, the intensity for a single dipole under plane polarized incident light can be expressed as <a class="reference internal" href="references.html#sinclair-light-1947" id="id21">[Sin47]</a></p>
<div class="math" id="equation-intensity_dipole">
<span class="eqno">(15)</span>\[{I_0} = \frac{{9{\pi ^2}n_c^4}}{{\lambda^4{f^2}}}{\left[ {\frac{{n_p^2 - n_c^2}}{{n_p^2 + 2n_c^2}}} \right]^2} V^2\]</div>
<p>where <span class="math">\(f=r/\sin(\theta)\)</span> is the distance from the dipole to the focal plane. <span class="math">\(V\)</span> and <span class="math">\(n_p\)</span> correspond to the volume and the refractive index of the particle surrounded by a medium with <span class="math">\(n_c\)</span>.</p>
<p>For perfect constructive interference of many dipoles <span class="math">\(N\)</span>, i.e. the phases of the dipole fields are exactly the same, the total intensity is calculated as the dipole intensity multiplied by the square of the number of dipoles. However, molecules cannot be placed on a single line but rather on zones as defined above (constructive interference rather than perfect phase match). Thus, the total scattered intensity <span class="math">\(I_{sca}\)</span> has to be adjusted with a weighting factor due to the sinusoidal contribution of the particles over the extend of a zone:</p>
<div class="math" id="equation-intensity_sca">
<span class="eqno">(16)</span>\[I_\mathrm{sca} = \left(\frac{2}{\pi} \right)^2 N^2 I_0\]</div>
<p>Inspection of Eqs. <a href="#equation-intensity_dipole">(15)</a> and <a href="#equation-intensity_sca">(16)</a> leads to the conclusion, that two properties of a particle can considerably influence the scattered signal of a collection of particles: On one hand it is the volume <span class="math">\(V\)</span> or the particle radius <span class="math">\(a\)</span>, on the other hand the refractive index <span class="math">\(n_p\)</span>. Assuming the particles are modeled as spheres with a volume <span class="math">\(V=\frac{4}{3}\pi a^3\)</span>, the number of a different particle of type 2 (<span class="math">\(N_2\)</span>) required to obtain the same intensity as for the number of particles of type 1 (<span class="math">\(N_1\)</span>) is</p>
<div class="math" id="equation-equivalentParticles">
<span class="eqno">(17)</span>\[{N_{2}} = {\left( {\frac{{{a_{1}}}}{{{a_{2}}}}} \right)^3}\left| {\frac{{\left( {n_{p1}^2 - n_m^2} \right)\left( {n_{p2}^2 + 2n_m^2} \right)}}{{\left( {n_{p2}^2 - n_m^2} \right)\left( {n_{p1}^2 + 2n_m^2} \right)}}} \right|{N_{1}}\]</div>
<p>Especially the size ratio of comparing particles influences the equivalent amount required. For example, 1000 times fewer particles with a 10 times larger radius were needed for the same signal. In theory, this factor is even higher if the refractive index of the second particle is larger. This principle is used in praxis by secondary labels, i.e. by attaching particles to the target molecule in order to make them visible. <a class="reference internal" href="references.html#id4" id="id22">[GTLCG03]</a></p>
<p>The quantity <span class="math">\(\left[ {n_p^2 - n_c^2}/{n_p^2 + 2n_c^2} \right]^2\)</span> from Eq. <a href="#equation-intensity_dipole">(15)</a> is weakly dependent on the wavelength for proteins (dielectric material). However, this dependency becomes more relevant when the secondary label is a plasmonically active metal such as gold. This is investigated in the <a class="reference internal" href="particle.html#particle"><span>Particle Scattering</span></a>.</p>
<p>» <a class="reference external" href="#">top</a></p>
</div>
</div>
<div class="section" id="implementation">
<h2>Implementation<a class="headerlink" href="#implementation" title="Permalink to this headline">¶</a></h2>
<div class="section" id="module-configuration">
<span id="configuration"></span><h3>Configuration<a class="headerlink" href="#module-configuration" title="Permalink to this headline">¶</a></h3>
<p>With this module, a configuration of different media can be defined. 
It also containes a function to calculate the effective focal point 
that will be measured in praxis.</p>
<p>&#64; author: Silvio Bischof</p>
<div class="admonition-todo admonition" id="index-0">
<p class="first admonition-title">Todo</p>
<p class="last">class isotropic material should be in materials.py (delete completely!)</p>
</div>
</div>
<div class="section" id="module-dipoleField">
<span id="dipolefield-py"></span><h3>dipoleField.py<a class="headerlink" href="#module-dipoleField" title="Permalink to this headline">¶</a></h3>
<p>Dipole emission near planar interfaces according to Novotny and Hecht.</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field-odd field"><th class="field-name">Reference:</th><td class="field-body">Lukas Novotny and Bert Hecht. Dipole emission near</td>
</tr>
</tbody>
</table>
<p>planar interfaces. In Principles of Nano-Optics. Cambridge 
University Press, 2006.</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field-odd field"><th class="field-name">author:</th><td class="field-body">Silvio</td>
</tr>
<tr class="field-even field"><th class="field-name">created:</th><td class="field-body">Thu Mar 31 15:51:53 2016</td>
</tr>
</tbody>
</table>
<dl class="function">
<dt id="dipoleField.calcField">
<code class="descclassname">dipoleField.</code><code class="descname">calcField</code><span class="sig-paren">(</span><em>dipolePositions</em>, <em>dipoleMoments</em>, <em>phase</em>, <em>screen</em>, <em>refractiveIndices</em>, <em>thickness</em>, <em>k_0</em>, <em>Phi</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dipoleField.html#calcField"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dipoleField.calcField" title="Permalink to this definition">¶</a></dt>
<dd><p>calculates the field of a dipole located at dipolePositions at 
the observer position r (point of observation).</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>dipolePositions</strong> &#8211; dipole positions (3d vector x,y,z)</li>
<li><strong>dipoleMoments</strong> &#8211; dipole moments (3d vector x,y,z)</li>
<li><strong>phase</strong> &#8211; phase for the different dipole positions</li>
<li><strong>screen</strong> &#8211; screen as meshgrid</li>
<li><strong>refractiveIndices</strong> &#8211; refractive indices of the media (medium1 (top) - medium3 (bottom))</li>
<li><strong>thickness</strong> &#8211; layer (waveguide) thickness</li>
<li><strong>k_0</strong> &#8211; wave number in air</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first last">3d field vector at the screen</p>
</td>
</tr>
</tbody>
</table>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Only the farfield is considered.</p>
</div>
<p>:reference:: Equation 10.32 Novotny</p>
</dd></dl>

<dl class="function">
<dt id="dipoleField.partialProjector">
<code class="descclassname">dipoleField.</code><code class="descname">partialProjector</code><span class="sig-paren">(</span><em>dipolePositions</em>, <em>dipoleMoments</em>, <em>phase</em>, <em>screen</em>, <em>refractiveIndices</em>, <em>thickness</em>, <em>k_0</em>, <em>Phi</em>, <em>num_process</em>, <em>running_index</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dipoleField.html#partialProjector"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dipoleField.partialProjector" title="Permalink to this definition">¶</a></dt>
<dd><p>Splits the array of the dipole positions in order to avoid memory 
errors and to speed up the simulations (parallel pooling possible).</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first last simple">
<li><strong>dipolePositions</strong> &#8211; positions of the dipoles (ndarray: 3 x n)</li>
<li><strong>dipoleMoments</strong> &#8211; dipole moments corresponding to the positions</li>
<li><strong>phase</strong> &#8211; phase at the given positions</li>
<li><strong>screen</strong> &#8211; screen that is investigated</li>
<li><strong>refractiveIndices</strong> &#8211; refractive indices of the given media (cover, film, substrate)</li>
<li><strong>thickness</strong> &#8211; thickness of the waveguide</li>
<li><strong>k_0</strong> &#8211; wave vector in free space</li>
<li><strong>Phi</strong> &#8211; potentials</li>
<li><strong>num_process</strong> &#8211; amount of processes the calculations shall be split in</li>
<li><strong>running_index</strong> &#8211; the current index for the calculations</li>
</ul>
</td>
</tr>
</tbody>
</table>
</dd></dl>

<dl class="function">
<dt id="dipoleField.potentialsLower">
<code class="descclassname">dipoleField.</code><code class="descname">potentialsLower</code><span class="sig-paren">(</span><em>k_0</em>, <em>z_dipole</em>, <em>theta</em>, <em>refractiveIndices</em>, <em>thickness</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/dipoleField.html#potentialsLower"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#dipoleField.potentialsLower" title="Permalink to this definition">¶</a></dt>
<dd><p>calculates the potentials for the lower half space when dipoles 
lie on a optical interface.</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field-odd field"><th class="field-name">Params k_0:</th><td class="field-body"><p class="first">wave number in free space</p>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>z_dipole</strong> &#8211; z-components of the dipoles</li>
<li><strong>theta</strong> &#8211; outgoing angle of the dipole field</li>
<li><strong>refractiveIndices</strong> &#8211; refractive indices of the cover, film and substrate</li>
<li><strong>thickness</strong> &#8211; thickness of the waveguide</li>
</ul>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">potentials in the upper half space</p>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Reference:</th><td class="field-body"><p class="first last">Equation 10.36-10.38 (mistake in book corrected -&gt; Eq. 10.37 sign)</p>
</td>
</tr>
</tbody>
</table>
</dd></dl>

<p>» <a class="reference external" href="#">top</a></p>
</div>
</div>
</div>


          </div>
        </div>
      </div>
        </div>
        <div class="sidebar">
          <h3>Table Of Contents</h3>
          <p class="caption"><span class="caption-text">Focal Molography Contents</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="getting_started.html">Getting started</a></li>
<li class="toctree-l1"><a class="reference internal" href="introduction.html">Introduction</a></li>
<li class="toctree-l1"><a class="reference internal" href="initialization.html">Initialization</a></li>
<li class="toctree-l1"><a class="reference internal" href="waveguide.html">Dielectric slab waveguide</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="">Interface</a></li>
<li class="toctree-l1"><a class="reference internal" href="mologram.html">Mologram</a></li>
<li class="toctree-l1"><a class="reference internal" href="phasemask.html">Reactive Immersion Lithography and Phase Mask Optimization</a></li>
<li class="toctree-l1"><a class="reference internal" href="particle.html">Particle Scattering</a></li>
<li class="toctree-l1"><a class="reference internal" href="materials.html">Materials</a></li>
<li class="toctree-l1"><a class="reference internal" href="performance.html">Performance</a></li>
<li class="toctree-l1"><a class="reference internal" href="GUI.html">Graphical User Interface (GUI)</a></li>
<li class="toctree-l1"><a class="reference internal" href="cuda.html">Accelerating Molography Simulations</a></li>
<li class="toctree-l1"><a class="reference internal" href="miscellaneous.html">Miscellaneous</a></li>
<li class="toctree-l1"><a class="reference internal" href="binder.html">Binder</a></li>
<li class="toctree-l1"><a class="reference internal" href="Variable_management.html">Variables and Datamanagement</a></li>
<li class="toctree-l1"><a class="reference internal" href="analysis.html">Analysis</a></li>
<li class="toctree-l1"><a class="reference internal" href="analytical.html">Analytical</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">References</a></li>
</ul>
<p class="caption"><span class="caption-text">Bragg Molography Contents</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="braggmolography.html">Bragg-molography</a></li>
</ul>
<p class="caption"><span class="caption-text">Fiber Bragg Molography Content</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="CoupledModeTheoryCylindricalWaveguides.html">Coupled Mode Theory Cylindrical Waveguides</a></li>
<li class="toctree-l1"><a class="reference internal" href="CylindricalWaveguide.html">Cylindrical Waveguide</a></li>
</ul>

          <div role="search">
            <h3 style="margin-top: 1.5em;">Search</h3>
            <form class="search" action="search.html" method="get">
                <input type="text" name="q" />
                <input type="submit" value="Go" />
                <input type="hidden" name="check_keywords" value="yes" />
                <input type="hidden" name="area" value="default" />
            </form>
            <p class="searchtip" style="font-size: 90%">
                Enter search terms or a module, class or function name.
            </p>
          </div>
        </div>
        <div class="clearer"></div>
      </div>
    </div>

    <div class="footer-wrapper">
      <div class="footer">
        <div class="left">
          <div role="navigation" aria-label="related navigaton">
            <a href="waveguide.html" title="Dielectric slab waveguide"
              >previous</a> |
            <a href="mologram.html" title="Mologram"
              >next</a> |
            <a href="py-modindex.html" title="Python Module Index"
              >modules</a> |
            <a href="genindex.html" title="General Index"
              >index</a>
          </div>
          <div role="note" aria-label="source link">
              <br/>
              <a href="_sources/interface.txt"
                rel="nofollow">Show Source</a>
          </div>
        </div>

        <div class="right">
          
    <div class="footer" role="contentinfo">
        &copy; Copyright 2016, Silvio Bischof.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 1.3.6.
    </div>
        </div>
        <div class="clearer"></div>
      </div>
    </div>

  </body>
</html>