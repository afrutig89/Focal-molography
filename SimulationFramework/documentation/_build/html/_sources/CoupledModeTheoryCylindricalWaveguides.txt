.. _CoupledModeTheoryCylindricalWaveguides:


Coupled Mode Theory Cylindrical Waveguides
===========================================


Ideal Mode Expansion
--------------------
In a perfect waveguide, modes propagate undisturbed. At imperfections such as bends, alternation of core diameter or index inhomogeneities, the wave form is altered by coupling to different modes. In this process, power is transferred from the initial guided and radiation modes to other guided or radiation modes. There are different ways of describing mode coupling. In this chapter, the ideal mode expansion is used which is the preferred choice in case of refractive index inhomogeneities. [#marcuse_theory_1974] A perfect waveguide is considered for which the eigenvalue equations can be solved for each mode. Thus, the electromagnetic field of all ideal modes is defined. The field of the actual waveguide is expressed as a linear combination of the modes in the ideal waveguide.

:math:`\vec{E_{t}}=\sum\limits _{\nu=0}^{N-1}\left(c_{\nu}^{(+)}e^{i{\beta_{\nu}}z}+c_{\nu}^{(-)}e^{-i{\beta_{\nu}}z}\right){\vec{\mathscr{E}}_{\nu t}}+\sum\limits _{\nu=0}^{N-1}{\int{\left(c_{\rho}^{(+)}e^{i{\beta_{\rho}}z}+c_{\rho}^{(-)}e^{-i{\beta_{\rho}}z}\right)\vec{\mathscr{E}_{\rho t}}d\rho}}`
:math:`\vec{H_{t}}=\sum\limits _{\nu=0}^{N-1}\left(c_{\nu}^{(+)}e^{i{\beta_{\nu}}z}+c_{\nu}^{(-)}e^{-i{\beta_{\nu}}z}\right){\vec{\mathscr{E}}_{\nu t}}+\sum\limits _{\nu=0}^{N-1}{\int{\left(c_{\rho}^{(+)}e^{i{\beta_{\rho}}z}+c_{\rho}^{(-)}e^{-i{\beta_{\rho}}z}\right)\vec{\mathscr{H}{}_{\rho t}^{*}}d\rho}}`


 The first terms include all guided modes, while the second terms contain the radiation modes. The subscript :math:`\nu`is the index of a guided mode, whereas :math:`\rho` stands for a radiation mode. :math:`\mathscr{E}` and `\mathscr{H}` are the electric and magnetic fields in the ideal waveguide. :math:`c^{+}_\nu` and :math:`c^{-}_\nu` are the expansion coefficients, where the (+) and (-) refer to the propagation direction of the mode. The subscript :math:`t` stands for the transverse direction of the fields. The fields in :math:`z` direction can be found from the transverse fields using Maxwell's equations (see equations (3.2-16) and (3.2-17) in reference [#marcuse_theory_1974]). In our case, the radiation modes can be ignored, because we are only interested in guided light. Thus, the second term can be omitted. With the orthogonality relation

:math:`\int_{-\infty}^{+\infty}{\int_{-\infty}^{+\infty}{{e_{z}}\cdot\left({\vec{\mathscr{E}_{\nu t}}\times\vec{\mathscr{H}{}_{\mu t}^{*}}}\right)dxdy}}=2{s_{\mu}}\frac{{\beta_{\mu}^{*}}}{{\left|{\beta_{\mu}}\right|}}P_{in}{\delta_{\mu\nu}}`

 a relation between the electric field before coupling :math:`c_\nu` and the magnetic field after coupling :math:`c_\mu` is given. The coefficient :math:`s_\mu` has a value of one for real values of :math:`\beta_\mu`. :math:`P_{in}` is the power of the input wave. With Eqs. [eq: linear combination of modes E], [eq: linear combination of modes H], [eq: orthogonality relation] and Maxwell's Eq. [eq: Maxwell equation Curl E] and [eq:Maxwell equation culrl B] a differential equation for the expansion coefficients can be derived, as explained in detail in reference [#marcuse_theory_1974].
 :math:`{\frac{{dc_{\mu}^{(+)}}}{{dz}}=\sum\limits _{\nu}{K_{\mu\nu}^{(+,+)}c_{\nu}^{(+)}e^{{i({\beta_{\mu}}-{\beta_{\nu}})z}}+}K_{\mu\nu}^{(+,-)}c_{\nu}^{(-)}}e^{{i({\beta_{\mu}}+{\beta_{\nu}})z}}`
 :math:`\frac{{dc_{\mu}^{(-)}}}{{dz}}=\sum\limits _{\nu}{K_{\mu\nu}^{(-,+)}c_{\nu}^{(+)}e^{{-i({\beta_{\mu}}+{\beta_{\nu}})z}}+}K_{\mu\nu}^{(-,-)}c_{\nu}^{(-)}e^{{-i({\beta_{\mu}}-{\beta_{\nu}})z}}}`
 where :math:`K_{\mu\nu}^{(p,q)}` is the coupling coefficient
 :math:`K_{\mu\nu}^{(p,q)}  =   \left(\frac{{\omega{\varepsilon_{0}}}}{{4i{s_{\mu}}P_{in}}}\right)\int\limits _{-\infty}^{\infty}{\int\limits _{-\infty}^{\infty}{\left({{n^{2}}-n_{0}^{2}}\right)\left[{\frac{{\beta_{\mu}^{*}}}{{\left|{\beta_{\mu}^{(p)}}\right|}}\vec{\mathscr{E}_{\mu t}^{(p)*}}\cdot\vec{\mathscr{E}_{\mu t}^{(q)}}+\frac{{\beta_{\mu}}}{{\left|{\beta_{\mu}^{(p)*}}\right|}}{\ensuremath{\frac{{n_{0}^{2}}}{{n^{2}}}}}\mathscr{E}_{\mu z}^{(p)*}\mathscr{E}_{\mu z}^{(q)}}\right]dx}dy}`
        
 :math:`n_0` is the refractive index in the ideal waveguide and :math:`n` is the index in the actual waveguide. Both are functions of position and should actually be written as :math:`n(x,y,z)`.

These equations can be solved at each point in z. Then the expansion coefficient can be used to calculate the power carried by each mode after coupling. [#marcuse_theory_1974]
:math:'P_{\mu}=\left|{c_{\mu}}\right|^{2}P_{{\rm in}}`
 


Fig 1a shows the approximation of an ideal waveguide which will be used for our calculations. Fig 1b illustrates the actual waveguide. It also shows the coupling of different modes and depicts which coupling we consider and which is neglected. In our device, the light initially propagates in a single mode fiber, meaning that only one mode is allowed. After the point where the cladding is removed, more modes are allowed. In addition, the imperfection of the taper compared to the ideal waveguide will result in coupling of the fundamental mode into higher order modes. However, this coupling is expected to be weak because only the evanescent field is affected. Thus, as initial mode we only consider the fundamental mode. For the same reason, the coupling within the fiber-Bragg grating is expected to be weak. Thus, :math:`c_{\nu}^{(+)}` is assumed to be constant over the entire grating. Because it carries the entire power of the launched mode it has the value one. For the reflected wave we are only interested in the fundamental mode because this is the only mode allowed in the single mode part of the fiber. The coupling of higher order modes into the fundamental mode at the end of the cladding can be neglected for the same reason as for the incoming mode.

|
.. _waveguides:

.. figure:: figures/Waveguide_with_modes.pdf
    :width: 100 %

    a) ideal waveguide b) actual waveguide with mode coupling (full lines: zeroth order mode, dashed lines: higher order modes, red arrows: coupling taken into account, gray arrows: coupling neglected)

|

With these assumptions, the calculation of the reflected power becomes much easier. We only need to consider the first term of Eq. [eq: differential equation c -] and only for the fundamental mode. The fundamental or dominant mode of an optical fiber is the :math:`HE_{11}` mode, with :math:'\nu' being one. [#marcuse_theory_1974] The same is valid for the reflection, meaning that :math:`\mu` is one. Thus, we have to calculate :math:`K_{1,1}^{(-,+)}`.

In Eq. [eq: coupling coefficient], the term :mat:`{{n^{2}}-n_{0}^{2}}` is zero wherever there is no difference in refractive index between the real waveguide and the actual waveguide. Since we neglect the cladding, this is the case everywhere except in the protein grating. In the regions of adsorbed proteins, :math:`{{n^{2}}-n_{0}^{2}}` has the constant value :math:`{n_{{\rm solv}}^{2}}-n_{{\rm layer}}^{2}`. Thus, we can take this term out of the integral and integrate only over the area of the polymer grating. The profile is approximated by a canonical grating expressed as f(z), which is a square wave function of height :math:`\delta` and period :math:`\frac{\lambda}{2n_{{\rm eff}}}`. By transforming the integral in [eq: coupling coefficient] into polar coordinates and replacing the integral boundaries as explained, we get for the dominant mode

:math:`K_{1,1}^{(-,+)}=\frac{{\omega{\varepsilon_{0}}\left({n_{{\rm solv}}^{2}-n_{{\rm layer}}^{2}}\right)}}{{2iP_{{\rm in}}}}\int\limits _{a}^{a+f(z)}{\int\limits _{0}^{2\pi}{r\left({\mathscr{E}{}_{1,r}^{*}{\mathscr{E}_{1,r}}+\mathscr{E}_{1,\phi}^{*}{\mathscr{E}_{1,\phi}}+\frac{{n_{{\rm solv}}^{2}}}{{n_{{\rm wg}}^{2}}}\mathscr{E}_{1,z}^{*}{\mathscr{E}_{1,z}}}\right)}}d\phi dr`
 
 because :math:`\beta_0` is real and positive and :math:`s_{\mu}` is of value one. :math:`\mathscr{E}{}_{1,r}` is the electric field in the radial direction and :math:`\mathscr{E}_{1,\phi}` in the tangential direction. 

For small deviations of the ideal waveguide it is reasonable to assume the electric field is constant over the distortion in radial direction. We take the field at the core radius a. Thus, we can separate the two integrals

:math:`K_{1,1}^{(-,+)}=\frac{2\pi{\omega{\varepsilon_{0}}\left({n_{{\rm solv}}^{2}-n_{{\rm layer}}^{2}}\right)}}{{2iP_{{\rm in}}}}\int\limits _{a}^{a+f(z)}{r}dr\cdot\int\limits _{0}^{2\pi}E{}_{1,r}^{*}(a){\mathscr{E}_{1,r}(a)}+\mathscr{E}_{1,\phi}^{*}(a){\mathscr{E}_{1,\phi}(a)}+\frac{{n_{{\rm solv}}^{2}}}{{n_{{\rm layer}}^{2}}}\mathscr{E}_{1,z}^{*}(a){\mathscr{E}_{1,z}(a)}d\phi.`

We can easily solve integral over the radius. It is :math:`\frac{1}{2}\left(2af(z)+f(z)^{2}\right)`. Because the grating has a small height, we can omit :math:`f(z)^{2}`. :math:`K_{1,1}^{(-,+)}` can be written as :math:`K_{1,1}^{(-,+)}=\hat{K}_{1,1}^{(-,+)}f(z)` where :math:`\hat{K}_{1,1}^{(-,+)}` is the z independent part of :math:`K_{1,1}^{(-,+)}`
 

:math:`\hat{K}_{1,1}^{(-,+)}=\frac{2a\pi{\omega{\varepsilon_{0}}\left({n_{{\rm solv}}^{2}-n_{{\rm layer}}^{2}}\right)}}{{2iP_{{\rm in}}}}\int\limits _{0}^{2\pi}\mathscr{E}{}_{1,r}^{*}(a,\phi){\mathscr{E}_{1,r}(a,\phi)}+\mathscr{E}_{1,\phi}^{*}(a,\phi){\mathscr{E}_{1,\phi}(a,\phi)}+\frac{{n_{{\rm solv}}^{2}}}{{n_{{\rm layer}}^{2}}}\mathscr{E}_{1,z}^{*}(a,\phi){\mathscr{E}_{1,z}(a,\phi)}d\phi`

and f(z) is the profile of the grating. Now we can solve the differential Eq. [eq: differential equation c -] by integration over the grating length L. As already mentioned before we only need to consider the first term and only the dominant mode. We can consider the expansion coefficient of incoming mode :math:`c_{\nu}^{(+)}` to be of value one because it is the only mode we allow for. Thus, we get 

:math:`{c_{1}^{(-)}=\hat{K}_{1,1}^{(-,+)}\int_{0}^{L}{f(z)e^{{-i({\beta_{\mu}}+{\beta_{\nu}})z}}dz}}`
 



Implementation
--------------

.. automodule:: idealModeExpansion

.. autoclass:: IdealModeExpansion.IdealModeExpansionCylinderBragg
    :members:


» `top`_

.. _top: #