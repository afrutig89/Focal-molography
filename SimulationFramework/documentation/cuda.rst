Accelerating Molography Simulations
===================================

Field computation
-----------------

Every scatterer on the mologram produces a scattering field, in spherical coordinates, given by Eq. :eq:`mologramscattering`

.. math::
    \vec E = \left[ \begin{array}{c} {E_\theta} \\ {E_\phi} \end{array} \right] = \frac{k_c^2}{4\pi\epsilon_0\epsilon_c}\frac{e^{ik_s r}}{r} \left[ \begin{array}{c} {(p_x\cos(\phi) + p_y\sin(\phi))\cos(\theta)\Phi_s^{(2)}-p_z\sin(\theta)\Phi_s^{(1)}} \\ {-(p_x\sin(\phi)-p_y\cos(\phi))\Phi_s^{(3)} } \end{array} \right]
    :label: mologramscattering


which is captured on a screen somewhere in space, characterized by a number of pixels :math:`N_{pix}=n_x\cdot n_y` and their respective spatial postions :math:`\{ x_i,y_i,z_i\}^{N_{pix}}`

Efficient calculations
^^^^^^^^^^^^^^^^^^^^^^

Hierarchy of quantities
"""""""""""""""""""""""

The calculations go from scatterer to scatterer and calculate for every screen pixel the contributed scattering. The quantities in Eq. :eq:`mologramscattering` are grouped into 3 categories:

.. math::
    \underbrace{k_c^2, 4\pi \epsilon_0\epsilon_c}_{\text{Independent quantities}}\quad \quad  \underbrace{p_x,p_y,p_z}_{\text{Scatterer quantities}} \quad \quad\underbrace{r,\phi,\theta,\Phi_s}_{\text{Scatter-screen quantitites}}

The independent quantities can simply multiply the final intensity.


Splitting Real and Imaginary
""""""""""""""""""""""""""""

In order to avoid any overhead from using Cuda's Complex library (not fast-math optimized), the calculation will be broken down into it's real and imaginary part. Equation :eq:`mologramscattering` contains a product of 3 complex numbers: the first complex exponential, the polarization :math:`\vec{p}`, and the :math:`\Phi` fields.

.. math::
    \text{Re}\left\{\prod_{i=1}^3 a_i+j\cdot b_i\right\} = a_1a_2a_3-a_1b_2b_3-a_2b_1b_3 -a_3b_1b_2 \\
    \text{Imag}\left\{\prod_{i=1}^3 a_i+j\cdot b_i\right\} =a_2a_3b_1 + a_1a_3b_2 + a_1a_2b_3 - b_1b_2b_3

with :math:`a_1= \cos(k_sr), b_1= \sin(k_sr)` and :math:`a_2,b_2` the real and imaginary parts of the different polarizations, :math:`a_3,b_3` the real and imaginary parts of the different Fields. The :math:`\Phi` fields are costly to calculate, but are quite smooth in function of :math:`\theta`. Therefore, the constant part is precomputed on a restricted number of angles and then simply linearly interpolated for the necessary angle.
Defining :math:`s_z:=\sqrt{ \left(n_c/n_s\right)^2 - \sin^2(\theta)}`

.. math::
    \Phi_s^{(i)} = \underbrace{\bar{f}^{(i)}(\theta)}_{\text{complex,interpolated}}e^{jk_sz_{scat}s_z}


Threads and memory access
"""""""""""""""""""""""""

Every processor (thread) on the Graphical Processing Unit (GPU) can execute one calculation on its own. For example, every thread could snatch one scatterer, and then in a loop calculate the contribution of the scatterer on every pixel. Therefore, if 1000 Threads are working in parallel on :math:`10^6` scatterers, the calculation would be done in a 1000 steps, 1000 scatterers at a time, but every pixel property (location) would be queried in memory :math:`10^6` times.

Instead, we structure our threads in blocks of (16,16) threads (256 total, seems to be the most efficient here). Every thread queries the pixel values of the screen, and then goes through all the scatterers. The scatterer positions are queried anyway :math:`10^6` times, but now the screen only :math:`1000` times. So, the calculation takes a subset of 16x16 pixels of the screen, and calculates the contribution of every scatterer at that point, then moves on the next subset, as seen in :num:`Fig. #cudascreen`

|
.. _cudaScreen:

.. figure:: figures/CUDA_screen.png
    :width: 100 %

    Thread operation

|


Limitations
"""""""""""

We compute the solution for the focus spot in the lower Z half of the plane, in the region inside the critical angle (where :math:`s_z` is real). The screen should be given in multiples of 16 to make sure the threads do not try to access unallocated memory. The code could be changed to lift this limitation (easily).

There are no limitations on the number of scatterers.


Mologram creation
^^^^^^^^^^^^^^^^^

On inverse cumulative distribution sampling and arc-length parametrization
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

The masked-array approach implemented before starts to become very time consuming for large scatterers, and unnecessarily slow for a small amount of scatterers. Furthermore, for most simulations, the mololines do not change. It is enough to generate them once at load up: if the refractive indices and the wave guide and the coupled wave's wavelength do not change, the mologram is the same.

First, every mololine is computed and stored as an array of a set length (to store all the lines in a matrix). This is done by calculating the crossing of the mololines (hyperbolas) with a set of three circles : the outer one, and the two bragg circle cutouts. The result can be seen on :num:`Fig. #mologram`. The shaded red regions correspond to the lines.

|
.. _mologram:

.. figure:: figures/upper_mololines_with_sine_seed_across_x_width_binding02.png
    :width: 100 %

    Mololines in red. The blue particles are the scattering centers. The binding probabiltiy was 0.2 here. The mologram is shown at the sensitive transition: where the 2 bragg circles cut out the middle of the mologram

|

Every mololine in the positive (upper) half-plane can be seen as a curve :math:`\vec{c}(x) := (x,f(x))`. However, in order to place a scatterer uniformly along the line, one cannot simply randomly seed :math:`x` on its interval, since the curve moves much faster in (x,y) space on certain intervals of :math:`x`, depending on its gradient. Therefore, some regions would be more densely seeded than others, as can be seen on the curve of Figure \ref{curve} which maps :math:`\vec{c}(t) := (\sin(t/4), e^{-\left[(t-4\pi)/(16\pi)\right]^2}/10)`

|

.. |logo1| image:: figures/non_uniform_sampling.png
.. |logo2| image:: figures/inversecdfsampling.png
.. |logo3| image:: figures/uniform_sampling.png

+---------+---------+---------+
| |logo1| | |logo2| | |logo3| |
+---------+---------+---------+

Left: :math:`\vec{c}(t)` is sampled by uniformly sampling :math:`t` on its interval. Middle: the arclength introduces cartesian measure onto the curve. The formula is simply the cumulative distribution function of the arclength with respect to :math:`t`. Inverse cdf sampling simply means that one now samples the cdf to get distributions of :math:`t`. Right: randomly sampled curve by sampling arc-length. The sampling is much more homogeneous, because now the points are 'uniformly' spaced (from a probability distribution point of view) along the curve.

|
 
For example, if one wishes to sample from a sinusoidal distribution :math:`p(x) = \sin(x), x \in [0,\pi]`, then the normalized cumulative distribution function is :math:`C(x) = 1/2\int \limits_0^x \sin(y)dy = \frac{1-\cos(x)}{2}` which is :math:`C: \left[0,\pi\right] \to \left[0,1\right]`. Conversely, :math:`C^{-1} : \left[0,1\right] \to \left[0,\pi\right]`. So, sampling :math:`C^{-1}(y)` where :math:`y` is a uniform random variable will produce :math:`x` values which are distributed sinusoidally. In this case, :math:`C^{-1}` can be computed analytically  :math:`C^{-1}(x) = \arccos(1-2x)`, and the results can be seen on :num:`Fig. #cdfsampling`

|
.. _cdfsampling:

.. figure:: figures/cdfsampling.png
    :width: 100 %

    The inverse cumulative distribution function is sampled uniformly on its :math:`x`-axis between 0 and 1. One can see the red shade is uniform. The values the sampling produce on the curve are then distributed between 0 and :math:`\pi` sinusoidally, as the blue shade and its histogram show.

|

Procedure
"""""""""

#. Compute the mololines as a matrix of :math:`x` and :math:`y` coordinates. Calculate their lengths so as to have a vector :math:`\{ l_0, l_1, ... , l_n\}`. Normalize the length vector to 1, and use this vector as a weight vector for weighted sampling of a given line.
#. For a chosen line :math:`j`, sample a uniform random number on 0-1, and calculate which entry of the CDF of the :math:`j`-th mololine it cuts. This puts the particle on the middle of the line at position x,y corresponding to that entry.
#. With probability :math:`p` of binding, seed the particle sinusoidally with respect to the middle of the line on an interval the width of the linewidth. With probabiltiy :math:`(1-p)` seed the particle sinusoidally to the left of the mololine, on an interval the size of the gap.