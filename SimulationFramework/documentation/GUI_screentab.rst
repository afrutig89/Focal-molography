Screen Tab
===============

Tab for screen settings and camera model selection. 


.. automodule:: GUI.screenTab.screen_widget
   :members:



CameraSensorModel
------------------

.. automodule:: camerasimulation.CameraSensorModels
   :members:

CameraSimulation
-----------------

.. automodule:: camerasimulation.CameraSensorSimulation
   :members: