.. _interface:

Interface
=========

Theory
------


Dipole emission near planar interfaces
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Molecules that are arranged in a diffraction pattern are directly attached to the surface. It is therefore important to know how the particles interact with an interface in close proximity (few tens of nm). This issue was extensively investigated by Novotny :cite:`novotny_allowed_1997`, :cite:`novotny_allowed_1997-1` and is briefly summarized in this section.

:num:`Fig. #dipoleinterface` illustrates a dipole placed near a planar interface, e.g. a waveguide. The total energy dissipation of an illuminated dipole will be split into different radiated parts, namely, into the upper and lower half-space (:math:`P^{\uparrow}, P^{\downarrow}`) and into the waveguide (:math:`P_m`). The radiated power in the lower half-space can be further subdivided into allowed and forbidden zones :math:`P_a^{\downarrow.}` and :math:`P_f^{\downarrow}`. In the forbidden zone, the waves are evanescent and decay exponentially from the waveguide. :cite:`lukosz_light_1977` In the following, the terminology for planar waveguides is used: the upper and the lower medium correspond to the cover and the substrate with refractive index :math:`n_c` and :math:`n_s`, respectively, and the interface to the film with refractive index :math:`n_f` (:num:`Fig. #dipoleinterface`\b).

|
.. _dipoleInterface:

.. figure:: figures/Dipole_interface.png
	:width: 100 %

	Dipole radiation near a planar interface. a) A dipole placed on a waveguide (position :math:`(x_0,y_0,z_0)`) radiates differently into the different media (figure adapted from :cite:`novotny_dipole_2006`). A part of the radiated power couples into the waveguide (:math:`P_m`) and the rest is emitted into the two half-spaces (:math:`P^{\uparrow}, P^{\downarrow}`), whereas the power in the lower medium can be divided into forbidden :math:`(P_f^{\downarrow})` and allowed zones :math:`(P_a^{\downarrow.})`. b) The dipole is located in a medium with refractive index :math:`n_c` at a height :math:`z_0` on a waveguide (:math:`n_f`) with thickness :math:`d_f`. Depending on the radiation angle (only one angle :math:`\theta` shown illustratively) and the refractive index difference, the fraction of transmitted and reflected power varies.

|


Important for this work is the power radiated into the allowed zone since the signal is measured in this region. Moreover, the detection system is placed in the far-field, i.e. at a distance much greater than the wavelength. For this case the electric field in spherical coordinates (:math:`r`, :math:`\theta`, :math:`\phi`) originating from a dipole can be expressed as: :cite:`novotny_dipole_2006`

.. math::
	\vec E = \left[ {\begin{array}{*{20}{c}}
	{{E_\theta }}\\
	{{E_\phi }}
	\end{array}} \right] = \frac{{k_c^2}}{{4\pi {\varepsilon _0}{\varepsilon _c}}}\frac{{\exp (i{k_s}r)}}{r}\left[ {\begin{array}{*{20}{c}}
	{\left( {{p_x}\cos \phi  + {p_y}\sin \phi } \right)\cos \theta \,\Phi _s^{(2)} - {p_z}\sin \theta \,\Phi _s^{(1)}}\\
	{ - \left( {{p_x}\sin \phi  - {p_y}\cos \phi } \right)\,\Phi _s^{(3)}}
	\end{array}} \right]
	:label: elField

with the dipole moment :math:`\vec{p}=(p_x,p_y,p_z)`

.. math:: \vec p = {\varepsilon _0}{\varepsilon _m}\alpha {{\vec E}_0}
	:label: dipoleMoment

:math:`\varepsilon_c` denotes the relative permittivity of the medium that surrounds the dipole, i.e. the cover medium. The electric far-field has no radial component, i.e. it is only transverse since the longitudinal part decays rapidly (:math:`1/r^3`). :cite:`stratton_radiation_2015`

The potentials :math:`\Phi_s^{(1)-(3)}` are defined as\footnote{Note: The second potential (Eq. (10.37)) reported in :cite:`novotny_dipole_2006` has the wrong sign. The potentials from Eqs. :eq:`potentials1` to :eq:`potentials3` are corrected}

.. math::
	\Phi _s^{(1)} = \frac{{{n_s}}}{{{n_c}}}\frac{{\cos \theta }}{{{{\tilde s}_z}(\theta )}}{t^{(p)}}(\theta ){e^{i{k_s}\left( {{z_0}{{\tilde s}_z}(\theta ) + d \cos \theta } \right)}}
	:label: potentials1

.. math::
	\Phi _s^{(2)} = \frac{{{n_s}}}{{{n_c}}}{t^{(p)}}(\theta ){e^{i{k_s}\left( {{z_0}{{\tilde s}_z}(\theta ) + d \cos \theta } \right)}}
	:label: potentials2

.. math::
	\Phi _s^{(3)} = \frac{{\cos \theta }}{{{{\tilde s}_z}(\theta )}}{t^{(s)}}(\theta ){e^{i{k_s}\left( {{z_0}{{\tilde s}_z}(\theta ) + d \cos \theta } \right)}}
	:label: potentials3

with

.. math::
	{{\tilde s}_z}(\theta ) = \sqrt {{{\left( {\frac{{{n_c}}}{{{n_s}}}} \right)}^2} - {{\sin }^2}\theta } 
	:label: tilde_s

The potential :math:`\Phi_s^{(1)}` describes a vertically oriented dipole, whereas potentials :math:`\Phi_s^{(2)}` and :math:`\Phi_s^{(3)}` characterize a p-polarized and s-polarized horizontal dipole. The amount of transmitted and reflected power is dependent on the radiation angle :math:`\theta` and the refractive indices of the media (:num:`Fig. #dipoleinterface`\b). Furthermore, an additional phase shift is introduced due to the dipole's vertical position :math:`z_0` and the waveguide thickness :math:`d_f`.

If the potentials are set to unity, the electric field from Eq. :eq:`elField` corresponds to the dipole radiation in free space. Therefore, the optical interface introduces an amplification of the radiated power into the lower half-space as can be observed in :num:`Fig. #potentials`.

|
.. _potentials:

.. figure:: figures/potentials.png
	:width: 100 %

	Potentials for a dipole near a planar interface. The intensity in the lower half space of a vertically oriented dipole is described by the square of the potential :math:`\left|\Phi_s^{(1)}\right|` (a), whereas the intensity of a horizontal dipole can be calculated with help of :math:`\left|\Phi_s^{(2)}\right|^2` and :math:`\left|\Phi_s^{(3)}\right|^2` with respect to the polarization. In free-space, these potentials equal unity, therefore the radiation into the lower half-space is higher if radiating into a denser medium. The refractive indices used for this plot are :math:`n_c=1.33`, :math:`n_f=2.117`, and :math:`n_s=1.521`, respectively.

|

The potentials :math:`\Phi_s^{(1)-(3)}` are functions of the Fresnel transmission coefficients :math:`t^{(p)}` and :math:`t^{(s)}`, which define the amount of power that is transmitted for parallel and perpendicularly polarized plane waves. The Fresnel coefficients of a two-interface structure are derived by Lukosz :cite:`lukosz_light_1981` as

.. math::
	{t^{(p,s)}} = \frac{{t_{1,2}^{(p,s)}t_{2,3}^{(p,s)}\exp \left( {i{k_{z2}}d} \right)}}{{1 + r_{1,2}^{(p,s)}r_{2,3}^{(p,s)}\exp \left( {2i{k_{z2}}d} \right)}}
	:label: transmissionSingleLayer

where the Fresnel transmission coefficients for a single interface are dependent on the z-component of the wave vector :math:`\vec k = (k_x, k_y, k_z)` in the different media (1,2) such as on the corresponding permeability :math:`\mu_{1,2}` and permittivity :math:`\varepsilon_{1,2}`:

.. math::
	t_{1,2}^{(s)}(\theta ) = \frac{{2{\mu _2}{k_{z1}}}}{{{\mu _2}{k_{z1}} + {\mu _1}{k_{z2}}}}
	:label: Fresnel_transmission_p

.. math::
	t_{1,2}^{(p)}(\theta ) = \frac{{2{\varepsilon _2}{k_{z1}}}}{{{\varepsilon _2}{k_{z1}} + {\varepsilon _1}{k_{z2}}}}\sqrt {\frac{{{\mu _2}{\varepsilon _1}}}{{{\mu _1}{\varepsilon _2}}}} 
	:label: Fresnel_transmission_s

.. math::
	r_{1,2}^{(s)}(\theta ) = \frac{{{\mu _2}{k_{{z_1}}} - {\mu _1}{k_{z2}}}}{{{\mu _2}{k_{z1}} + {\mu _1}{k_{z2}}}}
	:label: Fresnel_reflection_p

.. math::
	r_{1,2}^{(p)}(\theta ) = \frac{{{\varepsilon _2}{k_{z1}} - {\varepsilon _1}{k_{z2}}}}{{{\varepsilon _2}{k_{z1}} + {\varepsilon _1}{k_{z2}}}}
	:label: Fresnel_reflection_s

:num:`Fig. #fresnelcoefficients` shows that for similar refractive indices of cover and substrate (:math:`n_c=1.33`, :math:`n_s=1.521`), the Fresnel coefficients for a thin single layer (solid curves) do not differ significantly compared to the case of a single interface between cover and substrate (dashed curves). Also, both the transmission and the reflection are only slightly dependent on the polarization for small angles (parallel: blue, perpendicular: green).

|
.. _FresnelCoefficients:

.. figure:: figures/Fresnel_Coefficients.png
	:width: 100 %

	Absolute values of the Fresnel coefficients for a planar layered medium (solid) and single interface (dashed) according to Eqs. :eq:`transmissionSingleLayer` to :eq:`Fresnel_reflection_s` for parallel polarization (blue) and perpendicular polarization (green) as a function of the angle :math:`\theta`. The values used for these curves are :math:`n_c = 1.33`, :math:`n_f=2.117`, :math:`n_s=1.521` and :math:`d_f=145` nm.

|

While the electric field has to be summed up for a coherent signal, what is effectively measured by a detection system is the radiated power passing through an area (pointing flux). This is represented by the intensity :math:`I`:

.. math::
	I = \frac{1}{2}\sqrt {\frac{{{\varepsilon _0}{\varepsilon _s}}}{{{\mu _0}{\mu _s}}}} E{E^*}
	:label: intensity_general

whereas the asterisk denotes the complex conjugate. To keep it simple, only a y-directed excitation field :math:`E_y` (plane polarized incident wave) is analyzed (complete analysis can be found in :cite:`novotny_dipole_2006`). With the given simplification, inserting Eq. :eq:`elField` into Eq. :eq:`intensity_general` gives an expression for the intensity of a perpendicularly polarized horizontal dipole in the lower medium

.. math::
	{I_0} = \frac{3}{{8\pi {r^2}}}\frac{n_s}{n_1}\frac{{{P_0}}}{{{{\left| {\vec p} \right|}^2}}}\left[ {p_y^2\left( {{{\sin }^2}\phi \, {{\cos }^2}\theta \,{{\left| {\Phi _s^{(2)}} \right|}^2} + {{\cos }^2}\phi \,{{\left| {\Phi _s^{(3)}} \right|}^2}} \right)} \right]
	:label: dipole_intensity_TE

with the average radiated power of a dipole

.. math::
	{P_0} = \frac{{{{\left| {\vec p} \right|}^2}}}{{4\pi {\varepsilon _0}{\varepsilon _c}}}\frac{n_s^3 \omega^3}{3c^3}
	:label: average_radiated_power

:math:`\omega` is the angular frequency and :math:`c` the speed of light in free-space.

In free space where :math:`n_c=n_s=1`, the intensity for a single dipole under plane polarized incident light can be expressed as :cite:`sinclair_light_1947`

.. math::
	{I_0} = \frac{{9{\pi ^2}n_c^4}}{{\lambda^4{f^2}}}{\left[ {\frac{{n_p^2 - n_c^2}}{{n_p^2 + 2n_c^2}}} \right]^2} V^2
	:label: intensity_dipole

where :math:`f=r/\sin(\theta)` is the distance from the dipole to the focal plane. :math:`V` and :math:`n_p` correspond to the volume and the refractive index of the particle surrounded by a medium with :math:`n_c`.

For perfect constructive interference of many dipoles :math:`N`, i.e. the phases of the dipole fields are exactly the same, the total intensity is calculated as the dipole intensity multiplied by the square of the number of dipoles. However, molecules cannot be placed on a single line but rather on zones as defined above (constructive interference rather than perfect phase match). Thus, the total scattered intensity :math:`I_{sca}` has to be adjusted with a weighting factor due to the sinusoidal contribution of the particles over the extend of a zone:

.. math::
	I_\mathrm{sca} = \left(\frac{2}{\pi} \right)^2 N^2 I_0
	:label: intensity_sca


Inspection of Eqs. :eq:`intensity_dipole` and :eq:`intensity_sca` leads to the conclusion, that two properties of a particle can considerably influence the scattered signal of a collection of particles: On one hand it is the volume :math:`V` or the particle radius :math:`a`, on the other hand the refractive index :math:`n_p`. Assuming the particles are modeled as spheres with a volume :math:`V=\frac{4}{3}\pi a^3`, the number of a different particle of type 2 (:math:`N_2`) required to obtain the same intensity as for the number of particles of type 1 (:math:`N_1`) is

.. math::
	{N_{2}} = {\left( {\frac{{{a_{1}}}}{{{a_{2}}}}} \right)^3}\left| {\frac{{\left( {n_{p1}^2 - n_m^2} \right)\left( {n_{p2}^2 + 2n_m^2} \right)}}{{\left( {n_{p2}^2 - n_m^2} \right)\left( {n_{p1}^2 + 2n_m^2} \right)}}} \right|{N_{1}}
	:label: equivalentParticles

Especially the size ratio of comparing particles influences the equivalent amount required. For example, 1000 times fewer particles with a 10 times larger radius were needed for the same signal. In theory, this factor is even higher if the refractive index of the second particle is larger. This principle is used in praxis by secondary labels, i.e. by attaching particles to the target molecule in order to make them visible. :cite:`goh_quantitative_2003`

The quantity :math:`\left[ {n_p^2 - n_c^2}/{n_p^2 + 2n_c^2} \right]^2` from Eq. :eq:`intensity_dipole` is weakly dependent on the wavelength for proteins (dielectric material). However, this dependency becomes more relevant when the secondary label is a plasmonically active metal such as gold. This is investigated in the :ref:`particle`.

» `top`_


Implementation
--------------

Configuration
^^^^^^^^^^^^^

.. automodule:: configuration




dipoleField.py
^^^^^^^^^^^^^^

.. automodule:: dipoleField

.. autofunction:: dipoleField.calcField
.. autofunction:: dipoleField.partialProjector
.. autofunction:: dipolefield.potentialsUpper
.. autofunction:: dipoleField.potentialsLower


» `top`_

.. _top: #