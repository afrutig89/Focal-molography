.. _performance:

Performance
======================


Background scattering module
-----------------------------

All optical systems have noise which influences the sensitivity and hence the detection limit. In this section it is discussed which noise sources are present in a diffractometric biosensor and how they can be reduced or optimized. Knowing the total noise present in a system, the limit of detection (LOD) can be inferred by a defined signal-to-noise ratio (SNR) (2:1 reported in :cite:`nolte_interferometry_2012`). Furthermore, the bias for the spatial lock-in amplifier can be optimized.


Instrumental background and noise
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The main contributions to instrumental noise originate from the laser and the detector. In addition, vibrations and electronic components would fall in this category, but they are not considered here since their quantification is difficult and usually negligibly small or avoidable. 

Two imperfections of the laser are considered in the following: laser fluctuations and the photon noise.

Laser fluctuations or intensity noise is the temporally varying output power. These fluctuations have usually both slow and fast frequency components, therefore an accurate time-limited measurement is difficult. :cite:`sennaroglu_solid-state_2006` Intensity noise can also be influenced by fluctuations in temperature and humidity as well as vibrations. For biosensors that consist of a waveguide, the variations in the coupling efficiency fall in this type of noise.

The imperfection of light monochromaticity is referred to as photon noise and can be quantified by the coherence length. :cite:`sennaroglu_solid-state_2006` The coherence length is defined as the range of wavelengths contained in the light beam or expressed with the frequency range :math:`\Delta \nu`: :cite:`brooker_modern_2003`

.. math::
	l_c \approx \frac{c}{\Delta \nu}
	:label: coherenceLength


Illumination of a diffraction grating results in chromatic aberration, i.e. in a deflection of the focal point. :cite:`hecht_optics_2002`

Detector-generated noise sources define the baseline intensity of the background. :cite:`woolley_theoretical_2015` Among other noise sources (flicker noise, generation-recombination noise, temperature fluctuations), two main noise contributions are statistically independent, thus define the fundamental limit: the detector shot noise and the Johnson-Nyquist noise. :cite:`trager_springer_2012` Whereas detector shot noise again refers to the discrete nature of light that follows Poisson statistics, Johnson-Nyquist noise corresponds to the electronic noise generated by thermal motion of charge carriers.

Träger :cite:`trager_springer_2012` defines the noise current quadratic mean :math:`I_n^2` for a photodetector with no intrinsic amplification and excess noise as

.. math::
	I_n^2 = 2q \left(I_p+I_D\right) \Delta f + \frac{4kT\Delta f}{R}
	:label: noise_current


The first term of Eq. :eq:`noise_current` corresponds to the shot noise, whereas :math:`I_p` and :math:`I_D` are the signal current and the dark current, respectively. :math:`q` is the elementary charge and :math:`\Delta f` the bandwidth of the detector. The second term is the Johnson-Nyquist noise current with the Boltzmann constant :math:`k` and the temperature :math:`T` and the load resistance :math:`R` (for an equivalent circuit of the photodetector).

Surface roughness
^^^^^^^^^^^^^^^^^

The following equations all need to be adapted for a power per unit length.

Especially for biosensors composed of a waveguide, the scattering losses contributes significantly to the background. These losses are caused by imperfections within the volume of the waveguide or due to the surface roughness. Former can usually be neglected (cite ... Robert G. Hunsperger). The scattering loss can be described by an attenuation factor :math:`\alpha_s`. The intensity or the power within the x-axis can then be written as

.. math::
	P(x) = P_0 e^{-\alpha_s x}
	:label: power_xdependency

According to Tien [...], :math:`\alpha_s` can be expressed as

.. math::
	\alpha_s = A^2 \left(\frac{1}{2} \frac{\cos^3 \theta_m}{\sin \theta_m} \right) \left(\frac{1}{d+(1/\gamma)+(1/\delta)} \right)
	:label: scattering_attenuation

with

.. math::
	\theta_m = \frac{\pi}{2} - \arccos\left(\frac{N}{n_f}\right)
	:math: scattering_angle

.. math::
	A = \frac{4\pi}{\lambda_2} (\sigma_{12}^2 + \sigma_{23}^2)^{(1/2)}

:math:`\sigma_{12}` and :math:`\sigma_{23}` are the variances of surface roughness.

To calculate the background intensity at the focal plane we define the power as a function of the axes :math:`x` and :math:`y`:

.. math::
	P(x,y) = P_0 e^{-\alpha (x-x_0) - \frac{2y^2}{w^2}}
	:label: power_waveguide


The :math:`x`-dependence comes from the waveguide (scattering) attenuation and the second part of the exponent is coming from a Gaussian distribution in :math:`y`-direction. We assume that the beam waist :math:`w` is not dependent on the position :math:`x` and the loss is considered to be only in :math:`x`-direction. In order to get the power that is scattered, Eq. :eq:`#power_waveguide` is derivated with respect to :math:`x`. For power within an infinitesimal area :math:`dA = dxdy` we have

.. math::
	dP(x,y) = \frac{P_0}{\sqrt{\pi} w} \alpha e^{-\alpha x-\frac{2y^2}{w^2}} dx dy
	:label: power_loss


The power is devided by :math:`\sqrt{\pi} w` in order to normalize in :math:`y`-direction (area of a Gaussian function). The instensity :math:`I_f` is calculated by dividing the power over the solid angle :math:`d\Omega = 4\pi r^2`. In cylindrical coordinates, :math:`I_f` is

.. math::
	I_f = \frac{\alpha P_0}{\sqrt{\pi} w} \int\limits_{0}^{2\pi} \int\limits_{0}^{D/2} \frac{e^{-\alpha (\rho \cos \phi - x_0)-\frac{2\rho^2\sin^2\phi}{w^2}}}{4\pi (f^2+\rho^2)} \rho d\rho d\phi
	:label: wg_intensity

The radial distance :math:`\rho` is integrated over the numerical aperture of the microscope objective lens. Ideally, only the scattered intensity of the mologram is collected by the lens, therefore :math:`D` corresponds to the diameter of the mologram.

|
.. _backgroundPower:

.. figure:: figures/background_power.png
	:width: 100 %

	Power within the waveguide, assuming 2 mW input and 30% incoupling efficiency at :math:`x_0 = -4 \mathrm{mm}`. The power (a) decays with a propagation loss of 0.8 db/cm. (b) shows the power that is lost (Eq. :eq:`power_loss`) and (c) the contribution on the intensity at the focal point, thus the power loss divided by the solid angle :math:`4\pi r^2`. The background corresponds to the sum of the intensities. Since the microscope does only collect a part of the background, the intensity given in (c) is integrated over the numerical aperture. Ideally, this is the same as the mologram size (Eq. :eq:`wg_intensity`)

|


To define the signal-to-background ratio, it is convenient to express the waveguide background with respect to the incident intensity :math:`I_i = \frac{P_0}{\pi w^2}`. Hence, the SBR for :math:`N` particles is


.. math::
	\frac{I_\text{sca}}{I_\text{f}} = \frac{N^2 \frac{36 n_c^4}{\lambda^4 f^2} \left[ \frac{n_p^2 - n_c^2}{n_p^2 + 2n_c^2} \right]^2 V^2}{\alpha \sqrt{\pi} w \int\limits_{0}^{2\pi} \int\limits_{0}^{D/2} \frac{e^{-\alpha \rho \cos \phi-\frac{2\rho^2\sin^2\phi}{w^2}}}{4\pi (f^2+\rho^2)} \rho d\rho d\phi} =: SBR
	:label: SBR


If only the physical system was observed, the background would have to be summed up over the whole waveguide, i.e. from minus infinity to infinity. However, because the intensity is indirectly proportional to the square of the distance, the resulting background would eventually saturate (:num:`Fig. #backgrounddependency` a). Similarly, the higher the propagation loss is (:num:`Fig. #backgrounddependency` b), the less power is left at the closest point to the focal point, i.e. at :math:`x=0`.


|

.. _backgroundDependency:

.. figure:: figures/background_2.png
	:width: 100 %

	Background dependency on propagation loss (a) and integration boundaries (b). The power is :math:`P=0.3 \cdot 2 \mathrm{mW}` and the coupling grating is at :math:`x_0 = -4 \mathrm{mm}`.

|

Non-specific binding
^^^^^^^^^^^^^^^^^^^^

The scattered field of the randomly placed particles sums up incoherently since there is the same amount of constructive and destructive interference. Incoherent summation implies that the intensity scales linearly with the non-specifically bound scatterers :math:`N_\text{nsb}` on the waveguide:

.. math::
	{I_{\mathrm{nsb}}} = {N_{\mathrm{nsb}}}\frac{{9{\pi ^2}n_c^4}}{{\lambda^4{f^2}}}{\left[ {\frac{{n_p^2 - n_c^2}}{{n_p^2 + 2n_c^2}}} \right]^2}V^2
	:label: intensity_mean


with the volume :math:`V` and refractive index :math:`n_p` of the particles, the refractive index of the surrounding medium :math:`n_c` and the focal distance :math:`f`.

Eq. :eq:`intensity_mean` represents the mean intensity on the measured screen due to incoherent scattering. The standard deviation :math:`\sigma` of the intensity takes into account the variations. In the case of incoherent particles, where the scattered intensity at a focal plane is proportion to a zeroth Bessel function (see Eq. :eq:`wg_background`), the standard deviation is equal to the mean (Eq. :eq:`intensity_mean`). In other words, the exponential distribution within the first zero results in the same standard deviation as the mean value.

In spectroscopy, the limit of detection (LOD) is commonly calculated as the mean :math:`\mu` plus three times the standard deviation :math:`\sigma`. :cite:`thomsen_limits_2003` If only non-specific binding contributes to the background, the LOD is thus defined by:

.. math::
	\mathrm{LOD_{nsb}} = 4 I_{\mathrm{nsb}}\label{eq:LOD_nsb}
	:label: LOD_nsb


Note that if the surface roughness of the waveguide was known, the same analysis could be applied. This would imply that a SBR of 4 would be sufficient for a detectable signal, assuming that it is the main background source.

Eq. :eq:`LOD_nsb` can be used for particles that are uniformly distributed over the mologram. However, there are variations due to fluctuations in a dilute solution. This statistical difference in nonspecifically bound particles between ridges and grooves interferes either constructively or destructively in the focal spot and therefore belongs to the coherent noise sources. This phenomenon is called molecular shot noise and is discussed in the following section.

Molecular shot noise
^^^^^^^^^^^^^^^^^^^^

Hungerford et al. :cite:`hungerford_statistical_1986` showed that the analysis of dilute solutions is ultimately limited by statistical sampling errors. This so called molecular shot noise :cite:`chen_single-molecule_1996` follows a Poisson distribution and can thus not be averaged over time. The standard deviation of a Poisson distribution increases with the square root of the amount of scatterer :math:`N`: :cite:`hungerford_statistical_1986`

.. math::
	\sigma_{sn} = \sqrt{N}
	:label: shot_noise


Since diffractometric biosensors scale quadratically with the number of particles, the molecular shot noise is only relevant for low amounts of particles. Due to this limitation it is theoretically not possible to quantify fewer than 131 molecules within a certainty of 90\% (limit of quantification (LOQ)). :cite:`woolley_theoretical_2015`

Molecular shot noise becomes more relevant in the case of the spatial lock-in amplifier, since all the fluctuations in the solution will be enhanced by the applied bias. In that case both the molecular shot noise of the target and the background particles represent limiting factors.

This kind of noise is inherent and cannot be avoided for dilute solutions. Nonetheless, it ultimately limits to an extremely low amount of particles, such that usually other noise sources define the limit for zero bias. Therefore, it is important to reduce the total noise as much as possible in order to achieve fundamental physical limits.


Speckles
^^^^^^^^
See :cite:`rabal_dynamic_2008`

Which parameters influence the speckle pattern?

Amount/size of speckles defined by
	Wavelength
	Focal distance

Height of the speckles defined by
	Amount (which is essentially the mean)
	
Correlation



Reduction of noise
^^^^^^^^^^^^^^^^^^

Apart from instrumental shot noise, all uncorrelated noise contributions (e.g. vibrations, noise from electronic components) can be averaged temporally and spatially (i.e. reference measurements by multiple detectors). :cite:`piliarik_surface_2009` However, correlated noise such as the intensity fluctuations cannot be averaged spatially. Hence, the standard deviation of the sensor output :math:`\sigma_{so}` after averaging can only be reduced to :cite:`piliarik_surface_2009`

.. math::
	\sigma_{so} = \frac{\sigma_I}{\sqrt{N}} r_A
	:label: instrumental_shot_noise


with :math:`\sigma_I` being the standard deviation of the measured light intensity, :math:`N` the product of the number of intensities and the number of detectors used for averaging and :math:`r_A` a measure of signal correlation. This implies that for a high :math:`N`, i.e. for a long time averaging and measurement on multiple detectors, the output noise can essentially be eliminated. However, this elimination is achieved at the cost of losing temporal resolution.

Another way to consider the intensity noise would be the normalization with respect to the input, such that all the fluctuations are canceled. However, this would imply that the incident beam has to be separated by a beam splitter and measured by an additional detector. Consequently, the chain of uncertainties increases and thus the noise (impurities beam splitter, shot noise). Furthermore, in the case of a waveguide, the incoupling variations cannot be measured by such a setup.

The surface roughness can be optimized during the manufacturing process, but an atomically smooth surface is not achievable with physical vapor deposition methods. Consequently, this contribution cannot be removed. However, since this noise source is dependent on the sensor size and its numerical aperture rather than on the amount of particles, its influence can be minimized by the spatial lock-in amplifier as described in section \ref{sec:bias}: a bias can be introduced such that the diffraction signal at the focal screen due to the bias is already at a level which is much higher than the intensity due to the surface roughness.

The influence of non-specifically bound molecules is minimized correspondingly. In fact, all the incoherent noise sources lose their impact on the signal for a bias. In contrast, coherent noise (laser fluctuations, molecular shot noise) sources do not. Hence, the optimal number of particles derived in Sec. \ref{sec:bias} will decrease due to the presence of coherent noise sources.


» `top`_


Implementation
--------------

.. automodule:: waveguidebackground.background
	:members:

» `top`_

.. _top: #