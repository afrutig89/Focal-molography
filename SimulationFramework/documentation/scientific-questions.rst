.. _scientificQuestions:

Focal Molography
================

Waveguide
---------
- reverse symmetry waveguide

Particle
--------
- Shape, size
- + vesticles / magnetic particle

Mologram
--------
- focal point
- diameter (numerical aperture)
- shape

Reference mologram
------------------
- where
- how


NALIMA
======
- amount of particles

Speckle Analysis
----------------
- What happens when changing the distance?
- Difference between smooth distribution and little amount of particles (Fig. 1.8)?
- How does the intensity distribution look like outside mologram?
- Speckle dimensions in x, y, and z
- number of speckles (no variations)
- Influence of the particle size on the speckles


Bias
====
- Guideline how to design (think of different structures)
- Background analysis: what limits our system?
    - Experiment background: substract previous image