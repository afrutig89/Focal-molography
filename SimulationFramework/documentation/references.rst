.. _waveguide:

References
==========


Diffraction
-----------
.. bibliography:: literature/diffraction.bib

Dipole calculations
-------------------
.. bibliography:: literature/dipole_calculations.bib

Introduction
------------
.. bibliography:: literature/introduction.bib

Noise analysis
--------------
.. bibliography:: literature/noise.bib

Optical properties
------------------
.. bibliography:: literature/optical_properties.bib

Optics
------
.. bibliography:: literature/optics.bib

Parameters
----------
.. bibliography:: literature/parameters.bib

Particles
---------
.. bibliography:: literature/particles.bib

Performance
-----------
.. bibliography:: literature/performance.bib


Phase Mask
----------
.. bibliography:: literature/phase_mask.bib


Protein adlayer calculations
-------------------------
.. bibliography:: literature/protein_adlayer.bib


Scattering
----------
.. bibliography:: literature/scattering.bib

Waveguide
---------
.. bibliography:: literature/waveguide.bib
