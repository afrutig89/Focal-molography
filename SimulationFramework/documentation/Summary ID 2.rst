Summary simulation 2 on 2017-02-24 at 16:29:02
==============================================

Waveguide settings
------------------

:propagationLoss: 9.21034037198

:n_s: 1.52128836444

:wavelength: 6.328e-07

:n_f: 2.11801706167

:film: Tantalum Pentoxide

:n_c: 1.33186931363

:substrate: Glass

:inputPower: 0.002

:cover: Water

:thickness: 1.45e-07

:polarization: TE

:mode: 0

:couplingEfficiency: 0.3

