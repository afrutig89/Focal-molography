.. _waveguide:

Dielectric slab waveguide
=========================

A dielectric waveguide confines and guides electromagnetic waves due to continuous total internal reflection at the optical interfaces. It therefore allows darkfield illumination of a sample placed on this waveguide. In other words, the only light that is seen from outside is the scattered field of molecules in close proximity to the waveguide or imperfections of the waveguide surface itself. The quality of this darkfield illumination is the reason why waveguide based biosensors are so popular. :cite:`kozma_integrated_2014`

Theory
------

Waveguide modes
^^^^^^^^^^^^^^^

The theory of waveguides requires the solution of Maxwell equations for this specific configuration. In essence, an asymmetric dielectric waveguide consists of a film with high refractive index :math:`n_f` sandwiched between two media with lower refractive index (cover (:math:`n_c`) and substrate (:math:`n_s`)), such that light is reflected at the boundary when it is coupled within a certain angle. Depending on the thickness of the waveguide :math:`d_f` and the wave angular frequency :math:`\omega`, specific guided modes can propagate. In this work, the only modes of interest are fundamental :math:`\mathrm{TE}` and :math:`\mathrm{TM}` modes (:num:`Fig. #waveguidemodes`).

|
.. _waveguideModes:

.. figure:: figures/waveguide.png
	:width: 100 %

	Fundamental modes in a waveguide of thickness :math:`d` for TE polarization (a) and TM polarization (b). For the TE mode, the electric field is continuous at the boundaries between film (:math:`n_f`) and cover (:math:`n_c`) and between film and substrate (:math:`n_s`), respectively. Only the y-component of the field is nonzero for a TE mode. The TM mode is discontinuous at the boundaries. Only the z-component is depicted since the field in x-direction is imaginary.

|

TE guide mode
^^^^^^^^^^^^^

The electric field for a TE guided mode is calculated as follows :cite:`marcuse_chapter_1991`:

.. math::
	{E_y} = \left\{ {\begin{array}{*{20}{c}}
	{A{e^{ - \delta z}}}&{{\rm{for}}}&{z \ge 0}\\
	{A\cos \kappa z + B\sin \kappa z}&{{\rm{for}}}&{0 \ge z \ge  - d}\\
	{\left( {A\cos \kappa d - B\sin \kappa d} \right){e^{\gamma (z + d)}}}&{{\rm{for}}}&{z \le  - d}
	\end{array}} \right.
	:label: E_y

with 

.. math::
	A = {\left[ {\frac{{4{\kappa ^2}\omega {\mu _0}P}}{{\left| \beta  \right|\left( {{\kappa ^2} + {\delta ^2}} \right)\left( {d + \frac{1}{\gamma } + \frac{1}{\delta }} \right)}}} \right]^{1/2}}
	:label: waveguide_A

.. math::
	B=-A\frac{{\delta}}{\kappa}
	:label: waveguide_B

with input power :math:`P` in W per m and :math:`\beta` (propagation constant), :math:`\kappa` (propagation decay), :math:`\gamma` (transverse decay), and :math:`\delta` (evanescent wave decay) which are defined as follows

.. math::
	\beta  = Nk
	:label: waveguide_beta

.. math::
	\kappa  = k\sqrt {n_f^2 - {N^2}}
	:label: waveguide_kappa

.. math::
	\gamma  = k\sqrt {{N^2} - n_s^2}
	:label: waveguide_gamma

.. math::
	\delta  = k\sqrt {{N^2} - n_c^2} 
	:label: waveguide_delta


The parameters :math:`n_{f}`, :math:`n_{s}`, and :math:`n_{c}` correspond to the refractive indices of the waveguide for the film, the substrate and the cover. :math:`N` is the effective refractive index which can be found by the evaluation of the eigenvalue equation: :cite:`kogelnik_theory_1975`

.. math::
	\tan\kappa d=\kappa\left(\frac{{\gamma+\delta}}{\kappa^{2}-\gamma\delta}\right)
	:label: waveguide_tankappa

TM guided mode
^^^^^^^^^^^^^^

Since for a guided TE mode only the transverse electric field is nonzero, it is continuous at the boundary (see :num:`Fig. #waveguidemodes`\a). In contrast, the transverse magnetic field for the TM mode is nonzero and therefore the longitudinal components of the electric field are discontinuous at the interface (:num:`Fig. #waveguidemodes`\b). Note that :math:`E_x` is not illustrated since it is purely imaginary.

For the TM mode the electric fields in :math:`x` and :math:`z` direction are as follows:

.. math::
	{E_x} = \frac{\mathrm{i}}{{\omega {\varepsilon _0}}}\frac{\beta }{{\left| \beta  \right|}}\left\{ {\begin{array}{*{20}{c}}
	{\frac{\delta }{{n_c^2}}C{e^{ - \delta z}}}&{{\rm{for}}}&{z \ge 0}\\
	{\frac{\kappa }{{n_f^2}}\left( {C\cos \kappa z + D\sin \kappa z} \right)}&{{\rm{for}}}&{0 \ge z \ge  - d}\\
	{ - \frac{\gamma }{{n_s^2}}\left( {C\cos \kappa d - D\sin \kappa d} \right){e^{\gamma (z + d)}}}&{{\rm{for}}}&{z \le  - d}
	\end{array}} \right.
	:label: TM_Ex

.. math::
	{E_z} = \frac{\beta }{{\omega {\varepsilon _0}}}\frac{\beta }{{\left| \beta  \right|}}\left\{ {\begin{array}{*{20}{c}}
	{\frac{1}{{n_c^2}}C{e^{ - \delta z}}}&{{\rm{for}}}&{z \ge 0}\\
	{\frac{1}{{n_f^2}}\left( {C\cos \kappa z + D\sin \kappa z} \right)}&{{\rm{for}}}&{0 \ge z \ge  - d}\\
	{\frac{1}{{n_s^2}}\left( {C\cos \kappa d - D\sin \kappa d} \right){e^{\gamma (z + d)}}}&{{\rm{for}}}&{z \le  - d}
	\end{array}} \right.
	:label: TM_Ez

with the constants

.. math::
	C = \sqrt {\frac{{4\omega {\varepsilon _0}P}}{{\left| \beta  \right|}}\frac{{n_f^2n_c^4{\kappa ^2}}}{{\left( {n_c^4{\kappa ^2} + n_f^4{\delta ^2}} \right)}}{{\left[ {d + \frac{{n_f^2n_s^2}}{\gamma }\frac{{{\kappa ^2} + {\gamma ^2}}}{{n_s^4{\kappa ^2} + n_f^4{\gamma ^2}}} + \frac{{n_f^2n_c^2}}{\delta }\frac{{{\kappa ^2} + {\delta ^2}}}{{n_c^4{\kappa ^2} + n_f^4{\delta ^2}}}} \right]}^{ - 1}}}
	:label: waveguide_C

.. math::
	D =  - C \frac{n_f^2}{n_c^2} \frac{\delta}{\kappa}
	:label: waveguide_D


With the optical waveguide, the second key component for a sensitive diffractometric biosensor has been introduced. The following section shows, that a further improvement in the performance can be achieved by a molographic bias.

» `top`_



Sensitivity of grating couplers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Differential sensitivity

.. math::
	\frac{\partial N}{\partial n_c} = \frac{n_c}{N} \frac{n_f^2-N^2}{n_f^2-n_c^2} \frac{\Delta z_{f,c}}{d_{eff}}\left[2 \left(\frac{N}{n_c}\right)^2-1\right]^\rho

:math:`\rho = 0` for TE modes and :math:`\rho = 1` for TM modes.

Effective waveguide thickness

.. math::

	d_{\mathrm{eff}} = \Delta z_s + \Delta z_c + d_f\\
	\Delta z_s = \frac{\lambda}{2\pi} \left(N^2 - n_s^2\right)^{-1/2}\\
	\Delta z_c = \frac{\lambda}{2\pi} \left(N^2 - n_c^2\right)^{-1/2}

With waveguide thickness :math:`d_{\mathrm{eff}}` and penetration depths :math:`\Delta z` in the substrate and the cover, respectively.

:reference: Eq. 4.14 Sensitivity of grating couplers as integrated-optical chemical sensors (K. Tiefenthaler and W. Lukosz)

Effective refractive index
^^^^^^^^^^^^^^^^^^^^^^^^^^

Eigenvalue equation solved for a three-layer waveguide (term adlayer in reference omitted)

.. math::

	0 = & \frac{2\pi d_f}{\lambda} \sqrt{n_f^2-N^2} \\
		& - \arctan \left[\left(\frac{n_f}{n_c}\right)^{2\rho} \sqrt{\frac{N^2-n_c^2}{n_f^2-N^2}}\right] \\
		& - \arctan \left[\left(\frac{n_f}{n_s}\right)^{2\rho} \sqrt{\frac{N^2-n_s^2}{n_f^2-N^2}}\right] \\

:reference: Optical grating coupler biosensors, Janos Voeroes, Biomaterials 2002.


Cutoff thickness
^^^^^^^^^^^^^^^^



.. math::

	d_c \kappa = \left( \arctan \left[\sqrt{\frac{n_s^2-n_c^2}{n_f^2-n_s^2}}\right] + \nu \pi \right)

:reference: eq. 1.3-42 Marcuse


Implementation
--------------

Waveguide class
^^^^^^^^^^^^^^^^

.. automodule:: waveguide.waveguide
	:members:

Coupling Grating class
^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: waveguide.GratingCoupler
	:members:


» `top`_

.. _top: #