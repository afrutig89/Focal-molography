Todo
====


Background widget
-----------------

- Non-specific binding
- fluctuations, photon noise, molecular shot noise


Waveguide widget
----------------

- Documentation on coupling
- Polarization, thickness -> modes -> restrict thickness

- Background
    - Waveguide background
        - Fourier calculation --> speckles (?)

- imaginary values for materials (not only particle)


Mologram widget
---------------
- scattering efficiency
- Theory diffraction limited spot (Airy disk radius)
- Theory numerical aperture


Plots
^^^^^
- 3D plot electric field / power on waveguide
- 2D electric fields (x, y, z)
- Background intensity
- radiation patterns


Load widget
-----------
- multiple parameters for y-axis


Sweep widget
------------
- Pareto (several parameters)
- video (right now the number of particles sweep from 1 to specified value)


Layout
------

- Save image
    - image information (optimally such that it can be loaded in the moloreader as well)
- Load image
    - Experiment from moloreader should be possible as well
- Export summary works?
- rst-file changeable
- Combine with scripts of moloreader
    - See cameraFeed -> nice features
- Signal intensity / SNR
- Materials
- Mask tab
- Implement Load Experiment function
- Scrollbar if screen too small to display
- Mologram figure (text-annotations bad if diameter > 400um)
- Settings tab / widget
	- Save settings appearance
	- Shortcuts
	- Video settings
	- Figure settings