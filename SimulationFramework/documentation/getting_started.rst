Getting started
===========================================================

Process flow:

The file mainGUI.py is the main executable. Upon execution it loads the default values from the database/default.db file that can be created with the file:  *defaultDataset.py* The mainGUI.py then creates an instance of MainWidget in the file *Main_widget.py* which sets up all the widgets. Furthermore, a instance of the *worker* class is created that can execute simulations and calls the routine *main_widget.updateResults* if new results are created. This function then upates the GUI and saves the results to the database. 

General Settings
--------------------------



Description of the most important files

Variables and Datamanagement
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	* variables.py

	The variables file stores all the Parameters in a dictionary called *logParameters*. 

	Parameters() this is the function that keeps track of the parameters.

* How can a new parameter be stored in the database?

All parameters that will be stored in the database are in the dictionary *calParameters*. If a new parameter is to be stored this needs to be added into this dictionary. 


simulation_dict() is created in the file variables.py and manages all the?




The simulation results are saved in a database file called *simulations.db*.

 what is in there?




Starting a Simulation
--------------------------

By pressing the Run button in the runMenu (mainGUI.py) a worker object is created that gathers the parameters from the UI by the function getParameters in the Main_wiget.py and then launches the actual simulation routine *run_simulation* in the file simulations.py.

	* simulations.py
		
	

Implementation
--------------
