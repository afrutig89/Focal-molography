.. _introduction:

Introduction
============
Biomedical interactions play a key role in clinical medicine as its characterization allows to diagnose specific disease biomarkers in an early stage and to monitor possible treatments. Optical biosensors are commonly used for such purposes since they can achieve high sensitivity. :cite:`nolte_interferometry_2012` Most optical biosensors detect the presence of molecules by means of their different refractive index compared to the aqueous environment. Hence, accumulating surface-bound molecules result in a change of reflected or diffracted light.

Optical biosensors can be split into two groups: refractometric and diffractometric biosensors. Refractometric optical biosensors use refraction, i.e. the change in the direction of propagation of light due to interaction with matter. Diffractometric biosensors are based on diffraction which describes the wave phenomena that occur due to an obstacle.

While refractometric biosensors such as SPR (Surface Plasmon Resonance) are well-established and exhibit high sensitivities (refractive index changes < 10\ :sup:`-5` :cite:`helmerhorst_real-time_2012`), they suffer inherent drawbacks. Because all refractive index changes contribute to the signal, they are susceptible to non-specific binding (NSB) as well as temperature fluctuations and medium composition. :cite:`fattinger_focal_2014`

In diffraction-based biosensors, molecular interactions are visualized by observing interference pattern of the scattered light at biomolecules. :cite:`goh_diffraction-based_2002` Such an interference pattern is only visible if the biomolecules are immobilized or captured in a certain arrangement. Hence, diffractometric biosensors implement self-referencing within the sensing structure itself with a minimal separation of sample and reference in the order of the wavelength of visible light (few hundred nm).

Thus, the advantage of diffractometric biosensors, in contrast to refractometric optical biosensor, is that the signal is almost not influenced by nonspecific binding (NSB). However, this advantage over refractometric biosensors could not compensate for the low sensitivity they exhibited in the past years. :cite:`goh_quantitative_2003` However, Recently, Fattinger :cite:`fattinger_focal_2014` introduced a novel diffractometric technology - focal molography - which predicts comparable sensitivity to SPR and provides real-time measurements label-free in complex samples.

.. _focalMolography:

Focal Molography
----------------

Basic principle
^^^^^^^^^^^^^^^
Focal molography is a novel type of diffractometric biosensor presented by Fattinger in 2014. :cite:`fattinger_focal_2014` One key component he used to increase the diffractive signal is its maximum localization in space. This can be achieved by positioning the target molecules in a coherent pattern, such that they scatter the light into a focal spot. Due to such a pattern the intensity at the focal spot scales quadratically to the absorbed molecules, whereas the background intensity remains constant.

The other of his key ingredients is the use of an optical waveguide to illuminate the molecules in a darkfield configuration. This allows darkfield illumination of the molecules. Furthermore, it only excites molecules located in the proximity of the surface due to the exponentially decaying evanescent field. :cite:`mukundan_waveguide-based_2009` 


Schematic representation
^^^^^^^^^^^^^^^^^^^^^^^^

A functional schematic is shown in :num:`Fig. #figfocalmolography`\a. In principle, the setup serves two functions. First, it allows to couple a TE polarized He-Ne laser beam (632.8 nm wavelength) via a grating coupler (coupling angle -10.6°, period 318 nm, thickness 500 μm) into a thin-film optical waveguide (Ta\ :sub:`2`\ O\ :sub:`5`, 145 nm) on a D261 glass substrate (Schott). Molecules located on the waveguide are illuminated by the evanescent field of the fundamental guided mode (penetration depth 82 nm) (:num:`Fig. #figfocalmolography`\b). 9 The mologram consists of alternating ridges, where SAv is bound to immobilized biotin, and grooves, which are backfilled with an inert PEG molecule. This mologram is denoted as [NH-biotin/SAv¦NH-PEG12] and was fabricated as reported in :cite:`gatterdam_focal_2017` (:num:`Fig. #figfocalmolography`\c). When impinging on the mologram, a portion of the light is coupled out into two converging beams that form two diffraction-limited foci above and below the waveguide. The diffracted light of the lower beam is collected by a 20 X microscopy objective and visualized by a CMOS detector. The mologram composed of protein molecules (SAv) consists of hyperbolic lines as depicted in :num:`Fig. #figfocalmolography`\d. The central area is masked in order to avoid second-order Bragg reflections.

|
.. _figFocalMolography:

.. figure:: figures/focal_molography.png
	:width: 100 %

	Schematic representation of focal molography. (a) Light of a He-Ne laser is coupled to the TE mode of the waveguide via a grating coupler. The light propagates along the waveguide and is scattered at the molecules close to the surface. The molecules of interest Streptavidin (SAv) were arranged such that their scattered field interferes constructively at a defined focal point. In the schematic ten such molograms are arranged in a row. The signal of the mologram was collected by a 20x microscopy objective and captured by a CMOS-sensor. (b) The magnified waveguide with the field profile of the TE mode shown in blue (c) Active regions with biotin (ridges) capture the protein of interest and form a coherent assembly while inert regions (grooves) do not recognize the protein. (d) The mologram consists of hyperbolic lines (only each 50th line is depicted). 

|

» `top`_

.. _simulationTool:

Simulation tool
---------------

The simulation tool is composed of a main file and subfolders for the different components of focal molography. The subfolder are listed below:

* analysis
	* AiryDisk.py
		Evaluates the diffraction limited spot for different input parameters (see database/Airy\_??.db) in order to find out what influences the spot based on simulations. Only a few simulations were done with an insufficient high resolution.
	* analytical.py
		Calculation of different parameters by using the analytical formulae. These parameters are then used for the **Noise Analysis** presented in the master thesis of Silvio Bischof. By values in the dictionary *plots* can be set *True* in order to show a specific plot. This module supports a basic understanding of different noise sources and the concept of the **patial lock-in amplifier**.
	* focalMolography.py
		This file evaluates the equations presented in :cite:`fattinger_focal_2014`
	* parameterAnalysis.py
		This script plots the different parameters relevant for focal molography or diffractometric biosensors in general. Supports a fundamental understanding of the physics behind.
	* scattering.py
		A script programmed in order to compare Mie and Rayleigh scattering.
* auxiliaries
	* concentrationConverter.py
		Can be used to convert number of scatterers to surface mass modulation [pg/mm2], mass [fg] or concentration [mol]. Especially useful for figures if different labels for the axis are of interest.
	* constants.py
		A set of constants used for this simulation tool.
	* fields.py
		Geometrical description of the fields. I.e. calculation of the field distribution on a plane with an arbitrary incident angle and the corresponding phase calculation. 
	* functions.py
		Miscelannaeous functions used for the simulations. These are the definition of the screen, the numerical calculation of the Airy disk diameter and the calculation of an equivalent amount of scatterers with different refractive index and size.
	* LorentzDrude.py
		Calculation of the dielectric function of different materials according to the Lorentz-Drude model.
	* writeLogFile.py
		This module provides IO functions in order to store and load data.
* database
	.db files are stored in this folder.
* dipole
	* calculationMethods.py
		Based on the operating system the process can be split into a for loop (windows) or mapped onto different cores (unix).
	* dipole.py
		This module provides the basic characteristics of the dipole such as dipole polarizability, the static dipole moment, the dipole radiation as well as the scattering, absorption and extinction cross sections.
	* dipoleFields.py
		In this module, the far field of dipoles placed in proximity to an interface is calculated.
	* plots.py
		Contains different functions for plotting the field distribution of a dipole.
	* radiationPatterns.py
		Calculates the power distributed into different parts of the configuration (upper half space, waveguide, lower half space with allowed and forbidden zones (evanescent waves)).

		.. todo::

			This module is not yet correct and needs to be revised.

* interfaces
	* configuration.py
		With this module, a configuration of different media can be defined. It also containes a function to calculate the effective focal point that will be measured in praxis.
	* fresnelFormulae.py
		Calculation of the Fresnel transmission and reflection coefficients for a single interface and a layered medium. Furthermore it provides functions to calculate the Brewster and the critical angle for different media.
	* snellsLaw.py
		Contains functions that returns the angle for refraction and for reflection for a given incident angle and different refractive indices.
* mologram
	* braggCircle.py
		Calculates the radius of the Bragg annulus.
	* generateMask.py
		With this module, certain areas of the mologram can be masked. This mask can be circular, rectangular or annulus.
	* generatePattern.py
		Generation of the mologram in focal molography (incident angle 90 degrees). The particles can be distributed over the ridges or the lines with a certain probability as well as randomly over the whole mologram.
	* mologram.py
		This module contains the class *Mologram*. It contains several functions to find coordinates for specific lines or vice versa.

* performance
	* background.py
	* limitations.py
	* shotNoise.py
	* statisticalLOD.py
* plots
* waveguide
	* waveguide.py

What to start with?
^^^^^^^^^^^^^^^^^^^
This documentation provides most of the theory required to implement the functions and the corresponding references. In a first step, one should be aware of the different parts of focal molography (cp. :num:`Fig. #figfocalmolography`).

A basic understanding of the optics can be obtained by the principles of diffraction theory (see :ref:`diffraction`). It is recommendable to read about the commonly known Fresnel zone plate, since the construction of the mologram is based on the very same theory.

Knowing diffraction theory, one can dig into more specific subjects such as :ref:`Rayleigh` (elastic scattering at small particles). It will be seen that the field distribution of small particles is the same as for a dipole, which was investigated very thoroughly in literature. Larger particles would require a more complicated analysis of the Maxwell equations. Such particles are however not considered within this simulation tool.

The scattered field of a particle is dependent on the excitation field. In the case of focal molography, the molecules or nanoparticles are excited by the light propagating in a waveguide. The calculation of this excitation field is possible, however complex. Section :ref:`waveguide` provides a very short summary of the basic principle of a dielectric slab waveguide, i.e. about the coupled mode theory.

Diffraction, scattering and coupled mode theory supply the basic theory of focal molography. To find the performance of this biosensor, the different sources of noise must be known. This issues are covered in section :ref:`performance`.


» `top`_

.. _top: #