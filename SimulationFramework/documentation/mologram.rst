.. _mologram:

Mologram
========

Theory
------

.. _diffraction:

From diffraction gratings to diffractive lenses
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is well known that diffraction of light by an opaque disk can result in a diffraction-limited spot in the center point. Similarly, a bright spot can be observed for a circular aperture. :cite:`oleg_marchenko_shot_2007` This led to the idea of the Fresnel zone plate :cite:`attwood_soft_1999`: an amplification of light can be obtained if concentric transparent and opaque rings are arranged such that all the diffracted light interferes constructively at a desired focal point. Such an arrangement is nothing but a diffractive lens.

A Fresnel zone plate is illustrated in :num:`Fig. #fresnelzoneplate`. The configuration consists of alternating open and closed zones. When illuminating the lens by a monochromatic plane wave, the diffracted waves exhibit the same phase at the focal point :math:`f`. This principle is illustrated by three propagation paths (green, blue, orange) that intersect at a certain distance. The waves projected onto the z-axis exhibit the same phase at the focal point (illustrated in the box), thus they interfere constructively.

|
.. _FresnelZonePlate:

.. figure:: figures/Fresnel_zone_plate-04.png
	:width: 100 %

	Illustration of a Fresnel zone plate lens with plane wave illumination. The zones are specified such that constructive interference of the diffracted radiation occurs at a desired focal point. Three light rays are illustrated as green, blue and yellow lines. The phases of the waves are equal at the focal point as required for constructive interference. In other words, the pathlength differences :math:`l-l_0` correspond to an integer multiple of the wavelength :math:`\lambda`.

|

An electromagnetic plane electromagnetic wave in space can be expressed as :cite:`brooker_modern_2003`

.. math::
	\vec{E} = \vec{E}_0 e^{\mathrm{i}(\vec k \cdot \vec r - \alpha)}
	:label: planewave

where :math:`\vec{E}_0` is the amplitude and the exponent the phase with :math:`\vec k` being the wave vector, :math:`\vec r` the position vector. :math:`\alpha` is called the phase lead. If the wave travels a certain distance :math:`\Delta l`, the phase is :math:`\alpha = k \Delta l`. The phase has a period of :math:`2\pi`, thus it recurs for :math:`\Delta l=\lambda` since the wave number is defined as :math:`k=\frac{2\pi}{\lambda}`.

To build a grating such that the transmitted light interferes constructively at a desired point, all the different phases must not differ more than :math:`\pi/2`. In other words, the optical pathlengths, i.e. the distance from the source point to the focal point :math:`l=\sqrt{f^2+r^2}`, must fulfill the following inequation:

.. math::
	\frac{{(j - 1)\lambda }}{2} < l - {l_0} < \frac{{j\lambda }}{2}
	:label: inequation

whereas :math:`\lambda` is the wavelength of the incoming wave (:math:`\lambda=2\pi/k`) and :math:`j` either an odd or an even integer. In :num:`Fig. #fresnelzoneplate`, the transparent ranges correspond to the even zones (:math:`j=2,4,6,...`) and the opaque to the odd zones (:math:`j=1,3,5,...`).

The boundaries of the zones can be defined by the lines that fully extinguish each other. That means that the waves originated from an adjacent line are turned upside-down. Hence, the phases of the boundary points are shifted by :math:`\pi` or :math:`\lambda/2`, respectively:

.. math::
	\begin{eqnarray}
		l-l_0 &=& \frac{j\lambda}{2}\\
		{} &=& \sqrt{f^2+r_j^2}-f
	\end{eqnarray}
	:label: Fresnel_zeros

Solving Eq. :eq:`Fresnel_zeros` gives the radii :math:`r_j` for the circles that represent the transition to the :math:`j`\th zone:

.. math::
	r_j = \sqrt{j\lambda \left(f+\frac{j\lambda}{4}\right)}
	:label: rj

Such Fresnel zone plates are used in industry as diffractive image forming lenses for high resolution soft x-ray microscopy. :cite:`attwood_soft_1999` In this work, diffractometric biosensors that consist of diffraction structures built by molecules are investigated. These molecules scatter the light in the same way as expected from the theory above. However, this implies that the opaque zones become only phase objects. As a consequence, the light which propagates through a molecular grating consists of two parts: a transmitted beam :math:`E_{t}` and the scattered field :math:`E_\mathrm{sca}`: :cite:`nolte_interferometry_2012`

.. math::
	E = E_{t} + E_{\mathrm{sca}}
	:label: E_tot

It is intuitive, that a small scattered field :math:`E_{\mathrm{sca}}` can be more readily detected if the background, namely :math:`E_{t}`, is small or even inexistent. In order to reduce :math:`E_{t}`, the incident angle of the laser beam is altered. Apparently, this will break the circular symmetry of the pattern, since the path length difference is extended by a certain distance :math:`\Delta l` before hitting the diffraction grating (:num:`Fig. #fresnelzoneplatetilted`). Without loss of generality, the incident wave is tilted in :math:`x`-direction by an angle :math:`\theta`. Eq. :eq:`Fresnel_zeros` is then adjusted to

.. math::
	x \sin \theta + \sqrt{f^2+x^2+y^2}-f = \frac{j\lambda}{2}
	:label: Fresnel_ellipse_freespace

|
.. _FresnelZonePlateTilted:

.. figure:: figures/Fresnel_zone_plate-05.png
	:width: 100 %

	Fresnel zone plate lens for a tilted plane wave illumination. Due to the tilting, the path lengths differences from :num:`Fig. #fresnelzoneplate` are extended by :math:`\Delta l`. Consequently, the radial symmetry of the perpendicular incident angle breaks and the diffraction pattern yields the form of ellipses. The tilting is shown in x-direction, such that :math:`\Delta l = x \sin\theta`. Other incidence directions would just lead to a rotation of the corresponding pattern.

|

The solution of Eq. :eq:`Fresnel_ellipse_freespace` has an elliptical form with the major axis :math:`x` and the minor axis :math:`y`:

.. math::
	{\left(\frac{{x-{x_{0}}}}{a}\right)^{2}}+{\left(\frac{{y-{y_{0}}}}{b}\right)^{2}}=1
	:label: Fresnel_ellipse


with major and minor radius of the ellipse :math:`a` and :math:`b`:

.. math::
	a = {\frac{1}{{2\cos \theta }}\sqrt {{{\tan }^2}\theta {{\left( {j\lambda  + 2f} \right)}^2} + 4j\lambda f + {j^2}{\lambda ^2}} }
	:label: ellipse_a

.. math::
	b = a \cos\theta
	:label: ellipse_b

and the center :math:`(x_0,y_0)`

.. math::
	{x_{0}}=-\frac{{\sin \theta \left( {j\lambda  + 2f} \right)}}{{2{{\cos }^2}\theta }}
	:label: ellipse_x0

.. math::
	y_0 = 0
	:label: ellipse_y0


In words: all points positioned between two ellipses interfere constructively at a desired focus. Hence, as in the case of the Fresnel lens with normal incidence, the positions in between the ellipses define the transparent and the opaque zones.

In a next step, different media are considered. If light propagates through a transparent medium, the wavelength is reduced by a factor :math:`n`, which represents the refractive index of a material. Having two different media above and below the Fresnel zones with the refractive indices :math:`n_1` and :math:`n_2`, respectively, Eq. :eq:`Fresnel_ellipse` must be modified to

.. math::
	{n_1}x\sin \theta  + {n_2}\left( {\sqrt {{x^2} + {y^2} + {f^2}}  - f} \right) = \frac{{j\lambda}}{2}
	:label: Fresnel_ellispe_interface

Here, :math:`\lambda` is the wavelength in free space.

After a considerable amount of algebraic manipulations, again an ellipse of the form of Eq. :eq:`Fresnel_ellipse` is obtained. The major and minor radius :math:`a` and :math:`b`, as well as the center of the ellipse (:math:`x_0`,:math:`y_0`\) is given by

.. math::
	a=\sqrt{\frac{{4f{\lambda_{0}}j{n_{2}}+\lambda_{0}^{2}{j^{2}}}}{{4n_{2}^{2}-4n_{1}^{2}{{\sin}^{2}}\theta}}+{{\left(\frac{{\sin\theta\left({{\lambda_{0}}j{n_{1}}+2f{n_{1}}{n_{2}}}\right)}}{{2n_{2}^{2}-2n_{1}^{2}{{\sin}^{2}}\theta}}\right)}^{2}}}
	:label: ellipse_a

.. math::
	b=\sqrt{\frac{{f{\lambda_{0}}j}}{{n_{2}}}+\frac{{\lambda_{0}^{2}{j^{2}}}}{{4n_{2}^{2}}}+\frac{{{{\sin}^{2}}\theta{{\left({{\lambda_{0}}j\frac{{n_{1}}}{{n_{2}}}+2f{n_{1}}}\right)}^{2}}}}{{4\left({n_{2}^{2}-n_{1}^{2}{{\sin}^{2}}\theta}\right)}}}
	:label: ellipse_b

.. math::
	{x_{0}}=-\frac{{\sin\theta\left({{\lambda_{0}}j{n_{1}}+2{n_{1}}{n_{2}}f}\right)}}{{2n_{2}^{2}-2n_{1}^{2}{{\sin}^{2}}\theta}}
	:label: ellipse_x0

.. math::
	y_0 = 0
	:label: ellipse_y0

Depending on the refractive indices of the materials, :math:`b` can become imaginary. That implies that the zones are no longer elliptic in shape but hyperbolic. In a special case of 90 degrees angle of incidence, the sine functions become unity and the zeros are defined as solutions of

.. math::
	{\left( {\frac{{x - \frac{{{\lambda _0}j{n_1} + 2f{n_1}{n_2}}}{{2\left( {n_1^2 - n_2^2} \right)}}}}{{\frac{{2f{n_1}{n_2} + {\lambda _0}j{n_2}}}{{2\left( {n_1^2 - n_2^2} \right)}}}}} \right)^2} - {\left( {\frac{y}{{\sqrt {\frac{{4j{n_2}f{\lambda _0} + \lambda _0^2{j^2} + 4{f^2}n_1^2}}{{4\left( {n_1^2 - n_2^2} \right)}}} }}} \right)^2} = 1
	:label: hyperbola

This formula defines the zones that are alternately transparent or opaque for a horizontal illumination. Such a configuration seems not feasible with blocking zones. However, it is by a previously mentioned molecular pattern, such that the light is not blocked but scattered in the same way. Instead of speaking about opaque and transparent zones, the terminology according to :cite:`fattinger_focal_2014` is used: whereas the ridges immobilize molecules, the grooves are empty (In practice the grooves are backfilled with an inert molecule. The biological or chemical part of this project is however not covered within the scope of this thesis. A detailed description of the realization of such patterns will be published in 2017 by the focal molography group at LBB. :cite:`gatterdam_focal_2017`).

It should be noted that Eq. :eq:`hyperbola` is not identical to the formula derived by Fattinger in :cite:`fattinger_focal_2014`:

.. math::
	\pm(x_j-x_0) = \frac{\lambda n_1 (j_0+j) - \sqrt{n_2^2 \left(n_1^2-n_2^2 \right) \left(y_j^2+f^2 \right) + \left(n_2 \lambda \right)^2 \left(j_0+j \right)^2}}{n_1^2-n_2^2}
	:label: mololines

where :math:`j_0` denotes the number of wavelengths in the substrate that add up to the focal length f; thus, :math:`f=j_0 (\lambda/n_2)`. Fattinger's equation describes the lines for the middle of the ridges and the grooves, whereas the equation derived in this manuscript describes the location of the boundaries between ridges and grooves.

The replacement of opaque zones with pure phase objects such as molecules is not intuitive. It will be seen in the next section, that the scattered far-field of molecules indeed consists of plane waves in different directions as in the case of diffraction. Not only do the diffracted and the scattered fields behave similarly, but they actually yield the same solutions, since a collection of particles can also be represented by a medium with a refractive index as a function of particle density. :cite:`cai_introduction_2010`

» `top`_


Implementation
--------------

mologram.py
^^^^^^^^^^^

.. automodule:: mologram

.. autoclass:: mologram.Mologram
	:members:

braggCircle.py
^^^^^^^^^^^^^^

.. automodule:: braggCircle
	:members:



generatePattern.py
^^^^^^^^^^^^^^^^^^

.. automodule:: mologram.generatePattern
	:members:


checkMologram.py
^^^^^^^^^^^^^^^^^

.. automodule:: mologram.checkMologram
	:members:

generateMask.py
^^^^^^^^^^^^^^^

.. automodule:: generateMask

.. autoclass:: generateMask.Mask
	:members:

.. autofunction:: generateMask.diskMask
.. autofunction:: generateMask.annulusMask
.. autofunction:: generateMask.rectangleMask
.. autofunction:: generateMask.mologramMask
.. autofunction:: generateMask.plotMask
.. autofunction:: generateMask.display


» `top`_

.. _top: #