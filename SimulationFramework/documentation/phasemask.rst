.. _phasemask:

Reactive Immersion Lithography and Phase Mask Optimization
===========================================================

Phase Mask General definitions
-------------------------------

Intensity ratio
^^^^^^^^^^^^^^^

.. math::
	{r_{I}=\frac{I_{+}}{I_{-}}}

We refer to intensity ratio :math:`r_{I}` as ratio in light intensity by phase mask illumination between ridges :math:`I_{+}` and grooves :math:`I_{-}`.


Activation ratio
^^^^^^^^^^^^^^^^

.. math::
	{r_{I}=\frac{\phi_{+}}{\phi_{-}}}

We refer to as activation ratio :math:`r_{\phi}` as ratio in photochemical activation :math:`\phi` between ridges and grooves caused by phase mask illumination. The de-protection rate of the photo-cleavable group connects intensity and activation ratio.

Affinity ratio
^^^^^^^^^^^^^^

.. math::
	{r_{R}=\frac{R_{+}}{R_{-}}}

The affinity ratio :math:`r_{R}` is the ratio of binding sites/receptors between ridges and grooves for the target molecule, that is obtained after functionalizing the photoactivated chip. The affinity ratio is lower than the activation ratio, since coupling reagents also bind to non-photoinduced binding sites and the turnover of the chemical reaction is incomplete.

Surface mass density ratio of the target molecule
^^^^^^^^^^^^^^^^^

.. math::
	{r_{\Gamma}^{(T)}=\frac{\Gamma_{+}^{(T)}}{\Gamma_{-}^{(T)}}}

A surface mass density ratio emerges, when the target molecule binds to the ridges and grooves that exhibit a affinity ratio. Ideally, the surface mass density ratio is identical to the affinity ratio. Non-idealities arise from nonspecific adsorption of the target molecule to the sensor surface and steric hindrance/masking of binding sites of already adsorbed target molecules.


In summary, the affinity ratio is a quality measure for the molographic structure that is obtained by the fabrication process whereas affinity modulation is linked via surface mass density and refractive index modulation to the measured molographic signal. 


Photoactivatable Polymer properties
-----------------------------------

Molographic chips are coated by a brushed copolymer adlayer. 

.. _figBrushedPolymer:

.. figure:: figures/Photoactivatable_Polymer.png
	:width: 60 %
	:align: center

	The photoactivatable light-sensitive graft copolymer is composed of a polyacrylic backbone, which is functionalized with primary amines and nitro-catechol groups for a strong surface attachment to the Ta2O5 surface1. Additionally, two types of polyethylene glycol (PEG) chains are incorporated to the graft copolymer. One unreactive methoxy-PEG2000 is used as a filler polymer to prevent nonspecific binding of proteins to the assay surface. A second photoactivatable PEG3400 derivative is used for the generation of the affinity modulation on the assay chip surface. It is composed of an amino-PEG3400 protected by a 2- (4-ethyl-2-nitro-5-phenylsulfanyl-phenyl)propyl carbonyl (PhSNPPOC) group2 at the primary amine.

The brushed polymer has a deprotection rate of 3.23*10-3 cm^2/mJ at 390 nm and 4.04*10-4 at 405 nm with the setups used. 

Phase Mask Optimizer
---------------------

This part of the module allows for robust optimization of the phase mask that is used in the Reactive Immersion Lithography Process of Focal Molography. 

.. automodule:: phasemask.phase_mask
	:members:

Phase Mask Experimental Data Fitter
------------------------------------
With these functions the intensity, activation ratios of the phase mask can be extracted from experimental data.

.. automodule:: phasemask.Fitting_functions_phase_mask_zeptoreader_data
	:members:
