"""
This is an example file how to style the Python-scripts for the
simulation tool of focal molography.
This docstring explains the purpose and the content of the file. The
line length does not exceed 72 letters in favor of readability.

:Created:		05-Dec-2016
:Author:		Silvio Bischof
:Revised:		06-Dec-2016 by Silvio Bischof
:References:	https://www.python.org/dev/peps/pep-0008/
				Alchin and Browning, Pro Python.
"""

# imports are always at the top of the file and should be on separate 
# lines. Import in the following order:
# 1. standard library
# 2. related third party imports
# 3. local application / library specific imports
# 4. globals and constants
# Seperate the groups by blank lines.
# Don't use wildcard imports (from <module> import *)

import sys
import os
import numpy as np
# second way to import classes / modules
from subprocess import Popen, PIPE
# use absolute imports; add root path
sys.path.append('./')

# two blank lines before a top-level function and class definition.
# use blank lines two indicate logical sections.


class ClassName(var_1, var_2):
	"""
	This is a class. Class names always begin with an upper case
	letter. 

	:param var_1: this is the first variable
	:type var_1: this is the type of the variable
	:param var_2: second variable
	:type var_2: second type
	"""

	def this_is_a_long_function_name(
			var_1, var_2, var_3,
			var_4, var_5=4, var_6=3):
		"""
		Function description comes here. Default parameters are defined
		like this (no whitespace)

		:param var_1: parameters described
		:type var_1: type
		...
		:returns: description of the return value
		:rtype: type of returned variable

		:example:

		>>> a = this_is_a_long_function_name(...)
        >>> print(a)

		:reference:

		Reference Eq. 555
		
		.. note:: this is a note

		.. math:: \vec{E} = q \cdot \vec{F}

		.. todo:: this should be done in the close future

		.. seealso:: blabla

		"""

		if (this_is_1_thing
				and	that_is_another_thing):
			# extra indentation, comment like this
			# "and" on second line, only two lines if >79 characters

		# lists and function with more than three arguments look 
		# like this. Comments always before the code.
		myList [
			1, 2, 3,
			4, 5, 6,
		]

		# binary operators before the operator aligned with opening
		# delimiter. Variable names as capsWords but with lower case
		# initial character. Underscore declare indices.
		incomeStudent = (grossWages
        				 + taxableInterest
        				 + (dividends - qualifiedDividends)
        				 - iraDeduction
        				 - studentLoanInterest)


		# we use single-quoted strings, white spaces are used as
		# follows
		function_call('string1', 'string2', var[1])

		# If operators with different priorities are used, add 
		# whitespace around the operators with the lowest 
		# priority(ies)
		a = x*x + y*y


# function name: all characters lower case, words separated by an
# underscore. Mathematical functions with the variables used in
# praxis (in accordance with documentation / previous functions)
def mathematical_function(m, a):
	"""
	Calculates the force according to Newtons second law of motion.

	:param m: mass in kg
	:type m: float
	:param a: acceleration in m/s^2
	:type a: float
	:returns: force in Newton
	:rtype: float

	:reference:
	https://en.wikipedia.org/wiki/Newton's_laws_of_motion
	"""
	return m*a

if __name__ == '__main__':
	# write a test function in order to describe the methods. Plots for
	# mathematical functions helpful. Imports required for test function
	# are listed here
	import matplotlib.pyplot as plt

	# assume ...
	m = 2700		# mass of a car in kg
	v_0 = 120		# initial velocity of the car in m/s
	v_1 = 60		# velocity after decelerating
	t = 10			# time needed to brake in seconds

	# ...

	# the force is then 
	F = mathematical_function(...)

	print(F)