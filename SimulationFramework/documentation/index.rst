.. Focal Molography documentation master file, created by
   sphinx-quickstart on Wed Oct 26 08:58:02 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Focal Molography Toolbox
========================

.. toctree::
   :caption: Focal Molography Contents
   :maxdepth: 1

   getting_started
   introduction
   initialization
   waveguide
   interface
   mologram
   phasemask
   particle
   materials
   performance
   GUI
   cuda
   miscellaneous
   binder
   Variable_management
   analysis
   analytical
   references

Bragg Molography
========================

.. toctree::
   :caption: Bragg Molography Contents
   :maxdepth: 1
   
   braggmolography

Fiber Bragg Molography
========================
.. toctree::
   :caption: Fiber Bragg Molography Content
   :maxdepth: 1

   CoupledModeTheoryCylindricalWaveguides
   CylindricalWaveguide
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

