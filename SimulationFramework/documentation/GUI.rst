.. _GUI:

Graphical User Interface (GUI)
==============================


Widgets
--------
.. toctree::
    
    GUI_MainWidget.rst
    GUI_mologramWidget.rst
    GUI_LoadWidget.rst


Tabs
---------
.. toctree::
    GUI_screentab.rst


» `top`_


