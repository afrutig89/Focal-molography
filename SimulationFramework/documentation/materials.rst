.. _materials:

Materials
=========

Theory
------


Dispersion Formulas
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Formula 1: Sellmeier
""""""""""""""""""""

The Sellmeier equation is an empirical relationship between refractive index and wavelength for a particular transparent medium. The refractive index :math:`n` is determined by the equation

.. math::
	{n} ^ {2} -1= {C}_{1} + \sum_{n=2}^{N} \frac{{B}_{n} {λ} ^ {2}}{{λ} ^ {2} - {C}_{n+1}^{2}}
	:label: sellmeier

Each term of the sum represents an absorption resonance of strength :math:`B_n` at a wavelength :math:`\sqrt{C_n}`. :math:`\lambda` is the vacuum wavelength in :math:`\textrm{\mu}`m.


Formula 2: Sellmeier-2
""""""""""""""""""""""

The Sellmeier equation is an empirical relationship between refractive index and wavelength for a particular transparent medium. The refractive index :math:`n` is determined by the equation

.. math::
	{n} ^ {2} -1= {C}_{1} + \sum_{n=2}^{N} \frac{{B}_{n} {λ} ^ {2}}{{λ} ^ {2} - {C}_{n+1}}
	:label: sellmeier

Each term of the sum represents an absorption resonance of strength :math:`B_n` at a wavelength :math:`\sqrt{C_n}`. :math:`\lambda` is the vacuum wavelength in :math:`\mathrm{\mu m}`.


Formula 3 and 5: Polynomial (Cauchy's equation)
"""""""""""""""""""""""""""""""""""""""""""""""

.. math::
	{n } ^ {2} = {C }_{1} + \sum_{n=2}^{N} {C }_{n} {λ } ^ {{C}_{n+1}}
	:label: cauchy

The equation is only valid for regions of normal dispersion in the visible wavelength region. In the infrared, the equation becomes inaccurate, and it cannot represent regions of anomalous dispersion.

Note that formula 5 has only 11 coefficients, whereas formula 3 has 17 (refractiveindex.info). However, the script does not distinguish the two cases.

Formula 4: RefractiveIndex.INFO
"""""""""""""""""""""""""""""""

.. math::
	{n } ^ {2} = {C}_{1} + \frac{{C }_{2} {λ} ^ {{C}_{3}} }{{λ} ^ {2} - {{C}_{4}} ^ {{C}_{5}}} + \frac{{C }_{6} {λ} ^ {{C}_{7}} }{{λ} ^ {2} - {{C}_{8}} ^ {{C}_{9}}} + {C}_{10} {λ } ^ {{C}_{11}} + {C}_{12} {λ } ^ {{C}_{13}} + {C }_{14} {λ } ^ {{C}_{15}} + {C}_{16} {λ } ^ {{C}_{17}}
	:label: refractiveIndexInfo


Formula 6: Gases
""""""""""""""""

.. math::
	n-1= {C}_{1} + \sum_{n=2}^{N} \frac{{C}_{n}}{{C}_{n+1} - {λ} ^ {-2}}
	:label: gases


Formula 7: Herzberger
"""""""""""""""""""""

.. math::
	n = {C}_{1} + \frac{{C}_{2}}{{λ} ^ {2} -  0.028 } + {C}_{3} {\left (\frac{1}{{λ} ^ {2} -0.028} \right )} ^ {2} + {C}_{4} {λ} ^ {2} + {C}_{5} {λ} ^ {4} + {C}_{6} {λ} ^ {6}
	:label: herzberger


Formula 8: Retro
""""""""""""""""

.. math::
	\frac{{n} ^ {2} -1}{{n} ^ {2} +2} = {C}_{1} + \frac{{C}_{2} {λ} ^ {2}}{{λ} ^ {2} - {C}_{3}} + {C}_{4} {λ} ^ {2}
	:label: retro


Formula 9: Exotic
"""""""""""""""""

.. math::
	{n} ^ {2} = {C}_{1} + \frac{{C}_{2}}{{λ} ^ {2} - {C}_{3}} + \frac{{C}_{4} (λ- {C}_{5} )}{{(λ- {C}_{5} )} ^ {2} + {C}_{6}}
	:label: exotic

Formula 10: Lorentz-Drude
"""""""""""""""""""""""""

For metals which contain a large density of free electrons, the Lorentz-Drude oscillator model can be used to describe a frequency dependent dielectric function: :cite:`rakic_optical_1998`

.. math::
	\varepsilon_{r}(\omega)=\varepsilon_{r}^{(f)}(\omega)+\varepsilon_{r}^{(b)}(\omega)
	:label: Lorentz_Drude

The first term :math:`\varepsilon_{r}^{(f)}` that accounts for the free-electron effects is

.. math::
	\varepsilon_{r}^{^{(f)}}(\omega)=1-\frac{{\omega_{p}^{2}}}{\omega^{2}+i\gamma\omega}
	:label: Drude_model


with the material dependent plasma frequency :math:`\omega_p`

According to Novotny :cite:`novotny_principles_2012`, the dielectric function follows Eq. :eq:`Drude_model` for wavelengths above 650 nm. Below, the energy of a photon is sufficient to cause interband transition accounted for by the second term :math:`\varepsilon_{r}^{(b)}`. It is expressed as a Lorenzian by

.. math::
	\varepsilon_{r}^{^{(b)}}(\omega)=\sum_{j=1}^{k}\frac{f_{j}\omega_{p}^{2}}{(\omega_{j}^{2}-\omega^{2})+i\gamma_j\omega}
	:label: Lorentz_model


where :math:`k` is the number of oscillators. Both the oscillator strength :math:`f_j` and damping :math:`\gamma_j` are dependent on the material. Rakic et. al. :cite:`rakic_optical_1998` provide these parameters for different materials.

.. note:: this formula has been added to extend the database by metals.


Formula 11: Solute Concentration
""""""""""""""""""""""""""""""""

Refractive index is a linear function of the solute concentration. The average refractive index of a molecule (dielectric particle) in water (:math:`n_0 = 1.33`) is

.. math::
	n = n_0 + \left(\frac{dn}{dc}\right) \rho_R
	:label: refractiveIndexReceptor

where the mass density :math:`\rho_R` of the a molecule is

.. math:: 
	\rho_{R} = 1.410 + 0.145 \cdot e^{- \frac{M_{R} \left[\mathrm{kDa}\right]}{13}}

with the molecular mass :math:`M_R`. For the refractive-index increment :math:`dn/dc` 0.182 ml/g is used. :cite:`de_feijter_ellipsometry_1978`

.. note:: this formula has been added to extend the database by proteins in a solution.

Implementation
--------------

.. automodule:: mateerials.materials
   :members:

» `top`_

.. _top: #