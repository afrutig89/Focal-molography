import sys
sys.path.append('Simulation Framework')

sys.path.append('Moloreader')
sys.path.append('Snippets')

from auxiliariesMoloreader.intensityConversionDamping import determineDampingConstant

from auxiliaries.writeLogFile import loadDB
from auxiliaries.Journal_formats_lib import formatPRX
import matplotlib
matplotlib.use('AGG')
import matplotlib.pylab as plt

import matplotlib
matplotlib.use('Qt4Agg')
import matplotlib.pylab as plt


if __name__ == '__main__':

    # testing of damping constant function
    path = '/home/focal-molography/Experiments/AF028_AutomaticDampingConstantDetermination'

    selection = 'img_meanPhys,moloLine'

    condition = "WHERE line=2 AND field='{}' AND chipNo='{}'".format('B','R305')

    damping_data = loadDB('{}/database/data.db'.format(path),'results',selection,condition,returnDataFrame = True)

    damping_data = damping_data.sort_values('moloLine',ascending=False)

    print(damping_data)
    damping_data = damping_data.groupby('moloLine').mean()

    
    formatPRX(2,2)
    plt.figure()
    img_meanPhys = damping_data['img_meanPhys']

    delta, alpha = determineDampingConstant(img_meanPhys,plot = True,path = path,name='test')

    print(damping_data)





